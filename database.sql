-- MySQL dump 10.13  Distrib 5.7.12, for osx10.9 (x86_64)
--
-- Host: singulerp-test.cmrxmfbhxajd.ap-south-1.rds.amazonaws.com    Database: singulerptest
-- ------------------------------------------------------
-- Server version	5.6.27-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `_orgid` bigint(10) unsigned NOT NULL,
  `company_name` varchar(500) NOT NULL,
  `grp` varchar(200) DEFAULT NULL,
  `address` varchar(550) DEFAULT NULL,
  `city` varchar(250) DEFAULT NULL,
  `state` varchar(250) DEFAULT NULL,
  `pincode` varchar(250) DEFAULT NULL,
  `country` varchar(200) DEFAULT NULL,
  `TIN` varchar(50) DEFAULT NULL,
  `area_code` varchar(50) DEFAULT NULL,
  `PAN` varchar(50) DEFAULT NULL,
  `credit_in_days` int(11) DEFAULT NULL,
  `CST` varchar(50) DEFAULT NULL,
  `credit_limit` int(11) DEFAULT NULL,
  `ECC` varchar(50) DEFAULT NULL,
  `customer_type` varchar(20) DEFAULT NULL,
  `date_of_introduction` timestamp NULL DEFAULT NULL,
  `service_location` varchar(300) DEFAULT NULL,
  `status` tinyint(5) DEFAULT NULL,
  `gst` varchar(50) DEFAULT NULL,
  `provisional_gst` varchar(50) DEFAULT NULL,
  `ARN` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `customer_contact_person`
--

DROP TABLE IF EXISTS `customer_contact_person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_contact_person` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  `designation` varchar(250) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `cell_no` varchar(50) DEFAULT NULL,
  `email` varchar(254) DEFAULT NULL,
  `contact_type` tinyint(10) DEFAULT NULL,
  `_customer_fkid` bigint(10) unsigned NOT NULL,
  `_orgid` bigint(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`),
  KEY `customer_fkid_idx` (`_customer_fkid`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `customer_product_assoc`
--

DROP TABLE IF EXISTS `customer_product_assoc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_product_assoc` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `_product_master_fkid` bigint(10) unsigned DEFAULT NULL,
  `_customer_fkid` bigint(10) unsigned DEFAULT NULL,
  `last_updated` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(6) DEFAULT NULL,
  `_orgid` bigint(10) unsigned DEFAULT NULL,
  `invoice_naration` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `customer_support`
--

DROP TABLE IF EXISTS `customer_support`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_support` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `ticket_no` int(11) NOT NULL,
  `ticket_date` date DEFAULT NULL,
  `_sales_order_fkid` bigint(10) unsigned NOT NULL,
  `invoice_no` int(50) DEFAULT NULL,
  `party_name` varchar(20) DEFAULT NULL,
  `party_address` varchar(45) DEFAULT NULL,
  `product_name` varchar(30) DEFAULT NULL,
  `details` varchar(45) DEFAULT NULL,
  `observations` varchar(45) DEFAULT NULL,
  `instructions` varchar(45) DEFAULT NULL,
  `action_taken` varchar(45) DEFAULT NULL,
  `customer_accepted` varchar(20) DEFAULT NULL,
  `date_visited` date DEFAULT NULL,
  `other_remarks` varchar(45) DEFAULT NULL,
  `your_remarks` varchar(45) DEFAULT NULL,
  `initiated_by` varchar(20) DEFAULT NULL,
  `verified_by` varchar(20) DEFAULT NULL,
  `checked_by` varchar(20) DEFAULT NULL,
  `authorized_by` varchar(20) DEFAULT NULL,
  `_orgid` bigint(10) DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_ticket_no_UNIQUE` (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `customer_warehouse_address`
--

DROP TABLE IF EXISTS `customer_warehouse_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_warehouse_address` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_fkid` bigint(10) unsigned DEFAULT NULL,
  `_orgid` bigint(10) unsigned DEFAULT NULL,
  `address_line1` varchar(550) DEFAULT NULL,
  `wcity` varchar(160) DEFAULT NULL,
  `wstate` varchar(160) DEFAULT NULL,
  `wcountry` varchar(160) DEFAULT NULL,
  `wpincode` varchar(250) DEFAULT NULL,
  `type_address` tinyint(6) DEFAULT NULL,
  `lastupdated` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(6) DEFAULT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `grn_no_used`
--

DROP TABLE IF EXISTS `grn_no_used`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grn_no_used` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `_orgid` bigint(10) unsigned NOT NULL,
  `grn_no` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `po-so_items_with_grn_fkid` bigint(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `group_master`
--

DROP TABLE IF EXISTS `group_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `group_master` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `grp` varchar(20) DEFAULT NULL,
  `sub_group` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hr_empcontract`
--

DROP TABLE IF EXISTS `hr_empcontract`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr_empcontract` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `_employee_fkid` bigint(10) unsigned NOT NULL,
  `employee_status` tinyint(5) DEFAULT NULL,
  `job_title` varchar(20) DEFAULT NULL,
  `contract_type` varchar(20) DEFAULT NULL,
  `contract_date` date DEFAULT NULL,
  `contract_renew_date` date DEFAULT NULL,
  `salary` int(30) DEFAULT NULL,
  `_orgid` bigint(10) DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hr_empleaves`
--

DROP TABLE IF EXISTS `hr_empleaves`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr_empleaves` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `_employee_fkid` bigint(10) unsigned NOT NULL,
  `monthly_leaves` int(11) DEFAULT NULL,
  `casual_leaves` int(11) DEFAULT NULL,
  `earn_leaves` int(11) DEFAULT NULL,
  `restricted_holidays` int(11) DEFAULT NULL,
  `_orgid` bigint(10) DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hr_employeedetails`
--

DROP TABLE IF EXISTS `hr_employeedetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr_employeedetails` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `address` varchar(45) DEFAULT NULL,
  `email_id` varchar(45) DEFAULT NULL,
  `contact_no` varchar(50) DEFAULT NULL,
  `emergency_contact` varchar(50) DEFAULT NULL,
  `econtact_name` varchar(20) DEFAULT NULL,
  `relationship` varchar(20) DEFAULT NULL,
  `id_proof` varchar(50) DEFAULT NULL,
  `blood_group` varchar(20) DEFAULT NULL,
  `qualification` varchar(20) DEFAULT NULL,
  `department` varchar(20) DEFAULT NULL,
  `designation` varchar(20) DEFAULT NULL,
  `job_title` varchar(45) DEFAULT NULL,
  `joining_date` date DEFAULT NULL,
  `_orgid` bigint(10) DEFAULT NULL,
  `employee_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `industry_type`
--

DROP TABLE IF EXISTS `industry_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `industry_type` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `industry_type` tinytext,
  `industry_group` tinytext,
  `industry_class` tinytext,
  `_orgid` bigint(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `item_master`
--

DROP TABLE IF EXISTS `item_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_master` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `group_master_fkid` bigint(10) unsigned NOT NULL,
  `grp` varchar(20) DEFAULT NULL,
  `sub_group` varchar(20) DEFAULT NULL,
  `grade` varchar(20) DEFAULT NULL,
  `unit_measure` varchar(20) DEFAULT NULL,
  `status` tinyint(5) DEFAULT NULL,
  `last_updated` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `item_org_assoc`
--

DROP TABLE IF EXISTS `item_org_assoc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_org_assoc` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_unique_code` varchar(200) NOT NULL,
  `name` tinytext,
  `grp` varchar(100) DEFAULT NULL,
  `sub_group` varchar(100) DEFAULT NULL,
  `grade` varchar(100) DEFAULT NULL,
  `unit_measure` varchar(100) DEFAULT NULL,
  `item_master_fkid` bigint(10) unsigned DEFAULT NULL,
  `invoice_narration` varchar(500) DEFAULT NULL,
  `min_stock_trigger` decimal(10,5) DEFAULT NULL,
  `min_order_quantity` decimal(10,5) DEFAULT NULL,
  `max_storage_quantity` decimal(10,5) DEFAULT NULL,
  `bin_location` varchar(100) DEFAULT NULL,
  `lead_time` decimal(10,5) DEFAULT NULL,
  `status` tinyint(5) DEFAULT NULL,
  `geo_location` varchar(200) DEFAULT NULL,
  `_orgid` bigint(10) unsigned NOT NULL,
  `last_updated` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `raw_material_or_product` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=916 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `item_rates`
--

DROP TABLE IF EXISTS `item_rates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_rates` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `_orgid` bigint(10) unsigned NOT NULL,
  `item_org_assoc_fkid` bigint(10) unsigned NOT NULL,
  `supplier_org_assoc_fkid` bigint(10) unsigned NOT NULL,
  `rate` decimal(10,2) NOT NULL,
  `currency` varchar(20) DEFAULT NULL,
  `start_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `latest` tinyint(5) DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `item_stock`
--

DROP TABLE IF EXISTS `item_stock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_stock` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `_orgid` bigint(10) unsigned NOT NULL,
  `item_org_assoc_fkid` bigint(10) unsigned NOT NULL,
  `expires_by` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `quantity` decimal(10,5) DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `rate` decimal(10,2) DEFAULT NULL,
  `currency` varchar(20) DEFAULT NULL,
  `geo_location` varchar(20) DEFAULT NULL,
  `last_updated` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `latest` varchar(20) DEFAULT NULL,
  `po_so_items_with_grn_fkid` bigint(10) unsigned DEFAULT NULL,
  `items_stock_adjustment_fkid` bigint(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`),
  KEY `item_org_assoc_fkid_idx` (`item_org_assoc_fkid`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `item_stock_adjustment`
--

DROP TABLE IF EXISTS `item_stock_adjustment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_stock_adjustment` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `_orgid` bigint(10) unsigned NOT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  `reason` varchar(45) DEFAULT NULL,
  `last_updated` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `remark` varchar(150) DEFAULT NULL,
  `date_of_inspection` timestamp NULL DEFAULT NULL,
  `quantity_on_record` decimal(10,5) DEFAULT NULL,
  `observed_quantity` decimal(10,5) DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `item_total_inventory`
--

DROP TABLE IF EXISTS `item_total_inventory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_total_inventory` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `_orgid` bigint(10) unsigned NOT NULL,
  `item_org_assoc_fkid` bigint(10) unsigned NOT NULL,
  `total_quantity` decimal(10,5) DEFAULT NULL,
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `geo_location` varchar(20) DEFAULT NULL,
  `status` tinyint(6) DEFAULT NULL,
  `low_level` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`),
  KEY `item_org_assoc_fkid2_idx` (`item_org_assoc_fkid`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `machine`
--

DROP TABLE IF EXISTS `machine`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `machine` (
  `_pkid` bigint(6) unsigned NOT NULL AUTO_INCREMENT,
  `machine_code` varchar(250) DEFAULT NULL,
  `machine_name` varchar(250) DEFAULT NULL,
  `geo_location` varchar(100) DEFAULT NULL,
  `added_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(6) unsigned DEFAULT NULL,
  `_orgid` bigint(6) unsigned DEFAULT NULL,
  PRIMARY KEY (`_pkid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `machine_master`
--

DROP TABLE IF EXISTS `machine_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `machine_master` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `grp` varchar(20) DEFAULT NULL,
  `sub_group` varchar(20) DEFAULT NULL,
  `industrial_name` varchar(30) DEFAULT NULL,
  `unit_of_measure` varchar(30) DEFAULT NULL,
  `capacity_measure` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `operation_master`
--

DROP TABLE IF EXISTS `operation_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `operation_master` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `grp` varchar(20) NOT NULL,
  `sub_group` varchar(20) DEFAULT NULL,
  `industrial_name` varchar(30) NOT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `org_supplier_item_assoc`
--

DROP TABLE IF EXISTS `org_supplier_item_assoc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `org_supplier_item_assoc` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `_orgid` bigint(10) unsigned NOT NULL,
  `supplier_org_assoc_fkid` bigint(10) unsigned NOT NULL,
  `item_org_assoc_fkid` bigint(10) unsigned NOT NULL,
  `status` tinyint(5) NOT NULL,
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `invoice_naration` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`),
  KEY `supplier_org_assoc_fkid_idx` (`supplier_org_assoc_fkid`),
  KEY `item_org_assoc_fkid5_idx` (`item_org_assoc_fkid`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `organisation`
--

DROP TABLE IF EXISTS `organisation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `organisation` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `orgid` bigint(10) unsigned DEFAULT NULL,
  `org_name` varchar(250) DEFAULT NULL,
  `org_username` varchar(150) DEFAULT NULL,
  `org_mobile` varchar(40) DEFAULT NULL,
  `org_legal_name` varchar(250) DEFAULT NULL,
  `org_brand_name` varchar(250) DEFAULT NULL,
  `org_email` varchar(45) DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `verification_code` varchar(45) DEFAULT NULL,
  `verification_status` tinyint(6) DEFAULT NULL,
  `status` tinyint(6) DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `organisation_factories`
--

DROP TABLE IF EXISTS `organisation_factories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `organisation_factories` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `_orgid` bigint(10) unsigned DEFAULT NULL,
  `factory_location` varchar(45) DEFAULT NULL,
  `factory_type` tinyint(6) DEFAULT NULL,
  `added_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` bigint(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `po_so_items`
--

DROP TABLE IF EXISTS `po_so_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `po_so_items` (
  `_id` bigint(40) unsigned NOT NULL AUTO_INCREMENT,
  `_orgid` bigint(40) unsigned DEFAULT NULL,
  `purchase_service_order_fkid` bigint(40) unsigned DEFAULT NULL,
  `item_org_assoc_fkid` bigint(40) unsigned DEFAULT NULL,
  `rate` decimal(10,2) DEFAULT NULL,
  `quantity` decimal(10,5) DEFAULT NULL,
  `received_quantity` decimal(10,5) DEFAULT '0.00000',
  `rejected_quantity` decimal(10,5) DEFAULT '0.00000',
  `total_cost` decimal(10,5) DEFAULT NULL,
  `status` tinyint(10) DEFAULT NULL,
  `packet_size` decimal(10,5) DEFAULT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `po_so_items_with_grn`
--

DROP TABLE IF EXISTS `po_so_items_with_grn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `po_so_items_with_grn` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `_orgid` bigint(10) unsigned NOT NULL,
  `purchase_service_order_fkid` bigint(10) unsigned NOT NULL,
  `item_org_assoc_fkid` bigint(10) unsigned NOT NULL,
  `po_so_items_fkid` bigint(40) unsigned DEFAULT NULL,
  `so_action` varchar(45) DEFAULT NULL,
  `rate` decimal(10,2) DEFAULT NULL,
  `quantity` decimal(10,5) DEFAULT NULL,
  `total_cost` decimal(10,5) DEFAULT NULL,
  `grn_no` int(11) DEFAULT NULL,
  `grn_status` tinyint(5) DEFAULT NULL,
  `received_sent_quantity` decimal(10,5) DEFAULT NULL,
  `received_sent_by` varchar(45) DEFAULT NULL,
  `rejected_quantity` decimal(10,5) DEFAULT NULL,
  `rejected_note` varchar(45) DEFAULT NULL,
  `rejected_by` varchar(45) DEFAULT NULL,
  `status` tinyint(5) DEFAULT NULL,
  `delivered_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `po_so_status`
--

DROP TABLE IF EXISTS `po_so_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `po_so_status` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `_orgid` bigint(10) unsigned NOT NULL,
  `purchase_service_order_fkid` bigint(10) unsigned NOT NULL,
  `actedon_by_fkid` bigint(10) unsigned NOT NULL,
  `date_actedon` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
  `status` tinyint(5) NOT NULL,
  `notes` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `po_so_supplier_invoice`
--

DROP TABLE IF EXISTS `po_so_supplier_invoice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `po_so_supplier_invoice` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `_orgid` bigint(10) unsigned DEFAULT NULL,
  `po_so_items_with_grn_list` varchar(45) DEFAULT NULL,
  `grn_no` int(11) DEFAULT NULL,
  `invoice_no` int(11) DEFAULT NULL,
  `invoice_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `dc_no` int(11) DEFAULT NULL,
  `dc_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `supplier_org_assoc_fkid` bigint(40) unsigned DEFAULT NULL,
  `purchase_service_order_fkid` bigint(40) unsigned DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `prefix_company_values`
--

DROP TABLE IF EXISTS `prefix_company_values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prefix_company_values` (
  `pkid` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `_orgid` bigint(10) unsigned NOT NULL,
  `prefix_master_fkid` bigint(10) unsigned NOT NULL,
  `value` varchar(50) DEFAULT NULL,
  `added_on` timestamp NULL DEFAULT NULL,
  `added_by_fkid` bigint(10) DEFAULT NULL,
  `status` tinyint(10) DEFAULT NULL,
  PRIMARY KEY (`pkid`),
  UNIQUE KEY `pkid_UNIQUE` (`pkid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `prefix_master`
--

DROP TABLE IF EXISTS `prefix_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prefix_master` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_code` varchar(45) DEFAULT NULL,
  `product_code` varchar(45) DEFAULT NULL,
  `supplier_code` varchar(45) DEFAULT NULL,
  `customer_code` varchar(45) DEFAULT NULL,
  `purchase_order_code` varchar(45) DEFAULT NULL,
  `sales_order_code` varchar(45) DEFAULT NULL,
  `employee_id_code` varchar(45) DEFAULT NULL,
  `machine_code` varchar(45) DEFAULT NULL,
  `production_order_code` varchar(45) DEFAULT NULL,
  `customer_ticket_code` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `product_bom`
--

DROP TABLE IF EXISTS `product_bom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_bom` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `quantity` decimal(10,5) DEFAULT NULL,
  `product_org_assoc_fkid` bigint(10) DEFAULT NULL,
  `item_org_assoc_fkid` bigint(10) DEFAULT NULL,
  `wastage` decimal(10,5) DEFAULT '0.00000',
  `status` tinyint(5) DEFAULT NULL,
  `_orgid` bigint(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `product_master`
--

DROP TABLE IF EXISTS `product_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_master` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_code` varchar(20) DEFAULT NULL,
  `product_name` varchar(20) DEFAULT NULL,
  `industry_type_fkid` bigint(10) unsigned NOT NULL,
  `grp` varchar(20) NOT NULL,
  `sub_group` varchar(20) NOT NULL,
  `industrial_name` varchar(45) NOT NULL,
  `unit_of_measure` varchar(20) NOT NULL,
  `capacity_measure` int(11) NOT NULL,
  `is_finished_product` tinyint(10) NOT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `product_org_assoc`
--

DROP TABLE IF EXISTS `product_org_assoc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_org_assoc` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_org_code` varchar(150) NOT NULL,
  `product_org_name` varchar(500) NOT NULL,
  `_orgid` bigint(10) unsigned NOT NULL,
  `product_master_fkid` bigint(10) unsigned DEFAULT NULL,
  `invoice_narration` varchar(500) NOT NULL,
  `rate_per_UOM` decimal(10,5) DEFAULT NULL,
  `minimum_stock_keep` decimal(10,5) NOT NULL,
  `lead_time` int(11) DEFAULT NULL,
  `status` tinyint(5) NOT NULL,
  `grp` varchar(45) DEFAULT NULL,
  `subgroup` varchar(45) DEFAULT NULL,
  `product_type` varchar(250) DEFAULT NULL,
  `unit_of_measure` varchar(45) DEFAULT NULL,
  `capacity_measure` decimal(10,5) DEFAULT NULL,
  `is_finished_product` tinyint(10) DEFAULT NULL,
  `transport` varchar(100) DEFAULT NULL,
  `process_name` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `product_process`
--

DROP TABLE IF EXISTS `product_process`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_process` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `process_name` varchar(20) NOT NULL,
  `product_master_fkid` bigint(10) unsigned DEFAULT NULL,
  `operation_fkid` bigint(10) unsigned NOT NULL,
  `machine_fkid` bigint(10) unsigned NOT NULL,
  `total_start_time` time(6) NOT NULL,
  `total_stop_time` time(6) NOT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `product_process_param`
--

DROP TABLE IF EXISTS `product_process_param`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_process_param` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_process_fkid` bigint(10) unsigned NOT NULL,
  `start_time` time(6) DEFAULT NULL,
  `stop_time` time(6) DEFAULT NULL,
  `operation_param_type` varchar(20) DEFAULT NULL,
  `operation_param_measure_type` varchar(20) DEFAULT NULL,
  `operation_param_measure` varchar(20) DEFAULT NULL,
  `stage_no` varchar(20) DEFAULT NULL,
  `stage_name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `production_job_card`
--

DROP TABLE IF EXISTS `production_job_card`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `production_job_card` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `jobcard_no` int(11) DEFAULT NULL,
  `jobcard_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `purchase_service_order_fkid` bigint(10) unsigned NOT NULL,
  `order_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `invoice_no` int(11) DEFAULT NULL,
  `invoice_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `customer_name` varchar(20) DEFAULT NULL,
  `serial_no_fkuniqueSrno` bigint(10) unsigned NOT NULL,
  `status` tinyint(5) DEFAULT NULL,
  `QC_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `special_test` decimal(10,5) DEFAULT NULL,
  `machine_id` int(11) DEFAULT NULL,
  `firing_system` varchar(20) DEFAULT NULL,
  `max_oven_temp` decimal(10,5) DEFAULT NULL,
  `removal_temp` decimal(10,5) DEFAULT NULL,
  `preloading_check` varchar(20) DEFAULT NULL,
  `operator` varchar(20) DEFAULT NULL,
  `inspected_by` varchar(20) DEFAULT NULL,
  `inspection_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `notes` varchar(45) DEFAULT NULL,
  `_orgid` bigint(10) DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `production_order_status`
--

DROP TABLE IF EXISTS `production_order_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `production_order_status` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `_orgid` bigint(10) unsigned DEFAULT NULL,
  `production_orders_fkid` bigint(10) unsigned DEFAULT NULL,
  `status` tinyint(6) DEFAULT NULL,
  `latest` tinyint(6) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by_user_account_fkid` bigint(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `production_orders`
--

DROP TABLE IF EXISTS `production_orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `production_orders` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `_orgid` bigint(10) unsigned DEFAULT NULL,
  `batch_no` varchar(250) DEFAULT NULL,
  `order_date` timestamp NULL DEFAULT NULL,
  `org_product_assoc_fkid` bigint(10) unsigned DEFAULT NULL,
  `qty` decimal(10,5) DEFAULT NULL,
  `uom` varchar(250) DEFAULT NULL,
  `start_date` timestamp NULL DEFAULT NULL,
  `end_date` timestamp NULL DEFAULT NULL,
  `last_updated_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `added_by_user_account_fkid` bigint(10) unsigned DEFAULT NULL,
  `notes` varchar(250) DEFAULT NULL,
  `status` tinyint(6) DEFAULT NULL,
  `machine_name` varchar(250) DEFAULT NULL,
  `geo_location` bigint(6) unsigned DEFAULT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `production_queue`
--

DROP TABLE IF EXISTS `production_queue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `production_queue` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `_orgid` bigint(10) unsigned DEFAULT NULL,
  `org_product_assoc_fkid` bigint(10) unsigned DEFAULT NULL,
  `qty` decimal(10,5) DEFAULT NULL,
  `uom` varchar(250) DEFAULT NULL,
  `sales_order_fkid` bigint(10) unsigned DEFAULT NULL,
  `prod_order_fkid` bigint(10) unsigned DEFAULT NULL,
  `status` tinyint(6) DEFAULT NULL,
  `type` tinyint(6) DEFAULT NULL,
  `last_updated_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `notes` varchar(250) DEFAULT NULL,
  `added_by_user_account_fkid` bigint(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `production_queue_status`
--

DROP TABLE IF EXISTS `production_queue_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `production_queue_status` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `_orgid` bigint(10) unsigned DEFAULT NULL,
  `production_queue_fkid` bigint(10) unsigned DEFAULT NULL,
  `status` tinyint(6) DEFAULT NULL,
  `latest` tinyint(6) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by_user_account_fkid` bigint(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `purchase_service_order`
--

DROP TABLE IF EXISTS `purchase_service_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `purchase_service_order` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `unique_serial_no` varchar(150) DEFAULT NULL,
  `_orgid` bigint(10) unsigned DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `currency` varchar(20) DEFAULT NULL,
  `supplier_fkid` bigint(10) unsigned DEFAULT NULL,
  `delivery_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `geo_location` varchar(20) DEFAULT NULL,
  `instructions` varchar(45) DEFAULT NULL,
  `payment_terms` varchar(45) DEFAULT NULL,
  `billing_address` varchar(50) DEFAULT NULL,
  `discount_percentage` varchar(50) DEFAULT NULL,
  `excise_duty` varchar(50) DEFAULT NULL,
  `vat` varchar(50) DEFAULT NULL,
  `freight_type` varchar(45) DEFAULT NULL,
  `freight_cost` varchar(50) DEFAULT NULL,
  `octoroi` varchar(20) DEFAULT NULL,
  `item_total` varchar(50) DEFAULT NULL,
  `other_charges` tinytext,
  `other_total` varchar(50) DEFAULT NULL,
  `final_discount_percentage` varchar(50) DEFAULT NULL,
  `final_total` decimal(10,2) DEFAULT NULL,
  `status` tinyint(6) unsigned DEFAULT NULL,
  `delayed_status` tinyint(5) DEFAULT '0',
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qa_inspection_item`
--

DROP TABLE IF EXISTS `qa_inspection_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qa_inspection_item` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `report_no` int(11) NOT NULL,
  `product` varchar(20) NOT NULL,
  `product_sr_no` int(11) NOT NULL,
  `operations_performed` varchar(45) DEFAULT NULL,
  `remark` varchar(45) DEFAULT NULL,
  `_orgid` bigint(10) DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qa_inspection_predelivery`
--

DROP TABLE IF EXISTS `qa_inspection_predelivery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qa_inspection_predelivery` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `report_no` int(11) NOT NULL,
  `party_name` varchar(20) DEFAULT NULL,
  `invoice_no` int(11) DEFAULT NULL,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `product` varchar(20) DEFAULT NULL,
  `product_sr_no` int(11) NOT NULL,
  `material` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `observation` varchar(45) DEFAULT NULL,
  `checking` varchar(45) DEFAULT NULL,
  `remark` varchar(45) DEFAULT NULL,
  `_orgid` bigint(10) DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `registration`
--

DROP TABLE IF EXISTS `registration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `registration` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `org_name` varchar(45) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  `email` varchar(45) DEFAULT NULL,
  `contact_no` int(11) DEFAULT NULL,
  `subsciption_type` varchar(20) DEFAULT NULL,
  `no_of_users` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `reports_created`
--

DROP TABLE IF EXISTS `reports_created`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reports_created` (
  `pkid` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `_orgid` bigint(10) unsigned NOT NULL,
  `reports_master_fkid` bigint(10) unsigned NOT NULL,
  `unique_code` varchar(500) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` bigint(10) unsigned NOT NULL,
  `status` tinyint(5) NOT NULL DEFAULT '1',
  PRIMARY KEY (`pkid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `reports_datastore`
--

DROP TABLE IF EXISTS `reports_datastore`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reports_datastore` (
  `pkid` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `_orgid` bigint(10) unsigned NOT NULL,
  `reports_created_fkid` bigint(10) unsigned NOT NULL,
  `field_name` varchar(500) NOT NULL,
  `field_value` varchar(500) NOT NULL,
  `status` tinyint(5) NOT NULL DEFAULT '1',
  PRIMARY KEY (`pkid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `reports_fields`
--

DROP TABLE IF EXISTS `reports_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reports_fields` (
  `pkid` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `_orgid` bigint(10) unsigned NOT NULL,
  `reports_master_fkid` bigint(10) unsigned NOT NULL,
  `field_name` varchar(500) NOT NULL,
  `field_type` tinyint(5) NOT NULL,
  `listing_order` tinyint(30) NOT NULL,
  `status` tinyint(5) NOT NULL,
  PRIMARY KEY (`pkid`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `reports_master`
--

DROP TABLE IF EXISTS `reports_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reports_master` (
  `pkid` bigint(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(500) NOT NULL,
  `unique_code` varchar(250) NOT NULL,
  `_orgid` bigint(5) unsigned NOT NULL,
  `type` varchar(250) NOT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` bigint(10) DEFAULT NULL,
  `status` tinyint(5) NOT NULL DEFAULT '1',
  PRIMARY KEY (`pkid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `roles_master`
--

DROP TABLE IF EXISTS `roles_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles_master` (
  `roles_master_pkid` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `module` tinytext,
  `role_type` tinyint(6) DEFAULT NULL,
  `functionality` tinytext,
  PRIMARY KEY (`roles_master_pkid`),
  UNIQUE KEY `_id_UNIQUE` (`roles_master_pkid`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sales_order`
--

DROP TABLE IF EXISTS `sales_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales_order` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_no` varchar(150) DEFAULT NULL,
  `order_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `customer_fkid` bigint(10) unsigned DEFAULT NULL,
  `job_type` varchar(20) DEFAULT NULL,
  `delivery_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `excise_charges` varchar(50) DEFAULT NULL,
  `transport_charges` varchar(50) DEFAULT NULL,
  `form30` varchar(25) DEFAULT NULL,
  `formc` varchar(25) DEFAULT NULL,
  `dispatch_type` varchar(25) DEFAULT NULL,
  `payment_terms` varchar(25) DEFAULT NULL,
  `remarks` varchar(45) DEFAULT NULL,
  `_orgid` bigint(10) DEFAULT NULL,
  `total_quantity` varchar(50) DEFAULT NULL,
  `total_price` varchar(50) DEFAULT NULL,
  `total_discount` varchar(50) DEFAULT NULL,
  `total_sales_price` decimal(10,3) DEFAULT NULL,
  `status` tinyint(6) DEFAULT NULL,
  `vat` decimal(10,3) DEFAULT NULL,
  `cst` decimal(10,3) DEFAULT NULL,
  `total_after_discount` decimal(10,3) DEFAULT NULL,
  `total_after_excise` decimal(10,3) DEFAULT NULL,
  `total_price_after_taxes` decimal(10,3) DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sales_order_items`
--

DROP TABLE IF EXISTS `sales_order_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales_order_items` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `_sales_order_fkid` bigint(10) unsigned DEFAULT NULL,
  `product_org_assoc_fkid` bigint(10) unsigned DEFAULT NULL,
  `quantity` decimal(10,5) DEFAULT NULL,
  `uom` varchar(25) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `discount` varchar(100) DEFAULT NULL,
  `delivery_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `_orgid` bigint(10) unsigned DEFAULT NULL,
  `status` tinyint(6) DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sales_order_status`
--

DROP TABLE IF EXISTS `sales_order_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales_order_status` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `_orgid` bigint(10) unsigned DEFAULT NULL,
  `sales_order_fkid` bigint(10) unsigned DEFAULT NULL,
  `actedon_by_fkid` bigint(10) unsigned DEFAULT NULL,
  `date_actedon` timestamp NULL DEFAULT NULL,
  `status` tinyint(6) DEFAULT NULL,
  `notes` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `so_excise`
--

DROP TABLE IF EXISTS `so_excise`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `so_excise` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `_orgid` bigint(10) unsigned NOT NULL,
  `po-so_items_with_grn_list` varchar(45) NOT NULL,
  `grn_no` int(11) NOT NULL,
  `invoice_no` int(11) NOT NULL,
  `invoice_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `dc_no` int(11) NOT NULL,
  `dc_date` timestamp(6) NOT NULL DEFAULT '0000-00-00 00:00:00.000000',
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `so_items_used`
--

DROP TABLE IF EXISTS `so_items_used`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `so_items_used` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `_orgid` bigint(10) unsigned DEFAULT NULL,
  `purchase_service_order_fkid` bigint(10) unsigned DEFAULT NULL,
  `item_org_assoc_fkid` bigint(10) unsigned DEFAULT NULL,
  `quantity_used` decimal(10,5) DEFAULT NULL,
  `status` tinyint(6) DEFAULT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `supplier_address`
--

DROP TABLE IF EXISTS `supplier_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `supplier_address` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `supplier_org_assoc_fkid` bigint(10) unsigned NOT NULL,
  `_orgid` bigint(10) unsigned NOT NULL,
  `address_line1` varchar(550) NOT NULL,
  `wcity` varchar(160) NOT NULL,
  `wstate` varchar(160) NOT NULL,
  `wcountry` varchar(160) NOT NULL,
  `wpincode` varchar(250) NOT NULL,
  `type_address` tinyint(5) DEFAULT NULL COMMENT '1 - HQ\n2 - Warehouse\n3 - Office',
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(5) NOT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`),
  KEY `supplier_master_fkid_idx` (`supplier_org_assoc_fkid`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `supplier_contact_person`
--

DROP TABLE IF EXISTS `supplier_contact_person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `supplier_contact_person` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `supplier_org_assoc_fkid` bigint(10) unsigned NOT NULL,
  `_orgid` bigint(10) unsigned NOT NULL,
  `contact_name` varchar(200) NOT NULL,
  `designation` varchar(200) NOT NULL,
  `email` varchar(95) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `mobile` varchar(45) DEFAULT NULL,
  `supplier_address_fkid` bigint(10) unsigned NOT NULL,
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`),
  KEY `supplier_master_fkid1_idx` (`supplier_org_assoc_fkid`),
  KEY `supplier_address_fkid_idx` (`supplier_address_fkid`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `supplier_master`
--

DROP TABLE IF EXISTS `supplier_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `supplier_master` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `category` varchar(20) NOT NULL,
  `company_name` varchar(20) NOT NULL,
  `hq_address_line1` varchar(45) NOT NULL,
  `city` varchar(20) NOT NULL,
  `state` varchar(20) NOT NULL,
  `country` varchar(20) NOT NULL,
  `pincode` int(11) NOT NULL,
  `status` tinyint(5) NOT NULL,
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `lst` varchar(20) NOT NULL,
  `cst` varchar(20) NOT NULL,
  `pan` varchar(20) NOT NULL,
  `ecc` varchar(20) NOT NULL,
  `gst` varchar(20) DEFAULT NULL,
  `notes` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `supplier_org_assoc`
--

DROP TABLE IF EXISTS `supplier_org_assoc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `supplier_org_assoc` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `supplier_unique_code` varchar(150) NOT NULL,
  `_orgid` bigint(10) unsigned NOT NULL,
  `supplier_master_fkid` bigint(10) unsigned DEFAULT NULL,
  `category` varchar(20) DEFAULT NULL,
  `company_name` varchar(505) NOT NULL,
  `hq_address_line1` varchar(550) NOT NULL,
  `city` varchar(150) NOT NULL,
  `state` varchar(150) NOT NULL,
  `country` varchar(250) NOT NULL,
  `pincode` varchar(250) NOT NULL,
  `lst` varchar(50) DEFAULT NULL,
  `cst` varchar(50) DEFAULT NULL,
  `pan` varchar(50) DEFAULT NULL,
  `ecc` varchar(50) DEFAULT NULL,
  `gst` varchar(50) DEFAULT NULL,
  `notes` varchar(550) DEFAULT NULL,
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(5) NOT NULL,
  `provisional_gst` varchar(50) DEFAULT NULL,
  `ARN` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_account`
--

DROP TABLE IF EXISTS `user_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_account` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(45) DEFAULT NULL,
  `password` text,
  `full_name` varchar(250) DEFAULT NULL,
  `_orgid` bigint(10) unsigned DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `status` tinyint(6) DEFAULT NULL,
  `usertype` tinyint(6) DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_roles_allocation`
--

DROP TABLE IF EXISTS `user_roles_allocation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_roles_allocation` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `_orgid` bigint(10) unsigned DEFAULT NULL,
  `user_account_fkid` bigint(10) unsigned DEFAULT NULL,
  `role_master_fkid` bigint(10) unsigned DEFAULT NULL,
  `assigned_by_fkid` bigint(10) unsigned DEFAULT NULL,
  `assigned_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(6) DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('1', 'settings', '1', 'add-user');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('2', 'settings', '2', 'view-users');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('3', 'settings', '1', 'edit-configuration');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('4', 'settings', '2', 'view-configuration');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('5', 'stock', '1', 'add-stock-items');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('6', 'stock', '2', 'view-stock-items');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('7', 'stock', '1', 'add-inventory');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('8', 'stock', '2', 'view-inventory');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('9', 'stock', '1', 'raise-grn');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('10', 'stock', '2', 'view-grn');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('11', 'suppliers', '1', 'add-suppliers');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('12', 'suppliers', '2', 'view-suppliers');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('13', 'suppliers', '1', 'add-supplier-associations');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('14', 'suppliers', '2', 'view-supplier-associations');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('15', 'purchases', '1', 'create-po');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('16', 'purchases', '2', 'view-po');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('17', 'customers', '1', 'add-customers');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('18', 'customers', '2', 'view-customers');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('19', 'customers', '1', 'add-customer-association');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('20', 'customers', '2', 'view-customer-associations');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('21', 'sales', '1', 'create-sales-orders');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('22', 'sales', '2', 'view-sales-orders');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('23', 'production', '1', 'create-production-orders');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('24', 'production', '2', 'view-production-orders');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('25', 'production', '1', 'create-products');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('26', 'production', '2', 'view-products');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('27', 'production', '1', 'create-process');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('28', 'production', '2', 'view-process');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('29', 'statistics', '1', 'create-statistics-master');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('30', 'statistics', '2', 'view-statistics-master');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('31', 'reports', '1', 'create-report-master');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('32', 'reports', '2', 'view-report-master');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('33', 'reports', '1', 'create-reports');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('34', 'reports', '2', 'view-reports');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('35', 'hr', '1', 'add-employee-details');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('36', 'hr', '2', 'view-employee-details');

INSERT INTO `prefix_master` (`_id`, `prefix_codes`) VALUES ('1', 'item_code');
INSERT INTO `prefix_master` (`_id`, `prefix_codes`) VALUES ('2', 'product_code');
INSERT INTO `prefix_master` (`_id`, `prefix_codes`) VALUES ('3', 'supplier_code');
INSERT INTO `prefix_master` (`_id`, `prefix_codes`) VALUES ('4', 'customer_code');
INSERT INTO `prefix_master` (`_id`, `prefix_codes`) VALUES ('5', 'purchase_order_code');
INSERT INTO `prefix_master` (`_id`, `prefix_codes`) VALUES ('6', 'sales_order_code');
INSERT INTO `prefix_master` (`_id`, `prefix_codes`) VALUES ('7', 'employee_id_code');
INSERT INTO `prefix_master` (`_id`, `prefix_codes`) VALUES ('8', 'machine_code');
INSERT INTO `prefix_master` (`_id`, `prefix_codes`) VALUES ('9', 'production_order_code');
INSERT INTO `prefix_master` (`_id`, `prefix_codes`) VALUES ('10', 'customer_ticket_code');
--
-- Dumping routines for database 'singulerptest'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-05-25 17:45:22
