package controllers

import (
	"fmt"
	"strconv"
	"time"
	"strings"

	"bitbucket.org/caslinoerp/singulerp/models"
	"github.com/astaxie/beego"
)

type BaseController struct {
	// Embedding: "Inherit" beego.Controller
	beego.Controller
}

func (this *BaseController) Prepare() {
	var Orgname string
	sessionVar := this.StartSession()
	if sessionVar != nil {
		Orgname = fmt.Sprintf("%s", sessionVar.Get("Orgname"))
		this.Data["Orgname"] = Orgname
	} else {
		this.Redirect("/login", 301)
	}
}

func (this *BaseController) GetGeneratedCode(orgid string, code int, timezone string) (string, string) {
	prefixLeftData, errl := models.GetPrefixCodesForLeft(orgid, code)
	prefixRightData, errr := models.GetPrefixCodesForRight(orgid, code)
	var outputString string
	var ncode string

	if errl == 1 {
		for i := 3; i > 0; i-- {
			if prefixLeftData[i]["prefix_dropdown_master_fkid"].(string) != ""{
				outputString += this.GetPrefixValues(prefixLeftData[i]["prefix_dropdown_master_fkid"].(string), prefixLeftData[i]["value"].(string), timezone)			
				outputString += "-"
			}
			
		}

	}

	ncode = strconv.Itoa(this.GetNewCode(orgid, code))
	outputString += ncode
	outputString += "-"

	if errr == 1 {
		for i := 1; i <= len(prefixRightData); i++ {
			if prefixRightData[i]["prefix_dropdown_master_fkid"].(string) != ""{
				outputString += this.GetPrefixValues(prefixRightData[i]["prefix_dropdown_master_fkid"].(string), prefixLeftData[i]["value"].(string), timezone)
				outputString += "-"
			}			
		}
	}

	outputString = strings.TrimRight(outputString, "-")
	return outputString, ncode
}

func (this *BaseController) GetPrefixValues(code string, value string, timezone string) string {
	switch codeString := code; codeString {
	case "1":
		return strconv.Itoa(time.Now().Year())
	case "2":
		return strconv.Itoa(time.Now().Year())
	case "3":
		return time.Now().Local().Format("20060102")
	case "4":
		timeZoneA, _ := time.LoadLocation(timezone)
		return time.Now().In(timeZoneA).Format("150405")
	case "5":
		return "A"
	case "6":
		return "1"
	case "7":
		return value
	case "8":
		return time.Now().Format("Jan")
	case "9":
		return time.Now().Format("Monday")
	default:
		return ""
	}
}

func (this *BaseController) GetNewCode(orgid string, code int) int {
	var maxCode int
	maxCode = 0

	switch codeValue := code; codeValue {
	case 1:
		maxCode = models.GetMaxItemCode(orgid)
	case 2:
	case 3:
		maxCode = models.GetMaxSupplierCode(orgid)
	case 4:
		maxCode = models.GetMaxCustomerCode(orgid)
	case 5:
		maxCode = models.GetMaxPurchaseOrderCode(orgid)
	case 6:
		maxCode = models.GetMaxSalesOrderCode(orgid)
	case 7:
		maxCode = models.GetMaxEmployeeCode(orgid)
	case 8:
		maxCode = models.GetMaxMachineCode(orgid)
	case 9:
		maxCode = models.GetMaxProductionOrderCode(orgid)
	case 10:
		maxCode = models.GetMaxSupportTicketCode(orgid)
	case 11:
		maxCode = models.GetMaxGRNCode(orgid)
	case 12:
		maxCode = models.GetMaxTransportationCode(orgid)
	}
	return maxCode + 1
}
