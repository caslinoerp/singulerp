package controllers

import (
	"fmt"
	"html/template"

	"bitbucket.org/caslinoerp/singulerp/models"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"github.com/astaxie/beego/validation"
)

type HrController struct {
	BaseController
}

type addEmployeeValidate struct {
	Empid     string `form:"emp"`
	Name      string `form:"name" valid:"Required"`
	DOB       string `form:"dob" valid:"Required"`
	Email     string `form:"email" valid:"Email"`
	ContactNo string `form:"contact_no"`
	//CellNo            string `form:"cell_no" `
	OfficeAddress string `form:"office_address" valid:"Required"`
	//Address           string `form:"address" valid:"Required"`
	EContact      string `form:"e_contact" valid:"Required"`
	EPhoneNo      string `form:"e_phone_no" `
	Relationship  string `form:"relationship" valid:"Required"`
	BloodGroup    string `form:"blood_group" valid:"Required"`
	Qualification string `form:"qualification" valid:"Required"`
	Department    string `form:"department" valid:"Required"`
	Designation   string `form:"designation" valid:"Required"`
	JobTitle      string `form:"job_title" valid:"Required"`
	ID            string `form:"id_proof" valid:"Required"`
	DOJ           string `form:"doj" valid:"Required"`
	//Salary            string `form:"salary" valid:"Required"`
}

type manageLeavesValidate struct {
	EmpId     string `form:"emp_id" valid:"Required"`
	EmpName   string `form:"emp_name" `
	MLeaves   string `form:"m_leaves" valid:"Required"`
	CLeaves   string `form:"c_leaves" valid:"Required"`
	ELeaves   string `form:"e_leaves" valid:"Required"`
	RHolidays string `form:"r_holidays" valid:"Required"`
}

type manageContractValidate struct {
	EId               string `form:"e_id" valid:"Required"`
	EName             string `form:"e_name"`
	EStatus           string `form:"emp_status" valid:"Required"`
	JobTitle          string `form:"job_title" valid:"Required"`
	ContractType      string `form:"contract_type" valid:"Required"`
	ContractDate      string `form:"c_date" valid:"Required"`
	ContractRenewDate string `form:"cr_date" valid:"Required"`
	Salary            string `form:"salary" valid:"Required"`
}

type searchEmployees struct {
	EmployeeName       string `form:"EmployeeName"`
	EmployeeDepartment string `form:"EmployeeDepartment"`
}

func (c *HrController) CreateNewEmployee() {
	var user_id string
	var _orgId string
	var username string
	var localetimezone string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
		localetimezone = fmt.Sprintf("%s", sessionVar.Get("LocaleTimeZone"))
	} else {
		user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
		localetimezone = fmt.Sprintf("%s", c.Ctx.Input.GetData("LocaleTimeZone"))
	}
	c.Data["username"] = username
	formStruct1 := models.Employee{
		Orgid:    _orgId,
		Userid:   user_id,
		Username: username,
	}

	newGenCode, newCode := c.GetGeneratedCode(_orgId, 7, localetimezone)
	c.Data["genCode"] = newGenCode

	retrieved1 := make(map[string]orm.Params)
	retrieved1 = models.GetLastEmp(formStruct1)
	c.Data["Retrieved1"] = retrieved1
	retrieved := make(map[string]orm.Params)
	retrieved = models.GetEmployeeDetails(formStruct1)
	c.Data["Retrieved"] = retrieved
	c.TplName = "hr/employee-details.html"
	c.Data["HR"] = 1
	c.Data["PageTitle"] = "Employee Details"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	//generic code to handle post request and validation
	if c.Ctx.Input.Method() == "POST" {
		if c.GetString("Searchmethod") == "srch" {
			var buttonSearch string
			c.Ctx.Input.Bind(&buttonSearch, "eployee-search")
			if buttonSearch == "SearchEmployee" {
				flash := beego.NewFlash()
				items := searchEmployees{}
				if err := c.ParseForm(&items); err != nil {
					flash.Error("Cannot parse form")
					//flash.Store(&c.Controller)
					return
				}
				retrieved := make(map[string]orm.Params)
				retrieved = models.SearchEmployees(items.EmployeeName, items.EmployeeDepartment, _orgId)
				c.Data["SearchItems"] = items
				c.Data["Retrieved"] = retrieved
				c.TplName = "hr/employee-details.html"
				c.Data["PageTitle"] = "Employee Details"
				c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
				return
			}
			c.Ctx.Input.Bind(&buttonSearch, "Reset-employeeSearch")
			if buttonSearch == "ResetEmployeeSearch" {
				c.Redirect("/hr/employee-details", 303)
				return
			}
		}
		flash := beego.NewFlash()
		items := addEmployeeValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			c.Data["ErrorsPresent"] = true
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			c.Data["ErrorsPresent"] = true
			return
		}

		formStruct := models.Employee{
			Orgid:      _orgId,
			Userid:     user_id,
			Username:   username,
			Empid:      newCode,
			Name:       items.Name,
			DOB:        items.DOB,
			Address:    items.OfficeAddress,
			Email_id:   items.Email,
			Contact_no: items.ContactNo,
			//Cell_no:         items.CellNo,
			Emergency_contact: items.EPhoneNo,
			Econtact_name:     items.EContact,
			Relationship:      items.Relationship,
			Id_proof:          items.ID,
			Blood_group:       items.BloodGroup,
			Qualification:     items.Qualification,
			Department:        items.Department,
			Designation:       items.Designation,
			Job_title:         items.JobTitle,
			Joining_date:      items.DOJ,
			NewGenCode:        newGenCode,
			//Salary:          items.Salary,
		}
		//insertStatus := models.RegistrationInsert(formData)
		insertStatus := models.CreateNewEmployee(formStruct)
		if insertStatus == 0 {
			flash.Error("An error occured while saving. Please try again.")
			flash.Store(&c.Controller)
			c.Data["ErrorsPresent"] = true
			return
		}
		c.Redirect("/hr/employee-details", 303)

	}
}

func (c *HrController) ManageContracts() {
	var user_id string
	var _orgId string
	var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}
	c.Data["username"] = username
	formStruct1 := models.Contract{
		Orgid:    _orgId,
		Userid:   user_id,
		Username: username,
	}
	retrieved := make(map[string]orm.Params)
	retrieved = models.GetEmployeeContract(formStruct1)
	c.Data["Retrieved"] = retrieved
	c.TplName = "hr/manage-contracts.html"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Manage Contracts"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	//generic code to handle post request and validation
	if c.Ctx.Input.Method() == "POST" {
		flash := beego.NewFlash()
		items := manageContractValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			c.Data["ErrorsPresent"] = true
			return
		}

		formStruct := models.Contract{
			Orgid:             _orgId,
			Userid:            user_id,
			Username:          username,
			EmpID:             items.EId,
			EmpStatus:         items.EStatus,
			JobTitle:          items.JobTitle,
			ContractType:      items.ContractType,
			ContractDate:      items.ContractDate,
			ContractRenewDate: items.ContractRenewDate,
			Salary:            items.Salary,
		}
		//insertStatus := models.RegistrationInsert(formData)
		insertStatus := models.ManageContracts(formStruct)
		if insertStatus == 0 {
			flash.Error("An error occured while saving. Please try again.")
			flash.Store(&c.Controller)
			return
		}
		c.Redirect("/hr/manage-contracts", 303)
	}
}

func (c *HrController) ManageLeaves() {
	var user_id string
	var _orgId string
	var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}
	c.Data["username"] = username
	formStruct1 := models.Leaves{
		Orgid:    _orgId,
		Userid:   user_id,
		Username: username,
	}
	retrieved := make(map[string]orm.Params)
	retrieved = models.GetEmployeeLeaves(formStruct1)
	c.Data["Retrieved"] = retrieved
	c.TplName = "hr/manage-leaves.html"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Manage Leaves"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	//generic code to handle post request and validation
	if c.Ctx.Input.Method() == "POST" {
		flash := beego.NewFlash()
		items := manageLeavesValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			c.Data["ErrorsPresent"] = true
			return
		}

		formStruct := models.Leaves{
			Orgid:     _orgId,
			Userid:    user_id,
			Username:  username,
			EmpID:     items.EmpId,
			Mleaves:   items.MLeaves,
			Cleaves:   items.CLeaves,
			Eleaves:   items.ELeaves,
			Rholidays: items.RHolidays,
		}
		//insertStatus := models.RegistrationInsert(formData)
		insertStatus := models.ManageLeaves(formStruct)
		if insertStatus == 0 {
			flash.Error("An error occured while saving. Please try again.")
			flash.Store(&c.Controller)
			return
		}

		c.Redirect("/hr/manage-leaves", 303)
	}
}
