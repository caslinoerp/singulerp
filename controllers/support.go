package controllers

import (
	"fmt"
	"html/template"

	"bitbucket.org/caslinoerp/singulerp/models"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"github.com/astaxie/beego/validation"
)

type SupportController struct {
	BaseController
}

type serviceTicketValidate struct {
	TicketNo     string `form:"ticket_no" valid:"Required"`
	TicketDate   string `form:"date" valid:"Required"`
	SrNo         string `form:"sr_no" valid:"Required"`
	InvoiceNo    string `form:"invoice_no" valid:"Required"`
	CustName     string `form:"name" valid:"Required"`
	CustAddress  string `form:"address" valid:"Required"`
	ProductName  string `form:"p_name" valid:"Required" `
	Details      string `form:"details" valid:"Required"`
	Observations string `form:"observations" valid:"Required"`
	Instructions string `form:"instructions" valid:"Required"`
	Action       string `form:"action" valid:"Required"`
	CustAccepted string `form:"c_accepted" `
	DateVisit    string `form:"date_visited"`
	Remarks      string `form:"remarks"`
	InitiatedBy  string `form:"initiated_by" valid:"Required"`
	YourRemarks  string `form:"your_remarks"`
	VerifiedBy   string `form:"verified_by"`
	CheckedBy    string `form:"checked_by"`
	AuthorizedBy string `form:"authorized_by"`
}

type Search_SupportTicketDetails struct {
	InvoiceNumber string `form:"InvoiceNumber"`
	CustomerName  string `form:"CustomerName"`
}

func (c *SupportController) CreateNewSupportTicket() {
	var user_id string
	var _orgId string
	var username string
	var localetimezone string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
		localetimezone = fmt.Sprintf("%s", sessionVar.Get("LocaleTimeZone"))
	} else {
		user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
		localetimezone = fmt.Sprintf("%s", c.Ctx.Input.GetData("LocaleTimeZone"))
	}
	c.Data["username"] = username
	formStruct1 := models.Support{
		Orgid:    _orgId,
		Userid:   user_id,
		Username: username,
	}

	newGenCode, newCode := c.GetGeneratedCode(_orgId, 10, localetimezone)
	c.Data["genCode"] = newGenCode

	retrieved := make(map[string]orm.Params)
	retrieved = models.GetSupportTicket(formStruct1)
	c.Data["Retrieved"] = retrieved

	retrievedCustomers := make(map[string]orm.Params)
	retrievedCustomers = models.GetSupportCustomers(formStruct1)
	c.Data["RetrievedCustomers"] = retrievedCustomers

	retrievedProducts := make(map[string]orm.Params)
	//retrievedProducts = models.GetCustomerProducts(formStruct1)
	retrievedProducts = models.GetAllItemsForDropdowns(_orgId)
	c.Data["RetrievedProducts"] = retrievedProducts

	c.TplName = "support/customer-support-new.html"
	c.Data["Support"] = 1
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Support Ticket"
	//generic code to handle post request and validation
	if c.Ctx.Input.Method() == "POST" {

		if c.GetString("SearchSupportTicket") == "SupportTicket_Search" {
			var buttonSearch string
			c.Ctx.Input.Bind(&buttonSearch, "SupportTicket-search")
			if buttonSearch == "SearchSupportTicketDetails" {
				flash := beego.NewFlash()
				items := Search_SupportTicketDetails{}
				if err := c.ParseForm(&items); err != nil {
					flash.Error("Cannot parse form")
					return
				}
				retrieved := make(map[string]orm.Params)
				retrieved = models.SearchSupportTicketdetails(items.InvoiceNumber, items.CustomerName, _orgId)
				c.Data["SearchItems"] = items
				c.Data["Retrieved"] = retrieved
				c.TplName = "support/customer-support-new.html"
				c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
				c.Data["PageTitle"] = "Support Ticket"
				return
			}
			c.Ctx.Input.Bind(&buttonSearch, "Reset-SupportTicketSearch")
			if buttonSearch == "ResetSupportTicketSearch" {
				c.Redirect("/support/customer-support-new", 303)
				return
			}
		}

		flash := beego.NewFlash()
		items := serviceTicketValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			c.Data["ErrorsPresent"] = true
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			c.Data["ErrorsPresent"] = true
			return
		}

		formStruct := models.Support{
			Orgid:            _orgId,
			Userid:           user_id,
			Username:         username,
			TicketNo:         newCode,
			TicketDate:       items.TicketDate,
			Sales_order_fkid: items.SrNo,
			InvoiceNo:        items.InvoiceNo,
			Party_name:       items.CustName,
			Party_address:    items.CustAddress,
			ProductName:      items.ProductName,
			Observations:     items.Observations,
			Instructions:     items.Instructions,
			Action:           items.Action,
			CustAccepted:     items.CustAccepted,
			DateVisit:        items.DateVisit,
			Other_remarks:    items.Remarks,
			Your_remarks:     items.YourRemarks,
			InitiatedBy:      items.InitiatedBy,
			VerifiedBy:       items.VerifiedBy,
			CheckedBy:        items.CheckedBy,
			AuthorizedBy:     items.AuthorizedBy,
			NewGenCode:       newGenCode,
		}
		//insertStatus := models.RegistrationInsert(formData)
		insertStatus := models.CreateNewSupportTicket(formStruct)
		if insertStatus == 0 {
			flash.Error("An error occured while saving. Please try again.")
			flash.Store(&c.Controller)
			c.Data["ErrorsPresent"] = true
			return
		}
		c.Redirect("/support/customer-support-new", 303)
	}
}
