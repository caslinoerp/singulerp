package controllers

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"html/template"

	"bitbucket.org/caslinoerp/singulerp/models"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"github.com/astaxie/beego/validation"
)

type CustomersController struct {
	BaseController
}

//use this one
type EditCustomerValidate struct {
	CustomerID    string `form:"customer_id"`
	UniqueCode    string `form:"unique_code" valid:"Required"`
	CompanyName   string `form:"company_name" valid:"Required"`
	Group         string `form:"group" `
	Address       string `form:"address" valid:"Required"`
	City          string `form:"city" valid:"Required"`
	State         string `form:"state" valid:"Required"`
	Country       string `form:"country" valid:"Required"`
	Pincode       string `form:"pincode" valid:"Required"`
	TIN           string `form:"tin" `
	AreaCode      string `form:"area_code" `
	CST           string `form:"cst" `
	PAN           string `form:"pan" `
	ECC           string `form:"ecc" `
	CreditInDays  string `form:"cin"`
	CreditLimit   string `form:"credit_limit"`
	CustomerType  string `form:"customer_type" `
	Introduction  string `form:"introduction" `
	GST           string `form:"gst"`
	PGST          string `form:"provisional_gst"`
	MainGSTIN     string `form:"main_gstin"`
	ARN           string `form:"arn"`
	ServiceLoc    string `form:"service_loc" `
	CustomerCheck string `form:"customer_check"`
}

type addCustomerValidate struct {
	UniqueCode  string `form:"unique_code" valid:"Required"`
	CompanyName string `form:"company_name" valid:"Required"`
	Group       string `form:"group" `
	Address     string `form:"address" valid:"Required"`
	City        string `form:"city" valid:"Required"`
	State       string `form:"state" valid:"Required"`
	Country     string `form:"country" valid:"Required"`
	Pincode     string `form:"pincode" valid:"Required"`
	TIN         string `form:"tin" `
	AreaCode    string `form:"area_code" `
	CST         string `form:"cst" `
	PAN         string `form:"pan" `
	ECC         string `form:"ecc" `
	//LST               string `form:"lst" valid:"Required"`
	CreditInDays string `form:"cin" valid:"Required"`
	CreditLimit  string `form:"credit_limit" valid:"Required"`
	CustomerType string `form:"customer_type" `

	Introduction string `form:"introduction" `

	GST              string `form:"gst"`
	PGST             string `form:"provisional_gst"`
	MainGSTIN        string `form:"main_gstin" valid:"Required"`
	ARN              string `form:"arn"`
	ServiceLoc       string `form:"service_loc" `
	ContactPerson    string `form:"contactgrp[0][contact_person]" valid:"Required"`
	Designation      string `form:"contactgrp[0][designation]" valid:"Required"`
	Email            string `form:"contactgrp[0][email]"`
	PhoneNo          string `form:"contactgrp[0][phone_no]"`
	CellNo           string `form:"contactgrp[0][cell_no]"`
	WarehouseAddress string `form:"group-a[0][warehouse_address]"`
	WCity            string `form:"group-a[0][wcity]"`
	WState           string `form:"group-a[0][wstate]"`
	WCountry         string `form:"group-a[0][wcountry]"`
	WPincode         string `form:"group-a[0][wpincode]"`
	GSTIN            string `form:"group-a[0][gstin]"`
	CustomerCheck    string `form:"customer_check"`
	CustomerID       string `form:"customer_id"`
}

type customer_cp_struct struct {
	Name        []string `json:"name"`
	Designation []string `json:"designation"`
	Phone       []string `json:"phone"`
	Email       []string `json:"email"`
	CellNo      []string `json:"cellno"`
	Status      string   `json:"status"`
}

type customer_warehouse_struct struct {
	Address []string `json:"address"`
	City    []string `json:"city"`
	State   []string `json:"state"`
	Country []string `json:"country"`
	Pincode []string `json:"pincode"`
	GSTIN   []string `json:"gstin"`
	Status  string   `json:"status"`
}

type associateCustomerValidate struct {
	Customer        string   `form:"customer" valid:"Required"`
	Products        []string `form:"products[]"  valid:"Required"`
	SubmitButtonVal string   `form:"assoc-button"`
}

func ToNullString(s string) sql.NullString {
	fmt.Println(sql.NullString{String: s, Valid: s != ""})
	return sql.NullString{String: s, Valid: s != ""}
}

type Search_CustomerDetails struct {
	CompanyName          string `form:"CompanyName"`
	ContactPerson        string `form:"ContactPerson"`
	Location             string `form:"Location"`
	CustomerSearchOption string `form:"SearchCustomers"`
	ItemName             string `form:"item_name_check"`
}

func (c *CustomersController) AddNewCustomer() {
	flash := beego.ReadFromRequest(&c.Controller)
	if ok := flash.Data["error"]; ok != "" {
		c.Data["FlashErrors"] = ok
	}
	if ok := flash.Data["notice"]; ok != "" {
		// Display error messages
		c.Data["FlashNotices"] = ok
	}
	var user_id string
	var _orgId string
	var username string
	var localetimezone string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
		localetimezone = fmt.Sprintf("%s", sessionVar.Get("LocaleTimeZone"))
	} else {
		user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
		localetimezone = fmt.Sprintf("%s", c.Ctx.Input.GetData("LocaleTimeZone"))
	}
	c.Data["username"] = username
	formStructCustomer := models.Customer{
		Orgid:    _orgId,
		Userid:   user_id,
		Username: username,
	}
	// formstructItems := models.StockItems{
	// 	Orgid:    _orgId,
	// 	Userid:   user_id,
	// 	Username: username,
	// }

	newGenCode, newCode := c.GetGeneratedCode(_orgId, 4, localetimezone)
	c.Data["genCode"] = newGenCode

	retrieveditems := make(map[string]orm.Params)
	retrieveditems = models.GetAllItemsForDropdowns(_orgId)
	c.Data["ItemsRetrieved"] = retrieveditems

	retrieved := make(map[string]orm.Params)
	retrieved = models.GetCustomer(formStructCustomer)
	c.Data["Retrieved"] = retrieved
	c.Data["SearchByCustomerDetailsChecked"] = "checked"
	c.TplName = "customers/add-new-list.html"
	c.Data["Customers"] = 1
	c.Data["PageTitle"] = "Add New / List"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	//generic code to handle post request and validation
	if c.Ctx.Input.Method() == "POST" {
		if c.GetString("CustomerSearchmethod") == "Customer_Search" {
			var buttonSearch string
			c.Ctx.Input.Bind(&buttonSearch, "customer-search")
			if buttonSearch == "SearchCustomer" {
				flash := beego.NewFlash()
				items := Search_CustomerDetails{}
				if err := c.ParseForm(&items); err != nil {
					flash.Error("Cannot parse form")
					//flash.Store(&c.Controller)
					return
				}
				retrieved := make(map[string]orm.Params)
				if items.CustomerSearchOption == "SearchByCustomerDetails" {
					retrieved = models.SearchByCustomerDetails(items.CompanyName, items.ContactPerson, items.Location, _orgId)
					c.Data["SearchByCustomerDetailsChecked"] = "checked"
				}
				if items.CustomerSearchOption == "SearchByItemName" {
					retrieved = models.SearchCustomersByItemName(items.ItemName, _orgId)
					c.Data["SearchByItemNameChecked"] = "checked"
				}

				c.Data["SearchItems"] = items
				c.Data["Retrieved"] = retrieved
				c.TplName = "customers/add-new-list.html"
				c.Data["PageTitle"] = "Add New / List"
				c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
				return
			}
			c.Ctx.Input.Bind(&buttonSearch, "Reset-customerSearch")
			if buttonSearch == "ResetCustomerSearch" {
				c.TplName = "customers/add-new-list.html"
				c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
				c.Data["PageTitle"] = "Add New / List"
				return
			}

		}

		flash := beego.NewFlash()
		items := addCustomerValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			c.Data["ErrorsPresent"] = true
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			c.Data["ErrorsPresent"] = true
			return
		}

		formStruct := models.Customer{
			Orgid:        _orgId,
			UniqueCode:   newCode,
			CompanyName:  items.CompanyName,
			Group:        items.Group,
			Address:      items.Address,
			City:         items.City,
			State:        items.State,
			Country:      items.Country,
			Pincode:      items.Pincode,
			TIN:          items.TIN,
			AreaCode:     items.AreaCode,
			CST:          items.CST,
			PAN:          items.PAN,
			ECC:          items.ECC,
			CreditInDays: items.CreditInDays,
			CreditLimit:  items.CreditLimit,
			CustomerType: items.CustomerType,
			Introduction: items.Introduction,
			ServiceLoc:   items.ServiceLoc,
			//GST:           items.GST,
			MainGSTIN:     items.MainGSTIN,
			PGST:          items.PGST,
			ARN:           items.ARN,
			ContactPerson: items.ContactPerson,
			Designation:   items.Designation,
			Email:         items.Email,
			PhoneNo:       items.PhoneNo,
			CellNo:        items.CellNo,
			NewGenCode:    newGenCode,
			// WarehouseAddress: items.WarehouseAddress,
			// WCity:            items.WCity,
			// WState:           items.WState,
			// WCountry:         items.WCountry,
			// WPincode:         items.WPincode,
			// GSTIN: 			  items.GSTIN,
			//Customerid : 		 1,
		}
		//insertStatus := models.RegistrationInsert(formData)

		uniqueCodeS := models.CheckForExistingCustomerCode(items.UniqueCode, _orgId)
		if uniqueCodeS == 1 {
			flash.Error("Customer Code not unique")
			flash.Store(&c.Controller)
			c.Data["ErrorsPresent"] = true
			return
		}

		uniqueNameS := models.CheckForExistingCustomerName(items.CompanyName, _orgId)
		if uniqueNameS == 1 {
			flash.Error("Company Name already exists")
			flash.Store(&c.Controller)
			c.Data["ErrorsPresent"] = true
			return
		}

		warehouses := make([]map[string]string, 0, 3)
		c.Ctx.Input.Bind(&warehouses, "group-a")

		contactpersons := make([]map[string]string, 0, 3)
		c.Ctx.Input.Bind(&contactpersons, "contactgrp")

		insertStatus := models.AddNewCustomer(formStruct, warehouses, contactpersons)

		if insertStatus == 0 {
			flash.Error("An error occured while saving. Please try again.")
			flash.Store(&c.Controller)
			c.Data["ErrorsPresent"] = true
			return
		}

		flash.Notice("Customer Inserted")
		flash.Store(&c.Controller)
		c.Redirect("/customers/add-new-list", 303)
	}
}

func (c *CustomersController) GetCustomerContactPerson() {
	//var user_id string
	var _orgId string
	//var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		//user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		//username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		//user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		//username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}

	c.TplName = "customers/add-new-list.html"
	c.Data["Customers"] = 1
	c.Data["PageTitle"] = "Add New / List"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

	if c.Ctx.Input.Method() == "GET" {

		customer_id := c.GetString("customerId")
		//result := models.DeleteSuppliers(supplier_id, _orgId)
		name, designation, phone, cellno, email, status := models.GetViewContactPerson(customer_id, _orgId)
		outputObj := &customer_cp_struct{
			Name:        name,
			Designation: designation,
			Phone:       phone,
			Email:       email,
			CellNo:      cellno,
			Status:      status,
		}
		b, _ := json.Marshal(outputObj)
		c.Ctx.Output.Body([]byte(b))
	}
}

func (c *CustomersController) GetCustomerWarehouses() {
	//var user_id string
	var _orgId string
	//var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		//user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		//username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		//user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		//username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}

	c.TplName = "customers/add-new-list.html"
	c.Data["PageTitle"] = "Add New / List"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

	if c.Ctx.Input.Method() == "GET" {

		customer_id := c.GetString("customerId")
		//result := models.DeleteSuppliers(supplier_id, _orgId)
		address, city, state, country, pincode, gstin, status := models.GetCustomerWarehouses(customer_id, _orgId)
		outputObj := &customer_warehouse_struct{
			Address: address,
			City:    city,
			State:   state,
			Country: country,
			Pincode: pincode,
			GSTIN:   gstin,
			Status:  status,
		}
		b, _ := json.Marshal(outputObj)
		c.Ctx.Output.Body([]byte(b))
	}
}

func (c *CustomersController) DeleteCustomer() {
	//var user_id string
	var _orgId string
	//var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		//user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		//username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		//user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		//username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}

	c.TplName = "customers/add-new-list.html"
	c.Data["Customers"] = 1
	c.Data["PageTitle"] = "Add New / List"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

	if c.Ctx.Input.Method() == "GET" {

		customer_id := c.GetString("id")
		result := models.DeleteCustomer(customer_id, _orgId)
		c.Ctx.Output.Body([]byte(result))
	}
}

func (c *CustomersController) AssociateCustomerProducts() {
	flash := beego.ReadFromRequest(&c.Controller)
	if ok := flash.Data["error"]; ok != "" {
		c.Data["FlashErrors"] = ok
	}
	if ok := flash.Data["notice"]; ok != "" {
		// Display error messages
		c.Data["FlashNotices"] = ok
	}
	var user_id string
	var _orgId string
	var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}
	c.Data["username"] = username
	formStructSearch := models.Viewassoc2{
		Orgid:    _orgId,
		Userid:   user_id,
		Username: username,
	}
	retrieved := make(map[string]orm.Params)
	retrieved = models.SearchCustomer(formStructSearch)
	c.Data["Retrieved"] = retrieved
	retrievedProducts := make(map[string]orm.Params)
	retrievedProducts = models.GetAllItemsForDropdowns(_orgId)
	c.Data["RetrievedProducts"] = retrievedProducts
	c.TplName = "customers/associate-with-products.html"
	c.Data["Customers"] = 1
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Associate With Products"

	//generic code to handle post request and validation
	if c.Ctx.Input.Method() == "POST" {
		flash := beego.NewFlash()
		items := associateCustomerValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		if items.SubmitButtonVal == "supplier" {
			formStructAssoc := models.Viewassoc2{
				Orgid:    _orgId,
				Customer: items.Customer,
				Products: items.Products,
			}

			retrievedAssoc := make(map[string]orm.Params)
			retrievedAssoc = models.ViewAssoc(formStructAssoc)
			c.Data["RetrievedAssoc"] = retrievedAssoc
			return
		}
		if items.SubmitButtonVal == "items" {
			valid := validation.Validation{}
			if b, _ := valid.Valid(&items); !b {
				c.Data["Errors"] = valid.ErrorsMap
				c.Data["ErrorsPresent"] = true
				return
			}
			//fmt.Println("****Controller Products:", items.Products)
			formStruct := models.Viewassoc2{
				Orgid:    _orgId,
				Customer: items.Customer,
				Products: items.Products,
			}
			insertStatus := models.AssocCustProd(formStruct)
			if insertStatus == 0 {
				flash.Error("An error occured while saving. Please try again.")
				flash.Store(&c.Controller)
				c.Data["ErrorsPresent"] = true
				return
			} else if insertStatus == 1 {
				flash.Notice("The associations have been saved.")
				flash.Store(&c.Controller)
				//c.Redirect("/customers/associate-with-products", 303)
			}
		}
		c.Redirect("/customers/associate-with-products", 303)
	}
}

func (c *CustomersController) DeleteCustomersAssoc() {
	//var user_id string
	var _orgId string
	//var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		//user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		//username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		//user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		//username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}

	c.TplName = "customers/associate-with-products.html"
	c.Data["Customers"] = 1
	c.Data["PageTitle"] = "Associate With Products"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

	if c.Ctx.Input.Method() == "GET" {

		assoc_id := c.GetString("id")
		result := models.DeleteCustomersAssoc(assoc_id, _orgId)
		c.Ctx.Output.Body([]byte(result))
	}
}

func (c *CustomersController) EditCustomers() {
	flash := beego.ReadFromRequest(&c.Controller)
	if ok := flash.Data["error"]; ok != "" {
		c.Data["FlasErrors"] = ok
	}
	if ok := flash.Data["notice"]; ok != "" {
		// Display error messages
		c.Data["FlashNotices"] = ok
	}
	var user_id string
	var _orgId string
	var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}
	c.Data["username"] = username
	c.TplName = "customers/edit-customers.html"
	c.Data["Customers"] = 1
	c.Data["user_id"] = user_id
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Edit Customers"

	c.Data["ItemsRetrieved"] = models.GetActiveDropdownListOfCustomersByOrgid(_orgId)

	if c.Ctx.Input.Method() == "GET" {
		pkID := c.GetString("CustomerCode")
		flash := beego.NewFlash()
		items := addCustomerValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			return
		}
		_, c.Data["FormItems"] = models.GetCustomerDetailsByCompanyID(pkID, _orgId)
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

		retrievedcpersons := make(map[string]orm.Params)
		retrievedcpersons = models.GetCustomerContactPersonsByPkId(pkID, _orgId)
		c.Data["CPersonsRetrieved"] = retrievedcpersons

		retrievedwarehouses := make(map[string]orm.Params)
		retrievedwarehouses = models.GetCustomerWarehousesByPkId(pkID, _orgId)
		c.Data["WarehousesRetrieved"] = retrievedwarehouses

		return
	}
	if c.Ctx.Input.Method() == "POST" {
		var buttonSubmit string
		c.Ctx.Input.Bind(&buttonSubmit, "item-button")
		if buttonSubmit == "show" {
			// flash := beego.NewFlash()
			// items := addCustomerValidate{}
			// if err := c.ParseForm(&items); err != nil {
			// 	flash.Error("Cannot parse form")
			// 	flash.Store(&c.Controller)
			// 	return
			// }
			flash := beego.NewFlash()
			var recordPresentflag int64
			customerID := c.GetString("customer_check")
			recordPresentflag, c.Data["FormItems"] = models.GetCustomerDetailsByCompanyID(customerID, _orgId)
			if recordPresentflag == 0 {
				flash.Error("No detials present for the chosen supplier.")
				flash.Store(&c.Controller)
				return
			}
			c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

			retrievedcpersons := make(map[string]orm.Params)
			retrievedcpersons = models.GetCustomerContactPersonsByCompanyID(customerID, _orgId)
			c.Data["CPersonsRetrieved"] = retrievedcpersons

			retrievedwarehouses := make(map[string]orm.Params)
			retrievedwarehouses = models.GetCustomerWarehousesByCompanyID(customerID, _orgId)
			c.Data["WarehousesRetrieved"] = retrievedwarehouses

			return
		}
		flash := beego.NewFlash()
		customerDetailStruct := EditCustomerValidate{}
		if err := c.ParseForm(&customerDetailStruct); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			return
		}
		c.Data["FormItems"] = customerDetailStruct
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		valid := validation.Validation{}
		if b, _ := valid.Valid(&customerDetailStruct); !b {
			c.Data["Errors"] = valid.ErrorsMap
			c.Data["ErrorsPresent"] = true
			return
		}

		// get all contact-person form items
		contacts := make([]map[string]string, 0, 5)
		c.Ctx.Input.Bind(&contacts, "contactgrp")

		// get all warehouse form items
		warehouses := make([]map[string]string, 0, 5)
		c.Ctx.Input.Bind(&warehouses, "warehouse-group")

		checkCustomerStatus := models.CheckExistingCompanyCodeAgainstID(customerDetailStruct.CompanyName, _orgId, customerDetailStruct.CustomerID)
		if checkCustomerStatus == 1 {
			c.Data["CompanyNameExists"] = true
			return
		}
		customerDetails := models.CustomerDetails(customerDetailStruct)

		insertStatus := models.UpdateCustomerByCustomerStruct(customerDetails, contacts, warehouses, _orgId)
		if insertStatus == 0 {
			flash.Error("There was an error updating the detials. Please try again.")
			flash.Store(&c.Controller)
			return
		}
		if insertStatus == 2 {
			flash.Error("There was an error updating contact or warehouse details.")
			flash.Store(&c.Controller)
			return
		}
		c.Data["completeRegister"] = true
		flash.Notice("Customer details updated.")
		flash.Store(&c.Controller)
		c.Redirect("/customers/edit-customers?CustomerCode="+customerDetailStruct.CustomerID, 303)
	}
}

func (c *CustomersController) UpdateCustomerProductAssociation() {
	//var user_id string
	var _orgId string
	//var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		//user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		//username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		//user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		//username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}

	c.TplName = "customers/associate-with-products.html"
	c.Data["Customers"] = 1
	c.Data["PageTitle"] = "Associate With Products"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

	if c.Ctx.Input.Method() == "GET" {

		item_id := c.GetString("id")
		new_qty := c.GetString("new_qty")
		new_price := c.GetString("new_price")

		result := models.UpdateAssoc2(item_id, new_qty, new_price, _orgId)
		c.Ctx.Output.Body([]byte(result))
	}
}
