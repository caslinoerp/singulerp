package controllers

import (
	"github.com/astaxie/beego"
)

type ErrorController struct {
	beego.Controller
}

func (c *ErrorController) Error401() {
	c.Data["content"] = "You are not authorised to view this page."
	c.TplName = "error.html"
}

func (c *ErrorController) AccessDenied() {
	c.Data["content"] = "You are not authorised to view this page."
	c.TplName = "access-denied.html"
}

func (c *ErrorController) Error404() {
	c.Data["content"] = "Page Not Found."
	c.TplName = "error.html"
}

func (c *ErrorController) Error500() {
	c.Data["content"] = "Internal Server Error."
	c.TplName = "error.html"
}

func (c *ErrorController) ErrorDb() {
	c.Data["content"] = "No connection to the database."
	c.TplName = "dberror.html"
}

func (c *ErrorController) DberrorCheck() {
	c.Data["content"] = "No connection to the database."
	c.TplName = "dberror.html"
}

func (c *ErrorController) ErrorSessionOrContext() {
	c.Data["content"] = "There seems to be some internal error in the application. We are on it."
	c.Data["code"] = "909"
	c.TplName = "error.html"
}
