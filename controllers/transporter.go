package controllers

import (
	"fmt"
	"html/template"

	"bitbucket.org/caslinoerp/singulerp/models"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"github.com/astaxie/beego/validation"
)

type TransporterController struct {
	BaseController
}

type transporterValidate struct {
	TransporterName   string `form:"transporter_name" valid:"Required"`
	UniqueCode        string `form:"unique_code" valid:"Required"`
	MainContactPerson string `form:"mainContactPerson" valid:"Required"`
	Telephone         string `form:"telephone" valid:"Required"`
	Mobile            string `form:"mobile" valid:"Required"`
	Fax               string `form:"fax" `
	Email             string `form:"email" `
	HQ                string `form:"hq" valid:"Required"`
	City              string `form:"city" valid:"Required"`
	State             string `form:"state" valid:"Required"`
	Country           string `form:"country" valid:"Required"`
	Pincode           string `form:"pincode" valid:"Required"`
	GSTIN             string `form:"gstin" valid:"Required"`
	PAN               string `form:"pan" `
	Introduction      string `form:"introduction" `
	Notes             string `form:"notes" `
}

type Search_TranporterDetails struct {
	TransporterName         string `form:"TransporterName"`
	ContactPerson           string `form:"ContactPerson"`
	Location                string `form:"Location"`
	TransporterSearchOption string `form:"SearchTransporters"`
	ItemName                string `form:"item_name_check"`
}

func (c *TransporterController) CreateTransportation() {
	flash := beego.ReadFromRequest(&c.Controller)
	if ok := flash.Data["error"]; ok != "" {
		c.Data["FlashErrors"] = ok
	}
	if ok := flash.Data["notice"]; ok != "" {
		// Display error messages
		c.Data["FlashNotices"] = ok
	}
	var user_id string
	var _orgId string
	var username string
	var localetimezone string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
		localetimezone = fmt.Sprintf("%s", sessionVar.Get("LocaleTimeZone"))
	} else {
		user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
		localetimezone = fmt.Sprintf("%s", c.Ctx.Input.GetData("LocaleTimeZone"))
	}
	c.Data["username"] = username
	formStruct1 := models.Transporter{
		Orgid:    _orgId,
		Userid:   user_id,
		Username: username,
	}
	newGenCode, newCode := c.GetGeneratedCode(_orgId, 12, localetimezone)
	c.Data["genCode"] = newGenCode

	retrieved := make(map[string]orm.Params)
	retrieved = models.GetTransportersToView(formStruct1)
	c.Data["Retrieved"] = retrieved
	//c.Data["SearchByTransporterDetailsChecked"] = "checked"
	c.TplName = "transporter/add-new-transporter.html"
	c.Data["Transporter"] = 1
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Tranporter"
	//generic code to handle post request and validation
	if c.Ctx.Input.Method() == "POST" {
		flash := beego.NewFlash()
		if c.GetString("TransporterSearchmethod") == "Transporter_Search" {
			var buttonSearch string
			c.Ctx.Input.Bind(&buttonSearch, "transporter-search")
			if buttonSearch == "SearchTransporter" {

				items := Search_TranporterDetails{}
				if err := c.ParseForm(&items); err != nil {
					flash.Error("Cannot parse form")
					flash.Store(&c.Controller)
					return
				}
				retrieved := make(map[string]orm.Params)
				//if items.TransporterSearchOption == "SearchByTransporterDetails" {
				//fmt.Println("***Search")
				retrieved = models.SearchByTransporterDetails(items.TransporterName, items.ContactPerson, items.Location, _orgId)
				//c.Data["SearchByTransporterDetailsChecked"] = "checked"
				//}
				// if items.TransporterSearchOption == "SearchByItemName" {
				// 	//retrieved = models.SearchTransportersByItemName(items.ItemName, _orgId)
				// 	c.Data["SearchByItemNameChecked"] = "checked"
				// }

				c.Data["SearchItems"] = items
				c.Data["Retrieved"] = retrieved
				c.TplName = "transporter/add-new-transporter.html"
				c.Data["PageTitle"] = "Transporter"
				c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
				return
			}
			c.Ctx.Input.Bind(&buttonSearch, "Reset-transporterSearch")
			if buttonSearch == "ResetTransporterSearch" {
				c.TplName = "transporter/add-new-transporter.html"
				c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
				c.Data["PageTitle"] = "Transporter"
				return
			}

		}

		items := transporterValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			c.Data["ErrorsPresent"] = true
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			c.Data["ErrorsPresent"] = true
			return
		}

		formStruct := models.Transporter{
			Orgid:             _orgId,
			Userid:            user_id,
			Username:          username,
			TransporterName:   items.TransporterName,
			UniqueCode:        newCode,
			NewGenCode:        newGenCode,
			MainContactPerson: items.MainContactPerson,
			Telephone:         items.Telephone,
			Mobile:            items.Mobile,
			Fax:               items.Fax,
			Email:             items.Email,
			HQ:                items.HQ,
			City:              items.City,
			State:             items.State,
			Country:           items.Country,
			Pincode:           items.Pincode,
			GSTIN:             items.GSTIN,
			PAN:               items.PAN,
			Introduction:      items.Introduction,
			Notes:             items.Notes,
		}

		warehouses := make([]map[string]string, 0, 3)
		c.Ctx.Input.Bind(&warehouses, "warehouses")

		vehicles := make([]map[string]string, 0, 3)
		c.Ctx.Input.Bind(&vehicles, "vehicles")

		//insertStatus := models.RegistrationInsert(formData)
		insertStatus := models.CreateNewTransportation(formStruct, warehouses, vehicles)
		if insertStatus == 0 {
			flash.Error("An error occured while saving. Please try again.")
			flash.Store(&c.Controller)
			c.Data["ErrorsPresent"] = true
			//c.Redirect("/transportation/add-transporter", 302)
			return
		}
		flash.Notice("Transporter details added.")
		flash.Store(&c.Controller)
		c.Data["FlashNotices"] = "Transporter details added."
		c.Redirect("/transportation/add-transporter", 302)
	}
}

func (c *TransporterController) DeleteTransporter() {
	//var user_id string
	var _orgId string
	//var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		//user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		//username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		//user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		//username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}

	c.TplName = "transporter/add-new-transporter.html"
	c.Data["Transporter"] = 1
	c.Data["PageTitle"] = "Add New / List"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

	if c.Ctx.Input.Method() == "GET" {

		transporter_id := c.GetString("id")
		result := models.DeleteTransporter(transporter_id, _orgId)
		c.Ctx.Output.Body([]byte(result))
	}
}
