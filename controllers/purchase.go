package controllers

import (
	"encoding/json"
	"fmt"
	"html/template"
	"strconv"
	"time"

	"bitbucket.org/caslinoerp/singulerp/models"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"github.com/astaxie/beego/validation"
)

type PurchaseController struct {
	BaseController
}

type createPoValidate struct {
	TaxRegime            string `form:"tax_regime" valid:"Required"`
	POSONo               string `form:"po_so_no" valid:"Required"`
	PODate               string `form:"po_date" valid:"Required"`
	Supplier             string `form:"supplier" valid:"Required"`
	PoDeliveryDate       string `form:"po_delivery_date" valid:"Required"`
	DiscountSubTotal     string `form:"discount_sub_total"`
	Currency             string `form:"currency" valid:"Required" `
	DLoc                 string `form:"dloc" valid:"Required"`
	Transporter          string `form:"transporter"`
	TransporterWarehouse string `form:"transporter_warehouse"`
	Vehicle              string `form:"vehicle"`
	Notes                string `form:"notes"`
	TotalPrice           string `form:"tprice"`
	//ItemName                  string `form:"group-a[0][item_name]" `
	//UnitPrice                 string `form:"group-a[0][unit_price]"`
	//Quantity                  string `form:"group-a[0][qty]" `
	//UnitMeasure 	          string `form:"group-a[0][unit_measure]"`
	ItemName   string `form:"item_name" `
	UnitPrice  string `form:"unit_price" `
	Quantity   string `form:"qty" `
	PacketSize string `form:"p_size" `
	//UnitMeasure 	          string `form:"unit_measure" valid:"Required"`
	ItemName1   string `form:"item_name1"`
	UnitPrice1  string `form:"unit_price1"`
	Quantity1   string `form:"qty1"`
	PacketSize1 string `form:"p_size1" `
	//UnitMeasure1 	          string `form:"unit_measure1"`
	ItemName2   string `form:"item_name2"`
	UnitPrice2  string `form:"unit_price2"`
	Quantity2   string `form:"qty2"`
	PacketSize2 string `form:"p_size2" `
	//UnitMeasure2 	          string `form:"unit_measure2"`
	ItemName3   string `form:"item_name3"`
	UnitPrice3  string `form:"unit_price3"`
	Quantity3   string `form:"qty3"`
	PacketSize3 string `form:"p_size3" `
	//UnitMeasure3 	          string `form:"unit_measure3"`
	ItemName4   string `form:"item_name4"`
	UnitPrice4  string `form:"unit_price4"`
	Quantity4   string `form:"qty4"`
	PacketSize4 string `form:"p_size4" `
	//UnitMeasure4 	          string `form:"unit_measure4"`
	ItemName5   string `form:"item_name5"`
	UnitPrice5  string `form:"unit_price5"`
	Quantity5   string `form:"qty5"`
	PacketSize5 string `form:"p_size5" `
	//UnitMeasure5 	          string `form:"unit_measure5"`
	ItemName6   string `form:"item_name6"`
	UnitPrice6  string `form:"unit_price6"`
	Quantity6   string `form:"qty6"`
	PacketSize6 string `form:"p_size6" `
	// UnitMeasure6 	          string `form:"unit_measure6"`
	FreightType       string `form:"freight_type"`
	FreightCost       string `form:"freight_cost"`
	PaymentTerms      string `form:"payment_terms" `
	OtherChargesTitle string `form:"other_charges_title" `
	OtherChargesValue string `form:"other_charges_value"`
	ExciseDuty        string `form:"excise_duty" `
	OctroiCharges     string `form:"octroi_charges"`
	Total             string `form:"totalf"`
	FinalDiscount     string `form:"final_discount" `
	FinalTotal        string `form:"final_total" `
	BillingAddress    string `form:"billing_address" `

	TotalTaxableValue     string `form:"sum_total_taxable_value"`
	TotalTaxCess          string `form:"sum_total_tax_cess"`
	TotalAfterTaxes       string `form:"sum_total_after_taxes"`
	TotalFreightPackaging string `form:"sum_freight_packaging_others"`
	TotalInvoiceValue     string `form:"sum_total_invoice_value"`

	SubmitButtonVal string `form:"po-button"`
}

type createPoValidateEdit struct {
	TaxRegime        string `form:"tax_regime" valid:"Required"`
	POSONo           string `form:"po_so_no" valid:"Required"`
	PODate           string `form:"po_date" valid:"Required"`
	Supplier         string `form:"supplier" valid:"Required"`
	PoDeliveryDate   string `form:"po_delivery_date" valid:"Required"`
	DiscountSubTotal string `form:"discount_sub_total"`
	Currency         string `form:"currency" valid:"Required" `
	DLoc             string `form:"dloc" valid:"Required"`
	TotalPrice       string `form:"tprice"`
	ItemName         string `form:"group-a[0][item_name]" `
	UnitPrice        string `form:"group-a[0][unit_price]"`
	Quantity         string `form:"group-a[0][qty]" `
	UnitMeasure      string `form:"group-a[0][unit_measure]"`

	FreightType       string `form:"freight_type"`
	FreightCost       string `form:"freight_cost"`
	PaymentTerms      string `form:"payment_terms" `
	OtherChargesTitle string `form:"other_charges_title" `
	OtherChargesValue string `form:"other_charges_value"`
	ExciseDuty        string `form:"excise_duty" `
	OctroiCharges     string `form:"octroi_charges"`
	Total             string `form:"total"`
	FinalDiscount     string `form:"final_discount" `
	FinalTotal        string `form:"final_total" `
	BillingAddress    string `form:"billing_address" `
	PurchaseId        string `form:"purchase_id" `

	TotalTaxableValue     string `form:"sum_total_taxable_value" valid:"Required"`
	TotalTaxCess          string `form:"sum_total_tax_cess"`
	TotalAfterTaxes       string `form:"sum_total_after_taxes"`
	TotalFreightPackaging string `form:"sum_freight_packaging_others"`
	TotalInvoiceValue     string `form:"sum_total_invoice_value"`

	Transporter          string `form:"transporter"`
	TransporterWarehouse string `form:"transporter_warehouse"`
	Vehicle              string `form:"vehicle"`
	Notes                string `form:"notes"`
	SubmitButtonVal      string `form:"po-button"`
}

type serviceContractValidate struct {
	Contractor string `form:"contractor" valid:"Required"`
	ChallanNo  string `form:"challan_no" valid:"Required" `
	DateOrder  string `form:"date_order" valid:"Required"`
	//Items                     string `form:"itselect" valid:"Required"`
	RItem                 string `form:"resulting_item" valid:"Required"`
	RQuantity             string `form:"resulting_quantity" valid:"Required"`
	SDiscountSubTotal     string `form:"discount_sub_total" `
	Purpose               string `form:"purpose" valid:"Required"`
	EstimatedDeliveryDate string `form:"estimated_delivery_date" valid:"Required"`
	DispatchFrom          string `form:"dispatch_from"`
	DispatchDate          string `form:"dispatch_date"`
	DispatchFleet         string `form:"dispatch_fleet"`
	DispatchCost          string `form:"dispatch_cost"`
	SPaymentTerms         string `form:"spayment_terms"`
	SOtherChargesTitle    string `form:"sother_charges_title"`
	SOtherChargesValue    string `form:"sother_charges_value"`
	SExciseDuty           string `form:"sexcise_duty"`
	SOctroiCharges        string `form:"soctroi_charges"`
	STotal                string `form:"stotal"`
	SFinalDiscount        string `form:"sfinal_discount"`
	SFinalTotal           string `form:"sfinal_total" valid:"Required"`
	SBillingAddress       string `form:"sbilling_address"`
	SCurrency             string `form:"currency" `
	ItemName              string `form:"item_name" valid:"Required"`
	Quantity              string `form:"quantity" valid:"Required"`
	ItemName1             string `form:"item_name1"`
	Quantity1             string `form:"quantity1"`
	ItemName2             string `form:"item_name2"`
	Quantity2             string `form:"quantity2"`
	ItemName3             string `form:"item_name3"`
	Quantity3             string `form:"quantity3"`
	ItemName4             string `form:"item_name4"`
	Quantity4             string `form:"quantity4"`
	ItemName5             string `form:"item_name5"`
	Quantity5             string `form:"quantity5"`
	ItemName6             string `form:"item_name6"`
	Quantity6             string `form:"quantity6"`
	SubmitButtonVal       string `form:"so-button"`
}

type grnValidate struct {
	PoNo  string `form:"po_no" valid:"Required"`
	PoId  string `form:"po_id"`
	GRNNo string `form:"grn_no"`
	//ItemsArray []GrnItems
	Suppliers         string `form:"suppliers" valid:"Required"`
	DeliveryDate      string `form:"delivery_date" valid:"Required"`
	TotalCost         string `form:"total_cost" valid:"Required"`
	DeliveryLocation  string `form:"delivery_location" valid:"Required"`
	DeliveredDate     string `form:"delivered_date" valid:"Required"`
	SupplierInvoice   string `form:"supplier_invoice" valid:"Required"`
	SupplierInvoiceDt string `form:"supplier_invoice_dt" valid:"Required"`
	SupplierDcNo      string `form:"supplier_dc_no"`
	SupplierDcDt      string `form:"supplier_dc_dt"`
	LrNo              string `form:"lr_no" `
	LrDt              string `form:"lr_dt" `
	AddNote           string `form:"add_note"`
	ViewButton        string `form:"assoc-button"`
	Transporter       string `form:"transporter"`
	Vehicle           string `form:"vehicle"`
}

type mystruct2 struct {
	Items       []string `json:"items"`
	Quantity    []string `json:"quantity"`
	ReceivedQty []string `json:"received_qty"`
	RejectedQty []string `json:"rejected_qty"`
	PendingQty  []string `json:"pending_qty"`
	Rate        []string `json:"rate"`
	TotalCost   []string `json:"total_cost"`
	GstRate     []string `json:"gst_rate"`
	PacketSize  []string `json:"packet_size"`

	DiscountRate []string `json:"discount_rate"`
	DeliveryDate []string `json:"delivery_date"`
	UOM          []string `json:"uom"`

	DiscountValue       []string `json:"discount_value"`
	HsnSac              []string `json:"hsn_sac"`
	TaxableAmount       []string `json:"taxable_amount"`
	IgstRate            []string `json:"igst_rate"`
	IgstAmount          []string `json:"igst_amount"`
	CgstRate            []string `json:"cgst_rate"`
	CgstAmount          []string `json:"cgst_amount"`
	SgstRate            []string `json:"sgst_rate"`
	SgstAmount          []string `json:"sgst_amount"`
	CessRate            []string `json:"cess_rate"`
	CessAmount          []string `json:"cess_amount"`
	TotalTaxCess        []string `json:"total_tax_cess"`
	TotalAfterTax       []string `json:"total_after_tax"`
	Freight             []string `json:"freight"`
	Insurance           []string `json:"insurance"`
	PackagingForwarding []string `json:"packaging_forwarding"`
	Others              []string `json:"others"`
	TotalOtherCharges   []string `json:"total_other_charges"`
	TaxRegime           string   `json:"tax_regime"`
	Status              string   `json:"status"`
}

type purchasevehiclestruct struct {
	VehicleId      []string `json:"vehicleId"`
	DriverName     []string `json:"driverName"`
	RegistrationNo []string `json:"registrationNo"`
}

type purchasewarehusetruct struct {
	WarehouseID    []string `json:"warehouse_id"`
	BillingAddress []string `json:"billing_address"`
}

type ratestruct struct {
	HSN               string `json:"hsn"`
	UOM               string `json:"uom"`
	Rate              string `json:"rate"`
	GST               string `json:"gst"`
	SupplierNarration string `json:"supplier_narration"`
	Status            string `json:"status"`
}

type Search_PurchaseOrderDetails struct {
	CustomerName  string `form:"CustomerName"`
	FromOrderDate string `form:"po_FromDate"`
	ToOrderDate   string `form:"po_ToDate"`
}

type supplierlocationstruct struct {
	Address string `json:"address_line"`
}

type batchUniquenessStatus struct {
	IsUnique string `json:"is_unique"`
}

func (c *PurchaseController) CreatePO() {
	flash := beego.ReadFromRequest(&c.Controller)
	if ok := flash.Data["error"]; ok != "" {
		c.Data["FlashErrors"] = ok
	}
	if ok := flash.Data["notice"]; ok != "" {
		// Display error messages
		c.Data["FlashNotices"] = ok
	}
	var user_id string
	var _orgId string
	var username string
	var localetimezone string

	sessionVar := c.StartSession()
	if sessionVar != nil {
		user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
		localetimezone = fmt.Sprintf("%s", sessionVar.Get("LocaleTimeZone"))
	} else {
		user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
		localetimezone = fmt.Sprintf("%s", c.Ctx.Input.GetData("LocaleTimeZone"))
	}
	c.Data["username"] = username
	formStruct1 := models.Createpo{
		Orgid:    _orgId,
		Userid:   user_id,
		Username: username,
	}
	retrieved := make(map[string]orm.Params)
	retrieved = models.GetSupplierNames(formStruct1)
	c.Data["SupRetrieved"] = retrieved

	retrieved01 := make(map[string]orm.Params)
	retrieved01 = models.GetAllItemsForDropdowns(_orgId)
	c.Data["ItemsRetrieved"] = retrieved01

	retrievedlocation := make(map[string]orm.Params)
	retrievedlocation = models.GetLocation(formStruct1)
	c.Data["LocationRetrieved"] = retrievedlocation

	retrievedtransporters := make(map[string]orm.Params)
	retrievedtransporters = models.GetTransportersforDropdown(_orgId)
	c.Data["TransporterRetrieved"] = retrievedtransporters

	delayedPOs := c.GetString("delayedPOs")
	poId := c.GetString("poId")

	newGenCode, newCode := c.GetGeneratedCode(_orgId, 5, localetimezone)
	c.Data["genCode"] = newGenCode

	if poId != "" {
		retrieved := make(map[string]orm.Params)
		retrieved = models.GetPOdetailsByPOpkId(poId, _orgId)
		c.Data["Retrieved"] = retrieved
	} else if delayedPOs == "1" {
		retrieved2 := make(map[string]orm.Params)
		retrieved2 = models.GetdelayedPOdetails(formStruct1)
		c.Data["Retrieved"] = retrieved2
	} else {
		retrieved1 := make(map[string]orm.Params)
		retrieved1 = models.GetPOdetails(formStruct1)
		c.Data["Retrieved"] = retrieved1
	}

	c.TplName = "purchase/create-po.html"
	c.Data["Purchases"] = 1
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Create PO"
	c.Data["TaxValue"] = c.GetString("tax")
	if c.GetString("tax") != "" {
		c.Data["ErrorsPresent"] = true
	}

	if c.Ctx.Input.Method() == "POST" {

		if c.GetString("PurchaseOrderSearchmethod") == "PurchaseOrder_Search" {
			var buttonSearch string
			c.Ctx.Input.Bind(&buttonSearch, "PurchaseOrder-search")
			if buttonSearch == "SearchPurchaseOrder" {
				flash := beego.NewFlash()
				items := Search_PurchaseOrderDetails{}
				if err := c.ParseForm(&items); err != nil {
					flash.Error("Cannot parse form")
					//flash.Store(&c.Controller)
					return
				}
				retrieved := make(map[string]orm.Params)
				retrieved = models.SearchPOByOrderDetails(items.CustomerName, items.FromOrderDate, items.ToOrderDate, _orgId)
				c.Data["SearchItems"] = items
				c.Data["Retrieved"] = retrieved
				c.TplName = "purchase/create-po.html"
				c.Data["PageTitle"] = "Create PO"
				c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
				return
			}
			c.Ctx.Input.Bind(&buttonSearch, "Reset-PurchaseOrderSearch")
			if buttonSearch == "ResetPurchaseOrderSearch" {
				c.Redirect("/purchase/create-po", 303)
				return
			}
		}

		flash := beego.NewFlash()
		items := createPoValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			c.Data["ErrorsPresent"] = true
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

		formStruct := models.Createpo{
			Orgid:                _orgId,
			Userid:               user_id,
			POSONo:               newCode,
			PODate:               items.PODate,
			Supplier:             items.Supplier,
			PoDeliveryDate:       items.PoDeliveryDate,
			Currency:             items.Currency,
			DLoc:                 items.DLoc,
			Transporter:          items.Transporter,
			Vehicle:              items.Vehicle,
			TransporterWarehouse: items.TransporterWarehouse,
			Notes:                items.Notes,
			DiscountSubTotal:     items.DiscountSubTotal,
			TaxRegime:            items.TaxRegime,

			PaymentTerms: items.PaymentTerms,

			BillingAddress: items.BillingAddress,

			TotalTaxableValue:     items.TotalTaxableValue,
			TotalTaxCess:          items.TotalTaxCess,
			TotalAfterTaxes:       items.TotalAfterTaxes,
			TotalFreightPackaging: items.TotalFreightPackaging,
			TotalInvoiceValue:     items.TotalInvoiceValue,
			NewGenCode:            newGenCode,
		}

		c.Data["ErrorsPresent"] = true

		if items.SubmitButtonVal == "draft" {
			formStructgetpoitems := models.Createpo{
				Orgid:    _orgId,
				Userid:   user_id,
				Username: username,
				Supplier: items.Supplier,
			}
			retrieved01 := make(map[string]orm.Params)
			retrieved01 = models.GetPItems(formStructgetpoitems)
			c.Data["ItemsRetrieved"] = retrieved01
			valid := validation.Validation{}
			if b, _ := valid.Valid(&items); !b {
				c.Data["Errors"] = valid.ErrorsMap
				c.Data["ErrorsPresent"] = true
				return
			}
			var status string = "0"
			poitems := make([]map[string]string, 0, 3)
			//c.Ctx.Input.Bind(&poitems, "group-a")
			if items.TaxRegime == "1" {
				c.Ctx.Input.Bind(&poitems, "intra-state")
			} else if items.TaxRegime == "2" {
				c.Ctx.Input.Bind(&poitems, "inter-state")
			} else if items.TaxRegime == "3" {
				c.Ctx.Input.Bind(&poitems, "export-state")
			}

			insertStatus := models.CreatePO(formStruct, poitems, status)
			if insertStatus == 0 {
				flash.Error("An error occured while saving. Please try again.")
				flash.Store(&c.Controller)
				c.Data["ErrorsPresent"] = true
				return
			}

			flash.Notice("PO saved to draft.")
			flash.Store(&c.Controller)
			c.Redirect("/purchase/create-po", 303)
		}
		if items.SubmitButtonVal == "approval" {
			formStructgetpoitems := models.Createpo{
				Orgid:    _orgId,
				Userid:   user_id,
				Username: username,
				Supplier: items.Supplier,
			}
			retrieved01 := make(map[string]orm.Params)
			retrieved01 = models.GetPItems(formStructgetpoitems)
			c.Data["ItemsRetrieved"] = retrieved01
			valid := validation.Validation{}
			if b, _ := valid.Valid(&items); !b {
				c.Data["Errors"] = valid.ErrorsMap
				c.Data["ErrorsPresent"] = true
				return
			}
			var status string = "1"
			poitems := make([]map[string]string, 0, 3)
			//c.Ctx.Input.Bind(&poitems, "group-a")
			if items.TaxRegime == "1" {
				c.Ctx.Input.Bind(&poitems, "intra-state")
			} else if items.TaxRegime == "2" {
				c.Ctx.Input.Bind(&poitems, "inter-state")
			} else if items.TaxRegime == "3" {
				c.Ctx.Input.Bind(&poitems, "export-state")
			}
			// checkStatus := models.CheckPo(formStruct)
			// if checkStatus == 0 {
			insertStatus := models.CreatePO(formStruct, poitems, status)
			if insertStatus == 0 {
				flash.Error("An error occured while saving. Please try again.")
				flash.Store(&c.Controller)
				c.Data["ErrorsPresent"] = true
				return
			}

			flash.Notice("PO sent for approval.")
			flash.Store(&c.Controller)
			c.Redirect("/purchase/create-po", 303)
		}
		if items.SubmitButtonVal == "approved" {
			formStructgetpoitems := models.Createpo{
				Orgid:    _orgId,
				Userid:   user_id,
				Username: username,
				Supplier: items.Supplier,
			}
			retrieved01 := make(map[string]orm.Params)
			retrieved01 = models.GetPItems(formStructgetpoitems)
			c.Data["ItemsRetrieved"] = retrieved01
			valid := validation.Validation{}
			if b, _ := valid.Valid(&items); !b {
				c.Data["Errors"] = valid.ErrorsMap
				c.Data["ErrorsPresent"] = true
				return
			}
			var status string = "2"
			poitems := make([]map[string]string, 0, 3)
			//c.Ctx.Input.Bind(&poitems, "group-a")
			if items.TaxRegime == "1" {
				c.Ctx.Input.Bind(&poitems, "intra-state")
			} else if items.TaxRegime == "2" {
				c.Ctx.Input.Bind(&poitems, "inter-state")
			} else if items.TaxRegime == "3" {
				c.Ctx.Input.Bind(&poitems, "export-state")
			}

			insertStatus := models.CreatePO(formStruct, poitems, status)
			if insertStatus == 0 {
				flash.Error("An error occured while saving. Please try again.")
				flash.Store(&c.Controller)
				c.Data["ErrorsPresent"] = true
				//return
				c.Redirect("/purchase/create-po", 303)
			}

			flash.Notice("PO approved.")
			flash.Store(&c.Controller)
			c.Redirect("/purchase/create-po", 303)
		}

	}
}

func (c *PurchaseController) CreateGRN() {
	var user_id string
	var _orgId string
	var username string
	var localetimezone string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
		localetimezone = fmt.Sprintf("%s", sessionVar.Get("LocaleTimeZone"))
	} else {
		user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
		localetimezone = fmt.Sprintf("%s", c.Ctx.Input.GetData("LocaleTimeZone"))
	}
	c.Data["username"] = username
	formStruct1 := models.GRNData{
		Orgid:    _orgId,
		Userid:   user_id,
		Username: username,
	}

	newGenCode, newCode := c.GetGeneratedCode(_orgId, 11, localetimezone)
	c.Data["genCode"] = newGenCode
	retrievedtransporters := make(map[string]orm.Params)
	retrievedtransporters = models.GetTransportersforDropdown(_orgId)
	c.Data["TransporterRetrieved"] = retrievedtransporters
	retrievedpo := make(map[string]orm.Params)
	retrievedpo = models.GetPurchaseOrderDetailsByPONo(formStruct1)
	c.Data["PoRetrieved"] = retrievedpo
	retrieved := make(map[string]orm.Params)
	retrieved = models.GetAllGrnDetails(formStruct1)
	c.Data["Retrieved"] = retrieved
	retrieved8 := make(map[string]orm.Params)
	retrieved8 = models.GetSupplierNamess(formStruct1)
	c.Data["SupRetrieveds"] = retrieved8
	c.TplName = "purchase/grn.html"
	c.Data["Purchases"] = 1
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "GRN (List / View / Manage)"

	if c.Ctx.Input.Method() == "POST" {

		flash := beego.NewFlash()
		items := grnValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		//if items.ViewButton == "items" {
		formStruct2 := models.GRNData{
			Orgid: _orgId,
			PoNo:  items.PoNo,
		}
		retrieved3 := make(map[string]orm.Params)
		retrieved3 = models.GetItems(formStruct2)
		c.Data["Retrieved3"] = retrieved3
		c.Data["NoOfItems"] = len(retrieved3)
		retrieved13 := make(map[string]orm.Params)
		retrieved13 = models.GetGRNItems(formStruct2)
		c.Data["Retrieved13"] = retrieved13
		c.Data["ErrorsPresent"] = true
		//return
		//}

		if items.ViewButton == "sub" {
			valid := validation.Validation{}
			if b, _ := valid.Valid(&items); !b {
				//fmt.Println(err)
				//fmt.Println(valid.ErrorsMap)
				c.Data["Errors"] = valid.ErrorsMap
				c.Data["ErrorsPresent"] = true
				return
			}

			//Check if batches unique
			var arrayToStoreBatches []string
			grnitems := make([]map[string]string, 0, 3)
			c.Ctx.Input.Bind(&grnitems, "grnitems")
			for k, _ := range grnitems {
				//Get batches data
				batches := make([]map[string]string, 0, 3)
				c.Ctx.Input.Bind(&batches, "batch["+strconv.Itoa(k)+"]")
				for _, d := range batches {
					if d["batchno"] != "" {
						//Insert in Array to compare
						arrayToStoreBatches = append(arrayToStoreBatches, d["batchno"])
					}
				}
			}
			arrayLength := len(arrayToStoreBatches)
			if arrayLength > 0 {
				for i := 0; i < arrayLength; i++ {
					for j := i + 1; j < arrayLength; j++ {
						if arrayToStoreBatches[i] == arrayToStoreBatches[j] {
							//Enter unique batches
							return
						}
					}
				}
			}
			//Now Check in db if batches are unique
			for i := 0; i < arrayLength; i++ {
				uniqueStatus := models.CheckIfBatchNoUnique(_orgId, arrayToStoreBatches[i])
				if uniqueStatus != 1 {
					//Batch No already exists in db
					return
				}
			}

			//GET SUPPLIER IDS BASED ON SUPPLIER NAME
			supplierId := models.GetSuppplierIDByCompanyName(items.Suppliers, _orgId)

			layout := "2006-01-02 15:04:05"
			invoiceDate := time.Now()
			invDate, errIn := time.Parse(layout, items.SupplierInvoiceDt)
			if errIn == nil {
				invoiceDate = invDate
			}

			dcDate := time.Now()
			dcdt, errDc := time.Parse(layout, items.SupplierDcDt)
			if errDc == nil {
				dcDate = dcdt
			}

			//INSERT INTO PO SO SUPPLIER INVOICE
			supplierInvoiceDetails := models.SupplierInvoice{
				Orgid:             _orgId,
				SupplierFkId:      supplierId,
				SupplierInvoice:   items.SupplierInvoice,
				SupplierInvoiceDt: invoiceDate,
				SupplierDcNo:      items.SupplierDcNo,
				SupplierDcDt:      dcDate,
				DateAdded:         time.Now(),
			}

			invoiceInsertId := models.InsertPOSOSupplierInvoice(supplierInvoiceDetails)
			//fmt.Println("******invoiceInsertId ", invoiceInsertId)

			if invoiceInsertId != 0 {
				for k, it := range grnitems {

					//Get batches data
					batches := make([]map[string]string, 0, 3)
					c.Ctx.Input.Bind(&batches, "batch["+strconv.Itoa(k)+"]")
					//fmt.Println("******invoiceInsertId ", it["qtyDelivered"])
					//if it["qtyDelivered"] != "" {
					// 	var d string
					// 	d = "0.0"
					// 	it["qtyDelivered"] = d
					// }
					if it["qtyRejected"] == "" {
						var s string
						s = "0.0"
						it["qtyRejected"] = s
					}

					for _, d := range batches {
						if d["qty"] != "" {

							//Check if batch_no(d["batchno"]) is unique
							//uniqueStatus := CheckIfBatchNoUnique(_orgId, )
							formStruct := models.GRNData{
								Orgid:               _orgId,
								Userid:              user_id,
								PoNo:                items.PoNo,
								Itemss:              it["itemname"],
								Suppliers:           items.Suppliers,
								DeliveryDate:        items.DeliveryDate,
								AddNote:             items.AddNote,
								AQuantity:           d["qty"],
								RQuantity:           it["qtyRejected"],
								DeliveryLocation:    items.DeliveryLocation,
								TotalCost:           items.TotalCost,
								DeliveredDate:       items.DeliveredDate,
								BatchNo:             d["batchno"],
								NewGenCode:          newGenCode,
								GRNNo:               newCode,
								SupplierInvoiceFkId: invoiceInsertId,
								LrNo:                items.LrNo,
								LrDt:                items.LrDt,
								Transporter:         items.Transporter,
								Vehicle:             items.Vehicle,
							}
							//insertStatus := models.CreateGRN1(formStruct)
							insertStatus := models.CreateGRN(formStruct)
							if insertStatus == 0 {
								c.Data["ErrorsPresent"] = true
								return
							} else {

							}
						}
					}
					//}
				}
			}
			// Check PO is Complete
			completeStatus := models.CheckIfPOComplete(_orgId, items.PoId)
			if completeStatus == 1 {
				_ = models.ChangePurchaseOrderStatus(user_id, _orgId, items.PoId, "5")
			}
			c.Redirect("/purchase/grn", 303)
		}
	}
}

// func (c *PurchaseController) ServiceContract() {
// 	var user_id string
// 	var _orgId string
// 	var username string
// 	var insertStatus int
// 	var usedTableId string
// 	sessionVar := c.StartSession()
// 	if sessionVar != nil {
// 		user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
// 		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
// 		username = fmt.Sprintf("%s", sessionVar.Get("username"))
// 	} else {
// 		user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
// 		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
// 		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
// 	}
// 	c.Data["username"] = username
// 	formStruct1 := models.Createso{
// 		Orgid:    _orgId,
// 		Userid:   user_id,
// 		Username: username,
// 	}
// 	retrieved := make(map[string]orm.Params)
// 	retrieved = models.GetRItems(formStruct1)
// 	c.Data["Retrieved"] = retrieved
// 	retrievedContractor := make(map[string]orm.Params)
// 	retrievedContractor = models.GetContractorNames(formStruct1)
// 	c.Data["ContRetrieved"] = retrievedContractor

// 	retrievedlocation := make(map[string]orm.Params)
// 	retrievedlocation = models.GetDispatchLocation(formStruct1)
// 	c.Data["LocationRetrieved"] = retrievedlocation

// 	c.TplName = "purchase/service-contract.html"
// 	c.Data["Purchases"] = 1
// 	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
// 	c.Data["PageTitle"] = "Service Contract (Challan)"

// 	if c.Ctx.Input.Method() == "POST" {
// 		flash := beego.NewFlash()
// 		items := serviceContractValidate{}
// 		if err := c.ParseForm(&items); err != nil {
// 			flash.Error("Cannot parse form")
// 			flash.Store(&c.Controller)
// 			return
// 		}
// 		c.Data["FormItems"] = items
// 		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
// 		valid := validation.Validation{}
// 		if b, _ := valid.Valid(&items); !b {
// 			c.Data["Errors"] = valid.ErrorsMap
// 			c.Data["ErrorsPresent"] = true
// 			return
// 		}
// 		formStruct := models.Createso{
// 			Orgid:      _orgId,
// 			Userid:     user_id,
// 			Username:   username,
// 			Contractor: items.Contractor,
// 			ChallanNo:  items.ChallanNo,
// 			DateOrder:  items.DateOrder,
// 			//Items:               	  items.Items,
// 			RItem:                 items.RItem,
// 			RQuantity:             items.RQuantity,
// 			SDiscountSubTotal:     items.SDiscountSubTotal,
// 			Purpose:               items.Purpose,
// 			EstimatedDeliveryDate: items.EstimatedDeliveryDate,
// 			DispatchFrom:          items.DispatchFrom,
// 			DispatchDate:          items.DispatchDate,
// 			DispatchFleet:         items.DispatchFleet,
// 			DispatchCost:          items.DispatchCost,
// 			SPaymentTerms:         items.SPaymentTerms,
// 			SOtherChargesTitle:    items.SOtherChargesTitle,
// 			SOtherChargesValue:    items.SOtherChargesValue,
// 			SExciseDuty:           items.SExciseDuty,
// 			SOctroiCharges:        items.SOctroiCharges,
// 			STotal:                items.STotal,
// 			SFinalDiscount:        items.SFinalDiscount,
// 			SFinalTotal:           items.SFinalTotal,
// 			SBillingAddress:       items.SBillingAddress,
// 			SCurrency:             items.SCurrency,
// 			ItemName:              items.ItemName,
// 			Quantity:              items.Quantity,
// 			ItemName1:             items.ItemName1,
// 			Quantity1:             items.Quantity1,
// 			ItemName2:             items.ItemName2,
// 			Quantity2:             items.Quantity2,
// 			ItemName3:             items.ItemName3,
// 			Quantity3:             items.Quantity3,
// 			ItemName4:             items.ItemName4,
// 			Quantity4:             items.Quantity4,
// 			ItemName5:             items.ItemName5,
// 			Quantity5:             items.Quantity5,
// 			ItemName6:             items.ItemName6,
// 			Quantity6:             items.Quantity6,
// 		}
// 		var flag int
// 		if formStruct.ItemName != "" && formStruct.Quantity != "" {
// 			retrievedItem0 := models.CheckInventoryLevelItemCode(formStruct.ItemName, formStruct.Quantity, formStruct.Orgid)
// 			if retrievedItem0 == 0 {
// 				c.Data["quantity_error"] = 1
// 				flag = 1
// 				flash.Warning("Quantity exceeds available quantity")
// 				flash.Store(&c.Controller)

// 			}
// 		}
// 		if formStruct.ItemName1 != "" && formStruct.Quantity1 != "" {
// 			retrievedItem1 := models.CheckInventoryLevelItemCode(formStruct.ItemName1, formStruct.Quantity1, formStruct.Orgid)
// 			if retrievedItem1 == 0 {
// 				c.Data["quantity_error1"] = 1
// 				flag = 1
// 				flash.Warning("Quantity exceeds available quantity")
// 				flash.Store(&c.Controller)

// 			}
// 		}
// 		if formStruct.ItemName2 != "" && formStruct.Quantity2 != "" {
// 			retrievedItem2 := models.CheckInventoryLevelItemCode(formStruct.ItemName2, formStruct.Quantity2, formStruct.Orgid)
// 			if retrievedItem2 == 0 {
// 				c.Data["quantity_error2"] = 1
// 				flag = 1
// 				flash.Warning("Quantity exceeds available quantity")
// 				flash.Store(&c.Controller)

// 			}
// 		}
// 		if formStruct.ItemName3 != "" && formStruct.Quantity3 != "" {
// 			retrievedItem3 := models.CheckInventoryLevelItemCode(formStruct.ItemName3, formStruct.Quantity3, formStruct.Orgid)
// 			if retrievedItem3 == 0 {
// 				c.Data["quantity_error3"] = 1
// 				flag = 1
// 				flash.Warning("Quantity exceeds available quantity")
// 				flash.Store(&c.Controller)

// 			}
// 		}
// 		if formStruct.ItemName4 != "" && formStruct.Quantity4 != "" {
// 			retrievedItem4 := models.CheckInventoryLevelItemCode(formStruct.ItemName4, formStruct.Quantity4, formStruct.Orgid)
// 			if retrievedItem4 == 0 {
// 				c.Data["quantity_error4"] = 1
// 				flag = 1
// 				flash.Warning("Quantity exceeds available quantity")
// 				flash.Store(&c.Controller)

// 			}
// 		}
// 		if formStruct.ItemName5 != "" && formStruct.Quantity5 != "" {
// 			retrievedItem5 := models.CheckInventoryLevelItemCode(formStruct.ItemName5, formStruct.Quantity5, formStruct.Orgid)
// 			if retrievedItem5 == 0 {
// 				c.Data["quantity_error5"] = 1
// 				flag = 1
// 				flash.Warning("Quantity exceeds available quantity")
// 				flash.Store(&c.Controller)

// 			}
// 		}
// 		if formStruct.ItemName6 != "" && formStruct.Quantity6 != "" {
// 			retrievedItem6 := models.CheckInventoryLevelItemCode(formStruct.ItemName6, formStruct.Quantity6, formStruct.Orgid)
// 			if retrievedItem6 == 0 {
// 				c.Data["quantity_error6"] = 1
// 				flag = 1
// 				flash.Warning("Quantity exceeds available quantity")
// 				flash.Store(&c.Controller)

// 			}
// 		}
// 		if flag == 1 {
// 			c.Data["ErrorsPresent"] = true
// 			return
// 		}
// 		if items.SubmitButtonVal == "draft" {
// 			var status string = "0"
// 			insertStatus, usedTableId = models.CreateSONew(formStruct, status)
// 			if insertStatus == 0 {
// 				flash.Error("An error occured while saving. Please try again.")
// 				flash.Store(&c.Controller)
// 				return
// 			}
// 		}
// 		if items.SubmitButtonVal == "approval" {
// 			var status string = "1"
// 			insertStatus, usedTableId = models.CreateSONew(formStruct, status)
// 			if insertStatus == 0 {
// 				flash.Error("An error occured while saving. Please try again.")
// 				flash.Store(&c.Controller)
// 				return
// 			}
// 		}
// 		if items.SubmitButtonVal == "approved" {
// 			var status string = "2"
// 			insertStatus, usedTableId = models.CreateSONew(formStruct, status)
// 			if insertStatus == 0 {
// 				flash.Error("An error occured while saving. Please try again.")
// 				flash.Store(&c.Controller)
// 				return
// 			}
// 		}
// 		itemsmap := make(map[string]float64)
// 		if formStruct.ItemName != "" && formStruct.Quantity != "" && items.SubmitButtonVal == "approved" {
// 			q, _ := strconv.ParseFloat(formStruct.Quantity, 64)
// 			itemsmap[formStruct.ItemName] = q
// 		}
// 		if formStruct.ItemName1 != "" && formStruct.Quantity1 != "" && items.SubmitButtonVal == "approved" {
// 			q1, _ := strconv.ParseFloat(formStruct.Quantity1, 32)
// 			itemsmap[formStruct.ItemName1] = q1
// 		}
// 		if formStruct.ItemName2 != "" && formStruct.Quantity2 != "" && items.SubmitButtonVal == "approved" {
// 			q2, _ := strconv.ParseFloat(formStruct.Quantity2, 32)
// 			itemsmap[formStruct.ItemName2] = q2
// 		}
// 		if formStruct.ItemName3 != "" && formStruct.Quantity3 != "" && items.SubmitButtonVal == "approved" {
// 			q3, _ := strconv.ParseFloat(formStruct.Quantity3, 32)
// 			itemsmap[formStruct.ItemName3] = q3
// 		}
// 		if formStruct.ItemName4 != "" && formStruct.Quantity4 != "" && items.SubmitButtonVal == "approved" {
// 			q4, _ := strconv.ParseFloat(formStruct.Quantity4, 32)
// 			itemsmap[formStruct.ItemName4] = q4
// 		}
// 		if formStruct.ItemName5 != "" && formStruct.Quantity5 != "" && items.SubmitButtonVal == "approved" {
// 			q5, _ := strconv.ParseFloat(formStruct.Quantity5, 32)
// 			itemsmap[formStruct.ItemName5] = q5
// 		}
// 		if formStruct.ItemName6 != "" && formStruct.Quantity6 != "" && items.SubmitButtonVal == "approved" {
// 			q6, _ := strconv.ParseFloat(formStruct.Quantity6, 32)
// 			itemsmap[formStruct.ItemName6] = q6
// 		}
// 		if items.SubmitButtonVal == "approved" {
// 			inventoryupdate := models.ConsumeInventoryByItemsMap(itemsmap, formStruct.Orgid, "1", usedTableId, formStruct.ChallanNo)
// 			if inventoryupdate == 0 {
// 				flash.Error("An error occured while saving. Please try again.")
// 				flash.Store(&c.Controller)
// 				return
// 			}
// 			if formStruct.ItemName != "" {
// 				models.ItemTotalInventoryUpdateByPkid(formStruct.ItemName, formStruct.Orgid)
// 			}
// 			if formStruct.ItemName1 != "" {
// 				models.ItemTotalInventoryUpdateByPkid(formStruct.ItemName1, formStruct.Orgid)
// 			}
// 			if formStruct.ItemName2 != "" {
// 				models.ItemTotalInventoryUpdateByPkid(formStruct.ItemName2, formStruct.Orgid)
// 			}
// 			if formStruct.ItemName3 != "" {
// 				models.ItemTotalInventoryUpdateByPkid(formStruct.ItemName3, formStruct.Orgid)
// 			}
// 			if formStruct.ItemName4 != "" {
// 				models.ItemTotalInventoryUpdateByPkid(formStruct.ItemName4, formStruct.Orgid)
// 			}
// 			if formStruct.ItemName5 != "" {
// 				models.ItemTotalInventoryUpdateByPkid(formStruct.ItemName5, formStruct.Orgid)
// 			}
// 			if formStruct.ItemName6 != "" {
// 				models.ItemTotalInventoryUpdateByPkid(formStruct.ItemName6, formStruct.Orgid)
// 			}
// 		}

// 		c.Redirect("/purchase/service-contract", 303)
// 	}
// }

func (c *PurchaseController) POItems() {

	var _orgId string
	//var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		//user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		//username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		//user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		//username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}

	c.TplName = "purchase/create-po.html"
	c.Data["Purchases"] = 1
	c.Data["PageTitle"] = "Create PO"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

	if c.Ctx.Input.Method() == "GET" {

		purchase_id := c.GetString("id")
		//retrieved :=make(map[string]orm.Params)

		tax_regime, items, quantity, received_qty, rejected_qty, pending_qty, packet_size, status, rate, discount_rate, delivery_date, uom, discount_value, hsn_sac, taxable_amount, gst_rate, igst_rate, igst_amount, cgst_rate, cgst_amount, sgst_rate, sgst_amount, cess_rate, cess_amount, total_tax_cess, total_after_tax, freight, insurance, packaging_forwarding, others, total_other_charges := models.GetPOItems(purchase_id, _orgId)
		outputObj := &mystruct2{
			Items:       items,
			Quantity:    quantity,
			ReceivedQty: received_qty,
			RejectedQty: rejected_qty,
			PendingQty:  pending_qty,
			Rate:        rate,
			//TotalCost:   total_cost,
			GstRate:      gst_rate,
			PacketSize:   packet_size,
			TaxRegime:    tax_regime,
			DiscountRate: discount_rate,
			DeliveryDate: delivery_date,
			UOM:          uom,

			DiscountValue:       discount_value,
			HsnSac:              hsn_sac,
			TaxableAmount:       taxable_amount,
			IgstRate:            igst_rate,
			IgstAmount:          igst_amount,
			CgstRate:            cgst_rate,
			CgstAmount:          cgst_amount,
			SgstRate:            sgst_rate,
			SgstAmount:          sgst_amount,
			CessRate:            cess_rate,
			CessAmount:          cess_amount,
			TotalTaxCess:        total_tax_cess,
			TotalAfterTax:       total_after_tax,
			Freight:             freight,
			Insurance:           insurance,
			PackagingForwarding: packaging_forwarding,
			Others:              others,
			TotalOtherCharges:   total_other_charges,

			Status: status,
		}
		//fmt.Println(outputObj)
		b, _ := json.Marshal(outputObj)
		//fmt.Println("***string b", string(b))
		//c.Data["ProductRetrieved"] = retrieved
		//fmt.Println("****Result:",result)
		c.Ctx.Output.Body([]byte(b))
	}
}

func (c *PurchaseController) POItems2() {

	var _orgId string
	//var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		//user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		//username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		//user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		//username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}

	c.TplName = "purchase/create-po.html"
	c.Data["Purchases"] = 1
	c.Data["PageTitle"] = "Create PO"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

	if c.Ctx.Input.Method() == "GET" {

		product_id := c.GetString("id")
		//retrieved :=make(map[string]orm.Params)

		items, quantity, received_qty, rejected_qty, pending_qty, status, rate, total_cost := models.GetPOItems2(product_id, _orgId)
		outputObj := &mystruct2{
			Items:       items,
			Quantity:    quantity,
			ReceivedQty: received_qty,
			RejectedQty: rejected_qty,
			PendingQty:  pending_qty,
			Rate:        rate,
			TotalCost:   total_cost,
			Status:      status,
		}
		//fmt.Println(outputObj)
		b, _ := json.Marshal(outputObj)
		//fmt.Println("***string b", string(b))
		//c.Data["ProductRetrieved"] = retrieved
		//fmt.Println("****Result:",result)
		c.Ctx.Output.Body([]byte(b))
	}
}

func (c *PurchaseController) SOItemsUsed() {

	var _orgId string
	//var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		//user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		//username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		//user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		//username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}

	c.TplName = "purchase/create-po.html"
	c.Data["Purchases"] = 1
	c.Data["PageTitle"] = "Create PO"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

	if c.Ctx.Input.Method() == "GET" {

		product_id := c.GetString("id")
		//retrieved :=make(map[string]orm.Params)

		items, quantity, status := models.GetSOItemsUsed(product_id, _orgId)
		outputObj := &mystruct2{
			Items:    items,
			Quantity: quantity,
			Status:   status,
		}
		//fmt.Println(outputObj)
		b, _ := json.Marshal(outputObj)
		//fmt.Println("***string b", string(b))
		//c.Data["ProductRetrieved"] = retrieved
		//fmt.Println("****Result:",result)
		c.Ctx.Output.Body([]byte(b))
	}
}

func (c *PurchaseController) POStatus() {
	var user_id string
	var _orgId string
	//var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		//username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		//username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}

	c.TplName = "purchase/create-po.html"
	c.Data["Purchases"] = 1
	c.Data["PageTitle"] = "Create PO"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

	if c.Ctx.Input.Method() == "GET" {

		product_id := c.GetString("product_id")
		product_status := c.GetString("product_status")

		formStruct := models.Createpo{
			Orgid:  _orgId,
			POSONo: product_id,
			Userid: user_id,
		}
		insertStatus := models.ChangeStatus(formStruct, product_status)
		if insertStatus == 0 {
			//flash.Error("An error occured while saving. Please try again.")
			//flash.Store(&c.Controller)
			return
		}
		b, _ := json.Marshal(insertStatus)
		c.Ctx.Output.Body([]byte(b))
	}
}

func (c *PurchaseController) PORates() {

	var _orgId string
	//var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		//user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		//username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		//user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		//username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}

	c.TplName = "purchase/create-po.html"
	c.Data["Purchases"] = 1
	c.Data["PageTitle"] = "Create PO"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

	if c.Ctx.Input.Method() == "GET" {

		supplier_name := c.GetString("supplier")
		//fmt.Println("*****Supplier is ", supplier_name)
		item_name := c.GetString("item")
		//fmt.Println("*****Item is ", item_name)
		//retrieved :=make(map[string]orm.Params)

		new_hsn, new_uom, new_rate, new_gst, new_supplier_narration, status := models.GetPORates(item_name, supplier_name, _orgId)
		//fmt.Println("***Rate=", new_rate)
		//fmt.Println("***Status=", status)
		outputObj := &ratestruct{
			HSN:               new_hsn,
			UOM:               new_uom,
			Rate:              new_rate,
			GST:               new_gst,
			SupplierNarration: new_supplier_narration,
			Status:            status,
		}
		// fmt.Println(outputObj)
		b, _ := json.Marshal(outputObj)
		//fmt.Println("***string b", string(b))
		// //c.Data["ProductRetrieved"] = retrieved
		// //fmt.Println("****Result:",result)
		c.Ctx.Output.Body([]byte(b))
	}
}

func (c *PurchaseController) PurchaseGRN() {

	var _orgId string
	// //var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		// 	//user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		// 	//username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		// 	//user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		// 	//username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}

	c.TplName = "purchase/create-po.html"
	c.Data["Purchases"] = 1
	c.Data["PageTitle"] = "Create PO"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

	if c.Ctx.Input.Method() == "GET" {

		item_unique_code := c.GetString("item_unique_code")
		//fmt.Println("********Item:", item_unique_code)
		delivered_qty := c.GetString("delivered_qty")
		rejected_qty := c.GetString("rejected_qty")
		po_so_no := c.GetString("po_so_no")
		supplier := c.GetString("supplier")
		po_so_dt := c.GetString("po_so_dt")
		total_cost := c.GetString("total_cost")
		delivery_loc := c.GetString("delivery_loc")
		delivered_dt := c.GetString("delivered_dt")
		supplier_invoice := c.GetString("supplier_invoice")
		supplier_invoice_dt := c.GetString("supplier_invoice_dt")
		supplier_dc_no := c.GetString("supplier_dc_no")
		supplier_dc_dt := c.GetString("supplier_dc_dt")
		note := c.GetString("note")

		formStruct := models.GRNData{
			Orgid:             _orgId,
			PoNo:              po_so_no,
			Itemss:            item_unique_code,
			Suppliers:         supplier,
			DeliveryDate:      po_so_dt,
			SupplierInvoice:   supplier_invoice,
			SupplierInvoiceDt: supplier_invoice_dt,
			SupplierDcNo:      supplier_dc_no,
			SupplierDcDt:      supplier_dc_dt,
			AddNote:           note,
			AQuantity:         delivered_qty,
			RQuantity:         rejected_qty,
			DeliveryLocation:  delivery_loc,
			TotalCost:         total_cost,
			DeliveredDate:     delivered_dt,
		}
		insertStatus := models.CreateGRN(formStruct)
		if insertStatus == 0 {
			//flash.Error("An error occured while saving. Please try again.")
			//flash.Store(&c.Controller)
			return
		}

		// supplier_name := c.GetString("supplier")
		// fmt.Println("*****Supplier is ",supplier_name)
		// item_name := c.GetString("item")
		// fmt.Println("*****Item is ",item_name)
		// //retrieved :=make(map[string]orm.Params)

		// new_rate, status  := models.GetPORates(item_name, supplier_name, _orgId)
		// fmt.Println("***Rate=", new_rate)
		// fmt.Println("***Status=", status)
		// outputObj := &ratestruct{
		// 	Rate:     new_rate,
		// 	Status: status,
		// }
		// // fmt.Println(outputObj)
		b, _ := json.Marshal(insertStatus)
		//  fmt.Println("***string b",string(b))
		// // //c.Data["ProductRetrieved"] = retrieved
		// // //fmt.Println("****Result:",result)
		c.Ctx.Output.Body([]byte(b))
	}
}

func (c *PurchaseController) EditPO() {
	var user_id string
	var _orgId string
	var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}
	c.Data["username"] = username
	c.TplName = "purchase/edit-po.html"
	c.Data["Purchases"] = 1
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Edit PO Items"

	// userDetailsStruct := models.Createpo{
	// 	Orgid:    _orgId,
	// 	Userid:   user_id,
	// 	Username: username,
	// }
	//fmt.Println("inside edit PO")
	if c.Ctx.Input.Method() == "GET" {
		purchase_code := c.GetString("purchaseId")
		//item_id := c.GetString("id")
		//fmt.Println("Inside Get view grn controller with ", item_id)
		flash := beego.NewFlash()
		items := createPoValidateEdit{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			return
		}

		retrievedtransporters := make(map[string]orm.Params)
		retrievedtransporters = models.GetTransportersforDropdown(_orgId)
		c.Data["TransporterRetrieved"] = retrievedtransporters

		c.Data["FormItems"] = models.GetPODetailsByCode(purchase_code, _orgId)
		retrieved := make(map[string]orm.Params)
		retrieved = models.GetEditPOItems(purchase_code, _orgId)
		c.Data["RepeaterLength"] = len(retrieved)
		c.Data["POItemsRetrieved"] = retrieved
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		c.Data["ErrorsPresent"] = true
		return
	}
	if c.Ctx.Input.Method() == "POST" {
		//fmt.Println("inside post")
		flash := beego.NewFlash()
		items := createPoValidateEdit{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			c.Data["ErrorsPresent"] = true
			//fmt.Println(valid)
			return
		}

		formStruct := models.Createpo{
			Orgid:             _orgId,
			Userid:            user_id,
			POSONo:            items.POSONo,
			PODate:            items.PODate,
			Supplier:          items.Supplier,
			PoDeliveryDate:    items.PoDeliveryDate,
			Currency:          items.Currency,
			DLoc:              items.DLoc,
			DiscountSubTotal:  items.DiscountSubTotal,
			TotalPrice:        items.TotalPrice,
			FreightType:       items.FreightType,
			FreightCost:       items.FreightCost,
			PaymentTerms:      items.PaymentTerms,
			OtherChargesTitle: items.OtherChargesTitle,
			OtherChargesValue: items.OtherChargesValue,
			ExciseDuty:        items.ExciseDuty,
			OctroiCharges:     items.OctroiCharges,
			Total:             items.Total,
			FinalDiscount:     items.FinalDiscount,
			FinalTotal:        items.FinalTotal,
			BillingAddress:    items.BillingAddress,
			PurchaseId:        items.PurchaseId,

			TotalTaxableValue:     items.TotalTaxableValue,
			TotalTaxCess:          items.TotalTaxCess,
			TotalAfterTaxes:       items.TotalAfterTaxes,
			TotalFreightPackaging: items.TotalFreightPackaging,
			TotalInvoiceValue:     items.TotalInvoiceValue,
			Transporter:           items.Transporter,
			Vehicle:               items.Vehicle,
			TransporterWarehouse:  items.TransporterWarehouse,
		}
		//fmt.Println("Status is",result["status"])
		poitems := make([]map[string]string, 0, 3)
		if items.TaxRegime == "1" {
			c.Ctx.Input.Bind(&poitems, "intra-state")
		} else if items.TaxRegime == "2" {
			c.Ctx.Input.Bind(&poitems, "inter-state")
		} else if items.TaxRegime == "3" {
			c.Ctx.Input.Bind(&poitems, "export-state")
		}
		//c.Ctx.Input.Bind(&poitems, "group-a")

		// for count, _ := range poitems {
		// 	fmt.Println("**Number of Items: ", count)
		// }

		insertStatus := models.UpdatePO(formStruct, poitems)
		if insertStatus == 0 {
			flash.Error("Form Not Inserted")
			flash.Store(&c.Controller)
			return
		}

		//fmt.Println(grnitems)
		// poitems := make([]map[string]string, 0, 3)
		// c.Ctx.Input.Bind(&poitems, "group-a")

		// for _, it := range poitems {

		// 	if it["item_name"] != "" {

		// 		formStruct1 := models.Createpo{
		// 			Orgid:          _orgId,
		// 			Userid:         user_id,
		// 			Username:       username,
		// 			POSONo:         items.POSONo,
		// 			PODate:         items.PODate,
		// 			Supplier:       items.Supplier,
		// 			PoDeliveryDate: items.PoDeliveryDate,
		// 			Currency:       items.Currency,
		// 			DLoc:           items.DLoc,
		// 			// OrderNo:               items.OrderNo,
		// 			// OrderDate:             items.OrderDate,
		// 			// CustomerName:          items.CustomerName,
		// 			// JobType:               items.JobType,
		// 			// DeliveryDate:          items.DeliveryDate,
		// 			ItemName:            it["item_name"],
		// 			HSN:                 it["hsn"],
		// 			UOM:                 it["uom"],
		// 			Quantity:            it["qty"],
		// 			UnitPrice:           it["cost"],
		// 			DiscountPercent:     it["discount_percent"],
		// 			DiscountValue:       it["discount_value"],
		// 			TaxableAmount:       it["taxable_amount"],
		// 			GSTPercent:          it["gst_percent"],
		// 			IGSTRate:            it["igst_rate"],
		// 			IGSTAmount:          it["igst_amount"],
		// 			CGSTRate:            it["cgst_rate"],
		// 			CGSTAmount:          it["cgst_amount"],
		// 			SGSTRate:            it["sgst_rate"],
		// 			SGSTAmount:          it["sgst_amount"],
		// 			CessRate:            it["cess_rate"],
		// 			CessAmount:          it["cess_amount"],
		// 			ItemTotalTaxCess:    it["total_tax_cess"],
		// 			ItemTotalAfterTax:   it["total_after_tax"],
		// 			Freight:             it["freight"],
		// 			Insurance:           it["insurance"],
		// 			PackagingForwarding: it["packaging_forwarding"],
		// 			Others:              it["others"],
		// 			TotalOtherCharges:   it["total_other_charges"],
		// 			PaymentTerms:        items.PaymentTerms,
		// 			BillingAddress:      items.BillingAddress,
		// 			//Remarks:               items.Remarks,
		// 			TotalTaxableValue:     items.TotalTaxableValue,
		// 			TotalTaxCess:          items.TotalTaxCess,
		// 			TotalAfterTaxes:       items.TotalAfterTaxes,
		// 			TotalFreightPackaging: items.TotalFreightPackaging,
		// 			TotalInvoiceValue:     items.TotalInvoiceValue,
		// 		}

		// 		insertStatus := models.UpdatePOEditItems(formStruct1)
		// 		if insertStatus == 0 {
		// 			flash.Error("An error occured while saving. Please try again.")
		// 			flash.Store(&c.Controller)
		// 			c.Data["ErrorsPresent"] = true
		// 			return
		// 		}
		// 	}
		// }

		c.Redirect("/purchase/create-po", 303)
	}
}

func (c *PurchaseController) EditDraftPO() {
	var user_id string
	var _orgId string
	var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}
	c.Data["username"] = username
	c.TplName = "purchase/edit-draft-po.html"
	c.Data["Purchases"] = 1
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Edit Draft Items"

	//fmt.Println(retrieved01)

	// userDetailsStruct := models.Createpo{
	// 	Orgid:    _orgId,
	// 	Userid:   user_id,
	// 	Username: username,
	// }
	//fmt.Println("inside edit PO")
	if c.Ctx.Input.Method() == "GET" {

		purchase_code := c.GetString("purchaseId")
		//item_id := c.GetString("id")
		//fmt.Println("Inside Get view grn controller with ", item_id)
		flash := beego.NewFlash()
		items := createPoValidateEdit{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			return
		}

		retrievedtransporters := make(map[string]orm.Params)
		retrievedtransporters = models.GetTransportersforDropdown(_orgId)
		c.Data["TransporterRetrieved"] = retrievedtransporters

		c.Data["FormItems"] = models.GetPODetailsByCode(purchase_code, _orgId)
		retrieved := make(map[string]orm.Params)
		retrieved = models.GetEditPOItems(purchase_code, _orgId)
		//fmt.Println("**length: ", len(retrieved))
		c.Data["RepeaterLength"] = len(retrieved)
		c.Data["POItemsRetrieved"] = retrieved

		c.Data["username"] = username
		// formStruct1 := models.Createpo{
		// 	Orgid:    _orgId,
		// 	Userid:   user_id,
		// 	Username: username,
		// }

		retrieved01 := make(map[string]orm.Params)
		retrieved01 = models.GetAllItemsForDropdowns(_orgId)
		c.Data["ItemsRetrieved"] = retrieved01

		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		c.Data["ErrorsPresent"] = true
		return
	}
	if c.Ctx.Input.Method() == "POST" {
		//fmt.Println("inside post")
		flash := beego.NewFlash()
		items := createPoValidateEdit{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			c.Data["ErrorsPresent"] = true
			fmt.Println(valid)
			return
		}

		formStruct := models.Createpo{
			Orgid:             _orgId,
			Userid:            user_id,
			TaxRegime:         items.TaxRegime,
			POSONo:            items.POSONo,
			PODate:            items.PODate,
			Supplier:          items.Supplier,
			PoDeliveryDate:    items.PoDeliveryDate,
			Currency:          items.Currency,
			DLoc:              items.DLoc,
			DiscountSubTotal:  items.DiscountSubTotal,
			TotalPrice:        items.TotalPrice,
			FreightType:       items.FreightType,
			FreightCost:       items.FreightCost,
			PaymentTerms:      items.PaymentTerms,
			OtherChargesTitle: items.OtherChargesTitle,
			OtherChargesValue: items.OtherChargesValue,
			ExciseDuty:        items.ExciseDuty,
			OctroiCharges:     items.OctroiCharges,
			Total:             items.Total,
			FinalDiscount:     items.FinalDiscount,
			FinalTotal:        items.FinalTotal,
			BillingAddress:    items.BillingAddress,
			PurchaseId:        items.PurchaseId,

			TotalTaxableValue:     items.TotalTaxableValue,
			TotalTaxCess:          items.TotalTaxCess,
			TotalAfterTaxes:       items.TotalAfterTaxes,
			TotalFreightPackaging: items.TotalFreightPackaging,
			TotalInvoiceValue:     items.TotalInvoiceValue,
			Transporter:           items.Transporter,
			Vehicle:               items.Vehicle,
		}
		//fmt.Println("Status is",result["status"])
		podraftitems := make([]map[string]string, 0, 3)
		if items.TaxRegime == "1" {
			c.Ctx.Input.Bind(&podraftitems, "intra-state")
		} else if items.TaxRegime == "2" {
			c.Ctx.Input.Bind(&podraftitems, "inter-state")
		} else if items.TaxRegime == "3" {
			c.Ctx.Input.Bind(&podraftitems, "export-state")
		}
		//c.Ctx.Input.Bind(&podraftitems, "group-a")

		// for count, _ := range poitems {
		// 	fmt.Println("**Number of Items: ", count)
		// }

		insertStatus := models.UpdateDraftPO(formStruct, podraftitems)
		if insertStatus == 0 {
			flash.Error("Form Not Inserted")
			flash.Store(&c.Controller)
			return
		}

		c.Redirect("/purchase/create-po", 303)
	}
}

func (c *PurchaseController) GetBillingAddress() {
	//var user_id string
	var _orgId string
	//var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		//user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		//username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		//user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		//username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}

	if c.Ctx.Input.Method() == "GET" {

		supplierid := c.GetString("supplierid")

		address_line := models.GetSupplierBillingAddress(supplierid, _orgId)

		outputObj := &supplierlocationstruct{
			Address: address_line,
		}
		//fmt.Println(outputObj)
		b, _ := json.Marshal(outputObj)
		//fmt.Println("***string b", string(b))
		// //c.Data["ProductRetrieved"] = retrieved
		// //fmt.Println("****Result:",result)
		c.Ctx.Output.Body([]byte(b))
	}

}

func (c *PurchaseController) ChangePurchaseStatus() {
	//fmt.Println("Inside sales controller")
	var user_id string
	var _orgId string
	//var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		//username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		//username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}

	//c.TplName = "purchase/create-po.html"
	//c.Data["Purchases"] = 1
	//c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	//c.Data["PageTitle"] = "Change PO Status"

	if c.Ctx.Input.Method() == "GET" {

		order_id := c.GetString("purchase_id")
		order_status := c.GetString("purchase_status")

		var statusChanged int
		//if insertStatus == 0 {
		//Update the appropriate status for the sales order
		statusChanged = models.ChangePurchaseOrderStatus(user_id, _orgId, order_id, order_status)
		// if statusChanged == 0 {
		// 	return
		// }
		//}

		b, _ := json.Marshal(statusChanged)
		c.Ctx.Output.Body([]byte(b))
	}
}

func (c *PurchaseController) GetPurchaseDetailsForDetailsPage() {
	//var user_id string
	var _orgId string
	var username string
	//var localetimezone string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		//user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
		//localetimezone = fmt.Sprintf("%s", sessionVar.Get("LocaleTimeZone"))
	} else {
		//user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
		//localetimezone = fmt.Sprintf("%s", c.Ctx.Input.GetData("LocaleTimeZone"))
	}
	c.Data["username"] = username
	// formStruct5 := models.Sales{
	// 	Orgid:    _orgId,
	// 	Userid:   user_id,
	// 	Username: username,
	// }

	purchaseId := c.GetString("purchaseId")

	retrieved := make(map[string]orm.Params)
	retrieved = models.GetPurchasedetailsByPkId(purchaseId, _orgId)
	c.Data["PORetrieved"] = retrieved

	c.TplName = "purchase/purchase-details.html"
	c.Data["PageTitle"] = "Purchase Details"
	//c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
}

func (c *PurchaseController) GetPurchaseVehicles() {

	var _orgId string
	//var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		//user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		//username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		//user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		//username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}

	if c.Ctx.Input.Method() == "GET" {

		//supplier_name := c.GetString("supplier")
		//fmt.Println("*****Supplier is ",supplier_name)
		transporter_id := c.GetString("transporter")
		//fmt.Println("*****Item is ", item_name)
		//retrieved :=make(map[string]orm.Params)

		vehicle_id, driver_name, registration_no := models.GetTransporterVehicles(transporter_id, _orgId)
		//fmt.Println("***Rate=", new_rate)
		//fmt.Println("***Status=", status)
		outputObj := &purchasevehiclestruct{
			VehicleId:      vehicle_id,
			DriverName:     driver_name,
			RegistrationNo: registration_no,
		}
		//fmt.Println(outputObj)
		b, _ := json.Marshal(outputObj)
		//fmt.Println("***string b", string(b))
		// //c.Data["ProductRetrieved"] = retrieved
		// //fmt.Println("****Result:",result)
		c.Ctx.Output.Body([]byte(b))
	}
}

func (c *PurchaseController) GetPurchaseWarehouses() {

	var _orgId string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		//user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		//username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		//user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		//username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}

	if c.Ctx.Input.Method() == "GET" {

		transporter_id := c.GetString("transporter")

		billing_address, warehouse_id := models.GetTransporterWarehouses(transporter_id, _orgId)

		outputObj := &purchasewarehusetruct{
			WarehouseID:    warehouse_id,
			BillingAddress: billing_address,
		}
		b, _ := json.Marshal(outputObj)
		c.Ctx.Output.Body([]byte(b))
	}
}

func (c *PurchaseController) CheckBatchUniqueness() {

	var _orgId string
	//var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		//user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		//username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		//user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		//username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}

	if c.Ctx.Input.Method() == "GET" {

		batchNO := c.GetString("batch_no")
		uniqueStatus := models.CheckIfBatchNoUnique(_orgId, batchNO)
		fmt.Println("****uniqueStatus ", uniqueStatus)

		outputObj := &batchUniquenessStatus{
			IsUnique: strconv.Itoa(uniqueStatus),
		}

		b, _ := json.Marshal(outputObj)
		c.Ctx.Output.Body([]byte(b))
	}
}
