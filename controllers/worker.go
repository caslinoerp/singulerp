package controllers

import (
	"fmt"

	"bitbucket.org/caslinoerp/singulerp/models"

	"github.com/astaxie/beego"
)

type WorkerController struct {
	beego.Controller
}

func (c *WorkerController) CalculateDelayedPOs() {
	//var user_id string
	var _orgId string
	//var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		//user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		//username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		//user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		//username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}
	//c.Data["username"] = username

	insertStatus := models.DelayedPO(_orgId)
	if insertStatus == 0 {
		//fmt.Println("Error in query")
	} else {
		//fmt.Println("Delayed status Update success")
	}
}
