package controllers

import (
	"encoding/json"
	"fmt"
	"html/template"
	"strconv"
	"strings"

	"bitbucket.org/caslinoerp/singulerp/models"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"github.com/astaxie/beego/validation"
)

type ProductionController struct {
	BaseController
}

type addProductValidate struct {
	ProductCode      string   `form:"product_code" valid:"Required"`
	ProductName      string   `form:"product_name" valid:"Required"`
	InvoiceNarration string   `form:"invoice_narration" valid:"Required"`
	ProductType      string   `form:"product_type" `
	Group            string   `form:"group"`
	SubGroup         string   `form:"sub_group"`
	Capacity         string   `form:"capacity" valid:"Required"`
	UOM              string   `form:"uom"`
	RatePerUOM       string   `form:"rate_per_uom" valid:"Required"`
	MinStock         string   `form:"min_stock" valid:"Required"`
	Transport        string   `form:"transport"`
	ProductProcess   string   `form:"product_process"`
	Status1          string   `form:"status1"`
	HiddenMethod     string   `form:"hidden_method"`
	ProductID        string   `form:"product_id"`
	ProductCheck     string   `form:"product_check"`
	FactoryLoc       []string `form:"factory_location[]" valid:"Required"`
}

type modifybomvalidate struct {
	ProductCode string `form:"product_code" `
	ProductName string `form:"product_name" `
	Quantity    string `form:"quantity"`
	UOM         string `form:"uom" `
	ItemName    string `form:"item_name"`
	Wastage     string `form:"wastage"`
	Showbtn     string `form:"show-button"`
	Savebtn     string `form:"save_button"`
}

type bomValidate struct {
	ProductCode  string `form:"product_code" valid:"Required"`
	ProductName  string `form:"product_name" valid:"Required"`
	ItemName     string `form:"group-a[0][item_name]"`
	Qty          string `form:"group-a[0][qty]"`
	Wastage      string `form:"group-a[0][wastage]"`
	ItemName1    string `form:"item1"`
	Qty1         string `form:"qty1"`
	Wastage1     string `form:"wastage1"`
	ItemName2    string `form:"item2"`
	Qty2         string `form:"qty2"`
	Wastage2     string `form:"wastage2"`
	ItemName3    string `form:"item3"`
	Qty3         string `form:"qty3"`
	Wastage3     string `form:"wastage3"`
	ItemName4    string `form:"item4"`
	Qty4         string `form:"qty4"`
	Wastage4     string `form:"wastage4"`
	ItemName5    string `form:"item5"`
	Qty5         string `form:"qty5"`
	Wastage5     string `form:"wastage5"`
	ItemName6    string `form:"item6"`
	Qty6         string `form:"qty6"`
	Wastage6     string `form:"wastage6"`
	ItemName7    string `form:"item7"`
	Qty7         string `form:"qty7"`
	Wastage7     string `form:"wastage7"`
	HiddenMethod string `form:"hidden_method"`
	Nc1          string `form:"nc1"`
	NC2          string `form:"nc2"`
	NC3          string `form:"nc3"`
	NC4          string `form:"nc4"`
	NC5          string `form:"nc5"`
	NC6          string `form:"nc6"`
	NC7          string `form:"nc7"`
	NC           string `form:"nc"`
}

type jobcardValidate struct {
	JobcardNo      string `form:"jobcard_no" valid:"Required"`
	Date           string `form:"date" valid:"Required"`
	OrderNo        string `form:"order_no" valid:"Required"`
	OrderDate      string `form:"o_date" valid:"Required"`
	InvoiceNo      string `form:"invoice_no" valid:"Required"`
	InvoiceDate    string `form:"i_date" valid:"Required"`
	CustName       string `form:"name" valid:"Required"`
	SrNo           string `form:"sr_no" valid:"Required"`
	QCDate         string `form:"qc_date" valid:"Required"`
	SpecialTest    string `form:"special_test" valid:"Required"`
	MachineId      string `form:"machine_id" valid:"Required"`
	FiringSystem   string `form:"firing_system" valid:"Required"`
	MaxTemp        string `form:"max_temp" valid:"Required"`
	RemTemp        string `form:"removal_temp" valid:"Required"`
	PreCheck       string `form:"check" valid:"Required"`
	Operator       string `form:"operator" valid:"Required"`
	InspectedBy    string `form:"inspected_by" valid:"Required"`
	InspectionDate string `form:"i_date" valid:"Required"`
	Status         string `form:"status"`
	Notes          string `form:"notes" valid:"Required"`
}

type productionOrderValidate struct {
	BatchNo   string `form:"batch_no" valid:"Required"`
	OrderDate string `form:"order_date" valid:"Required"`
	Product   string `form:"product" valid:"Required"`
	Quantity  string `form:"quantity" valid:"Required" `
	//Check      string `form:"quantity_check"`
	SDate       string `form:"start_date" valid:"Required"`
	EDate       string `form:"end_date" valid:"Required"`
	Machine     string `form:"machine" valid:"Required"`
	FactoryLoc  string `form:"factory_location" valid:"Required"`
	St          string `form:"st"`
	PButton     string `form:"pr-button"`
	ShowBom     string `form:"showBom"`
	EditBom     string `form:"editBom"`
	AssignBatch string `form:"assignBatch"`
}

type updateProductionOrderValidate struct {
	ProductionId    string `form:"productionId" valid:"Required"`
	BatchNo         string `form:"batch_no" valid:"Required"`
	OrderDate       string `form:"order_date" valid:"Required"`
	Product         string `form:"product" valid:"Required"`
	Quantity        string `form:"quantity" valid:"Required" `
	StartDate       string `form:"start_date" valid:"Required"`
	EndDate         string `form:"end_date" valid:"Required"`
	Machine         string `form:"machine" valid:"Required"`
	FactoryLocation string `form:"factory_location" valid:"Required"`
}

type addMachineValidate struct {
	PrevId      string `form:"machine_id" `
	MachineCode string `form:"machine_code" valid:"Required"`
	MachineName string `form:"machine_name" valid:"Required"`
	GeoLocation string `form:"geo_location" valid:"Required"`
	AddedOn     string `form:"added_on" valid:"Required" `
	Status      string `form:"status" valid:"Required"`
}

type productionQueueValidate struct {
	Productq string `form:"product_name" `
	Status   string `form:"status" `
	Type     string `form:"type" `
}

type mystruct struct {
	Items    []string `json:"items"`
	Quantity []string `json:"quantity"`
	Wastage  []string `json:"wastage"`
	Consume  []string `json:"consume"`
	// Sent             []string `json:"sent"`
	// AlreadyInvited	 []string `json:"alreadyInvited"`
	// AlreadyMember	 []string `json:"alreadyMember"`
	Status string `json:"status"`
}

type uomstruct struct {
	UOM    string `json:"uom"`
	Status string `json:"status"`
}

type qtystruct struct {
	Qty    string `json:"qty"`
	Status string `json:"status"`
}

type productseditstruct struct {
	UOM      []string `json:"uom"`
	Group    []string `json:"group"`
	Subgroup []string `json:"subgroup"`
}

type machinesstruct struct {
	MachineId   []string `json:"machine_id"`
	MachineCode []string `json:"machine_code"`
	MachineName []string `json:"machine_name"`
	Status      string   `json:"status"`
}

type bomstruct struct {
	ItemNames         []string `json:"item_names"`
	ItemCodes         []string `json:"item_codes"`
	QtyUsed           []string `json:"qty_used"`
	QtyWasted         []string `json:"qty_wasted"`
	QtyInStock        []string `json:"qty_in_stock"`
	UOMs              []string `json:"uom"`
	AvaiabilityStatus []string `json:"avaiabilityStatus"`
	Status            string   `json:"status"`
	OnlyCode          string   `json:"onlyCode"`
}

//Need to check if this is used!!!!!!
func (c *ProductionController) CreateNewProduct() {
	var user_id string
	var _orgId string
	var username string

	sessionVar := c.StartSession()
	if sessionVar != nil {
		user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))

	} else {
		user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))

	}
	c.Data["OrgID"] = _orgId

	retrieved := make(map[string]string)
	retrieved = models.GetUserRolesByMap(username, _orgId)

	if _, ok := retrieved["26"]; ok {
		c.Data["username"] = username
		c.Data["_orgId"] = _orgId
		c.Data["user_id"] = user_id

		formStruct1 := models.Product{
			Orgid:    _orgId,
			Userid:   user_id,
			Username: username,
		}
		retrievedproduct := make(map[string]orm.Params)
		retrievedproduct = models.GetProduct(formStruct1)
		c.Data["Retrieved"] = retrievedproduct

		retrievedGrp := make(map[string]orm.Params)
		retrievedGrp = models.GetGroupProducts(formStruct1)
		c.Data["RetrievedGroup"] = retrievedGrp
		retrievedSubGrp := make(map[string]orm.Params)
		retrievedSubGrp = models.GetSubGroupProducts(formStruct1)
		c.Data["RetrievedSubGroup"] = retrievedSubGrp
		formStructfac := models.StockItems{
			Orgid:    _orgId,
			Userid:   user_id,
			Username: username,
		}
		retrievedlocs := make(map[string]orm.Params)
		retrievedlocs = models.GetFactory(formStructfac)
		c.Data["RetrievedLocs"] = retrievedlocs

		/*var retrieved1 string
		retrieved1=models.GetProductCode(formStruct1)
		c.Data["CodeRetrieved"] = retrieved1*/

		c.TplName = "production/add-new-product.html"
		c.Data["Production"] = 1
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		c.Data["PageTitle"] = "Add New Product"
		c.Data["view"] = "Product"

		if _, ok := retrieved["25"]; ok {
			if c.Ctx.Input.Method() == "POST" {
				flash := beego.NewFlash()
				if c.GetString("hidden_method") == "product" {
					items := addProductValidate{}
					if err := c.ParseForm(&items); err != nil {
						flash.Error("Cannot parse form")
						flash.Store(&c.Controller)
						return
					}
					c.Data["FormItems"] = items
					c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
					//fmt.Println("****POST Value: ",c.GetString("hidden_method"))
					valid := validation.Validation{}
					if b, _ := valid.Valid(&items); !b {
						c.Data["Errors"] = valid.ErrorsMap
						c.Data["ErrorsPresent"] = true
						return
					}

					formStruct := models.Product{
						Orgid:            _orgId,
						Userid:           user_id,
						Username:         username,
						ProductName:      strings.Title(items.ProductName),
						InvoiceNarration: strings.Title(items.InvoiceNarration),
						ProductCode:      strings.ToUpper(items.ProductCode),
						Group:            strings.ToUpper(items.Group),
						SubGroup:         strings.ToUpper(items.SubGroup),
						ProductType:      items.ProductType,
						Capacity:         items.Capacity,
						UOM:              strings.Title(items.UOM),
						RatePerUOM:       items.RatePerUOM,
						MinStock:         items.MinStock,
						Transport:        items.Transport,
						ProductProcess:   items.ProductProcess,
						Status1:          items.Status1,
						FactoryLoc:       items.FactoryLoc,
					}

					uniqueStatus := models.UniqueCheck(formStruct)
					if uniqueStatus == 0 {
						flash.Error("Product Code not unique")
						flash.Store(&c.Controller)
						c.Data["ErrorsPresent"] = true
						return
					}

					insertStatus := models.CreateNewProduct(formStruct)
					if insertStatus == 0 {
						flash.Error("Form Not Inserted")
						flash.Store(&c.Controller)
						c.Data["ErrorsPresent"] = true
						return
					}
					c.Data["completeProduct"] = true
				}
				formStruct5 := models.Items{
					Orgid:    _orgId,
					Userid:   user_id,
					Username: username,
				}
				retrievedb := make(map[string]orm.Params)
				retrievedb = models.GetBOItems(formStruct5)
				c.Data["Retrievedb"] = retrievedb
				c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
				c.Data["PageTitle"] = "Bill of Material"
				c.Data["view"] = "BOM"
				items := bomValidate{}
				if err := c.ParseForm(&items); err != nil {
					//fmt.Println("Cannot parse form")
					flash.Error("Cannot parse form")
					flash.Store(&c.Controller)
					return
				}
				c.Data["FormItems"] = items
				c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
				valid := validation.Validation{}
				if b, _ := valid.Valid(&items); !b {
					//fmt.Println("Validation error")
					c.Data["Errors"] = valid.ErrorsMap
					c.Data["ErrorsPresent"] = true
					return
				}

				bomformStruct := models.Items{
					Orgid:          _orgId,
					Userid:         user_id,
					Username:       username,
					BomProductName: items.ProductName,
					BomProductCode: items.ProductCode,
					ItemName1:      items.ItemName1,
					Qty1:           items.Qty1,
					Wastage1:       items.Wastage1,
					ItemName2:      items.ItemName2,
					Qty2:           items.Qty2,
					Wastage2:       items.Wastage2,
					ItemName3:      items.ItemName3,
					Qty3:           items.Qty3,
					Wastage3:       items.Wastage3,
					ItemName4:      items.ItemName4,
					Qty4:           items.Qty4,
					Wastage4:       items.Wastage4,
					ItemName5:      items.ItemName5,
					Qty5:           items.Qty5,
					Wastage5:       items.Wastage5,
					ItemName6:      items.ItemName6,
					Qty6:           items.Qty6,
					Wastage6:       items.Wastage6,
					ItemName7:      items.ItemName7,
					Qty7:           items.Qty7,
					Wastage7:       items.Wastage7,
					ItemName:       items.ItemName,
					Qty:            items.Qty,
					Wastage:        items.Wastage,
					NC:             items.NC,
					Nc1:            items.Nc1,
					NC2:            items.NC2,
					NC3:            items.NC3,
					NC4:            items.NC4,
					NC5:            items.NC5,
					NC6:            items.NC6,
					NC7:            items.NC7,
				}

				if c.GetString("hidden_method") == "bom" {

					bominsertStatus := models.BillofMaterial(bomformStruct)
					if bominsertStatus == 0 {
						//fmt.Println("Error in query")
						return
					}
					c.Redirect("/production/add-new-product", 303)
				}
			}
		} else {
			c.Data["NoProductAdd"] = true
		}
	} else {
		c.Redirect("/accessdenied", 303)
	}

}

func (c *ProductionController) ModifyBOM() {
	var user_id string
	var _orgId string
	var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}

	retrieved := make(map[string]string)
	retrieved = models.GetUserRolesByMap(username, _orgId)

	if _, ok := retrieved["26"]; ok {
		c.Data["username"] = username
		c.Data["_orgId"] = _orgId
		c.Data["user_id"] = user_id

		retrievedproducts := make(map[string]orm.Params)
		retrievedproducts = models.GetAllProductsForDropdowns(_orgId)
		c.Data["ProductsRetrieved"] = retrievedproducts

		c.TplName = "production/view-modify-bom.html"
		c.Data["Stock"] = 1
		c.Data["PageTitle"] = "View/Modify BOM"
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		if c.Ctx.Input.Method() == "GET" {

			item_id := c.GetString("itemid")
			bomCount, retrieved_search := models.GetBomItemsByProductID(item_id, _orgId)
			c.Data["ProductId"] = item_id
			if bomCount == 0 {
				c.Data["BomCount"] = 0
			}
			c.Data["ProductsAdd"] = true
			c.Data["Retrieved"] = retrieved_search
			return
		}

		if c.Ctx.Input.Method() == "POST" {
			flash := beego.NewFlash()
			items := modifybomvalidate{}
			if err := c.ParseForm(&items); err != nil {
				flash.Error("Cannot parse form")
				flash.Store(&c.Controller)
				return
			}
			c.Data["FormItems"] = items
			c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
			valid := validation.Validation{}
			if b, _ := valid.Valid(&items); !b {
				c.Data["Errors"] = valid.ErrorsMap
				c.Data["ErrorsPresent"] = true
				return
			}

			bomCount, retrieved_search := models.GetBomItemsByProductID(items.ProductName, _orgId)
			c.Data["Retrieved"] = retrieved_search
			c.Data["ProductId"] = items.ProductName
			if bomCount == 0 {
				c.Data["BomCount"] = 0
			}

		}

		if _, ok := retrieved["25"]; ok {
			c.Data["ProductsAdd"] = true
		} else {
			c.Data["NoProductsAdd"] = true
		}

	} else {
		c.Redirect("/accessdenied", 303)
	}

}

func (c *ProductionController) DeleteBomItems() {
	//var user_id string
	var _orgId string
	//var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		//user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		//username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		//user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		//username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}

	c.TplName = "production/view-modify-bom.html"
	c.Data["PageTitle"] = "View/Modify BOM"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

	if c.Ctx.Input.Method() == "GET" {

		item_id := c.GetString("id")
		result := models.DeleteBomItems(item_id, _orgId)
		c.Ctx.Output.Body([]byte(result))
	}
}

func (c *ProductionController) RestoreBomItems() {
	//var user_id string
	var _orgId string
	//var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		//user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		//username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		//user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		//username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}

	c.TplName = "production/view-modify-bom.html"
	c.Data["PageTitle"] = "View/Modify BOM"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

	if c.Ctx.Input.Method() == "GET" {

		item_id := c.GetString("id")
		result := models.RestoreBomItems(item_id, _orgId)
		c.Ctx.Output.Body([]byte(result))
	}
}

func (c *ProductionController) ChangeBomItems() {
	//var user_id string
	var _orgId string
	//var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		//user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		//username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		//user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		//username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}

	c.TplName = "production/view-modify-bom.html"
	c.Data["PageTitle"] = "View/Modify BOM"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

	if c.Ctx.Input.Method() == "GET" {

		item_id := c.GetString("id")
		new_qty := c.GetString("new_qty")
		new_wastage := c.GetString("new_wastage")
		result := models.ChangeBomItems(item_id, new_qty, new_wastage, _orgId)
		c.Ctx.Output.Body([]byte(result))
	}
}

func (c *ProductionController) ProductItems() {
	var _orgId string
	//var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		//user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		//username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		//user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		//username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}

	c.TplName = "production/add-new-product.html"
	c.Data["PageTitle"] = "Add New Product"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

	if c.Ctx.Input.Method() == "GET" {

		product_id := c.GetString("id")
		//retrieved :=make(map[string]orm.Params)
		items, quantity, wastage, consume, status := models.GetProductItems(product_id, _orgId)
		outputObj := &mystruct{
			Items:    items,
			Quantity: quantity,
			Wastage:  wastage,
			Consume:  consume,
			Status:   status,
		}
		//fmt.Println(outputObj)
		b, _ := json.Marshal(outputObj)
		//fmt.Println("***string b", string(b))
		c.Ctx.Output.Body([]byte(b))
	}
}

func (c *ProductionController) Jobcard() {
	var user_id string
	var _orgId string
	var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}
	c.Data["username"] = username
	formStruct1 := models.JobcardM{
		Orgid:    _orgId,
		Userid:   user_id,
		Username: username,
	}
	retrieved := make(map[string]orm.Params)
	retrieved = models.GetJobcard(formStruct1)
	c.Data["Retrieved"] = retrieved
	c.TplName = "production/jobcard.html"
	c.Data["Production"] = 1
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Jobcard"
	//generic code to handle post request and validation
	if c.Ctx.Input.Method() == "POST" {
		flash := beego.NewFlash()
		items := jobcardValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			c.Data["ErrorsPresent"] = true
			return
		}

		formStruct := models.JobcardM{
			Orgid:                       _orgId,
			Userid:                      user_id,
			Username:                    username,
			JobcardNo:                   items.JobcardNo,
			JobcardDate:                 items.Date,
			Purchase_service_order_fkid: items.OrderNo,
			OrderDate:                   items.OrderDate,
			InvoiceNo:                   items.InvoiceNo,
			InvoiceDate:                 items.InvoiceDate,
			Customer_name:               items.CustName,
			Sr_no:                       items.SrNo,
			QCDate:                      items.QCDate,
			Status:                      items.Status,
			SpecialTest:                 items.SpecialTest,
			MachineId:                   items.MachineId,
			FiringSystem:                items.FiringSystem,
			MaxTemp:                     items.MaxTemp,
			RemTemp:                     items.RemTemp,
			PreCheck:                    items.PreCheck,
			Operator:                    items.Operator,
			InspectedBy:                 items.InspectedBy,
			InspectionDate:              items.InspectionDate,
			Notes:                       items.Notes,
		}
		//insertStatus := models.RegistrationInsert(formData)
		insertStatus := models.Jobcard(formStruct)
		if insertStatus == 0 {
			flash.Error("An error occured while saving. Please try again.")
			flash.Store(&c.Controller)
			return
		}
		c.Redirect("/production/jobcard", 303)
	}
}

type Search_ProductionOrderDetails struct {
	ProductName     string `form:"ProductName"`
	FactoryLocation string `form:"FactoryLocation"`
	Machine         string `form:"Machine"`
	FromOrderDate   string `form:"po_FromDate"`
	ToOrderDate     string `form:"po_ToDate"`
	POSearchOption  string `form:"SearchPO"`
}

func (c *ProductionController) CreateNewProductionOrder() {

	var user_id string
	var _orgId string
	var username string
	var localetimezone string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
		localetimezone = fmt.Sprintf("%s", sessionVar.Get("LocaleTimeZone"))
	} else {
		user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
		localetimezone = fmt.Sprintf("%s", c.Ctx.Input.GetData("LocaleTimeZone"))
	}

	newGenCode, newCode := c.GetGeneratedCode(_orgId, 9, localetimezone)
	c.Data["genCode"] = newGenCode

	delayedprod := c.GetString("delayedprod")

	retrieved := make(map[string]string)
	retrieved = models.GetUserRolesByMap(username, _orgId)

	if _, ok := retrieved["24"]; ok {
		c.Data["username"] = username
		c.Data["_orgId"] = _orgId
		c.Data["user_id"] = user_id

		formStruct1 := models.ProductionOrder{
			Orgid:    _orgId,
			Userid:   user_id,
			Username: username,
		}

		retrievedg := make(map[string]orm.Params)
		retrievedg = models.GetAllProducts(formStruct1)
		c.Data["Retrievedg"] = retrievedg

		retrievedorders := make(map[string]orm.Params)

		if delayedprod == "1" {
			retrievedorders = models.GetDelayedProductionOrdersdetails(formStruct1)
			c.Data["RetrievedProductionOrder"] = retrievedorders
		} else {
			retrievedorders = models.GetProductionOrdersdetails(formStruct1)
			c.Data["RetrievedProductionOrder"] = retrievedorders
		}

		formStructfac := models.StockItems{
			Orgid:    _orgId,
			Userid:   user_id,
			Username: username,
		}
		retrievedlocs := make(map[string]orm.Params)
		retrievedlocs = models.GetFactory(formStructfac)
		c.Data["RetrievedLocs"] = retrievedlocs

		factoryid := c.GetString("factoryId")
		if factoryid != "" {
			c.Data["ErrorsPresent"] = true
			factoryStruct := productionOrderValidate{
				FactoryLoc: factoryid,
			}
			c.Data["FormItems"] = factoryStruct
		}
		flash := beego.ReadFromRequest(&c.Controller)
		if ok := flash.Data["error"]; ok != "" {
			c.Data["FlashErrors"] = ok
		}
		if ok := flash.Data["notice"]; ok != "" {
			// Display error messages
			c.Data["FlashNotices"] = ok
		}
		c.Data["SearchByPODetailsChecked"] = "checked"
		c.TplName = "production/production-orders.html"
		c.Data["Production"] = 1
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		c.Data["PageTitle"] = "Production Orders"
		c.Data["view"] = "ProductionOrders"

		if _, ok := retrieved["23"]; ok {
			if c.Ctx.Input.Method() == "POST" {
				// for the search option on the page
				if c.GetString("SearchProductionOrder") == "ProductionOrder_Search" {
					var buttonSearch string
					c.Ctx.Input.Bind(&buttonSearch, "PO-search")
					if buttonSearch == "SearchPODetails" {
						flash := beego.NewFlash()
						items := Search_ProductionOrderDetails{}
						if err := c.ParseForm(&items); err != nil {
							flash.Error("Cannot parse form")
							return
						}
						retrievedorders := make(map[string]orm.Params)
						//fmt.Println(items.ProductName)
						if items.POSearchOption == "SearchBy_PODetails" {
							retrievedorders = models.SearchProductionOrdersdetails(items.ProductName, items.FactoryLocation, items.Machine, _orgId)
							c.Data["SearchByPODetailsChecked"] = "checked"
						}
						if items.POSearchOption == "SearchPO_BydateRange" {
							retrievedorders = models.SearchProductionOrdersByDateRange(items.FromOrderDate, items.ToOrderDate, _orgId)
							c.Data["SearchPOBydateRangeChecked"] = "checked"
						}
						c.Data["SearchItems"] = items
						c.Data["RetrievedProductionOrder"] = retrievedorders
						c.TplName = "production/production-orders.html"
						c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
						c.Data["PageTitle"] = "Production Orders"
						return
					}
					c.Ctx.Input.Bind(&buttonSearch, "Reset-POSearch")
					if buttonSearch == "ResetPOSearch" {
						c.Redirect("/production/production-orders", 303)
						return
					}
				}

				flash := beego.NewFlash()
				items := productionOrderValidate{}
				if err := c.ParseForm(&items); err != nil {
					flash.Error("Cannot parse form")
					flash.Store(&c.Controller)
					return
				}
				c.Data["FormItems"] = items
				//fmt.Println("***Items ", items)
				c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
				valid := validation.Validation{}
				if b, _ := valid.Valid(&items); !b {
					c.Data["Errors"] = valid.ErrorsMap
					c.Data["ErrorsPresent"] = true
					return
				}

				editFlag := "0"
				assignFlag := "0"
				showBom := "0"

				if items.ShowBom == "on" {
					showBom = "1"
				}
				if items.EditBom == "on" {
					editFlag = "1"
				}
				if items.AssignBatch == "on" {
					assignFlag = "1"
				}

				formStruct2 := models.ProductionOrder{
					Orgid:      _orgId,
					Product:    items.Product,
					FactoryLoc: items.FactoryLoc,
					St:         "1",
				}
				retrieved3 := make(map[string]orm.Params)
				retrieved3 = models.GetBOMItems(formStruct2)
				c.Data["RetrievedBOM"] = retrieved3
				c.Data["ErrorsPresent"] = true

				formStruct := models.ProductionOrder{
					Orgid:               _orgId,
					Userid:              user_id,
					BatchNo:             newGenCode,
					OrderDate:           items.OrderDate,
					Product:             items.Product,
					Quantity:            items.Quantity,
					SDate:               items.SDate,
					EDate:               items.EDate,
					Machine:             items.Machine,
					FactoryLoc:          items.FactoryLoc,
					UniqueCode:          newCode,
					ShowBom:             showBom,
					TemporaryBOMUsed:    editFlag,
					ManuallyAssignBatch: assignFlag,
				}

				var status string

				//retrievedBOMItemsByPkid := make(map[string]orm.Params)
				retrievedBOMItemsByPkid, returnCount := models.GetBOMItemsByProductId(formStruct.Product, formStruct.Orgid)

				c.Data["RetrievedBOM"] = retrievedBOMItemsByPkid

				proditems := make([]map[string]string, 0, 3)

				prodQuantity := formStruct.Quantity //NO OF PRODUCTS
				prodQty, _ := strconv.ParseFloat(prodQuantity, 64)

				//fmt.Println("Controller BOM retrieved status====", retrievedBOMItemsByPkid["status"])
				if returnCount == 0 {
					flash.Error("The product has no BOM and hence cannot be used in a production order.")
					flash.Store(&c.Controller)
					c.Redirect("/production/production-orders", 303)
					return
				}

				for k, _ := range retrievedBOMItemsByPkid {
					data := make(map[string]string)
					data["item_name"] = retrievedBOMItemsByPkid[k]["_id"].(string)

					qtyNeededToProduce := retrievedBOMItemsByPkid[k]["quantity"].(string)
					QtyForProduction, _ := strconv.ParseFloat(qtyNeededToProduce, 64)

					totalNeeded := prodQty * QtyForProduction
					data["qty"] = strconv.FormatFloat(totalNeeded, 'f', -1, 64)

					proditems = append(proditems, data)

				}

				isInventoryOk := models.CheckInventoryStatusForAllItems(proditems, formStruct.FactoryLoc, _orgId)

				// INSERT ON PRODUCTION ORDERS BUTTON CLICK
				var productionOrderFkid string
				var insertStatus int
				toBeBlocked := "0"
				if items.PButton == "send_for_approval" {

					if isInventoryOk == "0" {
						status = "99"
					} else {
						status = "0"
					}

					insertStatus, productionOrderFkid = models.CreateNewProductionOrder(formStruct, status)
					if insertStatus == 0 {
						flash.Error("An error occured while saving. Please try again.")
						flash.Store(&c.Controller)
						c.Data["ErrorsPresent"] = true
						return
					}

				}

				if items.PButton == "approved" {

					if isInventoryOk == "0" {
						status = "19"
					} else {
						status = "1"
					}

					insertStatus, productionOrderFkid = models.CreateNewProductionOrder(formStruct, status)
					if insertStatus == 0 {
						flash.Error("An error occured while saving. Please try again.")
						flash.Store(&c.Controller)
						c.Data["ErrorsPresent"] = true
						return
					}
					toBeBlocked = "1"
				}

				if items.PButton == "production" {

					if isInventoryOk == "0" {
						status = "29"
					} else {
						status = "2"
					}

					insertStatus, productionOrderFkid = models.CreateNewProductionOrder(formStruct, status)
					if insertStatus == 0 {
						flash.Error("An error occured while saving. Please try again.")
						flash.Store(&c.Controller)
						c.Data["ErrorsPresent"] = true
						return
					}
					toBeBlocked = "1"
				}

				//REDIRECT BASED ON FLAGS AND BLOCK IF NOT FROM SEND FOR APPROVAL
				if editFlag == "1" {
					c.Redirect("/production/production-orders-edit-bom?productionOrderFkid="+productionOrderFkid+"&product="+items.Product+"&batchNo="+newGenCode+"&assignFlag="+assignFlag+"&qty="+formStruct.Quantity+"&tobeblocked="+toBeBlocked, 303)
				} else { // IF BOM IS NOT MODIFIED THEN INSERT THE ORIGINAL BOM ITEMS FOR THE PRODUCT IN PRODUCTION ORDER ITEMS

					//GET THE PRODUCTION ORDER FKID
					productionOrderFkId := models.GetProductionOrderFkIDFromOrgidBatchNo(_orgId, items.BatchNo)

					//GET THE BOM ITEMS FOR THE PRODUCT FROM PRODUCT BOM TABLE
					//INSERT EACH ITEM IN THE BOM IN THE PRODUCTION ORDER ITEMS TABLE WITH BOM_STATUS AS 0
					//retrievedBOMItemsByPkid := make(map[string]orm.Params)
					retrievedBOMItemsByPkid, retrievedCount := models.GetBOMItemsByProductId(items.Product, _orgId)
					if retrievedCount == 0 {
						flash.Error("The product has no BOM and hence cannot be used in a production order.")
						flash.Store(&c.Controller)
						c.Redirect("/production/production-orders", 303)
						return

					}
					for k, _ := range retrievedBOMItemsByPkid {
						itemPkid, _ := strconv.Atoi(retrievedBOMItemsByPkid[k]["_id"].(string))
						qty64, _ := strconv.ParseFloat(retrievedBOMItemsByPkid[k]["quantity"].(string), 64)
						qty64 = qty64 * prodQty
						if retrievedBOMItemsByPkid[k]["wastage"] == nil {
							retrievedBOMItemsByPkid[k]["wastage"] = 0.0
						}
						wastage64, _ := retrievedBOMItemsByPkid[k]["wastage"].(float64)
						wastage64 = wastage64 * prodQty
						//wastage64, _ := strconv.ParseFloat(retrievedBOMItemsByPkid[k]["wastage"].(string), 64)
						formStruct := models.ProductionOrderItem{
							ProductionOrderFkID: productionOrderFkId,
							ItemOrgAssocFkID:    itemPkid,
							Quantity:            qty64,
							Wastage:             wastage64,
							UOM:                 retrievedBOMItemsByPkid[k]["unit_measure"].(string),
							Location:            items.FactoryLoc,
							OrgID:               _orgId,
							Status:              1,
							BomStatus:           0,
						}
						poItemsinsert := models.InsertProductionOrderItems(formStruct)
						if poItemsinsert == 0 {
							return
						}
					}

				}
				if assignFlag == "1" {
					c.Redirect("/production/production-orders-assign-batches?productionOrderFkid="+productionOrderFkid+"&product="+items.Product+"&batchNo="+newGenCode, 303)
				}

				//BLOCK ITEMS IF NONE OF THE CHECKBOXES ARE CHECKED
				if editFlag == "0" && assignFlag == "0" {

					if toBeBlocked == "1" {
						blockStatus := BlockItemsForProduction(productionOrderFkid, _orgId, prodQty)
						if blockStatus == 0 {
							c.Data["ErrorsPresent"] = true
							return
						}
					}
				}

				c.Redirect("/production/production-orders", 303)
			}

		} else {
			c.Data["NoOrdersAdd"] = true
		}

	} else {
		c.Redirect("/accessdenied", 303)
	}

}

//THIS IS CALLED FROM THE EDIT BOM PAGE FOR THE PRODUCTION ORDER
func (c *ProductionController) ProductionEditBom() {
	flash := beego.ReadFromRequest(&c.Controller)
	if ok := flash.Data["error"]; ok != "" {
		c.Data["FlasErrors"] = ok
	}
	if ok := flash.Data["notice"]; ok != "" {
		// Display error messages
		c.Data["FlashNotices"] = ok
	}
	var _orgId string

	sessionVar := c.StartSession()
	if sessionVar != nil {
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
	} else {
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
	}

	BatchNo := c.GetString("batchNo")
	ProductPkId := c.GetString("product")
	AssignFlag := c.GetString("assignFlag")
	productionOrderFkid := c.GetString("productionOrderFkid")

	pQty := c.GetString("qty")
	prodQty, _ := strconv.ParseFloat(pQty, 64)

	tobeblocked := c.GetString("tobeblocked")

	retrieveddropItems := make(map[string]orm.Params)
	retrieveddropItems = models.GetAllItemsForDropdowns(_orgId)
	c.Data["ItemsRetrieved"] = retrieveddropItems

	retrieved := make(map[string]orm.Params)
	retrieved = models.GetProductionOrderFormDetails(BatchNo, ProductPkId, _orgId)
	c.Data["RetrievedData"] = retrieved

	retrieveditems := make(map[string]orm.Params)
	retrieveditems = models.GetProductionOrderItemsFromProductBOM(productionOrderFkid, _orgId)
	c.Data["RetrievedItems"] = retrieveditems

	c.Data["RepeaterLength"] = len(retrieveditems)

	c.TplName = "production/production-orders-edit-bom.html"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Production Orders Edit Bom"

	productionOrderTotalQty, _ := strconv.ParseFloat(retrieved["items_0"]["qty"].(string), 64)

	productionPkid, _ := strconv.Atoi(retrieved["items_0"]["poPkid"].(string))
	if c.Ctx.Input.Method() == "POST" {
		poitems := make([]map[string]string, 0, 3)
		c.Ctx.Input.Bind(&poitems, "BomGrp")

		for _, it := range poitems {
			if it["item_id"] != "" {
				itemIdint, _ := strconv.Atoi(it["item_id"])
				qty64, _ := strconv.ParseFloat(it["qty"], 64)
				qty64 = qty64 * productionOrderTotalQty
				wastage64, _ := strconv.ParseFloat(it["wastage"], 64)
				wastage64 = wastage64 * productionOrderTotalQty
				formStruct := models.ProductionOrderItem{
					ProductionOrderFkID: productionPkid,
					ItemOrgAssocFkID:    itemIdint,
					Quantity:            qty64,
					Wastage:             wastage64,
					UOM:                 it["uom"],
					Location:            retrieved["items_0"]["geo_location"].(string),
					OrgID:               _orgId,
					Status:              1,
					BomStatus:           1,
				}

				poItemsinsert := models.InsertProductionOrderItems(formStruct)
				if poItemsinsert == 0 {
					return
				}
			}
		}

		if AssignFlag == "1" {
			c.Redirect("/production/production-orders-assign-batches?product="+ProductPkId+"&batchNo="+BatchNo, 303)
		} else { //BLOCK ITEMS IF ASSIGN BATCHES WONT BE CALLED. BLOCK BASED ON MODIFIED BOM

			if tobeblocked == "1" {

				blockStatus := BlockItemsForProduction(productionOrderFkid, _orgId, prodQty)
				if blockStatus == 0 {
					c.Data["ErrorsPresent"] = true
					return
				}
			}
		}
		c.Redirect("/production/production-orders", 303)
	}
}

//THIS IS CALLED FROM THE ASSIGN BATCHES PAGE FOR THE PRODUCTION ORDER
func (c *ProductionController) AssignBatchesToProductionOrder() {
	flash := beego.ReadFromRequest(&c.Controller)
	if ok := flash.Data["error"]; ok != "" {
		c.Data["FlasErrors"] = ok
	}
	if ok := flash.Data["notice"]; ok != "" {
		// Display error messages
		c.Data["FlashNotices"] = ok
	}
	var _orgId string

	sessionVar := c.StartSession()
	if sessionVar != nil {
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
	} else {
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
	}

	var ProductionOrderFkid string

	if c.Ctx.Input.Method() == "GET" {
		BatchNo := c.GetString("batchNo")
		ProductPkId := c.GetString("product")
		ProductionOrderFkid = c.GetString("productionOrderFkid")

		c.Data["Productionorderfkid"] = ProductionOrderFkid

		retrieved := make(map[string]orm.Params)
		retrieved = models.GetProductionOrderFormDetails(BatchNo, ProductPkId, _orgId)
		c.Data["RetrievedData"] = retrieved

		retrieveditems := make(map[string]orm.Params)
		retrieveditems = models.GetProductionEditProductionOrderItems(ProductionOrderFkid, _orgId)
		c.Data["RetrievedItems"] = retrieveditems

		c.Data["RepeaterLength"] = len(retrieveditems)

		c.TplName = "production/production-orders-assign-batches.html"
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		c.Data["PageTitle"] = "Production Orders Assign Batches"
	}
	if c.Ctx.Input.Method() == "POST" {
		proditems := make([]map[string]string, 0, 3)
		c.Ctx.Input.Bind(&proditems, "proditems")

		var location string
		c.Ctx.Input.Bind(&location, "location")

		var Productionorderfkid string
		c.Ctx.Input.Bind(&Productionorderfkid, "Productionorderfkid")
		productionOrderfkId, _ := strconv.ParseInt(Productionorderfkid, 10, 64)

		var itemcode string
		c.Ctx.Input.Bind(&itemcode, "item_code")
		itemId, _ := strconv.ParseInt(itemcode, 10, 64)

		for k := range proditems {
			//Get batches data
			batches := make([]map[string]string, 0, 3)
			c.Ctx.Input.Bind(&batches, "BomGrpitems_"+strconv.Itoa(k))
			for _, d := range batches {

				qty, _ := strconv.ParseFloat(d["qty"], 64)

				formStruct := models.ItemDetailsForOrder{
					OrgId:             _orgId,
					ItemId:            itemId,
					RequiredQuantity:  qty,
					Location:          location,
					BlockedBy:         1,
					ConsumedTableFkId: productionOrderfkId,
					BatchNo:           d["batch_id"],
				}

				insertStatus := models.BlockItemsBeforeConsumption(formStruct)
				if insertStatus == 0 {
					c.Data["ErrorsPresent"] = true
					return
				}
			}
		}

		c.Redirect("/production/production-orders", 303)
	}
}

func (c *ProductionController) ProductionQueue() {

	var user_id string
	var _orgId string
	var username string

	sessionVar := c.StartSession()
	if sessionVar != nil {
		user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}

	retrieved := make(map[string]string)
	retrieved = models.GetUserRolesByMap(username, _orgId)

	if _, ok := retrieved["26"]; ok {
		c.TplName = "production/production-queue.html"
		c.Data["Production"] = 1
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		c.Data["PageTitle"] = "Production Queue"

		formStruct := models.ProductQueue{
			Orgid:    _orgId,
			Userid:   user_id,
			Username: username,
		}
		retrieved := make(map[string]orm.Params)
		//retrieved = models.GetProductQueue(formStruct)
		retrieved = models.GetAllProductsForDropdowns(_orgId)
		c.Data["Retrieved"] = retrieved

		retrievedpq := make(map[string]orm.Params)
		retrievedpq = models.GetInitialPQDetails(formStruct)
		c.Data["RetrievedProductionQueue"] = retrievedpq

		c.Data["username"] = username
		if c.Ctx.Input.Method() == "POST" {
			flash := beego.NewFlash()
			items := productionQueueValidate{}
			if err := c.ParseForm(&items); err != nil {
				flash.Error("Cannot parse form")
				flash.Store(&c.Controller)
				return
			}

			c.Data["FormItems"] = items
			c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
			valid := validation.Validation{}
			if b, _ := valid.Valid(&items); !b {
				c.Data["Errors"] = valid.ErrorsMap
				return
			}

			var actual_status string
			if items.Status == "Pending" {
				actual_status = "0"
			} else if items.Status == "In Queue" {
				actual_status = "1"
			} else if items.Status == "Production" {
				actual_status = "2"
			} else if items.Status == "Completed" {
				actual_status = "3"
			} else if items.Status == "Rejected" {
				actual_status = "4"
			} else if items.Status == "On Hold" {
				actual_status = "5"
			}

			var actual_type string
			if items.Type == "Against SO" {
				actual_type = "0"
			} else if items.Type == "Production Order" {
				actual_type = "1"
			}

			formStruct1 := models.ProductQueue{
				Orgid:    _orgId,
				Userid:   user_id,
				Username: username,
				ProductQ: items.Productq,
				Status:   actual_status,
				Type:     actual_type,
			}

			retrievedq := make(map[string]orm.Params)
			retrievedq = models.GetProductionQueuesdetails(formStruct1)
			c.Data["RetrievedProductionQueue"] = retrievedq

			return
		}
	} else {
		c.Redirect("/accessdenied", 303)
	}

}

func (c *ProductionController) ProductionStatus() {
	var user_id string
	var _orgId string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
	} else {
		user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
	}

	c.TplName = "production/production-orders.html"
	c.Data["Production"] = 1
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Production Orders"

	if c.Ctx.Input.Method() == "GET" {

		production_order_id := c.GetString("production_order_id")
		product_status := c.GetString("product_status")

		qty := c.GetString("qty")
		productionQuantity, _ := strconv.ParseFloat(qty, 64)

		// CHECK THE PREVIOUS STATUS OF PRODUCTION ORDER AND MANUALLY ASSIGNED BATCH FLAG.
		poStatus, isBatchedAssigned := models.GetStatusOfProductionOrder(production_order_id, _orgId)

		if poStatus == 0 && isBatchedAssigned == 0 {

			//IF APPROVE AND ADD TO QUEUE  OR THEN BLOCK
			if product_status == "1" || product_status == "2" {
				//GET THE INSERTED MODIFIED BOM ITEMS FROM PRODUCTION ORDER ITEMS TABLE

				blockStatus := BlockItemsForProduction(production_order_id, _orgId, productionQuantity)
				if blockStatus == 0 {
					c.Data["ErrorsPresent"] = true
					return
				}
			}
		}

		insertStatus := models.ChangeProductionOrderStatus(production_order_id, product_status, _orgId, user_id)

		b, _ := json.Marshal(insertStatus)
		c.Ctx.Output.Body([]byte(b))
	}
}

func (c *ProductionController) CheckProductionInventory() {
	//var user_id string
	var _orgId string
	//var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		//user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
	} else {
		//user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
	}

	//c.TplName = "sales/new-list.html"
	c.Data["Sales"] = 1
	//c.Data["PageTitle"] = "New/List"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

	if c.Ctx.Input.Method() == "GET" {
		production_order_id := c.GetString("production_order_id")
		//product_status := c.GetString("product_status")

		productId, reqQty, location := models.GetProductIdByProductionOrderId(_orgId, production_order_id)
		// formStruct := models.Sales{
		// 	Orgid:   _orgId,
		// 	OrderNo: order_no,
		// 	Userid:  user_id,
		// }
		var isInventoryOk string = "1"
		if productId != "false" {
			itemsmap := make(map[string]float64)

			//retrievedBOMItemsByPkid := make(map[string]orm.Params)
			retrievedBOMItemsByPkid, retreivedCount := models.GetBOMItemsByProductId(productId, _orgId)
			if retreivedCount == 0 {

			}
			for k, _ := range retrievedBOMItemsByPkid {
				var a string
				var p string
				// var s string
				var total float64
				a = retrievedBOMItemsByPkid[k]["quantity"].(string)
				p = retrievedBOMItemsByPkid[k]["_id"].(string)
				//s := reqQty
				available, _ := strconv.ParseFloat(a, 64)
				sent, _ := strconv.ParseFloat(reqQty, 64)
				total = available * sent
				total1 := strconv.FormatFloat(total, 'f', -1, 64)
				itemsmap[p] = total

				retrievedItem0 := models.CheckInventoryLevelItemCodeProductionOrders(p, total1, _orgId, location)
				if retrievedItem0 == 0 {
					//c.Data["quantity_exceeds"] = 1
					//flash.Warning("Quantity exceeds available quantity")
					//flash.Store(&c.Controller)
					//c.Data["ErrorsPresent"] = true
					// ormTransactionalErr = o.Rollback()
					// return 2
					isInventoryOk = "0"
				}
				if retrievedItem0 == 2 {
					//c.Data["NoStock"] = 1
					//flash.Warning("Item not in stock")
					//flash.Store(&c.Controller)
					//c.Data["ErrorsPresent"] = true
					// ormTransactionalErr = o.Rollback()
					// return 3
					isInventoryOk = "0"
				}
			}
		} else {
			isInventoryOk = "9"
		}

		outputObj := &InventoryStatus{
			InventoryOk: isInventoryOk,
		}
		b, _ := json.Marshal(outputObj)
		c.Ctx.Output.Body([]byte(b))
	}
}

func (c *ProductionController) GetUOM() {

	var _orgId string
	//var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		//user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		//username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		//user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		//username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}

	c.TplName = "production/view-modify-bom.html"
	c.Data["PageTitle"] = "View/Modify BOM"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

	if c.Ctx.Input.Method() == "GET" {

		item_name := c.GetString("item")
		//fmt.Println("*****Item is ",item_name)

		uom, status := models.GetUOM(item_name, _orgId)
		outputObj := &uomstruct{
			UOM:    uom,
			Status: status,
		}
		b, _ := json.Marshal(outputObj)
		//fmt.Println("***string b",string(b))
		c.Ctx.Output.Body([]byte(b))
	}
}

func (c *ProductionController) GetQuantityForSelectedBatchNo() {

	var _orgId string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
	} else {
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
	}

	if c.Ctx.Input.Method() == "GET" {

		batchNumber := c.GetString("batchNumber")
		location := c.GetString("location")

		qty, status := models.GetQuantityForSelectedBatchNo(batchNumber, _orgId, location)
		if qty == "" {
			qty = "0"
		}
		outputObj := &qtystruct{
			Qty:    qty,
			Status: status,
		}
		b, _ := json.Marshal(outputObj)
		c.Ctx.Output.Body([]byte(b))
	}
}

func (c *ProductionController) InsertItem() {
	var user_id string
	var _orgId string
	var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}
	c.Data["username"] = username
	UserSessionStruct := models.ModifyBom{
		Orgid:    _orgId,
		Userid:   user_id,
		Username: username,
	}

	if c.Ctx.Input.Method() == "GET" {
		product_name := c.GetString("product_name")
		product_code := c.GetString("product_code")
		new_item := c.GetString("new_item")
		new_qty := c.GetString("new_qty")
		new_wastage := c.GetString("new_wastage")
		//fmt.Println(product_name, product_code, new_item, new_qty, new_wastage)

		result := models.InsertNewItem(product_name, product_code, new_item, new_qty, new_wastage, UserSessionStruct)
		//fmt.Println(result)
		c.Ctx.Output.Body([]byte(result))
		//c.Data["completeRegister"] = true
	}
}

func (c *ProductionController) EditProducts() {
	var user_id string
	var _orgId string
	var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}
	c.Data["username"] = username
	c.Data["OrgID"] = _orgId
	c.TplName = "production/edit-products.html"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Edit Products"

	productDetailsStruct := models.Product{
		Orgid:    _orgId,
		Userid:   user_id,
		Username: username,
	}

	retrievedg := make(map[string]orm.Params)
	retrievedg = models.GetProduct(productDetailsStruct)
	c.Data["Retrievedg"] = retrievedg

	retrievedGrp := make(map[string]orm.Params)
	retrievedGrp = models.GetGroupProducts(productDetailsStruct)
	c.Data["RetrievedGroup"] = retrievedGrp
	retrievedSubGrp := make(map[string]orm.Params)
	retrievedSubGrp = models.GetSubGroupProducts(productDetailsStruct)
	c.Data["RetrievedSubGroup"] = retrievedSubGrp
	formStructfac := models.StockItems{
		Orgid:    _orgId,
		Userid:   user_id,
		Username: username,
	}
	retrievedlocs := make(map[string]orm.Params)
	retrievedlocs = models.GetFactory(formStructfac)
	c.Data["RetrievedLocs"] = retrievedlocs

	if c.Ctx.Input.Method() == "GET" {
		item_code := c.GetString("itemCode")
		//item_id := c.GetString("id")
		//fmt.Println("Inside Get view grn controller with ", item_id)
		flash := beego.NewFlash()
		items := addProductValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			return
		}
		c.Data["FormItems"] = models.GetProductDetailsByName(item_code, _orgId)
		var retrievedlocation []string
		maplocation := make(map[int]string)
		retrievedlocation = models.GetProductLocationDetailsByName(item_code, _orgId)

		for i := 0; i < len(retrievedlocation); i++ {
			maplocation[i] = retrievedlocation[i]
		}
		c.Data["RetrievedItemsLocation"] = maplocation
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		return
	}
	if c.Ctx.Input.Method() == "POST" {
		var buttonSubmit string
		c.Ctx.Input.Bind(&buttonSubmit, "item-button")
		if buttonSubmit == "show" {
			flash := beego.NewFlash()
			items := addProductValidate{}
			if err := c.ParseForm(&items); err != nil {
				flash.Error("Cannot parse form")
				flash.Store(&c.Controller)
				return
			}
			c.Data["FormItems"] = models.GetProductDetailsByName(items.ProductCheck, _orgId)
			var retrievedlocation []string
			maplocation := make(map[int]string)
			retrievedlocation = models.GetProductLocationDetailsByName(items.ProductCheck, _orgId)

			for i := 0; i < len(retrievedlocation); i++ {
				maplocation[i] = retrievedlocation[i]
			}
			c.Data["RetrievedItemsLocation"] = maplocation
			c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
			return
		}
		flash := beego.NewFlash()
		items := addProductValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			c.Data["ErrorsPresent"] = true
			return
		}

		checkItemStatus := models.CheckForExistingProductName1(items.ProductName, _orgId, items.ProductID)
		if checkItemStatus == 1 {
			c.Data["ProductNameExists"] = true
			return
		}

		checkItemStatus1 := models.CheckForExistingProductUniqueCode1(items.ProductCode, _orgId, items.ProductID)
		if checkItemStatus1 == 1 {
			c.Data["ProductCodeExists"] = true
			return
		}

		checkItemStatus2 := models.CheckForExistingProductInvoiceNarration1(items.InvoiceNarration, _orgId, items.ProductID)
		if checkItemStatus2 == 1 {
			c.Data["InvoiceNarrationExists"] = true
			return
		}
		formStruct := models.Product{
			Orgid:            _orgId,
			Userid:           user_id,
			Username:         username,
			ProductName:      strings.Title(items.ProductName),
			InvoiceNarration: strings.Title(items.InvoiceNarration),
			ProductCode:      strings.ToUpper(items.ProductCode),
			Group:            strings.ToUpper(items.Group),
			SubGroup:         strings.ToUpper(items.SubGroup),
			ProductType:      items.ProductType,
			Capacity:         items.Capacity,
			UOM:              strings.Title(items.UOM),
			RatePerUOM:       items.RatePerUOM,
			MinStock:         items.MinStock,
			Transport:        items.Transport,
			ProductProcess:   items.ProductProcess,
			Status1:          items.Status1,
			ProductCheck:     items.ProductCheck,
			ProductID:        items.ProductID,
			FactoryLoc:       items.FactoryLoc,
		}

		insertStatus := models.UpdateProduct(formStruct)
		if insertStatus == 0 {
			flash.Error("Form Not Inserted")
			flash.Store(&c.Controller)
			return
		}
		c.Redirect("/production/edit-products", 303)
	}
}

func (c *ProductionController) AddNewMachine() {
	flash := beego.ReadFromRequest(&c.Controller)
	if ok := flash.Data["error"]; ok != "" {
		c.Data["FlasErrors"] = ok
	}
	if ok := flash.Data["notice"]; ok != "" {
		// Display error messages
		c.Data["FlashNotices"] = ok
	}
	var user_id string
	var _orgId string
	var username string
	var localetimezone string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
		localetimezone = fmt.Sprintf("%s", sessionVar.Get("LocaleTimeZone"))
	} else {
		user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
		localetimezone = fmt.Sprintf("%s", c.Ctx.Input.GetData("LocaleTimeZone"))
	}
	c.Data["username"] = username
	formStructMachine := models.StockItems{
		Orgid:    _orgId,
		Userid:   user_id,
		Username: username,
	}
	formStructMachine1 := models.Machine{
		Orgid:    _orgId,
		Userid:   user_id,
		Username: username,
	}

	newGenCode, newCode := c.GetGeneratedCode(_orgId, 8, localetimezone)
	c.Data["genCode"] = newGenCode

	retrievedfac := make(map[string]orm.Params)
	retrievedfac = models.GetFactory(formStructMachine)
	c.Data["Retrievedfac"] = retrievedfac
	retrieved := make(map[string]orm.Params)
	retrieved = models.GetAllMachine(formStructMachine1)
	c.Data["Retrieved"] = retrieved

	c.TplName = "production/add-machine.html"
	c.Data["Production"] = 1
	c.Data["PageTitle"] = "Add Machine"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

	if c.Ctx.Input.Method() == "POST" {
		flash := beego.NewFlash()
		items := addMachineValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			c.Data["ErrorsPresent"] = true
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			c.Data["ErrorsPresent"] = true
			return
		}

		checkMachineStatus := models.CheckForExistingMachineName(items.MachineName, _orgId)
		if checkMachineStatus == 1 {
			c.Data["MachineNameExists"] = true
			return
		}

		checkCodeStatus := models.CheckForExistingMachineCode(items.MachineCode, _orgId)
		if checkCodeStatus == 1 {
			c.Data["MachineCodeExists"] = true
			return
		}

		formStruct := models.Machine{
			Orgid:       _orgId,
			Userid:      user_id,
			Username:    username,
			MachineName: items.MachineName,
			MachineCode: newCode,
			AddedOn:     items.AddedOn,
			GeoLocation: items.GeoLocation,
			Status:      items.Status,
			NewGenCode:  newGenCode,
		}
		//fmt.Println("Status is",result["status"])
		insertStatus := models.AddNewMachine(formStruct)
		if insertStatus == 0 {
			flash.Error("Form Not Inserted")
			flash.Store(&c.Controller)
			c.Data["ErrorsPresent"] = true
			return
		}
		c.Data["completeRegister"] = true
		flash.Notice("Machine Inserted")
		flash.Store(&c.Controller)
		c.Redirect("/production/add-machine", 303)
	}
}

func (c *ProductionController) EditMachine() {
	flash := beego.ReadFromRequest(&c.Controller)
	if ok := flash.Data["error"]; ok != "" {
		c.Data["FlasErrors"] = ok
	}
	if ok := flash.Data["notice"]; ok != "" {
		// Display error messages
		c.Data["FlashNotices"] = ok
	}
	var user_id string
	var _orgId string
	var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}
	c.Data["username"] = username
	c.TplName = "production/edit-machine.html"
	c.Data["PageTitle"] = "Edit Machine"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

	formStructMachine := models.StockItems{
		Orgid:    _orgId,
		Userid:   user_id,
		Username: username,
	}
	retrievedfac := make(map[string]orm.Params)
	retrievedfac = models.GetFactory(formStructMachine)

	if c.Ctx.Input.Method() == "GET" {
		machine_id := c.GetString("machineId")
		flash := beego.NewFlash()
		items := addMachineValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			return
		}
		c.Data["FormItems"] = models.GetMachineDataById(machine_id, _orgId)

		// retrievedfac := make(map[string]orm.Params)
		// retrievedfac = models.GetFactory(formStructMachine)
		c.Data["Retrievedfac"] = retrievedfac
		// retrieved := make(map[string]orm.Params)
		// retrieved = models.GetAllMachine(formStructMachine1)
		// c.Data["Retrieved"] = retrieved

	}

	if c.Ctx.Input.Method() == "POST" {
		c.Data["Retrievedfac"] = retrievedfac
		flash := beego.NewFlash()
		items := addMachineValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			c.Data["ErrorsPresent"] = true
			return
		}

		prevMacName, prevMacCode := models.GetPrevMachine(items.PrevId, _orgId)

		if prevMacName != items.MachineName {
			checkMachineStatus := models.CheckForExistingMachineName(items.MachineName, _orgId)
			if checkMachineStatus == 1 {
				c.Data["MachineNameExists"] = true
				return
			}
		}

		if prevMacCode != items.MachineCode {
			checkCodeStatus := models.CheckForExistingMachineCode(items.MachineCode, _orgId)
			if checkCodeStatus == 1 {
				c.Data["MachineCodeExists"] = true
				return
			}
		}

		formStruct := models.Machine{
			PrevId:      items.PrevId,
			Orgid:       _orgId,
			Userid:      user_id,
			Username:    username,
			MachineName: items.MachineName,
			MachineCode: items.MachineCode,
			AddedOn:     items.AddedOn,
			GeoLocation: items.GeoLocation,
			Status:      items.Status,
		}
		//fmt.Println("Status is",result["status"])
		insertStatus := models.UpdateMachine(formStruct)
		if insertStatus == 0 {
			flash.Error("Form Not Inserted")
			flash.Store(&c.Controller)
			c.Data["ErrorsPresent"] = true
			return
		}
		c.Data["completeRegister"] = true
		flash.Notice("Machine Updated")
		flash.Store(&c.Controller)
		c.Redirect("/production/add-machine", 303)
	}
}

func (c *ProductionController) GetProductsEditDetailsAjax() {
	//c.Data["username"] = username
	c.TplName = "production/edit-products.html"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Edit Products"

	if c.Ctx.Input.Method() == "GET" {

		_orgId := c.GetString("id")

		uom := models.GetProductsEditUOM(_orgId)
		group := models.GetProductsEditGroups(_orgId)
		subgroup := models.GetProductsEditSubgroups(_orgId)

		outputObj := &productseditstruct{
			UOM:      uom,
			Group:    group,
			Subgroup: subgroup,
		}
		b, _ := json.Marshal(outputObj)
		c.Ctx.Output.Body([]byte(b))
	}
}

func (c *ProductionController) GetMachinesAjax() {
	//var user_id string
	var _orgId string
	var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		//user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		//user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}
	c.Data["username"] = username

	c.TplName = "production/production-orders.html"
	c.Data["Production"] = 1
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Production Orders"

	if c.Ctx.Input.Method() == "GET" {

		factoryId := c.GetString("factory_id")

		machine_ids, machine_codes, machine_names, status := models.GetMachinesByFactoryId(factoryId, _orgId)

		outputObj := &machinesstruct{
			MachineId:   machine_ids,
			MachineCode: machine_codes,
			MachineName: machine_names,
			Status:      status,
		}
		b, _ := json.Marshal(outputObj)
		c.Ctx.Output.Body([]byte(b))
	}
}

func (c *ProductionController) GetProductsBomAjaxForProductionOrders() {
	//var user_id string
	var _orgId string
	var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		//user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		//user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}
	c.Data["username"] = username

	c.TplName = "production/production-orders.html"
	c.Data["Production"] = 1
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Production Orders"

	if c.Ctx.Input.Method() == "GET" {

		productId := c.GetString("product_Id")
		//production_id := c.GetString("production_id")
		factoryId := c.GetString("factory_Id")

		_, item_names, item_codes, quantity_used, quantity_wasted, qty_in_stock, uoms, status := models.GetBOMProductionOrders(productId, factoryId, _orgId)

		outputObj := &bomstruct{
			ItemNames:  item_names,
			ItemCodes:  item_codes,
			QtyUsed:    quantity_used,
			QtyWasted:  quantity_wasted,
			QtyInStock: qty_in_stock,
			UOMs:       uoms,
			Status:     status,
		}
		b, _ := json.Marshal(outputObj)
		c.Ctx.Output.Body([]byte(b))
	}
}

func (c *ProductionController) GetProductsBomAjaxForProductionOrdersModal() {
	//var user_id string
	var _orgId string
	var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		//user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		//user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}
	c.Data["username"] = username

	c.TplName = "production/production-orders.html"
	c.Data["Production"] = 1
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Production Orders"

	if c.Ctx.Input.Method() == "GET" {

		//productId := c.GetString("product_Id")
		production_id := c.GetString("production_id")
		factoryId := c.GetString("factory_Id")

		_, item_names, item_codes, quantity_used, quantity_wasted, qty_in_stock, uoms, status, only_code := models.GetBOMFromProductionOrderItems(production_id, factoryId, _orgId)

		outputObj := &bomstruct{
			ItemNames:  item_names,
			ItemCodes:  item_codes,
			QtyUsed:    quantity_used,
			QtyWasted:  quantity_wasted,
			QtyInStock: qty_in_stock,
			UOMs:       uoms,
			Status:     status,
			OnlyCode:   only_code,
		}
		b, _ := json.Marshal(outputObj)
		c.Ctx.Output.Body([]byte(b))
	}
}

func (c *ProductionController) GetPODetails() {
	//var user_id string
	var _orgId string
	var username string
	//var localetimezone string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		//user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
		//localetimezone = fmt.Sprintf("%s", sessionVar.Get("LocaleTimeZone"))
	} else {
		//user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
		//localetimezone = fmt.Sprintf("%s", c.Ctx.Input.GetData("LocaleTimeZone"))
	}
	c.Data["username"] = username
	// formStruct5 := models.Sales{
	// 	Orgid:    _orgId,
	// 	Userid:   user_id,
	// 	Username: username,
	// }

	poId := c.GetString("poId")

	retrieved := make(map[string]orm.Params)
	retrieved = models.GetPOdetailsByPkId(poId, _orgId)
	c.Data["PORetrieved"] = retrieved

	c.TplName = "production/po-details.html"
	c.Data["PageTitle"] = "Production Order	Details"
	//c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
}

func BlockItemsForProduction(orderId string, _orgId string, productionQuantity float64) int {

	//GET THE PRODUCTION ORDER ITEMS FOR THE PRODUCTION ORDER
	prodItems := models.GetProductionOrderItems(orderId, _orgId)

	productionFkid, _ := strconv.ParseInt(orderId, 10, 64)

	for _, it := range prodItems {

		itemid := it["_id"].(string)
		itemID, _ := strconv.ParseInt(itemid, 10, 64)

		rqty := it["quantity"].(string)
		reqQty, _ := strconv.ParseFloat(rqty, 64)

		//CALCULATE THE QUANTITY BASED ON PRODUCTION ORDER QUANTITY
		//totalNeeded := productionQuantity * reqQty
		totalNeeded := reqQty

		formStruct := models.ItemDetailsForOrder{
			OrgId:             _orgId,
			ItemId:            itemID,
			RequiredQuantity:  totalNeeded,
			Location:          it["geo_location"].(string),
			BlockedBy:         1,
			ConsumedTableFkId: productionFkid,
		}

		insertStatus := models.BlockItemsBeforeConsumption(formStruct)
		if insertStatus == 0 {
			//c.Data["ErrorsPresent"] = true
			return 0
		}
	}
	return 1
}

func (c *ProductionController) EditProductionOrder() {

	var user_id string
	var _orgId string
	var username string
	var localetimezone string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
		localetimezone = fmt.Sprintf("%s", sessionVar.Get("LocaleTimeZone"))
	} else {
		user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
		localetimezone = fmt.Sprintf("%s", c.Ctx.Input.GetData("LocaleTimeZone"))
	}

	productionId := c.GetString("productionId")

	c.Data["username"] = username
	c.Data["_orgId"] = _orgId
	c.Data["user_id"] = user_id
	c.Data["localeTimeZone"] = localetimezone

	c.Data["RetrievedProductionOrder"] = models.GetProductionOrdersdetailsById(productionId, _orgId)

	flash := beego.ReadFromRequest(&c.Controller)
	if ok := flash.Data["error"]; ok != "" {
		c.Data["FlashErrors"] = ok
	}
	if ok := flash.Data["notice"]; ok != "" {
		// Display error messages
		c.Data["FlashNotices"] = ok
	}

	c.TplName = "production/edit-production-order.html"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Edit Production Order"

	if c.Ctx.Input.Method() == "POST" {

		flash := beego.NewFlash()
		items := updateProductionOrderValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			c.Data["ErrorsPresent"] = true
			return
		}

		updateStatus := models.UpdateProductionOrderQuantity(items.ProductionId, _orgId, items.Quantity)

		if updateStatus == 0 {
			flash.Error("Error updating the quantity.")
			flash.Store(&c.Controller)
			return
		}
		flash.Notice("Production Order Quantity Updated.")
		flash.Store(&c.Controller)
		c.Redirect("/production/edit-production-order?productionId="+productionId, 303)
		// return
	}

}
