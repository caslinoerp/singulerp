package controllers

import (
	"fmt"

	"bitbucket.org/caslinoerp/singulerp/models"


	"github.com/astaxie/beego/orm"
)

type DashboardController struct {
	BaseController
}

func (c *DashboardController) Get() {
	c.TplName = "dashboard.html"
	c.Data["PageTitle"] = "Dashboard"
	var user_id string
	var _orgId string
	var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}
	//fmt.Println("%s %s %s", user_id, _orgId, username)
	c.Data["username"] = username
	c.Data["_orgId"] = _orgId
	c.Data["user_id"] = user_id

	retrieved := make(map[string]string)
	retrieved = models.GetDashboardSales(_orgId)
	c.Data["Retrieved"] = retrieved

	retrievedpo := make(map[string]string)
	retrievedpo = models.GetDashboardPO(_orgId)
	c.Data["Retrievedpo"] = retrievedpo

	retrievedprod := make(map[string]string)
	retrievedprod = models.GetDashboardProd(_orgId)
	c.Data["Retrievedprod"] = retrievedprod

	retrievedtransit := make(map[string]string)
	retrievedtransit = models.GetDashboardInTransit(_orgId)
	c.Data["Retrievedtransit"] = retrievedtransit

	retrievedlowlevels := make(map[string]orm.Params)
	retrievedlowlevels = models.GetLowLevels(_orgId)	
	c.Data["RetrievedLowLevels"] = retrievedlowlevels

	retrieveddelayedpo := make(map[string]orm.Params)
	retrieveddelayedpo = models.GetDelayedPOs(_orgId)
	c.Data["RetrievedDelayedPOs"] = retrieveddelayedpo

	retrieveddelayedproduction := make(map[string]orm.Params)
	retrieveddelayedproduction = models.GetDelayedProduction(_orgId)
	c.Data["RetrievedDelayedProduction"] = retrieveddelayedproduction

	retrieveddelayedsales := make(map[string]orm.Params)
	retrieveddelayedsales = models.GetDelayedSales(_orgId)
	c.Data["RetrievedDelayedSales"] = retrieveddelayedsales
}
