package controllers

import (
	"encoding/json"
	"fmt"
	"html/template"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/caslinoerp/singulerp/models"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"github.com/astaxie/beego/validation"
)

type StockController struct {
	BaseController
}

type addNewItemValidate struct {
	ItemName         string   `form:"item_name" valid:"Required"`
	InvoiceNarration string   `form:"invoice_narration"`
	ItemCode         string   `form:"item_code" valid:"Required"`
	Group            string   `form:"group" valid:"Required"`
	SubGroup         string   `form:"sub_group" valid:"Required"`
	ItemGrade        string   `form:"item_grade" `
	UOM              string   `form:"uom" valid:"Required"`
	BinLocation      string   `form:"bin_location" `
	MinOrderQty      string   `form:"min_order_qty"`
	LeadTimeDays     string   `form:"lead_time_days" `
	MinStockTrigger  string   `form:"min_stock_trigger" valid:"Required"`
	MaxStorageQty    string   `form:"max_storage" `
	GSTRate          string   `form:"gst_rate"`
	CessRate         string   `form:"cess_rate"`
	HSNSAC           string   `form:"hsn_sac"`
	GeoLocation      []string `form:"geo_location[]"`
	Status1          string   `form:"status1"`
	Status           string   `form:"status"`
	IsProduced       string   `form:"is_produced"`
	NonConsumable    string   `form:"non_consumable"`
	ItemNameCheck    string   `form:"item_name_check"`
	ItemID           string   `form:"item_id"`
	ShelfLife        string   `form:"Shelf_Life_Days"`
	UserDefItemCode  string   `form:"user_code"`
	ItemCodeOption   string   `form:"SetItemCode"`
}

type editItemValidate struct {
	ItemName         string   `form:"item_name" valid:"Required"`
	InvoiceNarration string   `form:"invoice_narration" `
	ItemCode         string   `form:"item_code" valid:"Required"`
	Group            string   `form:"group" valid:"Required"`
	SubGroup         string   `form:"sub_group" valid:"Required"`
	ItemGrade        string   `form:"item_grade" `
	UOM              string   `form:"uom" valid:"Required"`
	BinLocation      string   `form:"bin_location" `
	MinOrderQty      string   `form:"min_order_qty" `
	LeadTimeDays     string   `form:"lead_time_days" `
	MinStockTrigger  string   `form:"min_stock_trigger" valid:"Required"`
	MaxStorageQty    string   `form:"max_storage" `
	GSTRate          string   `form:"gst_rate"`
	CessRate         string   `form:"cess_rate"`
	HSNSAC           string   `form:"hsn_sac"`
	GeoLocation1     []string `form:"geo_location1" `
	Status           string   `form:"status"`
	IsProduced       string   `form:"is_produced"`
	NonConsumable    string   `form:"non_consumable"`
	ProductionStatus string   `form:"status1"`
	ItemNameCheck    string   `form:"item_name_check"`
	ItemID           string   `form:"item_id"`
	OrgID            string   `form:"org_id"`
	ShelfLife        string   `form:"Shelf_Life_Days"`
}

type bomValidate1 struct {
	ProductPKID     string `form:"product_id" valid:"Required"`
	ItemCode        string `form:"item_code"`
	ItemName        string `form:"item_name"`
	ItemName8       string `form:"group-a[0][item_name8]"`
	Qty             string `form:"group-a[0][qty]"`
	Wastage         string `form:"group-a[0][wastage]"`
	ItemName1       string `form:"item1"`
	Qty1            string `form:"qty1"`
	Wastage1        string `form:"wastage1"`
	ItemName2       string `form:"item2"`
	Qty2            string `form:"qty2"`
	Wastage2        string `form:"wastage2"`
	ItemName3       string `form:"item3"`
	Qty3            string `form:"qty3"`
	Wastage3        string `form:"wastage3"`
	ItemName4       string `form:"item4"`
	Qty4            string `form:"qty4"`
	Wastage4        string `form:"wastage4"`
	ItemName5       string `form:"item5"`
	Qty5            string `form:"qty5"`
	Wastage5        string `form:"wastage5"`
	ItemName6       string `form:"item6"`
	Qty6            string `form:"qty6"`
	Wastage6        string `form:"wastage6"`
	ItemName7       string `form:"item7"`
	Qty7            string `form:"qty7"`
	Wastage7        string `form:"wastage7"`
	Nc1             string `form:"nc1"`
	NC2             string `form:"nc2"`
	NC3             string `form:"nc3"`
	NC4             string `form:"nc4"`
	NC5             string `form:"nc5"`
	NC6             string `form:"nc6"`
	NC7             string `form:"nc7"`
	NC              string `form:"nc"`
	HiddenMethod    string `form:"hidden_method"`
	UserDefItemCode string `form:"user_code"`
}

type addInventoryValidate struct {
	ItemCode     string `form:"item_code" valid:"Required"`
	ItemName     string `form:"item_name" valid:"Required"`
	OrderDate    string `form:"date" `
	Suppliers    string `form:"suppliers" `
	Location     string `form:"location" valid:"Required"`
	DeliveryDate string `form:"delivery_date" `
	QtyPurchase  string `form:"qty_purchase" valid:"Required"`
	TotalPrice   string `form:"total_price" valid:"Required"`
	Currency     string `form:"currency"`
}

type viewInventoryValidate struct {
	ItemCode string `form:"item_code" `
	ItemName string `form:"item_name" `
	Group    string `form:"grp" `
	Grade    string `form:"grade" `
	Factory  string `form:"factory"`
	Min      string `form:"min" `
	Max      string `form:"max" `
}

type AdjustmentValidate struct {
	Code           string `form:"i_code"`
	Factory        string `form:"factory" valid:"Required"`
	Name           string `form:"i_name" valid:"Required"`
	DateInspection string `form:"date_inspection" valid:"Required"`
	QtyRecord      string `form:"qty_record"`
	QtyObserved    string `form:"qty_observed" valid:"Required"`
	Remark         string `form:"remark" valid:"Required"`
	Reason         string `form:"reason" valid:"Required"`
	ADbutton       string `form:"ad_button" `
}

type usageInventoryValidate struct {
	ItemCode        string `form:"item_code"`
	ItemName        string `form:"item_name"`
	From            string `form:"from"`
	To              string `form:"to" `
	SubmitButtonVal string `form:"assoc-button"`
}

type usageGRNValidate struct {
	ItemCode        string `form:"item_code"`
	ItemName        string `form:"item_name"`
	SubmitButtonVal string `form:"assoc-button"`
}

type editstruct struct {
	UOM      []string `json:"uom"`
	Grade    []string `json:"grade"`
	Group    []string `json:"group"`
	Subgroup []string `json:"subgroup"`
}

type inventorystruct struct {
	ItemCode    string `json:"item_code"`
	ExistingQty string `json:"existing_qty"`
}

type getItemStruct struct {
	ItemId           []string `json:"itemId"`
	ItemCode         []string `json:"itemCode"`
	ItemUom          []string `json:"itemUom"`
	InvoiceNarration []string `json:"invoiceNarration"`
}

type getStockStruct struct {
	StockId        []string `json:"stockId"`
	Quantity       []string `json:"quantity"`
	Rate           []string `json:"rate"`
	StartDate      []string `json:"startDate"`
	PurchaseOrProd []string `json:"purchaseOrProd"`
	GrnNo          []string `json:"grnNo"`
	AddedQty       []string `json:"addedQty"`
	IsEditable     []string `json:"isEditable"`
	ThePlaceholder []string `json:"thePlaceholder"`
	TotalStockQty  string   `json:"totalStockQty"`
}

type totalItemStock struct {
	ItemStock   string `json:"item_stock"`
	UnitMeasure string `json:"unit_measure"`
}

type SearchItemNameCode struct {
	ItemCode string `form:"ItemCode"`
	ItemName string `form:"ItemName"`
}

type ItemPackaging struct {
	PacketSize    string `form:"PackagingGrp[0][packet_size]" valid:"Required"`
	Price         string `form:"PackagingGrp[0][price]" valid:"Required"`
	ItemNameCheck string `form:"item_name_check"`
	ItemCode      string `form:"item_code"`
}

var CodeToDisplay, ItemCodeMethod, NewUniqueCode string

func (c *StockController) AddNewItemsProductToStock() {

	var user_id string
	var _orgId string
	var username string
	var localetimezone string
	var newGenCode string
	var newCode string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
		localetimezone = fmt.Sprintf("%s", sessionVar.Get("LocaleTimeZone"))
	} else {
		user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
		localetimezone = fmt.Sprintf("%s", c.Ctx.Input.GetData("LocaleTimeZone"))
	}

	c.Data["username"] = username
	c.Data["OrgID"] = _orgId
	c.Data["ItemCodeGenOption"] = "checked"

	if c.Ctx.Input.Method() == "GET" {
		newGenCode, newCode = c.GetGeneratedCode(_orgId, 1, localetimezone)
		c.Data["ItemCode"] = newGenCode
		c.Data["UserDefItemCode"] = ""
		NewUniqueCode = newCode

		formStruct1 := models.StockItems{
			Orgid:    _orgId,
			Userid:   user_id,
			Username: username,
		}

		flash := beego.ReadFromRequest(&c.Controller)
		if ok := flash.Data["error"]; ok != "" {
			c.Data["FlashErrors"] = ok
		}
		if ok := flash.Data["notice"]; ok != "" {

			c.Data["FlashNotices"] = ok
		}

		retrievedfac := make(map[string]orm.Params)
		retrievedfac = models.GetFactory(formStruct1)
		c.Data["Retrievedfac"] = retrievedfac
		retrievedGrp := make(map[string]orm.Params)
		retrievedGrp = models.GetGroup(formStruct1)
		c.Data["RetrievedGroup"] = retrievedGrp
		retrievedSubGrp := make(map[string]orm.Params)
		retrievedSubGrp = models.GetSubGroup(formStruct1)
		c.Data["RetrievedSubGroup"] = retrievedSubGrp
		retrievedGrade := make(map[string]orm.Params)
		retrievedGrade = models.GetGrade(formStruct1)
		c.Data["RetrievedGrade"] = retrievedGrade
		retrieved := make(map[string]orm.Params)
		retrieved = models.GetAllItemsForDropdowns(_orgId)
		c.Data["Retrieved"] = retrieved
		retrievedUOM := make(map[string]orm.Params)
		retrievedUOM = models.GetItemsUOM(formStruct1)
		c.Data["RetrievedUOM"] = retrievedUOM

		c.TplName = "stock/add-new-item.html"
		c.Data["Stock"] = 1
		c.Data["PageTitle"] = "Add Stock Item"
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		c.Data["view"] = "Product"

	}

	//generic code to handle post request and validation
	if c.Ctx.Input.Method() == "POST" {

		if c.GetString("Searchmethod") == "srch" {
			var buttonSearch string
			c.Ctx.Input.Bind(&buttonSearch, "item-search")
			if buttonSearch == "SearchItem" {
				flash := beego.NewFlash()
				items := SearchItemNameCode{}
				if err := c.ParseForm(&items); err != nil {
					flash.Error("Cannot parse form")
					//flash.Store(&c.Controller)
					return
				}
				retrieved := make(map[string]orm.Params)
				retrieved = models.SearchByItemNameCode(items.ItemCode, items.ItemName, _orgId)
				c.Data["SearchItems"] = items
				c.Data["Retrieved"] = retrieved
				c.TplName = "stock/add-new-item.html"
				c.Data["PageTitle"] = "Add Stock Item"
				c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
				c.Data["view"] = "Product"
				return
			}
			c.Ctx.Input.Bind(&buttonSearch, "Reset-itemSearch")
			if buttonSearch == "ResetItemSearch" {
				c.Redirect("/stock/add-new-items", 303)
				return
			}
		}
		flash := beego.NewFlash()

		if c.GetString("hidden_method") == "product" {
			items := addNewItemValidate{}
			if err := c.ParseForm(&items); err != nil {
				flash.Error("Cannot parse form")
				flash.Store(&c.Controller)
				return
			}
			c.TplName = "stock/add-new-item.html"
			c.Data["view"] = "Product"
			c.Data["FormItems"] = items
			c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
			valid := validation.Validation{}
			if b, _ := valid.Valid(&items); !b {
				c.Data["Errors"] = valid.ErrorsMap
				c.Data["ErrorsPresent"] = true
				return
			}

			if items.ItemCodeOption == "Set_ItemCode" {
				newGenCode = items.UserDefItemCode
				CodeToDisplay = newGenCode
				ItemCodeMethod = "0"
				c.Data["UserDefItemCodeOption"] = "checked"
			}
			if items.ItemCodeOption == "Set_ItemCodeAuto" {
				newGenCode = items.ItemCode
				CodeToDisplay = newGenCode
				ItemCodeMethod = "1"
				c.Data["ItemCodeGenOption"] = "checked"
			}

			checkItemStatus := models.CheckForExistingItemName(items.ItemName, _orgId)
			if checkItemStatus == 1 {
				c.Data["ItemNameExists"] = true
				return
			}

			checkCodeStatus := models.CheckForUserDefinedItemCode(newGenCode, _orgId)
			if checkCodeStatus == 1 {
				c.Data["ItemCodeExists"] = true
				return
			}

			checkUserDefinedCodeStatus := models.CheckForExistingItemUniqueCode(newCode, _orgId)
			if checkUserDefinedCodeStatus == 1 {
				c.Data["ItemCodeExists"] = true
				return
			}

			if items.IsProduced != "on" {
				items.Status1 = "0"
			}

			formStruct := models.StockItems{
				Orgid:            _orgId,
				Userid:           user_id,
				Username:         username,
				ItemName:         strings.Title(items.ItemName),
				Group:            strings.ToUpper(items.Group),
				SubGroup:         strings.ToUpper(items.SubGroup),
				ItemGrade:        strings.ToUpper(items.ItemGrade),
				UOM:              strings.Title(items.UOM),
				ItemCode:         strings.ToUpper(NewUniqueCode),
				InvoiceNarration: strings.Title(items.InvoiceNarration),
				BinLocation:      items.BinLocation,
				MinOrderQty:      items.MinOrderQty,
				LeadTimeDays:     items.LeadTimeDays,
				MinStockTrigger:  items.MinStockTrigger,
				MaxStorageQty:    items.MaxStorageQty,
				GSTRate:          items.GSTRate,
				CessRate:         items.CessRate,
				HSNSAC:           items.HSNSAC,
				GeoLocation1:     items.GeoLocation,
				IsProduced:       items.IsProduced,
				NonConsumable:    items.NonConsumable,
				ProductionStatus: items.Status1,
				ShelfLife:        items.ShelfLife,
				NewGenCode:       newGenCode,
			}

			insertStatus := models.CreateNewItemorProduct(formStruct)
			if insertStatus == 0 {
				flash.Error("Item/Product is Not Inserted")
				flash.Store(&c.Controller)
				return
			}

			if items.IsProduced == "on" {
				c.Redirect("/material/add-bom.html?productid="+newGenCode+"&productName="+items.ItemName, 303)
			}

			c.Data["completeRegister"] = true
			flash.Notice("Item/Product Inserted")
			flash.Store(&c.Controller)

			c.Redirect("/stock/add-new-items", 303)
		}
	} else {
		c.Data["NoProductAdd"] = true
	}

}

func (c *StockController) Packaging() {
	//var user_id string
	var _orgId string
	var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		//user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		//user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}

	// userDetailsStruct := models.StockItems{
	// 	Orgid:    _orgId,
	// 	Userid:   user_id,
	// 	Username: username,
	// }

	c.Data["username"] = username
	c.Data["OrgID"] = _orgId

	retrieveditems := make(map[string]orm.Params)
	retrieveditems = models.GetAllItemsForDropdowns(_orgId)
	c.Data["ItemsRetrieved"] = retrieveditems

	// retrieved := make(map[string]orm.Params)
	// retrieved = models.GetArchivedPackages(_orgId)
	// c.Data["Retrieved"] = retrieved

	if c.Ctx.Input.Method() == "GET" {
		flash := beego.ReadFromRequest(&c.Controller)
		if ok := flash.Data["error"]; ok != "" {
			c.Data["FlashErrors"] = ok
		}
		if ok := flash.Data["notice"]; ok != "" {
			c.Data["FlashNotices"] = ok
		}
		c.TplName = "stock/Packaging.html"
		c.Data["PageTitle"] = "Packaging"
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		return
	}
	if c.Ctx.Input.Method() == "POST" {
		var buttonSubmit string
		//c.Ctx.Input.Bind(&buttonSubmit, "item-button")
		//if buttonSubmit == "show" {
		flash := beego.NewFlash()
		items := ItemPackaging{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			return
		}

		retrieved := make(map[string]orm.Params)
		retrieved = models.GetArchivedPackages(items.ItemNameCheck, _orgId)
		c.Data["Retrieved"] = retrieved

		c.Data["FormItems"] = models.GetItemDetailsByItemId(items.ItemNameCheck, _orgId)
		c.Data["ItemPackages"] = models.GetItemsPackages(items.ItemNameCheck, _orgId)
		c.Data["PageTitle"] = "Packaging"
		c.TplName = "stock/Packaging.html"
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		//return
		//}
		c.Ctx.Input.Bind(&buttonSubmit, "ItemPackage-button")
		if buttonSubmit == "InsertItemPackages" {
			flash := beego.NewFlash()
			items := ItemPackaging{}
			PacketSizes := make([]map[string]string, 0, 3)
			c.Ctx.Input.Bind(&PacketSizes, "PackagingGrp")
			PacketPrice := make([]map[string]string, 0, 3)
			c.Ctx.Input.Bind(&PacketPrice, "PackagingGrp")

			if err := c.ParseForm(&items); err != nil {
				flash.Error("Cannot parse form")
				flash.Store(&c.Controller)
				return
			}

			insertStatus := models.AddItemPackaging(_orgId, items.ItemCode, PacketSizes, PacketPrice)
			if insertStatus == 0 {
				flash.Error("Form Not Inserted")
				flash.Store(&c.Controller)
				return
			}

			c.Data["username"] = username
			c.Data["OrgID"] = _orgId
			c.TplName = "stock/Packaging.html"
			c.Data["PageTitle"] = "Packaging"
			flash.Notice("Sub item/items Inserted!")
			flash.Store(&c.Controller)
			c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
			return
		}
	}

}

func (c *StockController) DeletePackage() {
	var _orgId string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
	} else {
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
	}
	c.TplName = "stock/Packaging.html"
	c.Data["PageTitle"] = "Add Item Packages"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

	if c.Ctx.Input.Method() == "GET" {
		pkg_id := c.GetString("id")
		t := time.Now()
		fmt.Println(t.Format("20060102150405"))
		result := models.DeletePackage(pkg_id, t.Format("20060102150405"), _orgId)
		c.Ctx.Output.Body([]byte(result))

		c.TplName = "stock/Packaging.html"
		c.Data["PageTitle"] = "Packaging"
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		return
	}
}

func (c *StockController) CheckItemInventory() {
	var user_id string
	var _orgId string
	//var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
	} else {
		user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
	}

	c.TplName = "stock/add-new-item.html"
	c.Data["Stock"] = 1
	c.Data["PageTitle"] = "Add Stock Item"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

	if c.Ctx.Input.Method() == "GET" {
		item_id := c.GetString("id")
		formStruct := models.StockItems{
			Orgid:  _orgId,
			Userid: user_id,
		}

		totalInventory, unit_of_measure := models.GetItemInventory(item_id, formStruct.Orgid)
		if totalInventory != "" {
			outputObj := &totalItemStock{
				ItemStock:   totalInventory,
				UnitMeasure: unit_of_measure,
			}
			b, _ := json.Marshal(outputObj)
			c.Ctx.Output.Body([]byte(b))
		}
	}
}

func (c *StockController) DeleteItems() {
	//var user_id string
	var _orgId string
	//var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		//user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		//username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		//user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		//username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}

	c.TplName = "stock/add-new-item.html"
	c.Data["Stock"] = 1
	c.Data["PageTitle"] = "Add Stock Item"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

	if c.Ctx.Input.Method() == "GET" {

		item_id := c.GetString("id")
		result := models.DeleteStockItems(item_id, _orgId)
		c.Ctx.Output.Body([]byte(result))
	}
}

func (c *StockController) AddInventory() {
	flash := beego.ReadFromRequest(&c.Controller)
	if ok := flash.Data["error"]; ok != "" {
		c.Data["FlasErrors"] = ok
	}
	if ok := flash.Data["notice"]; ok != "" {
		// Display error messages
		c.Data["FlashNotices"] = ok
	}
	c.TplName = "stock/add-inventory.html"
	c.Data["Stock"] = 1
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Add Inventory"

	var user_id string
	var _orgId string
	var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}

	formStruct2 := models.StockItems{
		Orgid:    _orgId,
		Userid:   user_id,
		Username: username,
	}
	retrieveditems := make(map[string]orm.Params)
	retrieveditems = models.GetAllItemsForDropdowns(_orgId)
	c.Data["ItemsRetrieved"] = retrieveditems

	retrievedsuppliers := make(map[string]orm.Params)
	retrievedsuppliers = models.GetSuppliers(formStruct2)
	c.Data["SupRetrieved"] = retrievedsuppliers

	retrievedlocation := make(map[string]orm.Params)
	retrievedlocation = models.GetFactory(formStruct2)
	c.Data["LocationRetrieved"] = retrievedlocation

	c.Data["username"] = username
	if c.Ctx.Input.Method() == "POST" {
		flash := beego.NewFlash()
		items := addInventoryValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

		// formStruct2 := models.StockInventory{
		// 	Orgid:    _orgId,
		// 	ItemCode: items.ItemCode,
		// 	ItemName: items.ItemName,
		// }
		// retrieved_qty := make(map[string]orm.Params)
		// retrieved_qty = models.GetInventoryQty(formStruct2)
		// c.Data["RetrievedQty"] = retrieved_qty

		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			return
		}

		formStruct := models.StockInventory{
			Orgid:        _orgId,
			Userid:       user_id,
			Username:     username,
			ItemName:     items.ItemName,
			ItemCode:     items.ItemCode,
			OrderDate:    items.OrderDate,
			DeliveryDate: items.DeliveryDate,
			Suppliers:    items.Suppliers,
			QtyPurchase:  items.QtyPurchase,
			TotalPrice:   items.TotalPrice,
			Location:     items.Location,
			Currency:     items.Currency,
		}
		insertStatus := models.AddInventory(formStruct)
		if insertStatus == 0 {
			flash.Error("Failed to add inventory")
			flash.Store(&c.Controller)
			return
		}
		c.Data["completeRegister"] = true
		flash.Notice("Inventory added successfully")
		flash.Store(&c.Controller)
		c.Redirect("/stock/add-inventory", 303)
	}
}

func (c *StockController) ViewInventory() {

	var user_id string
	var _orgId string
	var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}

	c.TplName = "stock/view-inventory.html"
	c.Data["Stock"] = 1
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "View Inventory"

	lowLevel := c.GetString("lowLevel")

	formStruct2 := models.StockItems{
		Orgid:    _orgId,
		Userid:   user_id,
		Username: username,
	}
	retrievedfac := make(map[string]orm.Params)
	retrievedfac = models.GetFactory(formStruct2)
	c.Data["Retrievedfac"] = retrievedfac

	retrieveditems := make(map[string]orm.Params)
	retrieveditems = models.GetAllItemsForDropdowns(_orgId)
	c.Data["ItemsRetrieved"] = retrieveditems

	retrievedGrp := make(map[string]orm.Params)
	retrievedGrp = models.GetGroup(formStruct2)
	c.Data["RetrievedGroup"] = retrievedGrp

	retrievedGrade := make(map[string]orm.Params)
	retrievedGrade = models.GetGrade(formStruct2)
	c.Data["RetrievedGrade"] = retrievedGrade

	formStruct1 := models.StockViewInventory{
		Orgid:    _orgId,
		Userid:   user_id,
		Username: username,
	}
	retrieved := make(map[string]orm.Params)

	if lowLevel == "1" {
		retrieved = models.GetLowLevelInventoryDetails(formStruct1)
		c.Data["Retrieved"] = retrieved
	} else {
		retrieved = models.GetInventoryDetails(formStruct1)
		c.Data["Retrieved"] = retrieved
	}
	c.Data["username"] = username
	if c.Ctx.Input.Method() == "POST" {
		flash := beego.NewFlash()
		items := viewInventoryValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			//flash.Store(&c.Controller)
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			return
		}
		formStruct3 := models.StockViewInventory{
			Orgid:    _orgId,
			ItemCode: items.ItemCode,
			ItemName: items.ItemName,
			Group:    items.Group,
			Grade:    items.Grade,
			Min:      items.Min,
			Max:      items.Max,
			Factory:  items.Factory,
		}
		retrieved_search := make(map[string]orm.Params)
		retrieved_search = models.NewSearchInventoryDetails(formStruct3)
		c.Data["Retrieved"] = retrieved_search

	}
}

func (c *StockController) ViewGRN() {
	//var user_id string
	var _orgId string
	var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		//user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		//user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}
	c.Data["username"] = username
	c.TplName = "stock/view-grn.html"
	c.Data["Stock"] = 1
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "View GRN"

	// formStruct2 := models.StockItems{
	// 	Orgid:    _orgId,
	// 	Userid:   user_id,
	// 	Username: username,
	// }
	retrieveditems := make(map[string]orm.Params)
	retrieveditems = models.GetAllItemsForDropdowns(_orgId)
	c.Data["ItemsRetrieved"] = retrieveditems

	retrievedfactories := make(map[string]orm.Params)
	retrievedfactories = models.GetOrgFactoriesIncludingDisabled(_orgId)
	c.Data["RetrievedFactories"] = retrievedfactories

	if c.Ctx.Input.Method() == "GET" {
		//item_code := c.GetString("itemCode")
		item_Id := c.GetString("itemId")
		factory_Id := c.GetString("factoryId")
		from_date := c.GetString("fromDate")
		to_date := c.GetString("toDate")

		formStruct := models.GRNUsage{
			Orgid:    _orgId,
			ItemName: item_Id,
			ItemID:   item_Id,
			Factory:  factory_Id,
			FromDate: from_date,
			ToDate:   to_date,
		}
		retrievedGRN := make(map[string]orm.Params)
		retrievedGRN = models.GetGRNView(formStruct)
		c.Data["RetrievedGRN"] = retrievedGRN // Bind the grid with the details item code that is passed.
		c.Data["FormItems"] = formStruct
		//return
	}

	// if c.Ctx.Input.Method() == "POST" {
	// 	flash := beego.NewFlash()
	// 	items := usageGRNValidate{}
	// 	if err := c.ParseForm(&items); err != nil {
	// 		flash.Error("Cannot parse form")
	// 		//flash.Store(&c.Controller)
	// 		return
	// 	}
	// 	c.Data["FormItems"] = items
	// 	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

	// 	valid := validation.Validation{}
	// 	if b, _ := valid.Valid(&items); !b {
	// 		c.Data["Errors"] = valid.ErrorsMap
	// 		c.Data["ErrorsPresent"] = true
	// 		return
	// 	}
	// 	//if items.SubmitButtonVal == "items" {
	// 	formStruct := models.GRNUsage{
	// 		Orgid:    _orgId,
	// 		Userid:   user_id,
	// 		Username: username,
	// 		//ItemName: items.ItemName,
	// 		//ItemCode: items.ItemCode,
	// 		ItemID: items.ItemName,
	// 	}
	// 	retrievedGRNUsage := make(map[string]orm.Params)
	// 	retrievedGRNUsage = models.GetGRNView(formStruct)
	// 	c.Data["RetrievedGRN"] = retrievedGRNUsage
	// 	//return
	// 	//}
	// 	flash.Store(&c.Controller)
	// 	//c.Redirect("/stock/view-inventory-usage", 303)
	// }
}

func (c *StockController) ViewGetGrn() { //Not used
	//var user_id string
	var _orgId string
	//var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		//user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		//username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		//user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		//username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}

	c.TplName = "stock/view-grn.html"
	c.Data["Stock"] = 1
	c.Data["PageTitle"] = "View Get GRN"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

	// formStruct2 := models.StockItems{
	// 	Orgid:    _orgId,
	// 	Userid:   user_id,
	// 	Username: username,
	// }
	retrieveditems := make(map[string]orm.Params)
	retrieveditems = models.GetAllItemsForDropdowns(_orgId)
	c.Data["ItemsRetrieved"] = retrieveditems

	if c.Ctx.Input.Method() == "GET" {
		//item_code := c.GetString("itemCode")
		item_Id := c.GetString("itemId")

		formStruct := models.GRNUsage{
			Orgid:    _orgId,
			ItemName: item_Id,
			ItemID:   item_Id,
		}
		retrievedGRN := make(map[string]orm.Params)
		retrievedGRN = models.GetGRNView(formStruct)
		c.Data["RetrievedGRN"] = retrievedGRN // Bind the grid with the details item code that is passed.
		c.Data["FormItems"] = formStruct
		//return
	}
	if c.Ctx.Input.Method() == "POST" {
		//	fmt.Println("*****Inside POST")
		flash := beego.NewFlash()
		items := usageGRNValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			//flash.Store(&c.Controller)
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			c.Data["ErrorsPresent"] = true
			return
		}
		if items.SubmitButtonVal == "items" {
			formStruct := models.GRNUsage{
				Orgid: _orgId,
				//	Userid:       user_id,
				//	Username:     username,
				ItemName: items.ItemName,
				//ItemCode: items.ItemCode,
				ItemID: items.ItemName,
			}
			retrievedGRN := make(map[string]orm.Params)
			retrievedGRN = models.GetGRNView(formStruct)
			c.Data["RetrievedGRN"] = retrievedGRN
			return
		}
		//flash.Store(&c.Controller)
	}
}

func (c *StockController) ViewInventoryUsage() {
	var user_id string
	var _orgId string
	var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}
	c.Data["username"] = username

	c.TplName = "stock/view-inventory-usage.html"
	c.Data["Stock"] = 1
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "View Inventory Usage"

	retrieveditems := make(map[string]orm.Params)
	retrieveditems = models.GetAllItemsForDropdowns(_orgId)
	c.Data["ItemsRetrieved"] = retrieveditems

	retrievedfactories := make(map[string]orm.Params)
	retrievedfactories = models.GetOrgFactoriesIncludingDisabled(_orgId)
	c.Data["RetrievedFactories"] = retrievedfactories

	if c.Ctx.Input.Method() == "GET" {
		//item_code := c.GetString("itemCode")
		item_Id := c.GetString("itemId")
		itemCode := c.GetString("item_code")
		fromTime := c.GetString("fromTime")
		toTime := c.GetString("toTime")
		factoryId := c.GetString("factoryId")

		if item_Id == "" && itemCode == "" && fromTime == "" && toTime == "" {
			return
		}

		formStruct := models.InventoryUsage{
			Orgid:    _orgId,
			Userid:   user_id,
			Username: username,
			ItemName: item_Id,
			Factory:  factoryId,
			ItemCode: itemCode,
			From:     fromTime,
			To:       toTime,
		}

		//retrievedInventoryUsage := make(map[string]orm.Params)
		retrievedInventoryUsage := models.GetInventoryUsage(formStruct)
		c.Data["RetrievedInventory"] = retrievedInventoryUsage
		//c.Redirect("/stock/view-inventory-usage", 303)
		c.Data["FormItems"] = formStruct
	}

	// if c.Ctx.Input.Method() == "POST" {
	// 	flash := beego.NewFlash()
	// 	items := usageInventoryValidate{}
	// 	if err := c.ParseForm(&items); err != nil {
	// 		flash.Error("Cannot parse form")
	// 		//flash.Store(&c.Controller)
	// 		return
	// 	}
	// 	c.Data["FormItems"] = items
	// 	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

	// 	valid := validation.Validation{}
	// 	if b, _ := valid.Valid(&items); !b {
	// 		c.Data["Errors"] = valid.ErrorsMap
	// 		c.Data["ErrorsPresent"] = true
	// 		return
	// 	}
	// 	if items.SubmitButtonVal == "items" {
	// 		formStruct := models.InventoryUsage{
	// 			Orgid:    _orgId,
	// 			Userid:   user_id,
	// 			Username: username,
	// 			ItemName: items.ItemName,
	// 			ItemCode: items.ItemCode,
	// 			From:     items.From,
	// 			To:       items.To,
	// 		}
	// 		retrievedInventoryUsage := make(map[string]orm.Params)
	// 		// retrievedInventoryUsage = models.GetInventoryUsage(formStruct)
	// 		retrievedInventoryUsage = models.GetViewInventoryUsage(formStruct)
	// 		c.Data["RetrievedInventory"] = retrievedInventoryUsage
	// 		return
	// 	}
	// 	//flash.Store(&c.Controller)
	// 	//c.Redirect("/stock/view-inventory-usage", 303)
	// }
}

func (c *StockController) ViewGetInventoryUsage() {
	//var user_id string
	var _orgId string
	//var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		//user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		//username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		//user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		//username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}

	c.TplName = "stock/view-inventory-usage.html"
	c.Data["Stock"] = 1
	c.Data["PageTitle"] = "View Get Inventory Usage"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

	// formStruct2 := models.StockItems{
	// 	Orgid:    _orgId,
	// 	Userid:   user_id,
	// 	Username: username,
	// }
	retrieveditems := make(map[string]orm.Params)
	retrieveditems = models.GetAllItemsForDropdowns(_orgId)
	c.Data["ItemsRetrieved"] = retrieveditems

	if c.Ctx.Input.Method() == "GET" {
		item_code := c.GetString("itemCode")
		//item_id := c.GetString("id")
		//fmt.Println("Inside Get view grn controller with ", item_id)

		retrieved := make(map[string]orm.Params)
		retrieved = models.GetViewGRNItems(item_code, _orgId)
		//fmt.Println("****Invoice=", retrieved)
		c.Data["Retrieved"] = retrieved

		//c.Redirect("/stock/view-inventory-usage", 303)
	}
	if c.Ctx.Input.Method() == "POST" {
		//	fmt.Println("*****Inside POST")
		flash := beego.NewFlash()
		items := usageInventoryValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			//flash.Store(&c.Controller)
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			c.Data["ErrorsPresent"] = true
			return
		}
		if items.SubmitButtonVal == "items" {
			formStruct := models.InventoryUsage{
				Orgid: _orgId,
				//Userid:       user_id,
				//Username:     username,
				ItemName: items.ItemName,
				ItemCode: items.ItemCode,
				From:     items.From,
				To:       items.To,
			}
			retrievedInventoryUsage := make(map[string]orm.Params)
			retrievedInventoryUsage = models.GetInventoryUsage(formStruct)
			c.Data["RetrievedInventory"] = retrievedInventoryUsage
			return
		}
		flash.Store(&c.Controller)
	}
}

func (c *StockController) StockAdjustment() {
	flash := beego.ReadFromRequest(&c.Controller)
	if ok := flash.Data["error"]; ok != "" {
		c.Data["FlasErrors"] = ok
	}
	if ok := flash.Data["notice"]; ok != "" {
		// Display error messages
		c.Data["FlashNotices"] = ok
	}
	var user_id string
	var _orgId string
	var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}
	c.Data["username"] = username

	formStruct2 := models.StockItems{
		Orgid:    _orgId,
		Userid:   user_id,
		Username: username,
	}
	retrievedfac := make(map[string]orm.Params)
	retrievedfac = models.GetFactory(formStruct2)
	c.Data["Retrievedfac"] = retrievedfac

	formStruct1 := models.StockAdjustmentRecord{
		Orgid:    _orgId,
		Userid:   user_id,
		Username: username,
	}
	retrieved := make(map[string]orm.Params)
	retrieved = models.GetAdjustedInventory(formStruct1)
	c.Data["Retrieved"] = retrieved

	factory_id := c.GetString("factoryId")
	item_id := c.GetString("itemId")

	if item_id != "" && factory_id != "" {
		c.Data["ErrorsPresent"] = true
		formStruct := AdjustmentValidate{
			//Orgid:       _orgId,
			Name:    item_id,
			Factory: factory_id,
		}
		// formStruct2 := models.StockAdjustmentRecord{
		// 	//Orgid:       _orgId,
		// 	ItemOrgFkid: item_id,
		// 	Location:    factory_id,
		// }
		c.Data["FormItems"] = formStruct
		//fmt.Println("***formStruct ", formStruct)
	}

	c.TplName = "stock/adjustment.html"
	c.Data["Stock"] = 1
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Stock Adjustments"

	if c.Ctx.Input.Method() == "POST" {
		flash := beego.NewFlash()
		items := AdjustmentValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

		formStruct2 := models.StockAdjustmentRecord{
			Orgid:       _orgId,
			ItemOrgFkid: items.Name,
			Location:    items.Factory,
		}
		// retrieved_items := make(map[string]orm.Params)
		// retrieved_items = models.GetItemsForAdjustment(formStruct2)
		// c.Data["RetrievedItems"] = retrieved_items

		retrieved_sum := make(map[string]orm.Params)
		retrieved_sum = models.GetSum(formStruct2)
		c.Data["RetrievedSum"] = retrieved_sum
		c.Data["ErrorsPresent"] = true

		if items.ADbutton == "adjust" {
			valid := validation.Validation{}
			if b, _ := valid.Valid(&items); !b {
				c.Data["Errors"] = valid.ErrorsMap
				c.Data["ErrorsPresent"] = true
				return
			}

			floatQtyObserved, _ := strconv.ParseFloat(items.QtyObserved, 64)
			if floatQtyObserved < 0 {
				flash.Error("Quantity observed cannot be negative")
				flash.Store(&c.Controller)
				c.Data["ErrorsPresent"] = true
				return
			}

			formStruct := models.StockAdjustmentRecord{
				Orgid:           _orgId,
				Userid:          user_id,
				Username:        username,
				ItemOrgFkid:     items.Name,
				DateInspection:  items.DateInspection,
				TotalQtyInStock: items.QtyRecord,
				QtyObserved:     items.QtyObserved,
				Remark:          items.Remark,
				Reason:          items.Reason,
				Location:        items.Factory,
			}

			stockData := make([]map[string]string, 0, 3)
			c.Ctx.Input.Bind(&stockData, "stock")

			insertStatus := models.MakeStockAdjustment(formStruct, stockData)
			if insertStatus == 0 {
				flash.Error("Adjustment failed")
				flash.Store(&c.Controller)
				c.Data["ErrorsPresent"] = true
				return
			}

			c.Data["completeRegister"] = true
			flash.Notice("Adjustment Complete")
			flash.Store(&c.Controller)
			c.Redirect("/stock/adjustment", 303)
		}
	}
}

func (c *StockController) EditItems() {
	flash := beego.ReadFromRequest(&c.Controller)
	if ok := flash.Data["error"]; ok != "" {
		c.Data["FlasErrors"] = ok
	}
	if ok := flash.Data["notice"]; ok != "" {
		// Display error messages
		c.Data["FlashNotices"] = ok
	}
	var user_id string
	var _orgId string
	var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}
	c.Data["username"] = username
	c.Data["OrgID"] = _orgId
	c.TplName = "stock/edit-items.html"
	c.Data["Stock"] = 1
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Edit Items"

	userDetailsStruct := models.StockItems{
		Orgid:    _orgId,
		Userid:   user_id,
		Username: username,
	}

	retrievedfactory := make(map[string]orm.Params)
	retrievedfactory = models.GetFactory(userDetailsStruct)
	c.Data["RetrievedFactory"] = retrievedfactory

	retrieveditems := make(map[string]orm.Params)
	retrieveditems = models.GetAllItemsForDropdowns(_orgId)
	c.Data["ItemsRetrieved"] = retrieveditems
	retrievedGrp := make(map[string]orm.Params)
	retrievedGrp = models.GetGroup(userDetailsStruct)
	c.Data["RetrievedGroup"] = retrievedGrp
	retrievedSubGrp := make(map[string]orm.Params)
	retrievedSubGrp = models.GetSubGroup(userDetailsStruct)
	c.Data["RetrievedSubGroup"] = retrievedSubGrp
	retrievedGrade := make(map[string]orm.Params)
	retrievedGrade = models.GetGrade(userDetailsStruct)
	c.Data["RetrievedGrade"] = retrievedGrade
	retrievedUOM := make(map[string]orm.Params)
	retrievedUOM = models.GetItemsUOM(userDetailsStruct)
	c.Data["RetrievedUOM"] = retrievedUOM

	if c.Ctx.Input.Method() == "GET" {
		item_name := c.GetString("itemId")
		//item_id := c.GetString("id")
		//fmt.Println("Inside Get view grn controller with ", item_id)
		flash := beego.NewFlash()
		items := editItemValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			return
		}
		_, retrievedBOM := models.GetBomItemsByProductID(item_name, _orgId)
		c.Data["RetrievedBOM"] = retrievedBOM
		c.Data["FormItems"] = models.GetItemDetailsByItemId(item_name, _orgId)
		var retrievedlocation []string
		maplocation := make(map[int]string)
		retrievedlocation = models.GetItemLocationDetailsByItemName(item_name, _orgId)

		for i := 0; i < len(retrievedlocation); i++ {
			maplocation[i] = retrievedlocation[i]
		}
		c.Data["RetrievedItemsLocation"] = maplocation

		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		return
	}
	if c.Ctx.Input.Method() == "POST" {
		var buttonSubmit string
		c.Ctx.Input.Bind(&buttonSubmit, "item-button")
		if buttonSubmit == "show" {
			flash := beego.NewFlash()
			items := editItemValidate{}
			if err := c.ParseForm(&items); err != nil {
				flash.Error("Cannot parse form")
				flash.Store(&c.Controller)
				return
			}
			retrievedBOM := make(map[string]orm.Params)
			retrievedBOM = models.GetBomItemsByName(items.ItemNameCheck, _orgId)
			c.Data["RetrievedBOM"] = retrievedBOM
			c.Data["FormItems"] = models.GetItemDetailsByItemId(items.ItemNameCheck, _orgId)
			var retrievedlocation []string
			maplocation := make(map[int]string)
			retrievedlocation = models.GetItemLocationDetailsByItemName(items.ItemNameCheck, _orgId)

			for i := 0; i < len(retrievedlocation); i++ {
				maplocation[i] = retrievedlocation[i]
			}
			c.Data["RetrievedItemsLocation"] = maplocation
			c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
			return
		}

		flash := beego.NewFlash()
		items := editItemValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			c.Data["ErrorsPresent"] = true
			return
		}

		checkItemStatus := models.CheckForExistingItemName1(items.ItemName, _orgId, items.ItemID)
		if checkItemStatus == 1 {
			c.Data["ItemNameExists"] = true
			return
		}

		checkCodeStatus := models.CheckForExistingItemUniqueCode1(items.ItemCode, _orgId, items.ItemID)
		if checkCodeStatus == 1 {
			c.Data["ItemCodeExists"] = true
			return
		}

		checkInvoiceNarationStatus := models.CheckForExistingInvoiceNaration1(items.InvoiceNarration, _orgId, items.ItemID)
		if checkInvoiceNarationStatus == 1 {
			c.Data["InvoiceNarationExists"] = true
			return
		}

		formStruct := models.StockItems{
			Orgid:            _orgId,
			Userid:           user_id,
			Username:         username,
			ItemName:         strings.Title(items.ItemName),
			Group:            strings.ToUpper(items.Group),
			SubGroup:         strings.ToUpper(items.SubGroup),
			ItemGrade:        strings.ToUpper(items.ItemGrade),
			UOM:              strings.Title(items.UOM),
			ItemCode:         strings.ToUpper(items.ItemCode),
			InvoiceNarration: strings.Title(items.InvoiceNarration),
			BinLocation:      items.BinLocation,
			MinOrderQty:      items.MinOrderQty,
			LeadTimeDays:     items.LeadTimeDays,
			MinStockTrigger:  items.MinStockTrigger,
			MaxStorageQty:    items.MaxStorageQty,
			GSTRate:          items.GSTRate,
			CessRate:         items.CessRate,
			HSNSAC:           items.HSNSAC,
			GeoLocation1:     items.GeoLocation1,
			Status:           items.Status,
			IsProduced:       items.IsProduced,
			NonConsumable:    items.NonConsumable,
			ItemID:           items.ItemID,
			ProductionStatus: items.ProductionStatus,
			ShelfLife:        items.ShelfLife,
		}
		//fmt.Println("Status is",result["status"])
		insertStatus := models.UpdateStockItems(formStruct)
		if insertStatus == 0 {
			flash.Error("Form Not Inserted")
			flash.Store(&c.Controller)
			return
		}
		c.Data["completeRegister"] = true
		flash.Notice("Item/Product Updated")
		flash.Store(&c.Controller)
		c.Redirect("/stock/edit-items", 303)
	}
}

func (c *StockController) GetEditDetailsAjax() {
	//c.Data["username"] = username
	c.TplName = "stock/edit-items.html"
	c.Data["Stock"] = 1
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Edit Items"

	if c.Ctx.Input.Method() == "GET" {

		_orgId := c.GetString("id")

		uom := models.GetEditUOM(_orgId)
		grade := models.GetEditGrades(_orgId)
		group := models.GetEditGroups(_orgId)
		subgroup := models.GetEditSubgroups(_orgId)

		outputObj := &editstruct{
			UOM:      uom,
			Grade:    grade,
			Group:    group,
			Subgroup: subgroup,
		}
		b, _ := json.Marshal(outputObj)
		c.Ctx.Output.Body([]byte(b))
	}
}

func (c *StockController) GetFactoryItems() {
	//var user_id string
	var _orgId string
	//var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		//user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		//username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		//user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		//username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}

	//c.Data["username"] = username
	//c.TplName = "stock/edit-items.html"
	c.Data["Stock"] = 1
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Get Items/Products as per geo_location"

	if c.Ctx.Input.Method() == "GET" {

		factoryId := c.GetString("factory_id")

		itemId, itemCode, itemUom, invoiceNarration := models.GetItemsByFactory(factoryId, _orgId)
		// grade := models.GetEditGrades(_orgId)
		// group := models.GetEditGroups(_orgId)
		// subgroup := models.GetEditSubgroups(_orgId)

		outputObj := &getItemStruct{
			ItemId:           itemId,
			ItemCode:         itemCode,
			ItemUom:          itemUom,
			InvoiceNarration: invoiceNarration,
			// Group:    group,
			// Subgroup: subgroup,
		}
		b, _ := json.Marshal(outputObj)
		c.Ctx.Output.Body([]byte(b))
	}
}

func (c *StockController) GetItemCode() {
	//var user_id string
	var _orgId string
	var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		//user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		//user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}
	c.Data["username"] = username

	item_id := c.GetString("item")

	item_code := models.GetItemCode(item_id, _orgId)

	outputObj := &inventorystruct{
		ItemCode: item_code,
	}
	b, _ := json.Marshal(outputObj)
	c.Ctx.Output.Body([]byte(b))

}

func (c *StockController) GetInventoryExistingQty() {
	//var user_id string
	var _orgId string
	//var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		//user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		//username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		//user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		//username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}

	item_id := c.GetString("item")
	factory_id := c.GetString("factoryId")

	existing_qty := models.GetInventoryQty(item_id, factory_id, _orgId)

	outputObj := &inventorystruct{
		ExistingQty: existing_qty,
	}
	b, _ := json.Marshal(outputObj)
	c.Ctx.Output.Body([]byte(b))
}

func (c *StockController) GetStockToAdjust() {
	//var user_id string
	var _orgId string
	//var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		//user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		//username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		//user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		//username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}

	//c.Data["username"] = username
	//c.TplName = "stock/edit-items.html"
	c.Data["Stock"] = 1
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Get Stock details to Adjust"

	if c.Ctx.Input.Method() == "GET" {

		itemId := c.GetString("item_id")
		factoryId := c.GetString("factory_id")

		stockId, quantity, rate, startDate, purchaseOrProd, grnNo, addedQty, isEditable, thePlaceholder := models.GetStockToAdjust(itemId, factoryId, _orgId)

		formStruct2 := models.StockAdjustmentRecord{
			Orgid:       _orgId,
			ItemOrgFkid: itemId,
			Location:    factoryId,
		}
		retrieved_sum := models.GetTotalStockQty(formStruct2)

		outputObj := &getStockStruct{
			StockId:        stockId,
			Quantity:       quantity,
			Rate:           rate,
			StartDate:      startDate,
			PurchaseOrProd: purchaseOrProd,
			GrnNo:          grnNo,
			AddedQty:       addedQty,
			IsEditable:     isEditable,
			ThePlaceholder: thePlaceholder,
			TotalStockQty:  retrieved_sum,
		}
		b, _ := json.Marshal(outputObj)
		c.Ctx.Output.Body([]byte(b))
	}
}

func (c *StockController) AddBOM() {

	var _orgId string
	var user_id string
	var username string

	sessionVar := c.StartSession()
	if sessionVar != nil {
		user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}
	flash := beego.NewFlash()
	if c.Ctx.Input.Method() == "GET" {
		c.TplName = "stock/add-bom.html"
		c.Data["Stock"] = 1
		c.Data["PageTitle"] = "Bill of Material"
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

		productId := c.GetString("productid")
		productName := c.GetString("productName")

		c.Data["ItemCode"] = productId
		c.Data["ItemName"] = productName

		formStruct5 := models.Items{
			Orgid:    _orgId,
			Userid:   user_id,
			Username: username,
		}
		retrievedb := make(map[string]orm.Params)
		retrievedb = models.GetBOIActiveItems(formStruct5)
		c.Data["Retrievedb"] = retrievedb
	}

	if c.Ctx.Input.Method() == "POST" {
		items := bomValidate1{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {

			c.Data["Errors"] = valid.ErrorsMap
			c.Data["ErrorsPresent"] = true
			return
		}

		bomformStruct := models.Items{
			Orgid:          _orgId,
			Userid:         user_id,
			Username:       username,
			BomProductName: items.ItemName,
			BomProductCode: CodeToDisplay,
			ItemCodeType:   ItemCodeMethod,
		}

		bomitems := make([]map[string]string, 0, 3)
		c.Ctx.Input.Bind(&bomitems, "group-a")

		//bominsertStatus := models.BillofMaterialNew(bomformStruct)
		bominsertStatus := models.BillofMaterialRepeater(bomformStruct, bomitems)
		if bominsertStatus == 0 {
			return
		}

		c.Data["completeRegister"] = true
		flash.Notice("Item/Product Inserted")
		flash.Store(&c.Controller)

		c.Redirect("/stock/add-new-items", 303)
	}

}

func (c *StockController) EditBOM() {

	var _orgId string
	var user_id string
	var username string

	sessionVar := c.StartSession()
	if sessionVar != nil {
		user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}
	flash := beego.NewFlash()
	c.TplName = "stock/edit-bom.html"
	c.Data["Stock"] = 1
	c.Data["PageTitle"] = "Bill of Material"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	if c.Ctx.Input.Method() == "GET" {

		formStruct5 := models.Items{
			Orgid:    _orgId,
			Userid:   user_id,
			Username: username,
		}
		retrievedb := make(map[string]orm.Params)
		retrievedb = models.GetBOIActiveItems(formStruct5)
		c.Data["Retrievedb"] = retrievedb

		productId := c.GetString("productid")

		productCode := models.GetItemCode(productId, _orgId)
		productName := models.GetItemName(productId, _orgId)

		c.Data["ItemCode"] = productCode
		c.Data["ItemName"] = productName
		c.Data["ProductID"] = productId

		editFlag := "1"
		c.Data["EditFlag"] = editFlag

		if productCode == "" {
			c.Data["EditFlag"] = "88"
			flash.Error("Material not found.")
			flash.Store(&c.Controller)
			return
		}

		// get BOM of the Product using primary key as parameter

		errorStatus, bomCount := models.CheckExistingBOMByProductId(productId, _orgId)
		if errorStatus == 1 {
			editFlag = "0"
			c.Data["EditFlag"] = editFlag
			flash.Error("Cannot fetch BOM details of the material.")
			flash.Store(&c.Controller)
			return
		} else if errorStatus == 0 && bomCount > 0 {
			editFlag = "99"
			c.Data["EditFlag"] = editFlag
			flash.Error("BOM already added for the Material and cannot be edited now.")
			flash.Store(&c.Controller)
			return
		}

	}

	if c.Ctx.Input.Method() == "POST" {
		items := bomValidate1{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			fmt.Println("Validation error on items=", b)
			flash.Error("Cannot parse material")
			flash.Store(&c.Controller)
			c.Data["Errors"] = valid.ErrorsMap
			c.Data["ErrorsPresent"] = true
			return
		}

		bomitems := make([]map[string]string, 0, 3)
		c.Ctx.Input.Bind(&bomitems, "group-a")

		//bominsertStatus := models.BillofMaterialNew(bomformStruct)
		bominsertStatus := models.InsertBOMByProductID(items.ProductPKID, _orgId, bomitems)
		if bominsertStatus == 0 {
			flash.Error("Error inserting BOM")
			flash.Store(&c.Controller)
			return
		}

		c.Data["completeRegister"] = true
		flash.Notice("BOM Edited.")
		flash.Store(&c.Controller)

		c.Redirect("/stock/add-new-items", 303)
	}

}
