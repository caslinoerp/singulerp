package controllers

import (
	"html/template"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/validation"
)

type ReportsController struct {
	BaseController
}

type poReportValidate struct{
	Supplier             string `form:"supplier" valid:"Required"`
	PurchaseOrder        string `form:"invoice_narration" valid:"Required"`
	Date                 string `form:"range" valid:"Required"`
	Last30               string `form:"last_30" valid:"Required"`
	Cost                 string `form:"cost" valid:"Required"`	
}

type grnReportValidate struct{
	Supplier1            string `form:"supplier1" valid:"Required"`
	Items                string `form:"items" valid:"Required"`
	ResultItem           string `form:"resulting_item" valid:"Required"`
	DRSR                 string `form:"dr_sr" valid:"Required"`
	L30DSR               string `form:"l30d_sr" valid:"Required"`
	Status               string `form:"status" valid:"Required"`	
}

type serviceReportValidate struct{
	Contractor           string `form:"contractor" valid:"Required"`
	ItemsSR              string `form:"items_sr" valid:"Required"`
	ResultingItem 	     string `form:"resulting_item"`
	PONo                 string `form:"po_no" valid:"Required"`
	DR                   string `form:"dr4" valid:"Required"`
	L3D4                 string `form:"l3d4" valid:"Required"`	
}

type qaReportValidate struct{
	Supplier2            string `form:"supplier2" valid:"Required"`
	Items2               string `form:"items2" valid:"Required"`
	PO2                  string `form:"po2" valid:"Required"`
	DR                   string `form:"dr" valid:"Required"`
	L3D                  string `form:"l3d" valid:"Required"`
	CR2                  string `form:"cr2" valid:"Required"`	
}

type customerorderValidate struct{
	Cust1               string `form:"customer1" valid:"Required"`
	Cust2               string `form:"customer2" valid:"Required"`
	}

type customercomplaintValidate struct{
	Cust3               string `form:"customer3" valid:"Required"`
	Cust4               string `form:"customer4" valid:"Required"`
	}

type customerhistoryValidate struct{
	Cust5               string `form:"customer5" valid:"Required"`
	Cust6               string `form:"customer6" valid:"Required"`
	}


func (c *ReportsController) GRNReport() {
	c.TplName = "reports/grn-report.html"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "GRN"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	//generic code to handle post request and validation
	if c.Ctx.Input.Method() == "POST" {
		flash := beego.NewFlash()
		items := grnReportValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			return
		}
	}
}

func (c *ReportsController) PoReport() {
	c.TplName = "reports/po-report.html"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Purchases / PO"
    c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	//generic code to handle post request and validation
	if c.Ctx.Input.Method() == "POST" {
		flash := beego.NewFlash()
		items := poReportValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			return
		}
	}

}
func (c *ReportsController) QaReport() {
	c.TplName = "reports/qa-report.html"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Purchases / QA"
    c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	//generic code to handle post request and validation
	if c.Ctx.Input.Method() == "POST" {
		flash := beego.NewFlash()
		items := qaReportValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			return
		}
	}

}

func (c *ReportsController) ServiceContractReport() {
	c.TplName = "reports/service-contract-report.html"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Service Contract Report"
    c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	//generic code to handle post request and validation
	if c.Ctx.Input.Method() == "POST" {
		flash := beego.NewFlash()
		items := serviceReportValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			return
		}
	}

}

func (c *ReportsController) CustomerComplaints() {
	c.TplName = "reports/complaints-reports.html"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Associate With Products"

	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

	//generic code to handle post request and validation
	if c.Ctx.Input.Method() == "POST" {
		flash := beego.NewFlash()
		items := customercomplaintValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			return
		}
	}
}


func (c *ReportsController) CustomerHistory() {
	c.TplName = "reports/history-reports.html"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Customer History Reports"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	//generic code to handle post request and validation
	if c.Ctx.Input.Method() == "POST" {
		flash := beego.NewFlash()
		items := customerhistoryValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			return
		}
	}
}

func (c *ReportsController) CustomerOrders() {
	c.TplName = "reports/order-reports.html"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Customer Order Reports"

	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

	//generic code to handle post request and validation
	if c.Ctx.Input.Method() == "POST" {
		flash := beego.NewFlash()
		items := customerorderValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			return
		}
	}
}