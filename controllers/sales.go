package controllers

import (
	"encoding/json"
	"fmt"
	"html/template"
	"strconv"
	//"time"

	"bitbucket.org/caslinoerp/singulerp/models"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"github.com/astaxie/beego/validation"
	"github.com/microcosm-cc/bluemonday"
	"github.com/russross/blackfriday"
)

type SalesController struct {
	BaseController
}

type SalesOrderValidate struct {
	OrderId          string `form:"order_id"`
	OrderNo          string `form:"order_no" valid:"Required"`
	OrderDate        string `form:"order_date" valid:"Required"`
	CustomerName     string `form:"customer_name" valid:"Required"`
	JobType          string `form:"job_type"`
	DeliveryDate     string `form:"delivery_date" valid:"Required"`
	DLocation        string `form:"d_location" valid:"Required"`
	OLocation        string `form:"o_location" valid:"Required"`
	TaxRegime        string `form:"tax_regime" valid:"Required"`
	Transporter      string `form:"transporter"`
	Vehicle          string `form:"vehicle"`
	ItemName         string `form:"item_name" `
	Quantity         string `form:"quantity" `
	Price            string `form:"price"`
	Discount         string `form:"discount"`
	ItemName1        string `form:"item_name1"`
	Quantity1        string `form:"quantity1"`
	Price1           string `form:"price1"`
	Discount1        string `form:"discount1"`
	ItemName2        string `form:"item_name2"`
	Quantity2        string `form:"quantity2"`
	Price2           string `form:"price2"`
	Discount2        string `form:"discount2"`
	ItemName3        string `form:"item_name3"`
	Quantity3        string `form:"quantity3"`
	Price3           string `form:"price3"`
	Discount3        string `form:"discount3"`
	ItemName4        string `form:"item_name4"`
	Quantity4        string `form:"quantity4"`
	Price4           string `form:"price4"`
	Discount4        string `form:"discount4"`
	ItemName5        string `form:"item_name5"`
	Quantity5        string `form:"quantity5"`
	Price5           string `form:"price5"`
	Discount5        string `form:"discount5"`
	ItemName6        string `form:"item_name6"`
	Quantity6        string `form:"quantity6"`
	Price6           string `form:"price6"`
	Discount6        string `form:"discount6"`
	ExciseCharges    string `form:"echarges" `
	TransportCharges string `form:"tcharges" `
	Form30           string `form:"form30"`
	FormC            string `form:"formc"`
	DispatchType     string `form:"dispatch_type"`
	PaymentTerms     string `form:"payment_terms"`

	Remarks            string `form:"remarks"`
	TotalQuantity      string `form:"tquantity"`
	TotalPrice         string `form:"tprice"`
	TotalDiscount      string `form:"tdiscount"`
	TotalSalesPrice    string `form:"sum_total_invoice_value" valid:"Required"` /* Total Invoice Value */
	TotalAfterDiscount string `form:"tadiscount"`
	TotalAfterExcise   string `form:"total_after_excise"`
	TotalAfterTaxes    string `form:"sum_total_after_taxes"`
	CST                string `form:"cst"`
	VAT                string `form:"vat"`
	SaveButton         string `form:"assoc-button"`
	BAddress           string `form:"billing_address"`
	TariffNo           string `form:"tariff_no"`

	TotalTaxableValue     string `form:"sum_total_taxable_value"`
	TotalTaxCess          string `form:"sum_total_tax_cess"`
	TotalFreightPackaging string `form:"sum_freight_packaging_others"`
	AssignBatch           string `form:"assignBatch"`
}

type newStockTransferValidate struct {
	OrderNo            string `form:"order_no" valid:"Required"`
	OrderDate          string `form:"order_date" valid:"Required"`
	CustomerName       string `form:"customer_name" valid:"Required"`
	JobType            string `form:"job_type"`
	DeliveryDate       string `form:"delivery_date" valid:"Required"`
	TotalQuantity      string `form:"tquantity"`
	TotalPrice         string `form:"tprice"`
	TotalDiscount      string `form:"tdiscount"`
	TotalAfterDiscount string `form:"tadiscount"`
	ExciseCharges      string `form:"echarges" `
	TotalAfterExcise   string `form:"total_after_excise"`
	OctroiCharges      string `form:"ocharges" `
	TotalAfterOctroi   string `form:"total_after_octroi"`
	TotalAfterTaxes    string `form:"tataxes"`
	TransportCharges   string `form:"tcharges" `
	DispatchType       string `form:"dispatch_type"`
	PaymentTerms       string `form:"payment_terms"`
	Remarks            string `form:"remarks"`
	TotalSalesPrice    string `form:"totalsalesprice"`
	TariffNo           string `form:"tariff_no"`
	SaveButton         string `form:"assoc-button"`
}

type mystruct3 struct {
	Items        []string `json:"items"`
	Quantity     []string `json:"quantity"`
	Rate         []string `json:"rate"`
	DiscountRate []string `json:"discount_rate"`
	DeliveryDate []string `json:"delivery_date"`
	UOM          []string `json:"uom"`

	DiscountValue       []string `json:"discount_value"`
	HsnSac              []string `json:"hsn_sac"`
	TaxableAmount       []string `json:"taxable_amount"`
	GstRate             []string `json:"gst_rate"`
	IgstRate            []string `json:"igst_rate"`
	IgstAmount          []string `json:"igst_amount"`
	CgstRate            []string `json:"cgst_rate"`
	CgstAmount          []string `json:"cgst_amount"`
	SgstRate            []string `json:"sgst_rate"`
	SgstAmount          []string `json:"sgst_amount"`
	CessRate            []string `json:"cess_rate"`
	CessAmount          []string `json:"cess_amount"`
	TotalTaxCess        []string `json:"total_tax_cess"`
	TotalAfterTax       []string `json:"total_after_tax"`
	Freight             []string `json:"freight"`
	Insurance           []string `json:"insurance"`
	PackagingForwarding []string `json:"packaging_forwarding"`
	Others              []string `json:"others"`
	TotalOtherCharges   []string `json:"total_other_charges"`
	TaxRegime           string   `json:"tax_regime"`
	TotalOfDiscount     float64  `json:"total_of_discount"`
	TotalOfCGST         float64  `json:"total_of_cgst"`
	TotalOfSGST         float64  `json:"total_of_sgst"`
	Status              string   `json:"status"`
}

type ratestruct2 struct {
	HSN               string `json:"hsn"`
	UOM               string `json:"uom"`
	Rate              string `json:"rate"`
	GST               string `json:"gst"`
	CustomerNarration string `json:"customer_narration"`
	AvailableQty      string `json:"available_qty"`
	Status            string `json:"status"`
}

type InventoryStatus struct {
	InventoryOk string `json:"inventory_Ok"`
}

type itemstruct struct {
	ItemId           []string `json:"item_id"`
	InvoiceNarration []string `json:"invoice_narration"`
	Name             []string `json:"name"`
	UOM              []string `json:"uom"`
}

type locationstruct struct {
	Address    []string `json:"address_line"`
	LocationId []string `json:"location_id"`
}

type vehiclestruct struct {
	VehicleId      []string `json:"vehicleId"`
	DriverName     []string `json:"driverName"`
	RegistrationNo []string `json:"registrationNo"`
}

type cststruct2 struct {
	CST    string `json:"cst"`
	Status string `json:"status"`
}

type Search_SalesOrderDetails struct {
	CustomerName  string `form:"CustomerName"`
	FromOrderDate string `form:"so_FromDate"`
	ToOrderDate   string `form:"so_ToDate"`
}

type deliveryValidate struct {
	SoNo       string `form:"so_no" valid:"Required"`
	DispatchNo string `form:"dispatch_no"`
	//ItemsArray []GrnItems
	SoId              string `form:"so_id"`
	Customer          string `form:"customer" valid:"Required"`
	DeliveryDate      string `form:"delivery_date" valid:"Required"`
	TotalCost         string `form:"total_cost" valid:"Required"`
	DeliveryLocation  string `form:"delivery_location" valid:"Required"`
	DeliveredDate     string `form:"delivered_date" valid:"Required"`
	CustomerInvoice   string `form:"customer_invoice" valid:"Required"`
	CustomerInvoiceDt string `form:"customer_invoice_dt" `
	CustomerDcNo      string `form:"customer_dc_no" `
	CustomerDcDt      string `form:"customer_dc_dt" `
	LrNo              string `form:"lr_no" `
	LrDt              string `form:"lr_dt" `
	Transporter       string `form:"transporter" `
	Vehicle           string `form:"vehicle" `
	AddNote           string `form:"add_note"`
	ViewButton        string `form:"assoc-button"`
}

func (c *SalesController) CreateNewSalesOrder() {
	flash := beego.ReadFromRequest(&c.Controller)
	if ok := flash.Data["error"]; ok != "" {
		c.Data["FlasErrors"] = ok
	}
	if ok := flash.Data["notice"]; ok != "" {
		// Display error messages
		c.Data["FlashNotices"] = ok
	}
	var user_id string
	var _orgId string
	var username string
	var localetimezone string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
		localetimezone = fmt.Sprintf("%s", sessionVar.Get("LocaleTimeZone"))
	} else {
		user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
		localetimezone = fmt.Sprintf("%s", c.Ctx.Input.GetData("LocaleTimeZone"))
	}
	c.Data["username"] = username
	formStruct5 := models.Sales{
		Orgid:    _orgId,
		Userid:   user_id,
		Username: username,
	}

	retrievedOrg := make(map[string]orm.Params)
	retrievedOrg = models.GetOrgDetails(_orgId)
	c.Data["OrgRetrieved"] = retrievedOrg
	//fmt.Println("***OrgRetrieved= ", retrievedOrg)

	var retrievedSOtc string
	retrievedSOtc = models.GetSOtc(_orgId)
	mySlice := []byte(retrievedSOtc)
	output := blackfriday.MarkdownCommon(mySlice)
	t := bluemonday.UGCPolicy().SanitizeBytes(output)
	s := string(t)
	c.Data["RetrievedSOtc"] = template.HTML(s)

	retrievedCustomer := make(map[string]orm.Params)
	retrievedCustomer = models.GetCustomersNames(formStruct5)
	c.Data["CustRetrieved"] = retrievedCustomer

	retrievedg := make(map[string]orm.Params)
	retrievedg = models.GetAllItemsForDropdowns(_orgId)
	c.Data["Retrievedg"] = retrievedg

	retrievedtransporters := make(map[string]orm.Params)
	retrievedtransporters = models.GetTransportersforDropdown(_orgId)
	c.Data["TransporterRetrieved"] = retrievedtransporters

	formStruct1 := models.Sales{
		Orgid:    _orgId,
		Userid:   user_id,
		Username: username,
	}

	newGenCode, newCode := c.GetGeneratedCode(_orgId, 6, localetimezone)
	c.Data["genCode"] = newGenCode

	delayedsales := c.GetString("delayedsales")

	retrieved := make(map[string]orm.Params)
	if delayedsales == "1" {
		retrieved = models.GetDelayedSalesOrders(formStruct1)
		c.Data["Retrieved"] = retrieved
	} else {
		retrieved = models.GetSales(formStruct1)
		c.Data["Retrieved"] = retrieved
	}

	retrievedfactories := make(map[string]orm.Params)
	retrievedfactories = models.GetOrgFactories(_orgId)
	c.Data["RetrievedFactories"] = retrievedfactories

	/*var retrieved1 string
	retrieved1=models.GetOrderCode(formStruct1)
	c.Data["CodeRetrieved"] = retrieved1*/

	c.TplName = "sales/new-list.html"
	c.Data["Sales"] = 1
	c.Data["TaxValue"] = c.GetString("tax")
	if c.GetString("tax") != "" {
		c.Data["ErrorsPresent"] = true
	}
	//tax_value := c.GetString("tax")
	c.Data["PageTitle"] = "New/List"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	//generic code to handle post request and validation
	if c.Ctx.Input.Method() == "POST" {

		if c.GetString("SalesOrderSearchmethod") == "SalesOrder_Search" {
			var buttonSearch string
			c.Ctx.Input.Bind(&buttonSearch, "SalesOrder-search")
			if buttonSearch == "SearchSalesOrder" {
				flash := beego.NewFlash()
				items := Search_SalesOrderDetails{}
				if err := c.ParseForm(&items); err != nil {
					flash.Error("Cannot parse form")
					//flash.Store(&c.Controller)
					return
				}
				retrieved := make(map[string]orm.Params)
				retrieved = models.SearchSOByOrderDetails(items.CustomerName, items.FromOrderDate, items.ToOrderDate, _orgId)
				c.Data["SearchItems"] = items
				c.Data["Retrieved"] = retrieved
				c.TplName = "sales/new-list.html"
				c.Data["PageTitle"] = "New/List"
				c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
				return
			}
			c.Ctx.Input.Bind(&buttonSearch, "Reset-SalesOrderSearch")
			if buttonSearch == "ResetSalesOrderSearch" {
				c.Redirect("/sales/new-list", 303)
				return
			}
		}

		flash := beego.NewFlash()
		items := SalesOrderValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			c.Data["ErrorsPresent"] = true
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			c.Data["ErrorsPresent"] = true
			return
		}

		assignFlag := "0"
		if items.AssignBatch == "on" {
			assignFlag = "1"
		}

		formStruct := models.Sales{
			Orgid:                 _orgId,
			Userid:                user_id,
			Username:              username,
			OrderNo:               newCode,
			OrderDate:             items.OrderDate,
			CustomerName:          items.CustomerName,
			JobType:               items.JobType,
			DeliveryDate:          items.DeliveryDate,
			DLocation:             items.DLocation,
			OLocation:             items.OLocation,
			Transporter:           items.Transporter,
			Vehicle:               items.Vehicle,
			TaxRegime:             items.TaxRegime,
			ExciseCharges:         items.ExciseCharges,
			TransportCharges:      items.TransportCharges,
			Form30:                items.Form30,
			FormC:                 items.FormC,
			DispatchType:          items.DispatchType,
			PaymentTerms:          items.PaymentTerms,
			Remarks:               items.Remarks,
			TotalDiscount:         items.TotalDiscount,
			TotalPrice:            items.TotalPrice,
			TotalQuantity:         items.TotalQuantity,
			TotalAfterDiscount:    items.TotalAfterDiscount,
			TotalAfterExcise:      items.TotalAfterExcise,
			TotalAfterTaxes:       items.TotalAfterTaxes,
			CST:                   items.CST,
			VAT:                   items.VAT,
			TotalSalesPrice:       items.TotalSalesPrice,
			BAddress:              items.BAddress,
			TariffNo:              items.TariffNo,
			TotalTaxableValue:     items.TotalTaxableValue,
			TotalTaxCess:          items.TotalTaxCess,
			TotalFreightPackaging: items.TotalFreightPackaging,
			NewGenCode:            newGenCode,
			ManuallyAssignBatch:   assignFlag,
		}
		uniqueStatus := models.UniqueCheck2(formStruct)
		if uniqueStatus == 0 {
			flash.Error("Product Code not unique")
			flash.Store(&c.Controller)
			c.Data["ErrorsPresent"] = true
			return
		}

		salesitems := make([]map[string]string, 0, 3)
		if items.TaxRegime == "1" {
			c.Ctx.Input.Bind(&salesitems, "intra-state")
		} else if items.TaxRegime == "2" {
			c.Ctx.Input.Bind(&salesitems, "inter-state")
		} else if items.TaxRegime == "3" {
			c.Ctx.Input.Bind(&salesitems, "export-state")
		}

		if items.SaveButton == "draft" {

			//NO CHECK FOR INVENTORY IS DONE IN CASE OF DRAFT

			insertStatus := models.CreateNewSalesOrder(formStruct, salesitems, models.GetSalesOrderStatusCode(models.SaveDraft))
			if insertStatus == 0 {
				flash.Error("An error occured while saving. Please try again.")
				flash.Store(&c.Controller)
				c.Data["ErrorsPresent"] = true
				return
			}

			flash.Notice("Sales order added to draft.")
			flash.Store(&c.Controller)
		}

		if items.SaveButton == "approval" {

			// CHECK INVENTORY AND GET THE FLAG
			isInventoryOk := models.CheckInventoryStatusForAllItems(salesitems, formStruct.OLocation, formStruct.Orgid)

			insertStatus := 1

			if isInventoryOk == "0" {
				insertStatus = models.CreateNewSalesOrder(formStruct, salesitems, models.GetSalesOrderStatusCode(models.SendForApprovalNoInventory)) //approval and no inventory
			} else {
				insertStatus = models.CreateNewSalesOrder(formStruct, salesitems, models.GetSalesOrderStatusCode(models.SendForApprovalWithInventory)) //approval and inventory present
			}

			if insertStatus == 0 {
				flash.Error("An error occured while saving. Please try again.")
				flash.Store(&c.Controller)
				c.Data["ErrorsPresent"] = true
				return
			}

			flash.Notice("Sales order sent for approval.")
			flash.Store(&c.Controller)

			if assignFlag == "1" {
				salesOrderFkId := models.GetSalesOrderFkIDFromOrgidOrderNo(_orgId, newCode)
				SalesOrderFkId := strconv.Itoa(salesOrderFkId)
				c.Redirect("/sales/sales-orders-assign-batches?salesOrderNo="+newCode+"&Salesorderfkid="+SalesOrderFkId, 303)
			}
		}

		if items.SaveButton == "approved" {

			// CHECK INVENTORY AND GET THE FLAG
			isInventoryOk := models.CheckInventoryStatusForAllItems(salesitems, formStruct.OLocation, formStruct.Orgid)

			insertStatus := 1

			if isInventoryOk == "0" {
				insertStatus = models.CreateNewSalesOrder(formStruct, salesitems, models.GetSalesOrderStatusCode(models.ApproveNoInventory)) //approval and no inventory
			} else {
				insertStatus = models.CreateNewSalesOrder(formStruct, salesitems, models.GetSalesOrderStatusCode(models.ApproveWithInventory)) //approval and inventory present

				if assignFlag == "0" {
					//BLOCK ITEMS IF APPROVED AND INVENTORY IS PRESENT
					salesOrderFkId := models.GetSalesOrderFkIDFromOrgidOrderNo(_orgId, newCode)

					for _, it := range salesitems {

						itemid := it["item_name"]
						itemID, _ := strconv.ParseInt(itemid, 10, 64)

						reqQty, _ := strconv.ParseFloat(it["qty"], 64)

						itemDetails := models.ItemDetailsForOrder{
							OrgId:             formStruct.Orgid,
							ItemId:            itemID,
							RequiredQuantity:  reqQty,
							Location:          formStruct.OLocation,
							BlockedBy:         3,
							ConsumedTableFkId: int64(salesOrderFkId),
						}

						insertQueryStatus := models.BlockItemsBeforeConsumption(itemDetails)
						if insertQueryStatus == 0 {
							flash.Error("An error occured while approving. Please try again.")
							flash.Store(&c.Controller)
							c.Data["ErrorsPresent"] = true
							c.Redirect("/sales/new-list", 303)
						}
					}
				}
				//END OF BLOCKING LOGIC
			}

			if insertStatus == 0 {
				flash.Error("An error occured while saving. Please try again.")
				flash.Store(&c.Controller)
				c.Data["ErrorsPresent"] = true
				c.Redirect("/sales/new-list", 303)
			}

			flash.Notice("Sales order approved.")
			flash.Store(&c.Controller)

			if assignFlag == "1" {
				salesOrderFkId := models.GetSalesOrderFkIDFromOrgidOrderNo(_orgId, newCode)
				SalesOrderFkId := strconv.Itoa(salesOrderFkId)
				c.Redirect("/sales/sales-orders-assign-batches?salesOrderNo="+newCode+"&Salesorderfkid="+SalesOrderFkId, 303)
			}
		}

		c.Redirect("/sales/new-list", 303)
	}
}

func (c *SalesController) CreateDelivery() {
	var user_id string
	var _orgId string
	var username string
	var localetimezone string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
		localetimezone = fmt.Sprintf("%s", sessionVar.Get("LocaleTimeZone"))
	} else {
		user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
		localetimezone = fmt.Sprintf("%s", c.Ctx.Input.GetData("LocaleTimeZone"))
	}
	c.Data["username"] = username
	formStruct := models.DeliveryData{
		Orgid:    _orgId,
		Userid:   user_id,
		Username: username,
	}

	newGenCode, newCode := c.GetGeneratedCode(_orgId, 12, localetimezone)
	c.Data["genCode"] = newGenCode

	retrievedso := make(map[string]orm.Params)
	retrievedso = models.GetSalesOrderDetailsForDelivery(formStruct)
	c.Data["SoRetrieved"] = retrievedso

	retrievedtransporters := make(map[string]orm.Params)
	retrievedtransporters = models.GetTransportersforDropdown(_orgId)
	c.Data["TransporterRetrieved"] = retrievedtransporters

	retrieved := make(map[string]orm.Params)
	retrieved = models.GetAllDeliveryDetails(formStruct)
	c.Data["Retrieved"] = retrieved
	//fmt.Println("***Retrieved: ", retrieved)

	// retrieved8 := make(map[string]orm.Params)
	// retrieved8 = models.GetSupplierNamess(formStruct)
	// c.Data["SupRetrieveds"] = retrieved8
	c.TplName = "sales/delivery.html"
	c.Data["Sales"] = 1
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Delivery (List / View / Manage)"

	if c.Ctx.Input.Method() == "POST" {

		flash := beego.NewFlash()
		items := deliveryValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		//if items.ViewButton == "items" {
		formStruct2 := models.DeliveryData{
			Orgid: _orgId,
			SoNo:  items.SoNo,
		}

		retrieved3 := make(map[string]orm.Params)
		retrieved3 = models.GetSOItemsBySoNo(formStruct2)
		//fmt.Println("***retrieved3", retrieved3)
		c.Data["Retrieved3"] = retrieved3

		retrieved13 := make(map[string]orm.Params)
		retrieved13 = models.GetSODetailsBySoNo(formStruct2)
		c.Data["Retrieved13"] = retrieved13

		c.Data["ErrorsPresent"] = true
		//return
		//}

		if items.ViewButton == "sub" {
			valid := validation.Validation{}
			if b, _ := valid.Valid(&items); !b {
				//fmt.Println(err)
				//fmt.Println(valid.ErrorsMap)
				c.Data["Errors"] = valid.ErrorsMap
				c.Data["ErrorsPresent"] = true
				return
			}

			//GET SUPPLIER IDS BASED ON SUPPLIER NAME
			//supplierId := models.GetSuppplierIDByCompanyName(items.Suppliers, _orgId)

			// layout := "2006-01-02 15:04:05"
			// invoiceDate := time.Now()
			// invDate, errIn := time.Parse(layout,items.CustomerInvoiceDt)
			// if errIn == nil {
			// 	invoiceDate = invDate
			// }

			// dcDate :=time.Now()
			// dcdt, errDc := time.Parse(layout, items.CustomerDcDt)
			// if errDc == nil {
			// 	dcDate = dcdt
			// }

			//INSERT INTO PO SO SUPPLIER INVOICE
			// supplierInvoiceDetails := models.SupplierInvoice{
			// 	Orgid: _orgId,
			// 	SupplierFkId: supplierId,
			// 	SupplierInvoice: items.SupplierInvoice,
			// 	SupplierInvoiceDt: invoiceDate,
			// 	//SupplierDcNo: items.SupplierDcNo,
			// 	SupplierDcDt: dcDate,
			// 	DateAdded: time.Now(),
			// }

			//invoiceInsertId := models.InsertPOSOSupplierInvoice(supplierInvoiceDetails)

			dispatchitems := make([]map[string]string, 0, 3)
			c.Ctx.Input.Bind(&dispatchitems, "dispatchitems")
			//if invoiceInsertId != 0 {
			for k, it := range dispatchitems {

				//Get batches data
				batches := make([]map[string]string, 0, 3)
				c.Ctx.Input.Bind(&batches, "batch["+strconv.Itoa(k)+"]")

				if it["qtyDelivered"] == "" {
					var d string
					d = "0.0"
					it["qtyDelivered"] = d
				}
				// if it["qtyRejected"] == "" {
				// 	var s string
				// 	s = "0.0"
				// 	it["qtyRejected"] = s
				// }

				for _, d := range batches {
					if d["qty"] != "" {
						formStruct := models.DeliveryData{
							Orgid:        _orgId,
							Userid:       user_id,
							SoNo:         items.SoNo,
							Itemss:       it["itemname"],
							Customer:     items.Customer,
							DeliveryDate: items.DeliveryDate,
							AddNote:      items.AddNote,
							DQuantity:    d["qty"],
							//RQuantity:         it["qtyRejected"],
							DeliveryLocation: items.DeliveryLocation,
							TotalCost:        items.TotalCost,
							DeliveredDate:    items.DeliveredDate,
							BatchNo:          d["batchno"],
							//NewGenCode:        items.DispatchNo,
							NewGenCode: newGenCode,
							DispatchNo: newCode,
							//SupplierInvoiceFkId: invoiceInsertId,
							LrNo:            items.LrNo,
							LrDt:            items.LrDt,
							TransporterFkid: items.Transporter,
							VehicleFkid:     items.Vehicle,
						}
						//insertStatus := models.CreateGRN1(formStruct)
						insertStatus := models.CreateDispatchNote(formStruct)
						if insertStatus == 0 {
							c.Data["ErrorsPresent"] = true
							return
						} else {

						}
					}
				}
			}
			//}

			// Check SO is Complete
			completeStatus := models.CheckIfSOComplete(_orgId, items.SoId)
			if completeStatus == 1 {
				integerSoId, _ := strconv.Atoi(items.SoId)
				_ = models.ChangeSalesOrderStatus(integerSoId, _orgId, user_id, 8, items.DeliveryLocation)
			}
			c.Redirect("/sales/delivery", 303)
		}
	}
}

func (c *SalesController) StockTransfer() {
	flash := beego.ReadFromRequest(&c.Controller)
	if ok := flash.Data["error"]; ok != "" {
		c.Data["FlasErrors"] = ok
	}
	if ok := flash.Data["notice"]; ok != "" {
		// Display error messages
		c.Data["FlashNotices"] = ok
	}

	var user_id string
	var _orgId string
	var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}
	c.Data["username"] = username
	formStruct5 := models.Sales{
		Orgid:    _orgId,
		Userid:   user_id,
		Username: username,
	}
	retrievedCustomer := make(map[string]orm.Params)
	retrievedCustomer = models.GetCustomersNames(formStruct5)
	c.Data["CustRetrieved"] = retrievedCustomer

	retrievedg := make(map[string]orm.Params)
	retrievedg = models.GetSaItems(formStruct5)
	c.Data["Retrievedg"] = retrievedg

	formStruct1 := models.Sales{
		Orgid:    _orgId,
		Userid:   user_id,
		Username: username,
	}
	retrieved := make(map[string]orm.Params)
	retrieved = models.GetSales(formStruct1)
	c.Data["Retrieved"] = retrieved

	/*var retrieved1 string
	retrieved1=models.GetOrderCode(formStruct1)
	c.Data["CodeRetrieved"] = retrieved1*/

	c.TplName = "sales/stock-transfer.html"
	c.Data["Sales"] = 1
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Stock Transfer"
	//generic code to handle post request and validation
	if c.Ctx.Input.Method() == "POST" {
		flash := beego.NewFlash()
		items := newStockTransferValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			c.Data["ErrorsPresent"] = true
			return
		}

		if items.SaveButton == "draft" {

			formStruct := models.StockTransfer{
				Orgid:    _orgId,
				Userid:   user_id,
				Username: username,

				OrderNo:      items.OrderNo,
				OrderDate:    items.OrderDate,
				CustomerName: items.CustomerName,
				//JobType:            items.JobType,
				DeliveryDate:       items.DeliveryDate,
				ExciseCharges:      items.ExciseCharges,
				TransportCharges:   items.TransportCharges,
				DispatchType:       items.DispatchType,
				PaymentTerms:       items.PaymentTerms,
				Remarks:            items.Remarks,
				TotalDiscount:      items.TotalDiscount,
				TotalPrice:         items.TotalPrice,
				TotalQuantity:      items.TotalQuantity,
				TotalAfterDiscount: items.TotalAfterDiscount,
				TotalAfterExcise:   items.TotalAfterExcise,
				OctroiCharges:      items.OctroiCharges,
				TotalAfterOctroi:   items.OctroiCharges,
				TotalAfterTaxes:    items.TotalAfterTaxes,
				TotalSalesPrice:    items.TotalSalesPrice,
				TariffNo:           items.TariffNo,
				Status:             "0",
			}

			uniqueStatus := models.STUniqueCheck(formStruct)
			if uniqueStatus == 0 {
				flash.Error("Product Code not unique")
				flash.Store(&c.Controller)
				c.Data["ErrorsPresent"] = true
				return
			}

			insertStatus := models.CreateStockTransfer(formStruct)
			if insertStatus == 0 {
				flash.Error("An error occured while saving. Please try again.")
				flash.Store(&c.Controller)
				c.Data["ErrorsPresent"] = true
				return
			}
		}

		if items.SaveButton == "approval" {

			formStruct := models.StockTransfer{
				Orgid:    _orgId,
				Userid:   user_id,
				Username: username,

				OrderNo:      items.OrderNo,
				OrderDate:    items.OrderDate,
				CustomerName: items.CustomerName,
				//JobType:          items.JobType,
				DeliveryDate:       items.DeliveryDate,
				ExciseCharges:      items.ExciseCharges,
				TransportCharges:   items.TransportCharges,
				DispatchType:       items.DispatchType,
				PaymentTerms:       items.PaymentTerms,
				Remarks:            items.Remarks,
				TotalDiscount:      items.TotalDiscount,
				TotalPrice:         items.TotalPrice,
				TotalQuantity:      items.TotalQuantity,
				TotalAfterDiscount: items.TotalAfterDiscount,
				TotalAfterExcise:   items.TotalAfterExcise,
				TotalSalesPrice:    items.TotalSalesPrice,
				TariffNo:           items.TariffNo,
				Status:             "1",
			}

			uniqueStatus := models.STUniqueCheck(formStruct)
			if uniqueStatus == 0 {
				flash.Error("Product Code not unique")
				flash.Store(&c.Controller)
				c.Data["ErrorsPresent"] = true
				return
			}

			insertStatus := models.CreateStockTransfer(formStruct)
			if insertStatus == 0 {
				flash.Error("An error occured while saving. Please try again.")
				flash.Store(&c.Controller)
				c.Data["ErrorsPresent"] = true
				return
			}
		}

		stitems := make([]map[string]string, 0, 3)
		c.Ctx.Input.Bind(&stitems, "group-a")

		for _, it := range stitems {

			if it["item_name"] != "" {

				formStruct1 := models.StockTransfer{
					Orgid:              _orgId,
					Userid:             user_id,
					Username:           username,
					OrderNo:            items.OrderNo,
					OrderDate:          items.OrderDate,
					CustomerName:       items.CustomerName,
					JobType:            items.JobType,
					DeliveryDate:       items.DeliveryDate,
					ItemName:           it["item_name"],
					Rate:               it["price"],
					Quantity:           it["quantity"],
					Discount:           it["discount"],
					ExciseCharges:      items.ExciseCharges,
					TransportCharges:   items.TransportCharges,
					DispatchType:       items.DispatchType,
					PaymentTerms:       items.PaymentTerms,
					Remarks:            items.Remarks,
					TotalDiscount:      items.TotalDiscount,
					TotalPrice:         items.TotalPrice,
					TotalQuantity:      items.TotalQuantity,
					TotalAfterDiscount: items.TotalAfterDiscount,
					TotalAfterExcise:   items.TotalAfterExcise,
					TotalSalesPrice:    items.TotalSalesPrice,
					TariffNo:           items.TariffNo,
				}

				insertStatus := models.InsertSTItems(formStruct1)
				if insertStatus == 0 {
					flash.Error("An error occured while saving. Please try again.")
					flash.Store(&c.Controller)
					c.Data["ErrorsPresent"] = true
					return
				}
			}
		}

		c.Redirect("/sales/stock-transfer", 303)
	}
}

func (c *SalesController) SOItems() {

	var _orgId string
	//var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		//user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		//username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		//user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		//username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}

	c.TplName = "sales/new-list.html"
	c.Data["Sales"] = 1
	c.Data["PageTitle"] = "New/List"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

	if c.Ctx.Input.Method() == "GET" {

		product_id := c.GetString("id")
		//retrieved :=make(map[string]orm.Params)

		items, quantity, status, rate, discount_rate, delivery_date, uom, discount_value, hsn_sac, taxable_amount, gst_rate, igst_rate, igst_amount, cgst_rate, cgst_amount, sgst_rate, sgst_amount, cess_rate, cess_amount, total_tax_cess, total_after_tax, freight, insurance, packaging_forwarding, others, total_other_charges, tax_regime := models.GetSOItems(product_id, _orgId)
		//fmt.Println("***total_after_tax", total_after_tax)
		outputObj := &mystruct3{
			Items:        items,
			Quantity:     quantity,
			Rate:         rate,
			DiscountRate: discount_rate,
			DeliveryDate: delivery_date,
			UOM:          uom,

			DiscountValue:       discount_value,
			HsnSac:              hsn_sac,
			TaxableAmount:       taxable_amount,
			GstRate:             gst_rate,
			IgstRate:            igst_rate,
			IgstAmount:          igst_amount,
			CgstRate:            cgst_rate,
			CgstAmount:          cgst_amount,
			SgstRate:            sgst_rate,
			SgstAmount:          sgst_amount,
			CessRate:            cess_rate,
			CessAmount:          cess_amount,
			TotalTaxCess:        total_tax_cess,
			TotalAfterTax:       total_after_tax,
			Freight:             freight,
			Insurance:           insurance,
			PackagingForwarding: packaging_forwarding,
			Others:              others,
			TotalOtherCharges:   total_other_charges,
			TaxRegime:           tax_regime,
			Status:              status,
		}
		//fmt.Println(outputObj)
		b, _ := json.Marshal(outputObj)
		//fmt.Println("***string b", string(b))
		//c.Data["ProductRetrieved"] = retrieved
		//fmt.Println("****Result:",result)
		c.Ctx.Output.Body([]byte(b))
	}
}

func (c *SalesController) SOInvoiceItems() {

	var _orgId string
	//var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		//user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		//username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		//user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		//username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}

	c.TplName = "sales/new-list.html"
	c.Data["Sales"] = 1
	c.Data["PageTitle"] = "New/List"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

	if c.Ctx.Input.Method() == "GET" {

		product_id := c.GetString("id")
		//retrieved :=make(map[string]orm.Params)

		items, quantity, status, rate, discount_rate, delivery_date, uom, discount_value, hsn_sac, taxable_amount, gst_rate, igst_rate, igst_amount, cgst_rate, cgst_amount, sgst_rate, sgst_amount, cess_rate, cess_amount, total_tax_cess, total_after_tax, freight, insurance, packaging_forwarding, others, total_other_charges, tax_regime := models.GetSOItems(product_id, _orgId)
		//fmt.Println("***discount value: ", discount_value)
		var total_of_discount = 0.0
		for i := 0; i < len(discount_value); i++ {
			new_value, _ := strconv.ParseFloat(discount_value[i], 64)
			total_of_discount = total_of_discount + new_value
		}

		var total_of_cgst = 0.0
		for i := 0; i < len(discount_value); i++ {
			new_value, _ := strconv.ParseFloat(cgst_amount[i], 64)
			total_of_cgst = total_of_cgst + new_value
		}

		var total_of_sgst = 0.0
		for i := 0; i < len(discount_value); i++ {
			new_value, _ := strconv.ParseFloat(sgst_amount[i], 64)
			total_of_sgst = total_of_sgst + new_value
		}
		//fmt.Println("***total of discount value: ", total_of_discount)

		outputObj := &mystruct3{
			Items:        items,
			Quantity:     quantity,
			Rate:         rate,
			DiscountRate: discount_rate,
			DeliveryDate: delivery_date,
			UOM:          uom,

			DiscountValue:       discount_value,
			HsnSac:              hsn_sac,
			TaxableAmount:       taxable_amount,
			GstRate:             gst_rate,
			IgstRate:            igst_rate,
			IgstAmount:          igst_amount,
			CgstRate:            cgst_rate,
			CgstAmount:          cgst_amount,
			SgstRate:            sgst_rate,
			SgstAmount:          sgst_amount,
			CessRate:            cess_rate,
			CessAmount:          cess_amount,
			TotalTaxCess:        total_tax_cess,
			TotalAfterTax:       total_after_tax,
			Freight:             freight,
			Insurance:           insurance,
			PackagingForwarding: packaging_forwarding,
			Others:              others,
			TotalOtherCharges:   total_other_charges,
			TaxRegime:           tax_regime,
			TotalOfDiscount:     total_of_discount,
			TotalOfCGST:         total_of_cgst,
			TotalOfSGST:         total_of_sgst,
			Status:              status,
		}
		//fmt.Println(outputObj)
		b, _ := json.Marshal(outputObj)
		//fmt.Println("***string b", string(b))
		//c.Data["ProductRetrieved"] = retrieved
		//fmt.Println("****Result:",result)
		c.Ctx.Output.Body([]byte(b))
	}
}

func (c *SalesController) SalesRates() {

	var _orgId string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
	} else {
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
	}

	c.TplName = "sales/new-list.html"
	c.Data["Sales"] = 1
	c.Data["PageTitle"] = "New/List"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

	if c.Ctx.Input.Method() == "GET" {
		item_name := c.GetString("item")
		customer_id := c.GetString("customer")
		origin_id := c.GetString("origin")

		new_hsn, new_uom, new_gst, new_rate, customer_narration, available_qty, status := models.GetSalesRates(item_name, customer_id, origin_id, _orgId)

		outputObj := &ratestruct2{
			HSN:               new_hsn,
			UOM:               new_uom,
			GST:               new_gst,
			Rate:              new_rate,
			CustomerNarration: customer_narration,
			AvailableQty:      available_qty,
			Status:            status,
		}
		b, _ := json.Marshal(outputObj)
		c.Ctx.Output.Body([]byte(b))
	}
}

func (c *SalesController) ChangeSalesOrderStatus() {
	var user_id string
	var _orgId string

	sessionVar := c.StartSession()
	if sessionVar != nil {
		user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))

	} else {
		user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))

	}

	c.TplName = "sales/new-list.html"
	c.Data["Sales"] = 1
	c.Data["PageTitle"] = "New/List"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

	if c.Ctx.Input.Method() == "GET" {

		order_no := c.GetString("order_no")
		button_status := c.GetString("button_status")
		var order_status int
		insertStatus := 1

		//NEED TO CHECK INVENTORY STATUS BEFORE UPDATING THE SALES ORDER STATUS
		//GET THE SALES ORDER FKID BASED ON SALES ORDER NUM AND ORG ID
		salesOrderFkId := models.GetSalesOrderFkIDFromOrgidOrderNo(_orgId, order_no)
		salesOrderLocation := models.GetSalesOrderLocationFromOrgidOrderNo(_orgId, order_no)
		if salesOrderFkId != 0 {
			//GET SALES ORDER ITEMS BASED ON SALES ORDER FKID
			salesItems := models.GetSalesOrderItemsByOrderId(salesOrderFkId, _orgId)
			salesitems := make([]map[string]string, 0, 3)

			for _, it := range salesItems {
				data := make(map[string]string)
				data["item_name"] = it["item_org_assoc_fkid"].(string)
				data["qty"] = it["quantity"].(string)

				salesitems = append(salesitems, data)
			}

			isInventoryOk := models.CheckInventoryStatusForAllItems(salesitems, strconv.Itoa(salesOrderLocation), _orgId)

			if button_status == "approval" {
				if isInventoryOk == "0" { //NO INVENTORY
					order_status = models.GetSalesOrderStatusCode(models.SendForApprovalNoInventory)
				} else {
					order_status = models.GetSalesOrderStatusCode(models.SendForApprovalWithInventory)
				}
			} else if button_status == "approved" {
				if isInventoryOk == "0" { //NO INVENTORY
					order_status = models.GetSalesOrderStatusCode(models.ApproveNoInventory)
				} else {
					order_status = models.GetSalesOrderStatusCode(models.ApproveWithInventory)
				}
			} else if button_status == "correction" {
				order_status = models.GetSalesOrderStatusCode(models.SendForCorrection)
			} else if button_status == "rejected" {
				salesOrderFkIdstring := strconv.Itoa(salesOrderFkId)
				prevStatus, _ := models.GetStatusOfSalesOrder(salesOrderFkIdstring, _orgId)
				if prevStatus == 0 || prevStatus == 11 || prevStatus == 12 || prevStatus == 3 {
					order_status = models.GetSalesOrderStatusCode(models.RejectBeforeApproval)
				} else {
					order_status = models.GetSalesOrderStatusCode(models.RejectAfterApproval)
				}
			} else if button_status == "production" {
				order_status = models.GetSalesOrderStatusCode(models.SendForProduction)
			} else if button_status == "pending" {
				order_status = models.GetSalesOrderStatusCode(models.PrepareForDispatch)
			} else if button_status == "dispatched" {
				order_status = models.GetSalesOrderStatusCode(models.Dispatch)
			} else if button_status == "completed" {
				order_status = models.GetSalesOrderStatusCode(models.Completed)
			}

			insertStatus = models.ChangeSalesOrderStatus(salesOrderFkId, _orgId, user_id, order_status, strconv.Itoa(salesOrderLocation))

		}

		b, _ := json.Marshal(insertStatus)
		c.Ctx.Output.Body([]byte(b))
	}
}

func (c *SalesController) CheckInventoryStatus() {
	var user_id string
	var _orgId string
	//var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
	} else {
		user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
	}

	c.TplName = "sales/new-list.html"
	c.Data["Sales"] = 1
	c.Data["PageTitle"] = "New/List"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

	if c.Ctx.Input.Method() == "GET" {
		order_no := c.GetString("order_no")
		//order_status := c.GetString("product_status")

		formStruct := models.Sales{
			Orgid:   _orgId,
			OrderNo: order_no,
			Userid:  user_id,
		}
		var isInventoryOk string = "1"
		//if order_status == "6" {
		salesOrder := models.GetSalesOrderByOrderNumber(formStruct.OrderNo, formStruct.Orgid)
		for k, _ := range salesOrder {
			getID := salesOrder[k]["_id"].(string)
			getValue, _ := strconv.ParseInt(getID, 10, 64)
			soID := int(getValue)
			oLocation := salesOrder[k]["origin_location"].(string)
			//oLocation := salesOrder[k]["origin_location"].(string)
			salesItems := models.GetSalesOrderItemsByOrderId(soID, formStruct.Orgid)
			for _, it := range salesItems {
				//available_qty := models.CheckForQty(it["item_name"], oLocation, formStruct.Orgid)
				if it["status"] == "99" {
					var QtyinStock string
					var reqQty float64 = 0.0
					reqQty, _ = strconv.ParseFloat(it["quantity"].(string), 64)
					QtyinStock = models.CheckForQty(it["item_org_assoc_fkid"].(string), oLocation, formStruct.Orgid)
					f, _ := strconv.ParseFloat(QtyinStock, 64)
					if reqQty <= f {
						isInventoryOk = "1"
					} else {
						isInventoryOk = "0"
						break
					}
				}
			}
		}
		//}

		outputObj := &InventoryStatus{
			InventoryOk: isInventoryOk,
		}
		b, _ := json.Marshal(outputObj)
		c.Ctx.Output.Body([]byte(b))
	}
}

func (c *SalesController) FormC() {
	//fmt.Println("Inside sales controller")
	//var user_id string
	var _orgId string
	//var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		//user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		//username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		//user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		//username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}

	c.TplName = "sales/new-list.html"
	c.Data["Sales"] = 1
	c.Data["PageTitle"] = "New/List"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

	if c.Ctx.Input.Method() == "GET" {
		supplier := c.GetString("supplier")
		//fmt.Println("id=", supplier)
		//fmt.Println("inside get")
		//product_status := c.GetString("product_status")

		new_cst, status := models.GetCST(supplier, _orgId)
		//fmt.Println("***Rate=", new_cst)
		//fmt.Println("***Status=", status)
		outputObj := &cststruct2{
			CST:    new_cst,
			Status: status,
		}
		// fmt.Println(outputObj)
		b, _ := json.Marshal(outputObj)
		//fmt.Println("***string b", string(b))
		// //c.Data["ProductRetrieved"] = retrieved
		// //fmt.Println("****Result:",result)
		c.Ctx.Output.Body([]byte(b))
	}
}

func (c *SalesController) SalesItems() {

	var _orgId string
	//var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		//user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		//username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		//user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		//username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}

	c.TplName = "sales/new-list.html"
	c.Data["Sales"] = 1
	c.Data["PageTitle"] = "New/List"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

	if c.Ctx.Input.Method() == "GET" {

		//supplier_name := c.GetString("supplier")
		//fmt.Println("*****Supplier is ",supplier_name)
		customer_id := c.GetString("customer")
		//fmt.Println("*****Item is ", item_name)
		//retrieved :=make(map[string]orm.Params)

		item_id, invoice_narration, name, uom := models.GetSalesItems(customer_id, _orgId)
		//fmt.Println("***Rate=", new_rate)
		//fmt.Println("***Status=", status)
		outputObj := &itemstruct{
			InvoiceNarration: invoice_narration,
			Name:             name,
			UOM:              uom,
			ItemId:           item_id,
		}
		// fmt.Println(outputObj)
		b, _ := json.Marshal(outputObj)
		//fmt.Println("***string b", string(b))
		// //c.Data["ProductRetrieved"] = retrieved
		// //fmt.Println("****Result:",result)
		c.Ctx.Output.Body([]byte(b))
	}
}

func (c *SalesController) SalesLocation() {

	var _orgId string
	//var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		//user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		//username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		//user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		//username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}

	c.TplName = "sales/new-list.html"
	c.Data["Sales"] = 1
	c.Data["PageTitle"] = "New/List"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

	if c.Ctx.Input.Method() == "GET" {

		//supplier_name := c.GetString("supplier")
		//fmt.Println("*****Supplier is ",supplier_name)
		customer_id := c.GetString("customer")
		//fmt.Println("*****Item is ", item_name)
		//retrieved :=make(map[string]orm.Params)

		location_id, address_line := models.GetCustomerLocation(customer_id, _orgId)
		//fmt.Println("***Rate=", new_rate)
		//fmt.Println("***Status=", status)
		outputObj := &locationstruct{
			Address:    address_line,
			LocationId: location_id,
		}
		//fmt.Println(outputObj)
		b, _ := json.Marshal(outputObj)
		//fmt.Println("***string b", string(b))
		// //c.Data["ProductRetrieved"] = retrieved
		// //fmt.Println("****Result:",result)
		c.Ctx.Output.Body([]byte(b))
	}
}

func (c *SalesController) GetSalesVehicles() {

	var _orgId string
	//var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		//user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		//username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		//user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		//username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}

	c.TplName = "sales/new-list.html"
	c.Data["Sales"] = 1
	c.Data["PageTitle"] = "New/List"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

	if c.Ctx.Input.Method() == "GET" {

		//supplier_name := c.GetString("supplier")
		//fmt.Println("*****Supplier is ",supplier_name)
		transporter_id := c.GetString("transporter")
		//fmt.Println("*****Item is ", item_name)
		//retrieved :=make(map[string]orm.Params)

		vehicle_id, driver_name, registration_no := models.GetTransporterVehicles(transporter_id, _orgId)
		//fmt.Println("***Rate=", new_rate)
		//fmt.Println("***Status=", status)
		outputObj := &vehiclestruct{
			VehicleId:      vehicle_id,
			DriverName:     driver_name,
			RegistrationNo: registration_no,
		}
		//fmt.Println(outputObj)
		b, _ := json.Marshal(outputObj)
		//fmt.Println("***string b", string(b))
		// //c.Data["ProductRetrieved"] = retrieved
		// //fmt.Println("****Result:",result)
		c.Ctx.Output.Body([]byte(b))
	}
}

type Page struct {
	Content template.HTML
}

func (c *SalesController) GenerateInvoiceSO() {
	//var user_id string
	var _orgId string
	var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		//user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		//user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}
	c.Data["username"] = username
	c.TplName = "sales/so-invoice.html"
	c.Data["Sales"] = 1
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "SO Invoice"

	if c.Ctx.Input.Method() == "GET" {

		sales_code := c.GetString("soId")
		flash := beego.NewFlash()
		items := SalesOrderValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			return
		}

		retrievedOrg := make(map[string]orm.Params)
		retrievedOrg = models.GetOrgDetails(_orgId)
		c.Data["OrgRetrieved"] = retrievedOrg

		retrievedSoDetails := make(map[string]orm.Params)
		retrievedSoDetails = models.GetSalesDetailsByCode(sales_code, _orgId)
		c.Data["SoDetailsRetrieved"] = retrievedSoDetails

		retrievedSoItems := make(map[string]orm.Params)
		retrievedSoItems = models.GetSOItemsByCode(sales_code, _orgId)
		c.Data["SoItemsRetrieved"] = retrievedSoItems

		fmt.Println("***retrievedSoItems ", retrievedSoDetails)
		var totalOfDiscountValue float64 = 0.0
		var totalOfCGST float64 = 0.0
		var totalOfSGST float64 = 0.0

		for k, _ := range retrievedSoItems {
			if retrievedSoItems[k]["discount_value"] != "" && retrievedSoItems[k]["discount_value"] != nil {
				temp_totalOfDiscountValue, _ := strconv.ParseFloat(retrievedSoItems[k]["discount_value"].(string), 64)
				totalOfDiscountValue = totalOfDiscountValue + temp_totalOfDiscountValue
			}
			if retrievedSoItems[k]["cgst_amount"] != "" && retrievedSoItems[k]["cgst_amount"] != nil {
				temp_totalOfCGST, _ := strconv.ParseFloat(retrievedSoItems[k]["cgst_amount"].(string), 64)
				totalOfCGST = totalOfCGST + temp_totalOfCGST
			}
			if retrievedSoItems[k]["sgst_amount"] != "" && retrievedSoItems[k]["sgst_amount"] != nil {
				temp_totalOfSGST, _ := strconv.ParseFloat(retrievedSoItems[k]["sgst_amount"].(string), 64)
				totalOfSGST = totalOfSGST + temp_totalOfSGST
			}
		}
		c.Data["TotalOfDiscount"] = totalOfDiscountValue
		c.Data["TotalOfCGST"] = totalOfCGST
		c.Data["TotalOfSGST"] = totalOfSGST

		var retrievedSOtc string
		retrievedSOtc = models.GetSOtc(_orgId)
		mySlice := []byte(retrievedSOtc)
		output := blackfriday.MarkdownCommon(mySlice)
		t := bluemonday.UGCPolicy().SanitizeBytes(output)
		s := string(t)
		//fmt.Println("retrievedSoDetails: ", s)
		c.Data["RetrievedSOtc"] = template.HTML(s)
		//c.Data["RetrievedSOtc"] = bluemonday.UGCPolicy().SanitizeBytes(output)
		//c.Data["RetrievedSOtc"] = retrievedSOtc

		//fmt.Println(retrievedSoDetails)
		//c.Data["FormItems"] = models.GetSalesDetailsByCode(sales_code, _orgId)
		// retrieved := make(map[string]orm.Params)
		// retrieved = models.GetEditPOItems(purchase_code, _orgId)
		// //fmt.Println("**length: ", len(retrieved))
		// c.Data["RepeaterLength"] = len(retrieved)
		// c.Data["POItemsRetrieved"] = retrieved

		// c.Data["username"] = username
		// formStruct1 := models.Createpo{
		// 	Orgid:    _orgId,
		// 	Userid:   user_id,
		// 	Username: username,
		// }

		// retrieved01 := make(map[string]orm.Params)
		// retrieved01 = models.GetPItems(formStruct1)
		// c.Data["ItemsRetrieved"] = retrieved01

		// c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		// c.Data["ErrorsPresent"] = true
		//return
	}
	// if c.Ctx.Input.Method() == "POST" {
	// 	//fmt.Println("inside post")
	// 	flash := beego.NewFlash()
	// 	items := createPoValidateEdit{}
	// 	if err := c.ParseForm(&items); err != nil {
	// 		flash.Error("Cannot parse form")
	// 		flash.Store(&c.Controller)
	// 		return
	// 	}
	// 	c.Data["FormItems"] = items
	// 	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	// 	valid := validation.Validation{}
	// 	if b, _ := valid.Valid(&items); !b {
	// 		c.Data["Errors"] = valid.ErrorsMap
	// 		c.Data["ErrorsPresent"] = true
	// 		fmt.Println(valid)
	// 		return
	// 	}

	// 	insertStatus := models.UpdateDraftPO(formStruct, podraftitems)
	// 	if insertStatus == 0 {
	// 		flash.Error("Form Not Inserted")
	// 		flash.Store(&c.Controller)
	// 		return
	// 	}

	// 	c.Redirect("/purchase/create-po", 303)
	// }
}

//Function to  process the dispatch
func ProcessSalesOrderDispatch(OrderNo string, Orgid string) int {

	//Get All the sales_order_items_inventory details
	salesOrderInventory := make(map[string]orm.Params)
	salesOrderInventory = models.GetSalesOrderInventoryDetailsBySalesOrderId(OrderNo, Orgid)

	//For each entry modify the item stock table

	for k := range salesOrderInventory {
		//Get the record from item stock and find its existing quantity

		itemStkid := salesOrderInventory[k]["item_stock_fkid"].(string)
		itemStockId, _ := strconv.ParseInt(itemStkid, 10, 64)

		itemStockRecord := models.GetItemsStockByPkId(itemStockId)

		for l := range itemStockRecord {
			oQty := itemStockRecord[l]["quantity"].(string)
			rQty := salesOrderInventory[k]["quantity"].(string)
			orgQuantity, _ := strconv.ParseFloat(oQty, 64)
			revisedquantity, _ := strconv.ParseFloat(rQty, 64)

			updatedQuantity := orgQuantity - revisedquantity

			oId := itemStockRecord[l]["_orgid"].(string)
			//orgId, _ := strconv.ParseInt(oId, 10, 64)
			itid := itemStockRecord[l]["item_org_assoc_fkid"].(string)
			itemid, _ := strconv.ParseInt(itid, 10, 64)
			geolocation := itemStockRecord[l]["geo_location"].(string)

			itemStockData := models.ItemStockTable{
				OrgId:            oId,
				Itemid:           itemid,
				QuantityModified: updatedQuantity,
				Location:         geolocation,
			}

			//Insert and update Item Stock
			isItemStockUpdated := models.InsertUpdateItemStock(itemStockData)

			//UPDATE THE ITEM TOTAL INVENTORY
			if isItemStockUpdated == 1 {

				itemInventoryData := models.ItemTotalInventory{
					OrgId:            oId,
					Itemid:           itemid,
					QuantityModified: updatedQuantity,
					Location:         geolocation,
					AddRemoveFlag:    0,
				}

				insertInvStatus := models.InsertUpdateItemTotalInventory(itemInventoryData)
				if insertInvStatus == 0 {
					return 0
				}
			} else {
				return 1
			}
		}
	}

	return 0
}

func (c *SalesController) GetSODetails() {
	//var user_id string
	var _orgId string
	var username string
	//var localetimezone string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		//user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
		//localetimezone = fmt.Sprintf("%s", sessionVar.Get("LocaleTimeZone"))
	} else {
		//user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
		//localetimezone = fmt.Sprintf("%s", c.Ctx.Input.GetData("LocaleTimeZone"))
	}
	c.Data["username"] = username
	// formStruct5 := models.Sales{
	// 	Orgid:    _orgId,
	// 	Userid:   user_id,
	// 	Username: username,
	// }

	soId := c.GetString("soId")

	retrieved := make(map[string]orm.Params)
	retrieved = models.GetSOdetailsByPkId(soId, _orgId)
	c.Data["SORetrieved"] = retrieved

	c.TplName = "sales/sales-details.html"
	c.Data["PageTitle"] = "Sales Details"
	//c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
}

func (c *SalesController) AssignBatchesToSalesOrder() {
	flash := beego.ReadFromRequest(&c.Controller)
	if ok := flash.Data["error"]; ok != "" {
		c.Data["FlasErrors"] = ok
	}
	if ok := flash.Data["notice"]; ok != "" {
		// Display error messages
		c.Data["FlashNotices"] = ok
	}
	var _orgId string

	sessionVar := c.StartSession()
	if sessionVar != nil {
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
	} else {
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
	}

	SalesOrderNo := c.GetString("salesOrderNo")
	//ProductPkId := c.GetString("product")
	SalesOrderFkid := c.GetString("salesOrderFkid")

	c.Data["SalesOrderFkid"] = SalesOrderFkid

	retrieved := make(map[string]orm.Params)
	retrieved = models.GetSalesOrderFormDetails(SalesOrderNo, _orgId)
	c.Data["RetrievedData"] = retrieved

	//fmt.Println("retrieved['sales_0']= ", retrieved["sales_0"])

	retrieveditems := make(map[string]orm.Params)
	retrieveditems = models.GetSalesOrderItems(retrieved["sales_0"]["_id"].(string), _orgId)
	c.Data["RetrievedItems"] = retrieveditems

	c.Data["RepeaterLength"] = len(retrieveditems)

	c.TplName = "sales/sales-order-assign-batches.html"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Sales Orders Assign Batches"

	if c.Ctx.Input.Method() == "POST" {
		salesitems := make([]map[string]string, 0, 3)
		c.Ctx.Input.Bind(&salesitems, "salesitems")

		var location string
		c.Ctx.Input.Bind(&location, "location")

		var Salesorderfkid string
		c.Ctx.Input.Bind(&Salesorderfkid, "Salesorderfkid")
		salesorderfkid, _ := strconv.ParseInt(Salesorderfkid, 10, 64)

		var itemcode string
		c.Ctx.Input.Bind(&itemcode, "item_code")
		itemId, _ := strconv.ParseInt(itemcode, 10, 64)

		for k := range salesitems {
			//Get batches data
			batches := make([]map[string]string, 0, 3)
			c.Ctx.Input.Bind(&batches, "BomGrpitems_"+strconv.Itoa(k))
			for _, d := range batches {

				qty, _ := strconv.ParseFloat(d["qty"], 64)

				formStruct := models.ItemDetailsForOrder{
					OrgId:             _orgId,
					ItemId:            itemId,
					RequiredQuantity:  qty,
					Location:          location,
					BlockedBy:         3,
					ConsumedTableFkId: salesorderfkid,
					BatchNo:           d["batch_id"],
				}

				insertStatus := models.BlockItemsBeforeConsumption(formStruct)
				if insertStatus == 0 {
					c.Data["ErrorsPresent"] = true
					return
				}
			}
		}

		c.Redirect("/sales/new-list", 303)
	}
}

func (c *SalesController) EditSO() {
	var user_id string
	var _orgId string
	var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}
	c.Data["username"] = username
	c.TplName = "sales/edit-so.html"
	c.Data["Sales"] = 1
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Edit Sales Order"

	// userDetailsStruct := models.Createpo{
	// 	Orgid:    _orgId,
	// 	Userid:   user_id,
	// 	Username: username,
	// }
	//fmt.Println("inside edit PO")
	if c.Ctx.Input.Method() == "GET" {
		soId := c.GetString("salesId")
		//item_id := c.GetString("id")
		//fmt.Println("Inside Get view grn controller with ", item_id)
		// flash := beego.NewFlash()
		// items := createPoValidateEdit{}
		// if err := c.ParseForm(&items); err != nil {
		// 	flash.Error("Cannot parse form")
		// 	flash.Store(&c.Controller)
		// 	return
		// }

		retrievedtransporters := make(map[string]orm.Params)
		retrievedtransporters = models.GetTransportersforDropdown(_orgId)
		c.Data["TransporterRetrieved"] = retrievedtransporters

		retrieved := make(map[string]orm.Params)
		retrieved = models.GetSOdetailsByPkId(soId, _orgId)
		c.Data["SORetrieved"] = retrieved
		//fmt.Println(retrieved["so_0"]["soitems"])
		arr := retrieved["so_0"]["soitems"].([]orm.Params)
		lenOfItems := len(arr)
		//fmt.Println("***Length: ", lenItems)
		//c.Data["FormItems"] = models.GetPODetailsByCode(purchase_code, _orgId)
		//retrieved := make(map[string]orm.Params)
		//retrieved = models.GetEditPOItems(purchase_code, _orgId)
		c.Data["RepeaterLength"] = lenOfItems
		//c.Data["POItemsRetrieved"] = retrieved
		//c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		c.Data["ErrorsPresent"] = true
		return
	}
	if c.Ctx.Input.Method() == "POST" {
		//fmt.Println("inside post")
		flash := beego.NewFlash()
		items := SalesOrderValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			c.Data["ErrorsPresent"] = true
			//fmt.Println(valid)
			return
		}

		formStruct := models.Sales{
			Orgid:            _orgId,
			Userid:           user_id,
			Username:         username,
			OrderId:          items.OrderId,
			OrderNo:          items.OrderNo,
			OrderDate:        items.OrderDate,
			CustomerName:     items.CustomerName,
			JobType:          items.JobType,
			DeliveryDate:     items.DeliveryDate,
			DLocation:        items.DLocation,
			OLocation:        items.OLocation,
			Transporter:      items.Transporter,
			Vehicle:          items.Vehicle,
			TaxRegime:        items.TaxRegime,
			ExciseCharges:    items.ExciseCharges,
			TransportCharges: items.TransportCharges,
			//Form30:                items.Form30,
			//FormC:                 items.FormC,
			DispatchType:       items.DispatchType,
			PaymentTerms:       items.PaymentTerms,
			Remarks:            items.Remarks,
			TotalDiscount:      items.TotalDiscount,
			TotalPrice:         items.TotalPrice,
			TotalQuantity:      items.TotalQuantity,
			TotalAfterDiscount: items.TotalAfterDiscount,
			TotalAfterExcise:   items.TotalAfterExcise,
			TotalAfterTaxes:    items.TotalAfterTaxes,
			//CST:                   items.CST,
			//VAT:                   items.VAT,
			TotalSalesPrice:       items.TotalSalesPrice,
			BAddress:              items.BAddress,
			TariffNo:              items.TariffNo,
			TotalTaxableValue:     items.TotalTaxableValue,
			TotalTaxCess:          items.TotalTaxCess,
			TotalFreightPackaging: items.TotalFreightPackaging,
			//NewGenCode:            newGenCode,
			//ManuallyAssignBatch: 	assignFlag,
		}

		salesitems := make([]map[string]string, 0, 3)
		if items.TaxRegime == "1" {
			c.Ctx.Input.Bind(&salesitems, "intra-state")
		} else if items.TaxRegime == "2" {
			c.Ctx.Input.Bind(&salesitems, "inter-state")
		} else if items.TaxRegime == "3" {
			c.Ctx.Input.Bind(&salesitems, "export-state")
		}

		insertStatus := models.UpdateSO(formStruct, salesitems)
		if insertStatus == 0 {
			flash.Error("Form Not Inserted")
			flash.Store(&c.Controller)
			return
		}

		c.Redirect("/sales/new-list", 303)
	}
}

func (c *SalesController) EditDraftSO() {
	var user_id string
	var _orgId string
	var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}
	c.Data["username"] = username
	c.TplName = "sales/edit-draft-so.html"
	c.Data["Sales"] = 1
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Edit Sales Order"

	// userDetailsStruct := models.Createpo{
	// 	Orgid:    _orgId,
	// 	Userid:   user_id,
	// 	Username: username,
	// }
	//fmt.Println("inside edit PO")
	if c.Ctx.Input.Method() == "GET" {
		soId := c.GetString("salesId")
		//item_id := c.GetString("id")
		//fmt.Println("Inside Get view grn controller with ", item_id)
		// flash := beego.NewFlash()
		// items := createPoValidateEdit{}
		// if err := c.ParseForm(&items); err != nil {
		// 	flash.Error("Cannot parse form")
		// 	flash.Store(&c.Controller)
		// 	return
		// }

		retrieveditems := make(map[string]orm.Params)
		retrieveditems = models.GetAllItemsForDropdowns(_orgId)
		c.Data["ItemsRetrieved"] = retrieveditems

		retrievedtransporters := make(map[string]orm.Params)
		retrievedtransporters = models.GetTransportersforDropdown(_orgId)
		c.Data["TransporterRetrieved"] = retrievedtransporters

		retrieved := make(map[string]orm.Params)
		retrieved = models.GetSOdetailsByPkId(soId, _orgId)
		c.Data["SORetrieved"] = retrieved
		//fmt.Println(retrieved["so_0"]["soitems"])
		arr := retrieved["so_0"]["soitems"].([]orm.Params)
		lenOfItems := len(arr)
		//fmt.Println("***Length: ", lenItems)
		//c.Data["FormItems"] = models.GetPODetailsByCode(purchase_code, _orgId)
		//retrieved := make(map[string]orm.Params)
		//retrieved = models.GetEditPOItems(purchase_code, _orgId)
		c.Data["RepeaterLength"] = lenOfItems
		//c.Data["POItemsRetrieved"] = retrieved
		//c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		c.Data["ErrorsPresent"] = true
		return
	}
	if c.Ctx.Input.Method() == "POST" {
		//fmt.Println("inside post")
		flash := beego.NewFlash()
		items := SalesOrderValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			c.Data["ErrorsPresent"] = true
			//fmt.Println(valid)
			return
		}

		formStruct := models.Sales{
			Orgid:            _orgId,
			Userid:           user_id,
			Username:         username,
			OrderId:          items.OrderId,
			OrderNo:          items.OrderNo,
			OrderDate:        items.OrderDate,
			CustomerName:     items.CustomerName,
			JobType:          items.JobType,
			DeliveryDate:     items.DeliveryDate,
			DLocation:        items.DLocation,
			OLocation:        items.OLocation,
			Transporter:      items.Transporter,
			Vehicle:          items.Vehicle,
			TaxRegime:        items.TaxRegime,
			ExciseCharges:    items.ExciseCharges,
			TransportCharges: items.TransportCharges,
			//Form30:                items.Form30,
			//FormC:                 items.FormC,
			DispatchType:       items.DispatchType,
			PaymentTerms:       items.PaymentTerms,
			Remarks:            items.Remarks,
			TotalDiscount:      items.TotalDiscount,
			TotalPrice:         items.TotalPrice,
			TotalQuantity:      items.TotalQuantity,
			TotalAfterDiscount: items.TotalAfterDiscount,
			TotalAfterExcise:   items.TotalAfterExcise,
			TotalAfterTaxes:    items.TotalAfterTaxes,
			//CST:                   items.CST,
			//VAT:                   items.VAT,
			TotalSalesPrice:       items.TotalSalesPrice,
			BAddress:              items.BAddress,
			TariffNo:              items.TariffNo,
			TotalTaxableValue:     items.TotalTaxableValue,
			TotalTaxCess:          items.TotalTaxCess,
			TotalFreightPackaging: items.TotalFreightPackaging,
			//NewGenCode:            newGenCode,
			//ManuallyAssignBatch: 	assignFlag,
		}

		salesitems := make([]map[string]string, 0, 3)
		if items.TaxRegime == "1" {
			c.Ctx.Input.Bind(&salesitems, "intra-state")
		} else if items.TaxRegime == "2" {
			c.Ctx.Input.Bind(&salesitems, "inter-state")
		} else if items.TaxRegime == "3" {
			c.Ctx.Input.Bind(&salesitems, "export-state")
		}

		insertStatus := models.UpdateDraftSO(formStruct, salesitems)
		if insertStatus == 0 {
			flash.Error("Form Not Inserted")
			flash.Store(&c.Controller)
			return
		}

		c.Redirect("/sales/new-list", 303)
	}
}
