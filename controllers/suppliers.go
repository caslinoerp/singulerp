package controllers

import (
	"encoding/json"
	"fmt"
	"html/template"

	"bitbucket.org/caslinoerp/singulerp/models"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"github.com/astaxie/beego/validation"
)

type SuppliersController struct {
	BaseController
}

type addSupplierValidate struct {
	Code          string `form:"code" valid:"Required"`
	CompanyName   string `form:"company_name" valid:"Required"`
	ContactPerson string `form:"contactgrp[0][contact_person]" valid:"Required"`
	Designation   string `form:"contactgrp[0][designation]" valid:"Required"`
	Email         string `form:"email" `
	PhoneNo       string `form:"phone_no" `
	CellNo        string `form:"cell_no" `
	OfficeAddress string `form:"office_address" valid:"Required"`
	City          string `form:"city" valid:"Required"`
	State         string `form:"state" valid:"Required"`
	Country       string `form:"country" valid:"Required"`
	Pincode       string `form:"pincode" valid:"Required"`
	MainGSTIN     string `form:"main_gstin" valid:"Required"`
	CST           string `form:"cst"`
	CreditInDays  string `form:"cin"`
	CreditLimit   string `form:"credit_limit"`
	PAN           string `form:"pan"`
	ECC           string `form:"ecc"`
	TIN           string `form:"tin"`
	Notes         string `form:"notes"`
	GST           string `form:"gst"`
	PGST          string `form:"provisional_gst"`
	ARN           string `form:"arn"`
	Introduction  string `form:"introduction"`

	WarehouseAddress string `form:"group-a[0][warehouse_address]"`
	WCity            string `form:"group-a[0][wcity]"`
	WState           string `form:"group-a[0][wstate]"`
	WCountry         string `form:"group-a[0][wcountry]"`
	WPincode         string `form:"group-a[0][wpincode]"`
	GSTIN            string `form:"group-a[0][gstin]"`

	SupplierNameCheck string `form:"supplier_name_check"`
	SupplierID        string `form:"supplier_id"`
}

// To handle repeater class form elements
type SupplierContactPerson struct {
	ContactPerson string `form:"contactgrp[][contact_person]"`
	Designation   string `form:"contactgrp[][designation]" `
	Email         string `form:"contactgrp[][email]" `
	PhoneNo       string `form:"contactgrp[][phone_no]" `
	CellNo        string `form:"contactgrp[][cell_no]" `
}

type SupplierWarehouse struct {
	WarehouseAddress string `form:"warehouse-a[][warehouse_address]"`
	WCity            string `form:"warehouse-a[][wcity]"`
	WState           string `form:"warehouse-a[][wstate]"`
	WCountry         string `form:"warehouse-a[][wcountry]"`
	WPincode         string `form:"warehouse-a[][wpincode]"`
	GSTIN            string `form:"warehouse-a[][gstin]"`
}

type EditSupplierValidate struct {
	Code              string `form:"code" valid:"Required"`
	CompanyName       string `form:"company_name" valid:"Required"`
	OfficeAddress     string `form:"office_address" valid:"Required"`
	City              string `form:"city" valid:"Required"`
	State             string `form:"state" valid:"Required"`
	Country           string `form:"country" valid:"Required"`
	Pincode           string `form:"pincode" valid:"Required"`
	MainGSTIN         string `form:"main_gstin"`
	CST               string `form:"cst"`
	CreditInDays      string `form:"cin"`
	CreditLimit       string `form:"credit_limit"`
	PAN               string `form:"pan"`
	ECC               string `form:"ecc"`
	TIN               string `form:"tin"`
	Notes             string `form:"notes"`
	GST               string `form:"gst"`
	PGST              string `form:"provisional_gst"`
	ARN               string `form:"arn"`
	Introduction      string `form:"introduction"`
	SupplierNameCheck string `form:"supplier_name_check"`
	SupplierID        string `form:"supplier_id"`
}

type manageSupplierValidate struct {
	Supplier        string   `form:"supplier" valid:"Required"`
	Services        []string `form:"services[]"  valid:"Required"`
	SubmitButtonVal string   `form:"assoc-button"`
}

type scp_struct struct {
	Name        []string `json:"cpname"`
	Designation []string `json:"designation"`
	Email       []string `json:"email"`
	Phone       []string `json:"phone"`
	Mobile      []string `json:"mobile"`
	Status      string   `json:"status"`
}

type warehouse_struct struct {
	Address []string `json:"address"`
	City    []string `json:"city"`
	State   []string `json:"state"`
	Country []string `json:"country"`
	Pincode []string `json:"pincode"`
	GSTIN   []string `json:"gstin"`
	Status  string   `json:"status"`
}

type Search_SupplierDetails struct {
	CompanyName           string `form:"CompanyName"`
	ContactPerson         string `form:"ContactPerson"`
	Location              string `form:"Location"`
	SuppliersSearchOption string `form:"SearchSuppliers"`
	ItemName              string `form:"item_name_check"`
}

func (c *SuppliersController) AddSuppliers() {
	var user_id string
	var _orgId string
	var username string
	var localetimezone string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
		localetimezone = fmt.Sprintf("%s", sessionVar.Get("LocaleTimeZone"))
	} else {
		user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
		localetimezone = fmt.Sprintf("%s", c.Ctx.Input.GetData("LocaleTimeZone"))
	}
	c.Data["username"] = username
	formStruct2 := models.AddSupplier{
		Orgid:    _orgId,
		Username: username,
		Userid:   user_id,
	}
	// formStruct3 := models.StockItems{
	// 	Orgid:    _orgId,
	// 	Userid:   user_id,
	// 	Username: username,
	// }

	newGenCode, newCode := c.GetGeneratedCode(_orgId, 3, localetimezone)
	c.Data["genCode"] = newGenCode

	retrieveditems := make(map[string]orm.Params)
	retrieveditems = models.GetAllItemsForDropdowns(_orgId)
	c.Data["ItemsRetrieved"] = retrieveditems

	retrieved := make(map[string]orm.Params)
	fetchCount := 0
	retrieved, fetchCount = models.GetAddSupplier(formStruct2)
	//fmt.Println(fetchCount)
	if fetchCount > 0 {
		c.Data["Retrieved"] = retrieved
	}

	/*var retrieved1 string
	retrieved1=models.GetSupplierCode(formStruct2)
	c.Data["CodeRetrieved"] = retrieved1*/
	c.Data["SearchBySupDetailsChecked"] = "checked"
	c.TplName = "suppliers/add-view.html"
	c.Data["Suppliers"] = 1
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Add New / View"

	//generic code to handle post request and validation
	if c.Ctx.Input.Method() == "POST" {
		if c.GetString("Searchmethod") == "Supplier_Search" {
			var buttonSearch string
			c.Ctx.Input.Bind(&buttonSearch, "supplier-search")
			if buttonSearch == "SearchSupplierDetails" {
				flash := beego.NewFlash()
				items := Search_SupplierDetails{}
				if err := c.ParseForm(&items); err != nil {
					flash.Error("Cannot parse form")
					//flash.Store(&c.Controller)
					return
				}
				retrieved := make(map[string]orm.Params)
				if items.SuppliersSearchOption == "SearchBy_SupplierDeatils" {
					retrieved = models.SearchBySupplierDetails(items.CompanyName, items.ContactPerson, items.Location, _orgId)
					c.Data["SearchBySupDetailsChecked"] = "checked"
				}
				if items.SuppliersSearchOption == "SearchByItemName" {
					retrieved = models.SearchBySupplierDetailsByItemName(items.ItemName, _orgId)
					c.Data["SearchByItemNameChecked"] = "checked"
				}
				c.Data["SearchItems"] = items
				c.Data["Retrieved"] = retrieved
				c.TplName = "suppliers/add-view.html"
				c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
				c.Data["PageTitle"] = "Add New / View"
				return
			}
			c.Ctx.Input.Bind(&buttonSearch, "Reset-SupplierSearch")
			if buttonSearch == "ResetSupplierSearch" {
				c.TplName = "suppliers/add-view.html"
				c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
				c.Data["PageTitle"] = "Add New / View"
				return
			}
		}
		flash := beego.NewFlash()
		items := addSupplierValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			c.Data["ErrorsPresent"] = true
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			c.Data["ErrorsPresent"] = true
			return
		}

		formStruct := models.AddSupplier{
			Orgid:         _orgId,
			Username:      username,
			Userid:        user_id,
			Code:          newCode,
			CompanyName:   items.CompanyName,
			ContactPerson: items.ContactPerson,
			Designation:   items.Designation,
			Email:         items.Email,
			PhoneNo:       items.PhoneNo,
			CellNo:        items.CellNo,
			OfficeAddress: items.OfficeAddress,
			City:          items.City,
			State:         items.State,
			Country:       items.Country,
			Pincode:       items.Pincode,
			MainGSTIN:     items.MainGSTIN,
			CST:           items.CST,
			CreditInDays:  items.CreditInDays,
			CreditLimit:   items.CreditLimit,
			PAN:           items.PAN,
			ECC:           items.ECC,
			TIN:           items.TIN,
			Notes:         items.Notes,
			//GST:              items.GST,
			PGST:             items.PGST,
			ARN:              items.ARN,
			Introduction:     items.Introduction,
			WarehouseAddress: items.WarehouseAddress,
			WCity:            items.WCity,
			WState:           items.WState,
			WCountry:         items.WCountry,
			WPincode:         items.WPincode,
			GSTIN:            items.GSTIN,
			NewGenCode:       newGenCode,
		}

		uniqueStatus := models.CheckForExistingSupplierUniqueCode(items.Code, _orgId)
		if uniqueStatus == 1 {
			flash.Error("Product Code not unique")
			flash.Store(&c.Controller)
			c.Data["ErrorsPresent"] = true
			return
		}

		uniqueNameS := models.CheckForExistingSupplierName(items.CompanyName, _orgId)
		if uniqueNameS == 1 {
			flash.Error("Product Name already exists")
			flash.Store(&c.Controller)
			c.Data["ErrorsPresent"] = true
			return
		}

		contactpersons := make([]map[string]string, 0, 3)
		c.Ctx.Input.Bind(&contactpersons, "contactgrp")

		warehouses := make([]map[string]string, 0, 3)
		c.Ctx.Input.Bind(&warehouses, "group-a")

		result := models.AddSuppliers(formStruct, warehouses, contactpersons)
		if result == 0 {
			c.Data["ErrorsPresent"] = true
			return
		}

		// warehouses := make([]map[string]string, 0, 3)
		// c.Ctx.Input.Bind(&warehouses, "group-a")
		// //fmt.Println("*********Bind Complete")
		// for _, it := range warehouses {

		// 	if it["warehouse_address"] != "" {

		// 		formStruct1 := models.AddSupplier{
		// 			Orgid:         _orgId,
		// 			Username:      username,
		// 			Userid:        user_id,
		// 			Code:          items.Code,
		// 			CompanyName:   items.CompanyName,
		// 			ContactPerson: items.ContactPerson,
		// 			Designation:   items.Designation,
		// 			Email:         items.Email,
		// 			PhoneNo:       items.PhoneNo,
		// 			CellNo:        items.CellNo,
		// 			OfficeAddress: items.OfficeAddress,
		// 			City:          items.City,
		// 			State:         items.State,
		// 			Country:       items.Country,
		// 			Pincode:       items.Pincode,
		// 			CST:           items.CST,
		// 			PAN:           items.PAN,
		// 			ECC:           items.ECC,
		// 			TIN:           items.TIN,
		// 			Notes:         items.Notes,
		// 			//GST:              items.GST,
		// 			PGST:             items.PGST,
		// 			ARN:              items.ARN,
		// 			Introduction:     items.Introduction,
		// 			WarehouseAddress: it["warehouse_address"],
		// 			WCity:            it["wcity"],
		// 			WState:           it["wstate"],
		// 			WCountry:         it["wcountry"],
		// 			WPincode:         it["wpincode"],
		// 			GSTIN:            it["gstin"],
		// 		}

		// 		insertStatus := models.InsertWarehouses(formStruct1)
		// 		if insertStatus == 0 {
		// 			flash.Error("An error occured while saving. Please try again.")
		// 			flash.Store(&c.Controller)
		// 			c.Data["ErrorsPresent"] = true
		// 			return
		// 		}
		// 	}
		// }
		c.Data["completeRegister"] = true
		c.Redirect("/suppliers/add-view", 303)
	}
}

func (c *SuppliersController) EditSuppliers() {
	flash := beego.ReadFromRequest(&c.Controller)
	if ok := flash.Data["error"]; ok != "" {
		c.Data["FlasErrors"] = ok
	}
	if ok := flash.Data["notice"]; ok != "" {
		// Display error messages
		c.Data["FlashNotices"] = ok
	}
	var user_id string
	var _orgId string
	var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}
	c.Data["username"] = username
	c.Data["userId"] = user_id
	c.TplName = "suppliers/edit-suppliers.html"
	c.Data["Suppliers"] = 1
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Edit Suppliers"

	c.Data["Retrieved"] = models.GetActiveDropdownListOfSuppliersByOrgid(_orgId)

	if c.Ctx.Input.Method() == "GET" {
		supplierPkID := c.GetString("supplierCode")
		flash := beego.NewFlash()
		items := addSupplierValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			return
		}
		_, c.Data["FormItems"] = models.GetSupplierDetailsByCompanyID(supplierPkID, _orgId)
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

		retrievedcpersons := make(map[string]orm.Params)
		retrievedcpersons = models.GetSupplierContactPersonsByPkId(supplierPkID, _orgId)
		c.Data["CPersonsRetrieved"] = retrievedcpersons

		retrievedwarehouses := make(map[string]orm.Params)
		retrievedwarehouses = models.GetSupplierWarehousesByPkId(supplierPkID, _orgId)
		c.Data["WarehousesRetrieved"] = retrievedwarehouses

		return
	}
	if c.Ctx.Input.Method() == "POST" {
		var buttonSubmit string
		c.Ctx.Input.Bind(&buttonSubmit, "supplier-button")
		if buttonSubmit == "show" {
			// flash := beego.NewFlash()
			// items := addSupplierValidate{}
			// if err := c.ParseForm(&items); err != nil {
			// 	flash.Error("Cannot parse form")
			// 	flash.Store(&c.Controller)
			// 	return
			// }
			flash := beego.NewFlash()
			var recordPresentflag int64
			supplierId := c.GetString("supplier_name_check")
			recordPresentflag, c.Data["FormItems"] = models.GetSupplierDetailsByCompanyID(supplierId, _orgId)
			if recordPresentflag == 0 {
				flash.Error("No detials present for the chosen supplier.")
				flash.Store(&c.Controller)
				return
			}
			c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

			retrievedcpersons := make(map[string]orm.Params)
			retrievedcpersons = models.GetSupplierContactPersonsByCompanyID(supplierId, _orgId)
			c.Data["CPersonsRetrieved"] = retrievedcpersons

			retrievedwarehouses := make(map[string]orm.Params)
			retrievedwarehouses = models.GetSupplierWarehousesByCompanyID(supplierId, _orgId)
			c.Data["WarehousesRetrieved"] = retrievedwarehouses

			return
		}

		flash := beego.NewFlash()
		supplierDetailStruct := EditSupplierValidate{}
		if err := c.ParseForm(&supplierDetailStruct); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			return
		}
		c.Data["FormItems"] = supplierDetailStruct
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		valid := validation.Validation{}
		if b, _ := valid.Valid(&supplierDetailStruct); !b {
			c.Data["Errors"] = valid.ErrorsMap
			c.Data["ErrorsPresent"] = true
			return
		}

		// get all contact-person form items
		contacts := make([]map[string]string, 0, 5)
		c.Ctx.Input.Bind(&contacts, "contactgrp")

		// get all warehouse form items
		warehouses := make([]map[string]string, 0, 5)
		c.Ctx.Input.Bind(&warehouses, "warehouse-group")

		checkSupplierStatus := models.CheckForExistingSupplierNameAgainstId(supplierDetailStruct.CompanyName, _orgId, supplierDetailStruct.SupplierID)
		if checkSupplierStatus == 1 {
			c.Data["SupplierNameExists"] = true
			return
		}

		checkCodeStatus := models.CheckForExistingSupplierCodeAgainstId(supplierDetailStruct.CompanyName, _orgId, supplierDetailStruct.SupplierID)
		if checkCodeStatus == 1 {
			c.Data["SupplierCodeExists"] = true
			return
		}
		supplierDetails := models.SupplierDetails(supplierDetailStruct)

		insertStatus := models.UpdateSupplierBySupplierStruct(supplierDetails, contacts, warehouses, _orgId)
		if insertStatus == 0 {
			flash.Error("There was an error updating the detials. Please try again.")
			flash.Store(&c.Controller)
			return
		}
		if insertStatus == 2 {
			flash.Error("There was an error updating contact or warehouse details.")
			flash.Store(&c.Controller)
			return
		}
		c.Data["completeRegister"] = true
		flash.Notice("Supplier details updated.")
		flash.Store(&c.Controller)
		c.Redirect("/suppliers/edit?supplierCode="+supplierDetailStruct.SupplierID, 303)
	}
}

func (c *SuppliersController) GetSupplierContactPerson() {
	//var user_id string
	var _orgId string
	//var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		//user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		//username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		//user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		//username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}

	c.TplName = "suppliers/add-view.html"
	c.Data["Suppliers"] = 1
	c.Data["PageTitle"] = "Add New / View"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

	if c.Ctx.Input.Method() == "GET" {

		supplier_id := c.GetString("supplierId")
		//result := models.DeleteSuppliers(supplier_id, _orgId)
		name, designation, email, phone, mobile, status := models.GetSupplierContactPersonAjax(supplier_id, _orgId)
		outputObj := &scp_struct{
			Name:        name,
			Designation: designation,
			Email:       email,
			Phone:       phone,
			Mobile:      mobile,
			Status:      status,
		}
		b, _ := json.Marshal(outputObj)
		c.Ctx.Output.Body([]byte(b))
	}
}

func (c *SuppliersController) GetSupplierWarehouses() {
	//var user_id string
	var _orgId string
	//var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		//user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		//username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		//user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		//username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}

	c.TplName = "suppliers/add-view.html"
	c.Data["Suppliers"] = 1
	c.Data["PageTitle"] = "Add New / View"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

	if c.Ctx.Input.Method() == "GET" {

		supplier_id := c.GetString("supplierId")
		//result := models.DeleteSuppliers(supplier_id, _orgId)
		address, city, state, country, pincode, gstin, status := models.GetSupplierWarehouses(supplier_id, _orgId)
		outputObj := &warehouse_struct{
			Address: address,
			City:    city,
			State:   state,
			Country: country,
			Pincode: pincode,
			GSTIN:   gstin,
			Status:  status,
		}
		b, _ := json.Marshal(outputObj)
		c.Ctx.Output.Body([]byte(b))
	}
}

func (c *SuppliersController) DeleteSuppliers() {
	//var user_id string
	var _orgId string
	//var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		//user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		//username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		//user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		//username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}

	c.TplName = "suppliers/add-view.html"
	c.Data["Suppliers"] = 1
	c.Data["PageTitle"] = "Add New / View"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

	if c.Ctx.Input.Method() == "GET" {

		supplier_id := c.GetString("id")
		result := models.DeleteSuppliers(supplier_id, _orgId)
		c.Ctx.Output.Body([]byte(result))
	}
}

func (c *SuppliersController) ViewSuppliers() {
	flash := beego.ReadFromRequest(&c.Controller)
	if ok := flash.Data["error"]; ok != "" {
		c.Data["FlasErrors"] = ok
	}
	if ok := flash.Data["notice"]; ok != "" {
		// Display error messages
		c.Data["FlashNotices"] = ok
	}

	var user_id string
	var _orgId string
	var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}
	c.Data["username"] = username
	formStruct1 := models.Viewassoc{
		Orgid:    _orgId,
		Username: username,
		Userid:   user_id,
	}
	RetrievedSuppliers := make(map[string]orm.Params)
	RetrievedSuppliers = models.SearchSupplier(formStruct1)
	c.Data["RetrievedSuppliers"] = RetrievedSuppliers

	retrievedItems := make(map[string]orm.Params)
	retrievedItems = models.GetAllItemsForDropdowns(_orgId)
	c.Data["RetrievedItems"] = retrievedItems

	c.TplName = "suppliers/view-manage-association.html"
	c.Data["Suppliers"] = 1
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "View/Manage Association"

	//generic code to handle post request and validation
	if c.Ctx.Input.Method() == "POST" {
		flash := beego.NewFlash()
		items := manageSupplierValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

		//user clicked on the Show Association button; show associated items
		if items.SubmitButtonVal == "supplier" {
			formStruct2 := models.Viewassoc{
				Orgid:    _orgId,
				Supplier: items.Supplier,
				//Services:           items.Services,
			}
			retrievedAssociations := make(map[string]orm.Params)
			retrievedAssociations = models.GetItemss(formStruct2)
			c.Data["RetrievedAssociations"] = retrievedAssociations
			return
		}

		//user clicked on the Associate Items button; save associations in db
		if items.SubmitButtonVal == "items" {
			valid := validation.Validation{}
			if b, _ := valid.Valid(&items); !b {
				c.Data["Errors"] = valid.ErrorsMap
				c.Data["ErrorsPresent"] = true
				return
			}
			formStruct := models.Viewassoc{
				Orgid:    _orgId,
				Supplier: items.Supplier,
				Services: items.Services,
			}
			insertStatus := models.ManageAssoc(formStruct)
			if insertStatus == 0 {
				flash.Error("An error occured while saving. Please try again.")
				flash.Store(&c.Controller)
				return
			} else if insertStatus == 1 {
				flash.Notice("The associations have been saved.")
				flash.Store(&c.Controller)
				c.Redirect("/suppliers/view-manage-association", 303)
			}
		}
	}
}

func (c *SuppliersController) DeleteSuppliersAssoc() {
	//var user_id string
	var _orgId string
	//var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		//user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		//username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		//user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		//username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}

	c.TplName = "suppliers/view-manage-association.html"
	c.Data["Suppliers"] = 1
	c.Data["PageTitle"] = "View/Manage Association"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

	if c.Ctx.Input.Method() == "GET" {

		assoc_id := c.GetString("id")
		result := models.DeleteSuppliersAssoc(assoc_id, _orgId)
		c.Ctx.Output.Body([]byte(result))
	}
}

func (c *SuppliersController) UpdateAssociation() {
	//var user_id string
	var _orgId string
	//var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		//user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		//username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		//user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		//username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}

	c.TplName = "suppliers/view-manage-association.html"
	c.Data["Suppliers"] = 1
	c.Data["PageTitle"] = "View/Manage Association"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

	if c.Ctx.Input.Method() == "GET" {

		item_id := c.GetString("id")
		new_qty := c.GetString("new_qty")

		result := models.UpdateAssoc(item_id, new_qty, _orgId)
		c.Ctx.Output.Body([]byte(result))
	}
}
