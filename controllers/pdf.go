package controllers

import (
	"fmt"
	"html/template"
	
	"github.com/jung-kurt/gofpdf"
)

type PdfController struct {
	BaseController
}

func (c *PdfController) GeneratePDF() {
	fmt.Println("***********Inside GeneratePDF")

	c.TplName = "master_pdf.html"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Generate pdf"

	pdf := gofpdf.New("P", "mm", "A4", "")
	pdf.AddPage()
	pdf.SetFont("Arial", "B", 16)
	pdf.Cell(40, 10, "Hello, world")
	//pdf.Output(w io.Writer)
	// pdf.OutputFileAndClose("hello.pdf")
	//fileName := "tmp/" + pdf.OutputFileAndClose("hello.pdf")

	// http.Handle("/", http.FileServer(http.Dir("./tmp")))
	// http.ListenAndServe(":8080", nil)

	//fmt.Println(err)
	// fmt.Println("*****Print******")

	// fileStr := example.Filename("basic")
	// err := pdf.OutputFileAndClose(fileStr)
	// example.Summary(err, fileStr)

	// pdf := gopdf.GoPdf{}
	// pdf.Start(gopdf.Config{PageSize: gopdf.Rect{W: 595.28, H: 841.89}}) //595.28, 841.89 = A4
	// pdf.AddPage()
	// var err error
	// err = pdf.AddTTFFont("loma", "../ttf/Loma.ttf")
	// if err != nil {
	// 	log.Print(err.Error())
	// 	return
	// }

	// //pdf.Image("../imgs/gopher.jpg", 200, 50, nil) //print image
	// err = pdf.SetFont("loma", "", 14)
	// if err != nil {
	// 	log.Print(err.Error())
	// 	return
	// }
	// pdf.SetX(250) //move current location
	// pdf.SetY(200)
	// pdf.Cell(nil, "gopher and gopher") //print text

	// pdf.WritePdf("image.pdf")
}
