package controllers

import (
	"fmt"
	"html/template"
	"log"
	"os"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/caslinoerp/singulerp/models"

	"github.com/astaxie/beego"
	"github.com/gocarina/gocsv"
)

type ImportController struct {
	BaseController
}

type ItemsCSV struct {
	SrNo             string `csv:"SrNo"`
	ItemUniqueCode   string `csv:"unique_code"`
	ItemName         string `csv:"item_product_name"`
	IsProduct        string `csv:"is_product"`
	Group            string `csv:"group"`
	SubGroup         string `csv:"sub_group"`
	Grade            string `csv:"grade"`
	UOM              string `csv:"unit_measure"`
	InvoiceNarration string `csv:"invoice_narration"`
	MinStockTrigger  string `csv:"min_stock_trigger"`
	MinOrderQty      string `csv:"min_order_qty"`
	MaxStorageQty    string `csv:"max_storage_qty"`
	BinLocation      string `csv:"bin_location"`
	GeoLocation      string `csv:"geo_location"`
	LeadTime         string `csv:"lead_time_days"`
	ProductionStatus string `csv:"production_status"`
	NonConsumable    string `csv:"non_consumable"`
	GstRate          string `csv:"gst_rate"`
	CessRate         string `csv:"cess_rate"`
	HsnSac           string `csv:"hsn_sac"`
	OpeningStock     string `csv:"opening_stock"`
}

type ProductCSV struct {
	SrNo int `csv:"unique_code"`
	//ProductOrgCode   string `csv:"sudoid"`
	ProductOrgName   string `csv:"name"`
	Group            string `csv:"group"`
	SubGroup         string `csv:"sub_group"`
	UOM              string `csv:"unit_of_measure"`
	InvoiceNarration string `csv:"invoice_narration"`
	MinStockKeep     string `csv:"min_stock_trigger"`
	RatePerUOM       string `csv:"rate_per_uom"`
	Capacity         string `csv:"measure"`
	Transport        string `csv:"transport"` //not in original format
	ProductType      string `csv:"product_type"`
	FactoryLoc       string `csv:"Geo_location"`
}

type ProductBomCSV struct {
	SrNo          string `csv:"SrNo"`
	ProductCode   string `csv:"product_unique_code"`
	ItemCode      string `csv:"item_unique_code"`
	Quantity      string `csv:"quantity"`
	Wastage       string `csv:"inherent_scrap"`
	NonConsumable string `csv:"is_non_consumable"`
	// UOM              string `csv:"unit_of_measure"`
	// InvoiceNarration string `csv:"invoice_narration"`
	// MinStockKeep     string `csv:"min_stock"`
	// RatePerUOM       string `csv:"rate_per_uom"`
	// Capacity         string `csv:"capacity"`
	// Transport        string `csv:"transport"`
	// ProductType      string `csv:"product_type"`
}

type SuppliersCSV struct {
	SrNo             string `csv:"SrNo"`
	Code             string `csv:"unique_code"`
	CompanyName      string `csv:"company_name"`
	ContactPerson    string `csv:"contact_name"`
	Designation      string `csv:"designation"`
	Email            string `csv:"email"`
	PhoneNo          string `csv:"phone_number"`
	CellNo           string `csv:"cell_number"`
	OfficeAddress    string `csv:"address"`
	City             string `csv:"city"`
	State            string `csv:"state"`
	Country          string `csv:"country"`
	Pincode          string `csv:"postcode"`
	CST              string `csv:"CST"`
	PAN              string `csv:"PAN"`
	ECC              string `csv:"ECC"`
	CreditInDays     string `csv:"credit_limit_days"`
	CreditLimit      string `csv:"credit_limit"`
	TIN              string `csv:"lST"`
	Notes            string `csv:"Notes"`
	GSTIN            string `csv:"GSTIN"`
	PGST             string `csv:"ProvisionalGST"`
	ARN              string `csv:"ARN"`
	Introduction     string `csv:"date_of_introduction"`
	WarehouseAddress string `csv:"address_2"` //not in original format
	WCity            string `csv:"wcity"`     //not in original format
	WState           string `csv:"wstate"`    //not in original format
	WCountry         string `csv:"wcountry"`  //not in original format
	WPincode         string `csv:"wpincode"`  //not in original format

}

type CustomersCSV struct {
	SrNo          string `csv:"SrNo"`
	UniqueCode    string `csv:"unique_code"`
	CompanyName   string `csv:"company_name"`
	Group         string `csv:"group_id"` //not in original format
	Address       string `csv:"address"`
	City          string `csv:"city"`
	State         string `csv:"state"`
	Country       string `csv:"country"`
	Pincode       string `csv:"postcode"`
	TIN           string `csv:"lST"`
	AreaCode      string `csv:"area_code"` //not in original format
	CST           string `csv:"CST"`
	PAN           string `csv:"PAN"`
	ECC           string `csv:"ECC"`
	CreditInDays  string `csv:"credit_limit_days"`
	CreditLimit   string `csv:"credit_limit"`
	CustomerType  string `csv:"customer_type"` //not in original format
	Introduction  string `csv:"date_of_introduction"`
	GST           string `csv:"GSTIN"`
	PGST          string `csv:"ProvisionalGST"`
	ARN           string `csv:"ARN"`
	ServiceLoc    string `csv:"location"`
	ContactPerson string `csv:"contact_name"`
	Designation   string `csv:"designation"`
	Email         string `csv:"email"`
	PhoneNo       string `csv:"phone_number"`
	CellNo        string `csv:"cell_number"`

	WarehouseAddress string `csv:"address_2"` //not in original format
	WCity            string `csv:"wcity"`     //not in original format
	WState           string `csv:"wstate"`    //not in original format
	WCountry         string `csv:"wcountry"`  //not in original format
	WPincode         string `csv:"wpincode"`  //not in original format
}

func (c *ImportController) CustomerCSV() {
	c.TplName = "import/import-customers.html"
	c.Data["Settings"] = 1
	c.Data["PageTitle"] = "Import Customers"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	flash := beego.NewFlash()

	// user_id := ""
	_orgID := ""
	username := ""
	var localetimezone string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		// user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgID = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
		localetimezone = fmt.Sprintf("%s", sessionVar.Get("LocaleTimeZone"))
	} else {
		//user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgID = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
		localetimezone = fmt.Sprintf("%s", c.Ctx.Input.GetData("LocaleTimeZone"))
	}
	c.Data["username"] = username
	c.Data["ItemErrorMap"] = "0"

	if c.Ctx.Input.Method() == "POST" {

		file, header, err := c.GetFile("customersFile")
		if file != nil && err == nil {
			//log.Println("Filename", header.Filename)
		} else {
			log.Println("File upload error:", err)
			flash.Error("There was a problem uploading your file.")
			flash.Store(&c.Controller)
			return
		}
		fileName := "tmp/" + header.Filename
		c.SaveToFile("customersFile", fileName)
		customersFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE, os.ModePerm)
		if err != nil {
			panic(err)
		}
		defer customersFile.Close()

		items := []*CustomersCSV{}

		if err := gocsv.UnmarshalFile(customersFile, &items); err != nil {
			panic(err)
		}
		c.Data["CsvCustomers"] = items
		// succesfully put all rows into struct. now range though every row and insert
		type ErrorStruct struct {
			CustomerNameErr, CustomerUniqueCodeErr, CustomerRequiredNameErr string
			InsertStatus                                                    int
		}
		errorMap := make(map[string]*ErrorStruct)
		i := 0
		AddedNumber := 0
		for _, customerDetails := range items {
			duplicateflag := 0
			customerStruct := models.Customer{}
			errorMap[customerDetails.SrNo] = &ErrorStruct{}

			newGenCode, newCode := c.GetGeneratedCode(_orgID, 4, localetimezone)
			if customerDetails.UniqueCode != "" {
				customerStruct.UniqueCode = customerDetails.UniqueCode
			} else {
				customerStruct.UniqueCode = newGenCode
			}
			customerStruct.CustomerUniqueSequence = newCode

			//if models.CheckForExistingCustomerCode(customerDetails.UniqueCode, _orgID) == 1 {
			if models.CheckForExistingGenCode(customerDetails.UniqueCode, 4, _orgID) == 1 {
				errorMap[customerDetails.SrNo].CustomerUniqueCodeErr = "Code already used"
				// fmt.Print(" ")
				// fmt.Print(errorMap[itemsDetials.SrNo].ItemUniqueCodeErr)
				duplicateflag = 1
			}
			if models.CheckForExistingCustomerName(customerDetails.CompanyName, _orgID) == 1 {
				errorMap[customerDetails.SrNo].CustomerNameErr = "Name already used"
				// fmt.Print(" ")
				// fmt.Print(errorMap[itemsDetials.SrNo].ItemNameErr)
				duplicateflag = 1
			}
			if customerDetails.CompanyName == "" {
				errorMap[customerDetails.SrNo].CustomerRequiredNameErr = "Name required"
				// fmt.Print(" ")
				// fmt.Print(errorMap[itemsDetials.SrNo].ItemNameErr)
				duplicateflag = 1
			}

			//customerStruct.UniqueCode = customerDetails.UniqueCode
			customerStruct.CompanyName = customerDetails.CompanyName
			customerStruct.Address = customerDetails.Address
			customerStruct.City = customerDetails.City
			customerStruct.State = customerDetails.State
			customerStruct.Country = customerDetails.Country
			customerStruct.Pincode = customerDetails.Pincode

			if customerDetails.TIN != "" {
				customerStruct.TIN = customerDetails.TIN
			}
			if customerDetails.AreaCode != "" {
				customerStruct.AreaCode = customerDetails.AreaCode
			}
			if customerDetails.CST != "" {
				customerStruct.CST = customerDetails.CST
			}
			if customerDetails.PAN != "" {
				customerStruct.PAN = customerDetails.PAN
			}
			if customerDetails.ECC != "" {
				customerStruct.ECC = customerDetails.ECC
			}
			if customerDetails.CreditInDays != "" {
				customerStruct.CreditInDays = customerDetails.CreditInDays
			} else {
				customerStruct.CreditInDays = "0"
			}
			if customerDetails.CreditLimit != "" {
				customerStruct.CreditLimit = customerDetails.CreditLimit
			}

			customerStruct.CustomerType = customerDetails.CustomerType
			if customerDetails.Introduction != "" && customerDetails.Introduction != "nil" {
				var s string
				var datetags []string
				datetags = strings.Split(customerDetails.Introduction, "/")
				//customerStruct.Introduction = customerDetails.Introduction
				s = datetags[2] + "-" + datetags[1] + "-" + datetags[0]
				customerStruct.Introduction = s
			} else {
				customerStruct.Introduction = ""
			}

			customerStruct.ServiceLoc = customerDetails.ServiceLoc

			customerStruct.ContactPerson = customerDetails.ContactPerson

			if customerDetails.Designation != "" {
				customerStruct.Designation = customerDetails.Designation
			}
			if customerDetails.Email != "" {
				customerStruct.Email = customerDetails.Email
			}
			if customerDetails.PhoneNo != "" {
				customerStruct.PhoneNo = customerDetails.PhoneNo
			}
			if customerDetails.CellNo != "" {
				customerStruct.CellNo = customerDetails.CellNo
			}
			if customerDetails.GST != "" {
				customerStruct.GST = customerDetails.GST
			}
			if customerDetails.PGST != "" {
				customerStruct.PGST = customerDetails.PGST
			}
			if customerDetails.ARN != "" {
				customerStruct.ARN = customerDetails.ARN
			}
			if customerDetails.WarehouseAddress != "" {
				customerStruct.WarehouseAddress = customerDetails.WarehouseAddress
			}

			customerStruct.WCity = customerDetails.WCity
			customerStruct.WState = customerDetails.WState
			customerStruct.WCountry = customerDetails.WCountry
			customerStruct.WPincode = customerDetails.WPincode
			customerStruct.Orgid = _orgID
			if duplicateflag == 0 {
				dbStatus := models.AddImportCustomer(customerStruct)
				fmt.Println("dbStatus= ", dbStatus)
				if dbStatus == 0 {
					errorMap[customerDetails.SrNo].InsertStatus = 0
				} else { //is dbStatus == 1
					delete(errorMap, customerDetails.SrNo)
					AddedNumber++
				}
				//errorMap[customerDetails.SrNo].InsertStatus = models.AddImportCustomer(customerStruct)
				// fmt.Print(" ")
				// fmt.Println(errorMap[itemsDetials.SrNo].InsertStatus)
			}
			i++
		}
		//fmt.Println(errorMap)
		if len(errorMap) == 0 {
			c.Data["ItemErrorMap"] = "nil"
		} else {
			c.Data["ItemErrorMap"] = errorMap
		}
		c.Data["SuccessEntries"] = AddedNumber
		c.Data["TotalEntries"] = i

		if _, err := customersFile.Seek(0, 0); err != nil { // Go to the start of the file
			panic(err)
		}
		return

	}
}

func (c *ImportController) ImportItemsProducts() {
	c.TplName = "import/import-items.html"
	c.Data["Settings"] = 1
	c.Data["PageTitle"] = "Import Items"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	flash := beego.NewFlash()

	// user_id := ""
	_orgID := ""
	username := ""
	var localetimezone string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		// user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgID = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
		localetimezone = fmt.Sprintf("%s", sessionVar.Get("LocaleTimeZone"))
	} else {
		//user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgID = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
		localetimezone = fmt.Sprintf("%s", c.Ctx.Input.GetData("LocaleTimeZone"))
	}
	c.Data["username"] = username
	c.Data["ItemErrorMap"] = "0"

	if c.Ctx.Input.Method() == "POST" {

		file, header, err := c.GetFile("itemsFile")
		if file != nil && err == nil {
			//log.Println("Filename", header.Filename)
		} else {
			log.Println("File upload error:", err)
			flash.Error("There was a problem uploading your file.")
			flash.Store(&c.Controller)
			return
		}
		fileName := "tmp/" + header.Filename
		c.SaveToFile("itemsFile", fileName)
		itemsFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE, os.ModePerm)
		if err != nil {
			panic(err)
		}
		defer itemsFile.Close()

		items := []*ItemsCSV{}

		if err := gocsv.UnmarshalFile(itemsFile, &items); err != nil {
			panic(err)
		}
		c.Data["CsvItems"] = items
		// succesfully put all rows into struct. now range though every row and insert
		type ErrorStruct struct {
			ItemUniqueCodeErr, ItemNameErr, ItemNameRequiredErr, Name string
			InsertStatus                                              int
		}
		errorMap := make(map[string]*ErrorStruct)
		i := 0

		AddedNumber := 0
		for _, itemsDetails := range items {
			duplicateflag := 0
			itemStruct := models.StockItems{}
			errorMap[itemsDetails.SrNo] = &ErrorStruct{}

			// fmt.Print(itemsDetails.SrNo)
			// fmt.Print(" ")

			// itemStruct.ItemCode = ""
			// if itemsDetails.ItemUniqueCode == "" {
			// 	stringOfI := strconv.Itoa(i)
			// 	itemStruct.ItemCode = "I" + stringOfI
			// } else {
			//item_code := strconv.Itoa(itemsDetails.SrNo)
			//var newGenCode, newCode string
			//if itemsDetails.IsProduct == "1" {
			//	newGenCode, newCode = c.GetGeneratedCode(_orgID, 2, localetimezone)
			//} else {
			newGenCode, newCode := c.GetGeneratedCode(_orgID, 1, localetimezone)
			//}
			if itemsDetails.ItemUniqueCode != "" {
				itemStruct.ItemCode = itemsDetails.ItemUniqueCode
			} else {
				itemStruct.ItemCode = newGenCode
			}
			itemStruct.ItemUniqueSequence = strings.ToUpper(newCode)
			// }
			//if models.CheckForExistingItemUniqueCode(itemStruct.ItemCode, _orgID) == 1 {
			if models.CheckForExistingGenCode(itemStruct.ItemCode, 1, _orgID) == 1 {
				errorMap[itemsDetails.SrNo].ItemUniqueCodeErr = "Code already used"
				errorMap[itemsDetails.SrNo].Name = itemsDetails.ItemName
				// fmt.Print(" ")
				// fmt.Print(errorMap[itemsDetails.SrNo].ItemUniqueCodeErr)
				duplicateflag = 1
			}

			checkCodeStatus := models.CheckForUserDefinedItemCode(itemStruct.ItemCode, _orgID)
			if checkCodeStatus == 1 {
				errorMap[itemsDetails.SrNo].ItemUniqueCodeErr = "Code already used"
				errorMap[itemsDetails.SrNo].Name = itemsDetails.ItemName
				//c.Data["ItemCodeExists"] = true
				//return
				duplicateflag = 1
			}

			checkUserDefinedCodeStatus := models.CheckForExistingItemUniqueCode(newCode, _orgID)
			if checkUserDefinedCodeStatus == 1 {
				errorMap[itemsDetails.SrNo].ItemUniqueCodeErr = "Code already used"
				errorMap[itemsDetails.SrNo].Name = itemsDetails.ItemName
				duplicateflag = 1
				//c.Data["ItemCodeExists"] = true
				//return
			}

			if models.CheckForExistingItemName(itemsDetails.ItemName, _orgID) == 1 {
				errorMap[itemsDetails.SrNo].ItemNameErr = "Name already used"
				errorMap[itemsDetails.SrNo].Name = itemsDetails.ItemName
				// fmt.Print(" ")
				// fmt.Print(errorMap[itemsDetails.SrNo].ItemNameErr)
				duplicateflag = 1
			}
			if itemsDetails.ItemName == "" {
				errorMap[itemsDetails.SrNo].ItemNameRequiredErr = "Name required"
				errorMap[itemsDetails.SrNo].Name = itemsDetails.ItemName
				// fmt.Print(" ")
				// fmt.Print(errorMap[itemsDetails.SrNo].ItemNameErr)
				duplicateflag = 1
			}

			if duplicateflag == 0 {
				itemStruct.ItemName = strings.Title(itemsDetails.ItemName)

				if itemsDetails.IsProduct != "1" {
					itemStruct.IsProduced = "0"
				} else {
					itemStruct.IsProduced = itemsDetails.IsProduct
				}

				itemStruct.Group = strings.ToUpper(itemsDetails.Group)
				itemStruct.SubGroup = strings.ToUpper(itemsDetails.SubGroup)
				itemStruct.ItemGrade = strings.ToUpper(itemsDetails.Grade)
				itemStruct.UOM = strings.Title(itemsDetails.UOM)

				if itemsDetails.InvoiceNarration == "" {
					itemStruct.InvoiceNarration = strings.Title(itemsDetails.ItemName)
				} else {
					itemStruct.InvoiceNarration = strings.Title(itemsDetails.InvoiceNarration)
				}

				if itemsDetails.MinStockTrigger == "" {
					itemStruct.MinStockTrigger = "100.0"
				} else {
					itemStruct.MinStockTrigger = itemsDetails.MinStockTrigger
				}

				if itemsDetails.MinOrderQty == "" {
					itemStruct.MinOrderQty = "1.0"
				} else {
					itemStruct.MinOrderQty = itemsDetails.MinOrderQty
				}

				if itemsDetails.LeadTime == "" {
					itemStruct.LeadTimeDays = "1.0"
				} else {
					itemStruct.LeadTimeDays = itemsDetails.LeadTime
				}

				if itemsDetails.MaxStorageQty == "" {
					itemStruct.MaxStorageQty = "1000.0"
				} else {
					itemStruct.MaxStorageQty = itemsDetails.MaxStorageQty
				}
				itemStruct.BinLocation = itemsDetails.BinLocation

				itemStruct.GeoLocation = itemsDetails.GeoLocation

				itemStruct.GSTRate = itemsDetails.GstRate
				itemStruct.CessRate = itemsDetails.CessRate
				itemStruct.HSNSAC = itemsDetails.HsnSac

				if itemsDetails.NonConsumable != "1" {
					itemStruct.NonConsumable = "0"
				} else {
					itemStruct.NonConsumable = itemsDetails.NonConsumable
				}

				if itemsDetails.IsProduct == "1" && itemsDetails.ProductionStatus == "" {
					itemStruct.ProductionStatus = "1" //Default status for a products
				} else if itemsDetails.IsProduct != "1" && itemsDetails.ProductionStatus == "" {
					itemStruct.ProductionStatus = "0" //Default status for a items
				} else {
					itemStruct.ProductionStatus = itemsDetails.ProductionStatus
				}

				itemStruct.Status = "1"
				itemStruct.Orgid = _orgID

				if itemsDetails.OpeningStock == "" {
					itemStruct.OpeningStock = "0"
				} else {
					itemStruct.OpeningStock = itemsDetails.OpeningStock
				}

				dbStatus := models.AddStockItemsImport(itemStruct)
				if dbStatus == 0 {
					errorMap[itemsDetails.SrNo].InsertStatus = 0
					errorMap[itemsDetails.SrNo].Name = itemsDetails.ItemName
				} else { //is dbStatus == 1
					//ADD ITEMS TO STOCK
					invUpdate := models.UpdateInventoryOnImport(itemStruct)
					if invUpdate == 1 {
						delete(errorMap, itemsDetails.SrNo)
						AddedNumber++
					}
				}
			}
			i++
		}
		if len(errorMap) == 0 {
			c.Data["ItemErrorMap"] = "nil"
		} else {
			c.Data["ItemErrorMap"] = errorMap
		}
		c.Data["SuccessEntries"] = AddedNumber
		c.Data["TotalEntries"] = i

		if _, err := itemsFile.Seek(0, 0); err != nil { // Go to the start of the file
			panic(err)
		}
		return

	}
}

func (c *ImportController) ProductCSV() {
	c.TplName = "import/import-products.html"
	c.Data["Settings"] = 1
	c.Data["PageTitle"] = "Import Products"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	flash := beego.NewFlash()

	// user_id := ""
	_orgID := ""
	username := ""
	sessionVar := c.StartSession()
	if sessionVar != nil {
		// user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgID = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
	}
	c.Data["username"] = username

	if c.Ctx.Input.Method() == "POST" {

		file, header, err := c.GetFile("productsFile")
		if file != nil && err == nil {
			//log.Println("Filename", header.Filename)
		} else {
			log.Println("File upload error:", err)
			flash.Error("There was a problem uploading your file.")
			flash.Store(&c.Controller)
			return
		}
		fileName := "tmp/" + header.Filename
		c.SaveToFile("productsFile", fileName)
		productsFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE, os.ModePerm)
		if err != nil {
			panic(err)
		}
		defer productsFile.Close()

		items := []*ProductCSV{}

		if err := gocsv.UnmarshalFile(productsFile, &items); err != nil {
			panic(err)
		}
		c.Data["CsvProducts"] = items
		// succesfully put all rows into struct. now range though every row and insert
		type ErrorStruct struct {
			ProductUniqueCodeErr, ProductNameErr string
			InsertStatus                         int
		}
		errorMap := make(map[int]*ErrorStruct)
		i := 0

		for _, productsDetails := range items {
			duplicateflag := 0
			productStruct := models.Product{}
			errorMap[productsDetails.SrNo] = &ErrorStruct{}

			// fmt.Print(itemsDetials.SrNo)
			// fmt.Print(" ")

			// productStruct.ProductCode = ""
			// if productsDetails.ProductOrgCode == "" {
			// 	stringOfI := strconv.Itoa(i)
			// 	productStruct.ProductCode = "I" + stringOfI
			// } else {
			product_code := strconv.Itoa(productsDetails.SrNo)
			productStruct.ProductCode = product_code
			// }
			if models.CheckForExistingProductUniqueCode(productStruct.ProductCode, _orgID) == 1 {
				errorMap[productsDetails.SrNo].ProductUniqueCodeErr = "Code already used"
				// fmt.Print(" ")
				// fmt.Print(errorMap[itemsDetials.SrNo].ItemUniqueCodeErr)
				duplicateflag = 1
			}
			if models.CheckForExistingProductName(productsDetails.ProductOrgName, _orgID) == 1 {
				errorMap[productsDetails.SrNo].ProductNameErr = "Name already used"
				// fmt.Print(" ")
				// fmt.Print(errorMap[itemsDetials.SrNo].ItemNameErr)
				duplicateflag = 1
			}
			if productsDetails.ProductOrgName == "" {
				errorMap[productsDetails.SrNo].ProductNameErr = "Name required"
				// fmt.Print(" ")
				// fmt.Print(errorMap[itemsDetials.SrNo].ItemNameErr)
				duplicateflag = 1
			}
			productStruct.ProductName = productsDetails.ProductOrgName
			productStruct.Group = productsDetails.Group
			productStruct.SubGroup = productsDetails.SubGroup
			productStruct.RatePerUOM = productsDetails.RatePerUOM
			productStruct.UOM = productsDetails.UOM
			if productsDetails.Capacity == "" {
				productStruct.Capacity = ""
			} else {
				productStruct.Capacity = productsDetails.Capacity
			}
			if productsDetails.Transport == "" {
				productStruct.Transport = ""
			} else {
				productStruct.Transport = productsDetails.Transport
			}
			if productsDetails.ProductType == "" {
				productStruct.ProductType = ""
			} else {
				productStruct.ProductType = productsDetails.ProductType
			}
			if productsDetails.InvoiceNarration == "" {
				productStruct.InvoiceNarration = productsDetails.ProductOrgName
			} else {
				productStruct.InvoiceNarration = productsDetails.InvoiceNarration
			}
			if productsDetails.MinStockKeep == "" {
				productStruct.MinStock = "100.0"
			} else {
				productStruct.MinStock = productsDetails.MinStockKeep
			}
			productStruct.FactoryLoc1 = productsDetails.FactoryLoc
			// if productsDetails.LeadTime == "" {
			// 	productStruct.LeadTimeDays = "1.0"
			// } else {
			// 	productStruct.LeadTimeDays = productsDetails.LeadTime
			// }
			// if productsDetails.MaxStorageQty == "" {
			// 	productStruct.MaxStorageQty = "1000.0"
			// } else {
			// 	productStruct.MaxStorageQty = productsDetails.MaxStorageQty
			// }
			//productStruct.MinOrderQty = "1.0"
			//productStruct.BinLocation = productsDetails.BinLocation
			//productStruct.GeoLocation = productsDetails.GeoLocation
			productStruct.Status1 = "1"
			productStruct.Orgid = _orgID
			if duplicateflag == 0 {
				errorMap[productsDetails.SrNo].InsertStatus = models.NewProductImport(productStruct)
				// fmt.Print(" ")
				// fmt.Println(errorMap[itemsDetials.SrNo].InsertStatus)
			}
			i++
		}
		c.Data["ItemErrorMap"] = errorMap
		if _, err := productsFile.Seek(0, 0); err != nil { // Go to the start of the file
			panic(err)
		}
		return

	}
}

func (c *ImportController) ProductBomCSV() {
	c.TplName = "import/import-product-bom.html"
	c.Data["Settings"] = 1
	c.Data["PageTitle"] = "Import Products Bom"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	flash := beego.NewFlash()

	// user_id := ""
	_orgID := ""
	username := ""
	sessionVar := c.StartSession()
	if sessionVar != nil {
		// user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgID = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
	}
	c.Data["username"] = username
	c.Data["ItemErrorMap"] = "0"

	if c.Ctx.Input.Method() == "POST" {

		file, header, err := c.GetFile("bomFile")
		if file != nil && err == nil {
			//log.Println("Filename", header.Filename)
		} else {
			log.Println("File upload error:", err)
			flash.Error("There was a problem uploading your file.")
			flash.Store(&c.Controller)
			return
		}
		fileName := "tmp/" + _orgID + "_BOM" + "_" + header.Filename + "_" + time.Now().Format(time.UnixDate)
		c.SaveToFile("bomFile", fileName)
		bomFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE, os.ModePerm)
		if err != nil {
			panic(err)
		}
		defer bomFile.Close()

		items := []*ProductBomCSV{}

		if err := gocsv.UnmarshalFile(bomFile, &items); err != nil {
			panic(err)
		}
		c.Data["CsvBom"] = items
		// succesfully put all rows into struct. now range though every row and insert
		type ErrorStruct struct {
			ProductCode, ItemCode, ProductCodeErr, ItemCodeErr, QuantityErr, NoProduct, NoItem, BOMExists string
			InsertStatus                                                                                  int
		}
		errorMap := make(map[string]*ErrorStruct)
		i := 0

		AddedNumber := 0
		//errorMapCount := 0
		for _, productsDetails := range items {
			duplicateflag := 0
			productStruct := models.BomImport{}
			errorMap[productsDetails.SrNo] = &ErrorStruct{}

			// product_code := strconv.Itoa(productsDetails.SrNo)
			// productStruct.ProductCode = product_code

			// if models.CheckForExistingProductUniqueCode(productStruct.ProductCode, _orgID) == 1 {
			// 	errorMap[productsDetails.SrNo].ProductUniqueCodeErr = "Code already used"
			// 	duplicateflag = 1
			// }
			// if models.CheckForExistingProductName(productsDetails.ProductOrgName, _orgID) == 1 {
			// 	errorMap[productsDetails.SrNo].ProductNameErr = "Name already used"
			// 	duplicateflag = 1
			// }
			//product_id := strconv.Itoa(productsDetails.ProductCode)
			if productsDetails.ProductCode == "" {
				errorMap[productsDetails.SrNo].ProductCodeErr = "Product Code required"
				errorMap[productsDetails.SrNo].ProductCode = productsDetails.ProductCode
				errorMap[productsDetails.SrNo].ItemCode = productsDetails.ItemCode
				duplicateflag = 1
			}
			if productsDetails.ItemCode == "" {
				errorMap[productsDetails.SrNo].ItemCodeErr = "Item Code required"
				errorMap[productsDetails.SrNo].ProductCode = productsDetails.ProductCode
				errorMap[productsDetails.SrNo].ItemCode = productsDetails.ItemCode
				duplicateflag = 1
			}
			if productsDetails.Quantity == "" {
				errorMap[productsDetails.SrNo].QuantityErr = "Quantity required"
				errorMap[productsDetails.SrNo].ProductCode = productsDetails.ProductCode
				errorMap[productsDetails.SrNo].ItemCode = productsDetails.ItemCode
				duplicateflag = 1
			}

			if duplicateflag == 0 {
				productStruct.ProductCode = productsDetails.ProductCode
				productStruct.ItemCode = productsDetails.ItemCode
				productStruct.Quantity = productsDetails.Quantity
				productStruct.Wastage = productsDetails.Wastage
				productStruct.NonConsumable = productsDetails.NonConsumable
				productStruct.Status1 = "1"
				productStruct.Orgid = _orgID

				dbStatus := models.NewImportBomProduct(productStruct)
				//errorMap[productsDetails.ProductCode].InsertStatus = models.NewImportBomProduct(productStruct)
				if dbStatus == 0 {
					errorMap[productsDetails.SrNo].InsertStatus = 0
				} else if dbStatus == 2 { // Product not present
					errorMap[productsDetails.SrNo].NoProduct = "Product Absent"
					errorMap[productsDetails.SrNo].ProductCode = productsDetails.ProductCode
					errorMap[productsDetails.SrNo].ItemCode = productsDetails.ItemCode
				} else if dbStatus == 3 { // Item not present
					errorMap[productsDetails.SrNo].NoItem = "Item Absent"
					errorMap[productsDetails.SrNo].ProductCode = productsDetails.ProductCode
					errorMap[productsDetails.SrNo].ItemCode = productsDetails.ItemCode
				} else if dbStatus == 4 { // BOM already exists
					errorMap[productsDetails.SrNo].ProductCode = productsDetails.ProductCode
					errorMap[productsDetails.SrNo].ItemCode = productsDetails.ItemCode
					errorMap[productsDetails.SrNo].BOMExists = "BOM Already Exists"
				} else { //is dbStatus == 1
					AddedNumber++
					delete(errorMap, productsDetails.SrNo)
				}
				// fmt.Print(" ")
				// fmt.Println(errorMap[itemsDetials.SrNo].InsertStatus)
			}
			i++
			//errorMapCount++
		}
		if len(errorMap) == 0 {
			c.Data["ItemErrorMap"] = "nil"
		} else {
			c.Data["ItemErrorMap"] = errorMap
		}
		c.Data["SuccessEntries"] = AddedNumber
		c.Data["TotalEntries"] = i
		if _, err := bomFile.Seek(0, 0); err != nil { // Go to the start of the file
			panic(err)
		}
		return

	}
}

func (c *ImportController) SupplierCSV() {
	c.TplName = "import/import-suppliers.html"
	c.Data["Settings"] = 1
	c.Data["PageTitle"] = "Import Suppliers"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	flash := beego.NewFlash()

	// user_id := ""
	_orgID := ""
	username := ""
	var localetimezone string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		// user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgID = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
		localetimezone = fmt.Sprintf("%s", sessionVar.Get("LocaleTimeZone"))
	} else {
		//user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgID = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
		localetimezone = fmt.Sprintf("%s", c.Ctx.Input.GetData("LocaleTimeZone"))
	}
	c.Data["username"] = username
	c.Data["ItemErrorMap"] = "0"

	if c.Ctx.Input.Method() == "POST" {

		file, header, err := c.GetFile("suppliersFile")
		if file != nil && err == nil {
			//log.Println("Filename", header.Filename)
		} else {
			log.Println("File upload error:", err)
			flash.Error("There was a problem uploading your file.")
			flash.Store(&c.Controller)
			return
		}
		fileName := "tmp/" + header.Filename
		c.SaveToFile("suppliersFile", fileName)
		suppliersFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE, os.ModePerm)
		if err != nil {
			panic(err)
		}
		defer suppliersFile.Close()

		items := []*SuppliersCSV{}

		if err := gocsv.UnmarshalFile(suppliersFile, &items); err != nil {
			panic(err)
		}
		c.Data["CsvSuppliers"] = items
		// succesfully put all rows into struct. now range though every row and insert
		type ErrorStruct struct {
			SupplierUniqueCodeErr, SupplierNameErr, SupplierNameRequiredErr string
			InsertStatus                                                    int
		}
		errorMap := make(map[string]*ErrorStruct)
		i := 0
		AddedNumber := 0
		for _, supplierDetails := range items {
			duplicateflag := 0
			supplierStruct := models.AddSupplier{}
			errorMap[supplierDetails.SrNo] = &ErrorStruct{}

			// fmt.Print(itemsDetials.SrNo)
			// fmt.Print(" ")

			// supplierStruct.Code = ""
			// if supplierDetails.Code == "" {
			// 	stringOfI := strconv.Itoa(i)
			// 	supplierStruct.Code = "I" + stringOfI
			// } else {
			//sup_code := strconv.Itoa(supplierDetails.Code)
			//supplierStruct.Code = supplierDetails.Code
			// }
			newGenCode, newCode := c.GetGeneratedCode(_orgID, 3, localetimezone)
			if supplierDetails.Code != "" {
				supplierStruct.Code = supplierDetails.Code
			} else {
				supplierStruct.Code = newGenCode
			}
			supplierStruct.SupplierUniqueSequence = newCode
			//if models.CheckForExistingSupplierUniqueCode(supplierStruct.Code, _orgID) == 1 {
			if models.CheckForExistingGenCode(supplierStruct.Code, 3, _orgID) == 1 {
				errorMap[supplierDetails.SrNo].SupplierUniqueCodeErr = "Code already used"
				// fmt.Print(" ")
				// fmt.Print(errorMap[itemsDetials.SrNo].ItemUniqueCodeErr)
				duplicateflag = 1
			}
			if models.CheckForExistingSupplierName(supplierDetails.CompanyName, _orgID) == 1 {
				errorMap[supplierDetails.SrNo].SupplierNameErr = "Name already used"
				// fmt.Print(" ")
				// fmt.Print(errorMap[itemsDetials.SrNo].ItemNameErr)
				duplicateflag = 1
			}
			if supplierDetails.CompanyName == "" {
				errorMap[supplierDetails.SrNo].SupplierNameRequiredErr = "Name required"
				// fmt.Print(" ")
				// fmt.Print(errorMap[itemsDetials.SrNo].ItemNameErr)
				duplicateflag = 1
			}

			if duplicateflag == 0 {
				supplierStruct.CompanyName = supplierDetails.CompanyName
				supplierStruct.ContactPerson = supplierDetails.ContactPerson
				supplierStruct.Designation = supplierDetails.Designation
				supplierStruct.Email = supplierDetails.Email
				supplierStruct.PhoneNo = supplierDetails.PhoneNo
				supplierStruct.CellNo = supplierDetails.CellNo
				supplierStruct.OfficeAddress = supplierDetails.OfficeAddress
				supplierStruct.City = supplierDetails.City
				supplierStruct.State = supplierDetails.State
				supplierStruct.Country = supplierDetails.Country
				supplierStruct.Pincode = supplierDetails.Pincode
				supplierStruct.CST = supplierDetails.CST
				if supplierDetails.CreditInDays != "" {
					supplierStruct.CreditInDays = supplierDetails.CreditInDays
				} else {
					supplierStruct.CreditInDays = "0"
				}
				if supplierDetails.CreditLimit != "" {
					supplierStruct.CreditLimit = supplierDetails.CreditLimit
				}
				supplierStruct.PAN = supplierDetails.PAN
				supplierStruct.ECC = supplierDetails.ECC
				supplierStruct.TIN = supplierDetails.TIN
				supplierStruct.Notes = supplierDetails.Notes
				//supplierStruct.GST = supplierDetails.GST
				supplierStruct.PGST = supplierDetails.PGST
				supplierStruct.ARN = supplierDetails.ARN
				if supplierDetails.Introduction != "" && supplierDetails.Introduction != "nil" {
					var s string
					var datetags []string
					datetags = strings.Split(supplierDetails.Introduction, "/")
					s = datetags[2] + "-" + datetags[1] + "-" + datetags[0]
					supplierStruct.Introduction = s
				} else {
					supplierStruct.Introduction = ""
				}
				supplierStruct.WarehouseAddress = supplierDetails.WarehouseAddress
				supplierStruct.MainGSTIN = supplierDetails.GSTIN
				// supplierStruct.WCity = supplierDetails.City
				// supplierStruct.WState = supplierDetails.State
				// supplierStruct.WCountry = supplierDetails.Country
				// supplierStruct.WPincode = supplierDetails.Pincode
				supplierStruct.Orgid = _orgID

				//errorMap[supplierDetails.Code].InsertStatus = models.AddImportSuppliers(supplierStruct)
				dbStatus := models.AddImportSuppliers(supplierStruct)
				if dbStatus == 0 {
					errorMap[supplierDetails.SrNo].InsertStatus = 0
				} else { //is dbStatus == 1
					delete(errorMap, supplierDetails.SrNo)
					AddedNumber++
				}

				// fmt.Print(" ")
				// fmt.Println(errorMap[itemsDetials.SrNo].InsertStatus)
			}
			i++
		}
		if len(errorMap) == 0 {
			c.Data["ItemErrorMap"] = "nil"
		} else {
			c.Data["ItemErrorMap"] = errorMap
		}
		c.Data["SuccessEntries"] = AddedNumber
		c.Data["TotalEntries"] = i

		if _, err := suppliersFile.Seek(0, 0); err != nil { // Go to the start of the file
			panic(err)
		}
		return
	}
}

func (c *ImportController) MasterImport() {
	c.TplName = "import/master-import.html"
	c.Data["Settings"] = 1
	c.Data["PageTitle"] = "Master Import"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	flash := beego.NewFlash()

	// user_id := ""
	_orgID := ""
	username := ""
	var localetimezone string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		// user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgID = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
		localetimezone = fmt.Sprintf("%s", sessionVar.Get("LocaleTimeZone"))
	} else {
		//user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgID = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
		localetimezone = fmt.Sprintf("%s", c.Ctx.Input.GetData("LocaleTimeZone"))
	}
	c.Data["username"] = username
	c.Data["ItemErrorMap"] = "0"
	c.Data["BomErrorMap"] = "0"
	c.Data["SuppliersErrorMap"] = "0"
	c.Data["CustomersErrorMap"] = "0"
	c.Data["ActiveTab"] = "tab_1_1"

	if c.Ctx.Input.Method() == "POST" {
		var importbutton string
		c.Ctx.Input.Bind(&importbutton, "config-btn")

		if importbutton == "ItemsImport" {
			c.Data["ActiveTab"] = "tab_1_1"
			file, header, err := c.GetFile("itemsFile")
			if file != nil && err == nil {
				//log.Println("Filename", header.Filename)
			} else {
				log.Println("File upload error:", err)
				flash.Error("There was a problem uploading your file.")
				flash.Store(&c.Controller)
				return
			}
			fileName := "tmp/" + header.Filename
			c.SaveToFile("itemsFile", fileName)
			itemsFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE, os.ModePerm)
			if err != nil {
				panic(err)
			}
			defer itemsFile.Close()

			items := []*ItemsCSV{}

			if err := gocsv.UnmarshalFile(itemsFile, &items); err != nil {
				panic(err)
			}
			c.Data["CsvItems"] = items
			// succesfully put all rows into struct. now range though every row and insert
			type ErrorStruct struct {
				ItemUniqueCodeErr, ItemNameErr, ItemNameRequiredErr, Name string
				InsertStatus                                              int
			}
			errorMap := make(map[string]*ErrorStruct)
			i := 0

			AddedNumber := 0
			j := 1
			for _, itemsDetails := range items {
				duplicateflag := 0
				itemStruct := models.StockItems{}
				stringOfj := strconv.Itoa(j)
				errorMap[stringOfj] = &ErrorStruct{}
				//errorMap[itemsDetails.SrNo] = &ErrorStruct{}

				// fmt.Print(itemsDetails.SrNo)
				// fmt.Print(" ")

				// itemStruct.ItemCode = ""
				// if itemsDetails.ItemUniqueCode == "" {
				// 	stringOfI := strconv.Itoa(i)
				// 	itemStruct.ItemCode = "I" + stringOfI
				// } else {
				//item_code := strconv.Itoa(itemsDetails.SrNo)
				//var newGenCode, newCode string
				//if itemsDetails.IsProduct == "1" {
				//	newGenCode, newCode = c.GetGeneratedCode(_orgID, 2, localetimezone)
				//} else {
				newGenCode, newCode := c.GetGeneratedCode(_orgID, 1, localetimezone)
				//}
				if itemsDetails.ItemUniqueCode != "" {
					itemStruct.ItemCode = itemsDetails.ItemUniqueCode
				} else {
					itemStruct.ItemCode = newGenCode
				}
				itemStruct.ItemUniqueSequence = strings.ToUpper(newCode)
				// }
				//if models.CheckForExistingItemUniqueCode(itemStruct.ItemCode, _orgID) == 1 {
				if models.CheckForExistingGenCode(itemStruct.ItemCode, 1, _orgID) == 1 {
					errorMap[stringOfj].ItemUniqueCodeErr = "Code already used"
					errorMap[stringOfj].Name = itemsDetails.ItemName
					// fmt.Print(" ")
					// fmt.Print(errorMap[itemsDetails.SrNo].ItemUniqueCodeErr)
					duplicateflag = 1
				}

				checkCodeStatus := models.CheckForUserDefinedItemCode(itemStruct.ItemCode, _orgID)
				if checkCodeStatus == 1 {
					errorMap[stringOfj].ItemUniqueCodeErr = "Code already used"
					errorMap[stringOfj].Name = itemsDetails.ItemName
					//c.Data["ItemCodeExists"] = true
					//return
					duplicateflag = 1
				}

				checkUserDefinedCodeStatus := models.CheckForExistingItemUniqueCode(newCode, _orgID)
				if checkUserDefinedCodeStatus == 1 {
					errorMap[stringOfj].ItemUniqueCodeErr = "Code already used"
					errorMap[stringOfj].Name = itemsDetails.ItemName
					duplicateflag = 1
					//c.Data["ItemCodeExists"] = true
					//return
				}

				if models.CheckForExistingItemName(itemsDetails.ItemName, _orgID) == 1 {
					errorMap[stringOfj].ItemNameErr = "Name already used"
					errorMap[stringOfj].Name = itemsDetails.ItemName
					// fmt.Print(" ")
					// fmt.Print(errorMap[itemsDetails.SrNo].ItemNameErr)
					duplicateflag = 1
				}
				if itemsDetails.ItemName == "" {
					errorMap[stringOfj].ItemNameRequiredErr = "Name required"
					errorMap[stringOfj].Name = itemsDetails.ItemName
					// fmt.Print(" ")
					// fmt.Print(errorMap[itemsDetails.SrNo].ItemNameErr)
					duplicateflag = 1
				}

				if duplicateflag == 0 {
					itemStruct.ItemName = strings.Title(itemsDetails.ItemName)

					if itemsDetails.IsProduct != "1" {
						itemStruct.IsProduced = "0"
					} else {
						itemStruct.IsProduced = itemsDetails.IsProduct
					}

					itemStruct.Group = strings.ToUpper(itemsDetails.Group)
					itemStruct.SubGroup = strings.ToUpper(itemsDetails.SubGroup)
					itemStruct.ItemGrade = strings.ToUpper(itemsDetails.Grade)
					itemStruct.UOM = strings.Title(itemsDetails.UOM)

					if itemsDetails.InvoiceNarration == "" {
						itemStruct.InvoiceNarration = strings.Title(itemsDetails.ItemName)
					} else {
						itemStruct.InvoiceNarration = strings.Title(itemsDetails.InvoiceNarration)
					}

					if itemsDetails.MinStockTrigger == "" {
						itemStruct.MinStockTrigger = "100.0"
					} else {
						itemStruct.MinStockTrigger = itemsDetails.MinStockTrigger
					}

					if itemsDetails.MinOrderQty == "" {
						itemStruct.MinOrderQty = "1.0"
					} else {
						itemStruct.MinOrderQty = itemsDetails.MinOrderQty
					}

					if itemsDetails.LeadTime == "" {
						itemStruct.LeadTimeDays = "1.0"
					} else {
						itemStruct.LeadTimeDays = itemsDetails.LeadTime
					}

					if itemsDetails.MaxStorageQty == "" {
						itemStruct.MaxStorageQty = "1000.0"
					} else {
						itemStruct.MaxStorageQty = itemsDetails.MaxStorageQty
					}
					itemStruct.BinLocation = itemsDetails.BinLocation

					itemStruct.GeoLocation = itemsDetails.GeoLocation

					itemStruct.GSTRate = itemsDetails.GstRate
					itemStruct.CessRate = itemsDetails.CessRate
					itemStruct.HSNSAC = itemsDetails.HsnSac

					if itemsDetails.NonConsumable != "1" {
						itemStruct.NonConsumable = "0"
					} else {
						itemStruct.NonConsumable = itemsDetails.NonConsumable
					}

					if itemsDetails.IsProduct == "1" && itemsDetails.ProductionStatus == "" {
						itemStruct.ProductionStatus = "1" //Default status for a products
					} else if itemsDetails.IsProduct != "1" && itemsDetails.ProductionStatus == "" {
						itemStruct.ProductionStatus = "0" //Default status for a items
					} else {
						itemStruct.ProductionStatus = itemsDetails.ProductionStatus
					}

					itemStruct.Status = "1"
					itemStruct.Orgid = _orgID

					if itemsDetails.OpeningStock == "" {
						itemStruct.OpeningStock = "0"
					} else {
						itemStruct.OpeningStock = itemsDetails.OpeningStock
					}

					dbStatus := models.AddStockItemsImport(itemStruct)
					if dbStatus == 0 {
						errorMap[stringOfj].InsertStatus = 0
						errorMap[stringOfj].Name = itemsDetails.ItemName
					} else { //is dbStatus == 1
						//ADD ITEMS TO STOCK
						invUpdate := models.UpdateInventoryOnImport(itemStruct)
						if invUpdate == 1 {
							delete(errorMap, stringOfj)
							AddedNumber++
						}
					}
				}
				i++
				j++
			}
			if len(errorMap) == 0 {
				c.Data["ItemErrorMap"] = "nil"
			} else {
				c.Data["ItemErrorMap"] = errorMap
			}
			c.Data["SuccessEntries"] = AddedNumber
			c.Data["TotalEntries"] = i

			if _, err := itemsFile.Seek(0, 0); err != nil { // Go to the start of the file
				panic(err)
			}
			return
		} else if importbutton == "BomImport" {
			c.Data["ActiveTab"] = "tab_1_2"

			file, header, err := c.GetFile("productsFile")
			if file != nil && err == nil {
				//log.Println("Filename", header.Filename)
			} else {
				log.Println("File upload error:", err)
				flash.Error("There was a problem uploading your file.")
				flash.Store(&c.Controller)
				return
			}
			fileName := "tmp/" + header.Filename
			c.SaveToFile("productsFile", fileName)
			productsFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE, os.ModePerm)
			if err != nil {
				panic(err)
			}
			defer productsFile.Close()

			items := []*ProductCSV{}

			if err := gocsv.UnmarshalFile(productsFile, &items); err != nil {
				panic(err)
			}
			c.Data["CsvProducts"] = items
			// succesfully put all rows into struct. now range though every row and insert
			type ErrorStruct struct {
				ProductUniqueCodeErr, ProductNameErr string
				InsertStatus                         int
			}
			//errorMap := make(map[int]*ErrorStruct)
			errorMap := make(map[string]*ErrorStruct)
			i := 0
			j := 1
			for _, productsDetails := range items {
				duplicateflag := 0
				productStruct := models.Product{}
				stringOfj := strconv.Itoa(j)
				//errorMap[productsDetails.SrNo] = &ErrorStruct{}
				errorMap[stringOfj] = &ErrorStruct{}

				// fmt.Print(itemsDetials.SrNo)
				// fmt.Print(" ")

				// productStruct.ProductCode = ""
				// if productsDetails.ProductOrgCode == "" {
				// 	stringOfI := strconv.Itoa(i)
				// 	productStruct.ProductCode = "I" + stringOfI
				// } else {
				product_code := strconv.Itoa(productsDetails.SrNo)
				productStruct.ProductCode = product_code
				// }
				if models.CheckForExistingProductUniqueCode(productStruct.ProductCode, _orgID) == 1 {
					errorMap[stringOfj].ProductUniqueCodeErr = "Code already used"
					// fmt.Print(" ")
					// fmt.Print(errorMap[itemsDetials.SrNo].ItemUniqueCodeErr)
					duplicateflag = 1
				}
				if models.CheckForExistingProductName(productsDetails.ProductOrgName, _orgID) == 1 {
					errorMap[stringOfj].ProductNameErr = "Name already used"
					// fmt.Print(" ")
					// fmt.Print(errorMap[itemsDetials.SrNo].ItemNameErr)
					duplicateflag = 1
				}
				if productsDetails.ProductOrgName == "" {
					errorMap[stringOfj].ProductNameErr = "Name required"
					// fmt.Print(" ")
					// fmt.Print(errorMap[itemsDetials.SrNo].ItemNameErr)
					duplicateflag = 1
				}
				productStruct.ProductName = productsDetails.ProductOrgName
				productStruct.Group = productsDetails.Group
				productStruct.SubGroup = productsDetails.SubGroup
				productStruct.RatePerUOM = productsDetails.RatePerUOM
				productStruct.UOM = productsDetails.UOM
				if productsDetails.Capacity == "" {
					productStruct.Capacity = ""
				} else {
					productStruct.Capacity = productsDetails.Capacity
				}
				if productsDetails.Transport == "" {
					productStruct.Transport = ""
				} else {
					productStruct.Transport = productsDetails.Transport
				}
				if productsDetails.ProductType == "" {
					productStruct.ProductType = ""
				} else {
					productStruct.ProductType = productsDetails.ProductType
				}
				if productsDetails.InvoiceNarration == "" {
					productStruct.InvoiceNarration = productsDetails.ProductOrgName
				} else {
					productStruct.InvoiceNarration = productsDetails.InvoiceNarration
				}
				if productsDetails.MinStockKeep == "" {
					productStruct.MinStock = "100.0"
				} else {
					productStruct.MinStock = productsDetails.MinStockKeep
				}
				productStruct.FactoryLoc1 = productsDetails.FactoryLoc
				// if productsDetails.LeadTime == "" {
				// 	productStruct.LeadTimeDays = "1.0"
				// } else {
				// 	productStruct.LeadTimeDays = productsDetails.LeadTime
				// }
				// if productsDetails.MaxStorageQty == "" {
				// 	productStruct.MaxStorageQty = "1000.0"
				// } else {
				// 	productStruct.MaxStorageQty = productsDetails.MaxStorageQty
				// }
				//productStruct.MinOrderQty = "1.0"
				//productStruct.BinLocation = productsDetails.BinLocation
				//productStruct.GeoLocation = productsDetails.GeoLocation
				productStruct.Status1 = "1"
				productStruct.Orgid = _orgID
				if duplicateflag == 0 {
					errorMap[stringOfj].InsertStatus = models.NewProductImport(productStruct)
					// fmt.Print(" ")
					// fmt.Println(errorMap[itemsDetials.SrNo].InsertStatus)
				}
				i++
				j++
			}
			c.Data["BomErrorMap"] = errorMap

			if _, err := productsFile.Seek(0, 0); err != nil { // Go to the start of the file
				panic(err)
			}
			return
		} else if importbutton == "SuppliersImport" {
			c.Data["ActiveTab"] = "tab_1_3"
			file, header, err := c.GetFile("suppliersFile")
			if file != nil && err == nil {
				//log.Println("Filename", header.Filename)
			} else {
				log.Println("File upload error:", err)
				flash.Error("There was a problem uploading your file.")
				flash.Store(&c.Controller)
				return
			}
			fileName := "tmp/" + header.Filename
			c.SaveToFile("suppliersFile", fileName)
			suppliersFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE, os.ModePerm)
			if err != nil {
				panic(err)
			}
			defer suppliersFile.Close()

			items := []*SuppliersCSV{}

			if err := gocsv.UnmarshalFile(suppliersFile, &items); err != nil {
				panic(err)
			}
			c.Data["CsvSuppliers"] = items
			// succesfully put all rows into struct. now range though every row and insert
			type ErrorStruct struct {
				SupplierUniqueCodeErr, SupplierNameErr, SupplierNameRequiredErr, Name string
				InsertStatus                                                          int
			}
			errorMap := make(map[string]*ErrorStruct)
			i := 0
			j := 1
			AddedNumber := 0
			for _, supplierDetails := range items {
				duplicateflag := 0
				supplierStruct := models.AddSupplier{}
				stringOfj := strconv.Itoa(j)
				//errorMap[supplierDetails.SrNo] = &ErrorStruct{}
				errorMap[stringOfj] = &ErrorStruct{}

				// fmt.Print(itemsDetials.SrNo)
				// fmt.Print(" ")

				// supplierStruct.Code = ""
				// if supplierDetails.Code == "" {
				// 	stringOfI := strconv.Itoa(i)
				// 	supplierStruct.Code = "I" + stringOfI
				// } else {
				//sup_code := strconv.Itoa(supplierDetails.Code)
				//supplierStruct.Code = supplierDetails.Code
				// }
				newGenCode, newCode := c.GetGeneratedCode(_orgID, 3, localetimezone)
				if supplierDetails.Code != "" {
					supplierStruct.Code = supplierDetails.Code
				} else {
					supplierStruct.Code = newGenCode
				}
				supplierStruct.SupplierUniqueSequence = newCode
				//if models.CheckForExistingSupplierUniqueCode(supplierStruct.Code, _orgID) == 1 {
				if models.CheckForExistingGenCode(supplierStruct.Code, 3, _orgID) == 1 {
					errorMap[stringOfj].SupplierUniqueCodeErr = "Code already used"
					errorMap[stringOfj].Name = supplierDetails.CompanyName
					// fmt.Print(" ")
					// fmt.Print(errorMap[itemsDetials.SrNo].ItemUniqueCodeErr)
					duplicateflag = 1
				}
				if models.CheckForExistingSupplierName(supplierDetails.CompanyName, _orgID) == 1 {
					errorMap[stringOfj].SupplierNameErr = "Name already used"
					errorMap[stringOfj].Name = supplierDetails.CompanyName
					// fmt.Print(" ")
					// fmt.Print(errorMap[itemsDetials.SrNo].ItemNameErr)
					duplicateflag = 1
				}
				if supplierDetails.CompanyName == "" {
					errorMap[stringOfj].SupplierNameRequiredErr = "Name required"
					errorMap[stringOfj].Name = supplierDetails.CompanyName
					// fmt.Print(" ")
					// fmt.Print(errorMap[itemsDetials.SrNo].ItemNameErr)
					duplicateflag = 1
				}

				if duplicateflag == 0 {
					supplierStruct.CompanyName = supplierDetails.CompanyName
					supplierStruct.ContactPerson = supplierDetails.ContactPerson
					supplierStruct.Designation = supplierDetails.Designation
					supplierStruct.Email = supplierDetails.Email
					supplierStruct.PhoneNo = supplierDetails.PhoneNo
					supplierStruct.CellNo = supplierDetails.CellNo
					supplierStruct.OfficeAddress = supplierDetails.OfficeAddress
					supplierStruct.City = supplierDetails.City
					supplierStruct.State = supplierDetails.State
					supplierStruct.Country = supplierDetails.Country
					supplierStruct.Pincode = supplierDetails.Pincode
					supplierStruct.CST = supplierDetails.CST
					if supplierDetails.CreditInDays != "" {
						supplierStruct.CreditInDays = supplierDetails.CreditInDays
					} else {
						supplierStruct.CreditInDays = "0"
					}
					if supplierDetails.CreditLimit != "" {
						supplierStruct.CreditLimit = supplierDetails.CreditLimit
					}
					supplierStruct.PAN = supplierDetails.PAN
					supplierStruct.ECC = supplierDetails.ECC
					supplierStruct.TIN = supplierDetails.TIN
					supplierStruct.Notes = supplierDetails.Notes
					//supplierStruct.GST = supplierDetails.GST
					supplierStruct.PGST = supplierDetails.PGST
					supplierStruct.ARN = supplierDetails.ARN
					if supplierDetails.Introduction != "" && supplierDetails.Introduction != "nil" {
						var s string
						var datetags []string
						datetags = strings.Split(supplierDetails.Introduction, "/")
						s = datetags[2] + "-" + datetags[1] + "-" + datetags[0]
						supplierStruct.Introduction = s
					} else {
						supplierStruct.Introduction = ""
					}
					supplierStruct.WarehouseAddress = supplierDetails.WarehouseAddress
					supplierStruct.MainGSTIN = supplierDetails.GSTIN
					// supplierStruct.WCity = supplierDetails.City
					// supplierStruct.WState = supplierDetails.State
					// supplierStruct.WCountry = supplierDetails.Country
					// supplierStruct.WPincode = supplierDetails.Pincode
					supplierStruct.Orgid = _orgID

					//errorMap[supplierDetails.Code].InsertStatus = models.AddImportSuppliers(supplierStruct)
					dbStatus := models.AddImportSuppliers(supplierStruct)
					if dbStatus == 0 {
						errorMap[stringOfj].InsertStatus = 0
					} else { //is dbStatus == 1
						delete(errorMap, stringOfj)
						AddedNumber++
					}

					// fmt.Print(" ")
					// fmt.Println(errorMap[itemsDetials.SrNo].InsertStatus)
				}
				i++
				j++
			}
			if len(errorMap) == 0 {
				c.Data["SuppliersErrorMap"] = "nil"
			} else {
				c.Data["SuppliersErrorMap"] = errorMap
			}
			c.Data["SuccessEntries"] = AddedNumber
			c.Data["TotalEntries"] = i

			if _, err := suppliersFile.Seek(0, 0); err != nil { // Go to the start of the file
				panic(err)
			}
			return
		} else if importbutton == "CustomersImport" {
			c.Data["ActiveTab"] = "tab_1_4"
			file, header, err := c.GetFile("customersFile")
			if file != nil && err == nil {
				//log.Println("Filename", header.Filename)
			} else {
				log.Println("File upload error:", err)
				flash.Error("There was a problem uploading your file.")
				flash.Store(&c.Controller)
				return
			}
			fileName := "tmp/" + header.Filename
			c.SaveToFile("customersFile", fileName)
			customersFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE, os.ModePerm)
			if err != nil {
				panic(err)
			}
			defer customersFile.Close()

			items := []*CustomersCSV{}

			if err := gocsv.UnmarshalFile(customersFile, &items); err != nil {
				panic(err)
			}
			c.Data["CsvCustomers"] = items
			// succesfully put all rows into struct. now range though every row and insert
			type ErrorStruct struct {
				CustomerNameErr, CustomerUniqueCodeErr, CustomerRequiredNameErr, Name string
				InsertStatus                                                          int
			}
			errorMap := make(map[string]*ErrorStruct)
			i := 0
			j := 0
			AddedNumber := 0
			for _, customerDetails := range items {
				duplicateflag := 0
				customerStruct := models.Customer{}
				stringOfj := strconv.Itoa(j)
				//errorMap[customerDetails.SrNo] = &ErrorStruct{}
				errorMap[stringOfj] = &ErrorStruct{}

				newGenCode, newCode := c.GetGeneratedCode(_orgID, 4, localetimezone)
				if customerDetails.UniqueCode != "" {
					customerStruct.UniqueCode = customerDetails.UniqueCode
				} else {
					customerStruct.UniqueCode = newGenCode
				}
				customerStruct.CustomerUniqueSequence = newCode

				//if models.CheckForExistingCustomerCode(customerDetails.UniqueCode, _orgID) == 1 {
				if models.CheckForExistingGenCode(customerDetails.UniqueCode, 4, _orgID) == 1 {
					errorMap[stringOfj].CustomerUniqueCodeErr = "Code already used"
					errorMap[stringOfj].Name = customerDetails.CompanyName
					// fmt.Print(" ")
					// fmt.Print(errorMap[itemsDetials.SrNo].ItemUniqueCodeErr)
					duplicateflag = 1
				}
				if models.CheckForExistingCustomerName(customerDetails.CompanyName, _orgID) == 1 {
					errorMap[stringOfj].CustomerNameErr = "Name already used"
					errorMap[stringOfj].Name = customerDetails.CompanyName
					// fmt.Print(" ")
					// fmt.Print(errorMap[itemsDetials.SrNo].ItemNameErr)
					duplicateflag = 1
				}
				if customerDetails.CompanyName == "" {
					errorMap[stringOfj].CustomerRequiredNameErr = "Name required"
					errorMap[stringOfj].Name = customerDetails.CompanyName
					// fmt.Print(" ")
					// fmt.Print(errorMap[itemsDetials.SrNo].ItemNameErr)
					duplicateflag = 1
				}

				//customerStruct.UniqueCode = customerDetails.UniqueCode
				customerStruct.CompanyName = customerDetails.CompanyName
				customerStruct.Address = customerDetails.Address
				customerStruct.City = customerDetails.City
				customerStruct.State = customerDetails.State
				customerStruct.Country = customerDetails.Country
				customerStruct.Pincode = customerDetails.Pincode

				if customerDetails.TIN != "" {
					customerStruct.TIN = customerDetails.TIN
				}
				if customerDetails.AreaCode != "" {
					customerStruct.AreaCode = customerDetails.AreaCode
				}
				if customerDetails.CST != "" {
					customerStruct.CST = customerDetails.CST
				}
				if customerDetails.PAN != "" {
					customerStruct.PAN = customerDetails.PAN
				}
				if customerDetails.ECC != "" {
					customerStruct.ECC = customerDetails.ECC
				}
				if customerDetails.CreditInDays != "" {
					customerStruct.CreditInDays = customerDetails.CreditInDays
				} else {
					customerStruct.CreditInDays = "0"
				}
				if customerDetails.CreditLimit != "" {
					customerStruct.CreditLimit = customerDetails.CreditLimit
				}

				customerStruct.CustomerType = customerDetails.CustomerType
				if customerDetails.Introduction != "" && customerDetails.Introduction != "nil" {
					var s string
					var datetags []string
					datetags = strings.Split(customerDetails.Introduction, "/")
					//customerStruct.Introduction = customerDetails.Introduction
					s = datetags[2] + "-" + datetags[1] + "-" + datetags[0]
					customerStruct.Introduction = s
				} else {
					customerStruct.Introduction = ""
				}

				customerStruct.ServiceLoc = customerDetails.ServiceLoc

				customerStruct.ContactPerson = customerDetails.ContactPerson

				if customerDetails.Designation != "" {
					customerStruct.Designation = customerDetails.Designation
				}
				if customerDetails.Email != "" {
					customerStruct.Email = customerDetails.Email
				}
				if customerDetails.PhoneNo != "" {
					customerStruct.PhoneNo = customerDetails.PhoneNo
				}
				if customerDetails.CellNo != "" {
					customerStruct.CellNo = customerDetails.CellNo
				}
				if customerDetails.GST != "" {
					customerStruct.GST = customerDetails.GST
				}
				if customerDetails.PGST != "" {
					customerStruct.PGST = customerDetails.PGST
				}
				if customerDetails.ARN != "" {
					customerStruct.ARN = customerDetails.ARN
				}
				if customerDetails.WarehouseAddress != "" {
					customerStruct.WarehouseAddress = customerDetails.WarehouseAddress
				}

				customerStruct.WCity = customerDetails.WCity
				customerStruct.WState = customerDetails.WState
				customerStruct.WCountry = customerDetails.WCountry
				customerStruct.WPincode = customerDetails.WPincode
				customerStruct.Orgid = _orgID
				if duplicateflag == 0 {
					dbStatus := models.AddImportCustomer(customerStruct)
					fmt.Println("dbStatus= ", dbStatus)
					if dbStatus == 0 {
						errorMap[stringOfj].InsertStatus = 0
					} else { //is dbStatus == 1
						delete(errorMap, stringOfj)
						AddedNumber++
					}
					//errorMap[customerDetails.SrNo].InsertStatus = models.AddImportCustomer(customerStruct)
					// fmt.Print(" ")
					// fmt.Println(errorMap[itemsDetials.SrNo].InsertStatus)
				}
				i++
				j++
			}
			//fmt.Println(errorMap)
			if len(errorMap) == 0 {
				c.Data["CustomersErrorMap"] = "nil"
			} else {
				c.Data["CustomersErrorMap"] = errorMap
			}
			c.Data["SuccessEntries"] = AddedNumber
			c.Data["TotalEntries"] = i

			if _, err := customersFile.Seek(0, 0); err != nil { // Go to the start of the file
				panic(err)
			}
			return
		}
	}
}
