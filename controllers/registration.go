package controllers

import (
	"bytes"
	"crypto/rand"
	"encoding/base64"
	"encoding/binary"
	"html/template"
	"strings"

	"bitbucket.org/caslinoerp/singulerp/models"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"github.com/astaxie/beego/validation"
	"golang.org/x/crypto/bcrypt"
	"gopkg.in/mailgun/mailgun-go.v1"
)

type RegistrationController struct {
	BaseController
}

type registrationValidate struct {
	OrgName          string `form:"fullname" valid:"Required"`
	Email            string `form:"email" valid:"Email"`
	AdminUsername    string `form:"admin_username" valid:"Required; MinSize(6)"`
	AdminMobile      string `form:"admin_mobile" valid:"Required"`
	Password         string `form:"password" valid:"Required; MinSize(4)"`
	CPassword        string `form:"rpassword" valid:"Required; MinSize(4)"`
	VerificationCode string
}

func (c *RegistrationController) Registration() {
	c.TplName = "registration.html"
	c.Data["PageTitle"] = "Registration"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

	//generic code to handle post request and validation
	if c.Ctx.Input.Method() == "POST" {
		flash := beego.NewFlash()
		items := registrationValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

		org_name := items.OrgName
		if strings.Contains(org_name, " ") {
			c.Data["spaceExist"] = true
			return
		}

		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			return
		}

		//check for existing organisation via name
		if models.CheckForExistingOrgName(items.OrgName) == 1 {
			c.Data["orgNameExist"] = 1
			flash.Warning("This orginisation already exists.")
			flash.Store(&c.Controller)
			return
		}

		//check for existing organisation via email
		if models.CheckForExistingOrgEmail(items.Email) == 1 {
			c.Data["orgExist"] = 1
			flash.Warning("This orginisation already exists.")
			flash.Store(&c.Controller)
			return
		}
		if models.CheckForExistingOrgEmail(items.Email) == 0 {

			// Example: this will give us a 44 byte, base64 encoded salt
			// salt, err := GenerateRandomString(32)
			// if err != nil {
			// 	// Serve an appropriately vague error to the
			// 	// user, but log the details internally.
			// }
			// saltedPassword := fmt.Sprintf("%s%s", salt, items.Password)
			// Hashing the password with the default cost of 10
			hashedPassword, err := bcrypt.GenerateFromPassword([]byte(items.Password), bcrypt.DefaultCost)
			if err != nil {
				panic(err)
			}
			items.Password = string(hashedPassword)

			verificationCode, err := GenerateRandomString(32)
			if err != nil {
				items.VerificationCode = "couldnotgenerate"
			}
			items.VerificationCode = verificationCode

			//formData := make(map[string]string)

			orgCode, err := GenerateRandomInt(64)

			formStruct := models.Organisation{
				Orgid:               orgCode,
				Org_name:            items.OrgName,
				Org_mobile:          items.AdminMobile,
				Org_legal_name:      items.OrgName,
				Org_email:           items.Email,
				Verification_code:   items.VerificationCode,
				Verification_status: 0,
				Status:              0,
				Password:            items.Password,
				Username:            items.AdminUsername,
			}

			//insertStatus := models.RegistrationInsert(formData)
			insertStatus := models.RegistrationInsertStructParam(formStruct)
			if insertStatus == 0 {
				flash.Error("An error occured while saving. Please try again.")
				flash.Store(&c.Controller)
				return
			}

			formStruct2 := models.Organisation{
				Orgid: orgCode,
				//Org_name:            items.OrgName,
				//Org_mobile:          items.AdminMobile,
				//Org_legal_name:      items.OrgName,
				Org_email: items.Email,
				//Verification_code:   items.VerificationCode,
				//Verification_status: 0,
				Status:   0,
				Password: items.Password,
				Username: items.AdminUsername,
			}
			retrieved := make(map[string]orm.Params)
			retrieved = models.GetUserId(formStruct2)

			user_id := retrieved["user_0"]["_id"].(string)

			insertRolesStatus := models.AddAllRolesByUserId(user_id, orgCode)
			if insertRolesStatus == 0 {
				flash.Error("An error occured while saving. Please try again.")
				flash.Store(&c.Controller)
				return
			}
		}
		verifyURL := beego.AppConfig.String("domain") + "/registration_verification/"
		//fmt.Println(verifyURL)
		mg := mailgun.NewMailgun("singulerp.net", "key-eec89877161237bfb0264764d75677eb", "")
		email_message := mailgun.NewMessage(
			"Singulerp <app@singulerp.net>",
			"Singulerp Verification Email",
			"",
			items.Email)
		emailMessage := "<html><body> <p><b>Hi " + items.AdminUsername + ",</b></p> <p>This is a verification email from Singulerp.net</p><p>Click on the link below to complete your verification process.</p><p>" + verifyURL + items.VerificationCode + "</p></body></html>"
		email_message.SetHtml(emailMessage)

		_, _, err := mg.Send(email_message)
		if err != nil {
			//fmt.Println("ERROR : ", err)
			return
		}
		subjectLine := "New User Signup (" + beego.AppConfig.String("domain") + ")"
		//send notification email to admin (Caslino)
		notificationMessage := mailgun.NewMessage(
			"Singulerp <app@singulerp.net>",
			subjectLine,
			"",
			"caslino@singulerp.com")
		emailMessage = "<html><body> <p>Organisation:" + items.OrgName + "<br> Username:" + items.AdminUsername + "<br> Email:" + items.Email + "<br> Mobile:" + items.AdminMobile + "</p></body></html>"
		notificationMessage.SetHtml(emailMessage)

		_, _, err = mg.Send(notificationMessage)

		c.Redirect("/registration_complete", 303)
	}
}
func (c *RegistrationController) RegistrationComplete() {
	c.TplName = "registration_complete.html"
	c.Data["PageTitle"] = "Registration Complete"

}

func (c *RegistrationController) RegistrationVerification() {
	token := c.Ctx.Input.Param(":token")
	//fmt.Println(token)
	c.TplName = "registration_complete.html"
	c.Data["PageTitle"] = "Registration Verification"
	verificationStatus := models.RegistrationValidateToken(token)
	if verificationStatus == 0 {
		c.Data["verification"] = 0
	} else {
		c.Data["verification"] = 1
	}

}

// GenerateRandomBytes returns securely generated random bytes.
// It will return an error if the system's secure random
// number generator fails to function correctly, in which
// case the caller should not continue.
func GenerateRandomBytes(n int) ([]byte, error) {
	b := make([]byte, n)
	_, err := rand.Read(b)
	// Note that err == nil only if we read len(b) bytes.
	if err != nil {
		return nil, err
	}
	return b, nil
}

// GenerateRandomString returns a URL-safe, base64 encoded
// securely generated random string.
// It will return an error if the system's secure random
// number generator fails to function correctly, in which
// case the caller should not continue.
func GenerateRandomString(s int) (string, error) {
	b, err := GenerateRandomBytes(s)
	return base64.URLEncoding.EncodeToString(b), err
}

func GenerateRandomInt(s int) (int64, error) {
	b, err := GenerateRandomBytes(s)
	var y int64
	err = binary.Read(bytes.NewReader(b), binary.BigEndian, &y)
	if y < 0 {
		y *= -1
	}
	return y, err
}
