package controllers

import (
	"html/template"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/validation"
)

type ConfigurationController struct {
	BaseController
}

type config1Validate struct {
	EntityName          string `form:"name" valid:"Required"`
	BrandName           string `form:"bname" valid:"Required"`
	Email               string `form:"email" valid:"Email"`
	Mobile              string `form:"mobile" valid:"AlphaNumeric"`
	HQLoc               string `form:"hq_location" valid:"Required"`
	IndType				string `form:"industry_type" valid:"Required"`
	LocationFac			string `form:"location_fac" valid:"Required"`	
}

type config2Validate struct {
	FullName          string `form:"name" valid:"Required"`
	UName             string `form:"uname" valid:"Required"`
	Email             string `form:"email" valid:"Email"`
	Mobile            string `form:"mobile" valid:"AlphaNumeric"`
	Password          string `form:"password" valid:"Required"`
}

func (c *ConfigurationController) Config1() {
	c.TplName = "config/config1.html"
	c.Data["PageTitle"] = "Configuration"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

	//generic code to handle post request and validation
	if c.Ctx.Input.Method() == "POST" {
		flash := beego.NewFlash()
		items := config1Validate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			return
		}
		c.Redirect("/config/2", 302)
	}
}
func (c *ConfigurationController) Config2() {
	c.TplName = "config/config2.html"
	c.Data["PageTitle"] = "Config2"
	 c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())

	//generic code to handle post request and validation
	if c.Ctx.Input.Method() == "POST" {
		flash := beego.NewFlash()
		items := config2Validate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			return
		}
	}
}