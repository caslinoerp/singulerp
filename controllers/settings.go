package controllers

import (
	"fmt"
	"html/template"

	"bitbucket.org/caslinoerp/singulerp/models"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"github.com/astaxie/beego/validation"
)

type SettingsController struct {
	BaseController
}

type PoTc struct {
	PoContent string `form:"po_content"`
}

type SoTc struct {
	SoContent string `form:"so_content"`
}

type Users struct {
	FullName string `form:"name" valid:"Required"`
	UName    string `form:"uname" valid:"Required"`
	Email    string `form:"email" valid:"Email"`
	Mobile   string `form:"mobile" valid:"AlphaNumeric"`
	Password string `form:"password" valid:"Required"`

	SelectAll     string `form:"select-all"`
	SelectAdd     string `form:"select-add"`
	SelectView    string `form:"select-view"`
	Usersadd      string `form:"uadd"`
	Usersview     string `form:"uview"`
	ConfigModify  string `form:"cmodify"`
	ConfigView    string `form:"cview"`
	Rolesadd      string `form:"roles_add"`
	Rolesview     string `form:"roles_view"`
	Itemsadd      string `form:"iadd"`
	Itemsview     string `form:"iview"`
	Inventoryadd  string `form:"inadd"`
	Inventoryview string `form:"inview"`
	GRNadd        string `form:"grnadd"`
	GRNview       string `form:"grnview"`
	//Usageview 		string `form:"usageview"`
	CompanyAdd       string `form:"company_add"`
	CompanyView      string `form:"company_view"`
	AssManage        string `form:"ass_manage"`
	AssView          string `form:"ass_view"`
	PoAdd            string `form:"po_add"`
	PoView           string `form:"po_view"`
	CustomersAdd     string `form:"customers_add"`
	CustomersView    string `form:"customers_view"`
	ProductAss       string `form:"ass_products"`
	AssProductView   string `form:"ass_products_view"`
	SalesAdd         string `form:"sales_add"`
	SalesView        string `form:"sales_view"`
	ProductionCreate string `form:"production_create"`
	ProductionView   string `form:"production_view"`
	ProductsAdd      string `form:"products_add"`
	ProductsView     string `form:"products_view"`
	//BomModify 		string `form:"bom_modify"`
	//Bomview 		string `form:"bom_view"`
	//Jobcardadd 		string `form:"jobcard_add"`
	//Jobcardview 	string `form:"jobcard_view"`
	Processadd  string `form:"process_add"`
	Processview string `form:"process_view"`
	// Ordersadd 		string `form:"orders_add"`
	// Ordersview 		string `form:"orders_view"`
	StatisticsCreate string `form:"statistics_create"`
	StatisticsView   string `form:"statistics_view"`
	// PoStats 		string `form:"po_stats"`
	// GrnStats 		string `form:"grn_stats"`
	// ContractStats 	string `form:"servicecontract_stats"`
	// QaStats 		string `form:"qa_stats"`
	// OrdersStats 	string `form:"orders_stats"`
	// ComplaintsStats string `form:"complaints_stats"`
	// HistoryStats 	string `form:"history_stats"`
	MasterAdd    string `form:"master_add"`
	MasterView   string `form:"master_view"`
	CreateReport string `form:"report_create"`
	ViewReport   string `form:"report_view"`
	// SupportAdd 		string `form:"support_add"`
	// SupportView 	string `form:"support_view"`
	EmployeeAdd  string `form:"employee_add"`
	EmployeeView string `form:"employee_view"`
	// LeavesAdd 		string `form:"leaves_add"`
	// LeavesView 		string `form:"leaves_view"`
	// ContractAdd 	string `form:"contract_add"`
	// ContractView 	string `form:"contract_view"`
}

type managerolesvalidate struct {
	OrgUserName string `form:"user_name" valid:"Required"`

	SelectAll     string `form:"select-all"`
	SelectAdd     string `form:"select-add"`
	SelectView    string `form:"select-view"`
	Usersadd      string `form:"uadd"`
	Usersview     string `form:"uview"`
	Rolesadd      string `form:"roles_add"`
	Rolesview     string `form:"roles_view"`
	Itemsadd      string `form:"iadd"`
	Itemsview     string `form:"iview"`
	Inventoryadd  string `form:"inadd"`
	Inventoryview string `form:"inview"`
	GRNadd        string `form:"grnadd"`
	GRNview       string `form:"grnview"`
	//Usageview 		string `form:"usageview"`
	ConfigModify     string `form:"cmodify"`
	ConfigView       string `form:"cview"`
	CompanyAdd       string `form:"company_add"`
	CompanyView      string `form:"company_view"`
	AssManage        string `form:"ass_manage"`
	AssView          string `form:"ass_view"`
	PoAdd            string `form:"po_add"`
	PoView           string `form:"po_view"`
	CustomersAdd     string `form:"customers_add"`
	CustomersView    string `form:"customers_view"`
	ProductAss       string `form:"ass_products"`
	AssProductView   string `form:"ass_products_view"`
	SalesAdd         string `form:"sales_add"`
	SalesView        string `form:"sales_view"`
	ProductionCreate string `form:"production_create"`
	ProductionView   string `form:"production_view"`
	ProductsAdd      string `form:"products_add"`
	ProductsView     string `form:"products_view"`
	//BomModify 		string `form:"bom_modify"`
	//Bomview 		string `form:"bom_view"`
	//Jobcardadd 		string `form:"jobcard_add"`
	//Jobcardview 	string `form:"jobcard_view"`
	Processadd  string `form:"process_add"`
	Processview string `form:"process_view"`
	// Ordersadd 		string `form:"orders_add"`
	// Ordersview 		string `form:"orders_view"`
	StatisticsCreate string `form:"statistics_create"`
	StatisticsView   string `form:"statistics_view"`
	// PoStats 		string `form:"po_stats"`
	// GrnStats 		string `form:"grn_stats"`
	// ContractStats 	string `form:"servicecontract_stats"`
	// QaStats 		string `form:"qa_stats"`
	// OrdersStats 	string `form:"orders_stats"`
	// ComplaintsStats string `form:"complaints_stats"`
	// HistoryStats 	string `form:"history_stats"`
	MasterAdd    string `form:"master_add"`
	MasterView   string `form:"master_view"`
	CreateReport string `form:"report_create"`
	ViewReport   string `form:"report_view"`
	// SupportAdd 		string `form:"support_add"`
	// SupportView 	string `form:"support_view"`
	EmployeeAdd  string `form:"employee_add"`
	EmployeeView string `form:"employee_view"`
	// LeavesAdd 		string `form:"leaves_add"`
	// LeavesView 		string `form:"leaves_view"`
	// ContractAdd 	string `form:"contract_add"`
	// ContractView 	string `form:"contract_view"`
	UserBtn   string `form:"roles-button"`
	UpdateBtn string `form:"update-button"`
}

type configValidate struct {
	OrgPKID     string   `form:"org_pkid"`
	EntityName  string   `form:"name" valid:"Required"`
	BrandName   string   `form:"bname" valid:"Required"`
	Email       string   `form:"email"`
	Mobile      string   `form:"mobile"`
	HQLoc       string   `form:"hq_location" `
	IndType     []string `form:"industry_type[]" `
	LocationFac string   `form:"location_fac" `
	GSTIN       string   `form:"gstin" `
	PAN         string   `form:"pan" `
	BankName    string   `form:"bank_name" `
	AccountNo   string   `form:"account_no" `
	IFSC        string   `form:"ifsc" `
}

type prefixValidate struct {
	ItemCode      string `form:"item_code" valid:"Required"`
	ProductCode   string `form:"product_code" valid:"Required"`
	SupplierCode  string `form:"supplier_code" valid:"Required"`
	CustomerCode  string `form:"customer_code" valid:"Required"`
	PurchaseOrder string `form:"purchase_order" valid:"Required"`
	SalesCode     string `form:"sales_code" valid:"Required"`
	EmployeeId    string `form:"employee_id" valid:"Required"`
}

// TODO: Add Alpha and Numeric validations for all 1l and 1r
type PrefixFormItem struct {
	ItemCode3l string `form:"item_code_3l"`
	ItemCode2l string `form:"item_code_2l"`
	ItemCode1l string `form:"item_code_1l"`
	ItemCode0  string `form:"item_code_0"`
	ItemCode1r string `form:"item_code_1r"`
	ItemCode2r string `form:"item_code_2r"`
	ItemCode3r string `form:"item_code_3r"`
}

type PrefixFormProduct struct {
	ProductCode3l string `form:"product_code_3l"`
	ProductCode2l string `form:"product_code_2l"`
	ProductCode1l string `form:"product_code_1l"`
	ProductCode0  string `form:"product_code_0"`
	ProductCode1r string `form:"product_code_1r"`
	ProductCode2r string `form:"product_code_2r"`
	ProductCode3r string `form:"product_code_3r"`
}

type PrefixFormSupplier struct {
	SupplierCode3l string `form:"supplier_code_3l"`
	SupplierCode2l string `form:"supplier_code_2l"`
	SupplierCode1l string `form:"supplier_code_1l"`
	SupplierCode0  string `form:"supplier_code_0"`
	SupplierCode1r string `form:"supplier_code_1r"`
	SupplierCode2r string `form:"supplier_code_2r"`
	SupplierCode3r string `form:"supplier_code_3r"`
}

type PrefixFormCustomer struct {
	CustomerCode3l string `form:"customer_code_3l"`
	CustomerCode2l string `form:"customer_code_2l"`
	CustomerCode1l string `form:"customer_code_1l"`
	CustomerCode0  string `form:"customer_code_0"`
	CustomerCode1r string `form:"customer_code_1r"`
	CustomerCode2r string `form:"customer_code_2r"`
	CustomerCode3r string `form:"customer_code_3r"`
}

type PrefixFormPO struct {
	POCode3l string `form:"po_code_3l"`
	POCode2l string `form:"po_code_2l"`
	POCode1l string `form:"po_code_1l"`
	POCode0  string `form:"po_code_0"`
	POCode1r string `form:"po_code_1r"`
	POCode2r string `form:"po_code_2r"`
	POCode3r string `form:"po_code_3r"`
}

type PrefixFormSO struct {
	SOCode3l string `form:"so_code_3l"`
	SOCode2l string `form:"so_code_2l"`
	SOCode1l string `form:"o_code_1l"`
	SOCode0  string `form:"so_code_0"`
	SOCode1r string `form:"so_code_1r"`
	SOCode2r string `form:"so_code_2r"`
	SOCode3r string `form:"so_code_3r"`
}

type PrefixFormMachineCode struct {
	MachineCode3l string `form:"machine_code_3l"`
	MachineCode2l string `form:"machine_code_2l"`
	MachineCode1l string `form:"machine_code_1l"`
	MachineCode0  string `form:"machine_code_0"`
	MachineCode1r string `form:"machine_code_1r"`
	MachineCode2r string `form:"machine_code_2r"`
	MachineCode3r string `form:"machine_code_3r"`
}

type PrefixFormProdOrder struct {
	ProdOrderCode3l string `form:"prod_order_code_3l"`
	ProdOrderCode2l string `form:"prod_order_code_2l"`
	ProdOrderCode1l string `form:"prod_order_code_1l"`
	ProdOrderCode0  string `form:"prod_order_code_0"`
	ProdOrderCode1r string `form:"prod_order_code_1r"`
	ProdOrderCode2r string `form:"prod_order_code_2r"`
	ProdOrderCode3r string `form:"prod_order_code_3r"`
}

type PrefixFormSupportTicket struct {
	SupportTicketCode3l string `form:"support_ticket_code_3l"`
	SupportTicketCode2l string `form:"support_ticket_code_2l"`
	SupportTicketCode1l string `form:"support_ticket_code_1l"`
	SupportTicketCode0  string `form:"support_ticket_code_0"`
	SupportTicketCode1r string `form:"support_ticket_code_1r"`
	SupportTicketCode2r string `form:"support_ticket_code_2r"`
	SupportTicketCode3r string `form:"support_ticket_code_3r"`
}

type PrefixFormEmployeeCode struct {
	EmployeeCode3l string `form:"employee_code_3l"`
	EmployeeCode2l string `form:"employee_code_2l"`
	EmployeeCode1l string `form:"employee_code_1l"`
	EmployeeCode0  string `form:"employee_code_0"`
	EmployeeCode1r string `form:"employee_code_1r"`
	EmployeeCode2r string `form:"employee_code_2r"`
	EmployeeCode3r string `form:"employee_code_3r"`
}

type PrefixFormGRNCode struct {
	GRNCode3l string `form:"grn_code_3l"`
	GRNCode2l string `form:"grn_code_2l"`
	GRNCode1l string `form:"grn_code_1l"`
	GRNCode0  string `form:"grn_code_0"`
	GRNCode1r string `form:"grn_code_1r"`
	GRNCode2r string `form:"grn_code_2r"`
	GRNCode3r string `form:"grn_code_3r"`
}

type PrefixFormDispatchCode struct {
	DispatchCode3l string `form:"dispatch_code_3l"`
	DispatchCode2l string `form:"dispatch_code_2l"`
	DispatchCode1l string `form:"dispatch_code_1l"`
	DispatchCode0  string `form:"dispatch_code_0"`
	DispatchCode1r string `form:"dispatch_code_1r"`
	DispatchCode2r string `form:"dispatch_code_2r"`
	DispatchCode3r string `form:"dispatch_code_3r"`
}

func (c *SettingsController) ManageUsers() {
	c.Data["xsrf_token"] = c.XSRFToken()
	var user_id string
	var _orgId string
	var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
		// roles := make(map[string]string)
		// roles = fmt.Sprintf("%s", sessionVar.Get("user_roles"))
	} else {
		user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}

	retrieved := make(map[string]string)
	retrieved = models.GetUserRolesByMap(username, _orgId)

	if _, ok := retrieved["2"]; ok {
		//fmt.Println("*********U got view permission")
		c.Data["username"] = username
		UserStruct := models.User_Account{
			Orgid:    _orgId,
			Userid:   user_id,
			Username: username,
		}
		retrievedusers := make(map[string]orm.Params)
		retrievedusers = models.GetAllUsers(UserStruct)
		c.Data["Retrieved"] = retrievedusers

		c.TplName = "settings/users.html"
		c.Data["Settings"] = 1
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		c.Data["PageTitle"] = "Manage Users"

		if _, ok := retrieved["1"]; ok {
			if c.Ctx.Input.Method() == "POST" {
				flash := beego.NewFlash()
				items := Users{}
				if err := c.ParseForm(&items); err != nil {
					flash.Error("Cannot parse form")
					flash.Store(&c.Controller)
					c.Data["ErrorsPresent"] = true
					return
				}
				c.Data["FormItems"] = items
				c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
				valid := validation.Validation{}
				//fmt.Println("***Check=", items.Itemsadd)
				if b, _ := valid.Valid(&items); !b {
					c.Data["Errors"] = valid.ErrorsMap
					c.Data["ErrorsPresent"] = true
					return
				}
				formStruct := models.User_Account{
					Orgid:         _orgId,
					Userid:        user_id,
					Username:      username,
					Email:         items.Email,
					Status:        1,
					Password:      items.Password,
					NewUsername:   items.UName,
					Usertype:      2,
					FullName:      items.FullName,
					Usersadd:      items.Usersadd,
					Usersview:     items.Usersview,
					Rolesadd:      items.Rolesadd,
					Rolesview:     items.Rolesview,
					Itemsadd:      items.Itemsadd,
					Itemsview:     items.Itemsview,
					Inventoryadd:  items.Inventoryadd,
					Inventoryview: items.Inventoryview,
					GRNadd:        items.GRNadd,
					GRNview:       items.GRNview,
					//Usageview: 		items.Usageview,
					ConfigModify:     items.ConfigModify,
					ConfigView:       items.ConfigView,
					CompanyAdd:       items.CompanyAdd,
					CompanyView:      items.CompanyView,
					AssManage:        items.AssManage,
					AssView:          items.AssView,
					PoAdd:            items.PoAdd,
					PoView:           items.PoView,
					CustomersAdd:     items.CustomersAdd,
					CustomersView:    items.CustomersView,
					ProductAss:       items.ProductAss,
					AssProductView:   items.AssProductView,
					SalesAdd:         items.SalesAdd,
					SalesView:        items.SalesView,
					ProductionCreate: items.ProductionCreate,
					ProductionView:   items.ProductionView,
					ProductsAdd:      items.ProductsAdd,
					ProductsView:     items.ProductsView,
					// BomModify: 		items.BomModify,
					// Bomview: 		items.Bomview,
					// Jobcardadd: 	items.Jobcardadd,
					// Jobcardview: 	items.Jobcardview,
					Processadd:  items.Processadd,
					Processview: items.Processview,
					// Ordersadd: 		items.Ordersadd,
					// Ordersview: 	items.Ordersview,
					StatisticsCreate: items.StatisticsCreate,
					StatisticsView:   items.StatisticsView,
					// PoStats: 		items.PoStats,
					// GrnStats: 		items.GrnStats,
					// ContractStats: 	items.ContractStats,
					// QaStats: 		items.QaStats,
					// OrdersStats:  	items.OrdersStats,
					// ComplaintsStats: items.ComplaintsStats,
					// HistoryStats: 	items.HistoryStats,
					MasterAdd:    items.MasterAdd,
					MasterView:   items.MasterView,
					CreateReport: items.CreateReport,
					ViewReport:   items.ViewReport,
					// SupportAdd: 	items.SupportAdd,
					// SupportView: 	items.SupportView,
					EmployeeAdd:  items.EmployeeAdd,
					EmployeeView: items.EmployeeView,
					// LeavesAdd: 		items.LeavesAdd,
					// LeavesView: 	items.LeavesView,
					// ContractAdd: 	items.ContractAdd,
					// ContractView: 	items.ContractView,
				}

				insertResult := models.AddUser(formStruct)
				if insertResult["status"] == "user-exists" {
					c.Data["UserExists"] = true
					c.Data["ErrorsPresent"] = true
					return
				} else if insertResult["status"] == "inserted" {
					c.Data["inserted"] = true
					c.Data["ErrorsPresent"] = true
					return
				} else if insertResult["status"] == "failed-insert" {
					c.Data["failed-insert"] = true
					//	return
				} else if insertResult["status"] == "user-deactivated" {
					c.Data["UserDeactivated"] = true
					return
				}

				c.Redirect("/settings/users", 303)
			}
		} else {
			c.Data["NoUserAdd"] = true
		}

	} else {
		c.Redirect("/accessdenied", 303)
	}
}

func (c *SettingsController) ManageUserRoles() {
	var user_id string
	var _orgId string
	var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}

	retrieved := make(map[string]string)
	retrieved = models.GetUserRolesByMap(username, _orgId)

	if _, ok := retrieved["38"]; ok {

		c.Data["username"] = username
		UserStruct := models.User_Account{
			Orgid:    _orgId,
			Userid:   user_id,
			Username: username,
		}
		retrievedusers := make(map[string]orm.Params)
		retrievedusers = models.GetAllUsers(UserStruct)
		c.Data["Retrieved"] = retrievedusers

		c.TplName = "settings/manage-roles.html"
		c.Data["Settings"] = 1
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		c.Data["PageTitle"] = "Manage User Roles"

		if c.Ctx.Input.Method() == "POST" {
			flash := beego.NewFlash()
			items := managerolesvalidate{}
			if err := c.ParseForm(&items); err != nil {
				flash.Error("Cannot parse form")
				flash.Store(&c.Controller)
				return
			}

			valid := validation.Validation{}
			if b, _ := valid.Valid(&items); !b {
				c.Data["FormItems"] = items
				c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
				c.Data["Errors"] = valid.ErrorsMap
				return
			}

			formStruct := models.User_Account{
				Orgid:       _orgId,
				Userid:      user_id,
				Username:    username,
				OrgUserName: items.OrgUserName,
			}

			retrievedroles := make(map[string]string)
			retrievedroles = models.GetUserRoles(formStruct)
			c.Data["Retrieveduser"] = items.OrgUserName
			c.Data["RetrievedRoles"] = retrievedroles
			typestatus := models.CheckUserType(items.OrgUserName, _orgId)
			if typestatus == 0 {
				c.Data["NotAdmin"] = true
			}

			if _, ok := retrieved["37"]; ok {
				c.Data["RolesAddAccess"] = true

				if items.UpdateBtn == "updateroles" {
					//fmt.Println("****Inside updateroles")
					formStruct := models.User_Account{
						OrgUserName: items.OrgUserName,
						Orgid:       _orgId,
						Userid:      user_id,
						Username:    username,
						//Email:    items.Email,
						Status: 1,
						//Password: items.Password,
						//NewUsername: items.UName,
						//Usertype: 2,
						//FullName: items.FullName,
						Usersadd:      items.Usersadd,
						Usersview:     items.Usersview,
						Rolesadd:      items.Rolesadd,
						Rolesview:     items.Rolesview,
						Itemsadd:      items.Itemsadd,
						Itemsview:     items.Itemsview,
						Inventoryadd:  items.Inventoryadd,
						Inventoryview: items.Inventoryview,
						GRNadd:        items.GRNadd,
						GRNview:       items.GRNview,
						//Usageview: 		items.Usageview,
						ConfigModify:     items.ConfigModify,
						ConfigView:       items.ConfigView,
						CompanyAdd:       items.CompanyAdd,
						CompanyView:      items.CompanyView,
						AssManage:        items.AssManage,
						AssView:          items.AssView,
						PoAdd:            items.PoAdd,
						PoView:           items.PoView,
						CustomersAdd:     items.CustomersAdd,
						CustomersView:    items.CustomersView,
						ProductAss:       items.ProductAss,
						AssProductView:   items.AssProductView,
						SalesAdd:         items.SalesAdd,
						SalesView:        items.SalesView,
						ProductionCreate: items.ProductionCreate,
						ProductionView:   items.ProductionView,
						ProductsAdd:      items.ProductsAdd,
						ProductsView:     items.ProductsView,
						// BomModify: 		items.BomModify,
						// Bomview: 		items.Bomview,
						// Jobcardadd: 	items.Jobcardadd,
						// Jobcardview: 	items.Jobcardview,
						Processadd:  items.Processadd,
						Processview: items.Processview,
						// Ordersadd: 		items.Ordersadd,
						// Ordersview: 	items.Ordersview,
						StatisticsCreate: items.StatisticsCreate,
						StatisticsView:   items.StatisticsView,
						// PoStats: 		items.PoStats,
						// GrnStats: 		items.GrnStats,
						// ContractStats: 	items.ContractStats,
						// QaStats: 		items.QaStats,
						// OrdersStats:  	items.OrdersStats,
						// ComplaintsStats: items.ComplaintsStats,
						// HistoryStats: 	items.HistoryStats,
						MasterAdd:    items.MasterAdd,
						MasterView:   items.MasterView,
						CreateReport: items.CreateReport,
						ViewReport:   items.ViewReport,
						// SupportAdd: 	items.SupportAdd,
						// SupportView: 	items.SupportView,
						EmployeeAdd:  items.EmployeeAdd,
						EmployeeView: items.EmployeeView,
						// LeavesAdd: 		items.LeavesAdd,
						// LeavesView: 	items.LeavesView,
						// ContractAdd: 	items.ContractAdd,
						// ContractView: 	items.ContractView,
					}

					insertResult := models.AddUser(formStruct)
					if insertResult["status"] == "user-exists" {
						c.Data["UserExists"] = true
						return
					} else if insertResult["status"] == "inserted" {
						c.Data["Retrieveduser"] = items.OrgUserName
						c.Data["FormItems"] = items
						c.Data["inserted"] = true
						c.Data["ErrorsPresent"] = true
						return
					} else if insertResult["status"] == "failed-insert" {
						c.Data["failed-insert"] = true
						//	return
					} else if insertResult["status"] == "user-deactivated" {
						c.Data["UserDeactivated"] = true
						return
					}

					c.Redirect("/settings/manage-user-roles", 303)
				}
			} else {
				c.Data["NoRolesAdd"] = true
			}
		}

	} else {
		c.Redirect("/accessdenied", 303)
	}

}

func (c *SettingsController) ManageUsersAjax() {
	var user_id string
	var _orgId string
	var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}
	c.Data["username"] = username
	UserSessionStruct := models.User_Account{
		Orgid:    _orgId,
		Userid:   user_id,
		Username: username,
	}

	if c.Ctx.Input.Method() == "GET" {

		affectedUser := c.GetString("userId")
		affectedAction := c.GetString("action")
		//fmt.Println(affectedAction, affectedUser)

		result := models.ManageUserBasedOnAction(affectedUser, affectedAction, UserSessionStruct)
		//fmt.Println(result)
		c.Ctx.Output.Body([]byte(result))
		//c.Data["completeRegister"] = true
	}
}

func (c *SettingsController) OrganizationSettings() {
	flash := beego.ReadFromRequest(&c.Controller)
	if ok := flash.Data["error"]; ok != "" {
		c.Data["FlashErrors"] = ok
	}
	if ok := flash.Data["notice"]; ok != "" {
		// Display error messages
		c.Data["FlashNotices"] = ok
	}

	var user_id string
	var _orgId string
	var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}

	c.Data["username"] = username

	retrieved := make(map[string]string)
	retrieved = models.GetUserRolesByMap(username, _orgId)

	if _, ok := retrieved["4"]; ok {

		ConfigurationStruct := models.Organisation{
			Orgidd:   _orgId,
			Userid:   user_id,
			Username: username,
		}
		/*retrieved := make(map[string]orm.Params)
		retrieved = models.GetConfiguration(formStruct5)
		//c.Data["Retrieved"] = retrieved
		c.Data["FormItemss"] = retrieved*/

		var retrievedIFSC string
		retrievedIFSC = models.GetIFSC(ConfigurationStruct)
		c.Data["RetrievedIFSC"] = retrievedIFSC

		var retrievedBankName string
		retrievedBankName = models.GetBankName(ConfigurationStruct)
		c.Data["RetrievedBankName"] = retrievedBankName

		var retrievedAccountNo string
		retrievedAccountNo = models.GetAccountNo(ConfigurationStruct)
		c.Data["RetrievedAccountNo"] = retrievedAccountNo

		var retrievedPAN string
		retrievedPAN = models.GetPAN(ConfigurationStruct)
		c.Data["RetrievedPAN"] = retrievedPAN

		var retrievedGSTIN string
		retrievedGSTIN = models.GetGSTIN(ConfigurationStruct)
		c.Data["RetrievedGSTIN"] = retrievedGSTIN

		retrievedIndustry := make(map[string]orm.Params)
		retrievedIndustry = models.GetIndustry(ConfigurationStruct)
		c.Data["RetrievedIndustry"] = retrievedIndustry

		var retrievedEmail string
		retrievedEmail = models.GetEmail(ConfigurationStruct)
		c.Data["RetrievedEmail"] = retrievedEmail

		var retrievedMobile string
		retrievedMobile = models.GetMobile(ConfigurationStruct)
		c.Data["RetrievedMobile"] = retrievedMobile

		var retrievedBrand string
		retrievedBrand = models.GetBrand(ConfigurationStruct)
		c.Data["RetrievedBrand"] = retrievedBrand

		var retrievedEntityName string
		retrievedEntityName = models.GetEntity(ConfigurationStruct)
		c.Data["RetrievedEntityName"] = retrievedEntityName

		var retrievedHQlocation string
		retrievedHQlocation = models.GetHQLocation(ConfigurationStruct)
		c.Data["RetrievedHQlocation"] = retrievedHQlocation

		var retrievedPreviousHQ string
		retrievedPreviousHQ = models.GetPreviousHQ(ConfigurationStruct)
		c.Data["RetrievedPreviousHQ"] = retrievedPreviousHQ

		var retrievedIndustryType string
		retrievedIndustryType = models.GetIndustryType(ConfigurationStruct)
		c.Data["RetrievedIndustryType"] = retrievedIndustryType

		var retrievedFactoryLocation string
		retrievedFactoryLocation = models.GetFactoryLocation(ConfigurationStruct)
		c.Data["RetrievedFactoryLocation"] = retrievedFactoryLocation

		var retrievedOldAdditionalFactories string
		retrievedOldAdditionalFactories = models.GetOldAdditionalFactories(ConfigurationStruct)
		c.Data["RetrievedOldAdditionalFactories"] = retrievedOldAdditionalFactories

		c.TplName = "settings/organization-settings.html"
		c.Data["Settings"] = 1
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		c.Data["PageTitle"] = "Organization Settings"

		if _, ok := retrieved["3"]; ok {
			if c.Ctx.Input.Method() == "POST" {
				flash := beego.NewFlash()
				if c.GetString("hidden_method") == "config" {
					items := configValidate{}
					if err := c.ParseForm(&items); err != nil {
						flash.Error("Cannot parse form")
						flash.Store(&c.Controller)
						c.Data["ErrorsPresent"] = true
						return
					}
					c.Data["FormItems"] = items
					c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
					valid := validation.Validation{}
					if b, _ := valid.Valid(&items); !b {
						c.Data["Errors"] = valid.ErrorsMap
						c.Data["ErrorsPresent"] = true
						return
					}
					formStruct := models.Organisation{
						Orgidd:      _orgId,
						Userid:      user_id,
						Username:    username,
						EntityName:  items.EntityName,
						BrandName:   items.BrandName,
						Email:       items.Email,
						Mobile:      items.Mobile,
						HQLoc:       items.HQLoc,
						IndType:     items.IndType,
						LocationFac: items.LocationFac,
						GSTIN:       items.GSTIN,
						PAN:         items.PAN,
						BankName:    items.BankName,
						AccountNo:   items.AccountNo,
						IFSC:        items.IFSC,
					}

					//insertStatus := models.RegistrationInsert(formData)
					insertStatus := models.InsertConfiguration(formStruct)
					if insertStatus == 0 {
						flash.Error("An error occured while saving. Please try again.")
						flash.Store(&c.Controller)
						return
					} else if insertStatus == 1 {
						flash.Notice("The Organization Settings have been updated.")
						flash.Store(&c.Controller)
					}

				}
				c.Redirect("/settings/organization-settings", 303)
			}
		} else {
			c.Data["NoConfigEdit"] = true
		}
	} else {
		c.Redirect("/accessdenied", 303)
	}

}

func (c *SettingsController) TermAndConditions() {
	var user_id string
	var _orgId string
	var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}

	c.Data["username"] = username

	retrieved := make(map[string]string)
	retrieved = models.GetUserRolesByMap(username, _orgId)

	if _, ok := retrieved["4"]; ok {

		PoTcStruct := models.TC{
			Orgid:    _orgId,
			Userid:   user_id,
			Username: username,
		}

		retrievedPoContent := make(map[string]orm.Params)
		retrievedPoContent = models.GetPoTcContent(PoTcStruct)
		c.Data["RetrievedPoTcContent"] = retrievedPoContent

		retrievedSoContent := make(map[string]orm.Params)
		retrievedSoContent = models.GetSoTcContent(PoTcStruct)
		c.Data["RetrievedSoTcContent"] = retrievedSoContent

		c.TplName = "settings/terms-and-conditions.html"
		c.Data["Settings"] = 1
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		c.Data["PageTitle"] = "Terms and Conditions"

		if _, ok := retrieved["3"]; ok {
			if c.Ctx.Input.Method() == "POST" {
				// flash := beego.NewFlash()
				c.Redirect("/settings/term-and-conditions", 303)
			}
		} else {
			c.Data["NoTCEdit"] = true
		}
	} else {
		c.Redirect("/accessdenied", 303)
	}
}

func (c *SettingsController) PoTcUpdate() {
	flash := beego.ReadFromRequest(&c.Controller)
	if ok := flash.Data["error"]; ok != "" {
		c.Data["FlashErrors"] = ok
	}
	if ok := flash.Data["notice"]; ok != "" {
		// Display error messages
		c.Data["FlashNotices"] = ok
	}

	var user_id string
	var _orgId string
	var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}

	retrieved := make(map[string]string)
	retrieved = models.GetUserRolesByMap(username, _orgId)

	if _, ok := retrieved["4"]; ok {

		c.TplName = "settings/config.html"
		c.Data["Settings"] = 1
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		c.Data["PageTitle"] = "Configuration"
		c.Data["username"] = username

		if _, ok := retrieved["3"]; ok {
			if c.Ctx.Input.Method() == "POST" {
				flash := beego.NewFlash()

				items := PoTc{}
				if err := c.ParseForm(&items); err != nil {
					flash.Error("Cannot parse form")
					flash.Store(&c.Controller)
					c.Data["ErrorsPresent"] = true
					return
				}
				c.Data["FormItems"] = items
				c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
				valid := validation.Validation{}
				if b, _ := valid.Valid(&items); !b {
					c.Data["Errors"] = valid.ErrorsMap
					c.Data["ErrorsPresent"] = true
					return
				}
				formStruct := models.TC{
					Orgid:    _orgId,
					Userid:   user_id,
					Username: username,
					Content:  items.PoContent,
					Type:     "1",
				}

				insertStatus := models.InsertTc(formStruct)
				if insertStatus == 0 {
					flash.Error("An error occured while saving. Please try again.")
					flash.Store(&c.Controller)
					c.Data["ErrorsPresent"] = true
					return
				} else if insertStatus == 1 {
					flash.Notice("The configuration has been updated.")
					flash.Store(&c.Controller)
				}

				c.Redirect("/settings/term-and-conditions", 303)
			}
		} else {
			c.Data["NoConfigEdit"] = true
		}
	} else {
		c.Redirect("/accessdenied", 303)
	}
}

func (c *SettingsController) SoTcUpdate() {
	flash := beego.ReadFromRequest(&c.Controller)
	if ok := flash.Data["error"]; ok != "" {
		c.Data["FlashErrors"] = ok
	}
	if ok := flash.Data["notice"]; ok != "" {
		// Display error messages
		c.Data["FlashNotices"] = ok
	}

	var user_id string
	var _orgId string
	var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}

	retrieved := make(map[string]string)
	retrieved = models.GetUserRolesByMap(username, _orgId)

	if _, ok := retrieved["4"]; ok {

		c.TplName = "settings/config.html"
		c.Data["Settings"] = 1
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		c.Data["PageTitle"] = "Configuration"
		c.Data["username"] = username

		if _, ok := retrieved["3"]; ok {
			if c.Ctx.Input.Method() == "POST" {
				flash := beego.NewFlash()

				items := SoTc{}
				if err := c.ParseForm(&items); err != nil {
					flash.Error("Cannot parse form")
					flash.Store(&c.Controller)
					c.Data["ErrorsPresent"] = true
					return
				}
				c.Data["FormItems"] = items
				c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
				valid := validation.Validation{}
				if b, _ := valid.Valid(&items); !b {
					c.Data["Errors"] = valid.ErrorsMap
					c.Data["ErrorsPresent"] = true
					return
				}
				formStruct := models.TC{
					Orgid:    _orgId,
					Userid:   user_id,
					Username: username,
					Content:  items.SoContent,
					Type:     "2",
				}

				insertStatus := models.InsertTc(formStruct)
				if insertStatus == 0 {
					flash.Error("An error occured while saving. Please try again.")
					flash.Store(&c.Controller)
					c.Data["ErrorsPresent"] = true
					return
				} else if insertStatus == 1 {
					flash.Notice("The configuration has been updated.")
					flash.Store(&c.Controller)
				}

				c.Redirect("/settings/term-and-conditions", 303)
			}
		} else {
			c.Data["NoConfigEdit"] = true
		}
	} else {
		c.Redirect("/accessdenied", 303)
	}
}

func (c *SettingsController) ConfigurationPrefix() {

	var user_id string
	var _orgId string
	var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}

	c.Data["username"] = username
	retrieved := make(map[string]string)
	retrieved = models.GetUserRolesByMap(username, _orgId)

	if _, ok := retrieved["4"]; ok {

		c.Data["PrefixDropdownMaster"] = models.GetPrefixDropdownMaster()

		c.TplName = "settings/config-prefix.html"
		c.Data["Settings"] = 1
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		c.Data["PageTitle"] = "Prefix Master"

		if _, ok := retrieved["3"]; ok {
			prefixMap := make(map[string]orm.Params)
			prefixQueryFetchStatus := 0

			prefixMap, prefixQueryFetchStatus = models.GetPrefixCodesByOrganisation(_orgId, 1)
			if prefixQueryFetchStatus == 1 {
				c.Data["ItemValue1l"] = prefixMap["1L"]["value"]
				c.Data["ItemValue1r"] = prefixMap["1R"]["value"]
				c.Data["ItemValue2l"] = prefixMap["2L"]["prefix_dropdown_master_fkid"]
				c.Data["ItemValue2r"] = prefixMap["2R"]["prefix_dropdown_master_fkid"]
				c.Data["ItemValue3l"] = prefixMap["3L"]["prefix_dropdown_master_fkid"]
				c.Data["ItemValue3r"] = prefixMap["3R"]["prefix_dropdown_master_fkid"]
			}

			//THIS IS NOT USED AS WE DO NOT HAVE SEPARATE DROP DOWN FOR PRODUCT
			prefixMap, prefixQueryFetchStatus = models.GetPrefixCodesByOrganisation(_orgId, 2)
			if prefixQueryFetchStatus == 1 {
				c.Data["ProductValue1l"] = prefixMap["1L"]["value"]
				c.Data["ProductValue1r"] = prefixMap["1R"]["value"]
				c.Data["ProductValue2l"] = prefixMap["2L"]["prefix_dropdown_master_fkid"]
				c.Data["ProductValue2r"] = prefixMap["2R"]["prefix_dropdown_master_fkid"]
				c.Data["ProductValue3l"] = prefixMap["3L"]["prefix_dropdown_master_fkid"]
				c.Data["ProductValue3r"] = prefixMap["3R"]["prefix_dropdown_master_fkid"]
			}

			prefixMap, prefixQueryFetchStatus = models.GetPrefixCodesByOrganisation(_orgId, 3)
			if prefixQueryFetchStatus == 1 {
				c.Data["SupplierValue1l"] = prefixMap["1L"]["value"]
				c.Data["SupplierValue1r"] = prefixMap["1R"]["value"]
				c.Data["SupplierValue2l"] = prefixMap["2L"]["prefix_dropdown_master_fkid"]
				c.Data["SupplierValue2r"] = prefixMap["2R"]["prefix_dropdown_master_fkid"]
				c.Data["SupplierValue3l"] = prefixMap["3L"]["prefix_dropdown_master_fkid"]
				c.Data["SupplierValue3r"] = prefixMap["3R"]["prefix_dropdown_master_fkid"]
			}

			prefixMap, prefixQueryFetchStatus = models.GetPrefixCodesByOrganisation(_orgId, 4)
			if prefixQueryFetchStatus == 1 {
				c.Data["CustomerValue1l"] = prefixMap["1L"]["value"]
				c.Data["CustomerValue1r"] = prefixMap["1R"]["value"]
				c.Data["CustomerValue2l"] = prefixMap["2L"]["prefix_dropdown_master_fkid"]
				c.Data["CustomerValue2r"] = prefixMap["2R"]["prefix_dropdown_master_fkid"]
				c.Data["CustomerValue3l"] = prefixMap["3L"]["prefix_dropdown_master_fkid"]
				c.Data["CustomerValue3r"] = prefixMap["3R"]["prefix_dropdown_master_fkid"]
			}

			prefixMap, prefixQueryFetchStatus = models.GetPrefixCodesByOrganisation(_orgId, 5)
			if prefixQueryFetchStatus == 1 {
				c.Data["POValue1l"] = prefixMap["1L"]["value"]
				c.Data["POValue1r"] = prefixMap["1R"]["value"]
				c.Data["POValue2l"] = prefixMap["2L"]["prefix_dropdown_master_fkid"]
				c.Data["POValue2r"] = prefixMap["2R"]["prefix_dropdown_master_fkid"]
				c.Data["POValue3l"] = prefixMap["3L"]["prefix_dropdown_master_fkid"]
				c.Data["POValue3r"] = prefixMap["3R"]["prefix_dropdown_master_fkid"]
			}

			prefixMap, prefixQueryFetchStatus = models.GetPrefixCodesByOrganisation(_orgId, 6)
			if prefixQueryFetchStatus == 1 {
				c.Data["SOValue1l"] = prefixMap["1L"]["value"]
				c.Data["SOValue1r"] = prefixMap["1R"]["value"]
				c.Data["SOValue2l"] = prefixMap["2L"]["prefix_dropdown_master_fkid"]
				c.Data["SOValue2r"] = prefixMap["2R"]["prefix_dropdown_master_fkid"]
				c.Data["SOValue3l"] = prefixMap["3L"]["prefix_dropdown_master_fkid"]
				c.Data["SOValue3r"] = prefixMap["3R"]["prefix_dropdown_master_fkid"]
			}

			prefixMap, prefixQueryFetchStatus = models.GetPrefixCodesByOrganisation(_orgId, 7)
			if prefixQueryFetchStatus == 1 {
				c.Data["EmployeeValue1l"] = prefixMap["1L"]["value"]
				c.Data["EmployeeValue1r"] = prefixMap["1R"]["value"]
				c.Data["EmployeeValue2l"] = prefixMap["2L"]["prefix_dropdown_master_fkid"]
				c.Data["EmployeeValue2r"] = prefixMap["2R"]["prefix_dropdown_master_fkid"]
				c.Data["EmployeeValue3l"] = prefixMap["3L"]["prefix_dropdown_master_fkid"]
				c.Data["EmployeeValue3r"] = prefixMap["3R"]["prefix_dropdown_master_fkid"]
			}

			prefixMap, prefixQueryFetchStatus = models.GetPrefixCodesByOrganisation(_orgId, 8)
			if prefixQueryFetchStatus == 1 {
				c.Data["MachineCodeValue1l"] = prefixMap["1L"]["value"]
				c.Data["MachineCodeValue1r"] = prefixMap["1R"]["value"]
				c.Data["MachineCodeValue2l"] = prefixMap["2L"]["prefix_dropdown_master_fkid"]
				c.Data["MachineCodeValue2r"] = prefixMap["2R"]["prefix_dropdown_master_fkid"]
				c.Data["MachineCodeValue3l"] = prefixMap["3L"]["prefix_dropdown_master_fkid"]
				c.Data["MachineCodeValue3r"] = prefixMap["3R"]["prefix_dropdown_master_fkid"]
			}

			prefixMap, prefixQueryFetchStatus = models.GetPrefixCodesByOrganisation(_orgId, 9)
			if prefixQueryFetchStatus == 1 {
				c.Data["ProdOrderValue1l"] = prefixMap["1L"]["value"]
				c.Data["ProdOrderValue1r"] = prefixMap["1R"]["value"]
				c.Data["ProdOrderValue2l"] = prefixMap["2L"]["prefix_dropdown_master_fkid"]
				c.Data["ProdOrderValue2r"] = prefixMap["2R"]["prefix_dropdown_master_fkid"]
				c.Data["ProdOrderValue3l"] = prefixMap["3L"]["prefix_dropdown_master_fkid"]
				c.Data["ProdOrderValue3r"] = prefixMap["3R"]["prefix_dropdown_master_fkid"]
			}

			prefixMap, prefixQueryFetchStatus = models.GetPrefixCodesByOrganisation(_orgId, 10)
			if prefixQueryFetchStatus == 1 {
				c.Data["CustomerTicketValue1l"] = prefixMap["1L"]["value"]
				c.Data["CustomerTicketValue1r"] = prefixMap["1R"]["value"]
				c.Data["CustomerTicketValue2l"] = prefixMap["2L"]["prefix_dropdown_master_fkid"]
				c.Data["CustomerTicketValue2r"] = prefixMap["2R"]["prefix_dropdown_master_fkid"]
				c.Data["CustomerTicketValue3l"] = prefixMap["3L"]["prefix_dropdown_master_fkid"]
				c.Data["CustomerTicketValue3r"] = prefixMap["3R"]["prefix_dropdown_master_fkid"]
			}

			prefixMap, prefixQueryFetchStatus = models.GetPrefixCodesByOrganisation(_orgId, 11)
			if prefixQueryFetchStatus == 1 {
				c.Data["GRNValue1l"] = prefixMap["1L"]["value"]
				c.Data["GRNValue1r"] = prefixMap["1R"]["value"]
				c.Data["GRNValue2l"] = prefixMap["2L"]["prefix_dropdown_master_fkid"]
				c.Data["GRNValue2r"] = prefixMap["2R"]["prefix_dropdown_master_fkid"]
				c.Data["GRNValue3l"] = prefixMap["3L"]["prefix_dropdown_master_fkid"]
				c.Data["GRNValue3r"] = prefixMap["3R"]["prefix_dropdown_master_fkid"]
			}

			prefixMap, prefixQueryFetchStatus = models.GetPrefixCodesByOrganisation(_orgId, 12)
			if prefixQueryFetchStatus == 1 {
				c.Data["DispatchValue1l"] = prefixMap["1L"]["value"]
				c.Data["DispatchValue1r"] = prefixMap["1R"]["value"]
				c.Data["DispatchValue2l"] = prefixMap["2L"]["prefix_dropdown_master_fkid"]
				c.Data["DispatchValue2r"] = prefixMap["2R"]["prefix_dropdown_master_fkid"]
				c.Data["DispatchValue3l"] = prefixMap["3L"]["prefix_dropdown_master_fkid"]
				c.Data["DispatchValue3r"] = prefixMap["3R"]["prefix_dropdown_master_fkid"]
			}

			if c.Ctx.Input.Method() == "POST" {
				flash := beego.NewFlash()

				var queryParamMap = make(map[string]string)

				if c.GetString("prefix_master") == "1" {
					formInputs := PrefixFormItem{}
					if err := c.ParseForm(&formInputs); err != nil {
						flash.Error("Cannot parse form")
						flash.Store(&c.Controller)
						return
					}
					c.Data["FormItems"] = formInputs
					c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
					valid := validation.Validation{}
					if b, _ := valid.Valid(&formInputs); !b {
						c.Data["Errors"] = valid.ErrorsMap
						c.Data["ErrorsPresent"] = true
						return
					}

					queryParamMap["added_by_fkid"] = user_id
					queryParamMap["prefix_master_fkid"] = "1"
					queryParamMap["1l"] = formInputs.ItemCode1l
					queryParamMap["1r"] = formInputs.ItemCode1r
					queryParamMap["2l"] = formInputs.ItemCode2l
					queryParamMap["3l"] = formInputs.ItemCode3l
					queryParamMap["2r"] = formInputs.ItemCode2r
					queryParamMap["3r"] = formInputs.ItemCode3r
				}

				if c.GetString("prefix_master") == "2" {
					formInputs := PrefixFormProduct{}
					if err := c.ParseForm(&formInputs); err != nil {
						flash.Error("Cannot parse form")
						flash.Store(&c.Controller)
						return
					}
					c.Data["FormItems"] = formInputs
					c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
					valid := validation.Validation{}
					if b, _ := valid.Valid(&formInputs); !b {
						c.Data["Errors"] = valid.ErrorsMap
						c.Data["ErrorsPresent"] = true
						return
					}

					queryParamMap["added_by_fkid"] = user_id
					queryParamMap["prefix_master_fkid"] = "2"
					queryParamMap["1l"] = formInputs.ProductCode1l
					queryParamMap["1r"] = formInputs.ProductCode1r
					queryParamMap["2l"] = formInputs.ProductCode2l
					queryParamMap["3l"] = formInputs.ProductCode3l
					queryParamMap["2r"] = formInputs.ProductCode2r
					queryParamMap["3r"] = formInputs.ProductCode3r
				}

				if c.GetString("prefix_master") == "3" {
					formInputs := PrefixFormSupplier{}
					if err := c.ParseForm(&formInputs); err != nil {
						flash.Error("Cannot parse form")
						flash.Store(&c.Controller)
						return
					}
					c.Data["FormItems"] = formInputs
					c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
					valid := validation.Validation{}
					if b, _ := valid.Valid(&formInputs); !b {
						c.Data["Errors"] = valid.ErrorsMap
						c.Data["ErrorsPresent"] = true
						return
					}

					queryParamMap["added_by_fkid"] = user_id
					queryParamMap["prefix_master_fkid"] = "3"
					queryParamMap["1l"] = formInputs.SupplierCode1l
					queryParamMap["1r"] = formInputs.SupplierCode1r
					queryParamMap["2l"] = formInputs.SupplierCode2l
					queryParamMap["3l"] = formInputs.SupplierCode3l
					queryParamMap["2r"] = formInputs.SupplierCode2r
					queryParamMap["3r"] = formInputs.SupplierCode2r
				}

				if c.GetString("prefix_master") == "4" {
					formInputs := PrefixFormCustomer{}
					if err := c.ParseForm(&formInputs); err != nil {
						flash.Error("Cannot parse form")
						flash.Store(&c.Controller)
						return
					}
					c.Data["FormItems"] = formInputs
					c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
					valid := validation.Validation{}
					if b, _ := valid.Valid(&formInputs); !b {
						c.Data["Errors"] = valid.ErrorsMap
						c.Data["ErrorsPresent"] = true
						return
					}

					queryParamMap["added_by_fkid"] = user_id
					queryParamMap["prefix_master_fkid"] = "4"
					queryParamMap["1l"] = formInputs.CustomerCode1l
					queryParamMap["1r"] = formInputs.CustomerCode1r
					queryParamMap["2l"] = formInputs.CustomerCode2l
					queryParamMap["3l"] = formInputs.CustomerCode3l
					queryParamMap["2r"] = formInputs.CustomerCode2r
					queryParamMap["3r"] = formInputs.CustomerCode3r
				}

				if c.GetString("prefix_master") == "5" {
					formInputs := PrefixFormPO{}
					if err := c.ParseForm(&formInputs); err != nil {
						flash.Error("Cannot parse form")
						flash.Store(&c.Controller)
						return
					}
					c.Data["FormItems"] = formInputs
					c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
					valid := validation.Validation{}
					if b, _ := valid.Valid(&formInputs); !b {
						c.Data["Errors"] = valid.ErrorsMap
						c.Data["ErrorsPresent"] = true
						return
					}

					queryParamMap["added_by_fkid"] = user_id
					queryParamMap["prefix_master_fkid"] = "5"
					queryParamMap["1l"] = formInputs.POCode1l
					queryParamMap["1r"] = formInputs.POCode1r
					queryParamMap["2l"] = formInputs.POCode2l
					queryParamMap["3l"] = formInputs.POCode3l
					queryParamMap["2r"] = formInputs.POCode2r
					queryParamMap["3r"] = formInputs.POCode3r
				}

				if c.GetString("prefix_master") == "6" {
					formInputs := PrefixFormSO{}
					if err := c.ParseForm(&formInputs); err != nil {
						flash.Error("Cannot parse form")
						flash.Store(&c.Controller)
						return
					}
					c.Data["FormItems"] = formInputs
					c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
					valid := validation.Validation{}
					if b, _ := valid.Valid(&formInputs); !b {
						c.Data["Errors"] = valid.ErrorsMap
						c.Data["ErrorsPresent"] = true
						return
					}

					queryParamMap["added_by_fkid"] = user_id
					queryParamMap["prefix_master_fkid"] = "6"
					queryParamMap["1l"] = formInputs.SOCode1l
					queryParamMap["1r"] = formInputs.SOCode1r
					queryParamMap["2l"] = formInputs.SOCode2l
					queryParamMap["3l"] = formInputs.SOCode3l
					queryParamMap["2r"] = formInputs.SOCode2r
					queryParamMap["3r"] = formInputs.SOCode3r
				}

				if c.GetString("prefix_master") == "7" {
					formInputs := PrefixFormEmployeeCode{}
					if err := c.ParseForm(&formInputs); err != nil {
						flash.Error("Cannot parse form")
						flash.Store(&c.Controller)
						return
					}
					c.Data["FormItems"] = formInputs
					c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
					valid := validation.Validation{}
					if b, _ := valid.Valid(&formInputs); !b {
						c.Data["Errors"] = valid.ErrorsMap
						c.Data["ErrorsPresent"] = true
						return
					}

					queryParamMap["added_by_fkid"] = user_id
					queryParamMap["prefix_master_fkid"] = "7"
					queryParamMap["1l"] = formInputs.EmployeeCode1l
					queryParamMap["1r"] = formInputs.EmployeeCode1r
					queryParamMap["2l"] = formInputs.EmployeeCode2l
					queryParamMap["3l"] = formInputs.EmployeeCode3l
					queryParamMap["2r"] = formInputs.EmployeeCode2r
					queryParamMap["3r"] = formInputs.EmployeeCode3r
				}

				if c.GetString("prefix_master") == "8" {
					formInputs := PrefixFormMachineCode{}
					if err := c.ParseForm(&formInputs); err != nil {
						flash.Error("Cannot parse form")
						flash.Store(&c.Controller)
						return
					}
					c.Data["FormItems"] = formInputs
					c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
					valid := validation.Validation{}
					if b, _ := valid.Valid(&formInputs); !b {
						c.Data["Errors"] = valid.ErrorsMap
						c.Data["ErrorsPresent"] = true
						return
					}

					queryParamMap["added_by_fkid"] = user_id
					queryParamMap["prefix_master_fkid"] = "8"
					queryParamMap["1l"] = formInputs.MachineCode1l
					queryParamMap["1r"] = formInputs.MachineCode1r
					queryParamMap["2l"] = formInputs.MachineCode2l
					queryParamMap["3l"] = formInputs.MachineCode3l
					queryParamMap["2r"] = formInputs.MachineCode2r
					queryParamMap["3r"] = formInputs.MachineCode3r
				}

				if c.GetString("prefix_master") == "9" {
					formInputs := PrefixFormProdOrder{}
					if err := c.ParseForm(&formInputs); err != nil {
						flash.Error("Cannot parse form")
						flash.Store(&c.Controller)
						return
					}
					c.Data["FormItems"] = formInputs
					c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
					valid := validation.Validation{}
					if b, _ := valid.Valid(&formInputs); !b {
						c.Data["Errors"] = valid.ErrorsMap
						c.Data["ErrorsPresent"] = true
						return
					}

					queryParamMap["added_by_fkid"] = user_id
					queryParamMap["prefix_master_fkid"] = "9"
					queryParamMap["1l"] = formInputs.ProdOrderCode1l
					queryParamMap["1r"] = formInputs.ProdOrderCode1r
					queryParamMap["2l"] = formInputs.ProdOrderCode2l
					queryParamMap["3l"] = formInputs.ProdOrderCode3l
					queryParamMap["2r"] = formInputs.ProdOrderCode2r
					queryParamMap["3r"] = formInputs.ProdOrderCode3r
				}

				if c.GetString("prefix_master") == "10" {
					formInputs := PrefixFormCustomer{}
					if err := c.ParseForm(&formInputs); err != nil {
						flash.Error("Cannot parse form")
						flash.Store(&c.Controller)
						return
					}
					c.Data["FormItems"] = formInputs
					c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
					valid := validation.Validation{}
					if b, _ := valid.Valid(&formInputs); !b {
						c.Data["Errors"] = valid.ErrorsMap
						c.Data["ErrorsPresent"] = true
						return
					}

					queryParamMap["added_by_fkid"] = user_id
					queryParamMap["prefix_master_fkid"] = "10"
					queryParamMap["1l"] = formInputs.CustomerCode1l
					queryParamMap["1r"] = formInputs.CustomerCode1r
					queryParamMap["2l"] = formInputs.CustomerCode2l
					queryParamMap["3l"] = formInputs.CustomerCode3l
					queryParamMap["2r"] = formInputs.CustomerCode2r
					queryParamMap["3r"] = formInputs.CustomerCode3r
				}

				if c.GetString("prefix_master") == "11" {
					formInputs := PrefixFormGRNCode{}
					if err := c.ParseForm(&formInputs); err != nil {
						flash.Error("Cannot parse form")
						flash.Store(&c.Controller)
						return
					}
					c.Data["FormItems"] = formInputs
					c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
					valid := validation.Validation{}
					if b, _ := valid.Valid(&formInputs); !b {
						c.Data["Errors"] = valid.ErrorsMap
						c.Data["ErrorsPresent"] = true
						return
					}

					queryParamMap["added_by_fkid"] = user_id
					queryParamMap["prefix_master_fkid"] = "11"
					queryParamMap["1l"] = formInputs.GRNCode1l
					queryParamMap["1r"] = formInputs.GRNCode1r
					queryParamMap["2l"] = formInputs.GRNCode2l
					queryParamMap["3l"] = formInputs.GRNCode3l
					queryParamMap["2r"] = formInputs.GRNCode2r
					queryParamMap["3r"] = formInputs.GRNCode3r
				}

				if c.GetString("prefix_master") == "12" {
					formInputs := PrefixFormDispatchCode{}
					if err := c.ParseForm(&formInputs); err != nil {
						flash.Error("Cannot parse form")
						flash.Store(&c.Controller)
						return
					}
					c.Data["FormItems"] = formInputs
					c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
					valid := validation.Validation{}
					if b, _ := valid.Valid(&formInputs); !b {
						c.Data["Errors"] = valid.ErrorsMap
						c.Data["ErrorsPresent"] = true
						return
					}

					queryParamMap["added_by_fkid"] = user_id
					queryParamMap["prefix_master_fkid"] = "12"
					queryParamMap["1l"] = formInputs.DispatchCode1l
					queryParamMap["1r"] = formInputs.DispatchCode1r
					queryParamMap["2l"] = formInputs.DispatchCode2l
					queryParamMap["3l"] = formInputs.DispatchCode3l
					queryParamMap["2r"] = formInputs.DispatchCode2r
					queryParamMap["3r"] = formInputs.DispatchCode3r
				}

				insertStatus := models.InsertPrefixCodesByOrganisation(_orgId, queryParamMap)
				if insertStatus == 1 {
					flash.Success("Updated Succesfully")
					flash.Store(&c.Controller)
					c.Redirect("/settings/prefix-codes", 303)
				}
			}
		} else {
			c.Data["NoConfigEdit"] = true
		}
	} else {
		c.Redirect("/accessdenied", 303)
	}
}

func (c *SettingsController) RemoveFactoriesAjax() {
	var user_id string
	var _orgId string
	var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}
	c.Data["username"] = username
	UserSessionStruct := models.Prefix{
		Orgid:    _orgId,
		Userid:   user_id,
		Username: username,
	}

	if c.Ctx.Input.Method() == "GET" {

		factoryTag := c.GetString("factory")
		//fmt.Println(affectedAction, affectedUser)

		result := models.RemoveFactoryTag(factoryTag, UserSessionStruct)
		//fmt.Println(result)
		c.Ctx.Output.Body([]byte(result))
		//c.Data["completeRegister"] = true
	}
}
