package controllers

import (
	"encoding/json"
	"fmt"
	"html/template"

	"bitbucket.org/caslinoerp/singulerp/models"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"github.com/astaxie/beego/validation"
)

type QaController struct {
	BaseController
}

type configureValidate struct {
	ReportNo     string `form:"report_no" valid:"Required"`
	PartyName    string `form:"party_name" valid:"Required"`
	InvoiceNo    string `form:"invoice_no" valid:"Required"`
	SrNo         string `form:"sr_no" valid:"Required"`
	Material     string `form:"material" valid:"Required"`
	ProductName  string `form:"p_name" valid:"Required" `
	Date         string `form:"date" valid:"Required"`
	Description  string `form:"description" valid:"Required"`
	Observations string `form:"observations" valid:"Required"`
	Checking     string `form:"checking" `
	Remark       string `form:"remark" valid:"Required"`
}

type createReportValidate struct {
	ProductId   string `form:"product_id" valid:"Required"`
	ProductName string `form:"p_name" valid:"Required" `
	SrNo        string `form:"sr_no" valid:"Required"`
	Operations  string `form:"operations" valid:"Required"`
	//InvoiceNo          string `form:"invoice_no" valid:"Required"`
	Remarks string `form:"remarks" valid:"Required"`
}

type createMasterReportValidate struct {
	Report string `form:"report" valid:"Required"`
}

type masterReportValidate struct {
	ReportName  string `form:"report_name" valid:"Required"`
	ReportType  string `form:"report_type" valid:"Required" `
	ReportCode  string `form:"report_code" valid:"Required"`
	FieldName1  string `form:"name1" valid:"Required"`
	FieldType1  string `form:"type1" valid:"Required"`
	FieldName2  string `form:"name2" `
	FieldType2  string `form:"type2" `
	FieldName3  string `form:"name3" `
	FieldType3  string `form:"type3" `
	FieldName4  string `form:"name4" `
	FieldType4  string `form:"type4" `
	FieldName5  string `form:"name5" `
	FieldType5  string `form:"type5" `
	FieldName6  string `form:"name6" `
	FieldType6  string `form:"type6" `
	FieldName7  string `form:"name7" `
	FieldType7  string `form:"type7" `
	FieldName8  string `form:"name8" `
	FieldType8  string `form:"type8" `
	FieldName9  string `form:"name9" `
	FieldType9  string `form:"type9" `
	FieldName10 string `form:"name10" `
	FieldType10 string `form:"type10" `
	FieldName11 string `form:"name11" `
	FieldType11 string `form:"type11" `
	FieldName12 string `form:"name12" `
	FieldType12 string `form:"type12" `
	FieldName13 string `form:"name13" `
	FieldType13 string `form:"type13" `
	FieldName14 string `form:"name14" `
	FieldType14 string `form:"type14" `
	FieldName15 string `form:"name15" `
	FieldType15 string `form:"type15" `
}

type mystructreport struct {
	FieldName []string `json:"field_name"`
	FieldType []string `json:"field_type"`
	ListOrder []string `json:"listing_order"`
	Status    string   `json:"status"`
}

func (c *QaController) Master() {
	var user_id string
	var _orgId string
	var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}
	c.Data["username"] = username
	formStruct1 := models.MasterReport{
		Orgid:    _orgId,
		Userid:   user_id,
		Username: username,
	}
	retrieved := make(map[string]orm.Params)
	retrieved = models.GetMasterReport(formStruct1)
	c.Data["Retrieved"] = retrieved

	c.TplName = "qa/master.html"
	c.Data["PageTitle"] = "Master"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	//generic code to handle post request and validation

	if c.Ctx.Input.Method() == "POST" {
		flash := beego.NewFlash()
		items := masterReportValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		if models.CheckForExistingCode(items.ReportCode, _orgId) == 1 {
			c.Data["codeExist"] = 1
			flash.Warning("This report code already exists.")
			flash.Store(&c.Controller)
			c.Data["ErrorsPresent"] = true
			return
		}
		if models.CheckForExistingName(items.ReportName, _orgId) == 1 {
			c.Data["nameExist"] = 1
			flash.Warning("This report name already exists.")
			flash.Store(&c.Controller)
			c.Data["ErrorsPresent"] = true
			return
		}
		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			//fmt.Println("fdghdgdgdgh")
			c.Data["Errors"] = valid.ErrorsMap
			c.Data["ErrorsPresent"] = true
			return
		}

		formStruct := models.MasterReport{
			Orgid:       _orgId,
			Userid:      user_id,
			Username:    username,
			ReportName:  items.ReportName,
			ReportType:  items.ReportType,
			ReportCode:  items.ReportCode,
			FieldName1:  items.FieldName1,
			FieldType1:  items.FieldType1,
			FieldName2:  items.FieldName2,
			FieldType2:  items.FieldType2,
			FieldName3:  items.FieldName3,
			FieldType3:  items.FieldType3,
			FieldName4:  items.FieldName4,
			FieldType4:  items.FieldType4,
			FieldName5:  items.FieldName5,
			FieldType5:  items.FieldType5,
			FieldName6:  items.FieldName6,
			FieldType6:  items.FieldType6,
			FieldName7:  items.FieldName7,
			FieldType7:  items.FieldType7,
			FieldName8:  items.FieldName8,
			FieldType8:  items.FieldType8,
			FieldName9:  items.FieldName9,
			FieldType9:  items.FieldType9,
			FieldName10: items.FieldName10,
			FieldType10: items.FieldType10,
			FieldName11: items.FieldName11,
			FieldType11: items.FieldType11,
			FieldName12: items.FieldName12,
			FieldType12: items.FieldType12,
			FieldName13: items.FieldName13,
			FieldType13: items.FieldType13,
			FieldName14: items.FieldName14,
			FieldType14: items.FieldType14,
			FieldName15: items.FieldName15,
			FieldType15: items.FieldType15,
		}
		//insertStatus := models.RegistrationInsert(formData)
		insertStatus := models.CreateMasterReport(formStruct)
		if insertStatus == 0 {
			flash.Error("An error occured while saving. Please try again.")
			flash.Store(&c.Controller)
			return
		}
		c.Redirect("/reports/master", 303)
	}
}

func (c *QaController) Configure() {
	var user_id string
	var _orgId string
	var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}
	c.Data["username"] = username
	formStruct1 := models.ConfigureM{
		Orgid:    _orgId,
		Userid:   user_id,
		Username: username,
	}
	retrieved := make(map[string]orm.Params)
	retrieved = models.GetConfigure(formStruct1)
	c.Data["Retrieved"] = retrieved
	c.TplName = "qa/configure.html"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Configure"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	//generic code to handle post request and validation
	if c.Ctx.Input.Method() == "POST" {
		flash := beego.NewFlash()
		items := configureValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			c.Data["ErrorsPresent"] = true
			return
		}

		formStruct := models.ConfigureM{
			Orgid:         _orgId,
			Userid:        user_id,
			Username:      username,
			ReportNo:      items.ReportNo,
			PartyName:     items.PartyName,
			InvoiceNo:     items.InvoiceNo,
			Date:          items.Date,
			Product:       items.ProductName,
			Product_sr_no: items.SrNo,
			Material:      items.Material,
			Description:   items.Description,
			Observations:  items.Observations,
			Checking:      items.Checking,
			Remark:        items.Remark,
		}
		//insertStatus := models.RegistrationInsert(formData)
		insertStatus := models.Configure(formStruct)
		if insertStatus == 0 {
			flash.Error("An error occured while saving. Please try again.")
			flash.Store(&c.Controller)
			return
		}
		c.Redirect("/qa/configure", 303)
	}
}

func (c *QaController) CreateReport() {
	var user_id string
	var _orgId string
	var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}
	c.Data["username"] = username
	formStruct1 := models.ReportM{
		Orgid:    _orgId,
		Userid:   user_id,
		Username: username,
	}
	retrieved := make(map[string]orm.Params)
	retrieved = models.GetReport(formStruct1)
	c.Data["Retrieved"] = retrieved
	c.TplName = "qa/create-report.html"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Create Report"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	//generic code to handle post request and validation
	if c.Ctx.Input.Method() == "POST" {
		flash := beego.NewFlash()
		items := createReportValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			c.Data["ErrorsPresent"] = true
			return
		}

		formStruct := models.ReportM{
			Orgid:         _orgId,
			Userid:        user_id,
			Username:      username,
			ReportNo:      items.ProductId,
			Product:       items.ProductName,
			Product_sr_no: items.SrNo,
			Operations:    items.Operations,
			Remark:        items.Remarks,
		}
		//insertStatus := models.RegistrationInsert(formData)
		insertStatus := models.CreateReport(formStruct)
		if insertStatus == 0 {
			flash.Error("An error occured while saving. Please try again.")
			flash.Store(&c.Controller)
			return
		}

		c.Redirect("/qa/create-report", 303)
	}
}

func (c *QaController) ReportFormat() {

	var _orgId string
	//var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		//user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		//username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		//user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		//username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}

	c.TplName = "qa/master.html"
	c.Data["PageTitle"] = "Master"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	if c.Ctx.Input.Method() == "GET" {

		product_id := c.GetString("id")
		//retrieved :=make(map[string]orm.Params)

		field_name, field_type, status, listing_order := models.GetReportFormat(product_id, _orgId)
		outputObj := &mystructreport{
			FieldName: field_name,
			FieldType: field_type,
			ListOrder: listing_order,

			Status: status,
		}
		//fmt.Println(outputObj)
		b, _ := json.Marshal(outputObj)
		//fmt.Println("***string b", string(b))
		//c.Data["ProductRetrieved"] = retrieved
		//fmt.Println("****Result:",result)
		c.Ctx.Output.Body([]byte(b))
	}
}

func (c *QaController) CreateMasterReport() {
	var user_id string
	var _orgId string
	var username string
	sessionVar := c.StartSession()
	if sessionVar != nil {
		user_id = fmt.Sprintf("%s", sessionVar.Get("user_id"))
		_orgId = fmt.Sprintf("%s", sessionVar.Get("_orgid"))
		username = fmt.Sprintf("%s", sessionVar.Get("username"))
	} else {
		user_id = fmt.Sprintf("%s", c.Ctx.Input.GetData("user_id"))
		_orgId = fmt.Sprintf("%s", c.Ctx.Input.GetData("_orgid"))
		username = fmt.Sprintf("%s", c.Ctx.Input.GetData("username"))
	}
	c.Data["username"] = username
	formStruct1 := models.MasterReport{
		Orgid:    _orgId,
		Userid:   user_id,
		Username: username,
	}
	retrieved := make(map[string]orm.Params)
	retrieved = models.GetMasterReport2(formStruct1)
	c.Data["RetrievedMaster"] = retrieved
	c.TplName = "qa/create-master-report.html"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Create Report"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	//generic code to handle post request and validation
	if c.Ctx.Input.Method() == "POST" {
		flash := beego.NewFlash()
		items := createMasterReportValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			c.Data["ErrorsPresent"] = true
			return
		}
		formStruct2 := models.MasterReport{
			Orgid:    _orgId,
			Userid:   user_id,
			Username: username,
			Report:   items.Report,
		}
		retrievedFields := make(map[string]orm.Params)
		retrievedFields = models.GetMasterFields(formStruct2)
		c.Data["RetrievedFields"] = retrievedFields

		c.Redirect("/qa/create-master-report", 303)
	}
}
