package controllers

import (
	"fmt"
	"html/template"

	"bitbucket.org/caslinoerp/singulerp/models"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/validation"
)

type LoginController struct {
	BaseController
}

type credentialValidate struct {
	UserName string `form:"username" valid:"Required"`
	Password string `form:"password" valid:"Required"`
	OrgName  string `form:"orgname" valid:"Required"`
}

func (c *LoginController) Login() {
	c.TplName = "login.html"
	c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
	c.Data["PageTitle"] = "Login"

	if c.Ctx.Input.Method() == "GET" {

		v := c.GetSession("SERPSession")
		if v != nil {
			c.SetSession("SERPSession", v.(int)+1)
			//fmt.Println("Session already set")
			c.Redirect("/dashboard", 301)
		}
	}

	if c.Ctx.Input.Method() == "POST" {

		flash := beego.NewFlash()
		items := credentialValidate{}
		if err := c.ParseForm(&items); err != nil {
			flash.Error("Cannot parse form")
			flash.Store(&c.Controller)
			return
		}
		c.Data["FormItems"] = items
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		valid := validation.Validation{}
		if b, _ := valid.Valid(&items); !b {
			c.Data["Errors"] = valid.ErrorsMap
			return
		}
		result := make(map[string]string)
		data := make(map[string]string)
		data["UserName"] = items.UserName
		data["Password"] = items.Password
		data["Orgname"] = items.OrgName

		result = models.Login(data)

		if result["status"] == "99" {
			flash.Error("Your account is pending activation. Thank you for your patience while we activate it.")
			flash.Store(&c.Controller)
			return
		} else if result["status"] == "1" {

			c.Data["completeRegister"] = true

			if beego.AppConfig.String("SessionOn") == "true" {
				v := c.GetSession("SERPSession")
				if v == nil {
					c.SetSession("SERPSession", int(1))
					c.SetSession("user_id", result["user_id"])
					c.SetSession("username", result["username"])
					c.SetSession("_orgid", result["_orgid"])
					c.SetSession("Orgname", result["Orgname"])
					c.SetSession("LocaleTimeZone", "Asia/Kolkata")
					// retrieved := make(map[string]string)
					// retrieved = models.GetUserRolesByMap(items.UserName, result["_orgid"])
					// c.SetSession("user_roles", retrieved)
					c.Redirect("/dashboard", 301)
				} else {
					c.SetSession("SERPSession", v.(int)+1)
					fmt.Println("Session already set")
					c.Redirect("/dashboard", 301)
				}
			} //else if beego.BConfig.RunMode == "dev" {
			// fmt.Println("In devmode login=")
			// c.Ctx.Input.SetData("_orgid", result["_orgid"])
			// fmt.Println(c.Ctx.Input.GetData("_orgid"))
			// c.Ctx.Input.SetData("user_id", result["user_id"])
			// fmt.Println(c.Ctx.Input.GetData("user_id"))
			// fmt.Println("end of login")
			//c.Redirect("/dashboard", 303)
			//}

			c.Redirect("/sessionerror", 303)
		} else {
			flash.Error("You seem to have entered a wrong organisation name, username or password. Please try again.")
			flash.Store(&c.Controller)
			return
		}
	}
}

func (c *LoginController) Logout() {

	if beego.AppConfig.String("SessionOn") == "true" {
		v := c.GetSession("SERPSession")
		if v == nil {
			c.TplName = "logout.html"
			c.Data["notLoggedin"] = 1
			c.Data["PageTitle"] = "Logout"
		} else {
			c.DestroySession()
			c.TplName = "logout.html"
			c.Data["PageTitle"] = "Logout"
		}
	} else {
		c.Redirect("/dashboard", 302)
	}
	if c.Ctx.Input.Method() == "POST" {
		if beego.AppConfig.String("SessionOn") == "true" {
			v := c.GetSession("SERPSession")
			if v == nil {
				c.Redirect("/login", 302)
			} else {
				c.DestroySession()
				c.Redirect("/login", 302)
			}
		} else {
			c.Redirect("/dashboard", 302)
		}
	}
}
