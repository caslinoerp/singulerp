package main

//add all dependencies here and run on new system
//go get golang.org/x/crypto/bcrypt
//go get github.com/astaxie/beego
//go get github.com/astaxie/beego/validation
//go get github.com/astaxie/beego/context
//go get github.com/astaxie/beego/orm
//go get github.com/go-sql-driver/mysql
//go get -u github.com/gocarina/gocsv
//go get gopkg.in/mailgun/mailgun-go.v1
//go get github.com/jung-kurt/gofpdf

import (
	"fmt"
	"html/template"
	"net/http"
	"strings"

	_ "bitbucket.org/caslinoerp/singulerp/routers"

	"github.com/astaxie/beego/context"
	"github.com/astaxie/beego/orm"

	"github.com/astaxie/beego"
	_ "github.com/astaxie/beego/session/mysql"
	_ "github.com/go-sql-driver/mysql"
)

func dbError(rw http.ResponseWriter, r *http.Request) {
	t, _ := template.ParseFiles(beego.AppConfig.String("viewspath") + "/dberror.html")
	data := make(map[string]interface{})
	data["content"] = "database is now down"
	t.Execute(rw, data)
}

func init() {

	if beego.AppConfig.String("usedb") == "true" {
		orm.RegisterDriver("mysql", orm.DRMySQL)

		var dbDSN = fmt.Sprintf("%s:%s@tcp(%s)/%s?charset=utf8", beego.AppConfig.String("dbUser"), beego.AppConfig.String("dbPass"), beego.AppConfig.String("dbHost"), beego.AppConfig.String("dbName"))
		//fmt.Println(dbDSN)
		err := orm.RegisterDataBase("default", "mysql", dbDSN)

		//err := orm.RegisterDataBase("default", "mysql", "root:root@tcp(127.0.0.1:3306)/singulerp?charset=utf8")
		if err != nil {

		}
	}

	//gob.Register(GlobalSessions)
	// globalSessions, _ = session.NewManager("mysql", `{"cookieName":"gosessionid","gclifetime":3600,"ProviderConfig":"[root[:root]@][localhost[(3306)]]/singulerp"}`)

	// globalSessions, _ = session.NewManager("memory", `{"cookieName":"gosessionid", "enableSetCookie,omitempty": true, "gclifetime":3600, "maxLifetime": 3600, "secure": false, "sessionIDHashFunc": "sha1", "sessionIDHashKey": "", "cookieLifeTime": 3600, "providerConfig": ""}`)

	// var providerValues = session.ManagerConfig{}
	// providerValues.CookieName = "SERPSession"
	// providerValues.Gclifetime = 3600
	// providerValues.ProviderConfig = "root:root@localhost(3306)/singulerp"
	// globalSessions, _ = session.NewManager("mysql", &providerValues)
	// go globalSessions.GC()

}

func main() {

	if beego.BConfig.RunMode == "dev" {
		orm.Debug = true
	}
	//filter to see that user is logged in and has a session. Executes only if Session configuration variable is true.

	if beego.AppConfig.String("SessionOn") == "true" {
		var FilterUser = func(ctx *context.Context) {
			if strings.HasPrefix(ctx.Input.URL(), "/login") {
				return
			}
			if strings.HasPrefix(ctx.Input.URL(), "/logout") {
				return
			}
			if strings.HasPrefix(ctx.Input.URL(), "/register") {
				return
			}
			if strings.HasPrefix(ctx.Input.URL(), "/registration_complete") {
				return
			}
			if strings.HasPrefix(ctx.Input.URL(), "/registration_verification") {
				return
			}
			if strings.HasPrefix(ctx.Input.URL(), "/sessionerror") {
				return
			}
			//_, ok := ctx.Input.Session("uid").(int)
			_, ok := ctx.Input.Session("SERPSession").(int)
			if !ok {
				ctx.Redirect(302, "/login")
			}
		}
		beego.InsertFilter("/*", beego.BeforeRouter, FilterUser)
	}

	//check config to see if database is to be used and then trigger db check on every visit
	var dbErrorCheck = func(ctx *context.Context) {
		if strings.HasPrefix(ctx.Input.URL(), "/dberror") {
			return
		}
		if strings.HasPrefix(ctx.Input.URL(), "/assets") {
			return
		}

		db, err := orm.GetDB("default")
		if db != nil {
			return
		}
		if err != nil {
			fmt.Println(err)
			ctx.Redirect(302, "/dberror")
		}
	}
	beego.InsertFilter("/*", beego.BeforeRouter, dbErrorCheck)

	if beego.AppConfig.String("SessionOn") == "false" && beego.BConfig.RunMode == "dev" {
		var injectOrgID = func(ctx *context.Context) {
			if ctx.Input.GetData("_orgid") == nil {
				ctx.Input.SetData("_orgid", "1")
			}
			if ctx.Input.GetData("user_id") == nil {
				ctx.Input.SetData("user_id", "1")
			}
			if ctx.Input.GetData("username") == nil {
				ctx.Input.SetData("username", "devuser")
			}
		}
		beego.InsertFilter("/*", beego.BeforeExec, injectOrgID)
	}

	beego.ErrorHandler("dbError", dbError)

	beego.Run()
}
