--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `_orgid` bigint(10) unsigned NOT NULL,
  `company_name` varchar(30) NOT NULL,
  `grp` varchar(20) DEFAULT NULL,
  `address` varchar(45) DEFAULT NULL,
  `city` varchar(20) DEFAULT NULL,
  `state` varchar(20) DEFAULT NULL,
  `pincode` int(30) DEFAULT NULL,
  `country` varchar(20) DEFAULT NULL,
  `TIN` int(11) DEFAULT NULL,
  `area_code` int(11) DEFAULT NULL,
  `PAN` varchar(20) DEFAULT NULL,
  `credit_in_days` int(11) DEFAULT NULL,
  `CST` int(11) DEFAULT NULL,
  `credit_limit` int(11) DEFAULT NULL,
  `ECC` int(11) DEFAULT NULL,
  `customer_type` varchar(20) DEFAULT NULL,
  `date_of_introduction` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `service_location` varchar(30) DEFAULT NULL,
  `_product_master_fkid` bigint(10) unsigned NOT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`),
  KEY `product_master_fkid_idx` (`_product_master_fkid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer`
--

LOCK TABLES `customer` WRITE;
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
INSERT INTO `customer` VALUES (1,1,'Samsung','1','Aquem Pajifond','margao','goa',456789,'india',77,679,'hvhv',5,79,5,78,'1','2017-02-27 08:00:00','margao',1);
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_contact_person`
--

DROP TABLE IF EXISTS `customer_contact_person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_contact_person` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `designation` varchar(30) DEFAULT NULL,
  `phone` int(11) DEFAULT NULL,
  `cell_no` int(30) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `_customer_fkid` bigint(10) unsigned NOT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`),
  KEY `customer_fkid_idx` (`_customer_fkid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_contact_person`
--

LOCK TABLES `customer_contact_person` WRITE;
/*!40000 ALTER TABLE `customer_contact_person` DISABLE KEYS */;
INSERT INTO `customer_contact_person` VALUES (1,'Anuj','Manager',2343456,1212123234,'anuj@gmail.com',1);
/*!40000 ALTER TABLE `customer_contact_person` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_product_assoc`
--

DROP TABLE IF EXISTS `customer_product_assoc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_product_assoc` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `_product_master_fkid` bigint(10) unsigned DEFAULT NULL,
  `_customer_fkid` bigint(10) unsigned DEFAULT NULL,
  `last_updated` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_product_assoc`
--

LOCK TABLES `customer_product_assoc` WRITE;
/*!40000 ALTER TABLE `customer_product_assoc` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer_product_assoc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_support`
--

DROP TABLE IF EXISTS `customer_support`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_support` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `ticket_no` int(11) NOT NULL,
  `ticket_date` date DEFAULT NULL,
  `_sales_order_fkid` bigint(10) unsigned NOT NULL,
  `invoice_no` int(11) DEFAULT NULL,
  `party_name` varchar(20) DEFAULT NULL,
  `party_address` varchar(45) DEFAULT NULL,
  `product_name` varchar(30) DEFAULT NULL,
  `details` varchar(45) DEFAULT NULL,
  `observations` varchar(45) DEFAULT NULL,
  `instructions` varchar(45) DEFAULT NULL,
  `action_taken` varchar(45) DEFAULT NULL,
  `customer_accepted` varchar(20) DEFAULT NULL,
  `date_visited` date DEFAULT NULL,
  `other_remarks` varchar(45) DEFAULT NULL,
  `your_remarks` varchar(45) DEFAULT NULL,
  `initiated_by` varchar(20) DEFAULT NULL,
  `verified_by` varchar(20) DEFAULT NULL,
  `checked_by` varchar(20) DEFAULT NULL,
  `authorized_by` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_ticket_no_UNIQUE` (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_support`
--

LOCK TABLES `customer_support` WRITE;
/*!40000 ALTER TABLE `customer_support` DISABLE KEYS */;
INSERT INTO `customer_support` VALUES (1,1,'2017-02-22',1,1,'annie','margao','Tank','Hole','Leakage','Fix','Option 1','CustAccepted','2017-02-27','Fixed','Ravi','No Leakage','Anil','Pravin','Harish');
/*!40000 ALTER TABLE `customer_support` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `grn_no_used`
--

DROP TABLE IF EXISTS `grn_no_used`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grn_no_used` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `_orgid` bigint(10) unsigned NOT NULL,
  `grn_no` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `po-so_items_with_grn_fkid` bigint(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `grn_no_used`
--

LOCK TABLES `grn_no_used` WRITE;
/*!40000 ALTER TABLE `grn_no_used` DISABLE KEYS */;
INSERT INTO `grn_no_used` VALUES (1,1,1,'2017-02-14 08:00:00',1);
/*!40000 ALTER TABLE `grn_no_used` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `group_master`
--

DROP TABLE IF EXISTS `group_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `group_master` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `grp` varchar(20) DEFAULT NULL,
  `sub_group` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `group_master`
--

LOCK TABLES `group_master` WRITE;
/*!40000 ALTER TABLE `group_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `group_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr_empcontract`
--

DROP TABLE IF EXISTS `hr_empcontract`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr_empcontract` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `_employee_fkid` bigint(10) unsigned NOT NULL,
  `employee_status` tinyint(5) DEFAULT NULL,
  `job_title` varchar(20) DEFAULT NULL,
  `contract_type` varchar(20) DEFAULT NULL,
  `contract_date` date DEFAULT NULL,
  `contract_renew_date` date DEFAULT NULL,
  `salary` int(30) DEFAULT NULL,
  `_orgid` bigint(10) DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr_empcontract`
--

LOCK TABLES `hr_empcontract` WRITE;
/*!40000 ALTER TABLE `hr_empcontract` DISABLE KEYS */;
INSERT INTO `hr_empcontract` VALUES (1,1,1,'Assistant trainee','1','2017-02-26','2017-02-28',NULL,1);
/*!40000 ALTER TABLE `hr_empcontract` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr_empleaves`
--

DROP TABLE IF EXISTS `hr_empleaves`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr_empleaves` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `_employee_fkid` bigint(10) unsigned NOT NULL,
  `monthly_leaves` int(11) DEFAULT NULL,
  `casual_leaves` int(11) DEFAULT NULL,
  `earn_leaves` int(11) DEFAULT NULL,
  `restricted_holidays` int(11) DEFAULT NULL,
  `_orgid` bigint(10) DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr_empleaves`
--

LOCK TABLES `hr_empleaves` WRITE;
/*!40000 ALTER TABLE `hr_empleaves` DISABLE KEYS */;
INSERT INTO `hr_empleaves` VALUES (1,1,2,5,15,10,1);
/*!40000 ALTER TABLE `hr_empleaves` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr_employeedetails`
--

DROP TABLE IF EXISTS `hr_employeedetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr_employeedetails` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `address` varchar(45) DEFAULT NULL,
  `email_id` varchar(45) DEFAULT NULL,
  `contact_no` int(40) DEFAULT NULL,
  `emergency_contact` int(40) DEFAULT NULL,
  `econtact_name` varchar(20) DEFAULT NULL,
  `relationship` varchar(20) DEFAULT NULL,
  `id_proof` int(11) DEFAULT NULL,
  `blood_group` varchar(20) DEFAULT NULL,
  `qualification` varchar(20) DEFAULT NULL,
  `department` varchar(20) DEFAULT NULL,
  `designation` varchar(20) DEFAULT NULL,
  `job_title` varchar(45) DEFAULT NULL,
  `joining_date` date DEFAULT NULL,
  `_orgid` bigint(10) DEFAULT NULL,
  `employee_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr_employeedetails`
--

LOCK TABLES `hr_employeedetails` WRITE;
/*!40000 ALTER TABLE `hr_employeedetails` DISABLE KEYS */;
INSERT INTO `hr_employeedetails` VALUES (1,'Anil','2017-02-22','Vasco','anil@gmail.com',4423432,334343,'Anuj','Brother',234324,'A+','B.E','Quality','Trainee','Assistant trainee','2017-02-10',1,NULL);
/*!40000 ALTER TABLE `hr_employeedetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `industry_type`
--

DROP TABLE IF EXISTS `industry_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `industry_type` (
  `_id` bigint(10) unsigned NOT NULL,
  `industry_type` tinytext,
  `industry_group` tinytext,
  `industry_class` tinytext,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `industry_type`
--

LOCK TABLES `industry_type` WRITE;
/*!40000 ALTER TABLE `industry_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `industry_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_master`
--

DROP TABLE IF EXISTS `item_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_master` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `group_master_fkid` bigint(10) unsigned NOT NULL,
  `grp` varchar(20) DEFAULT NULL,
  `sub_group` varchar(20) DEFAULT NULL,
  `grade` varchar(20) DEFAULT NULL,
  `unit_measure` varchar(20) DEFAULT NULL,
  `status` tinyint(5) DEFAULT NULL,
  `last_updated` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_master`
--

LOCK TABLES `item_master` WRITE;
/*!40000 ALTER TABLE `item_master` DISABLE KEYS */;
INSERT INTO `item_master` VALUES (4,'gcvjg',3421,'Option 1','Option 1','Option 1','Option 1',NULL,'2017-02-10 18:50:50'),(5,'test',3421,'Option 1','Option 1','Option 1','Option 1',NULL,'2017-02-14 14:11:07'),(6,'test',3421,'Option 1','Option 1','Option 1','Option 1',NULL,'2017-02-14 14:12:46'),(7,'test',3421,'Option 1','Option 1','Option 1','Option 1',NULL,'2017-02-14 14:13:08'),(8,'test',3421,'Option 2','Option 2','Option 1','Option 1',NULL,'2017-02-15 07:08:19'),(9,'test',3421,'Option 1','Option 1','Option 1','Option 1',NULL,'2017-02-16 09:37:56'),(10,'trstttttt',3421,'Option 1','Option 1','Option 1','Option 1',NULL,'2017-02-16 09:40:24');
/*!40000 ALTER TABLE `item_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_org_assoc`
--

DROP TABLE IF EXISTS `item_org_assoc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_org_assoc` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_unique_code` varchar(30) NOT NULL,
  `name` tinytext,
  `grp` varchar(45) DEFAULT NULL,
  `sub_group` varchar(45) DEFAULT NULL,
  `grade` varchar(45) DEFAULT NULL,
  `unit_measure` varchar(45) DEFAULT NULL,
  `item_master_fkid` bigint(10) unsigned DEFAULT NULL,
  `invoice_narration` varchar(30) DEFAULT NULL,
  `min_stock_trigger` tinyint(5) DEFAULT NULL,
  `min_order_quantity` int(11) DEFAULT NULL,
  `bin_location` varchar(100) DEFAULT NULL,
  `lead_time` int(11) DEFAULT NULL,
  `status` tinyint(5) DEFAULT NULL,
  `geo_location` varchar(20) DEFAULT NULL,
  `_orgid` bigint(10) unsigned NOT NULL,
  `last_updated` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `raw_material_or_product` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`),
  UNIQUE KEY `item_unique_code_UNIQUE` (`item_unique_code`)
) ENGINE=InnoDB AUTO_INCREMENT=906 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_org_assoc`
--

LOCK TABLES `item_org_assoc` WRITE;
/*!40000 ALTER TABLE `item_org_assoc` DISABLE KEYS */;
INSERT INTO `item_org_assoc` VALUES (903,'1234','Screw 2 inch','I','A','1','inch',4,'2inch Screw',3,12,'2',1,1,'Verna',1,'2017-02-16 13:42:07',''),(904,'GM2903','PVC Black Granules ','I','A','4','kilogram',NULL,'PVC Black Granules ',10,50,'2',3,1,'Verna',1,'2017-02-16 14:18:02',''),(905,'GMZ12OP','Red oxide powder','II','B','5','kilogram',NULL,'Red Oxide Powder',10,20,'2',1,1,'Verna',1,'2017-02-16 14:56:44','');
/*!40000 ALTER TABLE `item_org_assoc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_rates`
--

DROP TABLE IF EXISTS `item_rates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_rates` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `_orgid` bigint(10) unsigned NOT NULL,
  `item_org_assoc_fkid` bigint(10) unsigned NOT NULL,
  `supplier_org_assoc_fkid` bigint(10) unsigned NOT NULL,
  `rate` int(11) NOT NULL,
  `currency` varchar(20) DEFAULT NULL,
  `start_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `latest` tinyint(5) DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_rates`
--

LOCK TABLES `item_rates` WRITE;
/*!40000 ALTER TABLE `item_rates` DISABLE KEYS */;
INSERT INTO `item_rates` VALUES (2,24,1,5,300,'','2017-02-10 18:51:17','2017-02-10 18:51:17',1);
/*!40000 ALTER TABLE `item_rates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_stock`
--

DROP TABLE IF EXISTS `item_stock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_stock` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `_orgid` bigint(10) unsigned NOT NULL,
  `item_org_assoc_fkid` bigint(10) unsigned NOT NULL,
  `expires_by` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `quantity` int(11) NOT NULL,
  `start_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `rate` int(11) NOT NULL,
  `currency` varchar(20) NOT NULL,
  `geo_location` varchar(20) NOT NULL,
  `last_updated` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `latest` varchar(20) DEFAULT NULL,
  `po_fkid` int(11) DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`),
  KEY `item_org_assoc_fkid_idx` (`item_org_assoc_fkid`),
  KEY `po_fkid` (`po_fkid`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_stock`
--

LOCK TABLES `item_stock` WRITE;
/*!40000 ALTER TABLE `item_stock` DISABLE KEYS */;
INSERT INTO `item_stock` VALUES (12,24,1,'2017-02-10 18:51:17',3,'2017-02-10 18:51:17','2017-02-10 18:51:17',300,'','','2017-02-10 18:51:17','1',NULL);
/*!40000 ALTER TABLE `item_stock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_stock_adjustment`
--

DROP TABLE IF EXISTS `item_stock_adjustment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_stock_adjustment` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `_orgid` bigint(10) unsigned NOT NULL,
  `item_stock_fkid` bigint(10) unsigned NOT NULL,
  `updated_by` varchar(45) NOT NULL,
  `reason` varchar(45) NOT NULL,
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`),
  KEY `item_stock_fkid_idx` (`item_stock_fkid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_stock_adjustment`
--

LOCK TABLES `item_stock_adjustment` WRITE;
/*!40000 ALTER TABLE `item_stock_adjustment` DISABLE KEYS */;
/*!40000 ALTER TABLE `item_stock_adjustment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_total_inventory`
--

DROP TABLE IF EXISTS `item_total_inventory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_total_inventory` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `_orgid` bigint(10) unsigned NOT NULL,
  `item_org_assoc_fkid` bigint(10) unsigned NOT NULL,
  `total_quantity` int(11) DEFAULT NULL,
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `geo_location` varchar(20) DEFAULT NULL,
  `status` tinyint(6) DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`),
  KEY `item_org_assoc_fkid2_idx` (`item_org_assoc_fkid`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_total_inventory`
--

LOCK TABLES `item_total_inventory` WRITE;
/*!40000 ALTER TABLE `item_total_inventory` DISABLE KEYS */;
INSERT INTO `item_total_inventory` VALUES (34,24,1,NULL,'2017-02-10 18:51:17','',NULL);
/*!40000 ALTER TABLE `item_total_inventory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `machine_master`
--

DROP TABLE IF EXISTS `machine_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `machine_master` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `grp` varchar(20) DEFAULT NULL,
  `sub_group` varchar(20) DEFAULT NULL,
  `industrial_name` varchar(30) DEFAULT NULL,
  `unit_of_measure` varchar(30) DEFAULT NULL,
  `capacity_measure` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `machine_master`
--

LOCK TABLES `machine_master` WRITE;
/*!40000 ALTER TABLE `machine_master` DISABLE KEYS */;
INSERT INTO `machine_master` VALUES (1,'Option 1','Option 1','aa','Option 1','4');
/*!40000 ALTER TABLE `machine_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `operation_master`
--

DROP TABLE IF EXISTS `operation_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `operation_master` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `grp` varchar(20) NOT NULL,
  `sub_group` varchar(20) DEFAULT NULL,
  `industrial_name` varchar(30) NOT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `operation_master`
--

LOCK TABLES `operation_master` WRITE;
/*!40000 ALTER TABLE `operation_master` DISABLE KEYS */;
INSERT INTO `operation_master` VALUES (1,'Option 1','Option 1','aa');
/*!40000 ALTER TABLE `operation_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `org_supplier_item_assoc`
--

DROP TABLE IF EXISTS `org_supplier_item_assoc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `org_supplier_item_assoc` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `_orgid` bigint(10) unsigned NOT NULL,
  `supplier_org_assoc_fkid` bigint(10) unsigned NOT NULL,
  `item_org_assoc_fkid` bigint(10) unsigned NOT NULL,
  `status` tinyint(5) NOT NULL,
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`),
  KEY `supplier_org_assoc_fkid_idx` (`supplier_org_assoc_fkid`),
  KEY `item_org_assoc_fkid5_idx` (`item_org_assoc_fkid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `org_supplier_item_assoc`
--

LOCK TABLES `org_supplier_item_assoc` WRITE;
/*!40000 ALTER TABLE `org_supplier_item_assoc` DISABLE KEYS */;
/*!40000 ALTER TABLE `org_supplier_item_assoc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `organisation`
--

DROP TABLE IF EXISTS `organisation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `organisation` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `orgid` bigint(10) unsigned DEFAULT NULL,
  `org_name` varchar(250) DEFAULT NULL,
  `org_username` varchar(150) DEFAULT NULL,
  `org_mobile` varchar(40) DEFAULT NULL,
  `org_legal_name` varchar(250) DEFAULT NULL,
  `industry_type_fkid` bigint(10) unsigned DEFAULT NULL,
  `org_email` varchar(45) DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `verification_code` varchar(45) DEFAULT NULL,
  `verification_status` tinyint(6) DEFAULT NULL,
  `status` tinyint(6) DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `organisation`
--

LOCK TABLES `organisation` WRITE;
/*!40000 ALTER TABLE `organisation` DISABLE KEYS */;
INSERT INTO `organisation` VALUES (8,5383969460939496074,'CIBA','ciba123','2322233333',NULL,NULL,'efefef@sdsf.com','2017-02-15 13:46:27','vII8vX5h5g9Q7f9B8dEh_xcuc0ma3daEBZON0kGjRAM=',1,0);
/*!40000 ALTER TABLE `organisation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `organisation_factories`
--

DROP TABLE IF EXISTS `organisation_factories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `organisation_factories` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `_orgid` bigint(10) unsigned DEFAULT NULL,
  `factory_location` varchar(45) DEFAULT NULL,
  `factory_type` tinyint(6) DEFAULT NULL,
  `added_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` bigint(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `organisation_factories`
--

LOCK TABLES `organisation_factories` WRITE;
/*!40000 ALTER TABLE `organisation_factories` DISABLE KEYS */;
/*!40000 ALTER TABLE `organisation_factories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `po-so_items_with_grn`
--

DROP TABLE IF EXISTS `po-so_items_with_grn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `po-so_items_with_grn` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `_orgid` bigint(10) unsigned NOT NULL,
  `purchase_service_order_fkid` bigint(10) unsigned NOT NULL,
  `item_org_assoc_fkid` bigint(10) unsigned NOT NULL,
  `so_action` varchar(45) NOT NULL,
  `rate` int(11) DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `total_cost` int(11) NOT NULL,
  `grn_no` int(11) NOT NULL,
  `grn_status` tinyint(5) NOT NULL,
  `received_sent_quantity` int(11) NOT NULL,
  `received_sent_by` varchar(45) DEFAULT NULL,
  `rejected_quantity` int(11) NOT NULL,
  `rejected_note` varchar(45) NOT NULL,
  `rejected_by` varchar(45) NOT NULL,
  `status` tinyint(5) NOT NULL,
  `delivered_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `po-so_items_with_grn`
--

LOCK TABLES `po-so_items_with_grn` WRITE;
/*!40000 ALTER TABLE `po-so_items_with_grn` DISABLE KEYS */;
INSERT INTO `po-so_items_with_grn` VALUES (1,1,1,1,'sas',10,30,400,1,1,20,NULL,10,'fgh','gh',1,'2017-02-22 08:00:00');
/*!40000 ALTER TABLE `po-so_items_with_grn` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `po-so_status`
--

DROP TABLE IF EXISTS `po-so_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `po-so_status` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `_orgid` bigint(10) unsigned NOT NULL,
  `purchase_service_order_fkid` bigint(10) unsigned NOT NULL,
  `actedon_by_fkid` bigint(10) unsigned NOT NULL,
  `date_actedon` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
  `status` tinyint(5) NOT NULL,
  `notes` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `po-so_status`
--

LOCK TABLES `po-so_status` WRITE;
/*!40000 ALTER TABLE `po-so_status` DISABLE KEYS */;
INSERT INTO `po-so_status` VALUES (1,1,1,1,'2017-03-12 08:00:00.000000',1,'fdgdfgdfgfg');
/*!40000 ALTER TABLE `po-so_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `po-so_supplier_invoice`
--

DROP TABLE IF EXISTS `po-so_supplier_invoice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `po-so_supplier_invoice` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `_orgid` bigint(10) unsigned DEFAULT NULL,
  `po-so_items_with_grn_list` varchar(45) DEFAULT NULL,
  `grn_no` int(11) DEFAULT NULL,
  `invoice_no` int(11) DEFAULT NULL,
  `invoice_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `dc_no` int(11) DEFAULT NULL,
  `dc_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `po-so_supplier_invoice`
--

LOCK TABLES `po-so_supplier_invoice` WRITE;
/*!40000 ALTER TABLE `po-so_supplier_invoice` DISABLE KEYS */;
INSERT INTO `po-so_supplier_invoice` VALUES (1,1,'gfdgfd',1,1,'2017-02-12 08:00:00',1,'2017-02-15 08:00:00');
/*!40000 ALTER TABLE `po-so_supplier_invoice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_bom`
--

DROP TABLE IF EXISTS `product_bom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_bom` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_master_fkid` bigint(10) unsigned NOT NULL,
  `product_name` varchar(20) NOT NULL,
  `item_master_fkid` bigint(10) unsigned NOT NULL,
  `quantity` int(11) NOT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_bom`
--

LOCK TABLES `product_bom` WRITE;
/*!40000 ALTER TABLE `product_bom` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_bom` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_master`
--

DROP TABLE IF EXISTS `product_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_master` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_code` varchar(20) DEFAULT NULL,
  `product_name` varchar(20) DEFAULT NULL,
  `industry_type_fkid` bigint(10) unsigned NOT NULL,
  `grp` varchar(20) NOT NULL,
  `sub_group` varchar(20) NOT NULL,
  `industrial_name` varchar(45) NOT NULL,
  `unit_of_measure` varchar(20) NOT NULL,
  `capacity_measure` int(11) NOT NULL,
  `is_finished_product` tinyint(10) NOT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_master`
--

LOCK TABLES `product_master` WRITE;
/*!40000 ALTER TABLE `product_master` DISABLE KEYS */;
INSERT INTO `product_master` VALUES (1,'1212','Tank lid',1,'Option 1','Option 1','aa','Option 1',4,1);
/*!40000 ALTER TABLE `product_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_org_assoc`
--

DROP TABLE IF EXISTS `product_org_assoc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_org_assoc` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_org_code` int(11) NOT NULL,
  `product_org_name` varchar(20) NOT NULL,
  `_orgid` bigint(10) unsigned NOT NULL,
  `product_master_fkid` bigint(10) unsigned NOT NULL,
  `invoice_naration` varchar(45) NOT NULL,
  `rate_per_UOM` int(11) NOT NULL,
  `mininum_stock_keep` tinyint(5) NOT NULL,
  `lead_time` int(11) NOT NULL,
  `status` tinyint(5) NOT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_org_assoc`
--

LOCK TABLES `product_org_assoc` WRITE;
/*!40000 ALTER TABLE `product_org_assoc` DISABLE KEYS */;
INSERT INTO `product_org_assoc` VALUES (1,1212,'Tank lid',1,1,'121',9,5,1,1);
/*!40000 ALTER TABLE `product_org_assoc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_process`
--

DROP TABLE IF EXISTS `product_process`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_process` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `process_name` varchar(20) NOT NULL,
  `product_master_fkid` bigint(10) unsigned NOT NULL,
  `operation_fkid` bigint(10) unsigned NOT NULL,
  `machine_fkid` bigint(10) unsigned NOT NULL,
  `total_start_time` time(6) NOT NULL,
  `total_stop_time` time(6) NOT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_process`
--

LOCK TABLES `product_process` WRITE;
/*!40000 ALTER TABLE `product_process` DISABLE KEYS */;
INSERT INTO `product_process` VALUES (1,'Option 1',1,1,1,'17:09:06.000000','17:09:06.000000');
/*!40000 ALTER TABLE `product_process` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_process_param`
--

DROP TABLE IF EXISTS `product_process_param`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_process_param` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_process_fkid` bigint(10) unsigned NOT NULL,
  `start_time` time(6) DEFAULT NULL,
  `stop_time` time(6) DEFAULT NULL,
  `operation_param_type` varchar(20) DEFAULT NULL,
  `operation_param_measure_type` varchar(20) DEFAULT NULL,
  `operation_param_measure` varchar(20) DEFAULT NULL,
  `stage_no` varchar(20) DEFAULT NULL,
  `stage_name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_process_param`
--

LOCK TABLES `product_process_param` WRITE;
/*!40000 ALTER TABLE `product_process_param` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_process_param` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `production_job_card`
--

DROP TABLE IF EXISTS `production_job_card`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `production_job_card` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `jobcard_no` int(11) DEFAULT NULL,
  `jobcard_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `purchase_service_order_fkid` bigint(10) unsigned NOT NULL,
  `order_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `invoice_no` int(11) DEFAULT NULL,
  `invoice_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `customer_name` varchar(20) DEFAULT NULL,
  `serial_no_fkuniqueSrno` bigint(10) unsigned NOT NULL,
  `status` tinyint(5) DEFAULT NULL,
  `QC_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `special_test` varchar(20) DEFAULT NULL,
  `machine_id` int(11) DEFAULT NULL,
  `firing_system` varchar(20) DEFAULT NULL,
  `max_oven_temp` int(11) DEFAULT NULL,
  `removal_temp` int(11) DEFAULT NULL,
  `preloading_check` varchar(20) DEFAULT NULL,
  `operator` varchar(20) DEFAULT NULL,
  `inspected_by` varchar(20) DEFAULT NULL,
  `inspection_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `notes` varchar(45) DEFAULT NULL,
  `_orgid` bigint(10) DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `production_job_card`
--

LOCK TABLES `production_job_card` WRITE;
/*!40000 ALTER TABLE `production_job_card` DISABLE KEYS */;
INSERT INTO `production_job_card` VALUES (1,1,'2017-02-13 08:00:00',1,'2017-02-13 08:00:00',1,'2017-02-28 08:00:00','Anuj',1,1,'2017-02-15 08:00:00','3',1,'Oven',100,100,'Done','Raj','Ram','2017-02-28 08:00:00','Done',NULL);
/*!40000 ALTER TABLE `production_job_card` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `purchase_service_order`
--

DROP TABLE IF EXISTS `purchase_service_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `purchase_service_order` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `unique_serial_no` bigint(10) unsigned DEFAULT NULL,
  `_orgid` bigint(10) unsigned DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `currency` varchar(20) DEFAULT NULL,
  `supplier_fkid` bigint(10) unsigned DEFAULT NULL,
  `delivery_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `geo_location` varchar(20) DEFAULT NULL,
  `instructions` varchar(45) DEFAULT NULL,
  `payment_terms` varchar(45) DEFAULT NULL,
  `billing_address` varchar(50) DEFAULT NULL,
  `discount_percentage` int(11) DEFAULT NULL,
  `excise_duty` int(11) DEFAULT NULL,
  `vat` int(11) DEFAULT NULL,
  `freight_cost` int(11) DEFAULT NULL,
  `octoroi` varchar(20) DEFAULT NULL,
  `item_total` int(11) DEFAULT NULL,
  `other_charges` tinytext,
  `other_total` int(11) DEFAULT NULL,
  `final_discount_percentage` int(11) DEFAULT NULL,
  `final_total` int(11) DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`),
  UNIQUE KEY `unique_serial_no_UNIQUE` (`unique_serial_no`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `purchase_service_order`
--

LOCK TABLES `purchase_service_order` WRITE;
/*!40000 ALTER TABLE `purchase_service_order` DISABLE KEYS */;
INSERT INTO `purchase_service_order` VALUES (1,1,1,'dddd','2017-02-14 20:37:49','Option 1',1,'2017-02-14 20:37:49','Option 1','bnv','cash','margao',3,4,3,30,'cash',10,NULL,NULL,5,15);
/*!40000 ALTER TABLE `purchase_service_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qa_inspection_item`
--

DROP TABLE IF EXISTS `qa_inspection_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qa_inspection_item` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `report_no` int(11) NOT NULL,
  `product` varchar(20) NOT NULL,
  `product_sr_no` int(11) NOT NULL,
  `operations_performed` varchar(45) DEFAULT NULL,
  `remark` varchar(45) DEFAULT NULL,
  `_orgid` bigint(10) DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qa_inspection_item`
--

LOCK TABLES `qa_inspection_item` WRITE;
/*!40000 ALTER TABLE `qa_inspection_item` DISABLE KEYS */;
INSERT INTO `qa_inspection_item` VALUES (1,1,'Tank',1,'Leakage fixed','Ready ',NULL);
/*!40000 ALTER TABLE `qa_inspection_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qa_inspection_predelivery`
--

DROP TABLE IF EXISTS `qa_inspection_predelivery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qa_inspection_predelivery` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `report_no` int(11) NOT NULL,
  `party_name` varchar(20) DEFAULT NULL,
  `invoice_no` int(11) DEFAULT NULL,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `product` varchar(20) DEFAULT NULL,
  `product_sr_no` int(11) NOT NULL,
  `material` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `observation` varchar(45) DEFAULT NULL,
  `checking` varchar(45) DEFAULT NULL,
  `remark` varchar(45) DEFAULT NULL,
  `_orgid` bigint(10) DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qa_inspection_predelivery`
--

LOCK TABLES `qa_inspection_predelivery` WRITE;
/*!40000 ALTER TABLE `qa_inspection_predelivery` DISABLE KEYS */;
INSERT INTO `qa_inspection_predelivery` VALUES (1,1,'Ahuja',1,'2017-02-22 08:00:00','Tank',1,'Plastic','Hole','Leakage','Option 1','Fixed',NULL);
/*!40000 ALTER TABLE `qa_inspection_predelivery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `registration`
--

DROP TABLE IF EXISTS `registration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `registration` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `org_name` varchar(45) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  `email` varchar(45) DEFAULT NULL,
  `contact_no` int(11) DEFAULT NULL,
  `subsciption_type` varchar(20) DEFAULT NULL,
  `no_of_users` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `registration`
--

LOCK TABLES `registration` WRITE;
/*!40000 ALTER TABLE `registration` DISABLE KEYS */;
/*!40000 ALTER TABLE `registration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles_master`
--

DROP TABLE IF EXISTS `roles_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles_master` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `module` tinytext,
  `role_type` tinyint(6) DEFAULT NULL,
  `page` tinytext,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles_master`
--

LOCK TABLES `roles_master` WRITE;
/*!40000 ALTER TABLE `roles_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `roles_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_order`
--

DROP TABLE IF EXISTS `sales_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales_order` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_no` int(11) DEFAULT NULL,
  `order_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `customer_name` varchar(25) DEFAULT NULL,
  `job_type` varchar(20) DEFAULT NULL,
  `delivery_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `excise_charges` int(11) DEFAULT NULL,
  `transport_charges` int(11) DEFAULT NULL,
  `form30` varchar(25) DEFAULT NULL,
  `formc` varchar(25) DEFAULT NULL,
  `dispatch_type` varchar(25) DEFAULT NULL,
  `payment_terms` varchar(25) DEFAULT NULL,
  `repeat_order` varchar(25) DEFAULT NULL,
  `remarks` varchar(45) DEFAULT NULL,
  `_orgid` bigint(10) DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales_order`
--

LOCK TABLES `sales_order` WRITE;
/*!40000 ALTER TABLE `sales_order` DISABLE KEYS */;
INSERT INTO `sales_order` VALUES (1,1,'2017-02-12 08:00:00','Skakshi','Service','2017-02-22 08:00:00',232,555,'Form30','FormC','Option 1','aasasa','Weekly','vsvdsgdsg',1);
/*!40000 ALTER TABLE `sales_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_order_items`
--

DROP TABLE IF EXISTS `sales_order_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales_order_items` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `_sales_order_fkid` bigint(10) unsigned DEFAULT NULL,
  `item_name` varchar(25) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `uom` varchar(25) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `discount` int(11) DEFAULT NULL,
  `delivery_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales_order_items`
--

LOCK TABLES `sales_order_items` WRITE;
/*!40000 ALTER TABLE `sales_order_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `sales_order_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `so_excise`
--

DROP TABLE IF EXISTS `so_excise`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `so_excise` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `_orgid` bigint(10) unsigned NOT NULL,
  `po-so_items_with_grn_list` varchar(45) NOT NULL,
  `grn_no` int(11) NOT NULL,
  `invoice_no` int(11) NOT NULL,
  `invoice_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `dc_no` int(11) NOT NULL,
  `dc_date` timestamp(6) NOT NULL DEFAULT '0000-00-00 00:00:00.000000',
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `so_excise`
--

LOCK TABLES `so_excise` WRITE;
/*!40000 ALTER TABLE `so_excise` DISABLE KEYS */;
/*!40000 ALTER TABLE `so_excise` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `supplier_address`
--

DROP TABLE IF EXISTS `supplier_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `supplier_address` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `supplier_org_assoc_fkid` bigint(10) unsigned NOT NULL,
  `_orgid` bigint(10) unsigned NOT NULL,
  `address_line1` varchar(250) NOT NULL,
  `city` varchar(60) NOT NULL,
  `state` varchar(60) NOT NULL,
  `country` varchar(60) NOT NULL,
  `pincode` varchar(20) NOT NULL,
  `type_address` tinyint(5) NOT NULL COMMENT '1 - HQ\n2 - Warehouse\n3 - Office',
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(5) NOT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`),
  KEY `supplier_master_fkid_idx` (`supplier_org_assoc_fkid`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `supplier_address`
--

LOCK TABLES `supplier_address` WRITE;
/*!40000 ALTER TABLE `supplier_address` DISABLE KEYS */;
INSERT INTO `supplier_address` VALUES (5,10,1,'AZHAD MAHIDHAN, NEAR POLICE STATION','Panjim','Goa','India','403601',1,'2017-02-16 16:45:26',1);
/*!40000 ALTER TABLE `supplier_address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `supplier_contact_person`
--

DROP TABLE IF EXISTS `supplier_contact_person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `supplier_contact_person` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `supplier_org_assoc_fkid` bigint(10) unsigned NOT NULL,
  `_orgid` bigint(10) unsigned NOT NULL,
  `contact_name` varchar(20) NOT NULL,
  `designation` varchar(20) NOT NULL,
  `email` varchar(45) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `mobile` varchar(45) DEFAULT NULL,
  `supplier_address_fkid` bigint(10) unsigned NOT NULL,
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`),
  KEY `supplier_master_fkid1_idx` (`supplier_org_assoc_fkid`),
  KEY `supplier_address_fkid_idx` (`supplier_address_fkid`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `supplier_contact_person`
--

LOCK TABLES `supplier_contact_person` WRITE;
/*!40000 ALTER TABLE `supplier_contact_person` DISABLE KEYS */;
INSERT INTO `supplier_contact_person` VALUES (5,10,1,'Caslino Pereira','CTO','caslino@dccper.com','9923819097','9923819097',5,'2017-02-16 16:45:26');
/*!40000 ALTER TABLE `supplier_contact_person` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `supplier_master`
--

DROP TABLE IF EXISTS `supplier_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `supplier_master` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `category` varchar(20) NOT NULL,
  `company_name` varchar(20) NOT NULL,
  `hq_address_line1` varchar(45) NOT NULL,
  `city` varchar(20) NOT NULL,
  `state` varchar(20) NOT NULL,
  `country` varchar(20) NOT NULL,
  `pincode` int(11) NOT NULL,
  `status` tinyint(5) NOT NULL,
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `lst` varchar(20) NOT NULL,
  `cst` varchar(20) NOT NULL,
  `pan` varchar(20) NOT NULL,
  `ecc` varchar(20) NOT NULL,
  `gst` varchar(20) DEFAULT NULL,
  `notes` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `supplier_master`
--

LOCK TABLES `supplier_master` WRITE;
/*!40000 ALTER TABLE `supplier_master` DISABLE KEYS */;
INSERT INTO `supplier_master` VALUES (1,'123','Anant','Altinho','Panaji','Goa','India',3324345,1,'2017-02-09 19:45:14','323','2323','232','232','23213','dfdffsd'),(2,'1234','DCCPER','Panjim','Panjim','Goa','India',403601,1,'2017-02-16 10:36:07','Pereira','12343','22321','12342','3e3e23',''),(3,'12345','Test123','test2 address','test2 city','test2','India',33333,1,'2017-02-16 11:26:44','2232323','232323','23232323','23232323','34343434','');
/*!40000 ALTER TABLE `supplier_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `supplier_org_assoc`
--

DROP TABLE IF EXISTS `supplier_org_assoc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `supplier_org_assoc` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `supplier_unique_code` varchar(150) NOT NULL,
  `_orgid` bigint(10) unsigned NOT NULL,
  `supplier_master_fkid` bigint(10) unsigned DEFAULT NULL,
  `category` varchar(20) NOT NULL,
  `company_name` varchar(20) NOT NULL,
  `hq_address_line1` varchar(250) NOT NULL,
  `city` varchar(150) NOT NULL,
  `state` varchar(150) NOT NULL,
  `country` varchar(250) NOT NULL,
  `pincode` varchar(250) NOT NULL,
  `lst` varchar(50) NOT NULL,
  `cst` varchar(50) NOT NULL,
  `pan` varchar(50) NOT NULL,
  `ecc` varchar(50) NOT NULL,
  `gst` varchar(50) DEFAULT NULL,
  `notes` varchar(250) DEFAULT NULL,
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(5) NOT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `supplier_org_assoc`
--

LOCK TABLES `supplier_org_assoc` WRITE;
/*!40000 ALTER TABLE `supplier_org_assoc` DISABLE KEYS */;
INSERT INTO `supplier_org_assoc` VALUES (10,'GMZ12',1,NULL,'1','DCCPER','AZHAD MAHIDHAN, NEAR POLICE STATION','Panjim','Goa','India','403601','6768787878','3435454545','78786767676','64546576767','8748948747','Nope','2017-02-16 16:45:26',1);
/*!40000 ALTER TABLE `supplier_org_assoc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_account`
--

DROP TABLE IF EXISTS `user_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_account` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(45) DEFAULT NULL,
  `password` text,
  `_orgid` bigint(10) unsigned DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `status` tinyint(6) DEFAULT NULL,
  `usertype` tinyint(6) DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_account`
--

LOCK TABLES `user_account` WRITE;
/*!40000 ALTER TABLE `user_account` DISABLE KEYS */;
INSERT INTO `user_account` VALUES (6,'ciba123','$2a$10$77Bb8YKNlXtacFyoAQ2vBOgN6nUh0Mpdkhr0awuuVFNH5UPj4VMc2',5383969460939496074,'efefef@sdsf.com',1,1);
/*!40000 ALTER TABLE `user_account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_roles` (
  `_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `_orgid` bigint(10) unsigned DEFAULT NULL,
  `user_account_fkid` bigint(10) unsigned DEFAULT NULL,
  `role_master_fkid` bigint(10) unsigned DEFAULT NULL,
  `assigned_by_fkid` bigint(10) unsigned DEFAULT NULL,
  `assigned_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(6) DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_roles`
--

LOCK TABLES `user_roles` WRITE;
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-02-16 22:39:19
