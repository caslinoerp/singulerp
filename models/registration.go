package models

import (
	"strconv"
	//"fmt"
	"strings"
	"time"

	"github.com/astaxie/beego/orm"
)

type Organisation struct {
	Id                  int64
	Orgid               int64
	Org_name            string
	Org_mobile          string
	Org_legal_name      string
	Industry_type_fkid  int
	Org_email           string
	Verification_code   string
	Verification_status int
	Status              int
	Password            string
	Userid              string
	Username            string
	Orgidd              string
	EntityName          string
	BrandName           string
	Email               string
	Mobile              string
	HQLoc               string
	IndType             []string
	LocationFac         string
	GSTIN               string
	PAN                 string
	BankName            string
	AccountNo           string
	IFSC                string
	//Username 		   string
}

type OrganisationParam struct {
	OrgName          string
	Email            string
	AdminUsername    string
	AdminMobile      string
	Password         string
	CPassword        string
	VerificationCode string
}

type Prefix struct {
	Orgid         string
	Userid        string
	Username      string
	ItemCode      string
	ProductCode   string
	SupplierCode  string
	CustomerCode  string
	PurchaseOrder string
	SalesCode     string
	EmployeeId    string
}

func init() {
	//orm.RegisterModel(new(Organisation))
	//orm.RegisterModel(new(OrganisationParam))
}

func CheckForExistingOrgEmail(email string) int {

	o := orm.NewOrm()
	var maps []orm.Params
	num, _ := o.Raw("SELECT * FROM organisation where org_email= ? ", email).Values(&maps)
	if num > 0 {
		return 1
	}
	return 0
}

func CheckForExistingOrgName(orgName string) int {

	o := orm.NewOrm()
	var maps []orm.Params
	num, _ := o.Raw("SELECT * FROM organisation where org_name= ? ", orgName).Values(&maps)
	if num > 0 {
		return 1
	}
	return 0
}

func RegistrationInsertStructParam(formData Organisation) int {
	o := orm.NewOrm()
	//fmt.Println(formData.Orgid)
	_, err := o.Raw("INSERT INTO organisation (orgid,org_name,org_username,org_mobile,org_email,verification_code,verification_status,status)  VALUES (?,?,?,?,?,?,?,?) ", formData.Orgid, formData.Org_name, formData.Username, formData.Org_mobile, formData.Org_email, formData.Verification_code, formData.Verification_status, formData.Status).Exec()

	if err == nil {

		var maps []orm.Params
		num, _ := o.Raw("SELECT * FROM organisation WHERE org_name = ? and org_mobile = ? and org_email = ? ", formData.Org_name, formData.Org_mobile, formData.Org_email).Values(&maps)
		if num < 1 {
			return 0
		} else {
			_, err2 := o.Raw("INSERT INTO user_account (username,password,_orgid,email,status,usertype)  VALUES (?,?,?,?,?,?) ", formData.Username, formData.Password, maps[0]["orgid"], formData.Org_email, 0, 1).Exec()

			if err2 == nil {
				return 1
			}
		}
		return 0
	}
	return 0
}

func RegistrationValidateToken(token string) int {
	//fmt.Println(token)
	o := orm.NewOrm()
	var maps []orm.Params
	num, _ := o.Raw("SELECT _id, orgid, org_username FROM organisation WHERE verification_code = ? and status = 0 and verification_status = 0 ", token).Values(&maps)
	if num > 0 {

		_, err1 := o.Raw("UPDATE organisation SET verification_status = 1, status = 99  WHERE _id = ? and orgid = ?", maps[0]["_id"], maps[0]["orgid"]).Exec()

		_, err2 := o.Raw("UPDATE user_account SET status = 1 WHERE _orgid = ? and username = ?", maps[0]["orgid"], maps[0]["org_username"]).Exec()

		if err1 == nil && err2 == nil {
			return 1
		}
	}
	return 0
}

func GetConfiguration(formData Organisation) map[string]orm.Params {
	o := orm.NewOrm()
	var maps []orm.Params
	result := make(map[string]orm.Params)
	num, err := o.Raw("SELECT * from organisation where orgid = ?", formData.Orgidd).Values(&maps)
	if err == nil && num > 0 {
		for k, v := range maps {
			key := "config_" + strconv.Itoa(k)
			//value := maps
			//fmt.Println("key: ",key)
			//fmt.Println("values: ", v["name"])
			result[key] = v
		}
	} else {
		result["status"] = nil
	}
	return result
}

func InsertConfiguration(formData Organisation) int {
	//result :=make(map[string]string)
	//result["status"] = "0"
	//user := Users{ EmailAddress: email, Password: password }
	/*var maps []orm.Params
	   	_, err := o.Raw("INSERT INTO sales_order (order_no, order_date, customer_name, job_type, delivery_date, excise_charges, transport_charges, form30, formc, dispatch_type, payment_terms, repeat_order, remarks, _orgid) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?) ", data["OrderNo"], data["OrderDate"],data["CustomerName"],data["JobType"],data["DeliveryDate"],data["ExciseCharges"],data["TransportCharges"],data["Form30"],data["FormC"],data["DispatchType"],data["PaymentTerms"],data["RepeatOrder"],data["Remarks"],"1").Values(&maps)
		//fmt.Println(err, maps[0]["id"])
		//result["user_id"] = maps[0]["id"].(string)
		if err == nil  {
			result["status"] = "1"
			fmt.Println("Sales Order added")
		}
		return result*/
	o := orm.NewOrm()
	ormTransactionalErr := o.Begin()
	var tags1 []string
	tags1 = strings.Split(formData.LocationFac, ",")
	// var tags2 []string
	// tags2 = strings.Split(formData.IndType, ",")
	var maps []orm.Params
	_, err := o.Raw("SELECT * from organisation where orgid=?", formData.Orgidd).Values(&maps)
	if err != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}
	var s string = ""
	if tags1[0] == s {
		//fmt.Println("Error in for loop")
	} else {
		// _, err7 := o.Raw("Update organisation_factories SET status=0 where factory_type=? and _orgid=?", "2", formData.Orgidd).Exec()
		// if err7 != nil {
		// 	//fmt.Println("Error in for loop")
		// 	ormTransactionalErr = o.Rollback()
		// 	return 0
		// }
		for i := 0; i < len(tags1); i++ {
			var fac_maps []orm.Params
			fac_num, fac_err := o.Raw("SELECT factory_location FROM organisation_factories WHERE _orgid=? and status=1 and factory_type=2 and factory_location=?", formData.Orgidd, tags1[i]).Values(&fac_maps)
			if fac_err != nil {
				ormTransactionalErr = o.Rollback()
				return 0
			}
			if fac_num < 1 {
				var old_fac_maps []orm.Params
				old_fac_num, old_fac_err := o.Raw("SELECT factory_location FROM organisation_factories WHERE _orgid=? and status=0 and factory_type=2 and factory_location=?", formData.Orgidd, tags1[i]).Values(&old_fac_maps)
				if old_fac_err != nil {
					ormTransactionalErr = o.Rollback()
					return 0
				}
				if old_fac_num > 0 {
					_, old_update_err := o.Raw("Update organisation_factories SET status=1 where factory_type=? and _orgid=? and factory_location=?", "2", formData.Orgidd, old_fac_maps[0]["factory_location"]).Exec()
					if old_update_err != nil {
						//fmt.Println("Error in for loop")
						ormTransactionalErr = o.Rollback()
						return 0
					}
				} else {
					_, err1 := o.Raw("INSERT INTO organisation_factories (_orgid,factory_location,factory_type,added_on,status) VALUES (?,?,?,?,?) ", formData.Orgidd, tags1[i], "2", time.Now(), "1").Exec()
					if err1 != nil {
						//fmt.Println("Error in for loop")
						ormTransactionalErr = o.Rollback()
						return 0
						//break
					}
				}
			}

		}
	}

	if formData.HQLoc != "" {
		var maps5 []orm.Params
		num, err5 := o.Raw("SELECT factory_location from organisation_factories where _orgid=? and factory_type=? and status=1", formData.Orgidd, "1").Values(&maps5)
		if err5 != nil {
			ormTransactionalErr = o.Rollback()
			return 0
		}
		if num > 0 {
			if maps5[0]["factory_location"] == formData.HQLoc {
				//fmt.Println("Already exists")
			} else {
				_, err6 := o.Raw("Update organisation_factories SET status=0 where factory_type=? and _orgid=?", "1", formData.Orgidd).Exec()
				if err6 != nil {
					//fmt.Println("Error in for loop")
					ormTransactionalErr = o.Rollback()
					return 0
				}
			}
		}
		var maps_old_hq []orm.Params
		old_hq_count, old_hq_err := o.Raw("SELECT _id from organisation_factories where _orgid=? and factory_type=1 and status=0 and factory_location=?", formData.Orgidd, formData.HQLoc).Values(&maps_old_hq)
		if old_hq_err != nil {
			ormTransactionalErr = o.Rollback()
			return 0
		}
		if old_hq_count > 0 {
			_, old_hq_update_err := o.Raw("Update organisation_factories SET status=1 where factory_type=1 and _orgid=? and _id=?", formData.Orgidd, maps_old_hq[0]["_id"]).Exec()
			if old_hq_update_err != nil {
				ormTransactionalErr = o.Rollback()
				return 0
			}
		} else {
			_, err4 := o.Raw("INSERT INTO organisation_factories (_orgid,factory_location,factory_type,added_on,status) VALUES (?,?,?,?,?) ", formData.Orgidd, formData.HQLoc, "1", time.Now(), "1").Exec()
			if err4 != nil {
				//fmt.Println("Error in for loop")
				ormTransactionalErr = o.Rollback()
				return 0
			}
		}
	}

	for i := 0; i < len(formData.IndType); i++ {
		_, err2 := o.Raw("INSERT INTO industry_type (industry_type_master_fkid,_orgid,status) VALUES (?,?,?) ", formData.IndType[i], formData.Orgidd, 1).Exec()
		if err2 != nil {
			//fmt.Println("Error in for loop")
			ormTransactionalErr = o.Rollback()
			return 0
		}
	}
	_, err3 := o.Raw("UPDATE organisation SET org_legal_name=?,org_brand_name=?,org_email=? ,org_name=?,org_username=?,org_mobile=? ,created_on=?,verification_code=?,verification_status=?,status=?,GSTIN=?,pan=?,bank_name=?,account_no=?,ifsc_code=? where orgid=? ", formData.EntityName, formData.BrandName, formData.Email, maps[0]["org_name"], maps[0]["org_username"], formData.Mobile, time.Now(), maps[0]["verification_code"], maps[0]["verification_status"], "1", formData.GSTIN, formData.PAN, formData.BankName, formData.AccountNo, formData.IFSC, formData.Orgidd).Exec()
	if err3 != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}
	//_, err3 := o.Raw("INSERT INTO sales_order_items (_sales_order_fkid,item_name, quantity, uom, price, discount, delivery_date) VALUES (?,?,?,?,?,?,?) ", maps[0]["_id"], formData.ItemName, formData.Quantity, formData.UOM, formData.Price, formData.Discount, formData.Ddate).Exec()
	if ormTransactionalErr != nil {
		return 0
	}
	ormTransactionalErr = o.Commit()
	return 1
}

func GetEmail(formData Organisation) string {
	var result string
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT org_email FROM organisation WHERE orgid=? ", formData.Orgidd).Values(&maps)
	//fmt.Println("****code:",maps[0]["order_no"])
	if err == nil {
		if num > 0 && maps[0]["org_email"] != nil {
			s := maps[0]["org_email"].(string)
			result = s
		} else {
			s := " "
			result = s
		}
	} else {
		//fmt.Println("Error in code query")
	}

	return result
}

func GetGSTIN(formData Organisation) string {
	var result string
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT GSTIN FROM organisation WHERE orgid=? ", formData.Orgidd).Values(&maps)
	//fmt.Println("****code:",maps[0]["order_no"])
	if err == nil {
		if num > 0 && maps[0]["GSTIN"] != nil {
			s := maps[0]["GSTIN"].(string)
			result = s
		} else {
			s := " "
			result = s
		}
	} else {
		//fmt.Println("Error in code query")
	}

	return result
}

func GetPAN(formData Organisation) string {
	var result string
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT pan FROM organisation WHERE orgid=? ", formData.Orgidd).Values(&maps)
	//fmt.Println("****code:",maps[0]["order_no"])
	if err == nil {
		if num > 0 && maps[0]["pan"] != nil {
			s := maps[0]["pan"].(string)
			result = s
		} else {
			s := " "
			result = s
		}
	} else {
		//fmt.Println("Error in code query")
	}

	return result
}

func GetAccountNo(formData Organisation) string {
	var result string
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT account_no FROM organisation WHERE orgid=? ", formData.Orgidd).Values(&maps)
	//fmt.Println("****code:",maps[0]["order_no"])
	if err == nil {
		if num > 0 && maps[0]["account_no"] != nil {
			s := maps[0]["account_no"].(string)
			result = s
		} else {
			s := " "
			result = s
		}
	} else {
		//fmt.Println("Error in code query")
	}

	return result
}

func GetBankName(formData Organisation) string {
	var result string
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT bank_name FROM organisation WHERE orgid=? ", formData.Orgidd).Values(&maps)
	//fmt.Println("****code:",maps[0]["order_no"])
	if err == nil {
		if num > 0 && maps[0]["bank_name"] != nil {
			s := maps[0]["bank_name"].(string)
			result = s
		} else {
			s := " "
			result = s
		}
	} else {
		//fmt.Println("Error in code query")
	}

	return result
}

func GetIFSC(formData Organisation) string {
	var result string
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT ifsc_code FROM organisation WHERE orgid=? ", formData.Orgidd).Values(&maps)
	//fmt.Println("****code:",maps[0]["order_no"])
	if err == nil {
		if num > 0 && maps[0]["ifsc_code"] != nil {
			s := maps[0]["ifsc_code"].(string)
			result = s
		} else {
			s := " "
			result = s
		}
	} else {
		//fmt.Println("Error in code query")
	}

	return result
}

func GetMobile(formData Organisation) string {
	var result string
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT org_mobile FROM organisation WHERE orgid=? ", formData.Orgidd).Values(&maps)
	//fmt.Println("****code:",maps[0]["order_no"])
	if err == nil {
		if num > 0 && maps[0]["org_mobile"] != nil {
			s := maps[0]["org_mobile"].(string)
			result = s
		} else {
			s := " "
			result = s
		}
	} else {
		//fmt.Println("Error in code query")
	}

	return result
}

func GetBrand(formData Organisation) string {
	var result string
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT org_brand_name FROM organisation WHERE orgid=? ", formData.Orgidd).Values(&maps)
	//fmt.Println("****code:",maps[0]["order_no"])
	if err == nil {
		if num > 0 && maps[0]["org_brand_name"] != nil {
			s := maps[0]["org_brand_name"].(string)
			result = s
		} else {
			s := " "
			result = s
		}

	} else {
		//fmt.Println("Error in code query")
	}

	return result
}

func GetEntity(formData Organisation) string {
	var result string
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT org_legal_name FROM organisation WHERE orgid=? ", formData.Orgidd).Values(&maps)
	//fmt.Println("****code:",maps[0]["order_no"])
	if err == nil {
		if num > 0 && maps[0]["org_legal_name"] != nil {
			s := maps[0]["org_legal_name"].(string)
			result = s
		} else {
			s := " "
			result = s
		}
	} else {
		//fmt.Println("Error in code query")
	}

	return result
}

func GetHQLocation(formData Organisation) string {
	var result string
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT factory_location FROM organisation_factories WHERE _orgid=? and factory_type=? and status=? ", formData.Orgidd, "1", "1").Values(&maps)
	//fmt.Println("****code:",maps[0]["order_no"])
	if err == nil {
		if num > 0 && maps[0]["factory_location"] != nil {
			s := maps[0]["factory_location"].(string)
			result = s
		} else {
			s := "Not set"
			result = s
		}
	} else {
		//fmt.Println("Error in code query")
	}

	return result
}

func GetPreviousHQ(formData Organisation) string {
	var result string
	var s string
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT distinct factory_location FROM organisation_factories WHERE _orgid=? and factory_type=1 and status=0", formData.Orgidd).Values(&maps)
	//fmt.Println("****code:",maps[0]["order_no"])
	if err == nil {
		if num > 0 && maps[0]["factory_location"] != nil {
			for i := 0; i < len(maps); i++ {
				if i == len(maps)-1 {
					s = s + maps[i]["factory_location"].(string)
				} else {
					s = s + maps[i]["factory_location"].(string) + ", "
				}
			}
			//s:= maps[0]["industry_type"].(string)+ "," +  maps[1]["industry_type"].(string)
			result = s
		} else {
			s := " "
			result = s
		}
	} else {
		//fmt.Println("Error in code query")
	}

	return result
}

func GetIndustryType(formData Organisation) string {
	var result string
	var s string
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT industry_type_master_fkid,name FROM industry_type,industry_type_master WHERE _orgid=? and industry_type.industry_type_master_fkid=industry_type_master.pkid and industry_type.status=1", formData.Orgidd).Values(&maps)
	//fmt.Println("****code:",maps[0]["order_no"])
	if err == nil {
		if num > 0 && maps[0]["industry_type_master_fkid"] != nil {
			for i := 0; i < len(maps); i++ {
				s = s + maps[i]["name"].(string) + ","
			}
			//s:= maps[0]["industry_type"].(string)+ "," +  maps[1]["industry_type"].(string)
			result = s
		} else {
			s := " "
			result = s
		}
	} else {
		//fmt.Println("Error in code query")
	}

	return result
}

func GetIndustry(formData Organisation) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT pkid,name FROM industry_type_master WHERE status=1 ").Values(&maps)
	//fmt.Println("****code:",maps[0]["order_no"])
	if err == nil {
		var i int64 = 0
		for ; i < num; i++ {
			//fmt.Println(maps[i])
		}
		for k, v := range maps {

			key := "user_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {

	}

	return result
}

func GetFactoryLocation(formData Organisation) string {
	var result string
	var s string
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT factory_location FROM organisation_factories WHERE _orgid=? and factory_type=2 and status=1", formData.Orgidd).Values(&maps)
	//fmt.Println("****code:",maps[0]["order_no"])
	if err == nil {
		if num > 0 && maps[0]["factory_location"] != nil {
			for i := 0; i < len(maps); i++ {
				s = s + maps[i]["factory_location"].(string) + ","
			}
			//s:= maps[0]["industry_type"].(string)+ "," +  maps[1]["industry_type"].(string)
			result = s
		} else {
			s := " "
			result = s
		}
	} else {
		//fmt.Println("Error in code query")
	}

	return result
}

func GetOldAdditionalFactories(formData Organisation) string {
	var result string
	var s string
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT distinct factory_location FROM organisation_factories WHERE _orgid=? and factory_type=2 and status=0", formData.Orgidd).Values(&maps)
	//fmt.Println("****code:",maps[0]["order_no"])
	if err == nil {
		if num > 0 && maps[0]["factory_location"] != nil {
			for i := 0; i < len(maps); i++ {
				if i == len(maps)-1 {
					s = s + maps[i]["factory_location"].(string)
				} else {
					s = s + maps[i]["factory_location"].(string) + ", "
				}
			}
			//s:= maps[0]["industry_type"].(string)+ "," +  maps[1]["industry_type"].(string)
			result = s
		} else {
			s := " "
			result = s
		}
	} else {
		//fmt.Println("Error in code query")
	}

	return result
}

func AddPrefix(formData Prefix) int {
	//result := make(map[string]string)
	//result["status"] = "0"
	o := orm.NewOrm()
	ormTransactionalErr := o.Begin()
	var maps []orm.Params
	_, err := o.Raw("SELECT _id FROM user_account WHERE username = ? and _orgid = ? ", formData.Username, formData.Orgid).Values(&maps)
	var maps1 []orm.Params
	count, _ := o.Raw("SELECT pkid FROM prefix_company_values WHERE _orgid=? and status=1", formData.Orgid).Values(&maps1)
	if count > 0 {
		_, err0 := o.Raw("Update prefix_company_values set status=0 WHERE _orgid=? and status=1", formData.Orgid).Exec()
		if err0 != nil {
			ormTransactionalErr = o.Rollback()
			return 0
		}
	}

	if err == nil {
		_, err1 := o.Raw("INSERT INTO prefix_company_values (_orgid, prefix_master_fkid, value, added_on, added_by_fkid, status) VALUES (?,?,?,?,?,?) ", formData.Orgid, 1, formData.ItemCode, time.Now(), maps[0]["_id"], 1).Exec()
		if err1 != nil {
			ormTransactionalErr = o.Rollback()
			return 0
		}
		_, err2 := o.Raw("INSERT INTO prefix_company_values (_orgid, prefix_master_fkid, value, added_on, added_by_fkid, status) VALUES (?,?,?,?,?,?) ", formData.Orgid, 2, formData.ProductCode, time.Now(), maps[0]["_id"], 1).Exec()
		if err2 != nil {
			ormTransactionalErr = o.Rollback()
			return 0
		}
		_, err3 := o.Raw("INSERT INTO prefix_company_values (_orgid, prefix_master_fkid, value, added_on, added_by_fkid, status) VALUES (?,?,?,?,?,?) ", formData.Orgid, 3, formData.SupplierCode, time.Now(), maps[0]["_id"], 1).Exec()
		if err3 != nil {
			ormTransactionalErr = o.Rollback()
			return 0
		}
		_, err4 := o.Raw("INSERT INTO prefix_company_values (_orgid, prefix_master_fkid, value, added_on, added_by_fkid, status) VALUES (?,?,?,?,?,?) ", formData.Orgid, 4, formData.CustomerCode, time.Now(), maps[0]["_id"], 1).Exec()
		if err4 != nil {
			ormTransactionalErr = o.Rollback()
			return 0
		}
		_, err5 := o.Raw("INSERT INTO prefix_company_values (_orgid, prefix_master_fkid, value, added_on, added_by_fkid, status) VALUES (?,?,?,?,?,?) ", formData.Orgid, 5, formData.PurchaseOrder, time.Now(), maps[0]["_id"], 1).Exec()
		if err5 != nil {
			ormTransactionalErr = o.Rollback()
			return 0
		}
		_, err6 := o.Raw("INSERT INTO prefix_company_values (_orgid, prefix_master_fkid, value, added_on, added_by_fkid, status) VALUES (?,?,?,?,?,?) ", formData.Orgid, 6, formData.SalesCode, time.Now(), maps[0]["_id"], 1).Exec()
		if err6 != nil {
			ormTransactionalErr = o.Rollback()
			return 0
		}
		_, err7 := o.Raw("INSERT INTO prefix_company_values (_orgid, prefix_master_fkid, value, added_on, added_by_fkid, status) VALUES (?,?,?,?,?,?) ", formData.Orgid, 7, formData.EmployeeId, time.Now(), maps[0]["_id"], 1).Exec()
		if err7 != nil {
			ormTransactionalErr = o.Rollback()
			return 0
		}
		//_, err8 := o.Raw("INSERT INTO prefix_company_values (_orgid, prefix_master_fkid, value, added_on, added_by_fkid, status) VALUES (?,?,?,?,?,?) ", formData.Orgid, 8, formData.ItemCode, time.Now(), maps[0]["_id"],1).Exec()
		//_, err9 := o.Raw("INSERT INTO prefix_company_values (_orgid, prefix_master_fkid, value, added_on, added_by_fkid, status) VALUES (?,?,?,?,?,?) ", formData.Orgid, 9, formData.ItemCode, time.Now(), maps[0]["_id"],1).Exec()
		//_, err10 := o.Raw("INSERT INTO prefix_company_values (_orgid, prefix_master_fkid, value, added_on, added_by_fkid, status) VALUES (?,?,?,?,?,?) ", formData.Orgid, 10, formData.ItemCode, time.Now(), maps[0]["_id"],1).Exec()
		// assign_roles := InsertUserRoles(formData, assigned_for, assigned_by)
		// if assign_roles == 0 {
		// 	result["status"] = "failed-insert"
		// } else {
		// 	result["status"] = "inserted"
		// }

	}
	if ormTransactionalErr != nil {
		return 0
	}
	ormTransactionalErr = o.Commit()
	return 1
}

func GetPrefixCodes(formData Prefix) map[string]string {
	result := make(map[string]string)
	o := orm.NewOrm()
	var maps []orm.Params
	//_, err := o.Raw("SELECT _id FROM item_org_assoc WHERE (item_unique_code=? OR invoice_narration=?) and _orgid=?", formData.ItemCode, formData.ItemName, formData.Orgid).Values(&maps)
	//item_id := maps[0]["_id"]
	num, err := o.Raw("SELECT value, prefix_master_fkid FROM prefix_company_values as pcv WHERE pcv._orgid=? and pcv.status=1", formData.Orgid).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		// var i int64 = 0
		// for ; i < num; i++ {
		// 	fmt.Println(maps[i])
		// }
		var key string
		for k, _ := range maps {
			if maps[k]["prefix_master_fkid"] == "1" {
				key = "item_code"
			} else if maps[k]["prefix_master_fkid"] == "2" {
				key = "product_code"
			} else if maps[k]["prefix_master_fkid"] == "3" {
				key = "supplier_code"
			} else if maps[k]["prefix_master_fkid"] == "4" {
				key = "customer_code"
			} else if maps[k]["prefix_master_fkid"] == "5" {
				key = "purchase_order_code"
			} else if maps[k]["prefix_master_fkid"] == "6" {
				key = "sales_order_code"
			} else if maps[k]["prefix_master_fkid"] == "7" {
				key = "employee_id_code"
			} else if maps[k]["prefix_master_fkid"] == "8" {
				key = "machine_code"
			} else if maps[k]["prefix_master_fkid"] == "9" {
				key = "production_order_code"
			} else if maps[k]["prefix_master_fkid"] == "10" {
				key = "customer_ticket_code"
			}
			//key := "prefix_" + strconv.Itoa(k)
			result[key] = maps[k]["value"].(string)
		}

	} else {
		//fmt.Println(err)
		//result["status"] = nil
	}
	//fmt.Println(result["item_code"])
	return result
}

func AddAllRolesByUserId(user_id string, org_id int64) int {
	o := orm.NewOrm()

	for i := 1; i <= 38; i++ {

		_, err := o.Raw("INSERT INTO user_roles_allocation (_orgid, user_account_fkid, role_master_fkid, assigned_by_fkid, assigned_on, status) VALUES (?,?,?,?,?,?)", org_id, user_id, i, user_id, time.Now(), 1).Exec()
		if err != nil {
			return 0
		}
	}
	return 1
}

func GetUserId(formData Organisation) map[string]orm.Params {

	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT _id FROM user_account WHERE _orgid=? and username=? and password=? and email=? and status=? and usertype=?", formData.Orgid, formData.Username, formData.Password, formData.Org_email, formData.Status, 1).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		var i int64 = 0
		for ; i < num; i++ {
			//fmt.Println(maps[i])
		}
		for k, v := range maps {

			key := "user_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func RemoveFactoryTag(factoryTag string, UserSessionStruct Prefix) string {
	o := orm.NewOrm()
	var maps []orm.Params
	num, err1 := o.Raw("SELECT _id FROM organisation_factories WHERE factory_type=2 and status=1 and factory_location=? and _orgid=? ", factoryTag, UserSessionStruct.Orgid).Values(&maps)
	if err1 != nil {
		return "0"
	}
	if num > 0 { //If present in database else it was typed
		_, err2 := o.Raw("UPDATE organisation_factories set status=0, last_updated=? where factory_type=2 and status=1 and _id=? and _orgid = ?", time.Now(), maps[0]["_id"], UserSessionStruct.Orgid).Exec()
		if err2 != nil {
			return "0"
		}
	} else {
		return "2"
	}
	return "1"
}
