package models

import (
	"time"

	//"fmt"
	//"strings"
	"strconv"

	"github.com/astaxie/beego/orm"
)

type StockTransfer struct {
	//Id                 int64
	Orgid        string
	Userid       string
	Username     string
	ItemName     string
	Quantity     string
	Rate         string
	Discount     string
	OrderNo      string
	OrderDate    string
	CustomerName string
	JobType      string
	DeliveryDate string

	ExciseCharges      string
	TransportCharges   string
	DispatchType       string
	PaymentTerms       string
	RepeatOrder        string
	Remarks            string
	TotalQuantity      string
	TotalDiscount      string
	TotalPrice         string
	TotalAfterDiscount string
	TotalAfterExcise   string
	OctroiCharges      string
	TotalAfterOctroi   string
	TotalAfterTaxes    string
	TotalSalesPrice    string
	TariffNo           string
	Status             string
}

type Sales struct {
	Id                 int64
	OrderId            string
	OrderNo            string
	OrderDate          string
	CustomerName       string
	JobType            string
	DeliveryDate       string
	DLocation          string
	OLocation          string
	Transporter        string
	Vehicle            string
	TaxRegime          string
	ExciseCharges      string
	TransportCharges   string
	Form30             string
	FormC              string
	DispatchType       string
	PaymentTerms       string
	RepeatOrder        string
	Remarks            string
	Orgid              string
	Userid             string
	Username           string
	TotalQuantity      string
	TotalDiscount      string
	TotalPrice         string
	TotalAfterDiscount string
	TotalAfterExcise   string
	TotalAfterTaxes    string
	CST                string
	VAT                string
	TotalSalesPrice    string
	TariffNo           string
	BAddress           string

	TotalTaxableValue     string
	TotalTaxCess          string
	TotalFreightPackaging string

	ItemName            string
	HSN                 string
	UOM                 string
	Quantity            string
	Cost                string
	DiscountPercent     string
	DiscountValue       string
	TaxableAmount       string
	GSTPercent          string
	IGSTRate            string
	IGSTAmount          string
	CGSTRate            string
	CGSTAmount          string
	SGSTRate            string
	SGSTAmount          string
	CessRate            string
	CessAmount          string
	ItemTotalTaxCess    string
	ItemTotalAfterTax   string
	Freight             string
	Insurance           string
	PackagingForwarding string
	Others              string
	TotalOtherCharges   string
	NewGenCode          string
	ManuallyAssignBatch string
}

type DeliveryData struct {
	Orgid             string
	Username          string
	Userid            string
	SoNo              string
	Itemss            string
	Customer          string
	DeliveryDate      string
	CustomerInvoice   string
	CustomerInvoiceDt string
	CustomerDcNo      string
	CustomerDcDt      string
	AddNote           string
	//Status                    string
	DQuantity           string
	RQuantity           string
	DeliveryLocation    string
	TotalCost           string
	DeliveredDate       string
	BatchNo             string
	NewGenCode          string
	DispatchNo          string
	LrNo                string
	LrDt                string
	TransporterFkid     string
	VehicleFkid         string
	CustomerInvoiceFkId int64
}

/*ACTIVE FUNCTIONS*/
/* Active : Create New Sales Order */
func CreateNewSalesOrder(formData Sales, salesitems []map[string]string, Status int) int {

	o := orm.NewOrm()
	ormTransactionalErr := o.Begin()

	var freightpackagingothers string
	freightpackagingothers = "0.00"
	if formData.TotalFreightPackaging != "" {
		tempTotalFreightPackaging := formData.TotalFreightPackaging
		freightpackagingothers = tempTotalFreightPackaging
	}

	var transporter *string
	transporter = nil
	if formData.Transporter != "" {
		temptransporter := formData.Transporter
		transporter = &temptransporter
	}

	var vehicle *string
	vehicle = nil
	if formData.Vehicle != "" {
		tempvehicle := formData.Vehicle
		vehicle = &tempvehicle
	}

	var maps []orm.Params
	//MAKE ENTRY IN SALES ORDER TABLE FOR NEW ORDER
	_, err1 := o.Raw("INSERT INTO sales_order (order_no, order_date, customer_fkid, job_type, delivery_date, origin_location, delivery_location, tax_regime, payment_terms, remarks, _orgid, total_sales_price, status, total_price_after_taxes, tariff_no, billing_address, total_taxable_value, total_tax_cess, total_freight_packaging_others, last_updated_date, sales_order_code_gen, manually_assign_batch, transporter_fkid, vehicle_fkid) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ", formData.OrderNo, formData.OrderDate, formData.CustomerName, formData.JobType, formData.DeliveryDate, formData.OLocation, formData.DLocation, formData.TaxRegime, formData.PaymentTerms, formData.Remarks, formData.Orgid, formData.TotalSalesPrice, Status, formData.TotalAfterTaxes, formData.TariffNo, formData.BAddress, formData.TotalTaxableValue, formData.TotalTaxCess, freightpackagingothers, time.Now(), formData.NewGenCode, formData.ManuallyAssignBatch, &transporter, &vehicle).Exec()
	if err1 != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}

	//SELECT THE ID OF THE INSERTED RECORD AND MAKE ENTRY IN SALES ORDER STATUS
	_, err2 := o.Raw("SELECT _id, origin_location FROM sales_order WHERE order_no=? and _orgid=? and customer_fkid=? and job_type=? and delivery_date=? and total_taxable_value=? and total_tax_cess=? and total_freight_packaging_others=? and total_sales_price=?", formData.OrderNo, formData.Orgid, formData.CustomerName, formData.JobType, formData.DeliveryDate, formData.TotalTaxableValue, formData.TotalTaxCess, formData.TotalFreightPackaging, formData.TotalSalesPrice).Values(&maps)

	getID := maps[0]["_id"].(string)
	getValue, _ := strconv.ParseInt(getID, 10, 64)
	soID := int(getValue)

	//INSERT IN SALES ORDER STATUS
	_, err3 := o.Raw("INSERT INTO sales_order_status (_orgid, sales_order_fkid, actedon_by_fkid, date_actedon, status) VALUES (?,?,?,?,?) ", formData.Orgid, soID, formData.Userid, time.Now(), Status).Exec()
	if err2 != nil || err3 != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}

	//LOOP THROUGH THE ITEMS, ADD TO SALES ORDER ITEMS
	for _, it := range salesitems {
		res := AddSalesItemsByAvailableQuantity(soID, it, formData.OLocation, formData.Orgid, formData.DeliveryDate, Status)
		if res == 0 {
			ormTransactionalErr = o.Rollback()
			return 0
		}
	}

	if ormTransactionalErr != nil {
		return 0
	}
	ormTransactionalErr = o.Commit()
	return 1
}

/*Active : Insert in sales_order_items, based on status of available quantity*/
func AddSalesItemsByAvailableQuantity(soID int, item map[string]string, oLocation string, orgid string, deliveryDate string, status int) int {

	o := orm.NewOrm()
	ormTransactionalErr := o.Begin()
	if item["item_name"] != "" {
		var hsn *string
		hsn = nil
		if item["hsn"] != "" {
			tempHSN := item["hsn"]
			hsn = &tempHSN
		}

		var discountrate string
		discountrate = "0"
		if item["discount_percent"] != "" {
			tempDiscountPercent := item["discount_percent"]
			discountrate = tempDiscountPercent
		}

		var discountamount string
		discountamount = "0.00"
		if item["discount_value"] != "" {
			tempDiscountAmount := item["discount_value"]
			discountamount = tempDiscountAmount
		}

		var gstrate string
		gstrate = "0"
		if item["gst_percent"] != "" {
			tempGSTPercent := item["gst_percent"]
			gstrate = tempGSTPercent
		}

		var igstrate string
		igstrate = "0"
		if item["igst_rate"] != "" {
			tempIGSTRate := item["igst_rate"]
			igstrate = tempIGSTRate
		}

		var igstAmount string
		igstAmount = "0"
		if item["igst_amount"] != "" {
			tempIGSTAmount := item["igst_amount"]
			igstAmount = tempIGSTAmount
		}

		var cgstrate string
		cgstrate = "0"
		if item["cgst_rate"] != "" {
			tempCGSTRate := item["cgst_rate"]
			cgstrate = tempCGSTRate
		}

		var cgstAmount string
		cgstAmount = "0"
		if item["cgst_amount"] != "" {
			tempCGSTAmount := item["cgst_amount"]
			cgstAmount = tempCGSTAmount
		}

		var sgstrate string
		sgstrate = "0"
		if item["sgst_rate"] != "" {
			tempSGSTRate := item["sgst_rate"]
			sgstrate = tempSGSTRate
		}

		var sgstAmount string
		sgstAmount = "0"
		if item["sgst_amount"] != "" {
			tempSGSTAmount := item["sgst_amount"]
			sgstAmount = tempSGSTAmount
		}

		var cessrate string
		cessrate = "0"
		if item["cess_rate"] != "" {
			tempCessRate := item["cess_rate"]
			cessrate = tempCessRate
		}

		var cessAmount string
		cessAmount = "0"
		if item["cess_amount"] != "" {
			tempCessAmount := item["cess_amount"]
			cessAmount = tempCessAmount
		}

		var totalothercharges string
		totalothercharges = "0.00"
		if item["total_other_charges"] != "" {
			tempTotalOtherCharges := item["total_other_charges"]
			totalothercharges = tempTotalOtherCharges
		}

		// var transporter *string
		// transporter = nil
		// if transporterFkid != "" {
		// 	temptransporter := transporterFkid
		// 	transporter = &temptransporter
		// }

		// var vehicle *string
		// vehicle = nil
		// if vehicleFkid != "" {
		// 	tempvehicle := vehicleFkid
		// 	vehicle = &tempvehicle
		// }

		_, err4 := o.Raw("INSERT INTO sales_order_items (_sales_order_fkid, item_org_assoc_fkid, quantity, uom, hsn_sac, price, discount_rate, discount_value, delivery_date, taxable_amount, gst_rate, igst_rate, igst_amount, cgst_rate, cgst_amount, sgst_rate, sgst_amount, cess_rate, cess_amount, total_tax_cess, total_after_tax, total_other_charges, _orgid, status, geo_location) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ", soID, item["item_name"], item["qty"], item["uom"], &hsn, item["cost"], discountrate, discountamount, deliveryDate, item["taxable_amount"], gstrate, igstrate, igstAmount, cgstrate, cgstAmount, sgstrate, sgstAmount, cessrate, cessAmount, item["total_tax_cess"], item["total_after_tax"], totalothercharges, orgid, 1, oLocation).Exec()
		if err4 != nil {
			ormTransactionalErr = o.Rollback()
			return 0
		}

	}
	if ormTransactionalErr != nil {
		return 0
	}
	ormTransactionalErr = o.Commit()
	return 1
}

func GetSalesOrderByOrderNumber(orderNumber string, orgId string) map[string]orm.Params {
	o := orm.NewOrm()
	result := make(map[string]orm.Params)
	var maps []orm.Params
	//Select the id of the inserted record and make entry in sales order status
	num, err := o.Raw("SELECT * FROM sales_order WHERE order_no=? and _orgid=?", orderNumber, orgId).Values(&maps)

	if err == nil && num > 0 {
		for k, v := range maps {
			key := "sales_" + strconv.Itoa(k)
			result[key] = v
		}
	} else {
		result["status"] = nil
	}
	return result
}

func GetSalesOrderItemsByOrderId(orderId int, orgId string) map[string]orm.Params {
	o := orm.NewOrm()
	result := make(map[string]orm.Params)
	var maps []orm.Params
	//Select the id of the inserted record and make entry in sales order status
	num, err := o.Raw("SELECT * FROM sales_order_items WHERE _sales_order_fkid= ? and _orgid=?", orderId, orgId).Values(&maps)

	if err == nil && num > 0 {
		for k, v := range maps {
			key := "sales_" + strconv.Itoa(k)
			result[key] = v
		}
	} else {
		result["status"] = nil
	}
	//fmt.Println(result)
	return result
}

func GetMaxSalesOrderCode(orgid string) int {
	var maxCode int
	maxCode = 1
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT max(order_no) as maxcode FROM sales_order where _orgid=?", orgid).Values(&maps)

	if num > 0 && err == nil {
		if maps[0]["maxcode"] != nil {
			code := maps[0]["maxcode"].(string)
			getValue, _ := strconv.ParseInt(code, 10, 64)
			maxCode = int(getValue)
		}
	}
	return maxCode
}

/*--------------------------------------------------------XXXXX---------------------------------------------------*/

func CreateStockTransfer(formData StockTransfer) int {

	o := orm.NewOrm()
	var maps []orm.Params
	_, err1 := o.Raw("INSERT INTO sales_order (order_no, order_date, customer_fkid, job_type, delivery_date, excise_charges, octroi_charges, transport_charges, dispatch_type, payment_terms, remarks, _orgid,total_quantity,total_price,total_discount,total_sales_price,status, total_after_discount,total_after_excise, total_after_octroi,total_price_after_taxes, tariff_no) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ", formData.OrderNo, formData.OrderDate, formData.CustomerName, "ST", formData.DeliveryDate, formData.ExciseCharges, formData.OctroiCharges, formData.TransportCharges, formData.DispatchType, formData.PaymentTerms, formData.Remarks, formData.Orgid, formData.TotalQuantity, formData.TotalPrice, formData.TotalDiscount, formData.TotalSalesPrice, formData.Status, formData.TotalAfterDiscount, formData.TotalAfterExcise, formData.TotalAfterOctroi, formData.TotalAfterTaxes, formData.TariffNo).Exec()
	_, err2 := o.Raw("SELECT _id FROM sales_order WHERE order_no=? and _orgid=? and customer_fkid=? and job_type=? and delivery_date=? and excise_charges=? and transport_charges=?", formData.OrderNo, formData.Orgid, formData.CustomerName, "ST", formData.DeliveryDate, formData.ExciseCharges, formData.TransportCharges).Values(&maps)
	_, err3 := o.Raw("INSERT INTO sales_order_status (_orgid, sales_order_fkid, actedon_by_fkid, date_actedon, status) VALUES (?,?,?,?,?) ", formData.Orgid, maps[0]["_id"], formData.Userid, time.Now(), formData.Status).Exec()
	if err1 != nil || err2 != nil || err3 != nil {
		return 0
	}
	return 1
}

func InsertSTItems(formData StockTransfer) int {

	o := orm.NewOrm()
	var maps []orm.Params
	_, err1 := o.Raw("SELECT _id FROM sales_order WHERE order_no=? and _orgid=? and customer_fkid=? and job_type=? and delivery_date=? and excise_charges=? and transport_charges=?", formData.OrderNo, formData.Orgid, formData.CustomerName, "ST", formData.DeliveryDate, formData.ExciseCharges, formData.TransportCharges).Values(&maps)
	var maps0 []orm.Params
	_, err2 := o.Raw("SELECT unit_measure FROM item_org_assoc WHERE _id=? and _orgid=?", formData.ItemName, formData.Orgid).Values(&maps0)

	_, err3 := o.Raw("INSERT INTO sales_order_items (_sales_order_fkid, item_org_assoc_fkid, quantity, uom, price, discount, delivery_date, _orgid, status) VALUES (?,?,?,?,?,?,?,?,?) ", maps[0]["_id"], formData.ItemName, formData.Quantity, maps0[0]["unit_measure"], formData.Rate, formData.Discount, formData.DeliveryDate, formData.Orgid, "1").Exec()

	if err1 != nil || err2 != nil || err3 != nil {
		return 0
	}
	return 1
}

func GetSales(formData Sales) map[string]orm.Params {
	// var stock []Stock
	// o := orm.NewOrm()
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT so._id,so.order_no, so.order_date as o_date, c.company_name, so.job_type,so.delivery_date as d_date, so.delivery_location, so.origin_location, so.tax_regime, so.payment_terms, so.remarks, so.total_sales_price, so.status, so.total_price_after_taxes, so.tariff_no, so.billing_address, so.total_taxable_value, so.total_tax_cess, so.total_freight_packaging_others FROM sales_order as so, customer as c WHERE  so._orgid=? and c._orgid=? and c._id=so.customer_fkid ", formData.Orgid, formData.Orgid).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		var i int64 = 0
		for ; i < num; i++ {
			if maps[i]["delivery_location"] != nil {
				var maps1 []orm.Params
				count, err1 := o.Raw("SELECT cwa.wstate AS delivery_state, cwa.wpincode AS delivery_pincode, CONCAT(cwa.address_line1, ', ', cwa.wcity, ', ', cwa.wstate, ', ', cwa.wcountry, ', ', cwa.wpincode) AS delivery_location_string FROM customer_warehouse_address as cwa WHERE cwa._orgid=? and cwa._id=?", formData.Orgid, maps[i]["delivery_location"]).Values(&maps1)
				if err1 == nil && count > 0 {
					maps[i]["delivery_location_string"] = maps1[0]["delivery_location_string"]
					maps[i]["delivery_state"] = maps1[0]["delivery_state"]
					maps[i]["delivery_pincode"] = maps1[0]["delivery_pincode"]
				} else {
					maps[i]["delivery_location_string"] = ""
					maps[i]["delivery_state"] = ""
					maps[i]["delivery_pincode"] = ""
				}
			} else {
				maps[i]["delivery_location_string"] = ""
				maps[i]["delivery_state"] = ""
				maps[i]["delivery_pincode"] = ""
			}

			if maps[i]["origin_location"] != nil {
				var maps1 []orm.Params
				origin_count, err01 := o.Raw("SELECT of.factory_location FROM organisation_factories as of WHERE of._orgid=? and of._id=?", formData.Orgid, maps[i]["origin_location"]).Values(&maps1)
				if err01 == nil && origin_count > 0 {
					maps[i]["factory_location"] = maps1[0]["factory_location"]
				} else {
					maps[i]["factory_location"] = ""
				}
			} else {
				maps[i]["factory_location"] = ""
			}

			if maps[i]["billing_address"] != nil {
				var maps2 []orm.Params
				nos, err2 := o.Raw("SELECT cwa.wstate AS billing_state, cwa.wpincode AS billing_pincode, CONCAT(cwa.address_line1, ', ', cwa.wcity, ', ', cwa.wstate, ', ', cwa.wcountry, ', ', cwa.wpincode) AS billing_address_string FROM customer_warehouse_address as cwa WHERE cwa._orgid=? and cwa._id=?", formData.Orgid, maps[i]["billing_address"]).Values(&maps2)
				if err2 == nil && nos > 0 {
					maps[i]["billing_address_string"] = maps2[0]["billing_address_string"]
					maps[i]["billing_state"] = maps2[0]["billing_state"]
					maps[i]["billing_pincode"] = maps2[0]["billing_pincode"]
				} else {
					maps[i]["billing_address_string"] = ""
					maps[i]["billing_state"] = ""
					maps[i]["billing_pincode"] = ""
				}
			} else {
				maps[i]["billing_address_string"] = ""
				maps[i]["billing_state"] = ""
				maps[i]["billing_pincode"] = ""
			}
			var maps3 []orm.Params
			if (maps[i]["status"] == "99") || (maps[i]["status"] == "0") {

				itemStock, err3 := o.Raw("SELECT * FROM sales_order_items WHERE _sales_order_fkid=? and _orgid=? and status=1", maps[i]["_id"], formData.Orgid).Values(&maps3)
				if err3 == nil && itemStock > 0 {
					var j int64 = 0
					var canbDispatched bool = false
					for ; j < itemStock; j++ {
						if maps3[j]["status"] == "99" {
							var QtyinStock string
							var reqQty float64 = 0.0
							reqQty, _ = strconv.ParseFloat(maps3[j]["quantity"].(string), 64)
							QtyinStock = CheckForQty(maps3[j]["item_org_assoc_fkid"].(string), maps[i]["origin_location"].(string), formData.Orgid)
							f, _ := strconv.ParseFloat(QtyinStock, 64)

							if reqQty <= f {
								canbDispatched = true
							} else {
								canbDispatched = false
							}

						}
					}
					maps[i]["canbDispatched"] = canbDispatched
				}
			} else {
				//maps[i]["canbDispatched"]= false
			}
		}
		for k, v := range maps {
			if maps[k]["o_date"] != nil && maps[k]["o_date"] != "" {
				maps[k]["o_date"] = convertDateString(maps[k]["o_date"].(string))
			}
			if maps[k]["d_date"] != nil && maps[k]["d_date"] != "" {
				maps[k]["d_date"] = convertDateString(maps[k]["d_date"].(string))
			}

			// var transMaps []orm.Params
			// transNum, transErr := o.Raw("SELECT * FROM sales_order_items WHERE _orgid=? and _sales_order_fkid=? and status=1", formData.Orgid, maps[k]["_id"]).Values(&transMaps)
			// if transNum > 0 && transErr == nil {
			// 	if transMaps[0]["transporter_fkid"] == nil {
			// 		maps[k]["transporter_fkid"] = ""
			// 	} else {
			// 		maps[k]["transporter_fkid"] = transMaps[0]["transporter_fkid"].(string)
			// 	}
			// 	if transMaps[0]["vehicle_fkid"] == nil {
			// 		maps[k]["vehicle_fkid"] = ""
			// 	} else {
			// 		maps[k]["vehicle_fkid"] = transMaps[0]["vehicle_fkid"].(string)
			// 	}
			// } else {
			// 	maps[k]["vehicle_fkid"] = ""
			// 	maps[k]["vehicle_fkid"] = ""
			// }

			if maps[k]["transporter_fkid"] != nil && maps[k]["transporter_fkid"] != "" {
				var transportermaps []orm.Params
				transporternum, transportererr := o.Raw("SELECT * FROM transporter WHERE _id=? and _orgid=? ", maps[k]["transporter_fkid"], formData.Orgid).Values(&transportermaps)
				if transporternum > 0 && transportererr == nil {
					maps[k]["transporter_name"] = transportermaps[0]["transporter_name"]
				}
			}

			if maps[k]["vehicle_fkid"] != nil && maps[k]["vehicle_fkid"] != "" {
				var vehiclemaps []orm.Params
				vehiclenum, vehicleerr := o.Raw("SELECT * FROM vehicle WHERE _id=? and _orgid=? ", maps[k]["vehicle_fkid"], formData.Orgid).Values(&vehiclemaps)
				if vehiclenum > 0 && vehicleerr == nil {
					maps[k]["registration_number"] = vehiclemaps[0]["registration_number"]
				}
			}
			key := "sales_" + strconv.Itoa(k)
			result[key] = v

		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetDelayedSalesOrders(formData Sales) map[string]orm.Params {
	// var stock []Stock
	// o := orm.NewOrm()
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT so._id,so.order_no, so.order_date as o_date, c.company_name, so.job_type, so.delivery_date as d_date, so.delivery_location, so.origin_location, so.tax_regime, so.payment_terms, so.remarks, so.total_sales_price, so.status, so.total_price_after_taxes, so.tariff_no, so.billing_address, so.total_taxable_value, so.total_tax_cess, so.total_freight_packaging_others, so.transporter_fkid, so.vehicle_fkid FROM sales_order as so, customer as c WHERE  so._orgid=? and c._orgid=? and c._id=so.customer_fkid and so.delivery_date<? ORDER BY so.delivery_date ASC", formData.Orgid, formData.Orgid, GetTodaysDate()).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		var i int64 = 0
		for ; i < num; i++ {
			if maps[i]["delivery_location"] != nil {
				var maps1 []orm.Params
				count, err1 := o.Raw("SELECT cwa.wstate AS delivery_state, cwa.wpincode AS delivery_pincode, CONCAT(cwa.address_line1, ', ', cwa.wcity, ', ', cwa.wstate, ', ', cwa.wcountry, ', ', cwa.wpincode) AS delivery_location_string FROM customer_warehouse_address as cwa WHERE cwa._orgid=? and cwa._id=?", formData.Orgid, maps[i]["delivery_location"]).Values(&maps1)
				if err1 == nil && count > 0 {
					maps[i]["delivery_location_string"] = maps1[0]["delivery_location_string"]
					maps[i]["delivery_state"] = maps1[0]["delivery_state"]
					maps[i]["delivery_pincode"] = maps1[0]["delivery_pincode"]
				} else {
					maps[i]["delivery_location_string"] = ""
					maps[i]["delivery_state"] = ""
					maps[i]["delivery_pincode"] = ""
				}
			} else {
				maps[i]["delivery_location_string"] = ""
				maps[i]["delivery_state"] = ""
				maps[i]["delivery_pincode"] = ""
			}

			if maps[i]["origin_location"] != nil {
				var maps1 []orm.Params
				origin_count, err01 := o.Raw("SELECT of.factory_location FROM organisation_factories as of WHERE of._orgid=? and of._id=?", formData.Orgid, maps[i]["origin_location"]).Values(&maps1)
				if err01 == nil && origin_count > 0 {
					maps[i]["factory_location"] = maps1[0]["factory_location"]
				} else {
					maps[i]["factory_location"] = ""
				}
			} else {
				maps[i]["factory_location"] = ""
			}

			if maps[i]["billing_address"] != nil {
				var maps2 []orm.Params
				nos, err2 := o.Raw("SELECT cwa.wstate AS billing_state, cwa.wpincode AS billing_pincode, CONCAT(cwa.address_line1, ', ', cwa.wcity, ', ', cwa.wstate, ', ', cwa.wcountry, ', ', cwa.wpincode) AS billing_address_string FROM customer_warehouse_address as cwa WHERE cwa._orgid=? and cwa._id=?", formData.Orgid, maps[i]["billing_address"]).Values(&maps2)
				if err2 == nil && nos > 0 {
					maps[i]["billing_address_string"] = maps2[0]["billing_address_string"]
					maps[i]["billing_state"] = maps2[0]["billing_state"]
					maps[i]["billing_pincode"] = maps2[0]["billing_pincode"]
				} else {
					maps[i]["billing_address_string"] = ""
					maps[i]["billing_state"] = ""
					maps[i]["billing_pincode"] = ""
				}
			} else {
				maps[i]["billing_address_string"] = ""
				maps[i]["billing_state"] = ""
				maps[i]["billing_pincode"] = ""
			}
			var maps3 []orm.Params
			if (maps[i]["status"] == "99") || (maps[i]["status"] == "0") {

				itemStock, err3 := o.Raw("SELECT * FROM sales_order_items WHERE _sales_order_fkid=? and _orgid=? and status=1", maps[i]["_id"], formData.Orgid).Values(&maps3)
				if err3 == nil && itemStock > 0 {
					var j int64 = 0
					var canbDispatched bool = false
					for ; j < itemStock; j++ {
						if maps3[j]["status"] == "99" {
							var QtyinStock string
							var reqQty float64 = 0.0
							reqQty, _ = strconv.ParseFloat(maps3[j]["quantity"].(string), 64)
							QtyinStock = CheckForQty(maps3[j]["item_org_assoc_fkid"].(string), maps[i]["origin_location"].(string), formData.Orgid)
							f, _ := strconv.ParseFloat(QtyinStock, 64)

							if reqQty <= f {
								canbDispatched = true
							} else {
								canbDispatched = false
							}

						}
					}
					maps[i]["canbDispatched"] = canbDispatched
				}
			} else {
				//maps[i]["canbDispatched"]= false
			}
		}
		for k, v := range maps {
			if maps[k]["o_date"] != nil && maps[k]["o_date"] != "" {
				maps[k]["o_date"] = convertDateString(maps[k]["o_date"].(string))
			}
			if maps[k]["d_date"] != nil && maps[k]["d_date"] != "" {
				maps[k]["d_date"] = convertDateString(maps[k]["d_date"].(string))
			}

			// var transMaps []orm.Params
			// transNum, transErr := o.Raw("SELECT * FROM sales_order_items WHERE _orgid=? and _sales_order_fkid=? and status=1", formData.Orgid, maps[k]["_id"]).Values(&transMaps)
			// if transNum > 0 && transErr == nil {
			// 	if transMaps[0]["transporter_fkid"] == nil {
			// 		maps[k]["transporter_fkid"] = ""
			// 	} else {
			// 		maps[k]["transporter_fkid"] = transMaps[0]["transporter_fkid"].(string)
			// 	}
			// 	if transMaps[0]["vehicle_fkid"] == nil {
			// 		maps[k]["vehicle_fkid"] = ""
			// 	} else {
			// 		maps[k]["vehicle_fkid"] = transMaps[0]["vehicle_fkid"].(string)
			// 	}
			// } else {
			// 	maps[k]["vehicle_fkid"] = ""
			// 	maps[k]["vehicle_fkid"] = ""
			// }

			if maps[k]["transporter_fkid"] != nil && maps[k]["transporter_fkid"] != "" {
				var transportermaps []orm.Params
				transporternum, transportererr := o.Raw("SELECT * FROM transporter WHERE _id=? and _orgid=? ", maps[k]["transporter_fkid"], formData.Orgid).Values(&transportermaps)
				if transporternum > 0 && transportererr == nil {
					maps[k]["transporter_name"] = transportermaps[0]["transporter_name"]
				}
			}

			if maps[k]["vehicle_fkid"] != nil && maps[k]["vehicle_fkid"] != "" {
				var vehiclemaps []orm.Params
				vehiclenum, vehicleerr := o.Raw("SELECT * FROM vehicle WHERE _id=? and _orgid=? ", maps[k]["vehicle_fkid"], formData.Orgid).Values(&vehiclemaps)
				if vehiclenum > 0 && vehicleerr == nil {
					maps[k]["registration_number"] = vehiclemaps[0]["registration_number"]
				}
			}
			key := "sales_" + strconv.Itoa(k)
			result[key] = v

		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetSalesDetailsByCode(sales_code string, _orgId string) map[string]orm.Params {
	// var stock []Stock
	// o := orm.NewOrm()
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT so._id,so.order_no, so.order_date as o_date, c.company_name, so.job_type, so.delivery_date as d_date, so.delivery_location, so.origin_location, so.payment_terms, so.remarks, so.total_sales_price, so.status, so.total_price_after_taxes, so.tariff_no, so.billing_address, so.total_taxable_value, so.total_tax_cess, so.total_freight_packaging_others FROM sales_order as so, customer as c WHERE so._id=? and so._orgid=? and c._orgid=? and so.customer_fkid=c._id  ", sales_code, _orgId, _orgId).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		var i int64 = 0
		for ; i < num; i++ {
			if maps[i]["delivery_location"] != nil {
				var maps1 []orm.Params
				count, err1 := o.Raw("SELECT CONCAT(cwa.address_line1, ', ', cwa.wcity, ', ', cwa.wstate, ', ', cwa.wcountry, ', ', cwa.wpincode) AS delivery_location_string, cwa.wstate as delivery_state, cwa.wpincode as delivery_pincode FROM customer_warehouse_address as cwa WHERE cwa._orgid=? and cwa._id=?", _orgId, maps[i]["delivery_location"]).Values(&maps1)
				if err1 == nil && count > 0 {
					maps[i]["delivery_location_string"] = maps1[0]["delivery_location_string"]
					maps[i]["delivery_state"] = maps1[0]["delivery_state"]
					maps[i]["delivery_pincode"] = maps1[0]["delivery_pincode"]
				} else {
					maps[i]["delivery_location_string"] = ""
					maps[i]["delivery_state"] = ""
					maps[i]["delivery_pincode"] = ""
				}
			} else {
				maps[i]["delivery_location_string"] = ""
				maps[i]["delivery_state"] = ""
				maps[i]["delivery_pincode"] = ""
			}

			if maps[i]["billing_address"] != nil {
				var maps2 []orm.Params
				nos, err2 := o.Raw("SELECT cwa.wstate AS billing_state, cwa.wpincode AS billing_pincode, CONCAT(cwa.address_line1, ', ', cwa.wcity, ', ', cwa.wstate, ', ', cwa.wcountry, ', ', cwa.wpincode) AS billing_address_string FROM customer_warehouse_address as cwa WHERE cwa._orgid=? and cwa._id=?", _orgId, maps[i]["billing_address"]).Values(&maps2)
				if err2 == nil && nos > 0 {
					maps[i]["billing_address_string"] = maps2[0]["billing_address_string"]
					maps[i]["billing_state"] = maps2[0]["billing_state"]
					maps[i]["billing_pincode"] = maps2[0]["billing_pincode"]
				} else {
					maps[i]["billing_address_string"] = ""
					maps[i]["billing_state"] = ""
					maps[i]["billing_pincode"] = ""
				}
			} else {
				maps[i]["billing_address_string"] = ""
				maps[i]["billing_state"] = ""
				maps[i]["billing_pincode"] = ""
			}

			if maps[i]["origin_location"] != nil {
				var maps3 []orm.Params
				o_nos, err3 := o.Raw("SELECT factory_location FROM organisation_factories WHERE _orgid=? and _id=?", _orgId, maps[i]["origin_location"]).Values(&maps3)
				if err3 == nil && o_nos > 0 {
					maps[i]["origin_factory"] = maps3[0]["factory_location"]
				} else {
					maps[i]["origin_factory"] = ""
				}
			} else {
				maps[i]["origin_factory"] = ""
			}
		}
		for k, v := range maps {
			key := "sales_" + strconv.Itoa(k)
			result[key] = v

		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetSOItemsByCode(sales_code string, orgid string) map[string]orm.Params {

	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT ioa.invoice_narration, soi.quantity, soi.uom, soi.hsn_sac, soi.price, soi.discount_rate, soi.discount_value, soi.delivery_date as delivery_date, soi.taxable_amount, soi.gst_rate, soi.igst_rate, soi.igst_amount, soi.cgst_rate, soi.cgst_amount, soi.sgst_rate, soi.sgst_amount, soi.cess_rate, soi.cess_amount, soi.total_tax_cess, soi.total_after_tax, soi.freight, soi.insurance, soi.packaging_forwarding, soi.others, soi.total_other_charges FROM item_org_assoc as ioa, sales_order_items as soi, sales_order as so WHERE soi._sales_order_fkid=? and soi._orgid=? and ioa._orgid=? and ioa._id=soi.item_org_assoc_fkid and soi._sales_order_fkid=so._id and soi.status=1", sales_code, orgid, orgid).Values(&maps)
	//fmt.Println("num=", num)
	// var i int64 = 0
	// for ; i < num; i++ {
	// 	fmt.Println(maps[i])
	// }
	if err == nil {

		var j int64 = 0
		for ; j < num; j++ {

			if maps[j]["discount_rate"] == nil {
				maps[j]["discount_rate"] = ""
			}

			if maps[j]["uom"] == nil {
				maps[j]["uom"] = ""
			}

			if maps[j]["discount_value"] == nil {
				maps[j]["discount_value"] = ""
			}

			if maps[j]["hsn_sac"] == nil {
				maps[j]["hsn_sac"] = ""
			}

			if maps[j]["taxable_amount"] == nil {
				maps[j]["taxable_amount"] = ""
			}

			if maps[j]["gst_rate"] == nil {
				maps[j]["gst_rate"] = ""
			}

			if maps[j]["igst_rate"] == nil {
				maps[j]["igst_rate"] = ""
			}

			if maps[j]["igst_amount"] == nil {
				maps[j]["igst_amount"] = ""
			}

			if maps[j]["cgst_rate"] == nil {
				maps[j]["cgst_rate"] = ""
			}

			if maps[j]["cgst_amount"] == nil {
				maps[j]["cgst_amount"] = ""
			}

			if maps[j]["sgst_rate"] == nil {
				maps[j]["sgst_rate"] = ""
			}

			if maps[j]["sgst_amount"] == nil {
				maps[j]["sgst_amount"] = ""
			}

			if maps[j]["cess_rate"] == nil {
				maps[j]["cess_rate"] = ""
			}

			if maps[j]["cess_amount"] == nil {
				maps[j]["cess_amount"] = ""
			}

			if maps[j]["total_tax_cess"] == nil {
				maps[j]["total_tax_cess"] = ""
			}

			if maps[j]["total_after_tax"] == nil {
				maps[j]["total_after_tax"] = ""
			}

			if maps[j]["freight"] == nil {
				maps[j]["freight"] = ""
			}

			if maps[j]["insurance"] == nil {
				maps[j]["insurance"] = ""
			}

			if maps[j]["packaging_forwarding"] == nil {
				maps[j]["packaging_forwarding"] = ""
			}

			if maps[j]["others"] == nil {
				maps[j]["others"] = ""
			}

			if maps[j]["total_other_charges"] == nil {
				maps[j]["total_other_charges"] = ""
			}

		}

		for k, v := range maps {
			key := "sales_" + strconv.Itoa(k)
			result[key] = v

		}
	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetSaItems(formData Sales) map[string]orm.Params {

	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT ioa._id, ioa.invoice_narration, ioa.unit_measure, ioa.item_unique_code, ioa.name FROM item_org_assoc as ioa WHERE ioa._orgid=? and status=1", formData.Orgid).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		var i int64 = 0
		for ; i < num; i++ {
			//fmt.Println(maps[i])
		}
		for k, v := range maps {

			key := "io_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func STUniqueCheck(formData StockTransfer) int {
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT _id FROM sales_order WHERE order_no=?", formData.OrderNo).Values(&maps)
	//fmt.Println("num:",num)
	if err != nil || num > 0 {
		//fmt.Println("Returns 1")
		return 0
	}
	return 1
}

func UniqueCheck2(formData Sales) int {
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT _id FROM sales_order WHERE order_no=?", formData.OrderNo).Values(&maps)
	//fmt.Println("num:",num)
	if err != nil || num > 0 {
		//fmt.Println("Returns 1")
		return 0
	}
	return 1
}

func GetCustomersNames(formData Sales) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT _id,company_name FROM customer WHERE _orgid=?", formData.Orgid).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		var i int64 = 0
		for ; i < num; i++ {
			//fmt.Println(maps[i])
		}
		for k, v := range maps {

			key := "io_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		// fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetSOItems(product_id string, orgid string) ([]string, []string, string, []string, []string, []string, []string, []string, []string, []string, []string, []string, []string, []string, []string, []string, []string, []string, []string, []string, []string, []string, []string, []string, []string, []string, string) {

	status := "false"
	var items []string
	var quantity []string
	var rate []string
	var discount_rate []string
	var delivery_date []string
	var uom []string

	var discount_value []string
	var hsn_sac []string
	var taxable_amount []string
	var gst_rate []string
	var igst_rate []string
	var igst_amount []string
	var cgst_rate []string
	var cgst_amount []string
	var sgst_rate []string
	var sgst_amount []string
	var cess_rate []string
	var cess_amount []string
	var total_tax_cess []string
	var total_after_tax []string
	var freight []string
	var insurance []string
	var packaging_forwarding []string
	var others []string
	var total_other_charges []string

	var tax_regime string

	o := orm.NewOrm()
	var tax_maps []orm.Params
	count, err1 := o.Raw("SELECT tax_regime FROM sales_order as so WHERE so._id=? and so._orgid=?", product_id, orgid).Values(&tax_maps)
	if err1 == nil && count > 0 {
		if tax_maps[0]["tax_regime"] != nil {
			tax_regime = tax_maps[0]["tax_regime"].(string)
		} else {
			tax_regime = "1"
		}
	} else {
		tax_regime = "1"
	}

	var maps []orm.Params
	num, err := o.Raw("SELECT ioa.invoice_narration, soi.quantity, soi.uom, soi.hsn_sac, soi.price, soi.discount_rate, soi.discount_value, soi.delivery_date as delivery_date, soi.taxable_amount, soi.gst_rate, soi.igst_rate, soi.igst_amount, soi.cgst_rate, soi.cgst_amount, soi.sgst_rate, soi.sgst_amount, soi.cess_rate, soi.cess_amount, soi.total_tax_cess, soi.total_after_tax, soi.freight, soi.insurance, soi.packaging_forwarding, soi.others, soi.total_other_charges FROM item_org_assoc as ioa, sales_order_items as soi, sales_order as so WHERE soi._sales_order_fkid=? and soi._orgid=? and ioa._orgid=? and soi.status=1 and ioa._id=soi.item_org_assoc_fkid and soi._sales_order_fkid=so._id", product_id, orgid, orgid).Values(&maps) //and soi.status=1
	//fmt.Println("num=", num)
	// var i int64 = 0
	// for ; i < num; i++ {
	// 	fmt.Println(maps[i])
	// }
	if err == nil {
		var new_items string
		var new_quantity string
		var new_rate string
		var new_discount_rate string
		var new_delivery_date string
		var new_uom string

		var new_discount_value string
		var new_hsn_sac string
		var new_taxable_amount string
		var new_gst_rate string
		var new_igst_rate string
		var new_igst_amount string
		var new_cgst_rate string
		var new_cgst_amount string
		var new_sgst_rate string
		var new_sgst_amount string
		var new_cess_rate string
		var new_cess_amount string
		var new_total_tax_cess string
		var new_total_after_tax string
		var new_freight string
		var new_insurance string
		var new_packaging_forwarding string
		var new_others string
		var new_total_other_charges string

		var j int64 = 0
		for ; j < num; j++ {
			new_items = maps[j]["invoice_narration"].(string)
			items = append(items, new_items)

			new_quantity = maps[j]["quantity"].(string)
			quantity = append(quantity, new_quantity)

			new_rate = maps[j]["price"].(string)
			rate = append(rate, new_rate)

			if maps[j]["discount_rate"] != nil {
				new_discount_rate = maps[j]["discount_rate"].(string)
			} else {
				new_discount_rate = ""
			}
			discount_rate = append(discount_rate, new_discount_rate)

			new_delivery_date = maps[j]["delivery_date"].(string)
			delivery_date = append(delivery_date, new_delivery_date)

			new_uom = maps[j]["uom"].(string)
			uom = append(uom, new_uom)

			if maps[j]["discount_value"] != nil {
				new_discount_value = maps[j]["discount_value"].(string)
			} else {
				new_discount_value = ""
			}
			discount_value = append(discount_value, new_discount_value)

			if maps[j]["hsn_sac"] != nil {
				new_hsn_sac = maps[j]["hsn_sac"].(string)

			} else {
				new_hsn_sac = ""
			}
			hsn_sac = append(hsn_sac, new_hsn_sac)

			if maps[j]["taxable_amount"] != nil {
				new_taxable_amount = maps[j]["taxable_amount"].(string)

			} else {
				new_taxable_amount = ""
			}
			taxable_amount = append(taxable_amount, new_taxable_amount)

			if maps[j]["gst_rate"] != nil {
				new_gst_rate = maps[j]["gst_rate"].(string)

			} else {
				new_gst_rate = ""
			}
			gst_rate = append(gst_rate, new_gst_rate)

			if maps[j]["igst_rate"] != nil {
				new_igst_rate = maps[j]["igst_rate"].(string)

			} else {
				new_igst_rate = ""
			}
			igst_rate = append(igst_rate, new_igst_rate)

			if maps[j]["igst_amount"] != nil {
				new_igst_amount = maps[j]["igst_amount"].(string)

			} else {
				new_igst_amount = ""
			}
			igst_amount = append(igst_amount, new_igst_amount)

			if maps[j]["cgst_rate"] != nil {
				new_cgst_rate = maps[j]["cgst_rate"].(string)

			} else {
				new_cgst_rate = ""
			}
			cgst_rate = append(cgst_rate, new_cgst_rate)

			if maps[j]["cgst_amount"] != nil {
				new_cgst_amount = maps[j]["cgst_amount"].(string)

			} else {
				new_cgst_amount = ""
			}
			cgst_amount = append(cgst_amount, new_cgst_amount)

			if maps[j]["sgst_rate"] != nil {
				new_sgst_rate = maps[j]["sgst_rate"].(string)

			} else {
				new_sgst_rate = ""
			}
			sgst_rate = append(sgst_rate, new_sgst_rate)

			if maps[j]["sgst_amount"] != nil {
				new_sgst_amount = maps[j]["sgst_amount"].(string)

			} else {
				new_sgst_amount = ""
			}
			sgst_amount = append(sgst_amount, new_sgst_amount)

			if maps[j]["cess_rate"] != nil {
				new_cess_rate = maps[j]["cess_rate"].(string)

			} else {
				new_cess_rate = ""
			}
			cess_rate = append(cess_rate, new_cess_rate)

			if maps[j]["cess_amount"] != nil {
				new_cess_amount = maps[j]["cess_amount"].(string)

			} else {
				new_cess_amount = ""
			}
			cess_amount = append(cess_amount, new_cess_amount)

			if maps[j]["total_tax_cess"] != nil {
				new_total_tax_cess = maps[j]["total_tax_cess"].(string)

			} else {
				new_total_tax_cess = ""
			}
			total_tax_cess = append(total_tax_cess, new_total_tax_cess)

			if maps[j]["total_after_tax"] != nil {
				new_total_after_tax = maps[j]["total_after_tax"].(string)

			} else {
				new_total_after_tax = ""
			}
			total_after_tax = append(total_after_tax, new_total_after_tax)

			if maps[j]["freight"] != nil {
				new_freight = maps[j]["freight"].(string)

			} else {
				new_freight = ""
			}
			freight = append(freight, new_freight)

			if maps[j]["insurance"] != nil {
				new_insurance = maps[j]["insurance"].(string)

			} else {
				new_insurance = ""
			}
			insurance = append(insurance, new_insurance)

			if maps[j]["packaging_forwarding"] != nil {
				new_packaging_forwarding = maps[j]["packaging_forwarding"].(string)

			} else {
				new_packaging_forwarding = ""
			}
			packaging_forwarding = append(packaging_forwarding, new_packaging_forwarding)

			if maps[j]["others"] != nil {
				new_others = maps[j]["others"].(string)

			} else {
				new_others = ""
			}
			others = append(others, new_others)

			if maps[j]["total_other_charges"] != nil {
				new_total_other_charges = maps[j]["total_other_charges"].(string)

			} else {
				new_total_other_charges = ""
			}
			total_other_charges = append(total_other_charges, new_total_other_charges)

		}
		status = "true"
	}
	//fmt.Println("***total_after_tax=", total_after_tax)
	//fmt.Println("***quantity=", quantity)
	return items, quantity, status, rate, discount_rate, delivery_date, uom, discount_value, hsn_sac, taxable_amount, gst_rate, igst_rate, igst_amount, cgst_rate, cgst_amount, sgst_rate, sgst_amount, cess_rate, cess_amount, total_tax_cess, total_after_tax, freight, insurance, packaging_forwarding, others, total_other_charges, tax_regime
}

func GetSalesRates(item_id string, customer_id string, origin_id string, orgid string) (string, string, string, string, string, string, string) {

	var status string

	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT sales_price, invoice_naration FROM customer_product_assoc as cpa WHERE item_org_assoc_fkid=? and cpa._orgid=? and _customer_fkid=? and status=1", item_id, orgid, customer_id).Values(&maps)
	var new_rate string
	var new_invoice_narration string
	var new_hsn string
	var new_uom string
	var new_gst string
	var new_available_qty string
	if err == nil {
		if num > 0 {
			if maps[0]["sales_price"] == nil || maps[0]["sales_price"] == "" {
				var maps1 []orm.Params
				count, err1 := o.Raw("SELECT rate FROM item_rates WHERE item_org_assoc_fkid=? and _orgid=? ", item_id, orgid).Values(&maps1)
				if err1 == nil && count > 0 {
					if maps1[0]["rate"] != nil {
						new_rate = maps1[0]["rate"].(string)
					} else {
						new_rate = ""
					}
				} else {
					new_rate = ""
				}
			} else {
				new_rate = maps[0]["sales_price"].(string)
			}

			if maps[0]["invoice_naration"] != nil {
				new_invoice_narration = maps[0]["invoice_naration"].(string)
			} else {
				new_invoice_narration = ""
			}

			status = "true"
		} else {
			var maps1 []orm.Params
			count, err1 := o.Raw("SELECT rate FROM item_rates WHERE item_org_assoc_fkid=? and _orgid=? ", item_id, orgid).Values(&maps1)
			if err1 == nil && count > 0 {
				new_rate = maps1[0]["rate"].(string)
				new_invoice_narration = ""

				status = "true"
			}
		}
		var maps2 []orm.Params
		num, err2 := o.Raw("SELECT ioa.hsn_sac, ioa.gst_rate, ioa.unit_measure FROM item_org_assoc as ioa WHERE ioa._id=? and ioa._orgid=?", item_id, orgid).Values(&maps2)
		if err2 == nil && num > 0 {
			if maps2[0]["gst_rate"] == nil {
				new_gst = ""
			} else {
				new_gst = maps2[0]["gst_rate"].(string)
			}

			if maps2[0]["hsn_sac"] == nil {
				new_hsn = ""
			} else {
				new_hsn = maps2[0]["hsn_sac"].(string)
			}

			if maps2[0]["unit_measure"] == nil {
				new_uom = ""
			} else {
				new_uom = maps2[0]["unit_measure"].(string)
			}
		}
		var maps3 []orm.Params
		qty_num, err3 := o.Raw("SELECT Sum(ist.quantity) as total_quantity FROM item_stock as ist WHERE ist.item_org_assoc_fkid=? and ist._orgid=? and ist.geo_location=? and ist.latest=1 and ist.blocked=0", item_id, orgid, origin_id).Values(&maps3)
		//qty_num, err3 := o.Raw("SELECT iti.total_quantity FROM item_total_inventory as iti WHERE iti.item_org_assoc_fkid=? and iti._orgid=? and iti.geo_location=? and iti.status=1", item_id, orgid, origin_id).Values(&maps3)
		if err3 == nil && qty_num > 0 {
			if maps3[0]["total_quantity"] == nil {
				new_available_qty = "0.0"
			} else {
				new_available_qty = maps3[0]["total_quantity"].(string)
			}
		} else {
			new_available_qty = "0.0"
		}
	} else {
		status = "false"
	}

	return new_hsn, new_uom, new_gst, new_rate, new_invoice_narration, new_available_qty, status
}

func UpdateSales(salesstatus string, item_id string, orgid string, userid string) string {

	status := "false"
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("Update sales_order set status=? WHERE _id=? and product_org_assoc._orgid=? ", salesstatus, item_id, orgid).Values(&maps)
	_, err11 := o.Raw("INSERT INTO sales_order_status (_orgid, sales_order_fkid, actedon_by_fkid, date_actedon, status) VALUES (?,?,?,?,?) ", orgid, item_id, userid, time.Now(), salesstatus).Exec()
	if err == nil && err11 == nil && num > 0 {
		status = "true"
	}

	return status
}

func ChangeSalesOrderStatus(SalesOrderFkId int, Orgid string, UserId string, order_status int, salesOrderLocation string) int {
	o := orm.NewOrm()
	ormTransactionalErr := o.Begin()

	//ON APPROVE
	if order_status == 2 {
		//GET THE ITEMS ASSOCIATED WITH THE SALES ORDER FROM SALES ORDER ITEMS
		salesItems := GetSalesOrderItemsByOrderId(SalesOrderFkId, Orgid)
		salesitems := make([]map[string]string, 0, 3)

		for k, _ := range salesItems {
			data := make(map[string]string)
			data["item_name"] = salesItems[k]["item_org_assoc_fkid"].(string)
			data["qty"] = salesItems[k]["quantity"].(string)
			salesitems = append(salesitems, data)
		}

		isInventoryOk := CheckInventoryStatusForAllItems(salesitems, salesOrderLocation, Orgid)

		//BLOCK THE ITEMS AS INVENTORY IS OK
		if isInventoryOk == "1" {
			for _, it := range salesItems {
				itemid := it["item_org_assoc_fkid"].(string)
				itemID, _ := strconv.ParseInt(itemid, 10, 64)

				qty := it["quantity"].(string)
				quantity, _ := strconv.ParseFloat(qty, 64)

				itemDetails := ItemDetailsForOrder{
					OrgId:             Orgid,
					ItemId:            itemID,
					RequiredQuantity:  quantity,
					Location:          salesOrderLocation,
					BlockedBy:         3,
					ConsumedTableFkId: int64(SalesOrderFkId),
				}

				insertQueryStatus := BlockItemsBeforeConsumption(itemDetails)
				if insertQueryStatus == 0 {
					return 0
				}
			}

		} else { // UPDATE SALES ORDER STATUS TO 29
			statusChanged := ChangeSalesOrderStatus(SalesOrderFkId, Orgid, UserId, GetSalesOrderStatusCode(ApproveNoInventory), salesOrderLocation)
			if statusChanged == 0 {
				ormTransactionalErr = o.Rollback()
				return 0
			}
		}
	}

	//ON DISPATCH, CONSUME THE INVENTORY
	if order_status == 7 {
		// GET ALL THE ITEM STOCK ITEMS ASSIGNED FOR THE SALES ORDER BASED ON THE ID
		itemDetailsForOrder := ItemDetailsForOrder{
			OrgId:             Orgid,
			ConsumedTableFkId: int64(SalesOrderFkId),
			BlockedBy:         3,
			Location:          salesOrderLocation,
		}

		insertQueryStatus := ConsumeAllItemsOnOrderCompleted(itemDetailsForOrder)
		if insertQueryStatus == 0 {
			ormTransactionalErr = o.Rollback()
			return 0
		} else {
			//GET ALL SALES ORDER ITEMS BASED ON THE SALES ORDER ID AND UPDATE INVENTORY
			//Need to refactor this further
			var salesOrderItems []orm.Params
			count, error := o.Raw("SELECT sum(quantity) as requiredQty, item_org_assoc_fkid from sales_order_items where _orgid =?  and _sales_order_fkid=? group by item_org_assoc_fkid", Orgid, SalesOrderFkId).Values(&salesOrderItems)

			if count > 0 && error == nil {
				for _, item := range salesOrderItems {

					itemid := item["item_org_assoc_fkid"].(string)
					itemID, _ := strconv.ParseInt(itemid, 10, 64)

					qty := item["requiredQty"].(string)
					requiredQty, _ := strconv.ParseFloat(qty, 64)
					//UPDATE THE ITEM TOTAL INVENTORY
					itemInventoryData := ItemTotalInventory{
						OrgId:            Orgid,
						Itemid:           itemID,
						QuantityModified: requiredQty,
						Location:         salesOrderLocation,
						AddRemoveFlag:    0,
					}

					insertInvStatus := InsertUpdateItemTotalInventory(itemInventoryData)
					if insertInvStatus == 0 {
						ormTransactionalErr = o.Rollback()
						return 0
					}

				}
			}
		}
	}

	//UNBLOCK THE ITEMS FOR SALES ORDER
	if order_status == 4 || order_status == 90 {
		//GET THE ITEMS ASSOCIATED WITH THE SALES ORDER FROM SALES ORDER ITEMS
		salesItems := GetSalesOrderItemsByOrderId(SalesOrderFkId, Orgid)
		for _, it := range salesItems {

			itemid := it["item_org_assoc_fkid"].(string)
			itemID, _ := strconv.ParseInt(itemid, 10, 64)

			itemstockDetails := ItemStockTable{
				OrgId:             Orgid,
				Itemid:            itemID,
				Location:          salesOrderLocation,
				IsBlocked:         0,
				ConsumedTableFkid: int64(SalesOrderFkId),
				QuantityModified:  -1,
			}
			insertStatus := InsertUpdateItemStock(itemstockDetails)
			if insertStatus == 0 {
				ormTransactionalErr = o.Rollback()
				return 0
			}
		}
	}

	_, err := o.Raw("Update sales_order set status=? WHERE _id=? and _orgid=? ", order_status, SalesOrderFkId, Orgid).Exec()
	if err != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}

	_, err2 := o.Raw("INSERT INTO sales_order_status (_orgid, sales_order_fkid, actedon_by_fkid, date_actedon, status) VALUES (?,?,?,?,?) ", Orgid, SalesOrderFkId, UserId, time.Now(), order_status).Exec()
	if err2 != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}

	if ormTransactionalErr != nil {
		return 0
	}
	ormTransactionalErr = o.Commit()
	return 1
}

func GetSalesOrderInventoryDetailsBySalesOrderId(OrderNo string, Orgid string) map[string]orm.Params {

	//Once dispatched the inventory should be updated
	//Fetch all the sales_order_items_inventory details for the selected Sales Order
	o := orm.NewOrm()
	//ormTransactionalErr := o.Begin()
	var salesOrderDetails []orm.Params
	result := make(map[string]orm.Params)

	num, err1 := o.Raw("SELECT item_stock_fkid, quantity FROM sales_order_items_inventory WHERE sales_order_fkid= ? and _orgid=?", OrderNo, Orgid).Values(&salesOrderDetails)
	if err1 == nil && num > 0 {
		for k, v := range salesOrderDetails {
			key := "so_" + strconv.Itoa(k)
			result[key] = v
		}
	} else {
		result["status"] = nil
	}
	return result
}

func ProcessSalesOrderSendForProduction(salesOrder []map[string]string) {
	// o := orm.NewOrm()
	// ormTransactionalErr := o.Begin()
	// var i int

	// for i = 0; i < len(salesOrder); i++ {
	// 	_, err4 := o.Raw("INSERT into production_queue (_orgid,org_product_assoc_fkid,qty,uom,sales_order_fkid,status,last_updated_date,added_by_user_account_fkid,type) VALUES(?,?,?,?,?,?,?,?,?)", salesOrder[i].orgid, maps1[i]["product_org_assoc_fkid"], salesOrder[i]["quantity"], salesOrder[i]["uom"], maps1[i]["_id"], "1", time.Now(), salesOrder[i].userid, "0").Exec()
	// 	if err4 != nil {
	// 		ormTransactionalErr = o.Rollback()
	// 		return 0
	// 	}
	// 	var maps4 []orm.Params
	// 	_, err5 := o.Raw("SELECT _id FROM production_queue WHERE _orgid=? and sales_order_fkid=? ", formData.Orgid, maps1[i]["_id"]).Values(&maps4)
	// 	_, err6 := o.Raw("INSERT INTO production_queue_status (_orgid, production_queue_fkid, status, latest, updated_date,updated_by_user_account_fkid) VALUES (?,?,?,?,?,?)", formData.Orgid, maps4[0]["_id"], "1", "1", time.Now(), formData.Userid).Exec()
	// 	if err5 != nil || err6 != nil {
	// 		ormTransactionalErr = o.Rollback()
	// 		return 0
	// 	}
	// }
}
func GetCST(item_id string, orgid string) (string, string) {

	status := "false"

	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT CST FROM customer WHERE _id=? and _orgid=? ", item_id, orgid).Values(&maps)
	//fmt.Println(maps[0]["CST"])
	var new_cst string
	var s string
	if err == nil && num > 0 {
		s = maps[0]["CST"].(string)
		if s == "" {
			new_cst = "12.5"
		} else {
			new_cst = "2"
		}
		status = "true"
	}
	//fmt.Println("***Rate=", new_cst)
	return new_cst, status
}

func GetSalesItems(customer_id string, orgid string) ([]string, []string, []string, []string) {
	//status := "false"
	var item_id []string
	var invoice_narration []string
	var name []string
	var uom []string
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT ioa.invoice_narration, ioa.name, ioa.unit_measure, ioa._id FROM item_org_assoc as ioa, customer_product_assoc as cpa WHERE cpa._customer_fkid=? and ioa._orgid=? and cpa._orgid=? and ioa._id=cpa.item_org_assoc_fkid and cpa.status=1 and ioa.status=1", customer_id, orgid, orgid).Values(&maps)
	//fmt.Println("num=", num)
	var i int64 = 0
	for ; i < num; i++ {
		//fmt.Println(maps[i])
	}
	if err == nil {
		var new_id string
		var new_invoice_narration string
		var new_name string
		var new_uom string
		var j int64 = 0
		for ; j < num; j++ {
			new_invoice_narration = maps[j]["invoice_narration"].(string)
			invoice_narration = append(invoice_narration, new_invoice_narration)

			new_id = maps[j]["_id"].(string)
			item_id = append(item_id, new_id)

			new_name = maps[j]["name"].(string)
			name = append(name, new_name)

			new_uom = maps[j]["unit_measure"].(string)
			uom = append(uom, new_uom)

		}
		//status = "true"
	}
	//fmt.Println("***items=", invoice_narration)
	//fmt.Println("***quantity=", quantity)
	return item_id, invoice_narration, name, uom
}

func GetCustomerLocation(customer_id string, orgid string) ([]string, []string) {
	//status := "false"
	var location_id []string
	var address_line []string
	// var name []string
	// var uom []string
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT cwa._id, CONCAT(cwa.address_line1, ', ', cwa.wcity, ', ', cwa.wstate, ', ', cwa.wcountry, ', ', cwa.wpincode) AS address_string FROM customer_warehouse_address as cwa WHERE cwa.customer_fkid=? and cwa._orgid=? and cwa.status=1 ORDER BY type_address ASC", customer_id, orgid).Values(&maps)
	//fmt.Println("num=", num)
	var i int64 = 0
	for ; i < num; i++ {
		//fmt.Println(maps[i])
	}
	if num > 0 && err == nil {
		var new_id string
		var new_address string
		// var new_name string
		// var new_uom string
		var j int64 = 0
		for ; j < num; j++ {
			new_id = maps[j]["_id"].(string)
			location_id = append(location_id, new_id)

			new_address = maps[j]["address_string"].(string)
			address_line = append(address_line, new_address)

		}
		//status = "true"
	}
	//fmt.Println("***locations=", address_line)
	//fmt.Println("***quantity=", quantity)
	return location_id, address_line
}

func GetTransporterVehicles(transporter_id string, orgid string) ([]string, []string, []string) {
	//status := "false"
	var vehicle_id []string
	var driver []string
	var number []string
	// var uom []string
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT * FROM vehicle WHERE transporter_fkid=? and _orgid=?", transporter_id, orgid).Values(&maps)
	//fmt.Println("num=", num)
	var i int64 = 0
	for ; i < num; i++ {
		//fmt.Println(maps[i])
	}
	if num > 0 && err == nil {
		var new_id string
		var new_driver string
		var new_number string
		// var new_uom string
		var j int64 = 0
		for ; j < num; j++ {
			new_id = maps[j]["_id"].(string)
			vehicle_id = append(vehicle_id, new_id)

			new_driver = maps[j]["driver_name"].(string)
			driver = append(driver, new_driver)

			new_number = maps[j]["registration_number"].(string)
			number = append(number, new_number)

		}
		//status = "true"
	}
	//fmt.Println("***locations=", address_line)
	//fmt.Println("***quantity=", quantity)
	return vehicle_id, driver, number
}

func GetTransporterWarehouses(transporter_id string, orgid string) ([]string, []string) {
	//status := "false"
	var billing_address []string
	var warehouse_id []string

	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT * FROM warehouse WHERE transporter_fkid=? and _orgid=?", transporter_id, orgid).Values(&maps)
	// var i int64 = 0
	// for ; i < num; i++ {
	// 	//fmt.Println(maps[i])
	// }
	if num > 0 && err == nil {
		var new_id string
		var new_address string

		var j int64 = 0
		for ; j < num; j++ {
			new_id = maps[j]["_id"].(string)
			warehouse_id = append(warehouse_id, new_id)

			city := maps[j]["city"].(string)
			state := maps[j]["state"].(string)
			country := maps[j]["country"].(string)
			pincode := maps[j]["pincode"].(string)
			billingAddress := maps[j]["billing_address"].(string)

			new_address = billingAddress + ", " + city + ", " + state + ", " + country + ", " + pincode
			billing_address = append(billing_address, new_address)

		}
		//status = "true"
	}

	return billing_address, warehouse_id
}

func GetSOtc(_orgId string) string {
	var result string
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT content FROM data_master WHERE _orgid=? and type=? and status=1 ", _orgId, 2).Values(&maps)
	//fmt.Println("****code:",maps[0]["order_no"])
	if err == nil {
		if num > 0 && maps[0]["content"] != nil {
			s := maps[0]["content"].(string)
			result = s
		} else {
			s := " "
			result = s
		}
	} else {
		//fmt.Println("Error in code query")
	}

	return result
}

func GetOrgDetails(_orgId string) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT * FROM organisation WHERE orgid=? and status=1 ", _orgId).Values(&maps)
	if err == nil && num > 0 {
		for k, v := range maps {

			key := "org_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetOrgFactories(_orgId string) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT * FROM organisation_factories WHERE _orgid=? and status=1 ", _orgId).Values(&maps)
	if err == nil && num > 0 {
		for k, v := range maps {

			key := "org_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func SearchSOByOrderDetails(CustomerName string, FromOrderDate string, ToOrderDate string, _orgId string) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	var err error
	var num int64
	if (FromOrderDate == "") && (ToOrderDate == "") {
		num, err = o.Raw("SELECT * FROM (SELECT so._orgid, so._id,so.order_no, so.order_date as o_date, c.company_name, so.job_type,so.delivery_date as d_date, so.delivery_location, so.origin_location, so.payment_terms, so.remarks, so.total_sales_price, so.status, so.total_price_after_taxes, so.tariff_no, so.billing_address, so.total_taxable_value, so.total_tax_cess, so.total_freight_packaging_others FROM sales_order as so,customer as c WHERE  so._orgid=? and c._orgid=? and so.customer_fkid=c._id ) AS sod WHERE sod._orgid=? and company_name like ? ", _orgId, _orgId, _orgId, "%"+CustomerName+"%").Values(&maps)
	}
	if (ToOrderDate == "") && (FromOrderDate != "") {
		num, err = o.Raw("SELECT * FROM (SELECT so._orgid, so._id,so.order_no, so.order_date as o_date, c.company_name, so.job_type, so.delivery_date as d_date, so.delivery_location, so.origin_location, so.payment_terms, so.remarks, so.total_sales_price, so.status, so.total_price_after_taxes, so.tariff_no, so.billing_address, so.total_taxable_value, so.total_tax_cess, so.total_freight_packaging_others FROM sales_order as so,customer as c WHERE  so._orgid=? and c._orgid=? and so.customer_fkid=c._id ) AS sod WHERE sod._orgid=? and(company_name like ? and o_date >= ?)", _orgId, _orgId, _orgId, "%"+CustomerName+"%", FromOrderDate).Values(&maps)
	}
	if (FromOrderDate == "") && (ToOrderDate != "") {
		num, err = o.Raw("SELECT * FROM (SELECT so._orgid, so._id,so.order_no, so.order_date as o_date, c.company_name, so.job_type,so.delivery_date as d_date, so.delivery_location, so.origin_location, so.payment_terms, so.remarks, so.total_sales_price, so.status, so.total_price_after_taxes, so.tariff_no, so.billing_address, so.total_taxable_value, so.total_tax_cess, so.total_freight_packaging_others FROM sales_order as so,customer as c WHERE  so._orgid=? and c._orgid=? and so.customer_fkid=c._id ) AS sod WHERE sod._orgid=? and(company_name like ? and o_date <= ?)", _orgId, _orgId, _orgId, "%"+CustomerName+"%", ToOrderDate).Values(&maps)
	}
	if (FromOrderDate != "") && (ToOrderDate != "") {
		num, err = o.Raw("SELECT * FROM (SELECT so._orgid, so._id,so.order_no, so.order_date as o_date, c.company_name, so.job_type,so.delivery_date as d_date, so.delivery_location, so.origin_location, so.payment_terms, so.remarks, so.total_sales_price, so.status, so.total_price_after_taxes, so.tariff_no, so.billing_address, so.total_taxable_value, so.total_tax_cess, so.total_freight_packaging_others FROM sales_order as so,customer as c WHERE  so._orgid=? and c._orgid=? and so.customer_fkid=c._id ) AS sod WHERE sod._orgid=? and(company_name like ? and (o_date between ? and ?))", _orgId, _orgId, _orgId, "%"+CustomerName+"%", FromOrderDate, ToOrderDate).Values(&maps)
	}
	if err == nil && num > 0 {
		var i int64 = 0
		for ; i < num; i++ {
			if maps[i]["delivery_location"] != nil {
				var maps1 []orm.Params
				count, err1 := o.Raw("SELECT CONCAT(cwa.address_line1, ', ', cwa.wcity, ', ', cwa.wstate, ', ', cwa.wcountry, ', ', cwa.wpincode) AS delivery_location_string FROM customer_warehouse_address as cwa WHERE cwa._orgid=? and cwa._id=?", _orgId, maps[i]["delivery_location"]).Values(&maps1)
				if err1 == nil && count > 0 {
					maps[i]["delivery_location_string"] = maps1[0]["delivery_location_string"]
				} else {
					maps[i]["delivery_location_string"] = ""
				}
			} else {
				maps[i]["delivery_location_string"] = ""
			}

			if maps[i]["origin_location"] != nil {
				var maps1 []orm.Params
				origin_count, err01 := o.Raw("SELECT of.factory_location FROM organisation_factories as of WHERE of._orgid=? and of._id=?", _orgId, maps[i]["origin_location"]).Values(&maps1)
				if err01 == nil && origin_count > 0 {
					maps[i]["factory_location"] = maps1[0]["factory_location"]
				} else {
					maps[i]["factory_location"] = ""
				}
			} else {
				maps[i]["factory_location"] = ""
			}

			if maps[i]["billing_address"] != nil {
				var maps2 []orm.Params
				nos, err2 := o.Raw("SELECT CONCAT(cwa.address_line1, ', ', cwa.wcity, ', ', cwa.wstate, ', ', cwa.wcountry, ', ', cwa.wpincode) AS billing_address_string FROM customer_warehouse_address as cwa WHERE cwa._orgid=? and cwa._id=?", _orgId, maps[i]["billing_address"]).Values(&maps2)
				if err2 == nil && nos > 0 {
					maps[i]["billing_address_string"] = maps2[0]["billing_address_string"]
				} else {
					maps[i]["billing_address_string"] = ""
				}
			} else {
				maps[i]["billing_address_string"] = ""
			}
		}
		for k, v := range maps {
			if maps[k]["o_date"] != nil && maps[k]["o_date"] != "" {
				maps[k]["o_date"] = convertDateString(maps[k]["o_date"].(string))
			}
			if maps[k]["d_date"] != nil && maps[k]["d_date"] != "" {
				maps[k]["d_date"] = convertDateString(maps[k]["d_date"].(string))
			}

			var transMaps []orm.Params
			transNum, transErr := o.Raw("SELECT * FROM sales_order_items WHERE _orgid=? and _sales_order_fkid=? and status=1", _orgId, maps[k]["_id"]).Values(&transMaps)
			if transNum > 0 && transErr == nil {
				if transMaps[0]["transporter_fkid"] == nil {
					maps[k]["transporter_fkid"] = ""
				} else {
					maps[k]["transporter_fkid"] = transMaps[0]["transporter_fkid"].(string)
				}
				if transMaps[0]["vehicle_fkid"] == nil {
					maps[k]["vehicle_fkid"] = ""
				} else {
					maps[k]["vehicle_fkid"] = transMaps[0]["vehicle_fkid"].(string)
				}
			} else {
				maps[k]["vehicle_fkid"] = ""
				maps[k]["vehicle_fkid"] = ""
			}

			if maps[k]["transporter_fkid"] != nil && maps[k]["transporter_fkid"] != "" {
				var transportermaps []orm.Params
				transporternum, transportererr := o.Raw("SELECT * FROM transporter WHERE _id=? and _orgid=? ", maps[k]["transporter_fkid"], _orgId).Values(&transportermaps)
				if transporternum > 0 && transportererr == nil {
					maps[k]["transporter_name"] = transportermaps[0]["transporter_name"]
				}
			}

			if maps[k]["vehicle_fkid"] != nil && maps[k]["vehicle_fkid"] != "" {
				var vehiclemaps []orm.Params
				vehiclenum, vehicleerr := o.Raw("SELECT * FROM transporter WHERE _id=? and _orgid=? ", maps[k]["vehicle_fkid"], _orgId).Values(&vehiclemaps)
				if vehiclenum > 0 && vehicleerr == nil {
					maps[k]["registration_number"] = vehiclemaps[0]["registration_number"]
				}
			}
			key := "sales_" + strconv.Itoa(k)
			result[key] = v
		}
	} else {
		result["status"] = nil
	}
	return result
}

//Temporary added here. This function is same as stock.go ConsumeInventoryByItemsMapProductionOrders
func ConsumeInventoryBySalesOrders(itemsMaps map[string]float64, orgid string, location string, usedfor string, usedtablefkid string, usedtableCode string, productPkId string) int {
	var queryResultMap []orm.Params
	o := orm.NewOrm()
	ormTransactionalErr := o.Begin()

	//map[pkid][quantity]
	for itemPKID, quantityToConsume := range itemsMaps {
		num, err := o.Raw("select its._id, its._orgid, its.item_org_assoc_fkid, its.expires_by, its.quantity, its.start_date, its.end_date, its.rate, its.currency, its.geo_location, its.last_updated, its.latest, its.po_so_items_with_grn_fkid, its.items_stock_adjustment_fkid FROM item_stock as its, item_org_assoc as ioa, product_bom as pb WHERE item_org_assoc_fkid = ? and _orgid = ? and its.item_org_assoc_fkid = ioa._id and pb.item_product_org_assoc_fkid=? and pb.item_org_assoc_fkid=? and pb.non_consumable =0 and pb.status=1 and its.quantity > 0 and its.geo_location=? and its.latest =1 ORDER BY its.start_date asc  ", itemPKID, orgid, productPkId, itemPKID, location).Values(&queryResultMap)
		var i int64 = 0
		for ; i < num; i++ {
			//fmt.Println(queryResultMap[i])
		}
		if err == nil && num > 0 {
			for quantityToConsume > 0.0 {
				for count := int64(0); count < num; count++ {

					if queryResultMap[count]["end_date"] == nil {

						queryResultMap[count]["end_date"] = nil
					}
					if queryResultMap[count]["po_so_items_with_grn_fkid"] == nil {

						queryResultMap[count]["po_so_items_with_grn_fkid"] = nil
					}
					if queryResultMap[count]["items_stock_adjustment_fkid"] == nil {

						queryResultMap[count]["items_stock_adjustment_fkid"] = nil
					}
					rq := queryResultMap[count]["quantity"].(string)
					recordedQuantity, _ := strconv.ParseFloat(rq, 64)
					sid := queryResultMap[count]["_id"].(string)
					stockPKID, _ := strconv.ParseFloat(sid, 64)
					if quantityToConsume >= recordedQuantity {
						_, err := o.Raw("UPDATE item_stock set latest = 0 and end_date = ? where _id = ? and _orgid = ?", time.Now(), stockPKID, orgid).Exec()
						if err != nil {
							ormTransactionalErr = o.Rollback()
							return 0
						}
						_, err1 := o.Raw("INSERT into item_stock (_orgid,item_org_assoc_fkid,expires_by,quantity,start_date,end_date,rate,currency,geo_location,last_updated,latest,po_so_items_with_grn_fkid,items_stock_adjustment_fkid) values (?,?,?,?,?,?,?,?,?,?,?,?,?)", queryResultMap[count]["_orgid"], queryResultMap[count]["item_org_assoc_fkid"], queryResultMap[count]["expires_by"], 0.0, queryResultMap[count]["start_date"], queryResultMap[count]["end_date"], queryResultMap[count]["rate"], queryResultMap[count]["currency"], queryResultMap[count]["geo_location"], time.Now(), 1, queryResultMap[count]["po_so_items_with_grn_fkid"], queryResultMap[count]["items_stock_adjustment_fkid"]).Exec()
						if err1 != nil {
							ormTransactionalErr = o.Rollback()
							return 0
						}

						//insert recordedQuantity into item_stock_usage
						_, err2 := o.Raw("INSERT into item_stock_usage (_orgid, item_org_assoc_fkid, item_stock_fkid, quantity_used, used_for, used_table_fkid, used_table_unique_code, used_on, status) values (?,?,?,?,?,?,?,?,?)", queryResultMap[count]["_orgid"], queryResultMap[count]["item_org_assoc_fkid"], stockPKID, recordedQuantity, usedfor, usedtablefkid, usedtableCode, time.Now(), 1).Exec()
						if err2 != nil {
							ormTransactionalErr = o.Rollback()
							return 0
						}

						quantityToConsume = quantityToConsume - recordedQuantity
					} else {
						_, err := o.Raw("UPDATE item_stock set latest = 0 where _id = ? and _orgid = ?", stockPKID, orgid).Exec()
						if err != nil {
							ormTransactionalErr = o.Rollback()
							return 0
						}
						_, err2 := o.Raw("INSERT into item_stock (_orgid,item_org_assoc_fkid,expires_by,quantity,start_date,rate,currency,geo_location,last_updated,latest,po_so_items_with_grn_fkid,items_stock_adjustment_fkid) values (?,?,?,?,?,?,?,?,?,?,?,?)", queryResultMap[count]["_orgid"], queryResultMap[count]["item_org_assoc_fkid"], queryResultMap[count]["expires_by"], recordedQuantity-quantityToConsume, queryResultMap[count]["start_date"], queryResultMap[count]["rate"], queryResultMap[count]["currency"], queryResultMap[count]["geo_location"], time.Now(), 1, queryResultMap[count]["po_so_items_with_grn_fkid"], queryResultMap[count]["items_stock_adjustment_fkid"]).Exec()
						if err2 != nil {
							ormTransactionalErr = o.Rollback()
							return 0
						}

						//insert quantityToConsume into item_stock_usage
						_, err3 := o.Raw("INSERT into item_stock_usage (_orgid, item_org_assoc_fkid, item_stock_fkid, quantity_used, used_for, used_table_fkid, used_table_unique_code, used_on, status) values (?,?,?,?,?,?,?,?,?)", queryResultMap[count]["_orgid"], queryResultMap[count]["item_org_assoc_fkid"], stockPKID, quantityToConsume, usedfor, usedtablefkid, usedtableCode, time.Now(), 1).Exec()
						if err3 != nil {
							ormTransactionalErr = o.Rollback()
							return 0
						}
						quantityToConsume = 0.0
					}
				}
			}
		}
	}
	ormTransactionalErr = o.Commit()
	if ormTransactionalErr != nil {
		return 0
	}
	return 1
}

//Temporary added here. This function is same as stock.go ItemTotalInventoryUpdateByPkidProductionOrders
func ItemTotalInventoryUpdateBySalesOrders(itemid string, orgid string, location string, totalconsumption float64) int {

	o := orm.NewOrm()
	ormTransactionalErr := o.Begin()

	var maps []orm.Params
	var maps1 []orm.Params
	var maps2 []orm.Params
	var maps3 []orm.Params
	var maps4 []orm.Params
	var maps5 []orm.Params
	var t string
	var m string
	var val string
	var low_level string
	var itemcounttobeupdated float64
	var orgItemStockQty float64

	_, err := o.Raw("SELECT SUM(quantity) AS Total FROM item_stock WHERE _orgid=? and item_org_assoc_fkid=? and latest = 1 and geo_location=?", orgid, itemid, location).Values(&maps)
	_, err1 := o.Raw("SELECT min_stock_trigger FROM item_org_assoc WHERE _orgid=? and _id=?", orgid, itemid).Values(&maps1)
	_, err2 := o.Raw("SELECT geo_location FROM item_org_assoc WHERE item_org_assoc._orgid=? and _id=?", orgid, itemid).Values(&maps2)
	_, err3 := o.Raw("SELECT _id FROM organisation_factories WHERE _orgid=? and factory_location=?", orgid, maps2[0]["geo_location"]).Values(&maps3)
	t = maps[0]["Total"].(string)
	m = maps1[0]["min_stock_trigger"].(string)
	total_inventory, _ := strconv.ParseFloat(t, 64)
	min_trigger, _ := strconv.ParseFloat(m, 64)

	if total_inventory >= min_trigger {
		low_level = "0"
	} else {
		low_level = "1"
	}

	itemcounttobeupdated = total_inventory - totalconsumption

	//update item_total_inventory table

	_, err6 := o.Raw("SELECT _id FROM item_total_inventory WHERE _orgid=? and item_org_assoc_fkid=? and status=1 and geo_location=?", orgid, itemid, location).Values(&maps4)
	_, err5 := o.Raw("UPDATE item_total_inventory set status = 0 where _id = ? and _orgid = ?", maps4[0]["_id"], orgid).Exec()
	if err5 != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}
	_, err4 := o.Raw("INSERT into item_total_inventory (_orgid,item_org_assoc_fkid,total_quantity,last_updated,geo_location,status,low_level) values (?,?,?,?,?,?,?)", orgid, itemid, itemcounttobeupdated, time.Now(), location, "1", low_level).Exec()
	if err4 != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}

	//update item_stock table

	_, err7 := o.Raw("select * from item_stock WHERE _orgid=? and item_org_assoc_fkid=? and latest=1 and geo_location=? order by last_updated asc limit 1", orgid, itemid, location).Values(&maps5)

	_, err8 := o.Raw("UPDATE item_stock SET latest = 0 WHERE _id =?", maps5[0]["_id"]).Exec()
	if err8 != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}

	val = maps5[0]["quantity"].(string)
	qtyavailable, _ := strconv.ParseFloat(val, 64)

	orgItemStockQty = qtyavailable - totalconsumption

	if orgItemStockQty > -1 {
		_, err9 := o.Raw("INSERT INTO item_stock (_orgid, item_org_assoc_fkid, quantity, rate, currency, geo_location, latest,start_date,last_updated,expires_by) VALUES (?,?,?,?,?,?,?,?,?,?) ", orgid, itemid, orgItemStockQty, maps5[0]["rate"], maps5[0]["currency"], location, "1", time.Now(), time.Now(), time.Now()).Exec()
		if err9 != nil {
			ormTransactionalErr = o.Rollback()
			return 0
		}
	}

	if err == nil && err1 == nil && err2 == nil && err3 == nil && err5 == nil && err4 == nil && err6 == nil && err7 == nil && err8 == nil {

	} else {
		ormTransactionalErr = o.Rollback()
		return 0
	}
	ormTransactionalErr = o.Commit()
	if ormTransactionalErr != nil {
		return 0
	}
	return 1
}

func CheckForQty(item_id string, origin_id string, orgid string) string {
	o := orm.NewOrm()
	var err error
	var new_available_qty string
	var maps3 []orm.Params
	if err == nil {

		qty_num, err3 := o.Raw("SELECT iti.total_quantity FROM item_total_inventory as iti WHERE iti.item_org_assoc_fkid=? and iti._orgid=? and iti.geo_location=? and iti.status=1", item_id, orgid, origin_id).Values(&maps3)
		if err3 == nil && qty_num > 0 {
			if maps3[0]["total_quantity"] == nil {
				new_available_qty = "0.0"
			} else {
				new_available_qty = maps3[0]["total_quantity"].(string)
			}
		} else {
			new_available_qty = "0.0"
		}
	} else {

	}
	return new_available_qty
}

func GetSOdetailsByPkId(soId string, orgId string) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT so._id as order_id, so.*, c.* FROM sales_order as so, customer as c WHERE so._orgid=? and so._id=? and c._orgid=? and c._id=so.customer_fkid", orgId, soId, orgId).Values(&maps)
	if err == nil && num > 0 {
		for k, v := range maps {
			if maps[k]["order_date"] != nil && maps[k]["order_date"] != "" {
				maps[k]["o_date"] = convertDateString(maps[k]["order_date"].(string))
			}
			if maps[k]["delivery_date"] != nil && maps[k]["delivery_date"] != "" {
				maps[k]["d_date"] = convertDateString(maps[k]["delivery_date"].(string))
			}
			var soItemsMaps []orm.Params
			count, err1 := o.Raw("SELECT ioa._id as item_id, soi.*, ioa.* From sales_order_items as soi, item_org_assoc as ioa WHERE soi._orgid=? and soi._sales_order_fkid=? and soi.status=1 and ioa._id=soi.item_org_assoc_fkid and ioa._orgid=?", orgId, soId, orgId).Values(&soItemsMaps)
			if count > 0 && err1 == nil {
				for l, _ := range soItemsMaps {
					//maps[l]["no"] = l
					//fmt.Println(soItemsMaps[l]["delivered_quantity"])
					if soItemsMaps[l]["delivered_quantity"] == nil || maps[l]["delivered_quantity"] == "" {
						soItemsMaps[l]["editable"] = ""
						//fmt.Println("****equal")
					} else {
						deliveredQty, _ := strconv.ParseFloat(soItemsMaps[l]["delivered_quantity"].(string), 64)
						//fmt.Println("****", deliveredQty)
						if deliveredQty == 0 {
							soItemsMaps[l]["editable"] = ""
						} else {
							soItemsMaps[l]["editable"] = "readonly"
						}
					}
				}
				maps[k]["soitems"] = soItemsMaps
			}

			var originLocMaps []orm.Params
			origincount, err2 := o.Raw("SELECT * From organisation_factories WHERE _orgid=? and _id=?", orgId, maps[0]["origin_location"]).Values(&originLocMaps)
			if origincount > 0 && err2 == nil {
				maps[k]["origin_factory"] = originLocMaps[0]["factory_location"]
			}

			var deliveryLocMaps []orm.Params
			deliverycount, err3 := o.Raw("SELECT * From organisation_factories WHERE _orgid=? and _id=?", orgId, maps[0]["delivery_location"]).Values(&deliveryLocMaps)
			if deliverycount > 0 && err3 == nil {
				maps[k]["delivery_factory"] = deliveryLocMaps[0]["factory_location"]
			}

			if maps[k]["billing_address"] != nil {
				var maps2 []orm.Params
				nos, err2 := o.Raw("SELECT cwa.wstate AS billing_state, cwa.wpincode AS billing_pincode, CONCAT(cwa.address_line1, ', ', cwa.wcity, ', ', cwa.wstate, ', ', cwa.wcountry, ', ', cwa.wpincode) AS billing_address_string FROM customer_warehouse_address as cwa WHERE cwa._orgid=? and cwa._id=?", orgId, maps[k]["billing_address"]).Values(&maps2)
				if err2 == nil && nos > 0 {
					maps[k]["billing_address_string"] = maps2[0]["billing_address_string"]
					maps[k]["billing_state"] = maps2[0]["billing_state"]
					maps[k]["billing_pincode"] = maps2[0]["billing_pincode"]
				} else {
					maps[k]["billing_address_string"] = ""
					maps[k]["billing_state"] = ""
					maps[k]["billing_pincode"] = ""
				}
			} else {
				maps[k]["billing_address_string"] = ""
				maps[k]["billing_state"] = ""
				maps[k]["billing_pincode"] = ""
			}

			if maps[k]["transporter_fkid"] != nil && maps[k]["transporter_fkid"] != "" {
				var transportermaps []orm.Params
				transporternum, transportererr := o.Raw("SELECT * FROM transporter WHERE _id=? and _orgid=? ", maps[k]["transporter_fkid"], orgId).Values(&transportermaps)
				if transporternum > 0 && transportererr == nil {
					maps[k]["transporter_name"] = transportermaps[0]["transporter_name"]
				}
			}

			if maps[k]["vehicle_fkid"] != nil && maps[k]["vehicle_fkid"] != "" {
				var vehiclemaps []orm.Params
				vehiclenum, vehicleerr := o.Raw("SELECT * FROM vehicle WHERE _id=? and _orgid=? ", maps[k]["vehicle_fkid"], orgId).Values(&vehiclemaps)
				if vehiclenum > 0 && vehicleerr == nil {
					maps[k]["registration_number"] = vehiclemaps[0]["registration_number"]
				}
			}

			key := "so_" + strconv.Itoa(k)
			result[key] = v
		}
	} else {
		result["status"] = nil
	}
	return result
}

func CheckInventoryStatusForAllItems(salesitems []map[string]string, oLocation string, Orgid string) string {
	isInventoryOk := "0"

	for _, it := range salesitems {

		itemid := it["item_name"]
		itemID, _ := strconv.ParseInt(itemid, 10, 64)
		reqQty, _ := strconv.ParseFloat(it["qty"], 64)

		itemStockData := ItemStockTable{
			OrgId:    Orgid,
			Itemid:   itemID,
			Location: oLocation,
		}

		existing_qty := GetExistingUnblockedTotalQtyFromItemStock(itemStockData)

		if existing_qty < reqQty {
			isInventoryOk = "0" // No inventory returns 0
			break
		} else {
			isInventoryOk = "1" // Inventory present returns 1
		}

	}

	return isInventoryOk
}

func GetSalesOrderFormDetails(SalesOrderNo string, _orgId string) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT so._id, so.sales_order_code_gen, so.order_date as o_date, so.customer_fkid, so.job_type, so.delivery_date as d_date, so.delivery_location, so.origin_location, so.tax_regime, so.payment_terms, so.remarks, so.total_quantity, so.total_price, so.total_sales_price, so.total_price_after_taxes, so.tariff_no, so.billing_address, so.total_taxable_value, so.total_tax_cess, so.total_freight_packaging_others, so.order_no, c.company_name FROM sales_order as so, customer as c WHERE so._orgid=? and so.order_no=? and c._orgid=? and c._id=so.customer_fkid ", _orgId, SalesOrderNo, _orgId).Values(&maps)
	if err == nil && num > 0 {
		for k, v := range maps {

			var maps1 []orm.Params
			maps[k]["origin_factory_name"] = ""
			if maps[k]["origin_location"] != nil {
				origin_count, err1 := o.Raw("SELECT factory_location FROM organisation_factories WHERE _orgid=? and _id=?", _orgId, maps[0]["origin_location"]).Values(&maps1)
				if origin_count > 0 && err1 == nil {
					maps[k]["origin_factory_name"] = maps1[0]["factory_location"]
				}
			}

			var maps2 []orm.Params
			maps[k]["delivery_factory_name"] = ""
			if maps[k]["delivery_location"] != nil {
				delivery_count, err2 := o.Raw("SELECT CONCAT(cwa.address_line1, ', ', cwa.wcity, ', ', cwa.wstate, ', ', cwa.wcountry, ', ', cwa.wpincode) AS factory_location FROM customer_warehouse_address as cwa WHERE cwa._orgid=? and cwa._id=?", _orgId, maps[0]["delivery_location"]).Values(&maps2)
				if delivery_count > 0 && err2 == nil {
					maps[k]["delivery_factory_name"] = maps2[0]["factory_location"]
				}
			}

			var maps3 []orm.Params
			maps[k]["billing_factory_name"] = ""
			if maps[k]["billing_address"] != nil {
				billing_count, err3 := o.Raw("SELECT factory_location FROM organisation_factories WHERE _orgid=? and _id=?", _orgId, maps[0]["billing_address"]).Values(&maps3)
				if billing_count > 0 && err3 == nil {
					maps[k]["billing_factory_name"] = maps3[0]["factory_location"]
				}
			}
			if maps[k]["o_date"] != nil && maps[k]["o_date"] != "" {
				maps[k]["o_date"] = convertDateString(maps[k]["o_date"].(string))
			}
			if maps[k]["d_date"] != nil && maps[k]["d_date"] != "" {
				maps[k]["d_date"] = convertDateString(maps[k]["d_date"].(string))
			}
			key := "sales_" + strconv.Itoa(k)
			result[key] = v
		}
	} else {
		result["status"] = nil
	}
	return result
}

func GetSalesOrderItems(SalesId string, orgId string) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT ioa._id, ioa.item_unique_code, ioa.item_code_gen, ioa.invoice_narration, soi.quantity, ioa.unit_measure, ioa.status, soi.geo_location FROM item_org_assoc as ioa, sales_order_items as soi where ioa._orgid=? and soi._orgid=? and soi.status=1 and soi.item_org_assoc_fkid=ioa._id and soi._sales_order_fkid=?", orgId, orgId, SalesId).Values(&maps)

	if err == nil && num > 0 {
		for k, v := range maps {
			var batchmaps []orm.Params
			count, err1 := o.Raw("SELECT _id as batchId, batch_no FROM item_stock where blocked = 0 and _orgid=? and item_org_assoc_fkid=? and geo_location= ? and latest = 1", orgId, maps[k]["_id"], maps[k]["geo_location"]).Values(&batchmaps)
			if count > 0 && err1 == nil {
				maps[k]["batches"] = batchmaps
			}

			key := "items_" + strconv.Itoa(k)
			result[key] = v
		}
	} else {
		result["status"] = nil
	}
	return result
}

func GetSalesOrderFkIDFromOrgidOrderNo(orgId string, orderNo string) int {
	var id int
	id = 0
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT _id FROM sales_order WHERE _orgid=? and order_no=? ", orgId, orderNo).Values(&maps)

	if num > 0 && err == nil {
		if maps[0]["_id"] != nil {
			code := maps[0]["_id"].(string)
			getValue, _ := strconv.ParseInt(code, 10, 64)
			id = int(getValue)
		}
	}
	return id
}

func GetSalesOrderLocationFromOrgidOrderNo(orgId string, orderNo string) int {
	var origin_location int
	origin_location = 0
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT origin_location FROM sales_order WHERE _orgid=? and order_no=? ", orgId, orderNo).Values(&maps)

	if num > 0 && err == nil {
		if maps[0]["origin_location"] != nil {
			code := maps[0]["origin_location"].(string)
			getValue, _ := strconv.ParseInt(code, 10, 64)
			origin_location = int(getValue)
		}
	}
	return origin_location
}

func GetStatusOfSalesOrder(salesOrderFkid string, orgId string) (int, int) {
	o := orm.NewOrm()

	var status int64
	var isBatchAssigned int64
	status = 0
	isBatchAssigned = 0

	var maps []orm.Params
	num, err := o.Raw("SELECT status, manually_assign_batch FROM sales_order where _orgid=? and _id=?", orgId, salesOrderFkid).Values(&maps)
	if err == nil && num > 0 {

		if maps[0]["status"] != nil {
			s := maps[0]["status"].(string)
			status, _ = strconv.ParseInt(s, 10, 64)
		} else {
			status = 0
		}

		if maps[0]["manually_assign_batch"] != nil {
			batchAssigned := maps[0]["manually_assign_batch"].(string)
			isBatchAssigned, _ = strconv.ParseInt(batchAssigned, 10, 64)
		} else {
			isBatchAssigned = 0
		}

		return int(status), int(isBatchAssigned)
	}
	return int(status), int(isBatchAssigned)

}

func GetSalesOrderDetailsForDelivery(formData DeliveryData) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	//num, err := o.Raw("SELECT * FROM item_master ").Values(&maps)

	num, err := o.Raw("SELECT distinct(sales_order_code_gen), company_name FROM sales_order, customer WHERE sales_order._orgid=? and customer._orgid=? and customer._id=sales_order.customer_fkid and sales_order.status=2", formData.Orgid, formData.Orgid).Values(&maps)

	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		var i int64 = 0
		for ; i < num; i++ {
			//fmt.Println(maps[i])
		}

		for k, v := range maps {

			key := "delivery_" + strconv.Itoa(k)

			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetSOItemsBySoNo(formData DeliveryData) map[string]orm.Params {

	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	//var maps1 []orm.Params
	// count, err1 := o.Raw("SELECT pgrn._id FROM purchase_service_order as pso, po_so_items_with_grn as pgrn WHERE pgrn.purchase_service_order_fkid=pso._id and pso.unique_serial_no=? and pso._orgid=? and pgrn.purchase_service_order_fkid=pso._id and pgrn._orgid=?", formData.PoNo, formData.Orgid, formData.Orgid).Values(&maps1)
	// if err1 == nil {
	// 	if count <= 0 {
	//When no GRN is raised
	var maps []orm.Params
	num, err := o.Raw("SELECT io._id as item_id, io.invoice_narration, io.item_unique_code, soi.quantity, soi.delivered_quantity, soi.uom, so._id as sales_id, so.origin_location FROM item_org_assoc as io, sales_order as so, sales_order_items as soi WHERE io._orgid=? and so._orgid=? and soi._orgid=? and soi._sales_order_fkid=so._id and soi.item_org_assoc_fkid=io._id and soi.status=? and so.sales_order_code_gen=?", formData.Orgid, formData.Orgid, formData.Orgid, "1", formData.SoNo).Values(&maps)
	if err == nil && num > 0 {
		for k, v := range maps {
			//fmt.Println("Value k ",k)
			var orderedQty string
			var d string
			var QtyRemaining float64
			orderedQty = maps[k]["quantity"].(string)
			if maps[k]["delivered_quantity"] != nil && maps[k]["delivered_quantity"] != "" {
				d = maps[k]["delivered_quantity"].(string)
			} else {
				d = "0"
			}
			QtyOrdered, _ := strconv.ParseFloat(orderedQty, 64)
			QtyDelivered, _ := strconv.ParseFloat(d, 64)
			QtyRemaining = QtyOrdered - QtyDelivered
			maps[k]["qremaining"] = QtyRemaining

			maps[k]["Number"] = k

			var batchMaps []orm.Params
			batchNum, batchErr := o.Raw("SELECT _id as item_stock_id, batch_no, quantity as quantity_by_batch FROM item_stock as ist WHERE ist._orgid=? and ist.item_org_assoc_fkid=? and ist.geo_location=? and ist.consumed_table_fkid=? and ist.latest=1", formData.Orgid, maps[0]["item_id"], maps[0]["origin_location"], maps[0]["sales_id"]).Values(&batchMaps)
			if batchNum < 1 {
				batchNum, batchErr = o.Raw("SELECT _id as item_stock_id, batch_no, quantity as quantity_by_batch FROM item_stock as ist WHERE ist._orgid=? and ist.item_org_assoc_fkid=? and ist.geo_location=? and ist.latest=1 and ist.blocked=0", formData.Orgid, maps[0]["item_id"], maps[0]["origin_location"]).Values(&batchMaps)
			}
			if batchNum > 0 && batchErr == nil {
				maps[k]["Batches"] = batchMaps
			}
			key := "gi_" + strconv.Itoa(k)
			result[key] = v
		}
	} else {
		result["status"] = nil
	}

	return result
}

func GetSODetailsBySoNo(formData DeliveryData) map[string]orm.Params {

	result := make(map[string]orm.Params)
	o := orm.NewOrm()

	var maps []orm.Params
	num, err := o.Raw("SELECT so._id, so.total_sales_price, so.job_type, so.order_date, so.delivery_location, orgf.factory_location, so.customer_fkid, so.transporter_fkid, so.vehicle_fkid, customer.company_name FROM sales_order as so, customer, organisation_factories as orgf WHERE so._orgid=? and so.sales_order_code_gen=? and customer._orgid=? and customer._id=so.customer_fkid and so.delivery_location=orgf._id and orgf._orgid=?", formData.Orgid, formData.SoNo, formData.Orgid, formData.Orgid).Values(&maps)
	if err == nil && num > 0 {
		for k, v := range maps {
			key := "so_" + strconv.Itoa(k)
			result[key] = v
		}
	} else {
		result["status"] = nil
	}

	return result
}

// THIS WILL BE CALLED BATCH WISE
func CreateDispatchNote(formData DeliveryData) int {

	o := orm.NewOrm()
	ormTransactionalErr := o.Begin()
	var note *string
	note = nil
	if formData.AddNote != "" {
		tempnote := formData.AddNote
		note = &tempnote
	}

	//FETCH THE ID, SALES_ORDER_FKID AND ITEM_ORG_ASSOC_FKID FROM SALES_ORDER_ITEMS [THIS HAS SO V/S ITEMS DATA]
	//WE WILL GET LIST OF SALES_ORDER_ITEMS
	var maps []orm.Params

	_, err := o.Raw("SELECT soi._id, soi._sales_order_fkid, soi.item_org_assoc_fkid, soi.price, soi.quantity, soi.delivered_quantity, so.total_sales_price, so.delivery_location, so.origin_location FROM sales_order_items as soi, sales_order as so, item_org_assoc as ioa WHERE soi._orgid=? and sales_order_code_gen=? and item_unique_code=? and soi.item_org_assoc_fkid=ioa._id and so._id=soi._sales_order_fkid and so._orgid=? and ioa._orgid=?", formData.Orgid, formData.SoNo, formData.Itemss, formData.Orgid, formData.Orgid).Values(&maps)
	if err != nil {

	}

	new_dqty := "0.0"
	if formData.DQuantity != "nil" && formData.DQuantity != "" {
		new_dqty = formData.DQuantity
	}
	NewDQty, _ := strconv.ParseFloat(new_dqty, 64)
	NewDelivered := NewDQty
	if maps[0]["delivered_quantity"] != nil {
		p := maps[0]["delivered_quantity"].(string)
		Previous, _ := strconv.ParseFloat(p, 64)
		NewDelivered = Previous + NewDQty
	}

	// new_rqty := "0.0"
	// if formData.RQuantity != "nil" && formData.RQuantity != "" {
	// 	new_rqty = formData.RQuantity
	// }
	// NewRQty, _ := strconv.ParseFloat(new_rqty, 64)
	// NewRejected := NewRQty
	// if maps[0]["rejected_quantity"] != nil {
	// 	p := maps[0]["rejected_quantity"].(string)
	// 	Previous, _ := strconv.ParseFloat(p, 64)
	// 	NewRejected = Previous + NewRQty
	// }

	location := maps[0]["origin_location"].(string)
	itemid := maps[0]["item_org_assoc_fkid"].(string)
	itemID, _ := strconv.ParseInt(itemid, 10, 64)

	//UPDATE THE QUANTITY OF ITEM IN SALES_ORDER_ITEMS [FOR QTY DELIVERED IN THAT BATCH]
	_, errnew := o.Raw("Update sales_order_items SET delivered_quantity=? WHERE _sales_order_fkid=? and item_org_assoc_fkid=? and _orgid=? and status=1", NewDelivered, maps[0]["_sales_order_fkid"], maps[0]["item_org_assoc_fkid"], formData.Orgid).Exec()
	if errnew != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}

	generatedBatchNo := formData.BatchNo

	//CHECK IF BATCH NO IS GIVEN BY THE USER, ELSE GET A GENERATED BATCH NO
	//RIGHT NOW COMBINING THE SALES ORDER NUMBER AND DELIVERY NO GEN AS THE BATCH NO
	// var batchNoSeq []orm.Params
	// var incBatchNo int64
	// incBatchNo = 0
	// ct, errBatch := o.Raw("SELECT max(batch_no_seq) as maxBatchNo FROM sales_order_delivery WHERE _orgid=?", formData.Orgid).Values(&batchNoSeq)

	// if ct > 0 && errBatch == nil {
	// 	if batchNoSeq[0]["maxBatchNo"] != nil{
	// 		maxbatch := batchNoSeq[0]["maxBatchNo"].(string)
	// 		incBatchNo,_ = strconv.ParseInt(maxbatch, 10, 64)
	// 		incBatchNo  = incBatchNo + 1       // INCREMENT THE SEQUENCE BY 1
	// 			if formData.BatchNo == "" {
	// 				generatedBatchNo = formData.NewGenCode + strconv.Itoa(int(incBatchNo))  // SYSTEM DEFINED BATCH NUM IF USER DINT PROVIDE
	// 			}
	// 		} else {
	// 			incBatchNo = incBatchNo + 1
	// 			generatedBatchNo = formData.NewGenCode + "1"
	// 		}
	// } else {
	// 	incBatchNo = incBatchNo + 1
	// 	generatedBatchNo = formData.NewGenCode + "1"
	// }

	var lr_date *string
	lr_date = nil
	if formData.LrDt != "" {
		templr_date := formData.LrDt
		lr_date = &templr_date
	}

	var lr_no *string
	lr_no = nil
	if formData.LrNo != "" {
		templr_no := formData.LrNo
		lr_no = &templr_no
	}

	var transporter *string
	transporter = nil
	if formData.TransporterFkid != "" {
		temptransporter := formData.TransporterFkid
		transporter = &temptransporter
	}

	var vehicle *string
	vehicle = nil
	if formData.VehicleFkid != "" {
		tempvehicle := formData.VehicleFkid
		vehicle = &tempvehicle
	}

	//CREATE ENTRY IN SALES_ORDER_DELIVERY WITH THE BATCH NO
	_, err1 := o.Raw("INSERT INTO sales_order_delivery (_orgid, sales_order_fkid, item_org_assoc_fkid, sales_order_items_fkid, dispatch_no, delivered_date, rate, quantity, total_cost, quantity_delivered, notes, status, dispatch_no_gen, batch_no, customer_invoice_fkid, geo_location, lr_date, lr_number, transporter_fkid, vehicle_fkid) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ", formData.Orgid, maps[0]["_sales_order_fkid"], maps[0]["item_org_assoc_fkid"], maps[0]["_id"], formData.DispatchNo, formData.DeliveredDate, maps[0]["price"], maps[0]["quantity"], formData.TotalCost, formData.DQuantity, &note, "1", formData.NewGenCode, generatedBatchNo, formData.CustomerInvoiceFkId, formData.DeliveryLocation, &lr_date, &lr_no, &transporter, &vehicle).Exec()
	//_, err1 := o.Raw("INSERT INTO sales_order_delivery (_orgid, sales_order_fkid, item_org_assoc_fkid, sales_order_items_fkid, dispatch_no, delivered_date, rate, quantity, total_cost, quantity_delivered, notes, dispatch_no_gen, batch_no, customer_invoice_fkid, batch_no_seq, geo_location, lr_date, lr_number, transporter_fkid, vehicle_fkid) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ", formData.Orgid, maps[0]["sales_order_fkid"], maps[0]["item_org_assoc_fkid"], maps[0]["_id"], formData.DispatchNo, formData.DeliveredDate, maps[0]["price"], maps[0]["quantity"], formData.TotalCost, formData.DQuantity, &note, formData.NewGenCode, generatedBatchNo, formData.CustomerInvoiceFkId, incBatchNo, formData.DeliveryLocation, formData.LrDt, formData.LrNo, formData.TransporterFkid, formData.VehicleFkid).Exec()
	if err1 != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}

	//GET THE INSERTED ID FROM THE SALES_ORDER_DELIVERY
	// var dispatchData []orm.Params
	// _, err2 := o.Raw("SELECT _id, sales_order_fkid, item_org_assoc_fkid, rate, quantity_delivered FROM sales_order_delivery WHERE _orgid=? and dispatch_no_gen=? and item_org_assoc_fkid=? and batch_no =?", formData.Orgid, formData.NewGenCode, itemID, generatedBatchNo).Values(&dispatchData)
	// if err2 != nil {

	// }

	// dispatchId := dispatchData[0]["_id"].(string)
	// dispatchFkId, _ := strconv.ParseInt(dispatchId, 10, 64)

	// rate := dispatchData[0]["rate"].(string)
	// itemRate, _ := strconv.ParseFloat(rate, 64)

	//INSERT IN THE ITEM_STOCK TABLE

	// itemstockData := ItemStockTable{
	// 	OrgId:            formData.Orgid,
	// 	Itemid:           itemID,
	// 	QuantityModified: NewAQty,
	// 	Location:         location,
	// 	GRNNumber:        formData.NewGenCode,
	// 	InsertTableFkid:  dispatchFkId,
	// 	StartDate:        time.Now(),
	// 	EndDate:          time.Now(),
	// 	ExpiresBy:        time.Now(), // Temp assignment as no provision from UI
	// 	BatchNo:		  generatedBatchNo,
	// 	Rate:			  itemRate,
	// 	InsertStatus:	  4,
	// }

	// insertStatus := InsertUpdateItemStock(itemstockData)

	// if insertStatus == 0 {
	// 	ormTransactionalErr = o.Rollback()
	// 	return 0
	// }

	salesid := maps[0]["_sales_order_fkid"].(string)
	salesID, _ := strconv.ParseInt(salesid, 10, 64)

	itemstockData := ItemStockTable{
		OrgId:             formData.Orgid,
		Itemid:            itemID,
		Location:          location,
		QuantityModified:  0,
		ConsumedQuantity:  NewDQty,
		EndDate:           time.Now(),
		IsBlocked:         3,
		ConsumedTableFkid: salesID,
	}

	insertQueryStatus := InsertUpdateItemStock(itemstockData)
	if insertQueryStatus == 0 {
		ormTransactionalErr = o.Rollback()
		return 0
	}

	//UPDATE THE ITEM TOTAL INVENTORY
	itemInventoryData := ItemTotalInventory{
		OrgId:            formData.Orgid,
		Itemid:           itemID,
		QuantityModified: NewDQty,
		Location:         location,
		AddRemoveFlag:    0, // 0 subtracts from inventory
	}

	insertInvStatus := InsertUpdateItemTotalInventory(itemInventoryData)
	if insertInvStatus == 0 {
		ormTransactionalErr = o.Rollback()
		return 0
	}

	//FETCH THE DETAILS FROM SUPPLIER AND INSERT IN PO_SO_SUPPLIER_INVOICE
	// var supplierDetail []orm.Params
	// _, err09 := o.Raw("SELECT _id from supplier_org_assoc where company_name=? and _orgid=? ", formData.Suppliers, formData.Orgid).Values(&supplierDetail)

	// if err09 != nil {
	// 	ormTransactionalErr = o.Rollback()
	// 	return 0
	// }

	//INSERT INTO ITEM RATES
	// _, err6 := o.Raw("INSERT INTO item_rates (_orgid, item_org_assoc_fkid, supplier_org_assoc_fkid, rate, currency, start_date,latest) VALUES (?,?,?,?,?,?,?)", formData.Orgid, dispatchData[0]["item_org_assoc_fkid"], supplierDetail[0]["_id"], dispatchData[0]["rate"], maps[0]["currency"], time.Now(), "1").Exec()
	// if err6 != nil {
	// 	ormTransactionalErr = o.Rollback()
	// 	return 0
	// }

	ormTransactionalErr = o.Commit()
	if ormTransactionalErr != nil {
		return 0
	}
	return 1
}

//-------------------------End of Function

func GetAllDeliveryDetails(formData DeliveryData) map[string]orm.Params {
	// var stock []Stock
	// o := orm.NewOrm()

	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	//num, err := o.Raw("SELECT * FROM item_master ").Values(&maps)

	num, err := o.Raw("SELECT sod._id as sod_id, sod.dispatch_no_gen, so.sales_order_code_gen, c.company_name, sod.quantity_delivered, sod.delivered_date as d_date, orgf.factory_location, sod.customer_invoice_fkid, sod.notes, sod.lr_date as l_date, sod.lr_number, sod.transporter_fkid, sod.vehicle_fkid, ioa.invoice_narration FROM sales_order_delivery as sod, sales_order as so, customer as c, item_org_assoc as ioa,	organisation_factories as orgf WHERE sod.sales_order_fkid = so._id and sod.item_org_assoc_fkid=ioa._id and so.delivery_location = orgf._id and c._id=so.customer_fkid and sod._orgid=? and so._orgid=? and c._orgid=? and ioa._orgid=?", formData.Orgid, formData.Orgid, formData.Orgid, formData.Orgid).Values(&maps)

	if err == nil && num > 0 {
		for k, v := range maps {
			if maps[k]["d_date"] != nil && maps[k]["d_date"] != "" {
				maps[k]["d_date"] = convertDateString(maps[k]["d_date"].(string))
			}
			// if maps[k]["i_date"] != nil && maps[k]["i_date"] != "" {
			// 	maps[k]["i_date"] = convertDateString(maps[k]["i_date"].(string))
			// }
			// if maps[k]["c_date"] != nil && maps[k]["c_date"] != "" {
			// 	maps[k]["c_date"] = convertDateString(maps[k]["c_date"].(string))
			// }
			if maps[k]["l_date"] != nil && maps[k]["l_date"] != "" {
				maps[k]["l_date"] = convertDateString(maps[k]["l_date"].(string))
			}
			if maps[k]["transporter_fkid"] != nil && maps[k]["transporter_fkid"] != "" {
				var transportermaps []orm.Params
				transporternum, transportererr := o.Raw("SELECT * FROM transporter WHERE _id=? and _orgid=? ", maps[k]["transporter_fkid"], formData.Orgid).Values(&transportermaps)
				if transporternum > 0 && transportererr == nil {
					maps[k]["transporter_name"] = transportermaps[0]["transporter_name"]
				}
			}

			if maps[k]["vehicle_fkid"] != nil && maps[k]["vehicle_fkid"] != "" {
				var vehiclemaps []orm.Params
				vehiclenum, vehicleerr := o.Raw("SELECT * FROM transporter WHERE _id=? and _orgid=? ", maps[k]["vehicle_fkid"], formData.Orgid).Values(&vehiclemaps)
				if vehiclenum > 0 && vehicleerr == nil {
					maps[k]["registration_number"] = vehiclemaps[0]["registration_number"]
				}
			}

			key := "grn_" + strconv.Itoa(k)
			result[key] = v
		}
	} else {
		result["status"] = nil
	}
	return result
}

func UpdateSO(formData Sales, salesitems []map[string]string) int {
	o := orm.NewOrm()
	ormTransactionalErr := o.Begin()

	var freightpackagingothers string
	freightpackagingothers = "0.00"
	if formData.TotalFreightPackaging != "" {
		tempTotalFreightPackaging := formData.TotalFreightPackaging
		freightpackagingothers = tempTotalFreightPackaging
	}

	// var paymentterms *string
	// paymentterms = nil
	// if formData.PaymentTerms != "" {
	// 	temppaymentterms := formData.PaymentTerms
	// 	paymentterms = &temppaymentterms
	// }

	// var billlocation *string
	// billlocation = nil
	// if formData.BAddress != "" {
	// 	tempbilllocation := formData.BAddress
	// 	billlocation = &tempbilllocation
	// }

	var transporter *string
	transporter = nil
	if formData.Transporter != "" {
		temptransporter := formData.Transporter
		transporter = &temptransporter
	}

	var vehicle *string
	vehicle = nil
	if formData.Vehicle != "" {
		tempvehicle := formData.Vehicle
		vehicle = &tempvehicle
	}

	_, err := o.Raw("UPDATE sales_order SET delivery_date=?, total_taxable_value=?, total_tax_cess=?, total_freight_packaging_others=?, total_price_after_taxes=?, total_sales_price=?, transporter_fkid=?, vehicle_fkid=? WHERE _orgid=? and _id=?", formData.DeliveryDate, formData.TotalTaxableValue, formData.TotalTaxCess, freightpackagingothers, formData.TotalAfterTaxes, formData.TotalSalesPrice, &transporter, &vehicle, formData.Orgid, formData.OrderId).Exec()
	if err != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}

	//Remove deleted items
	var allItemsMaps []orm.Params
	_, allItemsErr := o.Raw("SELECT _id, item_org_assoc_fkid FROM sales_order_items as soi WHERE soi._orgid=? and soi._sales_order_fkid=? and status=1", formData.Orgid, formData.OrderId).Values(&allItemsMaps)
	if allItemsErr != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}
	for k, _ := range allItemsMaps {
		itemPresentFlag := 0
		for _, it := range salesitems {
			if allItemsMaps[k]["item_org_assoc_fkid"] == it["item_name"] {
				itemPresentFlag = 1
				break
			}
		}
		if itemPresentFlag == 0 {
			_, removeErr := o.Raw("UPDATE sales_order_items SET status=0 WHERE _orgid=? and _sales_order_fkid=? and item_org_assoc_fkid=? and status=1", formData.Orgid, formData.OrderId, allItemsMaps[k]["item_org_assoc_fkid"]).Exec()
			if removeErr != nil {
				ormTransactionalErr = o.Rollback()
				return 0
			}
		}
	}

	for _, it := range salesitems {

		if it["item_name"] != "" {

			// _, err1 := o.Raw("UPDATE po_so_items SET quantity=?, hsn_sac=?, rate=?, discount_rate=?, discount_value=?, delivery_date=?, taxable_amount=?, gst_rate=?, igst_rate=?, igst_amount=?, cgst_rate=?, cgst_amount=?, sgst_rate=?, sgst_amount=?, cess_rate=?, cess_amount=?, total_tax_cess=?, total_after_tax=?, freight=?, insurance=?, packaging_forwarding=?, others=?, total_other_charges=?, total_cost=?, transporter_fkid=?, vehicle_fkid=? WHERE _orgid=? and purchase_service_order_fkid=? and item_org_assoc_fkid=? and status=1", it["qty"], it["hsn"], it["cost"], discountrate, discountamount, formData.PoDeliveryDate, it["taxable_amount"], gstrate, igstrate, it["igst_amount"], cgstrate, it["cgst_amount"], sgstrate, it["sgst_amount"], cessrate, it["cess_amount"], it["total_tax_cess"], it["total_after_tax"], freight, insurance, packagingforwarding, others, totalothercharges, formData.TotalInvoiceValue, &transporter, &vehicle, formData.Orgid, formData.PurchaseId, it["item_name"]).Exec()
			// if err1 != nil {
			// 	ormTransactionalErr = o.Rollback()
			// 	return 0
			// }
			var soItemsMaps []orm.Params
			//psgCount, psgErr := o.Raw("SELECT * FROM po_so_items_with_grn as psg WHERE psg._orgid=? and psg.purchase_service_order_fkid=? and psg.item_org_assoc_fkid=? and psg.status=1", formData.Orgid, formData.PurchaseId, it["item_name"]).Values(&psgmaps)
			_, soItemsErr := o.Raw("SELECT * FROM sales_order_items as soi WHERE soi._orgid=? and soi._sales_order_fkid=? and soi.item_org_assoc_fkid=? and status=1", formData.Orgid, formData.OrderId, it["item_name"]).Values(&soItemsMaps)
			if soItemsErr != nil {
				soItemsErr = o.Rollback()
				return 0
			} else if soItemsMaps[0]["delivered_quantity"] == nil || soItemsMaps[0]["delivered_quantity"] == "" {
				//Update previous entry to 0 and insert a new entry
				_, err0 := o.Raw("UPDATE sales_order_items SET status=0 WHERE _orgid=? and _sales_order_fkid=? and item_org_assoc_fkid=? and status=1", formData.Orgid, formData.OrderId, it["item_name"]).Exec()
				if err0 != nil {
					ormTransactionalErr = o.Rollback()
					return 0
				}

				var hsn *string
				hsn = nil
				if it["hsn"] != "" {
					tempHSN := it["hsn"]
					hsn = &tempHSN
				}

				// var delivered_quantity *string
				// delivered_quantity = nil
				// if it["delivered_quantity"] != "" {
				// 	tempdelivered_quantity := it["delivered_quantity"]
				// 	delivered_quantity = &tempdelivered_quantity
				// }

				// var rejected_quantity *string
				// rejected_quantity = nil
				// if it["rejected_quantity"] != "" {
				// 	temprejected_quantity := it["rejected_quantity"]
				// 	rejected_quantity = &temprejected_quantity
				// }

				var discountrate string
				discountrate = "0"
				if it["discount_percent"] != "" {
					tempDiscountPercent := it["discount_percent"]
					discountrate = tempDiscountPercent
				}

				var discountamount string
				discountamount = "0.00"
				if it["discount_value"] != "" {
					tempDiscountAmount := it["discount_value"]
					discountamount = tempDiscountAmount
				}

				var gstrate string
				gstrate = "0"
				if it["gst_percent"] != "" {
					tempGSTPercent := it["gst_percent"]
					gstrate = tempGSTPercent
				}

				var igstrate string
				igstrate = "0"
				if it["igst_rate"] != "" {
					tempIGSTRate := it["igst_rate"]
					igstrate = tempIGSTRate
				}

				var igstAmount string
				igstAmount = "0"
				if it["igst_amount"] != "" {
					tempIGSTAmount := it["igst_amount"]
					igstAmount = tempIGSTAmount
				}

				var cgstrate string
				cgstrate = "0"
				if it["cgst_rate"] != "" {
					tempCGSTRate := it["cgst_rate"]
					cgstrate = tempCGSTRate
				}

				var cgstAmount string
				cgstAmount = "0"
				if it["cgst_amount"] != "" {
					tempCGSTAmount := it["cgst_amount"]
					cgstAmount = tempCGSTAmount
				}

				var sgstrate string
				sgstrate = "0"
				if it["sgst_rate"] != "" {
					tempSGSTRate := it["sgst_rate"]
					sgstrate = tempSGSTRate
				}

				var sgstAmount string
				sgstAmount = "0"
				if it["sgst_amount"] != "" {
					tempSGSTAmount := it["sgst_amount"]
					sgstAmount = tempSGSTAmount
				}

				var cessrate string
				cessrate = "0"
				if it["cess_rate"] != "" {
					tempCessRate := it["cess_rate"]
					cessrate = tempCessRate
				}

				var cessAmount string
				cessAmount = "0"
				if it["cess_amount"] != "" {
					tempCessAmount := it["cess_amount"]
					cessAmount = tempCessAmount
				}

				// var freight string
				// freight = "0"
				// if it["freight"] != "" {
				// 	tempFreight := it["freight"]
				// 	freight = tempFreight
				// }

				// var insurance string
				// insurance = "0"
				// if it["insurance"] != "" {
				// 	tempInsurance := it["insurance"]
				// 	insurance = tempInsurance
				// }

				// var packagingforwarding string
				// packagingforwarding = "0"
				// if it["packaging_forwarding"] != "" {
				// 	tempPackagingForwarding := it["packaging_forwarding"]
				// 	packagingforwarding = tempPackagingForwarding
				// }

				// var others string
				// others = "0"
				// if it["others"] != "" {
				// 	tempOthers := it["others"]
				// 	others = tempOthers
				// }

				var totalothercharges string
				totalothercharges = "0.00"
				if it["total_other_charges"] != "" {
					tempTotalOtherCharges := it["total_other_charges"]
					totalothercharges = tempTotalOtherCharges
				}

				_, err4 := o.Raw("INSERT INTO sales_order_items (_sales_order_fkid, item_org_assoc_fkid, quantity, uom, hsn_sac, price, discount_rate, discount_value, delivery_date, taxable_amount, gst_rate, igst_rate, igst_amount, cgst_rate, cgst_amount, sgst_rate, sgst_amount, cess_rate, cess_amount, total_tax_cess, total_after_tax, total_other_charges, _orgid, status) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ", formData.OrderId, it["item_name"], it["qty"], it["uom"], &hsn, it["cost"], discountrate, discountamount, formData.DeliveryDate, it["taxable_amount"], gstrate, igstrate, igstAmount, cgstrate, cgstAmount, sgstrate, sgstAmount, cessrate, cessAmount, it["total_tax_cess"], it["total_after_tax"], totalothercharges, formData.Orgid, "1").Exec()
				if err4 != nil {
					ormTransactionalErr = o.Rollback()
					return 0
				}

				// if Status == "2" {
				// 	var maps05 []orm.Params
				// 	count, err05 := o.Raw("SELECT _id FROM item_rates WHERE _orgid=? and item_org_assoc_fkid=? and supplier_org_assoc_fkid=?", formData.Orgid, it["item_name"], formData.Supplier).Values(&maps05)
				// 	if err05 != nil {
				// 		ormTransactionalErr = o.Rollback()
				// 		return 0
				// 	}
				// 	if count > 0 {
				// 		_, err005 := o.Raw("UPDATE item_rates SET latest = 0 WHERE _orgid=? and item_org_assoc_fkid=? and supplier_org_assoc_fkid=?", formData.Orgid, it["item_name"], formData.Supplier).Exec()
				// 		if err005 != nil {
				// 			ormTransactionalErr = o.Rollback()
				// 			return 0
				// 		}
				// 	}
				// 	_, err5 := o.Raw("INSERT INTO item_rates (item_org_assoc_fkid, supplier_org_assoc_fkid, rate, currency, start_date, latest, _orgid) VALUES (?,?,?,?,?,?,?) ", it["item_name"], formData.Supplier, it["cost"], formData.Currency, time.Now(), "1", formData.Orgid).Exec()
				// 	if err5 != nil {
				// 		ormTransactionalErr = o.Rollback()
				// 		return 0
				// 	}
				// }
			} else {
				deliveredQty, _ := strconv.ParseFloat(soItemsMaps[0]["delivered_quantity"].(string), 64)
				if deliveredQty == 0 {
					//Update previous entry to 0 and insert a new entry
					_, err0 := o.Raw("UPDATE sales_order_items SET status=0 WHERE _orgid=? and _sales_order_fkid=? and item_org_assoc_fkid=? and status=1", formData.Orgid, formData.OrderId, it["item_name"]).Exec()
					if err0 != nil {
						ormTransactionalErr = o.Rollback()
						return 0
					}

					var hsn *string
					hsn = nil
					if it["hsn"] != "" {
						tempHSN := it["hsn"]
						hsn = &tempHSN
					}

					// var delivered_quantity *string
					// delivered_quantity = nil
					// if it["delivered_quantity"] != "" {
					// 	tempdelivered_quantity := it["delivered_quantity"]
					// 	delivered_quantity = &tempdelivered_quantity
					// }

					// var rejected_quantity *string
					// rejected_quantity = nil
					// if it["rejected_quantity"] != "" {
					// 	temprejected_quantity := it["rejected_quantity"]
					// 	rejected_quantity = &temprejected_quantity
					// }

					var discountrate string
					discountrate = "0"
					if it["discount_percent"] != "" {
						tempDiscountPercent := it["discount_percent"]
						discountrate = tempDiscountPercent
					}

					var discountamount string
					discountamount = "0.00"
					if it["discount_value"] != "" {
						tempDiscountAmount := it["discount_value"]
						discountamount = tempDiscountAmount
					}

					var gstrate string
					gstrate = "0"
					if it["gst_percent"] != "" {
						tempGSTPercent := it["gst_percent"]
						gstrate = tempGSTPercent
					}

					var igstrate string
					igstrate = "0"
					if it["igst_rate"] != "" {
						tempIGSTRate := it["igst_rate"]
						igstrate = tempIGSTRate
					}

					var igstAmount string
					igstAmount = "0"
					if it["igst_amount"] != "" {
						tempIGSTAmount := it["igst_amount"]
						igstAmount = tempIGSTAmount
					}

					var cgstrate string
					cgstrate = "0"
					if it["cgst_rate"] != "" {
						tempCGSTRate := it["cgst_rate"]
						cgstrate = tempCGSTRate
					}

					var cgstAmount string
					cgstAmount = "0"
					if it["cgst_amount"] != "" {
						tempCGSTAmount := it["cgst_amount"]
						cgstAmount = tempCGSTAmount
					}

					var sgstrate string
					sgstrate = "0"
					if it["sgst_rate"] != "" {
						tempSGSTRate := it["sgst_rate"]
						sgstrate = tempSGSTRate
					}

					var sgstAmount string
					sgstAmount = "0"
					if it["sgst_amount"] != "" {
						tempSGSTAmount := it["sgst_amount"]
						sgstAmount = tempSGSTAmount
					}

					var cessrate string
					cessrate = "0"
					if it["cess_rate"] != "" {
						tempCessRate := it["cess_rate"]
						cessrate = tempCessRate
					}

					var cessAmount string
					cessAmount = "0"
					if it["cess_amount"] != "" {
						tempCessAmount := it["cess_amount"]
						cessAmount = tempCessAmount
					}

					// var freight string
					// freight = "0"
					// if it["freight"] != "" {
					// 	tempFreight := it["freight"]
					// 	freight = tempFreight
					// }

					// var insurance string
					// insurance = "0"
					// if it["insurance"] != "" {
					// 	tempInsurance := it["insurance"]
					// 	insurance = tempInsurance
					// }

					// var packagingforwarding string
					// packagingforwarding = "0"
					// if it["packaging_forwarding"] != "" {
					// 	tempPackagingForwarding := it["packaging_forwarding"]
					// 	packagingforwarding = tempPackagingForwarding
					// }

					// var others string
					// others = "0"
					// if it["others"] != "" {
					// 	tempOthers := it["others"]
					// 	others = tempOthers
					// }

					var totalothercharges string
					totalothercharges = "0.00"
					if it["total_other_charges"] != "" {
						tempTotalOtherCharges := it["total_other_charges"]
						totalothercharges = tempTotalOtherCharges
					}

					_, err4 := o.Raw("INSERT INTO sales_order_items (_sales_order_fkid, item_org_assoc_fkid, quantity, uom, hsn_sac, price, discount_rate, discount_value, delivery_date, taxable_amount, gst_rate, igst_rate, igst_amount, cgst_rate, cgst_amount, sgst_rate, sgst_amount, cess_rate, cess_amount, total_tax_cess, total_after_tax, total_other_charges, _orgid, status) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ", formData.OrderId, it["item_name"], it["qty"], it["uom"], &hsn, it["cost"], discountrate, discountamount, formData.DeliveryDate, it["taxable_amount"], gstrate, igstrate, igstAmount, cgstrate, cgstAmount, sgstrate, sgstAmount, cessrate, cessAmount, it["total_tax_cess"], it["total_after_tax"], totalothercharges, formData.Orgid, "1").Exec()
					if err4 != nil {
						ormTransactionalErr = o.Rollback()
						return 0
					}

					// if Status == "2" {
					// 	var maps05 []orm.Params
					// 	count, err05 := o.Raw("SELECT _id FROM item_rates WHERE _orgid=? and item_org_assoc_fkid=? and supplier_org_assoc_fkid=?", formData.Orgid, it["item_name"], formData.Supplier).Values(&maps05)
					// 	if err05 != nil {
					// 		ormTransactionalErr = o.Rollback()
					// 		return 0
					// 	}
					// 	if count > 0 {
					// 		_, err005 := o.Raw("UPDATE item_rates SET latest = 0 WHERE _orgid=? and item_org_assoc_fkid=? and supplier_org_assoc_fkid=?", formData.Orgid, it["item_name"], formData.Supplier).Exec()
					// 		if err005 != nil {
					// 			ormTransactionalErr = o.Rollback()
					// 			return 0
					// 		}
					// 	}
					// 	_, err5 := o.Raw("INSERT INTO item_rates (item_org_assoc_fkid, supplier_org_assoc_fkid, rate, currency, start_date, latest, _orgid) VALUES (?,?,?,?,?,?,?) ", it["item_name"], formData.Supplier, it["cost"], formData.Currency, time.Now(), "1", formData.Orgid).Exec()
					// 	if err5 != nil {
					// 		ormTransactionalErr = o.Rollback()
					// 		return 0
					// 	}
					// }
				} else {
					// Part of the order is delivered, Do not edit
				}
			}
		}
	}

	if ormTransactionalErr != nil {
		return 0
	}
	ormTransactionalErr = o.Commit()
	return 1
}

func UpdateDraftSO(formData Sales, salesitems []map[string]string) int {
	o := orm.NewOrm()
	ormTransactionalErr := o.Begin()

	var freightpackagingothers string
	freightpackagingothers = "0.00"
	if formData.TotalFreightPackaging != "" {
		tempTotalFreightPackaging := formData.TotalFreightPackaging
		freightpackagingothers = tempTotalFreightPackaging
	}

	// var paymentterms *string
	// paymentterms = nil
	// if formData.PaymentTerms != "" {
	// 	temppaymentterms := formData.PaymentTerms
	// 	paymentterms = &temppaymentterms
	// }

	// var billlocation *string
	// billlocation = nil
	// if formData.BAddress != "" {
	// 	tempbilllocation := formData.BAddress
	// 	billlocation = &tempbilllocation
	// }

	var transporter *string
	transporter = nil
	if formData.Transporter != "" {
		temptransporter := formData.Transporter
		transporter = &temptransporter
	}

	var vehicle *string
	vehicle = nil
	if formData.Vehicle != "" {
		tempvehicle := formData.Vehicle
		vehicle = &tempvehicle
	}

	_, err := o.Raw("UPDATE sales_order SET delivery_date=?, total_taxable_value=?, total_tax_cess=?, total_freight_packaging_others=?, total_price_after_taxes=?, total_sales_price=?, transporter_fkid=?, vehicle_fkid=? WHERE _orgid=? and _id=?", formData.DeliveryDate, formData.TotalTaxableValue, formData.TotalTaxCess, freightpackagingothers, formData.TotalAfterTaxes, formData.TotalSalesPrice, &transporter, &vehicle, formData.Orgid, formData.OrderId).Exec()
	if err != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}

	_, err01 := o.Raw("UPDATE sales_order_items SET status=? WHERE status=1 and _orgid=? and _sales_order_fkid=? ", 0, formData.Orgid, formData.OrderId).Exec()
	if err01 != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}

	for _, it := range salesitems {

		if it["item_name"] != "" {
			var hsn *string
			hsn = nil
			if it["hsn"] != "" {
				tempHSN := it["hsn"]
				hsn = &tempHSN
			}

			// var delivered_quantity *string
			// delivered_quantity = nil
			// if it["delivered_quantity"] != "" {
			// 	tempdelivered_quantity := it["delivered_quantity"]
			// 	delivered_quantity = &tempdelivered_quantity
			// }

			// var rejected_quantity *string
			// rejected_quantity = nil
			// if it["rejected_quantity"] != "" {
			// 	temprejected_quantity := it["rejected_quantity"]
			// 	rejected_quantity = &temprejected_quantity
			// }

			var discountrate string
			discountrate = "0"
			if it["discount_percent"] != "" {
				tempDiscountPercent := it["discount_percent"]
				discountrate = tempDiscountPercent
			}

			var discountamount string
			discountamount = "0.00"
			if it["discount_value"] != "" {
				tempDiscountAmount := it["discount_value"]
				discountamount = tempDiscountAmount
			}

			var gstrate string
			gstrate = "0"
			if it["gst_percent"] != "" {
				tempGSTPercent := it["gst_percent"]
				gstrate = tempGSTPercent
			}

			var igstrate string
			igstrate = "0"
			if it["igst_rate"] != "" {
				tempIGSTRate := it["igst_rate"]
				igstrate = tempIGSTRate
			}

			var igstAmount string
			igstAmount = "0"
			if it["igst_amount"] != "" {
				tempIGSTAmount := it["igst_amount"]
				igstAmount = tempIGSTAmount
			}

			var cgstrate string
			cgstrate = "0"
			if it["cgst_rate"] != "" {
				tempCGSTRate := it["cgst_rate"]
				cgstrate = tempCGSTRate
			}

			var cgstAmount string
			cgstAmount = "0"
			if it["cgst_amount"] != "" {
				tempCGSTAmount := it["cgst_amount"]
				cgstAmount = tempCGSTAmount
			}

			var sgstrate string
			sgstrate = "0"
			if it["sgst_rate"] != "" {
				tempSGSTRate := it["sgst_rate"]
				sgstrate = tempSGSTRate
			}

			var sgstAmount string
			sgstAmount = "0"
			if it["sgst_amount"] != "" {
				tempSGSTAmount := it["sgst_amount"]
				sgstAmount = tempSGSTAmount
			}

			var cessrate string
			cessrate = "0"
			if it["cess_rate"] != "" {
				tempCessRate := it["cess_rate"]
				cessrate = tempCessRate
			}

			var cessAmount string
			cessAmount = "0"
			if it["cess_amount"] != "" {
				tempCessAmount := it["cess_amount"]
				cessAmount = tempCessAmount
			}

			// var freight string
			// freight = "0"
			// if it["freight"] != "" {
			// 	tempFreight := it["freight"]
			// 	freight = tempFreight
			// }

			// var insurance string
			// insurance = "0"
			// if it["insurance"] != "" {
			// 	tempInsurance := it["insurance"]
			// 	insurance = tempInsurance
			// }

			// var packagingforwarding string
			// packagingforwarding = "0"
			// if it["packaging_forwarding"] != "" {
			// 	tempPackagingForwarding := it["packaging_forwarding"]
			// 	packagingforwarding = tempPackagingForwarding
			// }

			// var others string
			// others = "0"
			// if it["others"] != "" {
			// 	tempOthers := it["others"]
			// 	others = tempOthers
			// }

			var totalothercharges string
			totalothercharges = "0.00"
			if it["total_other_charges"] != "" {
				tempTotalOtherCharges := it["total_other_charges"]
				totalothercharges = tempTotalOtherCharges
			}

			var maps001 []orm.Params
			num, err001 := o.Raw("SELECT _id FROM sales_order_items WHERE _orgid=? and _sales_order_fkid=? and item_org_assoc_fkid=? and status=0", formData.Orgid, formData.OrderId, it["item_name"]).Values(&maps001)
			if err001 == nil && num > 0 {
				_, err2 := o.Raw("UPDATE sales_order_items SET quantity=?, uom=?, hsn_sac=?, price=?, discount_rate=?, discount_value=?, delivery_date=?, taxable_amount=?, gst_rate=?, igst_rate=?, igst_amount=?, cgst_rate=?, cgst_amount=?, sgst_rate=?, sgst_amount=?, cess_rate=?, cess_amount=?, total_tax_cess=?, total_after_tax=?, total_other_charges=?, status=? WHERE _orgid=? and _sales_order_fkid=? and item_org_assoc_fkid=? and status=?", it["qty"], it["uom"], &hsn, it["cost"], discountrate, discountamount, formData.DeliveryDate, it["taxable_amount"], gstrate, igstrate, igstAmount, cgstrate, cgstAmount, sgstrate, sgstAmount, cessrate, cessAmount, it["total_tax_cess"], it["total_after_tax"], totalothercharges, 1, formData.Orgid, formData.OrderId, it["item_name"], 0).Exec()
				if err2 != nil {
					ormTransactionalErr = o.Rollback()
					return 0
				}
			} else {
				_, err4 := o.Raw("INSERT INTO sales_order_items (_sales_order_fkid, item_org_assoc_fkid, quantity, uom, hsn_sac, price, discount_rate, discount_value, delivery_date, taxable_amount, gst_rate, igst_rate, igst_amount, cgst_rate, cgst_amount, sgst_rate, sgst_amount, cess_rate, cess_amount, total_tax_cess, total_after_tax, total_other_charges, _orgid, status) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ", formData.OrderId, it["item_name"], it["qty"], it["uom"], &hsn, it["cost"], discountrate, discountamount, formData.DeliveryDate, it["taxable_amount"], gstrate, igstrate, igstAmount, cgstrate, cgstAmount, sgstrate, sgstAmount, cessrate, cessAmount, it["total_tax_cess"], it["total_after_tax"], totalothercharges, formData.Orgid, "1").Exec()
				if err4 != nil {
					ormTransactionalErr = o.Rollback()
					return 0
				}
			}
		}
	}

	if ormTransactionalErr != nil {
		return 0
	}
	ormTransactionalErr = o.Commit()
	return 1
}

func CheckIfSOComplete(orgid string, order_id string) int {
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT * FROM sales_order_items WHERE _orgid=? and _sales_order_fkid=? and status=1", orgid, order_id).Values(&maps)
	if err != nil && num < 1 {
		return 0
	}

	for k, _ := range maps {
		var quantity float64 = 0.0
		var delivered_quantity float64 = 0.0
		if maps[k]["quantity"] != nil && maps[k]["quantity"] != "" {
			quantity, _ = strconv.ParseFloat(maps[k]["quantity"].(string), 64)
		}
		if maps[k]["delivered_quantity"] != nil && maps[k]["delivered_quantity"] != "" {
			delivered_quantity, _ = strconv.ParseFloat(maps[k]["delivered_quantity"].(string), 64)
		}
		if quantity != delivered_quantity {
			return 0
		}
	}
	return 1
}
