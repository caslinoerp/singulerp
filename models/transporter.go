package models

import (
	"strconv"
	//"time"
	//"fmt"
	"github.com/astaxie/beego/orm"
)

type Transporter struct {
	Orgid             string
	Userid            string
	Username          string
	TransporterName   string
	UniqueCode        string
	NewGenCode        string
	MainContactPerson string
	Telephone         string
	Mobile            string
	Fax               string
	Email             string
	HQ                string
	City              string
	State             string
	Country           string
	Pincode           string
	GSTIN             string
	PAN               string
	Introduction      string
	Notes             string
}

func CreateNewTransportation(formData Transporter, warehouses []map[string]string, vehicles []map[string]string) int {
	o := orm.NewOrm()

	ormTransactionalErr := o.Begin()

	var introDate *string
	introDate = nil
	if formData.Introduction != "" {
		templr_date := formData.Introduction
		introDate = &templr_date
	}

	_, err := o.Raw("INSERT INTO transporter (_orgid, transporter_code_gen, transporter_unique_code, transporter_name, main_contact_person, telephone, mobile, fax, email, hq, city, state, country, pincode, GSTIN, PAN, introduction_date, notes, status) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", formData.Orgid, formData.UniqueCode, formData.NewGenCode, formData.TransporterName, formData.MainContactPerson, formData.Telephone, formData.Mobile, formData.Fax, formData.Email, formData.HQ, formData.City, formData.State, formData.Country, formData.Pincode, formData.GSTIN, formData.PAN, &introDate, formData.Notes, "1").Exec()
	if err != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}

	var maps []orm.Params
	count, err1 := o.Raw("SELECT _id FROM transporter WHERE _orgid=? and transporter_code_gen=? and transporter_unique_code=? and transporter_name=? and status=?", formData.Orgid, formData.UniqueCode, formData.NewGenCode, formData.TransporterName, "1").Values(&maps)
	if err1 != nil || count < 1 {
		ormTransactionalErr = o.Rollback()
		return 0
	}

	for _, it := range warehouses {

		if it["billingAddress"] != "" {
			// var maps1 []orm.Params
			// count, err4 := o.Raw("SELECT _id FROM customer_warehouse_address WHERE customer_fkid=? and _orgid=? and address_line1=? and wcity=? and wstate=? and wcountry=? and wpincode=? and GSTIN=? and status=?", maps[0]["_id"], formData.Orgid, it["warehouse_address"], it["wcity"], it["wstate"], it["wcountry"], it["wpincode"], it["gstin"], "1").Values(&maps1)
			// if err4 != nil {
			// 	ormTransactionalErr = o.Rollback()
			// 	return 0
			// }
			// if count < 1 {

			// type_address=2 for warehouse address
			_, err2 := o.Raw("INSERT INTO warehouse (_orgid, transporter_fkid, billing_address, city, state, country, pincode) VALUES (?,?,?,?,?,?,?)", formData.Orgid, maps[0]["_id"], it["billingAddress"], it["scity"], it["sstate"], it["scountry"], it["spincode"]).Exec()
			if err2 != nil {
				ormTransactionalErr = o.Rollback()
				return 0
			}
			//}
			if it["contact_person"] != "" {
				var warehousemaps []orm.Params
				count, err3 := o.Raw("SELECT _id FROM warehouse WHERE _orgid=? and transporter_fkid=? and billing_address=?", formData.Orgid, maps[0]["_id"], it["billingAddress"]).Values(&warehousemaps)
				if err3 != nil || count < 1 {
					ormTransactionalErr = o.Rollback()
					return 0
				}

				_, err4 := o.Raw("INSERT INTO contact (_orgid, transporter_fkid, contact_person_name, designation, mobile, phone, email, warehouse_fkid) VALUES (?,?,?,?,?,?,?,?)", formData.Orgid, maps[0]["_id"], it["contact_person"], it["designation"], it["smobile"], it["sphone"], it["semail"], warehousemaps[0]["_id"]).Exec()
				if err4 != nil {
					ormTransactionalErr = o.Rollback()
					return 0
				}
			}
		}
	}

	for _, it := range vehicles {
		if it["driver_name"] != "" {
			// var maps1 []orm.Params
			// count, err4 := o.Raw("SELECT _id FROM customer_warehouse_address WHERE customer_fkid=? and _orgid=? and address_line1=? and wcity=? and wstate=? and wcountry=? and wpincode=? and GSTIN=? and status=?", maps[0]["_id"], formData.Orgid, it["warehouse_address"], it["wcity"], it["wstate"], it["wcountry"], it["wpincode"], it["gstin"], "1").Values(&maps1)
			// if err4 != nil {
			// 	ormTransactionalErr = o.Rollback()
			// 	return 0
			// }
			// if count < 1 {

			// type_address=2 for warehouse address
			_, err4 := o.Raw("INSERT INTO vehicle (_orgid, transporter_fkid, driver_name, driver_license_no, mobile, email, model_type, registration_number, storage_volume, weight_capacity, axel_weight) VALUES (?,?,?,?,?,?,?,?,?,?,?)", formData.Orgid, maps[0]["_id"], it["driver_name"], it["driver_license"], it["dmobile"], it["demail"], it["model_type"], it["registration_number"], it["storage_volume"], it["weight_capacity"], it["axel_weight"]).Exec()
			if err4 != nil {
				ormTransactionalErr = o.Rollback()
				return 0
			}
			//}
		}
	}

	if ormTransactionalErr != nil {
		return 0
	}
	ormTransactionalErr = o.Commit()
	return 1
}

func GetMaxTransportationCode(orgid string) int {
	var maxCode int
	maxCode = 0
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT max(transporter_unique_code) as maxcode FROM transporter where _orgid=?", orgid).Values(&maps)

	if num > 0 && err == nil {
		if maps[0]["maxcode"] != nil {
			code := maps[0]["maxcode"].(string)
			getValue, _ := strconv.ParseInt(code, 10, 64)
			maxCode = int(getValue)
		}
	}
	return maxCode
}

func GetTransportersToView(formData Transporter) map[string]orm.Params {

	result := make(map[string]orm.Params)

	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT _id, _orgid, transporter_code_gen, transporter_unique_code, transporter_name, main_contact_person, telephone, mobile, fax, email, hq, city, state, country, pincode, GSTIN, PAN, introduction_date , notes, status, last_updated FROM transporter WHERE _orgid=? and status=1", formData.Orgid).Values(&maps)
	if err == nil && num > 0 {
		for k, v := range maps {
			// if maps[k]["credit_in_days"] == nil {
			// 	maps[k]["credit_in_days"] = ""
			// }
			var s string
			if maps[k]["city"] != "" {
				s = maps[k]["city"].(string)
			}
			if maps[k]["state"] != "" {
				if maps[k]["city"] != "" {
					s = s + ", "
				}
				s = s + maps[k]["state"].(string)
			}
			if maps[k]["country"] != "" {
				if maps[k]["state"] != "" {
					s = s + ", "
				}
				s = s + maps[k]["country"].(string)
			}
			if maps[k]["pincode"] != "" {
				if maps[k]["country"] != "" {
					s = s + ", "
				}
				s = s + maps[k]["pincode"].(string)
			}
			maps[k]["city_state_country_pincode"] = s

			if maps[k]["introduction_date"] != "" && maps[k]["introduction_date"] != nil {
				maps[k]["introduction_date"] = convertDateString(maps[k]["introduction_date"].(string))
			}

			var warehousemaps []orm.Params
			warehouseCount, err1 := o.Raw("SELECT * FROM warehouse WHERE _orgid=? and transporter_fkid=?", formData.Orgid, maps[k]["_id"]).Values(&warehousemaps)
			if warehouseCount > 0 && err1 == nil {
				var i int64 = 0
				for ; i < warehouseCount; i++ {
					var contactmaps []orm.Params
					contactCount, err12 := o.Raw("SELECT * FROM contact WHERE _orgid=? and transporter_fkid=? and warehouse_fkid=?", formData.Orgid, maps[k]["_id"], warehousemaps[i]["_id"]).Values(&contactmaps)
					if contactCount > 0 && err12 == nil {
						//maps[k]["contacts"] = contactmaps
						if contactmaps[0]["contact_person_name"] != nil {
							warehousemaps[i]["contact_person_name"] = contactmaps[0]["contact_person_name"].(string)
						} else {
							warehousemaps[i]["contact_person_name"] = ""
						}
						if contactmaps[0]["designation"] != nil {
							warehousemaps[i]["designation"] = contactmaps[0]["designation"].(string)
						} else {
							warehousemaps[i]["designation"] = ""
						}
						if contactmaps[0]["mobile"] != nil {
							warehousemaps[i]["mobile"] = contactmaps[0]["mobile"].(string)
						} else {
							warehousemaps[i]["mobile"] = ""
						}
						if contactmaps[0]["phone"] != nil {
							warehousemaps[i]["phone"] = contactmaps[0]["phone"].(string)
						} else {
							warehousemaps[i]["phone"] = ""
						}
						if contactmaps[0]["email"] != nil {
							warehousemaps[i]["email"] = contactmaps[0]["email"].(string)
						} else {
							warehousemaps[i]["email"] = ""
						}
					}
				}
			}
			maps[k]["warehouses"] = warehousemaps

			var vehiclemaps []orm.Params
			_, err2 := o.Raw("SELECT * FROM vehicle WHERE _orgid=? and transporter_fkid=?", formData.Orgid, maps[k]["_id"]).Values(&vehiclemaps)
			//if vehicleCount > 0 && err2 == nil {
			if err2 == nil {
				maps[k]["vehicles"] = vehiclemaps
			}

			key := "transporter_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func SearchByTransporterDetails(transporterName string, contactName string, Location string, _orgId string) map[string]orm.Params {

	result := make(map[string]orm.Params)

	o := orm.NewOrm()
	var maps []orm.Params
	sqlString := ""
	if transporterName != "" {
		sqlString = sqlString + "and transporter_name like '%" + transporterName + "%'"
	}
	if Location != "" {
		sqlString = sqlString + "and (city like '%" + Location + "%' or state like '%" + Location + "%' or country like '%" + Location + "%') "
	}
	if contactName != "" {
		sqlString = sqlString + "and main_contact_person like '%" + contactName + "%' "
	}

	num, err := o.Raw("SELECT _id, _orgid, transporter_code_gen, transporter_unique_code, transporter_name, main_contact_person, telephone, mobile, fax, email, hq, city, state, country, pincode, GSTIN, PAN, introduction_date, notes, status, last_updated FROM transporter WHERE _orgid=? "+sqlString+" and status=1 ", _orgId).Values(&maps)
	if err == nil && num > 0 {
		for k, v := range maps {
			// if maps[k]["credit_in_days"] == nil {
			// 	maps[k]["credit_in_days"] = ""
			// }
			var s string
			if maps[k]["city"] != "" {
				s = maps[k]["city"].(string)
			}
			if maps[k]["state"] != "" {
				if maps[k]["city"] != "" {
					s = s + ", "
				}
				s = s + maps[k]["state"].(string)
			}
			if maps[k]["country"] != "" {
				if maps[k]["state"] != "" {
					s = s + ", "
				}
				s = s + maps[k]["country"].(string)
			}
			if maps[k]["pincode"] != "" {
				if maps[k]["country"] != "" {
					s = s + ", "
				}
				s = s + maps[k]["pincode"].(string)
			}
			maps[k]["city_state_country_pincode"] = s

			if maps[k]["introduction_date"] != "" && maps[k]["introduction_date"] != nil {
				maps[k]["introduction_date"] = convertDateString(maps[k]["introduction_date"].(string))
			}

			var warehousemaps []orm.Params
			warehouseCount, err1 := o.Raw("SELECT * FROM warehouse WHERE _orgid=? and transporter_fkid=?", _orgId, maps[k]["_id"]).Values(&warehousemaps)
			if warehouseCount > 0 && err1 == nil {
				var i int64 = 0
				for ; i < warehouseCount; i++ {
					var contactmaps []orm.Params
					contactCount, err12 := o.Raw("SELECT * FROM contact WHERE _orgid=? and transporter_fkid=? and warehouse_fkid=?", _orgId, maps[k]["_id"], warehousemaps[i]["_id"]).Values(&contactmaps)
					if contactCount > 0 && err12 == nil {
						//maps[k]["contacts"] = contactmaps
						if contactmaps[0]["contact_person_name"] != nil {
							warehousemaps[i]["contact_person_name"] = contactmaps[0]["contact_person_name"].(string)
						} else {
							warehousemaps[i]["contact_person_name"] = ""
						}
						if contactmaps[0]["designation"] != nil {
							warehousemaps[i]["designation"] = contactmaps[0]["designation"].(string)
						} else {
							warehousemaps[i]["designation"] = ""
						}
						if contactmaps[0]["mobile"] != nil {
							warehousemaps[i]["mobile"] = contactmaps[0]["mobile"].(string)
						} else {
							warehousemaps[i]["mobile"] = ""
						}
						if contactmaps[0]["phone"] != nil {
							warehousemaps[i]["phone"] = contactmaps[0]["phone"].(string)
						} else {
							warehousemaps[i]["phone"] = ""
						}
						if contactmaps[0]["email"] != nil {
							warehousemaps[i]["email"] = contactmaps[0]["email"].(string)
						} else {
							warehousemaps[i]["email"] = ""
						}
					}
				}
			}
			maps[k]["warehouses"] = warehousemaps

			var vehiclemaps []orm.Params
			_, err2 := o.Raw("SELECT * FROM vehicle WHERE _orgid=? and transporter_fkid=?", _orgId, maps[k]["_id"]).Values(&vehiclemaps)
			//if vehicleCount > 0 && err2 == nil {
			if err2 == nil {
				maps[k]["vehicles"] = vehiclemaps
			}

			key := "transporter_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func DeleteTransporter(transporter_id string, orgid string) string {
	result := "false"
	o := orm.NewOrm()
	//var values []orm.Params
	_, err := o.Raw("UPDATE transporter SET status=? WHERE _orgid=? and _id=? ", "0", orgid, transporter_id).Exec()
	if err != nil {
		//fmt.Println("Error in UPDATE")
	} else {
		result = "true"
	}
	return result
}
