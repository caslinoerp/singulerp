package models

import "github.com/astaxie/beego/orm"
import "strconv"

//"fmt"

type PrefixDropdownMaster struct {
	Id          int64
	DisplayText string
	Dropdown    string
	Status      int8
}

func GetPrefixDropdownMaster() map[string]orm.Params {

	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var resultMap []orm.Params

	num, err := o.Raw("SELECT pkid, dropdown, display_text, status from prefix_dropdown_master where status = 1").Values(&resultMap)

	if err == nil && num > 0 {
		for k, v := range resultMap {
			key := resultMap[k]["pkid"].(string)
			result[key] = v
		}
	} else {
		result["status"] = nil
	}
	return result
}

func GetPrefixCodesByOrganisation(orgID string, prefixMaster int) (map[string]orm.Params, int) {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var resultMap []orm.Params

	num, err := o.Raw("SELECT prefix_master_fkid, prefix_dropdown_master_fkid, position, value , direction from prefix_org_choice  where _orgid = ? and prefix_master_fkid = ? and status = 1", orgID, prefixMaster).Values(&resultMap)

	if err == nil && num > 0 {
		for k, v := range resultMap {
			key := resultMap[k]["position"].(string) + resultMap[k]["direction"].(string)
			result[key] = v
		}
		return result, 1
	}

	return result, 0
}

func GetPrefixCodesForLeft(orgID string, prefixMaster int) (map[int]orm.Params, int) {
	result := make(map[int]orm.Params)
	o := orm.NewOrm()
	var resultMap []orm.Params

	num, err := o.Raw("SELECT prefix_master_fkid, prefix_dropdown_master_fkid, position, value from prefix_org_choice  where _orgid = ? and prefix_master_fkid = ? and direction = 'L' and status = 1", orgID, prefixMaster).Values(&resultMap)

	if err == nil && num > 0 {
		for k, v := range resultMap {
			position := resultMap[k]["position"].(string)
			getValue, _ := strconv.ParseInt(position, 10, 64)
			key := int(getValue)
			result[key] = v
		}
		return result, 1
	}

	return result, 0
}

func GetPrefixCodesForRight(orgID string, prefixMaster int) (map[int]orm.Params, int) {
	result := make(map[int]orm.Params)
	o := orm.NewOrm()
	var resultMap []orm.Params

	num, err := o.Raw("SELECT prefix_master_fkid, prefix_dropdown_master_fkid, position, value from prefix_org_choice  where _orgid = ? and prefix_master_fkid = ? and direction = 'R' and status = 1", orgID, prefixMaster).Values(&resultMap)

	if err == nil && num > 0 {
		for k, v := range resultMap {
			position := resultMap[k]["position"].(string)
			getValue, _ := strconv.ParseInt(position, 10, 64)
			key := int(getValue)
			result[key] = v
		}
		return result, 1
	}

	return result, 0
}

func InsertPrefixCodesByOrganisation(orgID string, queryParamMap map[string]string) int {

	o := orm.NewOrm()
	ormTransactionalErr := o.Begin()
	_, err := o.Raw("UPDATE prefix_org_choice set status = 0 where _orgid = ? and prefix_master_fkid = ? ", orgID, queryParamMap["prefix_master_fkid"]).Exec()
	if err != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}

	var leftVal string
	leftVal = ""
	if queryParamMap["1l"] != "" {
		leftVal = "7"
	}

	var rightVal string
	rightVal = ""
	if queryParamMap["1r"] != "" {
		rightVal = "7"
	}

	_, err1 := o.Raw("INSERT into prefix_org_choice (_orgid,prefix_master_fkid,prefix_dropdown_master_fkid,position,value,added_by_fkid, direction) values (?,?,?,?,?,?,?) ", orgID, queryParamMap["prefix_master_fkid"], leftVal, "1", queryParamMap["1l"], queryParamMap["added_by_fkid"], "L").Exec()
	if err1 != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}
	_, err2 := o.Raw("INSERT into prefix_org_choice (_orgid,prefix_master_fkid,prefix_dropdown_master_fkid,position,value,added_by_fkid, direction) values (?,?,?,?,?,?,?) ", orgID, queryParamMap["prefix_master_fkid"], rightVal, "1", queryParamMap["1r"], queryParamMap["added_by_fkid"], "R").Exec()
	if err2 != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}
	_, err3 := o.Raw("INSERT into prefix_org_choice (_orgid,prefix_master_fkid,prefix_dropdown_master_fkid,position,value,added_by_fkid, direction) values (?,?,?,?,?,?,?) ", orgID, queryParamMap["prefix_master_fkid"], queryParamMap["2l"], "2", "", queryParamMap["added_by_fkid"], "L").Exec()
	if err3 != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}
	_, err4 := o.Raw("INSERT into prefix_org_choice (_orgid,prefix_master_fkid,prefix_dropdown_master_fkid,position,value,added_by_fkid, direction) values (?,?,?,?,?,?,?) ", orgID, queryParamMap["prefix_master_fkid"], queryParamMap["2r"], "2", "", queryParamMap["added_by_fkid"], "R").Exec()
	if err4 != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}
	_, err5 := o.Raw("INSERT into prefix_org_choice (_orgid,prefix_master_fkid,prefix_dropdown_master_fkid,position,value,added_by_fkid,direction) values (?,?,?,?,?,?,?) ", orgID, queryParamMap["prefix_master_fkid"], queryParamMap["3l"], "3", "", queryParamMap["added_by_fkid"], "L").Exec()
	if err5 != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}
	_, err6 := o.Raw("INSERT into prefix_org_choice (_orgid,prefix_master_fkid,prefix_dropdown_master_fkid,position,value,added_by_fkid,direction) values (?,?,?,?,?,?,?) ", orgID, queryParamMap["prefix_master_fkid"], queryParamMap["3r"], "3", "", queryParamMap["added_by_fkid"], "R").Exec()
	if err6 != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}
	if ormTransactionalErr != nil {
		return 0
	}
	ormTransactionalErr = o.Commit()
	return 1
}
