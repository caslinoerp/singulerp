package models

import (
	//"fmt"
    //"strings"
    "strconv"
	"github.com/astaxie/beego/orm"
	"time"
)

type ConfigureM struct {
	Id                 int64
	ReportNo           string
	PartyName          string
	InvoiceNo          string
	Date               string
	Product            string
	Product_sr_no      string
	Material           string
	Description        string
	Observations       string
	Checking           string
	Remark             string
	Orgid              string
	Userid 			   string
	Username 		   string
}

type ReportM struct {
	Id                 int64
	ReportNo           string
	Product            string
	Product_sr_no      string
	Operations         string
	Remark             string
	Orgid              string
	Userid 			   string
	Username 		   string
}

type MasterReport struct {
	Id                 int64
	ReportName         string
	ReportType         string
	ReportCode         string
	Status             string
	FieldName1         string
	FieldType1         string
	FieldName2         string
	FieldType2         string
	FieldName3         string
	FieldType3         string
	FieldName4         string
	FieldType4         string
	FieldName5         string
	FieldType5         string
	FieldName6         string
	FieldType6         string
	FieldName7         string
	FieldType7         string
	FieldName8         string
	FieldType8         string
	FieldName9         string
	FieldType9         string
	FieldName10        string
	FieldType10        string
	FieldName11        string
	FieldType11        string
	FieldName12        string
	FieldType12        string
	FieldName13        string
	FieldType13        string
	FieldName14        string
	FieldType14        string
	FieldName15        string
	FieldType15        string
	ListOrder          string
	Orgid              string
    Userid 			   string
	Username 		   string
	Report             string
}

func Configure(formData ConfigureM) int {
	
	o := orm.NewOrm()
   _, err := o.Raw("INSERT INTO qa_inspection_predelivery (report_no, party_name, invoice_no, date, product, product_sr_no, material, description, observation, checking, remark,_orgid) VALUES (?,?,?,?,?,?,?,?,?,?,?,?) ", formData.ReportNo, formData.PartyName,formData.InvoiceNo,formData.Date,formData.Product,formData.Product_sr_no,formData.Material,formData.Description,formData.Observations,formData.Checking,formData.Checking,formData.Orgid).Exec()
   
   if err == nil {
		return 1
	}
	return 0
}

func CreateReport(formData ReportM) int {
	
	o := orm.NewOrm()
    _, err := o.Raw("INSERT INTO qa_inspection_item (report_no, product, product_sr_no, operations_performed, remark,_orgid) VALUES (?,?,?,?,?,?) ", formData.ReportNo, formData.Product,formData.Product_sr_no,formData.Operations,formData.Remark,formData.Orgid).Exec()
    if err == nil {
		return 1
	}
	return 0
}

func GetConfigure(formData ConfigureM) (map[string]orm.Params){
	// var stock []Stock
	// o := orm.NewOrm()	
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	//num, err := o.Raw("SELECT * FROM item_master ").Values(&maps)
	num, err := o.Raw("SELECT *, date as q_date from qa_inspection_predelivery where _orgid=?",formData.Orgid).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		var i int64 = 0
		for ; i<num; i++ {
			//fmt.Println(maps[i])
		}
		for k, v:= range maps{
			key := "qa_" + strconv.Itoa(k)
			result[key] = v
		}		
		
	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetReport(formData ReportM) (map[string]orm.Params){
	// var stock []Stock
	// o := orm.NewOrm()	
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	//num, err := o.Raw("SELECT * FROM item_master ").Values(&maps)
	num, err := o.Raw("SELECT * from qa_inspection_item where _orgid=?",formData.Orgid).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		var i int64 = 0
		for ; i<num; i++ {
			//fmt.Println(maps[i])
		}
		for k, v:= range maps{
			key := "qar_" + strconv.Itoa(k)
			result[key] = v
		}		
		
	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func CreateMasterReport(formData MasterReport) int {
	
	o := orm.NewOrm()
   _, err := o.Raw("INSERT INTO reports_master (name, unique_code, type, created_on, created_by, status, _orgid) VALUES (?,?,?,?,?,?,?) ", formData.ReportName, formData.ReportCode,formData.ReportType,time.Now(),formData.Userid,"1",formData.Orgid).Exec()
   var maps []orm.Params
	_, err1 := o.Raw("SELECT pkid FROM reports_master WHERE _orgid=? and unique_code=?", formData.Orgid, formData.ReportCode).Values(&maps)
	
	if (formData.FieldName1 != "") {
	
	_, err01 := o.Raw("INSERT INTO reports_fields (reports_master_fkid, field_name, field_type, listing_order, status, _orgid) VALUES (?,?,?,?,?,?) ", maps[0]["pkid"], formData.FieldName1,formData.FieldType1,"1","1",formData.Orgid).Exec()
   if err01 != nil { 
 			//fmt.Println("Error in Item queries")
 		}
   }

   if (formData.FieldName2 != "") {
	
	_, err02 := o.Raw("INSERT INTO reports_fields (reports_master_fkid, field_name, field_type, listing_order, status, _orgid) VALUES (?,?,?,?,?,?) ", maps[0]["pkid"], formData.FieldName2,formData.FieldType2,"2","1",formData.Orgid).Exec()
   if err02 != nil { 
 			//fmt.Println("Error in Item queries")
 		}
   }

   if (formData.FieldName3 != "") {
	
	_, err03 := o.Raw("INSERT INTO reports_fields (reports_master_fkid, field_name, field_type, listing_order, status, _orgid) VALUES (?,?,?,?,?,?) ", maps[0]["pkid"], formData.FieldName3,formData.FieldType3,"3","1",formData.Orgid).Exec()
   if err03 != nil { 
 			//fmt.Println("Error in Item queries")
 		}
   }

   if (formData.FieldName4 != "") {
	
	_, err04 := o.Raw("INSERT INTO reports_fields (reports_master_fkid, field_name, field_type, listing_order, status, _orgid) VALUES (?,?,?,?,?,?) ", maps[0]["pkid"], formData.FieldName4,formData.FieldType4,"4","1",formData.Orgid).Exec()
   if err04 != nil { 
 			//fmt.Println("Error in Item queries")
 		}
   }

   if (formData.FieldName5 != "") {
	
	_, err05 := o.Raw("INSERT INTO reports_fields (reports_master_fkid, field_name, field_type, listing_order, status, _orgid) VALUES (?,?,?,?,?,?) ", maps[0]["pkid"], formData.FieldName5,formData.FieldType5,"5","1",formData.Orgid).Exec()
   if err05 != nil { 
 			//fmt.Println("Error in Item queries")
 		}
   }

   if (formData.FieldName6 != "") {
	
	_, err06 := o.Raw("INSERT INTO reports_fields (reports_master_fkid, field_name, field_type, listing_order, status, _orgid) VALUES (?,?,?,?,?,?) ", maps[0]["pkid"], formData.FieldName6,formData.FieldType6,"6","1",formData.Orgid).Exec()
   if err06 != nil { 
 			//fmt.Println("Error in Item queries")
 		}
   }

   if (formData.FieldName7 != "") {
	
	_, err07 := o.Raw("INSERT INTO reports_fields (reports_master_fkid, field_name, field_type, listing_order, status, _orgid) VALUES (?,?,?,?,?,?) ", maps[0]["pkid"], formData.FieldName7,formData.FieldType7,"7","1",formData.Orgid).Exec()
   if err07 != nil { 
 			//fmt.Println("Error in Item queries")
 		}
   }

   if (formData.FieldName8 != "") {
	
	_, err08 := o.Raw("INSERT INTO reports_fields (reports_master_fkid, field_name, field_type, listing_order, status, _orgid) VALUES (?,?,?,?,?,?) ", maps[0]["pkid"], formData.FieldName8,formData.FieldType8,"8","1",formData.Orgid).Exec()
   if err08 != nil { 
 			//fmt.Println("Error in Item queries")
 		}
   }

   if (formData.FieldName9 != "") {
	
	_, err09 := o.Raw("INSERT INTO reports_fields (reports_master_fkid, field_name, field_type, listing_order, status, _orgid) VALUES (?,?,?,?,?,?) ", maps[0]["pkid"], formData.FieldName9,formData.FieldType9,"9","1",formData.Orgid).Exec()
   if err09 != nil { 
 			//fmt.Println("Error in Item queries")
 		}
   }

   if (formData.FieldName10 != "") {
	
	_, err010 := o.Raw("INSERT INTO reports_fields (reports_master_fkid, field_name, field_type, listing_order, status, _orgid) VALUES (?,?,?,?,?,?) ", maps[0]["pkid"], formData.FieldName10,formData.FieldType10,"10","1",formData.Orgid).Exec()
   if err010 != nil { 
 			//fmt.Println("Error in Item queries")
 		}
   }

   if (formData.FieldName11 != "") {
	
	_, err011 := o.Raw("INSERT INTO reports_fields (reports_master_fkid, field_name, field_type, listing_order, status, _orgid) VALUES (?,?,?,?,?,?) ", maps[0]["pkid"], formData.FieldName11,formData.FieldType11,"11","1",formData.Orgid).Exec()
   if err011 != nil { 
 			//fmt.Println("Error in Item queries")
 		}
   }

   if (formData.FieldName12 != "") {
	
	_, err012 := o.Raw("INSERT INTO reports_fields (reports_master_fkid, field_name, field_type, listing_order, status, _orgid) VALUES (?,?,?,?,?,?) ", maps[0]["pkid"], formData.FieldName12,formData.FieldType12,"12","1",formData.Orgid).Exec()
   if err012 != nil { 
 			//fmt.Println("Error in Item queries")
 		}
   }

   if (formData.FieldName13 != "") {
	
	_, err013 := o.Raw("INSERT INTO reports_fields (reports_master_fkid, field_name, field_type, listing_order, status, _orgid) VALUES (?,?,?,?,?,?) ", maps[0]["pkid"], formData.FieldName13,formData.FieldType13,"13","1",formData.Orgid).Exec()
   if err013 != nil { 
 			//fmt.Println("Error in Item queries")
 		}
   }

   if (formData.FieldName14 != "") {
	
	_, err014 := o.Raw("INSERT INTO reports_fields (reports_master_fkid, field_name, field_type, listing_order, status, _orgid) VALUES (?,?,?,?,?,?) ", maps[0]["pkid"], formData.FieldName14,formData.FieldType14,"14","1",formData.Orgid).Exec()
   if err014 != nil { 
 			//fmt.Println("Error in Item queries")
 		}
   }
   if (formData.FieldName15 != "") {
	
	_, err015 := o.Raw("INSERT INTO reports_fields (reports_master_fkid, field_name, field_type, listing_order, status, _orgid) VALUES (?,?,?,?,?,?) ", maps[0]["pkid"], formData.FieldName15,formData.FieldType15,"15","1",formData.Orgid).Exec()
   if err015 != nil { 
 			//fmt.Println("Error in Item queries")
 		}
   }
	
   if err == nil && err1 == nil {
		return 1
	}
	return 0
}

func GetMasterReport(formData MasterReport) (map[string]orm.Params){
	// var stock []Stock
	// o := orm.NewOrm()	
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	//num, err := o.Raw("SELECT * FROM item_master ").Values(&maps)
	num, err := o.Raw("SELECT rm.pkid,rm.name,rm.unique_code,rm.type, rm.created_on as c_date from reports_master as rm where rm._orgid=? ",formData.Orgid).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		var i int64 = 0
		for ; i<num; i++ {
			//fmt.Println(maps[i])
		}
		for k, v:= range maps{
			key := "qar_" + strconv.Itoa(k)
			result[key] = v
		}		
		
	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetReportFormat(product_id string, orgid string) ([]string, []string, string, []string) {

	status := "false"
	var field_name []string
	var field_type []string
	var listing_order []string
	
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT rf.field_name,rf.field_type,rf.listing_order FROM reports_fields as rf,reports_master as rm WHERE rm.pkid=rf.reports_master_fkid and rf.reports_master_fkid=? and rm._orgid=? and rf._orgid=? order by rf.listing_order", product_id, orgid,orgid).Values(&maps)
	//fmt.Println("num=", num)
	var i int64 = 0
	for ; i < num; i++ {
		//fmt.Println(maps[i])
	}
	if err == nil {
		var new_field_name string
		var new_field_type string
		//var new_field_type2 string
		var new_listing_order string
		
		var j int64 = 0
		for ; j < num; j++ {
			new_field_name = maps[j]["field_name"].(string)
			field_name = append(field_name, new_field_name)

			new_field_type = maps[j]["field_type"].(string)
			if new_field_type == "1"{
				new_field_type2 := "Product"
				field_type = append(field_type, new_field_type2)
			}else if new_field_type == "2"{
				new_field_type2 := "Items"
				field_type = append(field_type, new_field_type2)
			}else if new_field_type == "3"{
				new_field_type2 := "GRN"
				field_type = append(field_type, new_field_type2)
			}else if new_field_type == "4"{
				new_field_type2 := "Suppliers"
				field_type = append(field_type, new_field_type2)
			}else if new_field_type == "5"{
				new_field_type2 := "Customers"
				field_type = append(field_type, new_field_type2)
			}else if new_field_type == "6"{
				new_field_type2 := "PO"
				field_type = append(field_type, new_field_type2)
			}else if new_field_type == "7"{
				new_field_type2 := "SO"
				field_type = append(field_type, new_field_type2)
			}else if new_field_type == "8"{
				new_field_type2 := "Production Order"
				field_type = append(field_type, new_field_type2)
			}else if new_field_type == "9"{
				new_field_type2 := "Ticket"
				field_type = append(field_type, new_field_type2)
			}else if new_field_type == "10"{
				new_field_type2 := "Employees"
				field_type = append(field_type, new_field_type2)
			}else if new_field_type == "11"{
				new_field_type2 := "Textbox"
				field_type = append(field_type, new_field_type2)
			}else if new_field_type == "12"{
				new_field_type2 := "Date"
				field_type = append(field_type, new_field_type2)
			}          
			

			new_listing_order = maps[j]["listing_order"].(string)
			listing_order = append(listing_order, new_listing_order)

			
		}
		status = "true"
	}
//	fmt.Println("***items=", field_name)
//	fmt.Println("***quantity=", field_type)
	// if field_type == "1"{
	// 	s := "Product"
	// }else if field_type == "2" {
	// 	s := "Items"
 //    }else if field_type == "3" {
	// 	s := "GRN"
	// }else if field_type == "4" {
	// 	s := "Suppliers"
	// }else if field_type == "5" {
	// 	s := "Customers"
	// }else if field_type == "6" {
	// 	s := "PO"
	// }else if field_type == "7" {
	// 	s := "SO"
	// }else if field_type == "8" {
	// 	s := "Production Order"
	// }else if field_type == "9" {
	// 	s := "Ticket"
	// }else if field_type == "10" {
	// 	s := "Employees"
	// }else if field_type == "11" {
	// 	s := "Textbox"
	// }else if field_type == "12" {
	// 	s := "Date"
	// }
	return field_name, field_type, status, listing_order
}

func CheckForExistingCode(code string ,orgid string) int {

	o := orm.NewOrm()
	var maps []orm.Params
	num, _ := o.Raw("SELECT * FROM reports_master where unique_code= ? and _orgid=? ", code,orgid).Values(&maps)
	if num > 0 {
		return 1
	}
	return 0
}

func CheckForExistingName(name string ,orgid string) int {

	o := orm.NewOrm()
	var maps []orm.Params
	num, _ := o.Raw("SELECT * FROM reports_master where name= ? and _orgid=? ", name,orgid).Values(&maps)
	if num > 0 {
		return 1
	}
	return 0
}

func GetMasterReport2(formData MasterReport) (map[string]orm.Params){
	// var stock []Stock
	// o := orm.NewOrm()	
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	//num, err := o.Raw("SELECT * FROM item_master ").Values(&maps)
	num, err := o.Raw("SELECT rm.pkid,rm.name,rm.unique_code,rm.type, rm.created_on as c_date from reports_master as rm where rm._orgid=? ",formData.Orgid).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		var i int64 = 0
		for ; i<num; i++ {
			//fmt.Println(maps[i])
		}
		for k, v:= range maps{
			key := "qar_" + strconv.Itoa(k)
			result[key] = v
		}		
		
	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetMasterFields(formData MasterReport) (map[string]orm.Params){
	// var stock []Stock
	// o := orm.NewOrm()	
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	//num, err := o.Raw("SELECT * FROM item_master ").Values(&maps)
	num, err := o.Raw("SELECT rf.pkid,rf.field_name,rf.field_type,rf.listing_order from reports_fields as rf where rf._orgid=? and rf.reports_master_fkid=? order_by listing_order",formData.Orgid,formData.Report).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		var i int64 = 0
		for ; i<num; i++ {
			//fmt.Println(maps[i])
		}
		for k, v:= range maps{
			key := "qar_" + strconv.Itoa(k)
			result[key] = v
		}		
		
	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}