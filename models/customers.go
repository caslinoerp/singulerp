package models

import (
	"time"
	// "strings"
	"strconv"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
)

// use this one everywhere
type CustomerDetails struct {
	CustomerID    string
	UniqueCode    string
	CompanyName   string
	Group         string
	Address       string
	City          string
	State         string
	Country       string
	Pincode       string
	TIN           string
	AreaCode      string
	CST           string
	PAN           string
	ECC           string
	CreditInDays  string
	CreditLimit   string
	CustomerType  string
	Introduction  string
	GST           string
	PGST          string
	MainGSTIN     string
	ARN           string
	ServiceLoc    string
	CustomerCheck string
}

type Customer struct {
	Orgid       string
	Userid      string
	Username    string
	Id          int64
	UniqueCode  string
	CompanyName string
	Group       string
	Address     string
	City        string
	State       string
	Country     string
	Pincode     string
	TIN         string
	AreaCode    string
	CST         string
	PAN         string
	ECC         string
	//LST               string `form:"lst" valid:"Required"`
	CreditInDays     string
	CreditLimit      string
	CustomerType     string
	Introduction     string
	ServiceLoc       string
	ContactPerson    string
	Designation      string
	Email            string
	PhoneNo          string
	CellNo           string
	GST              string
	PGST             string
	MainGSTIN        string
	ARN              string
	CustomerID       string
	WarehouseAddress string
	WCity            string
	WState           string
	WCountry         string
	WPincode         string
	GSTIN            string
	CustomerCheck    string
	IsPresentinDB    int
	NewGenCode       string
	CustomerUniqueSequence string
}

type Viewassoc2 struct {
	Orgid    string
	Userid   string
	Username string
	Customer string
	Products []string
	//Status             string
}

func CheckForExistingCustomerName(customerName string, orgID string) int {
	o := orm.NewOrm()
	var resultMap []orm.Params
	num, err := o.Raw("SELECT company_name FROM customer WHERE company_name=? and  _orgid=? and status = 1", customerName, orgID).Values(&resultMap)
	//fmt.Println("Query Check Name==========", num)
	if err == nil && num > 0 {
		return 1
	}
	return 0
}

func CheckForExistingCustomerCode(customerCode string, orgID string) int {
	o := orm.NewOrm()
	var resultMap []orm.Params
	num, err := o.Raw("SELECT customer_unique_code FROM customer WHERE customer_unique_code=? and _orgid=? and status = 1", customerCode, orgID).Values(&resultMap)
	//fmt.Println("Query Check Name==========", num)
	if err == nil && num > 0 {
		return 1
	}
	return 0
}

func AddNewCustomer(formData Customer, warehouses []map[string]string, contactpersons []map[string]string) int {

	o := orm.NewOrm()
	ormTransactionalErr := o.Begin()

	//fmt.Println("==============", formData.Introduction)
	var introDate *string
	introDate = nil
	if formData.Introduction != "" {
		tempIntroDate := formData.Introduction
		introDate = &tempIntroDate
	}
	var credit_in_days *string
	credit_in_days = nil
	if formData.CreditInDays != "" {
		tempCreditDays := formData.CreditInDays
		credit_in_days = &tempCreditDays
	}
	var credit_limit *string
	credit_limit = nil
	if formData.CreditLimit != "" {
		tempCreditLimit := formData.CreditLimit
		credit_limit = &tempCreditLimit
	}
	var gst *string
	gst = nil
	if formData.GST != "" {
		tempgst := formData.GST
		gst = &tempgst
	}
	var pgst *string
	pgst = nil
	if formData.PGST != "" {
		temppgst := formData.PGST
		pgst = &temppgst
	}
	var arn *string
	arn = nil
	if formData.ARN != "" {
		temparn := formData.ARN
		arn = &temparn
	}
	// var contactperson *string
	// contactperson = nil
	// if formData.ContactPerson != "" {
	// 	tempcontactperson := formData.ContactPerson
	// 	contactperson = &tempcontactperson
	// }
	// var designation *string
	// designation = nil
	// if formData.Designation != "" {
	// 	tempdesignation := formData.Designation
	// 	designation = &tempdesignation
	// }
	// var phoneno *string
	// phoneno = nil
	// if formData.PhoneNo != "" {
	// 	tempphoneno := formData.PhoneNo
	// 	phoneno = &tempphoneno
	// }
	// var cellno *string
	// cellno = nil
	// if formData.CellNo != "" {
	// 	tempcellno := formData.CellNo
	// 	cellno = &tempcellno
	// }
	// var email *string
	// email = nil
	// if formData.Email != "" {
	// 	tempemail := formData.Email
	// 	email = &tempemail
	// }

	//fmt.Println("==============", introDate)
	_, err := o.Raw("INSERT INTO customer (_orgid, customer_unique_code, company_name, grp, address, city, state, pincode, country, TIN, area_code, PAN, credit_in_days, CST, credit_limit, ECC, customer_type,date_of_introduction,service_location,status, GSTIN, provisional_gst,ARN, customer_code_gen) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ", formData.Orgid, formData.UniqueCode, formData.CompanyName, formData.Group, formData.Address, formData.City, formData.State, formData.Pincode, formData.Country, formData.TIN, formData.AreaCode, formData.PAN, &credit_in_days, formData.CST, &credit_limit, formData.ECC, formData.CustomerType, &introDate, formData.ServiceLoc, "1", &gst, &pgst, &arn, formData.NewGenCode).Exec()
	if err != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}

	var maps []orm.Params
	num, err1 := o.Raw("SELECT _id FROM customer WHERE _orgid=? and company_name=?", formData.Orgid, formData.CompanyName).Values(&maps)
	if err1 != nil || num < 1 {
		ormTransactionalErr = o.Rollback()
		return 0
	}

	// type_address=1 for billing address
	_, err2 := o.Raw("INSERT INTO customer_warehouse_address (customer_fkid, _orgid, address_line1, wcity, wstate, wcountry, wpincode, type_address, status, lastupdated, GSTIN) VALUES (?,?,?,?,?,?,?,?,?,?,?)", maps[0]["_id"], formData.Orgid, formData.Address, formData.City, formData.State, formData.Country, formData.Pincode, "1", "1", time.Now(), formData.MainGSTIN).Exec()
	if err2 != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}

	for cnum, itt := range contactpersons {
		if itt["contact_person"] != "" {
			var designation *string
			designation = nil
			if itt["designation"] != "" {
				tempdesignation := itt["designation"]
				designation = &tempdesignation
			}
			var phoneno *string
			phoneno = nil
			if itt["phone_no"] != "" {
				tempphoneno := itt["phone_no"]
				phoneno = &tempphoneno
			}
			var cellno *string
			cellno = nil
			if itt["cell_no"] != "" {
				tempcellno := itt["cell_no"]
				cellno = &tempcellno
			}
			var email *string
			email = nil
			if itt["email"] != "" {
				tempemail := itt["email"]
				email = &tempemail
			}
			// var maps01 []orm.Params
			// count, err03 := o.Raw("SELECT _id FROM customer_contact_person WHERE _customer_fkid=? and _orgid=? and name=? and designation=? and phone=? and cell_no=? and email=? ", maps[0]["_id"], formData.Orgid, itt["contact_person"], &designation, &phoneno, &cellno, &email).Values(&maps01)
			// if err03 != nil {
			// 	ormTransactionalErr = o.Rollback()
			// 	return 0
			// }

			var contact_type string
			if cnum == 0 {
				contact_type = "1"
			} else {
				contact_type = "2"
			}
			_, err3 := o.Raw("INSERT INTO customer_contact_person (_orgid, name, designation, phone, cell_no, email, contact_type, _customer_fkid) VALUES (?,?,?,?,?,?,?,?) ", formData.Orgid, itt["contact_person"], &designation, &phoneno, &cellno, &email, contact_type, maps[0]["_id"]).Exec()
			if err3 != nil {
				ormTransactionalErr = o.Rollback()
				return 0
			}
		}
	}

	for _, it := range warehouses {
		if it["warehouse_address"] != "" {
			// var maps1 []orm.Params
			// count, err4 := o.Raw("SELECT _id FROM customer_warehouse_address WHERE customer_fkid=? and _orgid=? and address_line1=? and wcity=? and wstate=? and wcountry=? and wpincode=? and GSTIN=? and status=?", maps[0]["_id"], formData.Orgid, it["warehouse_address"], it["wcity"], it["wstate"], it["wcountry"], it["wpincode"], it["gstin"], "1").Values(&maps1)
			// if err4 != nil {
			// 	ormTransactionalErr = o.Rollback()
			// 	return 0
			// }
			// if count < 1 {

			// type_address=2 for warehouse address
			_, err5 := o.Raw("INSERT INTO customer_warehouse_address (customer_fkid, _orgid, address_line1, wcity, wstate, wcountry, wpincode, type_address, status, lastupdated, GSTIN) VALUES (?,?,?,?,?,?,?,?,?,?,?)", maps[0]["_id"], formData.Orgid, it["warehouse_address"], it["wcity"], it["wstate"], it["wcountry"], it["wpincode"], "2", "1", time.Now(), it["gstin"]).Exec()
			if err5 != nil {
				ormTransactionalErr = o.Rollback()
				return 0
			}
			//}
		}
	}

	if ormTransactionalErr != nil {
		return 0
	}
	ormTransactionalErr = o.Commit()
	return 1
}

func AddImportCustomer(formData Customer) int {

	o := orm.NewOrm()
	ormTransactionalErr := o.Begin()

	//fmt.Println("==============", formData.Introduction)
	var introDate *string
	introDate = nil
	if formData.Introduction != "" {
		tempIntroDate := formData.Introduction
		introDate = &tempIntroDate
	}
	var credit_in_days *string
	credit_in_days = nil
	if formData.CreditInDays != "" {
		tempCreditDays := formData.CreditInDays
		credit_in_days = &tempCreditDays
	}
	var credit_limit *string
	credit_limit = nil
	if formData.CreditLimit != "" {
		tempCreditLimit := formData.CreditLimit
		credit_limit = &tempCreditLimit
	}
	var gst *string
	gst = nil
	if formData.GST != "" {
		tempgst := formData.GST
		gst = &tempgst
	}
	var pgst *string
	pgst = nil
	if formData.PGST != "" {
		temppgst := formData.PGST
		pgst = &temppgst
	}
	var arn *string
	arn = nil
	if formData.ARN != "" {
		temparn := formData.ARN
		arn = &temparn
	}
	var contactperson *string
	contactperson = nil
	if formData.ContactPerson != "" {
		tempcontactperson := formData.ContactPerson
		contactperson = &tempcontactperson
	}
	var designation *string
	designation = nil
	if formData.Designation != "" {
		tempdesignation := formData.Designation
		designation = &tempdesignation
	}
	var phoneno *string
	phoneno = nil
	if formData.PhoneNo != "" {
		tempphoneno := formData.PhoneNo
		phoneno = &tempphoneno
	}
	var cellno *string
	cellno = nil
	if formData.CellNo != "" {
		tempcellno := formData.CellNo
		cellno = &tempcellno
	}
	var email *string
	email = nil
	if formData.Email != "" {
		tempemail := formData.Email
		email = &tempemail
	}

	//fmt.Println("==============", introDate)
	_, err := o.Raw("INSERT INTO customer (_orgid, customer_code_gen, customer_unique_code, company_name, grp, address, city, state, pincode, country, TIN, area_code, PAN, credit_in_days, CST, credit_limit, ECC, customer_type, date_of_introduction, service_location, status, GSTIN, provisional_gst,ARN) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ", formData.Orgid, formData.UniqueCode, formData.CustomerUniqueSequence, formData.CompanyName, formData.Group, formData.Address, formData.City, formData.State, formData.Pincode, formData.Country, formData.TIN, formData.AreaCode, formData.PAN, &credit_in_days, formData.CST, &credit_limit, formData.ECC, formData.CustomerType, &introDate, formData.ServiceLoc, "1", &gst, &pgst, &arn).Exec()
	if err != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}

	var maps []orm.Params
	num, err2 := o.Raw("SELECT _id FROM customer WHERE _orgid=? and company_name=?", formData.Orgid, formData.CompanyName).Values(&maps)
	if err2 != nil || num < 1 {
		ormTransactionalErr = o.Rollback()
		return 0
	}

	// type_address=1 for billing address
	_, err02 := o.Raw("INSERT INTO customer_warehouse_address (customer_fkid, _orgid, address_line1, wcity, wstate, wcountry, wpincode, type_address, status, lastupdated, GSTIN) VALUES (?,?,?,?,?,?,?,?,?,?,?)", maps[0]["_id"], formData.Orgid, formData.Address, formData.City, formData.State, formData.Country, formData.Pincode, "1", "1", time.Now(), formData.GST).Exec()
	if err02 != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}

	_, err3 := o.Raw("INSERT INTO customer_contact_person (_orgid, name, designation, phone, cell_no, email, contact_type, _customer_fkid) VALUES (?,?,?,?,?,?,?,?) ", formData.Orgid, &contactperson, &designation, &phoneno, &cellno, &email, "1", maps[0]["_id"]).Exec()
	if err3 != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}

	if ormTransactionalErr != nil {
		return 0
	}
	ormTransactionalErr = o.Commit()
	return 1
}

func InsertCustomerWarehouses(formData Customer) int {
	var waddress *string
	waddress = nil
	if formData.WarehouseAddress != "" {
		tempwaddress := formData.WarehouseAddress
		waddress = &tempwaddress
	}
	var wcity *string
	wcity = nil
	if formData.WCity != "" {
		tempwcity := formData.WCity
		wcity = &tempwcity
	}
	var wstate *string
	wstate = nil
	if formData.WState != "" {
		tempwstate := formData.WState
		wstate = &tempwstate
	}
	var wpincode *string
	wpincode = nil
	if formData.WPincode != "" {
		tempwpincode := formData.WPincode
		wpincode = &tempwpincode
	}
	var wcountry *string
	wcountry = nil
	if formData.WCountry != "" {
		tempwcountry := formData.WCountry
		wcountry = &tempwcountry
	}
	var wgst *string
	wgst = nil
	if formData.GSTIN != "" {
		tempwgst := formData.GSTIN
		wgst = &tempwgst
	}
	o := orm.NewOrm()
	var maps []orm.Params
	num, err1 := o.Raw("SELECT _id FROM customer WHERE _orgid=? and company_name=? and address=? and city=? and state=? and pincode=? and country=? and status=?", formData.Orgid, formData.CompanyName, formData.Address, formData.City, formData.State, formData.Pincode, formData.Country, "1").Values(&maps)
	if err1 == nil && num > 0 {
		var maps1 []orm.Params
		count, err2 := o.Raw("SELECT _id FROM customer_warehouse_address WHERE customer_fkid=? and _orgid=? and address_line1=? and wcity=? and wstate=? and wcountry=? and wpincode=? and GSTIN=? and status=?", maps[0]["_id"], formData.Orgid, &waddress, &wcity, &wstate, &wcountry, &wpincode, &wgst, "1").Values(&maps1)
		if err2 == nil {
			if count < 1 {
				_, err3 := o.Raw("INSERT INTO customer_warehouse_address (customer_fkid, _orgid, address_line1, wcity, wstate, wcountry, wpincode, type_address, status, lastupdated, GSTIN) VALUES (?,?,?,?,?,?,?,?,?,?,?)", maps[0]["_id"], formData.Orgid, &waddress, &wcity, &wstate, &wcountry, &wpincode, "1", "1", time.Now(), &wgst).Exec()
				if err3 != nil {
					return 0
				}
			}
		} else {
			return 0
		}
	} else {
		return 0
	}
	return 1
}

func DeleteCustomer(customer_id string, orgid string) string {
	result := "false"
	o := orm.NewOrm()
	//var values []orm.Params
	_, err := o.Raw("UPDATE customer SET status=? WHERE _orgid=? and _id=? ", "0", orgid, customer_id).Exec()
	if err != nil {
		//fmt.Println("Error in UPDATE")
	} else {
		result = "true"
	}
	return result
}

func GetCustomer(formData Customer) map[string]orm.Params {

	result := make(map[string]orm.Params)

	o := orm.NewOrm()
	var maps []orm.Params
	//num, err := o.Raw("SELECT * FROM item_master ").Values(&maps)
	//num, err := o.Raw("SELECT c._id, c.company_name, c.grp, c.address, c.city, c.state, c.pincode, c.country, c.TIN, c.area_code, c.PAN, c.credit_in_days,c.credit_limit,c.CST, c.ECC, c.customer_type, DATE(c.date_of_introduction) as i_date, c.service_location,c.gst,c.provisional_gst,c.ARN, cp.name, cp.designation, cp.phone, cp.cell_no, cp.email, cp.contact_type, cw.address_line1, cw.wcity, cw.wstate, cw.wcountry, cw.wpincode, cw.type_address FROM customer as c,customer_contact_person as cp,customer_warehouse_address as cw where cp._customer_fkid=c._id and cw.customer_fkid=c._id and c._orgid=? and c.status=?", formData.Orgid, "1").Values(&maps)
	num, err := o.Raw("SELECT c._id, c.customer_unique_code, c.company_name, c.grp, c.address, c.city, c.state, c.pincode, c.country, c.TIN, c.area_code, c.PAN, c.credit_in_days,c.credit_limit,c.CST, c.ECC, c.customer_type, c.date_of_introduction as i_date, c.service_location, c.provisional_gst,c.ARN, c.GSTIN, cp.name, cp.designation, cp.phone, cp.cell_no, cp.email FROM customer as c, customer_contact_person as cp WHERE c._orgid=? and cp._orgid=? and c.status=1 and cp.contact_type=1 and cp._customer_fkid=c._id", formData.Orgid, formData.Orgid).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		//var i int64 = 0
		//for ; i < num; i++ {
		//fmt.Println(maps[i])
		//}
		for k, v := range maps {
			if maps[k]["credit_in_days"] == nil {
				maps[k]["credit_in_days"] = ""
			}
			if maps[k]["credit_limit"] == nil {
				maps[k]["credit_limit"] = ""
			}
			if maps[k]["i_date"] == nil {
				maps[k]["i_date"] = ""
			}
			if maps[k]["i_date"] != "" && maps[k]["i_date"] != nil {
				maps[k]["i_date"] = convertDateString(maps[k]["i_date"].(string))
			}
			if maps[k]["gst"] == nil {
				maps[k]["gst"] = ""
			}
			if maps[k]["provisional_gst"] == nil {
				maps[k]["provisional_gst"] = ""
			}
			if maps[k]["ARN"] == nil {
				maps[k]["ARN"] = ""
			}
			if maps[k]["name"] == nil {
				maps[k]["name"] = ""
			}
			if maps[k]["address"] == nil {
				maps[k]["address"] = ""
			}
			if maps[k]["designation"] == nil {
				maps[k]["designation"] = ""
			}
			if maps[k]["phone"] == nil {
				maps[k]["phone"] = ""
			}
			if maps[k]["cell_no"] == nil {
				maps[k]["cell_no"] = ""
			}
			if maps[k]["address_line1"] == nil {
				maps[k]["cell_no"] = ""
			}
			if maps[k]["wcity"] == nil {
				maps[k]["wcity"] = ""
			}
			if maps[k]["wstate"] == nil {
				maps[k]["wstate"] = ""
			}
			if maps[k]["wcountry"] == nil {
				maps[k]["wcountry"] = ""
			}
			if maps[k]["city"] == nil {
				maps[k]["city"] = ""
			}
			if maps[k]["state"] == nil {
				maps[k]["state"] = ""
			}
			if maps[k]["country"] == nil {
				maps[k]["country"] = ""
			}
			if maps[k]["pincode"] == nil {
				maps[k]["pincode"] = ""
			}

			var s string
			if maps[k]["city"] != "" {
				s = maps[k]["city"].(string)
			}
			if maps[k]["state"] != "" {
				if maps[k]["city"] != "" {
					s = s + ", "
				}
				s = s + maps[k]["state"].(string)
			}
			if maps[k]["country"] != "" {
				if maps[k]["state"] != "" {
					s = s + ", "
				}
				s = s + maps[k]["country"].(string)
			}
			if maps[k]["pincode"] != "" {
				if maps[k]["country"] != "" {
					s = s + ", "
				}
				s = s + maps[k]["pincode"].(string)
			}
			maps[k]["city_state_country_pincode"] = s

			key := "customer_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetActiveDropdownListOfCustomersByOrgid(orgID string) []orm.Params {

	o := orm.NewOrm()
	var maps []orm.Params

	_, err := o.Raw("SELECT _id, customer_unique_code,company_name FROM customer WHERE _orgid=? and status=1", orgID).Values(&maps)
	if err != nil {
	}

	return maps
}

func GetCustomerContactPersonsByPkId(pkID string, orgID string) map[string]orm.Params {

	result := make(map[string]orm.Params)

	o := orm.NewOrm()
	var maps []orm.Params
	//num, err := o.Raw("SELECT * FROM item_master ").Values(&maps)
	num, err := o.Raw("SELECT cp._id, cp.name, cp.designation, cp.phone, cp.cell_no, cp.email, cp.contact_type FROM customer_contact_person as cp WHERE cp._customer_fkid=? and cp._orgid=? ORDER BY contact_type ASC", pkID, orgID).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		//var i int64 = 0
		//for ; i < num; i++ {
		//fmt.Println(maps[i])
		//}
		for k, v := range maps {

			if maps[k]["name"] == nil {
				maps[k]["name"] = ""
			}
			if maps[k]["designation"] == nil {
				maps[k]["designation"] = ""
			}
			if maps[k]["phone"] == nil {
				maps[k]["phone"] = ""
			}
			if maps[k]["cell_no"] == nil {
				maps[k]["cell_no"] = ""
			}
			if maps[k]["email"] == nil {
				maps[k]["email"] = ""
			}
			key := "customer_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetCustomerWarehousesByPkId(pkID string, orgID string) map[string]orm.Params {

	result := make(map[string]orm.Params)

	o := orm.NewOrm()
	var maps []orm.Params
	//num, err := o.Raw("SELECT * FROM item_master ").Values(&maps)
	num, err := o.Raw("SELECT cw._id, cw.address_line1, cw.wcity, cw.wstate, cw.wcountry, cw.wpincode, cw.GSTIN FROM customer_warehouse_address as cw WHERE cw.customer_fkid=? and cw._orgid=? and cw.status=1 and type_address=2", pkID, orgID).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		var i int64 = 0
		for ; i < num; i++ {
			//fmt.Println(maps[i])
		}
		for k, v := range maps {
			if maps[k]["address_line1"] == nil {
				maps[k]["address_line1"] = ""
			}
			if maps[k]["wcity"] == nil {
				maps[k]["wcity"] = ""
			}
			if maps[k]["wstate"] == nil {
				maps[k]["wstate"] = ""
			}
			if maps[k]["wcountry"] == nil {
				maps[k]["wcountry"] = ""
			}
			if maps[k]["wpincode"] == nil {
				maps[k]["wpincode"] = ""
			}
			if maps[k]["GSTIN"] == nil {
				maps[k]["GSTIN"] = ""
			}

			key := "customer_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetCustomerContactPersonsByName(companyName string, orgID string) map[string]orm.Params {

	result := make(map[string]orm.Params)

	o := orm.NewOrm()

	var maps01 []orm.Params
	_, err01 := o.Raw("SELECT _id FROM customer WHERE _orgid=? and status=1 and company_name=?", orgID, companyName).Values(&maps01)
	if err01 != nil {
		result["status"] = nil
		return result
	}

	var maps []orm.Params
	//num, err := o.Raw("SELECT * FROM item_master ").Values(&maps)
	num, err := o.Raw("SELECT cp._id, cp.name, cp.designation, cp.phone, cp.cell_no, cp.email, cp.contact_type FROM customer_contact_person as cp WHERE cp._customer_fkid=? and cp._orgid=? ORDER BY contact_type ASC", maps01[0]["_id"], orgID).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		// var i int64 = 0
		// for ; i < num; i++ {
		// 	fmt.Println(maps[i])
		// }
		for k, v := range maps {

			if maps[k]["name"] == nil {
				maps[k]["name"] = ""
			}
			if maps[k]["designation"] == nil {
				maps[k]["designation"] = ""
			}
			if maps[k]["phone"] == nil {
				maps[k]["phone"] = ""
			}
			if maps[k]["cell_no"] == nil {
				maps[k]["cell_no"] = ""
			}
			if maps[k]["email"] == nil {
				maps[k]["email"] = ""
			}
			key := "customer_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetCustomerContactPersonsByCompanyID(companyID string, orgID string) map[string]orm.Params {

	result := make(map[string]orm.Params)

	o := orm.NewOrm()
	var maps []orm.Params

	num, err := o.Raw("SELECT cp._id, cp.name, cp.designation, cp.phone, cp.cell_no, cp.email, cp.contact_type FROM customer_contact_person as cp WHERE cp._customer_fkid=? and cp._orgid=? ORDER BY contact_type ASC", companyID, orgID).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		// var i int64 = 0
		// for ; i < num; i++ {
		// 	fmt.Println(maps[i])
		// }
		for k, v := range maps {

			if maps[k]["name"] == nil {
				maps[k]["name"] = ""
			}
			if maps[k]["designation"] == nil {
				maps[k]["designation"] = ""
			}
			if maps[k]["phone"] == nil {
				maps[k]["phone"] = ""
			}
			if maps[k]["cell_no"] == nil {
				maps[k]["cell_no"] = ""
			}
			if maps[k]["email"] == nil {
				maps[k]["email"] = ""
			}
			key := "customer_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetCustomerWarehousesByName(companyName string, orgID string) map[string]orm.Params {

	result := make(map[string]orm.Params)

	o := orm.NewOrm()

	var maps01 []orm.Params
	_, err01 := o.Raw("SELECT _id FROM customer WHERE _orgid=? and status=1 and company_name=?", orgID, companyName).Values(&maps01)
	if err01 != nil {
		result["status"] = nil
		return result
	}

	var maps []orm.Params
	//num, err := o.Raw("SELECT * FROM item_master ").Values(&maps)
	num, err := o.Raw("SELECT cw._id, cw.address_line1, cw.wcity, cw.wstate, cw.wcountry, cw.wpincode, cw.GSTIN FROM customer_warehouse_address as cw WHERE cw.customer_fkid=? and cw._orgid=? and cw.status=1 and type_address=2", maps01[0]["_id"], orgID).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		//var i int64 = 0
		// for ; i < num; i++ {
		// 	fmt.Println(maps[i])
		// }
		for k, v := range maps {
			if maps[k]["address_line1"] == nil {
				maps[k]["address_line1"] = ""
			}
			if maps[k]["wcity"] == nil {
				maps[k]["wcity"] = ""
			}
			if maps[k]["wstate"] == nil {
				maps[k]["wstate"] = ""
			}
			if maps[k]["wcountry"] == nil {
				maps[k]["wcountry"] = ""
			}
			if maps[k]["wpincode"] == nil {
				maps[k]["wpincode"] = ""
			}
			if maps[k]["GSTIN"] == nil {
				maps[k]["GSTIN"] = ""
			}

			key := "customer_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetCustomerWarehousesByCompanyID(companyID string, orgID string) map[string]orm.Params {

	result := make(map[string]orm.Params)

	o := orm.NewOrm()

	var maps []orm.Params
	//num, err := o.Raw("SELECT * FROM item_master ").Values(&maps)
	num, err := o.Raw("SELECT cw._id, cw.address_line1, cw.wcity, cw.wstate, cw.wcountry, cw.wpincode, cw.GSTIN FROM customer_warehouse_address as cw WHERE cw.customer_fkid=? and cw._orgid=? and cw.status=1 and type_address=2", companyID, orgID).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		//var i int64 = 0
		// for ; i < num; i++ {
		// 	fmt.Println(maps[i])
		// }
		for k, v := range maps {
			if maps[k]["address_line1"] == nil {
				maps[k]["address_line1"] = ""
			}
			if maps[k]["wcity"] == nil {
				maps[k]["wcity"] = ""
			}
			if maps[k]["wstate"] == nil {
				maps[k]["wstate"] = ""
			}
			if maps[k]["wcountry"] == nil {
				maps[k]["wcountry"] = ""
			}
			if maps[k]["wpincode"] == nil {
				maps[k]["wpincode"] = ""
			}
			if maps[k]["GSTIN"] == nil {
				maps[k]["GSTIN"] = ""
			}

			key := "customer_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func AssocCustProd(formData Viewassoc2) int {
	o := orm.NewOrm()
	ormTransactionalErr := o.Begin()
	flash := beego.NewFlash()
	/*var maps []orm.Params
		 _, err := o.Raw("SELECT _id FROM customer WHERE customer._orgid=? and company_name=?", formData.Orgid, formData.Customer).Values(&maps)
	     fmt.Println(maps[0]["_id"])*/
	for i := 0; i < len(formData.Products); i++ {
		//var maps2 []orm.Params
		var maps3 []orm.Params
		//_, err1 := o.Raw("SELECT _id FROM item_org_assoc WHERE item_org_assoc._orgid=? and item_org_assoc.name=?", formData.Orgid, formData.Products[i]).Values(&maps2)
		// fmt.Println(maps2[0]["_id"])
		num, err3 := o.Raw("SELECT _id FROM customer_product_assoc WHERE customer_product_assoc._orgid=? and item_org_assoc_fkid=? and _customer_fkid=? and status=1 ", formData.Orgid, formData.Products[i], formData.Customer).Values(&maps3)
		if err3 == nil && num > 0 {
			flash.Notice("This association exists")
			ormTransactionalErr = o.Rollback()
			return 0
		} else {
			_, err2 := o.Raw("INSERT INTO customer_product_assoc (item_org_assoc_fkid, _customer_fkid, last_updated,status,_orgid) VALUES (?,?,?,?,?) ", formData.Products[i], formData.Customer, time.Now(), "1", formData.Orgid).Exec()
			if err2 != nil {
				ormTransactionalErr = o.Rollback()
				return 0
			}
		}
		//if err1 != nil {
		//fmt.Println("Error in for loop")
		//break
		//}
	}
	if ormTransactionalErr != nil {
		return 0
	}
	ormTransactionalErr = o.Commit()
	return 1

}

func SearchCustomer(formData Viewassoc2) map[string]orm.Params {

	result := make(map[string]orm.Params)

	o := orm.NewOrm()
	var maps []orm.Params
	//num, err := o.Raw("SELECT * FROM item_master ").Values(&maps)
	num, err := o.Raw("SELECT _id,company_name from customer WHERE _orgid=? and status=?", formData.Orgid, "1").Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		//var i int64 = 0
		//for ; i < num; i++ {
		//fmt.Println(maps[i])
		//}
		for k, v := range maps {
			key := "customers_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func ViewAssoc(formData Viewassoc2) map[string]orm.Params {

	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params

	num, err := o.Raw("SELECT ioa.item_unique_code, ioa.item_code_gen, ioa.name, customer_product_assoc._id, customer_product_assoc.invoice_naration,customer_product_assoc.sales_price  FROM customer_product_assoc, item_org_assoc as ioa, customer where customer_product_assoc._orgid=? and customer._id=? and customer_product_assoc.status=? and customer_product_assoc._customer_fkid=customer._id and customer_product_assoc.item_org_assoc_fkid=ioa._id ", formData.Orgid, formData.Customer, "1").Values(&maps)
	//fmt.Println(maps[0]["product_org_code"])
	//fmt.Println(maps[0]["product_org_name"])
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		//var i int64 = 0
		//for ; i < num; i++ {
		//fmt.Println(maps[i])
		//}
		for k, v := range maps {

			key := "gi_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func SearchProducts(formData Viewassoc2) map[string]orm.Params {

	result := make(map[string]orm.Params)

	o := orm.NewOrm()
	var maps []orm.Params
	//num, err := o.Raw("SELECT * FROM item_master ").Values(&maps)
	num, err := o.Raw("SELECT name, item_unique_code, unit_measure from item_org_assoc WHERE _orgid=?", formData.Orgid).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		//var i int64 = 0
		//for ; i < num; i++ {
		//fmt.Println(maps[i])
		//}
		for k, v := range maps {
			key := "customers_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func DeleteCustomersAssoc(assoc_id string, orgid string) string {
	result := "false"
	o := orm.NewOrm()
	//var values []orm.Params
	_, err := o.Raw("UPDATE customer_product_assoc SET status=?, last_updated=? WHERE _orgid=? and _id=? ", "0", time.Now(), orgid, assoc_id).Exec()
	if err != nil {
		//fmt.Println("Error in UPDATE")
	} else {
		result = "true"
	}
	return result
}

func GetCustomerDetailsByName(companyName string, orgID string) Customer {
	o := orm.NewOrm()
	var maps []orm.Params
	var itemStruct = Customer{}
	num, err := o.Raw("SELECT c._id, c.customer_unique_code, c.company_name,c.grp,c.address,c.city,c.state,c.pincode,c.country,c.TIN,c.area_code,c.PAN,c.credit_in_days,c.CST,c.credit_limit,c.ECC,c.customer_type,c.date_of_introduction,c.service_location,c.status,c.GSTIN,c.provisional_gst,c.ARN,cp.name,cp.designation,cp.phone,cp.cell_no,cp.email from customer as c,customer_contact_person as cp where c._orgid =? and c._orgid=? and c._id=cp._customer_fkid and c.status = 1  and c.company_name = ? ", orgID, orgID, companyName).Values(&maps)
	if err != nil || num == 0 {
		itemStruct.IsPresentinDB = 0
	} else {
		itemStruct.CustomerID = maps[0]["_id"].(string)
		itemStruct.UniqueCode = maps[0]["customer_unique_code"].(string)
		itemStruct.CompanyName = maps[0]["company_name"].(string)
		itemStruct.Group = maps[0]["grp"].(string)

		if maps[0]["credit_in_days"] != nil {
			itemStruct.Address = maps[0]["address"].(string)
		} else {
			itemStruct.Address = ""
		}
		itemStruct.City = maps[0]["city"].(string)
		itemStruct.State = maps[0]["state"].(string)
		itemStruct.Country = maps[0]["country"].(string)
		itemStruct.Pincode = maps[0]["pincode"].(string)
		itemStruct.TIN = maps[0]["TIN"].(string)

		if maps[0]["credit_limit"] != nil {
			itemStruct.CreditLimit = maps[0]["credit_limit"].(string)
		} else {
			itemStruct.CreditLimit = ""
		}
		itemStruct.CustomerType = maps[0]["customer_type"].(string)
		itemStruct.AreaCode = maps[0]["area_code"].(string)
		itemStruct.CST = maps[0]["CST"].(string)
		itemStruct.PAN = maps[0]["PAN"].(string)
		itemStruct.ECC = maps[0]["ECC"].(string)

		if maps[0]["credit_in_days"] != nil {
			itemStruct.CreditInDays = maps[0]["credit_in_days"].(string)
		} else {
			itemStruct.CreditInDays = ""
		}
		// dateOfIntro := maps[0]["date_of_introduction"].(string)
		// itemStruct.Introduction = &dateOfIntro
		if maps[0]["date_of_introduction"] != nil {
			itemStruct.Introduction = maps[0]["date_of_introduction"].(string)
		} else {
			itemStruct.Introduction = ""
		}

		itemStruct.ServiceLoc = maps[0]["service_location"].(string)

		if maps[0]["GSTIN"] != nil {
			itemStruct.MainGSTIN = maps[0]["GSTIN"].(string)
		} else {
			itemStruct.MainGSTIN = ""
		}

		if maps[0]["provisional_gst"] != nil {
			itemStruct.PGST = maps[0]["provisional_gst"].(string)
		} else {
			itemStruct.PGST = ""
		}
		if maps[0]["ARN"] != nil {
			itemStruct.ARN = maps[0]["ARN"].(string)
		} else {
			itemStruct.ARN = ""
		}
		// itemStruct.GST = maps[0]["gst"].(string)
		// itemStruct.PGST = maps[0]["provisional_gst"].(string)
		// itemStruct.ARN = maps[0]["ARN"].(string)
		if maps[0]["name"] != nil {
			itemStruct.ContactPerson = maps[0]["name"].(string)
		} else {
			itemStruct.ContactPerson = ""
		}
		if maps[0]["designation"] != nil {
			itemStruct.Designation = maps[0]["designation"].(string)
		} else {
			itemStruct.Designation = ""
		}
		if maps[0]["email"] != nil {
			itemStruct.Email = maps[0]["email"].(string)
		} else {
			itemStruct.Email = ""
		}
		if maps[0]["phone"] != nil {
			itemStruct.PhoneNo = maps[0]["phone"].(string)
		} else {
			itemStruct.PhoneNo = ""
		}
		if maps[0]["cell_no"] != nil {
			itemStruct.CellNo = maps[0]["cell_no"].(string)
		} else {
			itemStruct.CellNo = ""
		}

		itemStruct.IsPresentinDB = 1
	}
	return itemStruct
}

func GetCustomerDetailsByPkId(pkID string, orgID string) Customer {
	o := orm.NewOrm()
	var maps []orm.Params
	var itemStruct = Customer{}
	num, err := o.Raw("SELECT c._id, c.customer_unique_code, c.company_name,c.grp,c.address,c.city,c.state,c.pincode,c.country,c.TIN,c.area_code,c.PAN,c.credit_in_days,c.CST,c.credit_limit,c.ECC,c.customer_type,c.date_of_introduction,c.service_location,c.status,c.GSTIN,c.provisional_gst,c.ARN,cp.name,cp.designation,cp.phone,cp.cell_no,cp.email from customer as c,customer_contact_person as cp where c._orgid =? and c._orgid=? and c._id=cp._customer_fkid and c.status = 1  and c._id = ? ", orgID, orgID, pkID).Values(&maps)
	if err != nil || num == 0 {
		itemStruct.IsPresentinDB = 0
	} else {
		itemStruct.CustomerID = maps[0]["_id"].(string)
		itemStruct.UniqueCode = maps[0]["customer_unique_code"].(string)
		itemStruct.CompanyName = maps[0]["company_name"].(string)
		itemStruct.Group = maps[0]["grp"].(string)

		if maps[0]["credit_in_days"] != nil {
			itemStruct.Address = maps[0]["address"].(string)
		} else {
			itemStruct.Address = ""
		}
		itemStruct.City = maps[0]["city"].(string)
		itemStruct.State = maps[0]["state"].(string)
		itemStruct.Country = maps[0]["country"].(string)
		itemStruct.Pincode = maps[0]["pincode"].(string)
		itemStruct.TIN = maps[0]["TIN"].(string)

		if maps[0]["credit_limit"] != nil {
			itemStruct.CreditLimit = maps[0]["credit_limit"].(string)
		} else {
			itemStruct.CreditLimit = ""
		}
		itemStruct.CustomerType = maps[0]["customer_type"].(string)
		itemStruct.AreaCode = maps[0]["area_code"].(string)
		itemStruct.CST = maps[0]["CST"].(string)
		itemStruct.PAN = maps[0]["PAN"].(string)
		itemStruct.ECC = maps[0]["ECC"].(string)

		if maps[0]["credit_in_days"] != nil {
			itemStruct.CreditInDays = maps[0]["credit_in_days"].(string)
		} else {
			itemStruct.CreditInDays = ""
		}
		// dateOfIntro := maps[0]["date_of_introduction"].(string)
		// itemStruct.Introduction = &dateOfIntro
		if maps[0]["date_of_introduction"] != nil {
			itemStruct.Introduction = maps[0]["date_of_introduction"].(string)
		} else {
			itemStruct.Introduction = ""
		}

		itemStruct.ServiceLoc = maps[0]["service_location"].(string)
		if maps[0]["GSTIN"] != nil {
			itemStruct.MainGSTIN = maps[0]["GSTIN"].(string)
		} else {
			itemStruct.MainGSTIN = ""
		}

		if maps[0]["provisional_gst"] != nil {
			itemStruct.PGST = maps[0]["provisional_gst"].(string)
		} else {
			itemStruct.PGST = ""
		}
		if maps[0]["ARN"] != nil {
			itemStruct.ARN = maps[0]["ARN"].(string)
		} else {
			itemStruct.ARN = ""
		}
		// itemStruct.GST = maps[0]["gst"].(string)
		// itemStruct.PGST = maps[0]["provisional_gst"].(string)
		// itemStruct.ARN = maps[0]["ARN"].(string)
		if maps[0]["name"] != nil {
			itemStruct.ContactPerson = maps[0]["name"].(string)
		} else {
			itemStruct.ContactPerson = ""
		}
		if maps[0]["designation"] != nil {
			itemStruct.Designation = maps[0]["designation"].(string)
		} else {
			itemStruct.Designation = ""
		}
		if maps[0]["email"] != nil {
			itemStruct.Email = maps[0]["email"].(string)
		} else {
			itemStruct.Email = ""
		}
		if maps[0]["phone"] != nil {
			itemStruct.PhoneNo = maps[0]["phone"].(string)
		} else {
			itemStruct.PhoneNo = ""
		}
		if maps[0]["cell_no"] != nil {
			itemStruct.CellNo = maps[0]["cell_no"].(string)
		} else {
			itemStruct.CellNo = ""
		}

		itemStruct.IsPresentinDB = 1
	}
	return itemStruct
}

func GetCustomerDetailsByCompanyID(pkID string, orgID string) (int64, CustomerDetails) {
	o := orm.NewOrm()
	var maps []orm.Params
	var itemStruct = CustomerDetails{}
	num, err := o.Raw("SELECT * from customer where _orgid=? and status = 1  and _id = ? ", orgID, pkID).Values(&maps)
	if err != nil || num == 0 {

	} else {
		itemStruct.CustomerID = maps[0]["_id"].(string)
		itemStruct.UniqueCode = maps[0]["customer_unique_code"].(string)
		itemStruct.CompanyName = maps[0]["company_name"].(string)
		itemStruct.Group = maps[0]["grp"].(string)

		if maps[0]["credit_in_days"] != nil {
			itemStruct.Address = maps[0]["address"].(string)
		} else {
			itemStruct.Address = ""
		}
		itemStruct.City = maps[0]["city"].(string)
		itemStruct.State = maps[0]["state"].(string)
		itemStruct.Country = maps[0]["country"].(string)
		itemStruct.Pincode = maps[0]["pincode"].(string)
		itemStruct.TIN = maps[0]["TIN"].(string)

		if maps[0]["credit_limit"] != nil {
			itemStruct.CreditLimit = maps[0]["credit_limit"].(string)
		} else {
			itemStruct.CreditLimit = ""
		}
		itemStruct.CustomerType = maps[0]["customer_type"].(string)
		itemStruct.CST = maps[0]["CST"].(string)
		itemStruct.PAN = maps[0]["PAN"].(string)
		itemStruct.ECC = maps[0]["ECC"].(string)

		if maps[0]["credit_in_days"] != nil {
			itemStruct.CreditInDays = maps[0]["credit_in_days"].(string)
		} else {
			itemStruct.CreditInDays = ""
		}

		if maps[0]["date_of_introduction"] != nil {
			itemStruct.Introduction = maps[0]["date_of_introduction"].(string)
		} else {
			itemStruct.Introduction = ""
		}
		if maps[0]["GSTIN"] != nil {
			itemStruct.MainGSTIN = maps[0]["GSTIN"].(string)
		} else {
			itemStruct.MainGSTIN = ""
		}

		if maps[0]["provisional_gst"] != nil {
			itemStruct.PGST = maps[0]["provisional_gst"].(string)
		} else {
			itemStruct.PGST = ""
		}
		if maps[0]["ARN"] != nil {
			itemStruct.ARN = maps[0]["ARN"].(string)
		} else {
			itemStruct.ARN = ""
		}
	}
	return num, itemStruct
}

func CheckForExistingCompanyName(companyName string, orgID string, compid string) int {
	o := orm.NewOrm()
	var resultMap []orm.Params
	num, err := o.Raw("SELECT company_name FROM customer WHERE company_name=? and  _orgid=? and status = 1 and _id != ?", companyName, orgID, compid).Values(&resultMap)
	//fmt.Println("Query Check Name==========", num)
	if err == nil && num > 0 {
		return 1
	}
	return 0
}

func CheckExistingCompanyCodeAgainstID(customerName string, orgID string, customerID string) int {
	o := orm.NewOrm()
	var resultMap []orm.Params
	num, err := o.Raw("SELECT customer_unique_code FROM customer WHERE company_name=? and  _orgid=? and status = 1 and _id != ?", customerName, orgID, customerID).Values(&resultMap)
	if err == nil && num > 0 {
		return 1
	}
	return 0
}

func UpdateCustomer(formData Customer, warehouses []map[string]string, contactpersons []map[string]string, prevcontactpersons []map[string]string, prevwarehouses []map[string]string) int {

	o := orm.NewOrm()
	ormTransactionalErr := o.Begin()
	var introDate *string
	introDate = nil
	if formData.Introduction != "" {
		tempIntroDate := formData.Introduction
		introDate = &tempIntroDate
	}
	var main_gst *string
	main_gst = nil
	if formData.MainGSTIN != "" {
		tempmain_gst := formData.MainGSTIN
		main_gst = &tempmain_gst
	}
	var pgst *string
	pgst = nil
	if formData.PGST != "" {
		temppgst := formData.PGST
		pgst = &temppgst
	}
	var arn *string
	arn = nil
	if formData.ARN != "" {
		temparn := formData.ARN
		arn = &temparn
	}
	// var contactperson *string
	// contactperson = nil
	// if formData.ContactPerson != "" {
	// 	tempcontactperson := formData.ContactPerson
	// 	contactperson = &tempcontactperson
	// }
	// var designation *string
	// designation = nil
	// if formData.Designation != "" {
	// 	tempdesignation := formData.Designation
	// 	designation = &tempdesignation
	// }
	// var phoneno *string
	// phoneno = nil
	// if formData.PhoneNo != "" {
	// 	tempphoneno := formData.PhoneNo
	// 	phoneno = &tempphoneno
	// }
	// var cellno *string
	// cellno = nil
	// if formData.CellNo != "" {
	// 	tempcellno := formData.CellNo
	// 	cellno = &tempcellno
	// }
	// var email *string
	// email = nil
	// if formData.Email != "" {
	// 	tempemail := formData.Email
	// 	email = &tempemail
	// }
	_, err := o.Raw("UPDATE customer SET company_name=?, grp=?, address=?, city=?, state=?, pincode=?, country=?, TIN=?, area_code=?, PAN=?, credit_in_days=?, CST=?, credit_limit=?, ECC=?, customer_type=?,date_of_introduction=?,service_location=?,status=?,GSTIN=?,provisional_gst=?,ARN=? where customer._orgid=? and customer._id=?  ", formData.CompanyName, formData.Group, formData.Address, formData.City, formData.State, formData.Pincode, formData.Country, formData.TIN, formData.AreaCode, formData.PAN, formData.CreditInDays, formData.CST, formData.CreditLimit, formData.ECC, formData.CustomerType, &introDate, formData.ServiceLoc, "1", &main_gst, &pgst, &arn, formData.Orgid, formData.CustomerID).Exec()
	if err != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}

	for _, citt := range prevcontactpersons {
		if citt["contact_person"] != "" {
			var designation *string
			designation = nil
			if citt["designation"] != "" {
				tempdesignation := citt["designation"]
				designation = &tempdesignation
			}
			var phoneno *string
			phoneno = nil
			if citt["phone_no"] != "" {
				tempphoneno := citt["phone_no"]
				phoneno = &tempphoneno
			}
			var cellno *string
			cellno = nil
			if citt["cell_no"] != "" {
				tempcellno := citt["cell_no"]
				cellno = &tempcellno
			}
			var email *string
			email = nil
			if citt["email"] != "" {
				tempemail := citt["email"]
				email = &tempemail
			}

			_, err1 := o.Raw("UPDATE customer_contact_person SET name=?, designation=?, phone=?, cell_no=?, email=? where _orgid=? and _customer_fkid=? and _id=? ", citt["contact_person"], &designation, &phoneno, &cellno, &email, formData.Orgid, formData.CustomerID, citt["contact_person_id"]).Exec()
			if err1 != nil {
				ormTransactionalErr = o.Rollback()
				return 0
			}
		}
	}

	_, err2 := o.Raw("UPDATE customer_warehouse_address SET address_line1=?, wcity=?, wstate=?, wcountry=?, wpincode=?, lastupdated=? WHERE _orgid=? and customer_fkid=? and type_address=1 and status=1 ", formData.Address, formData.City, formData.State, formData.Country, formData.Pincode, time.Now(), formData.Orgid, formData.CustomerID).Exec()
	if err2 != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}

	for _, pwit := range prevwarehouses {
		if pwit["warehouse_address"] != "" {

			// type_address=2 for warehouse address
			_, err02 := o.Raw("UPDATE customer_warehouse_address SET address_line1=?, wcity=?, wstate=?, wcountry=?, wpincode=?, lastupdated=? WHERE _orgid=? and customer_fkid=? and type_address=2 and status=1 and _id=?", pwit["warehouse_address"], pwit["wcity"], pwit["wstate"], pwit["wcountry"], pwit["wpincode"], time.Now(), formData.Orgid, formData.CustomerID, pwit["w_id"]).Exec()
			if err02 != nil {
				ormTransactionalErr = o.Rollback()
				return 0
			}
		}
	}

	for _, itt := range contactpersons {
		if itt["contact_person"] != "" {
			var designation *string
			designation = nil
			if itt["designation"] != "" {
				tempdesignation := itt["designation"]
				designation = &tempdesignation
			}
			var phoneno *string
			phoneno = nil
			if itt["phone_no"] != "" {
				tempphoneno := itt["phone_no"]
				phoneno = &tempphoneno
			}
			var cellno *string
			cellno = nil
			if itt["cell_no"] != "" {
				tempcellno := itt["cell_no"]
				cellno = &tempcellno
			}
			var email *string
			email = nil
			if itt["email"] != "" {
				tempemail := itt["email"]
				email = &tempemail
			}
			// var maps01 []orm.Params
			// count, err03 := o.Raw("SELECT _id FROM customer_contact_person WHERE _customer_fkid=? and _orgid=? and name=? and designation=? and phone=? and cell_no=? and email=? ", maps[0]["_id"], formData.Orgid, itt["contact_person"], &designation, &phoneno, &cellno, &email).Values(&maps01)
			// if err03 != nil {
			// 	ormTransactionalErr = o.Rollback()
			// 	return 0
			// }

			// if count < 1 {
			_, err3 := o.Raw("INSERT INTO customer_contact_person (_orgid, name, designation, phone, cell_no, email, contact_type, _customer_fkid) VALUES (?,?,?,?,?,?,?,?) ", formData.Orgid, itt["contact_person"], &designation, &phoneno, &cellno, &email, "2", formData.CustomerID).Exec()
			if err3 != nil {
				ormTransactionalErr = o.Rollback()
				return 0
			}
			// }
		}
	}

	for _, it := range warehouses {
		if it["warehouse_address"] != "" {

			// type_address=2 for warehouse address
			_, err5 := o.Raw("INSERT INTO customer_warehouse_address (customer_fkid, _orgid, address_line1, wcity, wstate, wcountry, wpincode, type_address, status, lastupdated, GSTIN) VALUES (?,?,?,?,?,?,?,?,?,?,?)", formData.CustomerID, formData.Orgid, it["warehouse_address"], it["wcity"], it["wstate"], it["wcountry"], it["wpincode"], "2", "1", time.Now(), it["gstin"]).Exec()
			if err5 != nil {
				ormTransactionalErr = o.Rollback()
				return 0
			}
		}
	}

	if ormTransactionalErr != nil {
		return 0
	}
	ormTransactionalErr = o.Commit()
	return 1
}

func UpdateCustomerByCustomerStruct(customerDetilsStruct CustomerDetails, contacts []map[string]string, warehouses []map[string]string, orgId string) int {
	o := orm.NewOrm()

	var introDate *string
	introDate = nil
	if customerDetilsStruct.Introduction != "" {
		tempIntroDate := customerDetilsStruct.Introduction
		introDate = &tempIntroDate
	}
	var main_gst *string
	main_gst = nil
	if customerDetilsStruct.MainGSTIN != "" {
		tempmain_gst := customerDetilsStruct.MainGSTIN
		main_gst = &tempmain_gst
	}
	var pgst *string
	pgst = nil
	if customerDetilsStruct.PGST != "" {
		temppgst := customerDetilsStruct.PGST
		pgst = &temppgst
	}
	var arn *string
	arn = nil
	if customerDetilsStruct.ARN != "" {
		temparn := customerDetilsStruct.ARN
		arn = &temparn
	}

	var credit_in_days *string
	credit_in_days = nil
	if customerDetilsStruct.CreditInDays != "" {
		tempCreditDays := customerDetilsStruct.CreditInDays
		credit_in_days = &tempCreditDays
	}
	var credit_limit *string
	credit_limit = nil
	if customerDetilsStruct.CreditLimit != "" {
		tempCreditLimit := customerDetilsStruct.CreditLimit
		credit_limit = &tempCreditLimit
	}

	_, err := o.Raw("UPDATE customer SET company_name=?, grp=?, address=?, city=?, state=?, pincode=?, country=?, TIN=?, PAN=?, credit_in_days=?, CST=?, credit_limit=?, ECC=?, customer_type=?,date_of_introduction=?,status=?,GSTIN=?,provisional_gst=?,ARN=? where customer._orgid=? and _id=?  ", customerDetilsStruct.CompanyName, customerDetilsStruct.Group, customerDetilsStruct.Address, customerDetilsStruct.City, customerDetilsStruct.State, customerDetilsStruct.Pincode, customerDetilsStruct.Country, customerDetilsStruct.TIN, customerDetilsStruct.PAN, &credit_in_days, customerDetilsStruct.CST, &credit_limit, customerDetilsStruct.ECC, customerDetilsStruct.CustomerType, &introDate, "1", &main_gst, &pgst, &arn, orgId, customerDetilsStruct.CustomerID).Exec()
	if err != nil {
		return 0
	}

	contactInsertError := 0
	warehouseInsertError := 0
	ormTransactionalErrContacts := o.Begin()
	for _, contact := range contacts {
		if contact["contact_person_id"] == "" {
			_, err11 := o.Raw("INSERT into customer_contact_person (_orgid,_customer_fkid, name, designation, phone, cell_no, email) VALUES (?,?,?,?,?)", orgId, customerDetilsStruct.CustomerID, contact["contact_person"], contact["designation"], contact["phoneno"], contact["cellno"], contact["email"]).Exec()
			if err11 != nil {
				contactInsertError = 1
			}
		} else if contact["contact_person"] != "" {
			var designation *string
			designation = nil
			if contact["designation"] != "" {
				tempdesignation := contact["designation"]
				designation = &tempdesignation
			}
			var phoneno *string
			phoneno = nil
			if contact["phone_no"] != "" {
				tempphoneno := contact["phone_no"]
				phoneno = &tempphoneno
			}
			var cellno *string
			cellno = nil
			if contact["cell_no"] != "" {
				tempcellno := contact["cell_no"]
				cellno = &tempcellno
			}
			var email *string
			email = nil
			if contact["email"] != "" {
				tempemail := contact["email"]
				email = &tempemail
			}

			_, err1 := o.Raw("UPDATE customer_contact_person SET name=?, designation=?, phone=?, cell_no=?, email=?  WHERE _orgid=? and _customer_fkid=? and _id=? ", contact["name"], &designation, &phoneno, &cellno, &email, orgId, customerDetilsStruct.CustomerID, contact["contact_person_id"]).Exec()
			if err1 != nil {
				contactInsertError = 1
			}
		}
	}
	if contactInsertError == 1 {
		ormTransactionalErrContacts = o.Rollback()
	} else {
		ormTransactionalErrContacts = o.Commit()
	}

	ormTransactionalErrWarehouse := o.Begin()
	for _, warehouse := range warehouses {

		if warehouse["w_id"] == "" {
			_, err2 := o.Raw("INSERT INTO customer_warehouse_address (customer_fkid, _orgid, address_line1, wcity, wstate, wcountry, wpincode, type_address, status, lastupdated, GSTIN) VALUES (?,?,?,?,?,?,?,?,?,?,?)", customerDetilsStruct.CustomerID, orgId, warehouse["warehouse_address"], warehouse["wcity"], warehouse["wstate"], warehouse["wcountry"], warehouse["wpincode"], "2", "1", time.Now(), warehouse["gstin"]).Exec()
			if err2 != nil {
				warehouseInsertError = 1
			}
		} else if warehouse["w_id"] != "" && warehouse["warehouse_address"] != "" {
			_, err5 := o.Raw("UPDATE customer_warehouse_address SET address_line1=?, wcity=?, wstate=?, wcountry=?, wpincode=?, lastupdated=?, GSTIN=? WHERE _orgid=? and customer_fkid=?  and _id=?", warehouse["warehouse_address"], warehouse["wcity"], warehouse["wstate"], warehouse["wcountry"], warehouse["wpincode"], time.Now(), warehouse["gstin"], customerDetilsStruct.CustomerID, orgId, warehouse["w_id"]).Exec()
			if err5 != nil {
				warehouseInsertError = 1
			}
		}
	}
	if warehouseInsertError == 1 {
		ormTransactionalErrWarehouse = o.Rollback()
	} else {
		ormTransactionalErrWarehouse = o.Commit()
	}
	if ormTransactionalErrWarehouse != nil || ormTransactionalErrContacts != nil {
		return 2
	}
	return 1
}

func UpdateAssoc2(item_id string, new_qty string, price string, orgid string) string {
	result := "false"
	o := orm.NewOrm()
	//var values []orm.Params
	var naration *string
	naration = nil
	if new_qty != "" {
		tempnaration := new_qty
		naration = &tempnaration
	}
	var rate *string
	rate = nil
	if price != "" {
		tempprice := price
		rate = &tempprice
	}
	_, err := o.Raw("UPDATE customer_product_assoc SET invoice_naration=?,sales_price=? WHERE _orgid=? and _id=? ", &naration, &rate, orgid, item_id).Exec()
	if err != nil {
		//fmt.Println("Error in UPDATE")
	} else {
		result = "true"
	}
	return result
}

func GetViewContactPerson(customer_id string, orgid string) ([]string, []string, []string, []string, []string, string) {

	status := "false"
	var name []string
	var designation []string
	var phone []string
	var cellno []string
	var email []string
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT name, designation, phone, cell_no, email FROM customer_contact_person WHERE _customer_fkid=? and _orgid=? ORDER BY contact_type ASC", customer_id, orgid).Values(&maps)
	//fmt.Println("num=", num)
	//var i int64 = 0
	// for ; i < num; i++ {
	// 	fmt.Println(maps[i])
	// }
	//fmt.Println("***Retrieved warehouses= ", num)
	if err == nil {
		var new_name string
		var new_designation string
		var new_phone string
		var new_cellno string
		var new_email string
		var j int64 = 0
		for ; j < num; j++ {
			if maps[j]["name"] != nil && maps[j]["name"] != "" {
				new_name = maps[j]["name"].(string)
				name = append(name, new_name)

				if maps[j]["designation"] != nil {
					new_designation = maps[j]["designation"].(string)
				} else {
					new_designation = ""
				}
				designation = append(designation, new_designation)

				if maps[j]["phone"] != nil {
					new_phone = maps[j]["phone"].(string)
				} else {
					new_phone = ""
				}
				phone = append(phone, new_phone)

				if maps[j]["cell_no"] != nil {
					new_cellno = maps[j]["cell_no"].(string)
				} else {
					new_cellno = ""
				}
				cellno = append(cellno, new_cellno)

				if maps[j]["email"] != nil {
					new_email = maps[j]["email"].(string)
				} else {
					new_email = ""
				}
				email = append(email, new_email)
			}
		}
		status = "true"
	}
	//fmt.Println("***items=", items)
	//fmt.Println("***quantity=", quantity)
	return name, designation, phone, cellno, email, status
}

func GetCustomerWarehouses(customer_id string, orgid string) ([]string, []string, []string, []string, []string, []string, string) {

	status := "false"
	var address []string
	var city []string
	var state []string
	var country []string
	var pincode []string
	var gstin []string
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT address_line1, wcity, wstate, wcountry, wpincode, GSTIN FROM customer_warehouse_address WHERE customer_fkid=? and _orgid=? and status=1 and type_address=2", customer_id, orgid).Values(&maps)
	//fmt.Println("num=", num)
	//var i int64 = 0
	// for ; i < num; i++ {
	// 	fmt.Println(maps[i])
	// }
	//fmt.Println("***Retrieved warehouses= ", num)
	if err == nil {
		var new_address string
		var new_city string
		var new_state string
		var new_country string
		var new_pincode string
		var new_gstin string
		var j int64 = 0
		for ; j < num; j++ {
			new_address = maps[j]["address_line1"].(string)
			address = append(address, new_address)

			new_city = maps[j]["wcity"].(string)
			city = append(city, new_city)

			new_state = maps[j]["wstate"].(string)
			state = append(state, new_state)

			new_country = maps[j]["wcountry"].(string)
			country = append(country, new_country)

			new_pincode = maps[j]["wpincode"].(string)
			pincode = append(pincode, new_pincode)

			new_gstin = maps[j]["GSTIN"].(string)
			gstin = append(gstin, new_gstin)

		}
		status = "true"
	}
	//fmt.Println("***items=", items)
	//fmt.Println("***quantity=", quantity)
	return address, city, state, country, pincode, gstin, status
}

func SearchByCustomerDetails(companyName string, contactName string, Location string, _orgId string) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	var num int64
	var err error
	if contactName == "" && Location != "" {
		num, err = o.Raw("SELECT * FROM (SELECT c._id, c.customer_unique_code, c.company_name, c.grp, c.address, c.city, c.state, c.pincode, c.country, c.TIN, c.area_code, c.PAN, c.credit_in_days,c.credit_limit,c.CST, c.ECC, c.customer_type, c.date_of_introduction as i_date, c.service_location,c.gst,c.provisional_gst,c.ARN, c.GSTIN, c._orgid, cp.name, cp.designation, cp.phone, cp.cell_no, cp.email FROM customer as c, customer_contact_person as cp WHERE c._orgid=? and cp._orgid=? and c.status=1 and cp.contact_type=1 and cp._customer_fkid=c._id) AS cust WHERE cust._orgid =? and (cust.company_name like ? and (city like ? or state like ? or country like ? ) )", _orgId, _orgId, _orgId, "%"+companyName+"%", "%"+Location+"%", "%"+Location+"%", "%"+Location+"%").Values(&maps)
	}
	if contactName != "" && Location == "" {
		num, err = o.Raw("SELECT * FROM (SELECT c._id, c.customer_unique_code, c.company_name, c.grp, c.address, c.city, c.state, c.pincode, c.country, c.TIN, c.area_code, c.PAN, c.credit_in_days,c.credit_limit,c.CST, c.ECC, c.customer_type, c.date_of_introduction as i_date, c.service_location,c.gst,c.provisional_gst,c.ARN, c.GSTIN, c._orgid, cp.name, cp.designation, cp.phone, cp.cell_no, cp.email FROM customer as c, customer_contact_person as cp WHERE c._orgid=? and cp._orgid=? and c.status=1 and cp.contact_type=1 and cp._customer_fkid=c._id) AS cust WHERE cust._orgid =? and (cust.company_name like ? and name like ? )", _orgId, _orgId, _orgId, "%"+companyName+"%", "%"+contactName+"%").Values(&maps)

	}
	if contactName == "" && Location == "" {
		num, err = o.Raw("SELECT * FROM (SELECT c._id, c.customer_unique_code, c.company_name, c.grp, c.address, c.city, c.state, c.pincode, c.country, c.TIN, c.area_code, c.PAN, c.credit_in_days,c.credit_limit,c.CST, c.ECC, c.customer_type, c.date_of_introduction as i_date, c.service_location,c.gst,c.provisional_gst,c.ARN, c.GSTIN, c._orgid, cp.name, cp.designation, cp.phone, cp.cell_no, cp.email FROM customer as c, customer_contact_person as cp WHERE c._orgid=? and cp._orgid=? and c.status=1 and cp.contact_type=1 and cp._customer_fkid=c._id) AS cust WHERE cust._orgid =? and cust.company_name like ?", _orgId, _orgId, _orgId, "%"+companyName+"%").Values(&maps)

	}
	if contactName != "" && Location != "" {
		num, err = o.Raw("SELECT * FROM (SELECT c._id, c.customer_unique_code, c.company_name, c.grp, c.address, c.city, c.state, c.pincode, c.country, c.TIN, c.area_code, c.PAN, c.credit_in_days,c.credit_limit,c.CST, c.ECC, c.customer_type, c.date_of_introduction as i_date, c.service_location,c.gst,c.provisional_gst,c.ARN, c.GSTIN, c._orgid, cp.name, cp.designation, cp.phone, cp.cell_no, cp.email FROM customer as c, customer_contact_person as cp WHERE c._orgid=? and cp._orgid=? and c.status=1 and cp.contact_type=1 and cp._customer_fkid=c._id) AS cust WHERE cust._orgid =? and (cust.company_name like ? and name like ? and (city like ? or state like ? or country like ? ) )", _orgId, _orgId, _orgId, "%"+companyName+"%", "%"+contactName+"%", "%"+Location+"%", "%"+Location+"%", "%"+Location+"%").Values(&maps)
	}
	if err == nil && num > 0 {
		for k, v := range maps {
			if maps[k]["credit_in_days"] == nil {
				maps[k]["credit_in_days"] = ""
			}
			if maps[k]["credit_limit"] == nil {
				maps[k]["credit_limit"] = ""
			}
			if maps[k]["i_date"] != "" && maps[k]["i_date"] != nil {
				maps[k]["i_date"] = convertDateString(maps[k]["i_date"].(string))
			}
			if maps[k]["gst"] == nil {
				maps[k]["gst"] = ""
			}
			if maps[k]["provisional_gst"] == nil {
				maps[k]["provisional_gst"] = ""
			}
			if maps[k]["ARN"] == nil {
				maps[k]["ARN"] = ""
			}
			if maps[k]["name"] == nil {
				maps[k]["name"] = ""
			}
			if maps[k]["address"] == nil {
				maps[k]["address"] = ""
			}
			if maps[k]["designation"] == nil {
				maps[k]["designation"] = ""
			}
			if maps[k]["phone"] == nil {
				maps[k]["phone"] = ""
			}
			if maps[k]["cell_no"] == nil {
				maps[k]["cell_no"] = ""
			}
			if maps[k]["address_line1"] == nil {
				maps[k]["cell_no"] = ""
			}
			if maps[k]["wcity"] == nil {
				maps[k]["wcity"] = ""
			}
			if maps[k]["wstate"] == nil {
				maps[k]["wstate"] = ""
			}
			if maps[k]["wcountry"] == nil {
				maps[k]["wcountry"] = ""
			}
			if maps[k]["city"] == nil {
				maps[k]["city"] = ""
			}
			if maps[k]["state"] == nil {
				maps[k]["state"] = ""
			}
			if maps[k]["country"] == nil {
				maps[k]["country"] = ""
			}
			if maps[k]["pincode"] == nil {
				maps[k]["pincode"] = ""
			}

			var s string
			if maps[k]["city"] != "" {
				s = maps[k]["city"].(string)
			}
			if maps[k]["state"] != "" {
				if maps[k]["city"] != "" {
					s = s + ", "
				}
				s = s + maps[k]["state"].(string)
			}
			if maps[k]["country"] != "" {
				if maps[k]["state"] != "" {
					s = s + ", "
				}
				s = s + maps[k]["country"].(string)
			}
			if maps[k]["pincode"] != "" {
				if maps[k]["country"] != "" {
					s = s + ", "
				}
				s = s + maps[k]["pincode"].(string)
			}
			maps[k]["city_state_country_pincode"] = s

			key := "customer_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		result["status"] = nil
	}
	return result
}

func SearchCustomersByItemName(ItemCode string, _orgId string) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("select c.*, ccp.name, ccp.phone FROM ( Select so.* From (	Select soi.* FROM (select ioa.* from item_org_assoc as ioa where ioa._id = ? and ioa._orgid = ?) as items, sales_order_items AS soi where soi.item_org_assoc_fkid = items._id ) as salesorderitems , sales_order as so where so._id = salesorderitems._sales_order_fkid	) as sales, customer as c , customer_contact_person as ccp where sales.customer_fkid = c._id and c._id = ccp._customer_fkid", ItemCode, _orgId).Values(&maps)

	if err == nil && num > 0 {
		for k, v := range maps {
			if maps[k]["i_date"] != "" && maps[k]["i_date"] != nil {
				maps[k]["i_date"] = convertDateString(maps[k]["i_date"].(string))
			}
			var s string
			if maps[k]["city"] != "" {
				s = maps[k]["city"].(string)
			}
			if maps[k]["state"] != "" {
				if maps[k]["city"] != "" {
					s = s + ", "
				}
				s = s + maps[k]["state"].(string)
			}
			if maps[k]["country"] != "" {
				if maps[k]["state"] != "" {
					s = s + ", "
				}
				s = s + maps[k]["country"].(string)
			}
			if maps[k]["pincode"] != "" {
				if maps[k]["country"] != "" {
					s = s + ", "
				}
				s = s + maps[k]["pincode"].(string)
			}
			maps[k]["city_state_country_pincode"] = s
			key := "customer_" + strconv.Itoa(k)
			result[key] = v
		}
	}
	return result
}

func GetMaxCustomerCode(orgid string) int {
	var maxCode int
	maxCode = 1
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT max(customer_unique_code) as maxcode FROM customer where _orgid=?", orgid).Values(&maps)

	if num > 0 && err == nil {
		if maps[0]["maxcode"] != nil {
			code := maps[0]["maxcode"].(string)
			getValue, _ := strconv.ParseInt(code, 10, 64)
			maxCode = int(getValue)
		}
	}
	return maxCode
}
