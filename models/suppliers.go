package models

import (
	"strconv"
	"time"
	//"strings"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
)

type SupplierContactPerson struct {
	ContactPerson string
	Designation   string
	Email         string
	PhoneNo       string
	CellNo        string
}

type SupplierWarehouse struct {
	WarehouseAddress string
	WCity            string
	WState           string
	WCountry         string
	WPincode         string
	GSTIN            string
}

type SupplierDetails struct {
	Code              string
	CompanyName       string
	OfficeAddress     string
	City              string
	State             string
	Country           string
	Pincode           string
	MainGSTIN         string
	CST               string
	CreditInDays      string
	CreditLimit       string
	PAN               string
	ECC               string
	TIN               string
	Notes             string
	GST               string
	PGST              string
	ARN               string
	Introduction      string
	SupplierNameCheck string
	SupplierID        string
}

type AddSupplier struct {
	Orgid             string
	Username          string
	Userid            string
	Code              string
	CompanyName       string
	ContactPerson     string
	Designation       string
	Email             string
	PhoneNo           string
	CellNo            string
	OfficeAddress     string
	City              string
	State             string
	Country           string
	Pincode           string
	MainGSTIN         string
	CST               string
	CreditInDays      string
	CreditLimit       string
	PAN               string
	ECC               string
	TIN               string
	Notes             string
	GST               string
	PGST              string
	ARN               string
	Introduction      string
	WarehouseAddress  string
	WCity             string
	WState            string
	WCountry          string
	WPincode          string
	GSTIN             string
	IsPresentinDB     int
	SupplierNameCheck string
	SupplierID        string
	NewGenCode        string
	SupplierUniqueSequence string
}

type Viewassoc struct {
	Supplier string
	Services []string
	//Status                  string
	Orgid    string
	Username string
	Userid   string
}

func GetActiveDropdownListOfSuppliersByOrgid(orgID string) []orm.Params {
	o := orm.NewOrm()
	var maps []orm.Params
	_, err := o.Raw("SELECT _id, supplier_unique_code,company_name from supplier_org_assoc where _orgid=? and status = 1 ", orgID).Values(&maps)
	if err != nil {

	}
	return maps
}

func GetSupplierDetailsBySupplierName(supplierName string, orgID string) AddSupplier {
	o := orm.NewOrm()
	var maps []orm.Params
	var itemStruct = AddSupplier{}
	num, err := o.Raw("SELECT soa._id, soa.company_name, soa.supplier_unique_code, soa.hq_address_line1, soa.city, soa.state, soa.country, soa.pincode, soa.tin, soa.cst, soa.pan, soa.ecc, soa.gst, soa.notes, soa.provisional_gst, soa.ARN, soa.date_of_introduction, soa.GSTIN, scp.contact_name, scp.designation, scp.email, scp.phone, scp.mobile, sa.address_line1, sa.wcity, sa.wstate, sa.wcountry, sa.wpincode, sa.type_address from supplier_org_assoc as soa, supplier_contact_person as scp, supplier_address as sa where soa._orgid =? and scp._orgid =? and sa._orgid =? and scp.supplier_org_assoc_fkid=soa._id and sa.supplier_org_assoc_fkid=soa._id and soa.status = 1 and soa.company_name = ? ", orgID, orgID, orgID, supplierName).Values(&maps)
	if err != nil || num == 0 {
		itemStruct.IsPresentinDB = 0
	} else {
		itemStruct.SupplierID = maps[0]["_id"].(string)
		itemStruct.SupplierNameCheck = maps[0]["company_name"].(string)
		itemStruct.CompanyName = maps[0]["company_name"].(string)
		itemStruct.Code = maps[0]["supplier_unique_code"].(string)
		itemStruct.ContactPerson = maps[0]["contact_name"].(string)
		itemStruct.Designation = maps[0]["designation"].(string)
		itemStruct.Email = maps[0]["email"].(string)
		itemStruct.PhoneNo = maps[0]["phone"].(string)
		itemStruct.CellNo = maps[0]["mobile"].(string)
		itemStruct.OfficeAddress = maps[0]["hq_address_line1"].(string)
		itemStruct.City = maps[0]["city"].(string)
		itemStruct.State = maps[0]["state"].(string)
		itemStruct.Country = maps[0]["country"].(string)
		itemStruct.Pincode = maps[0]["pincode"].(string)
		if maps[0]["GSTIN"] != nil {
			itemStruct.MainGSTIN = maps[0]["GSTIN"].(string)
		} else {
			itemStruct.MainGSTIN = ""
		}
		if maps[0]["cst"] != nil {
			itemStruct.CST = maps[0]["cst"].(string)
		} else {
			itemStruct.CST = ""
		}
		if maps[0]["pan"] != nil {
			itemStruct.PAN = maps[0]["pan"].(string)
		} else {
			itemStruct.PAN = ""
		}
		if maps[0]["ecc"] != nil {
			itemStruct.ECC = maps[0]["ecc"].(string)
		} else {
			itemStruct.ECC = ""
		}
		if maps[0]["credit_limit"] != nil {
			itemStruct.CreditLimit = maps[0]["credit_limit"].(string)
		} else {
			itemStruct.CreditLimit = ""
		}
		if maps[0]["credit_in_days"] != nil {
			itemStruct.CreditInDays = maps[0]["credit_in_days"].(string)
		} else {
			itemStruct.CreditInDays = ""
		}
		if maps[0]["tin"] != nil {
			itemStruct.TIN = maps[0]["tin"].(string)
		} else {
			itemStruct.TIN = ""
		}
		if maps[0]["notes"] != nil {
			itemStruct.Notes = maps[0]["notes"].(string)
		} else {
			itemStruct.Notes = ""
		}
		if maps[0]["gst"] != nil {
			itemStruct.GST = maps[0]["gst"].(string)
		} else {
			itemStruct.GST = ""
		}

		if maps[0]["provisional_gst"] != nil {
			itemStruct.PGST = maps[0]["provisional_gst"].(string)
		} else {
			itemStruct.PGST = ""
		}
		if maps[0]["ARN"] != nil {
			itemStruct.ARN = maps[0]["ARN"].(string)
		} else {
			itemStruct.ARN = ""
		}
		if maps[0]["date_of_introduction"] != nil {
			itemStruct.Introduction = maps[0]["date_of_introduction"].(string)
		} else {
			itemStruct.Introduction = ""
		}
		if maps[0]["address_line1"] != nil {
			itemStruct.WarehouseAddress = maps[0]["address_line1"].(string)
		} else {
			itemStruct.WarehouseAddress = ""
		}
		if maps[0]["wcity"] != nil {
			itemStruct.WCity = maps[0]["wcity"].(string)
		} else {
			itemStruct.WCity = ""
		}
		if maps[0]["wstate"] != nil {
			itemStruct.WState = maps[0]["wstate"].(string)
		} else {
			itemStruct.WState = ""
		}
		if maps[0]["wcountry"] != nil {
			itemStruct.WCountry = maps[0]["wcountry"].(string)
		} else {
			itemStruct.WCountry = ""
		}
		if maps[0]["wpincode"] != nil {
			itemStruct.WPincode = maps[0]["wpincode"].(string)
		} else {
			itemStruct.WPincode = ""
		}

		itemStruct.IsPresentinDB = 1
	}
	return itemStruct
}

func GetSupplierDetailsByPkId(pkID string, orgID string) AddSupplier {
	o := orm.NewOrm()
	var maps []orm.Params
	var itemStruct = AddSupplier{}
	num, err := o.Raw("SELECT soa._id, soa.company_name, soa.supplier_unique_code, soa.hq_address_line1, soa.city, soa.state, soa.country, soa.pincode, soa.tin, soa.cst, soa.credit_in_days, soa.credit_limit, soa.pan, soa.ecc, soa.gst, soa.notes, soa.provisional_gst, soa.ARN, soa.date_of_introduction, soa.GSTIN, scp.contact_name, scp.designation, scp.email, scp.phone, scp.mobile, sa.address_line1, sa.wcity, sa.wstate, sa.wcountry, sa.wpincode, sa.GSTIN, sa.type_address from supplier_org_assoc as soa, supplier_contact_person as scp, supplier_address as sa where soa._orgid =? and scp._orgid =? and sa._orgid =? and scp.supplier_org_assoc_fkid=soa._id and sa.supplier_org_assoc_fkid=soa._id and soa.status = 1 and soa._id = ? ", orgID, orgID, orgID, pkID).Values(&maps)
	if err != nil || num == 0 {
		itemStruct.IsPresentinDB = 0
	} else {
		itemStruct.SupplierID = maps[0]["_id"].(string)
		itemStruct.SupplierNameCheck = maps[0]["company_name"].(string)
		itemStruct.CompanyName = maps[0]["company_name"].(string)
		itemStruct.Code = maps[0]["supplier_unique_code"].(string)
		itemStruct.ContactPerson = maps[0]["contact_name"].(string)
		itemStruct.Designation = maps[0]["designation"].(string)
		if maps[0]["email"] != nil {
			itemStruct.Email = maps[0]["email"].(string)
		} else {
			itemStruct.Email = ""
		}
		if maps[0]["phone"] != nil {
			itemStruct.PhoneNo = maps[0]["phone"].(string)
		} else {
			itemStruct.PhoneNo = ""
		}
		if maps[0]["mobile"] != nil {
			itemStruct.CellNo = maps[0]["mobile"].(string)
		} else {
			itemStruct.CellNo = ""
		}
		itemStruct.OfficeAddress = maps[0]["hq_address_line1"].(string)
		itemStruct.City = maps[0]["city"].(string)
		itemStruct.State = maps[0]["state"].(string)
		itemStruct.Country = maps[0]["country"].(string)
		itemStruct.Pincode = maps[0]["pincode"].(string)
		if maps[0]["GSTIN"] != nil {
			itemStruct.MainGSTIN = maps[0]["GSTIN"].(string)
		} else {
			itemStruct.MainGSTIN = ""
		}
		if maps[0]["cst"] != nil {
			itemStruct.CST = maps[0]["cst"].(string)
		} else {
			itemStruct.CST = ""
		}
		if maps[0]["pan"] != nil {
			itemStruct.PAN = maps[0]["pan"].(string)
		} else {
			itemStruct.PAN = ""
		}
		if maps[0]["ecc"] != nil {
			itemStruct.ECC = maps[0]["ecc"].(string)
		} else {
			itemStruct.ECC = ""
		}
		if maps[0]["credit_limit"] != nil {
			itemStruct.CreditLimit = maps[0]["credit_limit"].(string)
		} else {
			itemStruct.CreditLimit = ""
		}
		if maps[0]["credit_in_days"] != nil {
			itemStruct.CreditInDays = maps[0]["credit_in_days"].(string)
		} else {
			itemStruct.CreditInDays = ""
		}
		if maps[0]["tin"] != nil {
			itemStruct.TIN = maps[0]["tin"].(string)
		} else {
			itemStruct.TIN = ""
		}
		if maps[0]["notes"] != nil {
			itemStruct.Notes = maps[0]["notes"].(string)
		} else {
			itemStruct.Notes = ""
		}
		if maps[0]["gst"] != nil {
			itemStruct.GST = maps[0]["gst"].(string)
		} else {
			itemStruct.GST = ""
		}

		if maps[0]["provisional_gst"] != nil {
			itemStruct.PGST = maps[0]["provisional_gst"].(string)
		} else {
			itemStruct.PGST = ""
		}
		if maps[0]["ARN"] != nil {
			itemStruct.ARN = maps[0]["ARN"].(string)
		} else {
			itemStruct.ARN = ""
		}
		if maps[0]["date_of_introduction"] != nil {
			itemStruct.Introduction = maps[0]["date_of_introduction"].(string)
		} else {
			itemStruct.Introduction = ""
		}
		if maps[0]["address_line1"] != nil {
			itemStruct.WarehouseAddress = maps[0]["address_line1"].(string)
		} else {
			itemStruct.WarehouseAddress = ""
		}
		if maps[0]["wcity"] != nil {
			itemStruct.WCity = maps[0]["wcity"].(string)
		} else {
			itemStruct.WCity = ""
		}
		if maps[0]["wstate"] != nil {
			itemStruct.WState = maps[0]["wstate"].(string)
		} else {
			itemStruct.WState = ""
		}
		if maps[0]["wcountry"] != nil {
			itemStruct.WCountry = maps[0]["wcountry"].(string)
		} else {
			itemStruct.WCountry = ""
		}
		if maps[0]["wpincode"] != nil {
			itemStruct.WPincode = maps[0]["wpincode"].(string)
		} else {
			itemStruct.WPincode = ""
		}
		if maps[0]["GSTIN"] != nil {
			itemStruct.GSTIN = maps[0]["GSTIN"].(string)
		} else {
			itemStruct.GSTIN = ""
		}

		itemStruct.IsPresentinDB = 1
	}
	return itemStruct
}

func GetSupplierDetailsByCompanyID(pkID string, orgID string) (int64, SupplierDetails) {
	o := orm.NewOrm()
	var maps []orm.Params
	var itemStruct = SupplierDetails{}
	num, err := o.Raw("SELECT * from supplier_org_assoc where _id = ?  and _orgid = ?", pkID, orgID).Values(&maps)
	if err != nil || num == 0 {

	} else {
		itemStruct.SupplierID = maps[0]["_id"].(string)
		itemStruct.SupplierNameCheck = maps[0]["company_name"].(string)
		itemStruct.CompanyName = maps[0]["company_name"].(string)
		itemStruct.Code = maps[0]["supplier_unique_code"].(string)
		itemStruct.OfficeAddress = maps[0]["hq_address_line1"].(string)
		itemStruct.City = maps[0]["city"].(string)
		itemStruct.State = maps[0]["state"].(string)
		itemStruct.Country = maps[0]["country"].(string)
		itemStruct.Pincode = maps[0]["pincode"].(string)
		if maps[0]["GSTIN"] != nil {
			itemStruct.MainGSTIN = maps[0]["GSTIN"].(string)
		} else {
			itemStruct.MainGSTIN = ""
		}
		if maps[0]["cst"] != nil {
			itemStruct.CST = maps[0]["cst"].(string)
		} else {
			itemStruct.CST = ""
		}
		if maps[0]["pan"] != nil {
			itemStruct.PAN = maps[0]["pan"].(string)
		} else {
			itemStruct.PAN = ""
		}
		if maps[0]["ecc"] != nil {
			itemStruct.ECC = maps[0]["ecc"].(string)
		} else {
			itemStruct.ECC = ""
		}
		if maps[0]["credit_limit"] != nil {
			itemStruct.CreditLimit = maps[0]["credit_limit"].(string)
		} else {
			itemStruct.CreditLimit = ""
		}
		if maps[0]["credit_in_days"] != nil {
			itemStruct.CreditInDays = maps[0]["credit_in_days"].(string)
		} else {
			itemStruct.CreditInDays = ""
		}
		if maps[0]["tin"] != nil {
			itemStruct.TIN = maps[0]["tin"].(string)
		} else {
			itemStruct.TIN = ""
		}
		if maps[0]["notes"] != nil {
			itemStruct.Notes = maps[0]["notes"].(string)
		} else {
			itemStruct.Notes = ""
		}
		if maps[0]["gst"] != nil {
			itemStruct.GST = maps[0]["gst"].(string)
		} else {
			itemStruct.GST = ""
		}

		if maps[0]["provisional_gst"] != nil {
			itemStruct.PGST = maps[0]["provisional_gst"].(string)
		} else {
			itemStruct.PGST = ""
		}
		if maps[0]["ARN"] != nil {
			itemStruct.ARN = maps[0]["ARN"].(string)
		} else {
			itemStruct.ARN = ""
		}
		if maps[0]["date_of_introduction"] != nil {
			itemStruct.Introduction = maps[0]["date_of_introduction"].(string)
		} else {
			itemStruct.Introduction = ""
		}
	}
	return num, itemStruct
}

func GetSupplierWarehousesByPkId(pkID string, orgID string) map[string]orm.Params {

	result := make(map[string]orm.Params)

	o := orm.NewOrm()
	var maps []orm.Params
	//num, err := o.Raw("SELECT * FROM item_master ").Values(&maps)
	num, err := o.Raw("SELECT _id, address_line1, wcity, wstate, wcountry, wpincode, GSTIN FROM supplier_address WHERE supplier_org_assoc_fkid=? and _orgid=? and status=1 and type_address=2", pkID, orgID).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		var i int64 = 0
		for ; i < num; i++ {
			//fmt.Println(maps[i])
		}
		for k, v := range maps {
			if maps[k]["address_line1"] == nil {
				maps[k]["address_line1"] = ""
			}
			if maps[k]["wcity"] == nil {
				maps[k]["wcity"] = ""
			}
			if maps[k]["wstate"] == nil {
				maps[k]["wstate"] = ""
			}
			if maps[k]["wcountry"] == nil {
				maps[k]["wcountry"] = ""
			}
			if maps[k]["wpincode"] == nil {
				maps[k]["wpincode"] = ""
			}
			if maps[k]["GSTIN"] == nil {
				maps[k]["GSTIN"] = ""
			}

			key := "customer_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}
func GetSupplierWarehousesByCompanyName(supplierName string, orgID string) map[string]orm.Params {

	result := make(map[string]orm.Params)

	o := orm.NewOrm()

	var maps01 []orm.Params
	_, err01 := o.Raw("SELECT _id FROM supplier_org_assoc WHERE _orgid=? and status=1 and company_name=?", orgID, supplierName).Values(&maps01)
	if err01 != nil {
		result["status"] = nil
		return result
	}

	var maps []orm.Params
	//num, err := o.Raw("SELECT * FROM item_master ").Values(&maps)
	num, err := o.Raw("SELECT _id, address_line1, wcity, wstate, wcountry, wpincode, GSTIN FROM supplier_address WHERE supplier_org_assoc_fkid=? and _orgid=? and status=1 and type_address=2", maps01[0]["_id"], orgID).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		// var i int64 = 0
		// for ; i < num; i++ {
		// 	fmt.Println(maps[i])
		// }
		for k, v := range maps {
			if maps[k]["address_line1"] == nil {
				maps[k]["address_line1"] = ""
			}
			if maps[k]["wcity"] == nil {
				maps[k]["wcity"] = ""
			}
			if maps[k]["wstate"] == nil {
				maps[k]["wstate"] = ""
			}
			if maps[k]["wcountry"] == nil {
				maps[k]["wcountry"] = ""
			}
			if maps[k]["wpincode"] == nil {
				maps[k]["wpincode"] = ""
			}
			if maps[k]["GSTIN"] == nil {
				maps[k]["GSTIN"] = ""
			}

			key := "customer_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetSupplierWarehousesByCompanyID(supplierID string, orgID string) map[string]orm.Params {
	result := make(map[string]orm.Params)
	var maps []orm.Params
	o := orm.NewOrm()

	num, err := o.Raw("SELECT _id, address_line1, wcity, wstate, wcountry, wpincode, GSTIN FROM supplier_address WHERE supplier_org_assoc_fkid=? and _orgid=? and status=1 and type_address=2", supplierID, orgID).Values(&maps)
	if err == nil && num > 0 {

		for k, v := range maps {
			if maps[k]["address_line1"] == nil {
				maps[k]["address_line1"] = ""
			}
			if maps[k]["wcity"] == nil {
				maps[k]["wcity"] = ""
			}
			if maps[k]["wstate"] == nil {
				maps[k]["wstate"] = ""
			}
			if maps[k]["wcountry"] == nil {
				maps[k]["wcountry"] = ""
			}
			if maps[k]["wpincode"] == nil {
				maps[k]["wpincode"] = ""
			}
			if maps[k]["GSTIN"] == nil {
				maps[k]["GSTIN"] = ""
			}

			key := "customer_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func CheckForExistingSupplierName(supplierName string, orgID string) int {
	o := orm.NewOrm()
	var resultMap []orm.Params
	num, err := o.Raw("SELECT company_name FROM supplier_org_assoc WHERE company_name=? and  _orgid=? and status = 1", supplierName, orgID).Values(&resultMap)
	//fmt.Println("Query Check Name==========", num)
	if err == nil && num > 0 {
		return 1
	}
	return 0
}

func CheckForExistingSupplierUniqueCode(supplierUniqueCode string, orgID string) int {
	o := orm.NewOrm()
	var resultMap []orm.Params
	num, err := o.Raw("SELECT supplier_unique_code FROM supplier_org_assoc WHERE supplier_unique_code=? and  _orgid=? and status = 1", supplierUniqueCode, orgID).Values(&resultMap)
	//fmt.Println("Query Check Code==========", num)
	if err == nil && num > 0 {
		return 1
	}
	return 0
}

func AddSuppliers(formData AddSupplier, warehouses []map[string]string, contactpersons []map[string]string) int {

	o := orm.NewOrm()
	ormTransactionalErr := o.Begin()
	//user := Users{ EmailAddress: email, Password: password }

	var introDate *string
	introDate = nil
	if formData.Introduction != "" {
		tempIntroDate := formData.Introduction
		introDate = &tempIntroDate
	}
	var credit_in_days *string
	credit_in_days = nil
	if formData.CreditInDays != "" {
		tempCreditDays := formData.CreditInDays
		credit_in_days = &tempCreditDays
	}
	var credit_limit *string
	credit_limit = nil
	if formData.CreditLimit != "" {
		tempCreditLimit := formData.CreditLimit
		credit_limit = &tempCreditLimit
	}

	_, err := o.Raw("INSERT INTO supplier_org_assoc ( company_name, hq_address_line1, city, state, country, pincode, status, last_updated, tin, cst, credit_in_days, credit_limit, pan, ecc, notes, supplier_unique_code, _orgid,provisional_gst,ARN, date_of_introduction, GSTIN, supplier_code_gen) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ", formData.CompanyName, formData.OfficeAddress, formData.City, formData.State, formData.Country, formData.Pincode, 1, time.Now(), formData.TIN, formData.CST, &credit_in_days, &credit_limit, formData.PAN, formData.ECC, formData.Notes, formData.Code, formData.Orgid, formData.PGST, formData.ARN, &introDate, formData.MainGSTIN, formData.NewGenCode).Exec()
	if err != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}

	var maps []orm.Params
	num, err1 := o.Raw("SELECT _id FROM supplier_org_assoc WHERE supplier_unique_code = ? and company_name = ? and _orgid = ? ", formData.Code, formData.CompanyName, formData.Orgid).Values(&maps)
	if err1 != nil || num < 1 {
		ormTransactionalErr = o.Rollback()
		return 0
	}

	_, err2 := o.Raw("INSERT INTO supplier_address (supplier_org_assoc_fkid, _orgid, address_line1, wcity, wstate, wcountry, wpincode, type_address, GSTIN, status, last_updated) VALUES (?,?,?,?,?,?,?,?,?,?,?)", maps[0]["_id"], formData.Orgid, formData.OfficeAddress, formData.City, formData.State, formData.Country, formData.Pincode, "1", formData.MainGSTIN, "1", time.Now()).Exec()
	if err2 != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}
	//supplierAddressID, _ := result.LastInsertId()

	// _, err3 := o.Raw("INSERT INTO supplier_contact_person (supplier_org_assoc_fkid, _orgid, contact_name, designation, email, phone, mobile, supplier_address_fkid, last_updated) VALUES (?,?,?,?,?,?,?,?,?) ", maps[0]["_id"], formData.Orgid, formData.ContactPerson, formData.Designation, formData.Email, formData.PhoneNo, formData.CellNo, supplierAddressID, time.Now()).Exec()
	// if err3 != nil {
	// 	ormTransactionalErr = o.Rollback()
	// 	return 0
	// }

	for cnum, itt := range contactpersons {
		if itt["contact_person"] != "" {
			var designation *string
			designation = nil
			if itt["designation"] != "" {
				tempdesignation := itt["designation"]
				designation = &tempdesignation
			}
			var phoneno *string
			phoneno = nil
			if itt["phone_no"] != "" {
				tempphoneno := itt["phone_no"]
				phoneno = &tempphoneno
			}
			var cellno *string
			cellno = nil
			if itt["cell_no"] != "" {
				tempcellno := itt["cell_no"]
				cellno = &tempcellno
			}
			var email *string
			email = nil
			if itt["email"] != "" {
				tempemail := itt["email"]
				email = &tempemail
			}
			// var maps01 []orm.Params
			// count, err03 := o.Raw("SELECT _id FROM customer_contact_person WHERE _customer_fkid=? and _orgid=? and name=? and designation=? and phone=? and cell_no=? and email=? ", maps[0]["_id"], formData.Orgid, itt["contact_person"], &designation, &phoneno, &cellno, &email).Values(&maps01)
			// if err03 != nil {
			// 	ormTransactionalErr = o.Rollback()
			// 	return 0
			// }

			var contact_type string
			if cnum == 0 {
				contact_type = "1"
			} else {
				contact_type = "2"
			}
			_, err3 := o.Raw("INSERT INTO supplier_contact_person (_orgid, contact_name, designation, phone, mobile, email, contact_type, supplier_org_assoc_fkid, last_updated) VALUES (?,?,?,?,?,?,?,?,?) ", formData.Orgid, itt["contact_person"], &designation, &phoneno, &cellno, &email, contact_type, maps[0]["_id"], time.Now()).Exec()
			if err3 != nil {
				ormTransactionalErr = o.Rollback()
				return 0
			}
		}
	}

	for _, it := range warehouses {
		if it["warehouse_address"] != "" {
			// var maps1 []orm.Params
			// count, err4 := o.Raw("SELECT _id FROM supplier_address WHERE supplier_org_assoc_fkid=? and _orgid=? and address_line1=? and wcity=? and wstate=? and wcountry=? and wpincode=? and GSTIN=? and status=?", maps[0]["_id"], formData.Orgid, it["warehouse_address"], it["wcity"], it["wstate"], it["wcountry"], it["wpincode"], it["gstin"], "1").Values(&maps1)
			// if err4 != nil {
			// 	ormTransactionalErr = o.Rollback()
			// 	return 0
			// }
			// if count < 1 {
			_, err5 := o.Raw("INSERT INTO supplier_address (supplier_org_assoc_fkid, _orgid, address_line1, wcity, wstate, wcountry, wpincode, type_address, GSTIN, status, last_updated) VALUES (?,?,?,?,?,?,?,?,?,?,?)", maps[0]["_id"], formData.Orgid, it["warehouse_address"], it["wcity"], it["wstate"], it["wcountry"], it["wpincode"], "2", it["gstin"], "1", time.Now()).Exec()
			if err5 != nil {
				ormTransactionalErr = o.Rollback()
				return 0
			}
			// }
		}
	}

	if ormTransactionalErr != nil {
		return 0
	}
	ormTransactionalErr = o.Commit()
	return 1
}

func AddImportSuppliers(formData AddSupplier) int {

	o := orm.NewOrm()
	ormTransactionalErr := o.Begin()
	//user := Users{ EmailAddress: email, Password: password }

	var introDate *string
	introDate = nil
	if formData.Introduction != "" {
		tempIntroDate := formData.Introduction
		introDate = &tempIntroDate
	}
	var credit_in_days *string
	credit_in_days = nil
	if formData.CreditInDays != "" {
		tempCreditDays := formData.CreditInDays
		credit_in_days = &tempCreditDays
	}
	var credit_limit *string
	credit_limit = nil
	if formData.CreditLimit != "" {
		tempCreditLimit := formData.CreditLimit
		credit_limit = &tempCreditLimit
	}

	_, err := o.Raw("INSERT INTO supplier_org_assoc ( company_name, hq_address_line1, city, state, country, pincode, status, last_updated, tin, cst, credit_in_days, credit_limit, pan, ecc, notes, supplier_code_gen, supplier_unique_code, _orgid,provisional_gst,ARN, date_of_introduction, GSTIN) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ", formData.CompanyName, formData.OfficeAddress, formData.City, formData.State, formData.Country, formData.Pincode, 1, time.Now(), formData.TIN, formData.CST, &credit_in_days, &credit_limit, formData.PAN, formData.ECC, formData.Notes, formData.Code, formData.SupplierUniqueSequence, formData.Orgid, formData.PGST, formData.ARN, &introDate, formData.MainGSTIN).Exec()

	if err != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}

	var maps []orm.Params
	num, err1 := o.Raw("SELECT _id FROM supplier_org_assoc WHERE supplier_unique_code = ? and company_name = ? and _orgid = ? ", formData.Code, formData.CompanyName, formData.Orgid).Values(&maps)
	if err1 != nil || num < 1 {
		ormTransactionalErr = o.Rollback()
		return 0
	}

	result, err2 := o.Raw("INSERT INTO supplier_address (supplier_org_assoc_fkid, _orgid, address_line1, wcity, wstate, wcountry, wpincode, type_address, GSTIN, status, last_updated) VALUES (?,?,?,?,?,?,?,?,?,?,?)", maps[0]["_id"], formData.Orgid, formData.WarehouseAddress, formData.WCity, formData.WState, formData.WCountry, formData.WPincode, "1", formData.GSTIN, "1", time.Now()).Exec()
	if err2 != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}
	supplierAddressID, _ := result.LastInsertId()

	_, err3 := o.Raw("INSERT INTO supplier_contact_person (supplier_org_assoc_fkid, _orgid, contact_name, designation, email, phone, mobile, supplier_address_fkid, last_updated) VALUES (?,?,?,?,?,?,?,?,?) ", maps[0]["_id"], formData.Orgid, formData.ContactPerson, formData.Designation, formData.Email, formData.PhoneNo, formData.CellNo, supplierAddressID, time.Now()).Exec()
	if err3 != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}

	if ormTransactionalErr != nil {
		return 0
	}
	ormTransactionalErr = o.Commit()
	return 1
}

func InsertWarehouses(formData AddSupplier) int {

	o := orm.NewOrm()
	var maps []orm.Params
	num, err1 := o.Raw("SELECT _id FROM supplier_org_assoc WHERE supplier_unique_code = ? and company_name = ? and _orgid = ? and hq_address_line1 = ? and city=? and state=? and country=? and pincode = ? and status = ? ", formData.Code, formData.CompanyName, formData.Orgid, formData.OfficeAddress, formData.City, formData.State, formData.Country, formData.Pincode, "1").Values(&maps)
	if err1 == nil && num > 0 {
		var maps1 []orm.Params
		count, err2 := o.Raw("SELECT _id FROM supplier_address WHERE supplier_org_assoc_fkid=? and _orgid=? and address_line1=? and wcity=? and wstate=? and wcountry=? and wpincode=? and GSTIN=? and status=?", maps[0]["_id"], formData.Orgid, formData.WarehouseAddress, formData.WCity, formData.WState, formData.WCountry, formData.WPincode, formData.GSTIN, "1").Values(&maps1)
		if err2 == nil {
			if count < 1 {
				_, err3 := o.Raw("INSERT INTO supplier_address (supplier_org_assoc_fkid, _orgid, address_line1, wcity, wstate, wcountry, wpincode, GSTIN, status, last_updated) VALUES (?,?,?,?,?,?,?,?,?,?)", maps[0]["_id"], formData.Orgid, formData.WarehouseAddress, formData.WCity, formData.WState, formData.WCountry, formData.WPincode, formData.GSTIN, "1", time.Now()).Exec()
				if err3 != nil {
					return 0
				}
			}
		} else {
			return 0
		}
	} else {
		return 0
	}
	return 1
}

func DeleteSuppliers(supplier_id string, orgid string) string {
	result := "false"
	o := orm.NewOrm()
	//var values []orm.Params
	_, err := o.Raw("UPDATE supplier_org_assoc SET status=?, last_updated=? WHERE _orgid=? and _id=? ", "0", time.Now(), orgid, supplier_id).Exec()
	if err != nil {
		//fmt.Println("Error in UPDATE")
	} else {
		result = "true"
	}
	return result
}

func GetAddSupplier(formData AddSupplier) (map[string]orm.Params, int) {
	result := make(map[string]orm.Params)
	queryCount := 1
	o := orm.NewOrm()
	var maps []orm.Params
	//num, err := o.Raw("SELECT soa._id, soa.supplier_unique_code, soa._orgid, soa.company_name, soa.hq_address_line1, soa.city,soa.state,soa.country,soa.pincode,soa.tin,soa.cst,soa.pan,soa.ecc,soa.gst, soa.notes,soa.provisional_gst,soa.ARN, soa.date_of_introduction, soa.GSTIN, scp.contact_name, scp.designation, scp.email, scp.phone, scp.mobile, sa.address_line1, sa.wcity, sa.wstate, sa.wcountry, sa.wpincode from supplier_org_assoc as soa , supplier_contact_person as scp, supplier_address as sa where soa._orgid=? and scp._orgid=? and sa._orgid=? and scp.supplier_org_assoc_fkid = soa._id and sa.supplier_org_assoc_fkid=soa._id and soa.status = 1 ", formData.Orgid, formData.Orgid, formData.Orgid).Values(&maps)
	num, err := o.Raw("SELECT soa._id, soa.supplier_code_gen, soa._orgid, soa.company_name, soa.hq_address_line1, soa.city,soa.state,soa.country,soa.pincode,soa.tin,soa.cst, soa.credit_in_days, soa.credit_limit, soa.pan,soa.ecc,soa.gst, soa.notes,soa.provisional_gst,soa.ARN, soa.date_of_introduction, soa.GSTIN FROM supplier_org_assoc as soa WHERE soa._orgid=? and soa.status = 1", formData.Orgid).Values(&maps)
	if err == nil && num > 0 {
		for k, v := range maps {
			if maps[k]["date_of_introduction"] != nil && maps[k]["date_of_introduction"] != "" {
				maps[k]["date_of_introduction"] = convertDateString(maps[k]["date_of_introduction"].(string))
			}

			var s string
			if maps[k]["city"] != "" && maps[k]["city"] != nil {
				s = maps[k]["city"].(string)
			}
			if maps[k]["state"] != "" && maps[k]["state"] != nil {
				if maps[k]["city"] != "" && maps[k]["city"] != nil {
					s = s + ", "
				}
				s = s + maps[k]["state"].(string)
			}
			// if maps[k]["country"] != "" {
			// 	if maps[k]["state"] != "" {
			// 		s = s + ", "
			// 	}
			// 	s = s + maps[k]["country"].(string)
			// }
			if maps[k]["pincode"] != "" && maps[k]["pincode"] != nil {
				if (maps[k]["state"] != "" && maps[k]["state"] != nil) || (maps[k]["city"] != "" && maps[k]["city"] != nil) {
					s = s + ", "
				}
				s = s + maps[k]["pincode"].(string)
			}
			maps[k]["city_state_pincode"] = s
			key := "supplier_" + strconv.Itoa(k)
			result[key] = v

			var maps1 []orm.Params
			count, err1 := o.Raw("SELECT contact_name, designation, phone, mobile, email FROM supplier_contact_person as scp WHERE scp._orgid=? and scp.supplier_org_assoc_fkid=? and contact_type=1", formData.Orgid, maps[k]["_id"]).Values(&maps1)
			if count > 0 && err1 == nil {
				if maps1[0]["contact_name"] != nil {
					maps[k]["contact_name"] = maps1[0]["contact_name"].(string)
				} else {
					maps[k]["contact_name"] = ""
				}
				if maps1[0]["designation"] != nil {
					maps[k]["designation"] = maps1[0]["designation"].(string)
				} else {
					maps[k]["designation"] = ""
				}
				if maps1[0]["phone"] != nil {
					maps[k]["phone"] = maps1[0]["phone"].(string)
				} else {
					maps[k]["phone"] = ""
				}
				if maps1[0]["mobile"] != nil {
					maps[k]["mobile"] = maps1[0]["mobile"].(string)
				} else {
					maps[k]["mobile"] = ""
				}
				if maps1[0]["email"] != nil {
					maps[k]["email"] = maps1[0]["email"].(string)
				} else {
					maps[k]["email"] = ""
				}
			} else {
				maps[k]["contact_name"] = ""
				maps[k]["designation"] = ""
				maps[k]["phone"] = ""
				maps[k]["mobile"] = ""
				maps[k]["email"] = ""
			}
		}

	} else {
		//fmt.Println(err)
		queryCount = 0
	}
	return result, queryCount
}

func GetSupplier(formData AddSupplier) (map[string]orm.Params, int) {
	result := make(map[string]orm.Params)
	queryCount := 1
	o := orm.NewOrm()
	var maps []orm.Params
	//num, err := o.Raw("SELECT soa._id, soa.supplier_unique_code, soa._orgid, soa.company_name, soa.hq_address_line1, soa.city,soa.state,soa.country,soa.pincode,soa.tin,soa.cst,soa.pan,soa.ecc,soa.gst, soa.notes,soa.provisional_gst,soa.ARN, soa.date_of_introduction, soa.GSTIN, scp.contact_name, scp.designation, scp.email, scp.phone, scp.mobile, sa.address_line1, sa.wcity, sa.wstate, sa.wcountry, sa.wpincode from supplier_org_assoc as soa , supplier_contact_person as scp, supplier_address as sa where soa._orgid=? and scp._orgid=? and sa._orgid=? and scp.supplier_org_assoc_fkid = soa._id and sa.supplier_org_assoc_fkid=soa._id and soa.status = 1 ", formData.Orgid, formData.Orgid, formData.Orgid).Values(&maps)
	num, err := o.Raw("SELECT soa._id, soa.supplier_unique_code, soa._orgid, soa.company_name, soa.hq_address_line1, soa.city,soa.state,soa.country,soa.pincode,soa.tin,soa.cst, soa.credit_in_days, soa.credit_limit, soa.pan,soa.ecc,soa.gst, soa.notes,soa.provisional_gst,soa.ARN, soa.date_of_introduction, soa.GSTIN, scp.contact_name, scp.designation, scp.email, scp.phone, scp.mobile FROM supplier_org_assoc as soa, supplier_contact_person as scp WHERE soa._orgid=? and soa.status = 1 and scp._orgid=? and scp.supplier_org_assoc_fkid = soa._id", formData.Orgid, formData.Orgid).Values(&maps)
	if err == nil && num > 0 {
		for k, v := range maps {
			if maps[k]["date_of_introduction"] == nil {
				maps[k]["date_of_introduction"] = ""
			}

			var s string
			if maps[k]["city"] != "" && maps[k]["city"] != nil {
				s = maps[k]["city"].(string)
			}
			if maps[k]["state"] != "" && maps[k]["state"] != nil {
				if maps[k]["city"] != "" && maps[k]["city"] != nil {
					s = s + ", "
				}
				s = s + maps[k]["state"].(string)
			}
			// if maps[k]["country"] != "" {
			// 	if maps[k]["state"] != "" {
			// 		s = s + ", "
			// 	}
			// 	s = s + maps[k]["country"].(string)
			// }
			if maps[k]["pincode"] != "" && maps[k]["pincode"] != nil {
				if (maps[k]["state"] != "" && maps[k]["state"] != nil) || (maps[k]["city"] != "" && maps[k]["city"] != nil) {
					s = s + ", "
				}
				s = s + maps[k]["pincode"].(string)
			}
			maps[k]["city_state_pincode"] = s
			key := "supplier_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		queryCount = 0
	}
	return result, queryCount
}

func ManageAssoc(formData Viewassoc) int {
	o := orm.NewOrm()
	ormTransactionalErr := o.Begin()
	flash := beego.NewFlash()
	for i := 0; i < len(formData.Services); i++ {
		//var maps2 []orm.Params
		var maps3 []orm.Params
		//_, err1 := o.Raw("SELECT _id FROM item_org_assoc WHERE item_org_assoc._orgid=? and invoice_narration=?", formData.Orgid, formData.Services[i]).Values(&maps2)
		//fmt.Println(maps2[0]["_id"])
		num, err3 := o.Raw("SELECT _id FROM org_supplier_item_assoc WHERE org_supplier_item_assoc._orgid=? and item_org_assoc_fkid=? and supplier_org_assoc_fkid=? and status=1 ", formData.Orgid, formData.Services[i], formData.Supplier).Values(&maps3)
		if err3 == nil && num > 0 {
			flash.Notice("This association exists")
			ormTransactionalErr = o.Rollback()
			return 0
		} else {
			_, err2 := o.Raw("INSERT INTO org_supplier_item_assoc (_orgid, supplier_org_assoc_fkid, item_org_assoc_fkid, status, last_updated) VALUES (?,?,?,?,?) ", formData.Orgid, formData.Supplier, formData.Services[i], "1", time.Now()).Exec()
			if err2 != nil {
				ormTransactionalErr = o.Rollback()
				return 0
			}
		}
		// if err1 != nil {
		// 	//fmt.Println("Error in for loop")
		// 	break
		// }
	}
	if ormTransactionalErr != nil {
		return 0
	}
	ormTransactionalErr = o.Commit()
	return 1

}

func SearchSupplier(formData Viewassoc) map[string]orm.Params {

	result := make(map[string]orm.Params)

	o := orm.NewOrm()
	var maps []orm.Params
	//num, err := o.Raw("SELECT * FROM item_master ").Values(&maps)
	num, err := o.Raw("SELECT _id,company_name,supplier_unique_code FROM supplier_org_assoc WHERE _orgid=? and status=?", formData.Orgid, "1").Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		var i int64 = 0
		for ; i < num; i++ {
			//fmt.Println(maps[i])
		}
		for k, v := range maps {
			key := "suppliers_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetItemss(formData Viewassoc) map[string]orm.Params {

	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params

	num, err := o.Raw("SELECT distinct(item_code_gen), invoice_narration, org_supplier_item_assoc._id ,org_supplier_item_assoc.invoice_naration FROM org_supplier_item_assoc, item_org_assoc, supplier_org_assoc where org_supplier_item_assoc._orgid=? and supplier_org_assoc._id=? and org_supplier_item_assoc.status=? and org_supplier_item_assoc.supplier_org_assoc_fkid=supplier_org_assoc._id and org_supplier_item_assoc.item_org_assoc_fkid=item_org_assoc._id ", formData.Orgid, formData.Supplier, "1").Values(&maps)

	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		var i int64 = 0
		for ; i < num; i++ {
			//fmt.Println(maps[i])
		}
		for k, v := range maps {

			key := "gi_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetAssocItems(formData Viewassoc) map[string]orm.Params {

	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT invoice_narration, item_unique_code, unit_measure FROM item_org_assoc WHERE _orgid=?", formData.Orgid).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		var i int64 = 0
		for ; i < num; i++ {
			//fmt.Println(maps[i])
		}
		for k, v := range maps {

			key := "gi_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func DeleteSuppliersAssoc(assoc_id string, orgid string) string {
	result := "false"
	o := orm.NewOrm()
	//var values []orm.Params
	_, err := o.Raw("UPDATE org_supplier_item_assoc SET status=?, last_updated=? WHERE _orgid=? and _id=? ", "0", time.Now(), orgid, assoc_id).Exec()
	if err != nil {
		//fmt.Println("Error in UPDATE")
	} else {
		result = "true"
	}
	return result
}

func UniqueCheck3(formData AddSupplier) int {
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT _id FROM supplier_org_assoc WHERE supplier_unique_code=?", formData.Code).Values(&maps)
	//fmt.Println("num:",num)
	if err != nil || num > 0 {
		//fmt.Println("Returns 1")
		return 0
	}
	return 1
}

func GetSupplierCode(formData AddSupplier) string {
	var result string
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT supplier_unique_code FROM supplier_org_assoc WHERE _orgid=? ORDER BY _id DESC LIMIT 1", formData.Orgid).Values(&maps)
	//fmt.Println("****code:",maps[0]["supplier_unique_code"])
	if err == nil {
		if num > 0 {
			s := maps[0]["supplier_unique_code"].(string)
			//fmt.Println("**s:", s)
			//fmt.Println("***length", len(s))
			prefix := s[0 : len(s)-1]
			//fmt.Println("Prefix slice:", prefix)
			last := s[len(s)-1:]
			//fmt.Println("Last slice:", last)
			sufix, _ := strconv.Atoi(last)
			sufix = sufix + 1
			new_sufix := strconv.Itoa(sufix)
			//fmt.Println("Sufix:", new_sufix)
			append_code := prefix + new_sufix
			//fmt.Println("Append_code:", append_code)
			count, _ := o.Raw("SELECT _id FROM supplier_org_assoc WHERE supplier_unique_code=? ", append_code).Values(&maps)
			if count == 0 {
				result = append_code
			} else {
				var safe int = 1
				for safe != 0 {
					prefix := append_code[0 : len(append_code)-1]
					last := append_code[len(append_code)-1:]
					//fmt.Println("Last slice:", last)
					sufix, _ := strconv.Atoi(last)
					sufix = sufix + 1
					new_sufix := strconv.Itoa(sufix)
					//fmt.Println("Sufix:", new_sufix)
					append_code := prefix + new_sufix
					count, _ := o.Raw("SELECT _id FROM supplier_org_assoc WHERE supplier_unique_code=? ", append_code).Values(&maps)
					if count == 0 {
						safe = 0
						result = append_code
					} else {
						safe = 0
					}
				}
			}
		} else {
			prefix := "SINGU"
			s := formData.Orgid
			mid := s[0:3]
			sufix := "S1"
			append_code := prefix + mid + sufix
			count, _ := o.Raw("SELECT _id FROM supplier_org_assoc WHERE supplier_unique_code=? ", append_code).Values(&maps)
			if count == 0 {
				result = append_code
			} else {
				var safe int = 1
				for safe != 0 {
					prefix := append_code[0 : len(append_code)-1]
					last := append_code[len(append_code)-1:]
					//fmt.Println("Last slice:", last)
					sufix, _ := strconv.Atoi(last)
					sufix = sufix + 1
					new_sufix := strconv.Itoa(sufix)
					//fmt.Println("Sufix:", new_sufix)
					append_code := prefix + new_sufix
					count, _ := o.Raw("SELECT _id FROM supplier_org_assoc WHERE supplier_unique_code=? ", append_code).Values(&maps)
					if count == 0 {
						safe = 0
						result = append_code
					} else {
						safe = 0
					}
				}
			}
		}

	} else {
		//fmt.Println("Error in code query")
	}

	return result
}

func CheckForExistingSupplierNameAgainstId(supplierName string, orgID string, supplierid string) int {
	o := orm.NewOrm()
	var resultMap []orm.Params
	num, err := o.Raw("SELECT company_name FROM supplier_org_assoc WHERE company_name=? and  _orgid=? and status = 1 and _id != ?", supplierName, orgID, supplierid).Values(&resultMap)
	//fmt.Println("Query Check Name==========", num)
	if err == nil && num > 0 {
		return 1
	}
	return 0
}

func CheckForExistingSupplierCodeAgainstId(supplierCode string, orgID string, supplierid string) int {
	o := orm.NewOrm()
	var resultMap []orm.Params
	num, err := o.Raw("SELECT supplier_unique_code FROM supplier_org_assoc WHERE supplier_unique_code=? and  _orgid=? and status = 1 and _id != ?", supplierCode, orgID, supplierid).Values(&resultMap)
	//fmt.Println("Query Check Code==========", num)
	if err == nil && num > 0 {
		return 1
	}
	return 0
}

func UpdateSupplier(formData AddSupplier, prevwarehouses []map[string]string, warehouses []map[string]string, contactpersons []map[string]string, prevcontactpersons []map[string]string) int {
	o := orm.NewOrm()
	ormTransactionalErr := o.Begin()
	// var maps []orm.Params
	// _, err01 := o.Raw("SELECT _id FROM organisation_factories WHERE factory_location=? and _orgid=? and status=1", formData.GeoLocation, formData.Orgid).Values(&maps)
	// if err01 == nil{

	// }
	var introDate *string
	introDate = nil
	if formData.Introduction != "" {
		tempIntroDate := formData.Introduction
		introDate = &tempIntroDate
	}
	var credit_in_days *string
	credit_in_days = nil
	if formData.CreditInDays != "" {
		tempCreditDays := formData.CreditInDays
		credit_in_days = &tempCreditDays
	}
	var credit_limit *string
	credit_limit = nil
	if formData.CreditLimit != "" {
		tempCreditLimit := formData.CreditLimit
		credit_limit = &tempCreditLimit
	}

	_, err := o.Raw("UPDATE supplier_org_assoc SET supplier_unique_code=?, category=?, company_name=?, hq_address_line1=?, city=?, state=?, country=?,  pincode=?, tin=?, cst=?, credit_in_days=?, credit_limit=?, pan=?, ecc=?, gst=?, notes=?, last_updated=?, status=?, provisional_gst=?, ARN=?, date_of_introduction=?, GSTIN=? WHERE _orgid=? and _id=?", formData.Code, 1, formData.CompanyName, formData.OfficeAddress, formData.City, formData.State, formData.Country, formData.Pincode, formData.TIN, formData.CST, &credit_in_days, &credit_limit, formData.PAN, formData.ECC, formData.GST, formData.Notes, time.Now(), 1, formData.PGST, formData.ARN, &introDate, formData.MainGSTIN, formData.Orgid, formData.SupplierID).Exec()
	if err != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}

	// _, err1 := o.Raw("UPDATE supplier_contact_person SET contact_name=?, designation=?, email=?, phone=?, mobile=?, last_updated=? WHERE _orgid=? and supplier_org_assoc_fkid=?", formData.ContactPerson, formData.Designation, formData.Email, formData.PhoneNo, formData.CellNo, time.Now(), formData.Orgid, formData.SupplierID).Exec()
	// if err1 != nil {
	// 	ormTransactionalErr = o.Rollback()
	// 	return 0
	// }

	for _, citt := range prevcontactpersons {
		if citt["contact_person"] != "" {
			var designation *string
			designation = nil
			if citt["designation"] != "" {
				tempdesignation := citt["designation"]
				designation = &tempdesignation
			}
			var phoneno *string
			phoneno = nil
			if citt["phone_no"] != "" {
				tempphoneno := citt["phone_no"]
				phoneno = &tempphoneno
			}
			var cellno *string
			cellno = nil
			if citt["cell_no"] != "" {
				tempcellno := citt["cell_no"]
				cellno = &tempcellno
			}
			var email *string
			email = nil
			if citt["email"] != "" {
				tempemail := citt["email"]
				email = &tempemail
			}

			_, err1 := o.Raw("UPDATE supplier_contact_person SET contact_name=?, designation=?, phone=?, mobile=?, email=? where _orgid=? and supplier_org_assoc_fkid=? and _id=? ", citt["contact_person"], &designation, &phoneno, &cellno, &email, formData.Orgid, formData.SupplierID, citt["contact_person_id"]).Exec()
			if err1 != nil {
				ormTransactionalErr = o.Rollback()
				return 0
			}
		}
	}

	_, err2 := o.Raw("UPDATE supplier_address SET address_line1=?, wcity=?, wstate=?, wcountry=?, wpincode=?, GSTIN=?, last_updated=? WHERE _orgid=? and supplier_org_assoc_fkid=? and status=1 and type_address=1", formData.OfficeAddress, formData.City, formData.State, formData.Country, formData.Pincode, formData.MainGSTIN, time.Now(), formData.Orgid, formData.SupplierID).Exec()
	if err2 != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}

	for _, pwit := range prevwarehouses {
		if pwit["warehouse_address"] != "" {

			// type_address=2 for warehouse address
			_, err2 := o.Raw("UPDATE supplier_address SET address_line1=?, wcity=?, wstate=?, wcountry=?, wpincode=?, GSTIN=?, last_updated=? WHERE _orgid=? and supplier_org_assoc_fkid=? and _id=? and status=1 and type_address=2", pwit["warehouse_address"], pwit["wcity"], pwit["wstate"], pwit["wcountry"], pwit["wpincode"], pwit["gstin"], time.Now(), formData.Orgid, formData.SupplierID, pwit["w_id"]).Exec()
			if err2 != nil {
				ormTransactionalErr = o.Rollback()
				return 0
			}
		}
	}

	for _, itt := range contactpersons {
		if itt["contact_person"] != "" {
			var designation *string
			designation = nil
			if itt["designation"] != "" {
				tempdesignation := itt["designation"]
				designation = &tempdesignation
			}
			var phoneno *string
			phoneno = nil
			if itt["phone_no"] != "" {
				tempphoneno := itt["phone_no"]
				phoneno = &tempphoneno
			}
			var cellno *string
			cellno = nil
			if itt["cell_no"] != "" {
				tempcellno := itt["cell_no"]
				cellno = &tempcellno
			}
			var email *string
			email = nil
			if itt["email"] != "" {
				tempemail := itt["email"]
				email = &tempemail
			}
			// var maps01 []orm.Params
			// count, err03 := o.Raw("SELECT _id FROM customer_contact_person WHERE _customer_fkid=? and _orgid=? and name=? and designation=? and phone=? and cell_no=? and email=? ", maps[0]["_id"], formData.Orgid, itt["contact_person"], &designation, &phoneno, &cellno, &email).Values(&maps01)
			// if err03 != nil {
			// 	ormTransactionalErr = o.Rollback()
			// 	return 0
			// }

			// if count < 1 {
			_, err3 := o.Raw("INSERT INTO supplier_contact_person (_orgid, contact_name, designation, phone, mobile, email, contact_type, supplier_org_assoc_fkid) VALUES (?,?,?,?,?,?,?,?) ", formData.Orgid, itt["contact_person"], &designation, &phoneno, &cellno, &email, "2", formData.SupplierID).Exec()
			if err3 != nil {
				ormTransactionalErr = o.Rollback()
				return 0
			}
			// }
		}
	}

	for _, it := range warehouses {
		if it["warehouse_address"] != "" {

			// type_address=2 for warehouse address
			_, err5 := o.Raw("INSERT INTO supplier_address (supplier_org_assoc_fkid, _orgid, address_line1, wcity, wstate, wcountry, wpincode, type_address, status, last_updated, GSTIN) VALUES (?,?,?,?,?,?,?,?,?,?,?)", formData.SupplierID, formData.Orgid, it["warehouse_address"], it["wcity"], it["wstate"], it["wcountry"], it["wpincode"], "2", "1", time.Now(), it["gstin"]).Exec()
			if err5 != nil {
				ormTransactionalErr = o.Rollback()
				return 0
			}
		}
	}

	if ormTransactionalErr != nil {
		return 0
	}
	ormTransactionalErr = o.Commit()
	return 1
}

func UpdateSupplierBySupplierStruct(supplierDetailStruct SupplierDetails, contacts []map[string]string, warehouses []map[string]string, orgId string) int {
	o := orm.NewOrm()

	var introDate *string
	introDate = nil
	if supplierDetailStruct.Introduction != "" {
		tempIntroDate := supplierDetailStruct.Introduction
		introDate = &tempIntroDate
	}
	var credit_in_days *string
	credit_in_days = nil
	if supplierDetailStruct.CreditInDays != "" {
		tempCreditDays := supplierDetailStruct.CreditInDays
		credit_in_days = &tempCreditDays
	}
	var credit_limit *string
	credit_limit = nil
	if supplierDetailStruct.CreditLimit != "" {
		tempCreditLimit := supplierDetailStruct.CreditLimit
		credit_limit = &tempCreditLimit
	}

	_, err := o.Raw("UPDATE supplier_org_assoc SET supplier_unique_code=?, category=?, company_name=?, hq_address_line1=?, city=?, state=?, country=?,  pincode=?, tin=?, cst=?, credit_in_days=?, credit_limit=?, pan=?, ecc=?, gst=?, notes=?, last_updated=?, status=?, provisional_gst=?, ARN=?, date_of_introduction=?, GSTIN=? WHERE _orgid=? and _id=?", supplierDetailStruct.Code, 1, supplierDetailStruct.CompanyName, supplierDetailStruct.OfficeAddress, supplierDetailStruct.City, supplierDetailStruct.State, supplierDetailStruct.Country, supplierDetailStruct.Pincode, supplierDetailStruct.TIN, supplierDetailStruct.CST, &credit_in_days, &credit_limit, supplierDetailStruct.PAN, supplierDetailStruct.ECC, supplierDetailStruct.GST, supplierDetailStruct.Notes, time.Now(), 1, supplierDetailStruct.PGST, supplierDetailStruct.ARN, &introDate, supplierDetailStruct.MainGSTIN, orgId, supplierDetailStruct.SupplierID).Exec()
	if err != nil {
		return 0
	}

	contactInsertError := 0
	warehouseInsertError := 0
	ormTransactionalErrContacts := o.Begin()
	for _, contact := range contacts {
		if contact["contact_person_id"] == "" {
			_, err11 := o.Raw("INSERT into supplier_contact_person (_orgid, supplier_org_assoc_fkid, contact_name, designation, phone, mobile, email ) VALUES (?,?,?,?,?,?,?) ", orgId, supplierDetailStruct.SupplierID, contact["contact_person"], contact["designation"], contact["phone_no"], contact["cell_no"], contact["email"]).Exec()
			if err11 != nil {
				contactInsertError = 1
			}
		} else if contact["contact_person"] != "" {
			var designation *string
			designation = nil
			if contact["designation"] != "" {
				tempdesignation := contact["designation"]
				designation = &tempdesignation
			}
			var phoneno *string
			phoneno = nil
			if contact["phone_no"] != "" {
				tempphoneno := contact["phone_no"]
				phoneno = &tempphoneno
			}
			var cellno *string
			cellno = nil
			if contact["cell_no"] != "" {
				tempcellno := contact["cell_no"]
				cellno = &tempcellno
			}
			var email *string
			email = nil
			if contact["email"] != "" {
				tempemail := contact["email"]
				email = &tempemail
			}

			_, err1 := o.Raw("UPDATE supplier_contact_person SET contact_name=?, designation=?, phone=?, mobile=?, email=? where _orgid=? and supplier_org_assoc_fkid=? and _id=? ", contact["contact_person"], &designation, &phoneno, &cellno, &email, orgId, supplierDetailStruct.SupplierID, contact["contact_person_id"]).Exec()
			if err1 != nil {
				contactInsertError = 1
			}
		}
	}
	if contactInsertError == 1 {
		ormTransactionalErrContacts = o.Rollback()
	} else {
		ormTransactionalErrContacts = o.Commit()
	}

	ormTransactionalErrWarehouse := o.Begin()
	for _, warehouse := range warehouses {

		if warehouse["w_id"] == "" {
			_, err2 := o.Raw("INSERT into supplier_address (_orgid, supplier_org_assoc_fkid, address_line1, wcity, wstate, wcountry, wpincode, type_address, status, last_updated, GSTIN) VALUES (?,?,?,?,?,?,?,?,?,?,?)", orgId, supplierDetailStruct.SupplierID, warehouse["warehouse_address"], warehouse["wcity"], warehouse["wstate"], warehouse["wcountry"], warehouse["wpincode"], "2", "1", time.Now(), warehouse["gstin"]).Exec()
			if err2 != nil {
				warehouseInsertError = 1
			}
		} else if warehouse["w_id"] != "" && warehouse["warehouse_address"] != "" {
			_, err5 := o.Raw("UPDATE supplier_address SET address_line1=?, wcity=?, wstate=?, wcountry=?, wpincode=?, type_address=?, status=?, last_updated=?, GSTIN=? WHERE _orgid=? and _id=? and supplier_org_assoc_fkid=? ", warehouse["warehouse_address"], warehouse["wcity"], warehouse["wstate"], warehouse["wcountry"], warehouse["wpincode"], "2", "1", time.Now(), warehouse["gstin"], orgId, warehouse["w_id"], supplierDetailStruct.SupplierID).Exec()
			if err5 != nil {
				warehouseInsertError = 1
			}
		}
	}
	if warehouseInsertError == 1 {
		ormTransactionalErrWarehouse = o.Rollback()
	} else {
		ormTransactionalErrWarehouse = o.Commit()
	}

	if ormTransactionalErrWarehouse != nil || ormTransactionalErrContacts != nil {
		return 2
	}
	return 1
}

func UpdateAssoc(item_id string, new_qty string, orgid string) string {
	result := "false"
	o := orm.NewOrm()
	//var values []orm.Params
	_, err := o.Raw("UPDATE org_supplier_item_assoc SET invoice_naration=? WHERE _orgid=? and _id=? ", new_qty, orgid, item_id).Exec()
	if err != nil {
		//fmt.Println("Error in UPDATE")
	} else {
		result = "true"
	}
	return result
}

func GetSupplierContactPersonAjax(supplier_id string, orgid string) ([]string, []string, []string, []string, []string, string) {

	status := "false"
	var name []string
	var designation []string
	var email []string
	var phone []string
	var mobile []string
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT contact_name, designation, email, phone, mobile FROM supplier_contact_person WHERE supplier_org_assoc_fkid=? and _orgid=? ORDER BY contact_type ASC", supplier_id, orgid).Values(&maps)
	//fmt.Println("num=", num)
	//var i int64 = 0
	// for ; i < num; i++ {
	// 	fmt.Println(maps[i])
	// }
	//fmt.Println("***Retrieved warehouses= ", num)
	if err == nil {
		var new_name string
		var new_designation string
		var new_email string
		var new_phone string
		var new_mobile string
		var j int64 = 0
		for ; j < num; j++ {
			if maps[j]["contact_name"] != nil && maps[j]["contact_name"] != "" {
				new_name = maps[j]["contact_name"].(string)
				name = append(name, new_name)

				new_designation = maps[j]["designation"].(string)
				designation = append(designation, new_designation)

				if maps[j]["email"] != nil {
					new_email = maps[j]["email"].(string)
				} else {
					new_email = ""
				}
				email = append(email, new_email)

				if maps[j]["phone"] != nil {
					new_phone = maps[j]["phone"].(string)
				} else {
					new_phone = ""
				}
				phone = append(phone, new_phone)

				if maps[j]["mobile"] != nil {
					new_mobile = maps[j]["mobile"].(string)
				} else {
					new_mobile = ""
				}
				mobile = append(mobile, new_mobile)
			}
		}
		status = "true"
	}
	//fmt.Println("***items=", items)
	//fmt.Println("***quantity=", quantity)
	return name, designation, email, phone, mobile, status
}

func GetSupplierWarehouses(supplier_id string, orgid string) ([]string, []string, []string, []string, []string, []string, string) {

	status := "false"
	var address []string
	var city []string
	var state []string
	var country []string
	var pincode []string
	var gstin []string
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT address_line1, wcity, wstate, wcountry, wpincode, GSTIN FROM supplier_address WHERE supplier_org_assoc_fkid=? and _orgid=? and status=1 and type_address=2", supplier_id, orgid).Values(&maps)
	//fmt.Println("num=", num)
	//var i int64 = 0
	// for ; i < num; i++ {
	// 	fmt.Println(maps[i])
	// }
	//fmt.Println("***Retrieved warehouses= ", num)
	if err == nil {
		var new_address string
		var new_city string
		var new_state string
		var new_country string
		var new_pincode string
		var new_gstin string
		var j int64 = 0
		for ; j < num; j++ {
			new_address = maps[j]["address_line1"].(string)
			address = append(address, new_address)

			new_city = maps[j]["wcity"].(string)
			city = append(city, new_city)

			new_state = maps[j]["wstate"].(string)
			state = append(state, new_state)

			new_country = maps[j]["wcountry"].(string)
			country = append(country, new_country)

			new_pincode = maps[j]["wpincode"].(string)
			pincode = append(pincode, new_pincode)

			new_gstin = maps[j]["GSTIN"].(string)
			gstin = append(gstin, new_gstin)

		}
		status = "true"
	}
	//fmt.Println("***items=", items)
	//fmt.Println("***quantity=", quantity)
	return address, city, state, country, pincode, gstin, status
}

func SearchBySupplierDetails(companyName string, contactName string, Location string, _orgId string) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	var num int64
	var err error
	if contactName == "" && Location != "" {
		num, err = o.Raw("SELECT * FROM (SELECT soa._id, soa.supplier_unique_code, soa._orgid, soa.company_name, soa.hq_address_line1, soa.city,soa.state,soa.country,soa.pincode,soa.tin,soa.cst,soa.pan,soa.ecc,soa.gst, soa.notes,soa.provisional_gst,soa.ARN, soa.date_of_introduction, soa.GSTIN, scp.contact_name, scp.designation, scp.email, scp.phone, scp.mobile FROM supplier_org_assoc as soa, supplier_contact_person as scp WHERE soa._orgid=? and soa.status = 1 and scp._orgid=? and scp.supplier_org_assoc_fkid = soa._id) AS sup WHERE sup._orgid =? and (sup.company_name like ? and (city like ? or state like ? or country like ? ) )", _orgId, _orgId, _orgId, "%"+companyName+"%", "%"+Location+"%", "%"+Location+"%", "%"+Location+"%").Values(&maps)
	}
	if contactName != "" && Location == "" {
		num, err = o.Raw("SELECT * FROM (SELECT soa._id, soa.supplier_unique_code, soa._orgid, soa.company_name, soa.hq_address_line1, soa.city,soa.state,soa.country,soa.pincode,soa.tin,soa.cst,soa.pan,soa.ecc,soa.gst, soa.notes,soa.provisional_gst,soa.ARN, soa.date_of_introduction, soa.GSTIN, scp.contact_name, scp.designation, scp.email, scp.phone, scp.mobile FROM supplier_org_assoc as soa, supplier_contact_person as scp WHERE soa._orgid=? and soa.status = 1 and scp._orgid=? and scp.supplier_org_assoc_fkid = soa._id) AS sup WHERE sup._orgid =? and (sup.company_name like ? and contact_name like ? )", _orgId, _orgId, _orgId, "%"+companyName+"%", "%"+contactName+"%").Values(&maps)
	}
	if contactName == "" && Location == "" {
		num, err = o.Raw("SELECT * FROM (SELECT soa._id, soa.supplier_unique_code, soa._orgid, soa.company_name, soa.hq_address_line1, soa.city,soa.state,soa.country,soa.pincode,soa.tin,soa.cst,soa.pan,soa.ecc,soa.gst, soa.notes,soa.provisional_gst,soa.ARN, soa.date_of_introduction, soa.GSTIN, scp.contact_name, scp.designation, scp.email, scp.phone, scp.mobile FROM supplier_org_assoc as soa, supplier_contact_person as scp WHERE soa._orgid=? and soa.status = 1 and scp._orgid=? and scp.supplier_org_assoc_fkid = soa._id) AS sup WHERE sup._orgid =? and (sup.company_name like ? )", _orgId, _orgId, _orgId, "%"+companyName+"%").Values(&maps)
	}
	if contactName != "" && Location != "" {
		num, err = o.Raw("SELECT * FROM (SELECT soa._id, soa.supplier_unique_code, soa._orgid, soa.company_name, soa.hq_address_line1, soa.city,soa.state,soa.country,soa.pincode,soa.tin,soa.cst,soa.pan,soa.ecc,soa.gst, soa.notes,soa.provisional_gst,soa.ARN, soa.date_of_introduction, soa.GSTIN, scp.contact_name, scp.designation, scp.email, scp.phone, scp.mobile FROM supplier_org_assoc as soa, supplier_contact_person as scp WHERE soa._orgid=? and soa.status = 1 and scp._orgid=? and scp.supplier_org_assoc_fkid = soa._id) AS sup WHERE sup._orgid =? and (sup.company_name like ? and contact_name like ? and (city like ? or state like ? or country like ? ) )", _orgId, _orgId, _orgId, "%"+companyName+"%", "%"+contactName+"%", "%"+Location+"%", "%"+Location+"%", "%"+Location+"%").Values(&maps)
	}

	if err == nil && num > 0 {
		for k, v := range maps {
			if maps[k]["date_of_introduction"] != nil && maps[k]["date_of_introduction"] != "" {
				maps[k]["date_of_introduction"] = convertDateString(maps[k]["date_of_introduction"].(string))
			}
			var s string
			if maps[k]["city"] != "" && maps[k]["city"] != nil {
				s = maps[k]["city"].(string)
			}
			if maps[k]["state"] != "" && maps[k]["state"] != nil {
				if maps[k]["city"] != "" && maps[k]["city"] != nil {
					s = s + ", "
				}
				s = s + maps[k]["state"].(string)
			}
			if maps[k]["pincode"] != "" && maps[k]["pincode"] != nil {
				if (maps[k]["state"] != "" && maps[k]["state"] != nil) || (maps[k]["city"] != "" && maps[k]["city"] != nil) {
					s = s + ", "
				}
				s = s + maps[k]["pincode"].(string)
			}
			maps[k]["city_state_pincode"] = s
			key := "supplier_" + strconv.Itoa(k)
			result[key] = v
		}
	}
	return result
}

func SearchBySupplierDetailsByItemName(ItemCode string, _orgId string) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT soa.*, scp.contact_name, scp.designation, scp.email, scp.phone, scp.mobile FROM(SELECT DISTINCT itemSID.*, itemSup.supplier_fkid FROM (SELECT oposo.*, pso.supplier_fkid FROM (SELECT ioa.*, psi.purchase_service_order_fkid  FROM item_org_assoc AS ioa, po_so_items AS psi WHERE  ioa._id = psi.item_org_assoc_fkid and ioa._id=? and ioa._orgid=? ) AS oposo, purchase_service_order AS pso WHERE oposo.purchase_service_order_fkid = pso._id) AS itemSup, supplier_org_assoc itemSID WHERE itemSup.supplier_fkid = itemSID._id) AS soa, supplier_contact_person as scp WHERE soa._orgid=? and soa.status = 1 and scp._orgid=? and scp.supplier_org_assoc_fkid = soa._id ", ItemCode, _orgId, _orgId, _orgId).Values(&maps)

	if err == nil && num > 0 {
		for k, v := range maps {
			if maps[k]["date_of_introduction"] != nil && maps[k]["date_of_introduction"] != "" {
				maps[k]["date_of_introduction"] = convertDateString(maps[k]["date_of_introduction"].(string))
			}
			var s string
			if maps[k]["city"] != "" && maps[k]["city"] != nil {
				s = maps[k]["city"].(string)
			}
			if maps[k]["state"] != "" && maps[k]["state"] != nil {
				if maps[k]["city"] != "" && maps[k]["city"] != nil {
					s = s + ", "
				}
				s = s + maps[k]["state"].(string)
			}
			if maps[k]["pincode"] != "" && maps[k]["pincode"] != nil {
				if (maps[k]["state"] != "" && maps[k]["state"] != nil) || (maps[k]["city"] != "" && maps[k]["city"] != nil) {
					s = s + ", "
				}
				s = s + maps[k]["pincode"].(string)
			}
			maps[k]["city_state_pincode"] = s
			key := "supplier_" + strconv.Itoa(k)
			result[key] = v
		}
	}
	return result
}

func GetSupplierContactPersonsByPkId(pkID string, orgID string) map[string]orm.Params {

	result := make(map[string]orm.Params)

	o := orm.NewOrm()
	var maps []orm.Params
	//num, err := o.Raw("SELECT * FROM item_master ").Values(&maps)
	num, err := o.Raw("SELECT scp._id, scp.contact_name, scp.designation, scp.phone, scp.mobile, scp.email, scp.contact_type FROM supplier_contact_person as scp WHERE scp.supplier_org_assoc_fkid=? and scp._orgid=? ORDER BY contact_type ASC", pkID, orgID).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		//var i int64 = 0
		//for ; i < num; i++ {
		//fmt.Println(maps[i])
		//}
		for k, v := range maps {

			if maps[k]["contact_name"] == nil {
				maps[k]["contact_name"] = ""
			}
			if maps[k]["designation"] == nil {
				maps[k]["designation"] = ""
			}
			if maps[k]["phone"] == nil {
				maps[k]["phone"] = ""
			}
			if maps[k]["mobile"] == nil {
				maps[k]["mobile"] = ""
			}
			if maps[k]["email"] == nil {
				maps[k]["email"] = ""
			}
			key := "customer_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}
func GetSupplierContactPersonsByCompanyName(companyName string, orgID string) map[string]orm.Params {

	result := make(map[string]orm.Params)

	o := orm.NewOrm()

	var maps01 []orm.Params
	_, err01 := o.Raw("SELECT _id FROM supplier_org_assoc WHERE _orgid=? and status=1 and company_name=?", orgID, companyName).Values(&maps01)
	if err01 != nil {
		result["status"] = nil
		return result
	}

	var maps []orm.Params
	//num, err := o.Raw("SELECT * FROM item_master ").Values(&maps)
	num, err := o.Raw("SELECT scp._id, scp.contact_name, scp.designation, scp.phone, scp.mobile, scp.email, scp.contact_type FROM supplier_contact_person as scp WHERE scp.supplier_org_assoc_fkid=? and scp._orgid=? ORDER BY contact_type ASC", maps01[0]["_id"], orgID).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		// var i int64 = 0
		// for ; i < num; i++ {
		// 	fmt.Println(maps[i])
		// }
		for k, v := range maps {

			if maps[k]["contact_name"] == nil {
				maps[k]["contact_name"] = ""
			}
			if maps[k]["designation"] == nil {
				maps[k]["designation"] = ""
			}
			if maps[k]["phone"] == nil {
				maps[k]["phone"] = ""
			}
			if maps[k]["mobile"] == nil {
				maps[k]["mobile"] = ""
			}
			if maps[k]["email"] == nil {
				maps[k]["email"] = ""
			}
			key := "customer_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetSupplierContactPersonsByCompanyID(companyID string, orgID string) map[string]orm.Params {

	result := make(map[string]orm.Params)

	o := orm.NewOrm()

	var maps []orm.Params

	num, err := o.Raw("SELECT scp._id, scp.contact_name, scp.designation, scp.phone, scp.mobile, scp.email, scp.contact_type FROM supplier_contact_person as scp WHERE scp.supplier_org_assoc_fkid=? and scp._orgid=? ORDER BY contact_type ASC", companyID, orgID).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		// var i int64 = 0
		// for ; i < num; i++ {
		// 	fmt.Println(maps[i])
		// }
		for k, v := range maps {

			if maps[k]["contact_name"] == nil {
				maps[k]["contact_name"] = ""
			}
			if maps[k]["designation"] == nil {
				maps[k]["designation"] = ""
			}
			if maps[k]["phone"] == nil {
				maps[k]["phone"] = ""
			}
			if maps[k]["mobile"] == nil {
				maps[k]["mobile"] = ""
			}
			if maps[k]["email"] == nil {
				maps[k]["email"] = ""
			}
			key := "customer_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetMaxSupplierCode(orgid string) int {
	var maxCode int
	maxCode = 1
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT max(supplier_unique_code) as maxcode FROM supplier_org_assoc where _orgid=?", orgid).Values(&maps)

	if num > 0 && err == nil {
		if maps[0]["maxcode"] != nil {
			code := maps[0]["maxcode"].(string)
			getValue, _ := strconv.ParseInt(code, 10, 64)
			maxCode = int(getValue)
		}
	}
	return maxCode
}

func GetSuppplierIDByCompanyName(companyName string, orgId string) int64{
	
		var id int64
		id = 0
		o := orm.NewOrm()
		var maps []orm.Params
		num, err := o.Raw("SELECT _id from supplier_org_assoc where company_name =? and _orgid = ? and status=1", companyName, orgId).Values(&maps)
		if err == nil && num > 0 {
			q := maps[0]["_id"].(string)
			id, _ = strconv.ParseInt(q, 10, 64)
		} else {
			id = 0
		}
		return id	
}