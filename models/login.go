package models

import (
	"github.com/astaxie/beego/orm"
	"golang.org/x/crypto/bcrypt"
)

func Login(data map[string]string) map[string]string {
	result := make(map[string]string)
	result["status"] = "0"

	o := orm.NewOrm()
	//user := Users{ EmailAddress: email, Password: password }

	var maps []orm.Params

	num, err := o.Raw("SELECT org.orgid, org.org_name, ua._orgid , ua.username, ua._id , ua.password, org.status as orgStatus FROM user_account as ua, organisation as org WHERE ua.username = ? and ua.status = 1 and ua._orgid = org.orgid and org.org_name = ? ", data["UserName"], data["Orgname"]).Values(&maps)

	if err == nil && num > 0 {
		// fmt.Println("user found")
		// fmt.Println(data["Password"])
		// fmt.Println(maps[0]["password"])
		passwordByte := []byte(data["Password"])
		hashByte := []byte(maps[0]["password"].(string))
		err1 := bcrypt.CompareHashAndPassword(hashByte, passwordByte)
		if err1 == nil {
			// fmt.Println("Password Match")
			// fmt.Println(err1)
			if maps[0]["orgStatus"].(string) == "99" {
				result["status"] = "99"
			} else {
				result["status"] = "1"
			}
			result["username"] = maps[0]["username"].(string)
			result["_orgid"] = maps[0]["orgid"].(string)
			result["user_id"] = maps[0]["_id"].(string)
			result["Orgname"] = maps[0]["org_name"].(string)
		} else {
			// fmt.Println("Password Mismatch")
			// fmt.Println(err1)
			result["status"] = "0"
		}
	} else if err == orm.ErrNoRows {
		result["status"] = "0"
	}

	return result
}

func GetUserRolesByMap(username string, orgid string) map[string]string {
	result := make(map[string]string)
	o := orm.NewOrm()
	var maps []orm.Params
	_, err := o.Raw("SELECT _id FROM user_account WHERE username=? and _orgid=?", username, orgid).Values(&maps)
	user_id := maps[0]["_id"]
	num, err := o.Raw("SELECT role_master_fkid, role_type FROM user_roles_allocation, roles_master WHERE _orgid=? and role_master_fkid=roles_master_pkid and user_account_fkid=? and status=1", orgid, user_id).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		// var i int64 = 0
		// for ; i < num; i++ {
		// 	fmt.Println(maps[i])
		// }
		for k, _ := range maps {

			key := maps[k]["role_master_fkid"].(string)
			result[key] = maps[k]["role_type"].(string)
		}

	} else {
		//fmt.Println(err)
		//result["status"] = nil
	}
	//fmt.Println(result)
	return result

}
