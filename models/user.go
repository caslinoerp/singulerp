package models

import (
	"strconv"
	"time"

	"github.com/astaxie/beego/orm"
	"golang.org/x/crypto/bcrypt"
)

type User_Account struct {
	OrgUserName string
	Id          int64
	FullName    string
	NewUsername string
	Email       string
	Status      int
	Password    string
	Usertype    int

	Orgid    string
	Userid   string
	Username string

	Usersadd      string
	Usersview     string
	Rolesadd      string
	Rolesview     string
	Itemsadd      string
	Itemsview     string
	Inventoryadd  string
	Inventoryview string
	GRNadd        string
	GRNview       string
	//Usageview 		string
	ConfigModify     string
	ConfigView       string
	CompanyAdd       string
	CompanyView      string
	AssManage        string
	AssView          string
	PoAdd            string
	PoView           string
	CustomersAdd     string
	CustomersView    string
	ProductAss       string
	AssProductView   string
	SalesAdd         string
	SalesView        string
	ProductionCreate string
	ProductionView   string
	ProductsAdd      string
	ProductsView     string
	// BomModify 		string
	// Bomview 		string
	// Jobcardadd 		string
	// Jobcardview 	string
	Processadd  string
	Processview string
	// Ordersadd 		string
	// Ordersview 		string
	StatisticsCreate string
	StatisticsView   string
	// PoStats			string
	// GrnStats 		string
	// ContractStats 	string
	// QaStats 		string
	// OrdersStats 	string
	// ComplaintsStats string
	// HistoryStats 	string
	MasterAdd    string
	MasterView   string
	CreateReport string
	ViewReport   string
	// SupportAdd 		string
	// 	SupportView 	string
	EmployeeAdd  string
	EmployeeView string
	// LeavesAdd 		string
	// LeavesView 		string
	// ContractAdd 	string
	// ContractView 	string
}

// type Manage_roles struct {
// 	OrgUserName 	string

// 	Orgid    		string
// 	Userid   		string
// 	Username 		string

// 	Usersadd 		string
// 	Usersview 		string
// 	Itemsadd 		string
// 	Itemsview 		string
// 	Inventoryadd 	string
// 	Inventoryview 	string
// 	GRNadd 			string
// 	GRNview 		string
// 	//Usageview 		string
// 	ConfigModify  	string
// 	ConfigView 		string
// 	CompanyAdd 		string
// 	CompanyView 	string
// 	AssManage 		string
// 	AssView 		string
// 	PoAdd 			string
//  	PoView 			string
//  	CustomersAdd 	string
// 	CustomersView 	string
// 	ProductAss 		string
// 	AssProductView 	string
// 	SalesAdd 		string
// 	SalesView 		string
// 	ProductionCreate string
//   	ProductionView 	string
// 	ProductsAdd 	string
// 	ProductsView 	string
// 	// BomModify 		string
// 	// Bomview 		string
// 	// Jobcardadd 		string
// 	// Jobcardview 	string
// 	Processadd 		string
// 	Processview 	string
// 	// Ordersadd 		string
// 	// Ordersview 		string
// 	StatisticsCreate string
// 	StatisticsView 	string
// 	// PoStats			string
// 	// GrnStats 		string
// 	// ContractStats 	string
// 	// QaStats 		string
// 	// OrdersStats 	string
// 	// ComplaintsStats string
// 	// HistoryStats 	string
// 	MasterAdd 		string
// 	MasterView 		string
// 	CreateReport 	string
// 	ViewReport 		string
// 	// SupportAdd 		string
//  // 	SupportView 	string
//  	EmployeeAdd 	string
// 	EmployeeView 	string
// 	// LeavesAdd 		string
// 	// LeavesView 		string
// 	// ContractAdd 	string
// 	// ContractView 	string
// }

func init() {
	// orm.RegisterModel(new(Users))
	// orm.RegisterModel(new(UserProfile))
}

func AddUser(formData User_Account) map[string]string {

	o := orm.NewOrm()
	var maps []orm.Params
	result := make(map[string]string)
	if formData.OrgUserName != "" {
		count, _ := o.Raw("SELECT _id FROM user_account WHERE username = ? and _orgid = ? ", formData.OrgUserName, formData.Orgid).Values(&maps)
		if count > 0 {
			assigned_for := maps[0]["_id"].(string)
			var maps1 []orm.Params
			numb, _ := o.Raw("SELECT _id FROM user_account WHERE username = ? and _orgid = ? ", formData.Username, formData.Orgid).Values(&maps1)
			if numb > 0 {
				_, err := o.Raw("Update user_roles_allocation set status=0 WHERE user_account_fkid=? and _orgid=? and status=1", maps[0]["_id"], formData.Orgid).Exec()
				if err == nil {
					assigned_by := maps1[0]["_id"].(string)
					assign_roles := InsertUserRoles(formData, assigned_for, assigned_by)
					if assign_roles == 0 {
						result["status"] = "failed-insert"
					} else {
						result["status"] = "inserted"
					}
				}

			}
		}
	} else {
		count, _ := o.Raw("SELECT * FROM user_account WHERE username = ? and _orgid = ? and status= ?", formData.NewUsername, formData.Orgid, 2).Values(&maps)
		if count > 0 {
			result["status"] = "user-deactivated"
		} else {
			num, _ := o.Raw("SELECT * FROM user_account WHERE username = ? and _orgid = ? ", formData.NewUsername, formData.Orgid).Values(&maps)
			if num > 0 {
				result["status"] = "user-exists"
			} else if num == 0 {
				passwordByte := []byte(formData.Password)
				hashedPassword, err := bcrypt.GenerateFromPassword(passwordByte, bcrypt.DefaultCost)
				//fmt.Println(string(hashedPassword)
				_, err = o.Raw("INSERT INTO user_account (username,password,full_name,_orgid,email,status,usertype) VALUES (?,?,?,?,?,?,?)", formData.NewUsername, string(hashedPassword), formData.FullName, formData.Orgid, formData.Email, 1, 2).Exec()

				if err == nil {
					result["status"] = "inserted"
					var maps2 []orm.Params
					number, _ := o.Raw("SELECT _id FROM user_account WHERE username = ? and _orgid = ? and status= ? and full_name=? and email=? and usertype=?", formData.NewUsername, formData.Orgid, 1, formData.FullName, formData.Email, 2).Values(&maps2)

					var maps3 []orm.Params
					numb, _ := o.Raw("SELECT _id FROM user_account WHERE username = ? and _orgid = ? ", formData.Username, formData.Orgid).Values(&maps3)

					if number > 0 && numb > 0 {
						assigned_for := maps2[0]["_id"].(string)
						assigned_by := maps3[0]["_id"].(string)
						assign_roles := InsertUserRoles(formData, assigned_for, assigned_by)
						if assign_roles == 0 {
							result["status"] = "failed-insert"
						}
					} else {
						result["status"] = "failed-insert"
					}

				} else {
					result["status"] = "failed-insert"
				}
			}
		}
	}

	return result
}

func GetAllUsers(formData User_Account) map[string]orm.Params {

	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT * from user_account where _orgid = ? order by status", formData.Orgid).Values(&maps)
	if err == nil && num > 0 {
		for k, v := range maps {
			key := "users_" + strconv.Itoa(k)
			//value := maps
			//fmt.Println("key: ",key)
			//fmt.Println("values: ", v["name"])
			result[key] = v
		}
	} else {
		result["status"] = nil
	}
	return result
}

func ManageUserBasedOnAction(affectedUser string, action string, UserSessionStruct User_Account) string {
	o := orm.NewOrm()
	_, err := o.Raw("UPDATE user_account set status = ? where _id = ? and _orgid = ?", action, affectedUser, UserSessionStruct.Orgid).Exec()

	if err == nil {
		return "1"
	} else {
		return "0"
	}
}

func CheckUserType(username string, orgid string) int {

	//result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT _id from user_account where _orgid = ? and username=? and usertype=1", orgid, username).Values(&maps)
	if err == nil && num > 0 {
		return 1
	} else {
		return 0
	}
}

func GetUserRoles(formData User_Account) map[string]string {
	result := make(map[string]string)
	// result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	var key string
	//Usersadd
	num, err1 := o.Raw("SELECT ura._id FROM user_account as ua, user_roles_allocation as ura WHERE ua._orgid = ? and ura._orgid=? and ua.username=? and ua._id=ura.user_account_fkid and ura.status=1 and ura.role_master_fkid=1", formData.Orgid, formData.Orgid, formData.OrgUserName).Values(&maps)
	if err1 == nil {
		key = "Usersadd"
		if num > 0 {
			result[key] = "on"
		} else {
			result[key] = "off"
		}
	} else {
		//fmt.Println("ERROR in query")
	}

	//Usersview
	num, err2 := o.Raw("SELECT ura._id FROM user_account as ua, user_roles_allocation as ura WHERE ua._orgid = ? and ura._orgid=? and ua.username=? and ua._id=ura.user_account_fkid and ura.status=1 and ura.role_master_fkid=2", formData.Orgid, formData.Orgid, formData.OrgUserName).Values(&maps)
	if err2 == nil {
		key = "Usersview"
		if num > 0 {
			result[key] = "on"
		} else {
			result[key] = "off"
		}
	} else {
		//fmt.Println("ERROR in query")
	}

	//Rolesadd
	num, err37 := o.Raw("SELECT ura._id FROM user_account as ua, user_roles_allocation as ura WHERE ua._orgid = ? and ura._orgid=? and ua.username=? and ua._id=ura.user_account_fkid and ura.status=1 and ura.role_master_fkid=37", formData.Orgid, formData.Orgid, formData.OrgUserName).Values(&maps)
	if err37 == nil {
		key = "Rolesadd"
		if num > 0 {
			result[key] = "on"
		} else {
			result[key] = "off"
		}
	} else {
		//fmt.Println("ERROR in query")
	}

	//Rolesview
	num, err38 := o.Raw("SELECT ura._id FROM user_account as ua, user_roles_allocation as ura WHERE ua._orgid = ? and ura._orgid=? and ua.username=? and ua._id=ura.user_account_fkid and ura.status=1 and ura.role_master_fkid=38", formData.Orgid, formData.Orgid, formData.OrgUserName).Values(&maps)
	if err38 == nil {
		key = "Rolesview"
		if num > 0 {
			result[key] = "on"
		} else {
			result[key] = "off"
		}
	} else {
		//fmt.Println("ERROR in query")
	}

	//ConfigModify
	num, err3 := o.Raw("SELECT ura._id FROM user_account as ua, user_roles_allocation as ura WHERE ua._orgid = ? and ura._orgid=? and ua.username=? and ua._id=ura.user_account_fkid and ura.status=1 and ura.role_master_fkid=3", formData.Orgid, formData.Orgid, formData.OrgUserName).Values(&maps)
	if err3 == nil {
		key = "ConfigModify"
		if num > 0 {
			result[key] = "on"
		} else {
			result[key] = "off"
		}
	} else {
		//fmt.Println("ERROR in query")
	}

	//ConfigView
	num, err4 := o.Raw("SELECT ura._id FROM user_account as ua, user_roles_allocation as ura WHERE ua._orgid = ? and ura._orgid=? and ua.username=? and ua._id=ura.user_account_fkid and ura.status=1 and ura.role_master_fkid=4", formData.Orgid, formData.Orgid, formData.OrgUserName).Values(&maps)
	if err4 == nil {
		key = "ConfigView"
		if num > 0 {
			result[key] = "on"
		} else {
			result[key] = "off"
		}
	} else {
		//fmt.Println("ERROR in query")
	}

	//Itemsadd
	num, err5 := o.Raw("SELECT ura._id FROM user_account as ua, user_roles_allocation as ura WHERE ua._orgid = ? and ura._orgid=? and ua.username=? and ua._id=ura.user_account_fkid and ura.status=1 and ura.role_master_fkid=5", formData.Orgid, formData.Orgid, formData.OrgUserName).Values(&maps)
	if err5 == nil {
		key = "Itemsadd"
		if num > 0 {
			result[key] = "on"
		} else {
			result[key] = "off"
		}
	} else {
		//fmt.Println("ERROR in query")
	}

	//Itemsview
	num, err6 := o.Raw("SELECT ura._id FROM user_account as ua, user_roles_allocation as ura WHERE ua._orgid = ? and ura._orgid=? and ua.username=? and ua._id=ura.user_account_fkid and ura.status=1 and ura.role_master_fkid=6", formData.Orgid, formData.Orgid, formData.OrgUserName).Values(&maps)
	if err6 == nil {
		key = "Itemsview"
		if num > 0 {
			result[key] = "on"
		} else {
			result[key] = "off"
		}
	} else {
		//fmt.Println("ERROR in query")
	}

	//Inventoryadd
	num, err7 := o.Raw("SELECT ura._id FROM user_account as ua, user_roles_allocation as ura WHERE ua._orgid = ? and ura._orgid=? and ua.username=? and ua._id=ura.user_account_fkid and ura.status=1 and ura.role_master_fkid=7", formData.Orgid, formData.Orgid, formData.OrgUserName).Values(&maps)
	if err7 == nil {
		key = "Inventoryadd"
		if num > 0 {
			result[key] = "on"
		} else {
			result[key] = "off"
		}
	} else {
		//fmt.Println("ERROR in query")
	}

	//Inventoryview
	num, err8 := o.Raw("SELECT ura._id FROM user_account as ua, user_roles_allocation as ura WHERE ua._orgid = ? and ura._orgid=? and ua.username=? and ua._id=ura.user_account_fkid and ura.status=1 and ura.role_master_fkid=8", formData.Orgid, formData.Orgid, formData.OrgUserName).Values(&maps)
	if err8 == nil {
		key = "Inventoryview"
		if num > 0 {
			result[key] = "on"
		} else {
			result[key] = "off"
		}
	} else {
		//fmt.Println("ERROR in query")
	}

	//GRNadd
	num, err9 := o.Raw("SELECT ura._id FROM user_account as ua, user_roles_allocation as ura WHERE ua._orgid = ? and ura._orgid=? and ua.username=? and ua._id=ura.user_account_fkid and ura.status=1 and ura.role_master_fkid=9", formData.Orgid, formData.Orgid, formData.OrgUserName).Values(&maps)
	if err9 == nil {
		key = "GRNadd"
		if num > 0 {
			result[key] = "on"
		} else {
			result[key] = "off"
		}
	} else {
		//fmt.Println("ERROR in query")
	}

	//GRNview
	num, err10 := o.Raw("SELECT ura._id FROM user_account as ua, user_roles_allocation as ura WHERE ua._orgid = ? and ura._orgid=? and ua.username=? and ua._id=ura.user_account_fkid and ura.status=1 and ura.role_master_fkid=10", formData.Orgid, formData.Orgid, formData.OrgUserName).Values(&maps)
	if err10 == nil {
		key = "GRNview"
		if num > 0 {
			result[key] = "on"
		} else {
			result[key] = "off"
		}
	} else {
		//fmt.Println("ERROR in query")
	}

	//CompanyAdd (Supplier)
	num, err11 := o.Raw("SELECT ura._id FROM user_account as ua, user_roles_allocation as ura WHERE ua._orgid = ? and ura._orgid=? and ua.username=? and ua._id=ura.user_account_fkid and ura.status=1 and ura.role_master_fkid=11", formData.Orgid, formData.Orgid, formData.OrgUserName).Values(&maps)
	if err11 == nil {
		key = "CompanyAdd"
		if num > 0 {
			result[key] = "on"
		} else {
			result[key] = "off"
		}
	} else {
		//fmt.Println("ERROR in query")
	}

	//CompanyView (Supplier)
	num, err12 := o.Raw("SELECT ura._id FROM user_account as ua, user_roles_allocation as ura WHERE ua._orgid = ? and ura._orgid=? and ua.username=? and ua._id=ura.user_account_fkid and ura.status=1 and ura.role_master_fkid=12", formData.Orgid, formData.Orgid, formData.OrgUserName).Values(&maps)
	if err12 == nil {
		key = "CompanyView"
		if num > 0 {
			result[key] = "on"
		} else {
			result[key] = "off"
		}
	} else {
		//fmt.Println("ERROR in query")
	}

	//AssManage (Supplier)
	num, err13 := o.Raw("SELECT ura._id FROM user_account as ua, user_roles_allocation as ura WHERE ua._orgid = ? and ura._orgid=? and ua.username=? and ua._id=ura.user_account_fkid and ura.status=1 and ura.role_master_fkid=13", formData.Orgid, formData.Orgid, formData.OrgUserName).Values(&maps)
	if err13 == nil {
		key = "AssManage"
		if num > 0 {
			result[key] = "on"
		} else {
			result[key] = "off"
		}
	} else {
		//fmt.Println("ERROR in query")
	}

	//AssView (Supplier)
	num, err14 := o.Raw("SELECT ura._id FROM user_account as ua, user_roles_allocation as ura WHERE ua._orgid = ? and ura._orgid=? and ua.username=? and ua._id=ura.user_account_fkid and ura.status=1 and ura.role_master_fkid=14", formData.Orgid, formData.Orgid, formData.OrgUserName).Values(&maps)
	if err14 == nil {
		key = "AssView"
		if num > 0 {
			result[key] = "on"
		} else {
			result[key] = "off"
		}
	} else {
		//fmt.Println("ERROR in query")
	}

	//PoAdd
	num, err15 := o.Raw("SELECT ura._id FROM user_account as ua, user_roles_allocation as ura WHERE ua._orgid = ? and ura._orgid=? and ua.username=? and ua._id=ura.user_account_fkid and ura.status=1 and ura.role_master_fkid=15", formData.Orgid, formData.Orgid, formData.OrgUserName).Values(&maps)
	if err15 == nil {
		key = "PoAdd"
		if num > 0 {
			result[key] = "on"
		} else {
			result[key] = "off"
		}
	} else {
		//fmt.Println("ERROR in query")
	}

	//PoView
	num, err16 := o.Raw("SELECT ura._id FROM user_account as ua, user_roles_allocation as ura WHERE ua._orgid = ? and ura._orgid=? and ua.username=? and ua._id=ura.user_account_fkid and ura.status=1 and ura.role_master_fkid=16", formData.Orgid, formData.Orgid, formData.OrgUserName).Values(&maps)
	if err16 == nil {
		key = "PoView"
		if num > 0 {
			result[key] = "on"
		} else {
			result[key] = "off"
		}
	} else {
		//fmt.Println("ERROR in query")
	}

	//CustomersAdd
	num, err17 := o.Raw("SELECT ura._id FROM user_account as ua, user_roles_allocation as ura WHERE ua._orgid = ? and ura._orgid=? and ua.username=? and ua._id=ura.user_account_fkid and ura.status=1 and ura.role_master_fkid=17", formData.Orgid, formData.Orgid, formData.OrgUserName).Values(&maps)
	if err17 == nil {
		key = "CustomersAdd"
		if num > 0 {
			result[key] = "on"
		} else {
			result[key] = "off"
		}
	} else {
		//fmt.Println("ERROR in query")
	}

	//CustomersView
	num, err18 := o.Raw("SELECT ura._id FROM user_account as ua, user_roles_allocation as ura WHERE ua._orgid = ? and ura._orgid=? and ua.username=? and ua._id=ura.user_account_fkid and ura.status=1 and ura.role_master_fkid=18", formData.Orgid, formData.Orgid, formData.OrgUserName).Values(&maps)
	if err18 == nil {
		key = "CustomersView"
		if num > 0 {
			result[key] = "on"
		} else {
			result[key] = "off"
		}
	} else {
		//fmt.Println("ERROR in query")
	}

	//ProductAss Customers
	num, err19 := o.Raw("SELECT ura._id FROM user_account as ua, user_roles_allocation as ura WHERE ua._orgid = ? and ura._orgid=? and ua.username=? and ua._id=ura.user_account_fkid and ura.status=1 and ura.role_master_fkid=19", formData.Orgid, formData.Orgid, formData.OrgUserName).Values(&maps)
	if err19 == nil {
		key = "ProductAss"
		if num > 0 {
			result[key] = "on"
		} else {
			result[key] = "off"
		}
	} else {
		//fmt.Println("ERROR in query")
	}

	//AssProductView Customers
	num, err20 := o.Raw("SELECT ura._id FROM user_account as ua, user_roles_allocation as ura WHERE ua._orgid = ? and ura._orgid=? and ua.username=? and ua._id=ura.user_account_fkid and ura.status=1 and ura.role_master_fkid=20", formData.Orgid, formData.Orgid, formData.OrgUserName).Values(&maps)
	if err20 == nil {
		key = "AssProductView"
		if num > 0 {
			result[key] = "on"
		} else {
			result[key] = "off"
		}
	} else {
		//fmt.Println("ERROR in query")
	}

	//SalesAdd
	num, err21 := o.Raw("SELECT ura._id FROM user_account as ua, user_roles_allocation as ura WHERE ua._orgid = ? and ura._orgid=? and ua.username=? and ua._id=ura.user_account_fkid and ura.status=1 and ura.role_master_fkid=21", formData.Orgid, formData.Orgid, formData.OrgUserName).Values(&maps)
	if err21 == nil {
		key = "SalesAdd"
		if num > 0 {
			result[key] = "on"
		} else {
			result[key] = "off"
		}
	} else {
		//fmt.Println("ERROR in query")
	}

	//SalesView
	num, err22 := o.Raw("SELECT ura._id FROM user_account as ua, user_roles_allocation as ura WHERE ua._orgid = ? and ura._orgid=? and ua.username=? and ua._id=ura.user_account_fkid and ura.status=1 and ura.role_master_fkid=22", formData.Orgid, formData.Orgid, formData.OrgUserName).Values(&maps)
	if err22 == nil {
		key = "SalesView"
		if num > 0 {
			result[key] = "on"
		} else {
			result[key] = "off"
		}
	} else {
		//fmt.Println("ERROR in query")
	}

	//ProductionCreate Orders
	num, err23 := o.Raw("SELECT ura._id FROM user_account as ua, user_roles_allocation as ura WHERE ua._orgid = ? and ura._orgid=? and ua.username=? and ua._id=ura.user_account_fkid and ura.status=1 and ura.role_master_fkid=23", formData.Orgid, formData.Orgid, formData.OrgUserName).Values(&maps)
	if err23 == nil {
		key = "ProductionCreate"
		if num > 0 {
			result[key] = "on"
		} else {
			result[key] = "off"
		}
	} else {
		//fmt.Println("ERROR in query")
	}

	//ProductionView Orders
	num, err24 := o.Raw("SELECT ura._id FROM user_account as ua, user_roles_allocation as ura WHERE ua._orgid = ? and ura._orgid=? and ua.username=? and ua._id=ura.user_account_fkid and ura.status=1 and ura.role_master_fkid=24", formData.Orgid, formData.Orgid, formData.OrgUserName).Values(&maps)
	if err24 == nil {
		key = "ProductionView"
		if num > 0 {
			result[key] = "on"
		} else {
			result[key] = "off"
		}
	} else {
		//fmt.Println("ERROR in query")
	}

	//ProductsAdd
	num, err25 := o.Raw("SELECT ura._id FROM user_account as ua, user_roles_allocation as ura WHERE ua._orgid = ? and ura._orgid=? and ua.username=? and ua._id=ura.user_account_fkid and ura.status=1 and ura.role_master_fkid=25", formData.Orgid, formData.Orgid, formData.OrgUserName).Values(&maps)
	if err25 == nil {
		key = "ProductsAdd"
		if num > 0 {
			result[key] = "on"
		} else {
			result[key] = "off"
		}
	} else {
		//fmt.Println("ERROR in query")
	}

	//ProductsView
	num, err26 := o.Raw("SELECT ura._id FROM user_account as ua, user_roles_allocation as ura WHERE ua._orgid = ? and ura._orgid=? and ua.username=? and ua._id=ura.user_account_fkid and ura.status=1 and ura.role_master_fkid=26", formData.Orgid, formData.Orgid, formData.OrgUserName).Values(&maps)
	if err26 == nil {
		key = "ProductsView"
		if num > 0 {
			result[key] = "on"
		} else {
			result[key] = "off"
		}
	} else {
		//fmt.Println("ERROR in query")
	}

	//Processadd
	num, err27 := o.Raw("SELECT ura._id FROM user_account as ua, user_roles_allocation as ura WHERE ua._orgid = ? and ura._orgid=? and ua.username=? and ua._id=ura.user_account_fkid and ura.status=1 and ura.role_master_fkid=27", formData.Orgid, formData.Orgid, formData.OrgUserName).Values(&maps)
	if err27 == nil {
		key = "Processadd"
		if num > 0 {
			result[key] = "on"
		} else {
			result[key] = "off"
		}
	} else {
		//fmt.Println("ERROR in query")
	}

	//Processview
	num, err28 := o.Raw("SELECT ura._id FROM user_account as ua, user_roles_allocation as ura WHERE ua._orgid = ? and ura._orgid=? and ua.username=? and ua._id=ura.user_account_fkid and ura.status=1 and ura.role_master_fkid=28", formData.Orgid, formData.Orgid, formData.OrgUserName).Values(&maps)
	if err28 == nil {
		key = "Processview"
		if num > 0 {
			result[key] = "on"
		} else {
			result[key] = "off"
		}
	} else {
		//fmt.Println("ERROR in query")
	}

	//StatisticsCreate
	num, err29 := o.Raw("SELECT ura._id FROM user_account as ua, user_roles_allocation as ura WHERE ua._orgid = ? and ura._orgid=? and ua.username=? and ua._id=ura.user_account_fkid and ura.status=1 and ura.role_master_fkid=29", formData.Orgid, formData.Orgid, formData.OrgUserName).Values(&maps)
	if err29 == nil {
		key = "StatisticsCreate"
		if num > 0 {
			result[key] = "on"
		} else {
			result[key] = "off"
		}
	} else {
		//fmt.Println("ERROR in query")
	}

	//StatisticsView
	num, err30 := o.Raw("SELECT ura._id FROM user_account as ua, user_roles_allocation as ura WHERE ua._orgid = ? and ura._orgid=? and ua.username=? and ua._id=ura.user_account_fkid and ura.status=1 and ura.role_master_fkid=30", formData.Orgid, formData.Orgid, formData.OrgUserName).Values(&maps)
	if err30 == nil {
		key = "StatisticsView"
		if num > 0 {
			result[key] = "on"
		} else {
			result[key] = "off"
		}
	} else {
		//fmt.Println("ERROR in query")
	}

	//MasterAdd (Reports)
	num, err31 := o.Raw("SELECT ura._id FROM user_account as ua, user_roles_allocation as ura WHERE ua._orgid = ? and ura._orgid=? and ua.username=? and ua._id=ura.user_account_fkid and ura.status=1 and ura.role_master_fkid=31", formData.Orgid, formData.Orgid, formData.OrgUserName).Values(&maps)
	if err31 == nil {
		key = "MasterAdd"
		if num > 0 {
			result[key] = "on"
		} else {
			result[key] = "off"
		}
	} else {
		//fmt.Println("ERROR in query")
	}

	//MasterView (Reports)
	num, err32 := o.Raw("SELECT ura._id FROM user_account as ua, user_roles_allocation as ura WHERE ua._orgid = ? and ura._orgid=? and ua.username=? and ua._id=ura.user_account_fkid and ura.status=1 and ura.role_master_fkid=32", formData.Orgid, formData.Orgid, formData.OrgUserName).Values(&maps)
	if err32 == nil {
		key = "MasterView"
		if num > 0 {
			result[key] = "on"
		} else {
			result[key] = "off"
		}
	} else {
		//fmt.Println("ERROR in query")
	}

	//CreateReport
	num, err33 := o.Raw("SELECT ura._id FROM user_account as ua, user_roles_allocation as ura WHERE ua._orgid = ? and ura._orgid=? and ua.username=? and ua._id=ura.user_account_fkid and ura.status=1 and ura.role_master_fkid=33", formData.Orgid, formData.Orgid, formData.OrgUserName).Values(&maps)
	if err33 == nil {
		key = "CreateReport"
		if num > 0 {
			result[key] = "on"
		} else {
			result[key] = "off"
		}
	} else {
		//fmt.Println("ERROR in query")
	}

	//ViewReport
	num, err34 := o.Raw("SELECT ura._id FROM user_account as ua, user_roles_allocation as ura WHERE ua._orgid = ? and ura._orgid=? and ua.username=? and ua._id=ura.user_account_fkid and ura.status=1 and ura.role_master_fkid=34", formData.Orgid, formData.Orgid, formData.OrgUserName).Values(&maps)
	if err34 == nil {
		key = "ViewReport"
		if num > 0 {
			result[key] = "on"
		} else {
			result[key] = "off"
		}
	} else {
		//fmt.Println("ERROR in query")
	}

	//EmployeeAdd (HR)
	num, err35 := o.Raw("SELECT ura._id FROM user_account as ua, user_roles_allocation as ura WHERE ua._orgid = ? and ura._orgid=? and ua.username=? and ua._id=ura.user_account_fkid and ura.status=1 and ura.role_master_fkid=35", formData.Orgid, formData.Orgid, formData.OrgUserName).Values(&maps)
	if err35 == nil {
		key = "EmployeeAdd"
		if num > 0 {
			result[key] = "on"
		} else {
			result[key] = "off"
		}
	} else {
		//fmt.Println("ERROR in query")
	}

	//EmployeeView (HR)
	num, err36 := o.Raw("SELECT ura._id FROM user_account as ua, user_roles_allocation as ura WHERE ua._orgid = ? and ura._orgid=? and ua.username=? and ua._id=ura.user_account_fkid and ura.status=1 and ura.role_master_fkid=36", formData.Orgid, formData.Orgid, formData.OrgUserName).Values(&maps)
	if err36 == nil {
		key = "EmployeeView"
		if num > 0 {
			result[key] = "on"
		} else {
			result[key] = "off"
		}
	} else {
		//fmt.Println("ERROR in query")
	}

	return result
}

func InsertUserRoles(formData User_Account, assigned_for string, assigned_by string) int {
	o := orm.NewOrm()
	err := 1

	if formData.Usersadd == "on" {
		_, err1 := o.Raw("INSERT INTO user_roles_allocation (_orgid, user_account_fkid, role_master_fkid, assigned_by_fkid, assigned_on, status) VALUES (?,?,?,?,?,?)", formData.Orgid, assigned_for, 1, assigned_by, time.Now(), 1).Exec()
		if err1 != nil {
			//fmt.Println("Error in roles insert")
			err = 0
		}
	}
	if formData.Usersview == "on" {
		_, err1 := o.Raw("INSERT INTO user_roles_allocation (_orgid, user_account_fkid, role_master_fkid, assigned_by_fkid, assigned_on, status) VALUES (?,?,?,?,?,?)", formData.Orgid, assigned_for, 2, assigned_by, time.Now(), 1).Exec()
		if err1 != nil {
			//fmt.Println("Error in roles insert")
			err = 0
		}
	}
	if formData.Rolesadd == "on" {
		_, err1 := o.Raw("INSERT INTO user_roles_allocation (_orgid, user_account_fkid, role_master_fkid, assigned_by_fkid, assigned_on, status) VALUES (?,?,?,?,?,?)", formData.Orgid, assigned_for, 37, assigned_by, time.Now(), 1).Exec()
		if err1 != nil {
			//fmt.Println("Error in roles insert")
			err = 0
		}
	}
	if formData.Rolesview == "on" {
		_, err1 := o.Raw("INSERT INTO user_roles_allocation (_orgid, user_account_fkid, role_master_fkid, assigned_by_fkid, assigned_on, status) VALUES (?,?,?,?,?,?)", formData.Orgid, assigned_for, 38, assigned_by, time.Now(), 1).Exec()
		if err1 != nil {
			//fmt.Println("Error in roles insert")
			err = 0
		}
	}
	if formData.ConfigModify == "on" {
		_, err1 := o.Raw("INSERT INTO user_roles_allocation (_orgid, user_account_fkid, role_master_fkid, assigned_by_fkid, assigned_on, status) VALUES (?,?,?,?,?,?)", formData.Orgid, assigned_for, 3, assigned_by, time.Now(), 1).Exec()
		if err1 != nil {
			//fmt.Println("Error in roles insert")
			err = 0
		}
	}
	if formData.ConfigView == "on" {
		_, err1 := o.Raw("INSERT INTO user_roles_allocation (_orgid, user_account_fkid, role_master_fkid, assigned_by_fkid, assigned_on, status) VALUES (?,?,?,?,?,?)", formData.Orgid, assigned_for, 4, assigned_by, time.Now(), 1).Exec()
		if err1 != nil {
			//fmt.Println("Error in roles insert")
			err = 0
		}
	}
	if formData.Itemsadd == "on" {
		_, err1 := o.Raw("INSERT INTO user_roles_allocation (_orgid, user_account_fkid, role_master_fkid, assigned_by_fkid, assigned_on, status) VALUES (?,?,?,?,?,?)", formData.Orgid, assigned_for, 5, assigned_by, time.Now(), 1).Exec()
		if err1 != nil {
			//fmt.Println("Error in roles insert")
			err = 0
		}
	}
	if formData.Itemsview == "on" {
		_, err1 := o.Raw("INSERT INTO user_roles_allocation (_orgid, user_account_fkid, role_master_fkid, assigned_by_fkid, assigned_on, status) VALUES (?,?,?,?,?,?)", formData.Orgid, assigned_for, 6, assigned_by, time.Now(), 1).Exec()
		if err1 != nil {
			//fmt.Println("Error in roles insert")
			err = 0
		}
	}
	if formData.Inventoryadd == "on" {
		_, err1 := o.Raw("INSERT INTO user_roles_allocation (_orgid, user_account_fkid, role_master_fkid, assigned_by_fkid, assigned_on, status) VALUES (?,?,?,?,?,?)", formData.Orgid, assigned_for, 7, assigned_by, time.Now(), 1).Exec()
		if err1 != nil {
			//fmt.Println("Error in roles insert")
			err = 0
		}
	}
	if formData.Inventoryview == "on" {
		_, err1 := o.Raw("INSERT INTO user_roles_allocation (_orgid, user_account_fkid, role_master_fkid, assigned_by_fkid, assigned_on, status) VALUES (?,?,?,?,?,?)", formData.Orgid, assigned_for, 8, assigned_by, time.Now(), 1).Exec()
		if err1 != nil {
			//fmt.Println("Error in roles insert")
			err = 0
		}
	}
	if formData.GRNadd == "on" {
		_, err1 := o.Raw("INSERT INTO user_roles_allocation (_orgid, user_account_fkid, role_master_fkid, assigned_by_fkid, assigned_on, status) VALUES (?,?,?,?,?,?)", formData.Orgid, assigned_for, 9, assigned_by, time.Now(), 1).Exec()
		if err1 != nil {
			//fmt.Println("Error in roles insert")
			err = 0
		}
	}
	if formData.GRNview == "on" {
		_, err1 := o.Raw("INSERT INTO user_roles_allocation (_orgid, user_account_fkid, role_master_fkid, assigned_by_fkid, assigned_on, status) VALUES (?,?,?,?,?,?)", formData.Orgid, assigned_for, 10, assigned_by, time.Now(), 1).Exec()
		if err1 != nil {
			//fmt.Println("Error in roles insert")
			err = 0
		}
	}
	// if formData.Usageview == "on" {
	// 	_, err1 := o.Raw("INSERT INTO user_roles_allocation (_orgid, user_account_fkid, role_master_fkid, assigned_by_fkid, assigned_on, status) VALUES (?,?,?,?,?,?)", formData.Orgid, assigned_for, 11, maps3[0]["_id"], time.Now(), 1).Exec()
	// 	if err1 != nil {
	// 		fmt.Println("Error in roles insert")
	// 	}
	// }
	if formData.CompanyAdd == "on" {
		_, err1 := o.Raw("INSERT INTO user_roles_allocation (_orgid, user_account_fkid, role_master_fkid, assigned_by_fkid, assigned_on, status) VALUES (?,?,?,?,?,?)", formData.Orgid, assigned_for, 11, assigned_by, time.Now(), 1).Exec()
		if err1 != nil {
			//fmt.Println("Error in roles insert")
			err = 0
		}
	}
	if formData.CompanyView == "on" {
		_, err1 := o.Raw("INSERT INTO user_roles_allocation (_orgid, user_account_fkid, role_master_fkid, assigned_by_fkid, assigned_on, status) VALUES (?,?,?,?,?,?)", formData.Orgid, assigned_for, 12, assigned_by, time.Now(), 1).Exec()
		if err1 != nil {
			//fmt.Println("Error in roles insert")
			err = 0
		}
	}
	if formData.AssManage == "on" {
		_, err1 := o.Raw("INSERT INTO user_roles_allocation (_orgid, user_account_fkid, role_master_fkid, assigned_by_fkid, assigned_on, status) VALUES (?,?,?,?,?,?)", formData.Orgid, assigned_for, 13, assigned_by, time.Now(), 1).Exec()
		if err1 != nil {
			//fmt.Println("Error in roles insert")
			err = 0
		}
	}
	if formData.AssView == "on" {
		_, err1 := o.Raw("INSERT INTO user_roles_allocation (_orgid, user_account_fkid, role_master_fkid, assigned_by_fkid, assigned_on, status) VALUES (?,?,?,?,?,?)", formData.Orgid, assigned_for, 14, assigned_by, time.Now(), 1).Exec()
		if err1 != nil {
			//fmt.Println("Error in roles insert")
			err = 0
		}
	}
	if formData.PoAdd == "on" {
		_, err1 := o.Raw("INSERT INTO user_roles_allocation (_orgid, user_account_fkid, role_master_fkid, assigned_by_fkid, assigned_on, status) VALUES (?,?,?,?,?,?)", formData.Orgid, assigned_for, 15, assigned_by, time.Now(), 1).Exec()
		if err1 != nil {
			//fmt.Println("Error in roles insert")
			err = 0
		}
	}
	if formData.PoView == "on" {
		_, err1 := o.Raw("INSERT INTO user_roles_allocation (_orgid, user_account_fkid, role_master_fkid, assigned_by_fkid, assigned_on, status) VALUES (?,?,?,?,?,?)", formData.Orgid, assigned_for, 16, assigned_by, time.Now(), 1).Exec()
		if err1 != nil {
			//fmt.Println("Error in roles insert")
			err = 0
		}
	}
	if formData.CustomersAdd == "on" {
		_, err1 := o.Raw("INSERT INTO user_roles_allocation (_orgid, user_account_fkid, role_master_fkid, assigned_by_fkid, assigned_on, status) VALUES (?,?,?,?,?,?)", formData.Orgid, assigned_for, 17, assigned_by, time.Now(), 1).Exec()
		if err1 != nil {
			//fmt.Println("Error in roles insert")
			err = 0
		}
	}
	if formData.CustomersView == "on" {
		_, err1 := o.Raw("INSERT INTO user_roles_allocation (_orgid, user_account_fkid, role_master_fkid, assigned_by_fkid, assigned_on, status) VALUES (?,?,?,?,?,?)", formData.Orgid, assigned_for, 18, assigned_by, time.Now(), 1).Exec()
		if err1 != nil {
			//fmt.Println("Error in roles insert")
			err = 0
		}
	}
	if formData.ProductAss == "on" {
		_, err1 := o.Raw("INSERT INTO user_roles_allocation (_orgid, user_account_fkid, role_master_fkid, assigned_by_fkid, assigned_on, status) VALUES (?,?,?,?,?,?)", formData.Orgid, assigned_for, 19, assigned_by, time.Now(), 1).Exec()
		if err1 != nil {
			//fmt.Println("Error in roles insert")
			err = 0
		}
	}
	if formData.AssProductView == "on" {
		_, err1 := o.Raw("INSERT INTO user_roles_allocation (_orgid, user_account_fkid, role_master_fkid, assigned_by_fkid, assigned_on, status) VALUES (?,?,?,?,?,?)", formData.Orgid, assigned_for, 20, assigned_by, time.Now(), 1).Exec()
		if err1 != nil {
			//fmt.Println("Error in roles insert")
			err = 0
		}
	}
	if formData.SalesAdd == "on" {
		_, err1 := o.Raw("INSERT INTO user_roles_allocation (_orgid, user_account_fkid, role_master_fkid, assigned_by_fkid, assigned_on, status) VALUES (?,?,?,?,?,?)", formData.Orgid, assigned_for, 21, assigned_by, time.Now(), 1).Exec()
		if err1 != nil {
			//fmt.Println("Error in roles insert")
			err = 0
		}
	}
	if formData.SalesView == "on" {
		_, err1 := o.Raw("INSERT INTO user_roles_allocation (_orgid, user_account_fkid, role_master_fkid, assigned_by_fkid, assigned_on, status) VALUES (?,?,?,?,?,?)", formData.Orgid, assigned_for, 22, assigned_by, time.Now(), 1).Exec()
		if err1 != nil {
			//fmt.Println("Error in roles insert")
			err = 0
		}
	}
	if formData.ProductionCreate == "on" {
		_, err1 := o.Raw("INSERT INTO user_roles_allocation (_orgid, user_account_fkid, role_master_fkid, assigned_by_fkid, assigned_on, status) VALUES (?,?,?,?,?,?)", formData.Orgid, assigned_for, 23, assigned_by, time.Now(), 1).Exec()
		if err1 != nil {
			//fmt.Println("Error in roles insert")
			err = 0
		}
	}
	if formData.ProductionView == "on" {
		_, err1 := o.Raw("INSERT INTO user_roles_allocation (_orgid, user_account_fkid, role_master_fkid, assigned_by_fkid, assigned_on, status) VALUES (?,?,?,?,?,?)", formData.Orgid, assigned_for, 24, assigned_by, time.Now(), 1).Exec()
		if err1 != nil {
			//fmt.Println("Error in roles insert")
			err = 0
		}
	}
	if formData.ProductsAdd == "on" {
		_, err1 := o.Raw("INSERT INTO user_roles_allocation (_orgid, user_account_fkid, role_master_fkid, assigned_by_fkid, assigned_on, status) VALUES (?,?,?,?,?,?)", formData.Orgid, assigned_for, 25, assigned_by, time.Now(), 1).Exec()
		if err1 != nil {
			//fmt.Println("Error in roles insert")
			err = 0
		}
	}
	if formData.ProductsView == "on" {
		_, err1 := o.Raw("INSERT INTO user_roles_allocation (_orgid, user_account_fkid, role_master_fkid, assigned_by_fkid, assigned_on, status) VALUES (?,?,?,?,?,?)", formData.Orgid, assigned_for, 26, assigned_by, time.Now(), 1).Exec()
		if err1 != nil {
			//fmt.Println("Error in roles insert")
			err = 0
		}
	}
	// if formData.BomModify == "on" {
	// 	_, err1 := o.Raw("INSERT INTO user_roles_allocation (_orgid, user_account_fkid, role_master_fkid, assigned_by_fkid, assigned_on, status) VALUES (?,?,?,?,?,?)", formData.Orgid, assigned_for, 26, maps3[0]["_id"], time.Now(), 1).Exec()
	// 	if err1 != nil {
	// 		fmt.Println("Error in roles insert")
	// 	}
	// }
	// if formData.Bomview == "on" {
	// 	_, err1 := o.Raw("INSERT INTO user_roles_allocation (_orgid, user_account_fkid, role_master_fkid, assigned_by_fkid, assigned_on, status) VALUES (?,?,?,?,?,?)", formData.Orgid, assigned_for, 27, maps3[0]["_id"], time.Now(), 1).Exec()
	// 	if err1 != nil {
	// 		fmt.Println("Error in roles insert")
	// 	}
	// }
	// if formData.Jobcardadd == "on" {
	// 	_, err1 := o.Raw("INSERT INTO user_roles_allocation (_orgid, user_account_fkid, role_master_fkid, assigned_by_fkid, assigned_on, status) VALUES (?,?,?,?,?,?)", formData.Orgid, assigned_for, 28, maps3[0]["_id"], time.Now(), 1).Exec()
	// 	if err1 != nil {
	// 		fmt.Println("Error in roles insert")
	// 	}
	// }
	// if formData.Jobcardview == "on" {
	// 	_, err1 := o.Raw("INSERT INTO user_roles_allocation (_orgid, user_account_fkid, role_master_fkid, assigned_by_fkid, assigned_on, status) VALUES (?,?,?,?,?,?)", formData.Orgid, assigned_for, 29, maps3[0]["_id"], time.Now(), 1).Exec()
	// 	if err1 != nil {
	// 		fmt.Println("Error in roles insert")
	// 	}
	// }
	if formData.Processadd == "on" {
		_, err1 := o.Raw("INSERT INTO user_roles_allocation (_orgid, user_account_fkid, role_master_fkid, assigned_by_fkid, assigned_on, status) VALUES (?,?,?,?,?,?)", formData.Orgid, assigned_for, 27, assigned_by, time.Now(), 1).Exec()
		if err1 != nil {
			//fmt.Println("Error in roles insert")
			err = 0
		}
	}
	if formData.Processview == "on" {
		_, err1 := o.Raw("INSERT INTO user_roles_allocation (_orgid, user_account_fkid, role_master_fkid, assigned_by_fkid, assigned_on, status) VALUES (?,?,?,?,?,?)", formData.Orgid, assigned_for, 28, assigned_by, time.Now(), 1).Exec()
		if err1 != nil {
			//fmt.Println("Error in roles insert")
			err = 0
		}
	}
	// if formData.Ordersadd == "on" {
	// 	_, err1 := o.Raw("INSERT INTO user_roles_allocation (_orgid, user_account_fkid, role_master_fkid, assigned_by_fkid, assigned_on, status) VALUES (?,?,?,?,?,?)", formData.Orgid, assigned_for, 32, maps3[0]["_id"], time.Now(), 1).Exec()
	// 	if err1 != nil {
	// 		fmt.Println("Error in roles insert")
	// 	}
	// }
	// if formData.Ordersview == "on" {
	// 	_, err1 := o.Raw("INSERT INTO user_roles_allocation (_orgid, user_account_fkid, role_master_fkid, assigned_by_fkid, assigned_on, status) VALUES (?,?,?,?,?,?)", formData.Orgid, assigned_for, 33, maps3[0]["_id"], time.Now(), 1).Exec()
	// 	if err1 != nil {
	// 		fmt.Println("Error in roles insert")
	// 	}
	// }
	if formData.StatisticsCreate == "on" {
		_, err1 := o.Raw("INSERT INTO user_roles_allocation (_orgid, user_account_fkid, role_master_fkid, assigned_by_fkid, assigned_on, status) VALUES (?,?,?,?,?,?)", formData.Orgid, assigned_for, 29, assigned_by, time.Now(), 1).Exec()
		if err1 != nil {
			//fmt.Println("Error in roles insert")
			err = 0
		}
	}
	if formData.StatisticsView == "on" {
		_, err1 := o.Raw("INSERT INTO user_roles_allocation (_orgid, user_account_fkid, role_master_fkid, assigned_by_fkid, assigned_on, status) VALUES (?,?,?,?,?,?)", formData.Orgid, assigned_for, 30, assigned_by, time.Now(), 1).Exec()
		if err1 != nil {
			//fmt.Println("Error in roles insert")
			err = 0
		}
	}
	// if formData.PoStats == "on" {
	// 	_, err1 := o.Raw("INSERT INTO user_roles_allocation (_orgid, user_account_fkid, role_master_fkid, assigned_by_fkid, assigned_on, status) VALUES (?,?,?,?,?,?)", formData.Orgid, assigned_for, 34, maps3[0]["_id"], time.Now(), 1).Exec()
	// 	if err1 != nil {
	// 		fmt.Println("Error in roles insert")
	// 	}
	// }
	// if formData.GrnStats == "on" {
	// 	_, err1 := o.Raw("INSERT INTO user_roles_allocation (_orgid, user_account_fkid, role_master_fkid, assigned_by_fkid, assigned_on, status) VALUES (?,?,?,?,?,?)", formData.Orgid, assigned_for, 35, maps3[0]["_id"], time.Now(), 1).Exec()
	// 	if err1 != nil {
	// 		fmt.Println("Error in roles insert")
	// 	}
	// }
	// if formData.ContractStats == "on" {
	// 	_, err1 := o.Raw("INSERT INTO user_roles_allocation (_orgid, user_account_fkid, role_master_fkid, assigned_by_fkid, assigned_on, status) VALUES (?,?,?,?,?,?)", formData.Orgid, assigned_for, 36, maps3[0]["_id"], time.Now(), 1).Exec()
	// 	if err1 != nil {
	// 		fmt.Println("Error in roles insert")
	// 	}
	// }
	// if formData.QaStats == "on" {
	// 	_, err1 := o.Raw("INSERT INTO user_roles_allocation (_orgid, user_account_fkid, role_master_fkid, assigned_by_fkid, assigned_on, status) VALUES (?,?,?,?,?,?)", formData.Orgid, assigned_for, 37, maps3[0]["_id"], time.Now(), 1).Exec()
	// 	if err1 != nil {
	// 		fmt.Println("Error in roles insert")
	// 	}
	// }
	// if formData.OrdersStats == "on" {
	// 	_, err1 := o.Raw("INSERT INTO user_roles_allocation (_orgid, user_account_fkid, role_master_fkid, assigned_by_fkid, assigned_on, status) VALUES (?,?,?,?,?,?)", formData.Orgid, assigned_for, 38, maps3[0]["_id"], time.Now(), 1).Exec()
	// 	if err1 != nil {
	// 		fmt.Println("Error in roles insert")
	// 	}
	// }
	// if formData.ComplaintsStats == "on" {
	// 	_, err1 := o.Raw("INSERT INTO user_roles_allocation (_orgid, user_account_fkid, role_master_fkid, assigned_by_fkid, assigned_on, status) VALUES (?,?,?,?,?,?)", formData.Orgid, assigned_for, 39, maps3[0]["_id"], time.Now(), 1).Exec()
	// 	if err1 != nil {
	// 		fmt.Println("Error in roles insert")
	// 	}
	// }
	// if formData.HistoryStats == "on" {
	// 	_, err1 := o.Raw("INSERT INTO user_roles_allocation (_orgid, user_account_fkid, role_master_fkid, assigned_by_fkid, assigned_on, status) VALUES (?,?,?,?,?,?)", formData.Orgid, assigned_for, 40, maps3[0]["_id"], time.Now(), 1).Exec()
	// 	if err1 != nil {
	// 		fmt.Println("Error in roles insert")
	// 	}
	// }
	if formData.MasterAdd == "on" {
		_, err1 := o.Raw("INSERT INTO user_roles_allocation (_orgid, user_account_fkid, role_master_fkid, assigned_by_fkid, assigned_on, status) VALUES (?,?,?,?,?,?)", formData.Orgid, assigned_for, 31, assigned_by, time.Now(), 1).Exec()
		if err1 != nil {
			//fmt.Println("Error in roles insert")
			err = 0
		}
	}
	if formData.MasterView == "on" {
		_, err1 := o.Raw("INSERT INTO user_roles_allocation (_orgid, user_account_fkid, role_master_fkid, assigned_by_fkid, assigned_on, status) VALUES (?,?,?,?,?,?)", formData.Orgid, assigned_for, 32, assigned_by, time.Now(), 1).Exec()
		if err1 != nil {
			//fmt.Println("Error in roles insert")
			err = 0
		}
	}
	if formData.CreateReport == "on" {
		_, err1 := o.Raw("INSERT INTO user_roles_allocation (_orgid, user_account_fkid, role_master_fkid, assigned_by_fkid, assigned_on, status) VALUES (?,?,?,?,?,?)", formData.Orgid, assigned_for, 33, assigned_by, time.Now(), 1).Exec()
		if err1 != nil {
			//fmt.Println("Error in roles insert")
			err = 0
		}
	}
	if formData.ViewReport == "on" {
		_, err1 := o.Raw("INSERT INTO user_roles_allocation (_orgid, user_account_fkid, role_master_fkid, assigned_by_fkid, assigned_on, status) VALUES (?,?,?,?,?,?)", formData.Orgid, assigned_for, 34, assigned_by, time.Now(), 1).Exec()
		if err1 != nil {
			//fmt.Println("Error in roles insert")
			err = 0
		}
	}
	// if formData.SupportAdd == "on" {
	// 	_, err1 := o.Raw("INSERT INTO user_roles_allocation (_orgid, user_account_fkid, role_master_fkid, assigned_by_fkid, assigned_on, status) VALUES (?,?,?,?,?,?)", formData.Orgid, maps2[0]["_id"], 44, maps3[0]["_id"], time.Now(), 1).Exec()
	// 	if err1 != nil {
	// 		fmt.Println("Error in roles insert")
	// 	}
	// }
	// if formData.SupportView == "on" {
	// 	_, err1 := o.Raw("INSERT INTO user_roles_allocation (_orgid, user_account_fkid, role_master_fkid, assigned_by_fkid, assigned_on, status) VALUES (?,?,?,?,?,?)", formData.Orgid, maps2[0]["_id"], 45, maps3[0]["_id"], time.Now(), 1).Exec()
	// 	if err1 != nil {
	// 		fmt.Println("Error in roles insert")
	// 	}
	// }
	if formData.EmployeeAdd == "on" {
		_, err1 := o.Raw("INSERT INTO user_roles_allocation (_orgid, user_account_fkid, role_master_fkid, assigned_by_fkid, assigned_on, status) VALUES (?,?,?,?,?,?)", formData.Orgid, assigned_for, 35, assigned_by, time.Now(), 1).Exec()
		if err1 != nil {
			//fmt.Println("Error in roles insert")
			err = 0
		}
	}
	if formData.EmployeeView == "on" {
		_, err1 := o.Raw("INSERT INTO user_roles_allocation (_orgid, user_account_fkid, role_master_fkid, assigned_by_fkid, assigned_on, status) VALUES (?,?,?,?,?,?)", formData.Orgid, assigned_for, 36, assigned_by, time.Now(), 1).Exec()
		if err1 != nil {
			//fmt.Println("Error in roles insert")
			err = 0
		}
	}
	// if formData.LeavesAdd == "on" {
	// 	_, err1 := o.Raw("INSERT INTO user_roles_allocation (_orgid, user_account_fkid, role_master_fkid, assigned_by_fkid, assigned_on, status) VALUES (?,?,?,?,?,?)", formData.Orgid, maps2[0]["_id"], 48, maps3[0]["_id"], time.Now(), 1).Exec()
	// 	if err1 != nil {
	// 		fmt.Println("Error in roles insert")
	// 	}
	// }
	// if formData.LeavesView == "on" {
	// 	_, err1 := o.Raw("INSERT INTO user_roles_allocation (_orgid, user_account_fkid, role_master_fkid, assigned_by_fkid, assigned_on, status) VALUES (?,?,?,?,?,?)", formData.Orgid, maps2[0]["_id"], 49, maps3[0]["_id"], time.Now(), 1).Exec()
	// 	if err1 != nil {
	// 		fmt.Println("Error in roles insert")
	// 	}
	// }
	// if formData.ContractAdd == "on" {
	// 	_, err1 := o.Raw("INSERT INTO user_roles_allocation (_orgid, user_account_fkid, role_master_fkid, assigned_by_fkid, assigned_on, status) VALUES (?,?,?,?,?,?)", formData.Orgid, maps2[0]["_id"], 50, maps3[0]["_id"], time.Now(), 1).Exec()
	// 	if err1 != nil {
	// 		fmt.Println("Error in roles insert")
	// 	}
	// }
	// if formData.ContractView == "on" {
	// 	_, err1 := o.Raw("INSERT INTO user_roles_allocation (_orgid, user_account_fkid, role_master_fkid, assigned_by_fkid, assigned_on, status) VALUES (?,?,?,?,?,?)", formData.Orgid, maps2[0]["_id"], 51, maps3[0]["_id"], time.Now(), 1).Exec()
	// 	if err1 != nil {
	// 		fmt.Println("Error in roles insert")
	// 	}
	// }
	if err != 0 {
		return 1
	} else {
		return 0
	}
}
