package models

import (
	"strconv"
	"time"

	"github.com/astaxie/beego/orm"
)

type TC struct {
	Orgid    string
	Userid   string
	Username string
	Content  string
	Type     string
}

func InsertTc(formData TC) int {
	o := orm.NewOrm()
	ormTransactionalErr := o.Begin()
	var maps []orm.Params
	num, _ := o.Raw("SELECT pkid FROM data_master WHERE type=? and _orgid=? and status=1", formData.Type, formData.Orgid).Values(&maps)
	if num > 0 {
		_, err1 := o.Raw("UPDATE data_master set status=0 WHERE type=? and _orgid=? and status=1", formData.Type, formData.Orgid).Exec()
		if err1 != nil {
			ormTransactionalErr = o.Rollback()
			return 0
		}
	}
	_, err2 := o.Raw("INSERT INTO data_master (_orgid,type,content,date_added,added_by_fkid,status) VALUES (?,?,?,?,?,?)", formData.Orgid, formData.Type, formData.Content, time.Now(), formData.Userid, "1").Exec()
	if err2 != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}
	if ormTransactionalErr != nil {
		return 0
	}
	ormTransactionalErr = o.Commit()
	return 1
}

func GetPoTcContent(formData TC) map[string]orm.Params {

	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT * FROM data_master WHERE _orgid = ? and type=1 and status=1", formData.Orgid).Values(&maps)
	if err == nil && num > 0 {
		for k, v := range maps {
			key := "users_" + strconv.Itoa(k)
			result[key] = v
		}
	} else {
		result["status"] = nil
	}
	return result
}

func GetSoTcContent(formData TC) map[string]orm.Params {

	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT * FROM data_master WHERE _orgid = ? and type=2 and status=1", formData.Orgid).Values(&maps)
	if err == nil && num > 0 {
		for k, v := range maps {
			key := "users_" + strconv.Itoa(k)
			result[key] = v
		}
	} else {
		result["status"] = nil
	}
	return result
}
