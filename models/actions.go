package models

type Action int

const (
	SaveDraft Action = iota
	SendForApprovalWithInventory
 	SendForApprovalNoInventory
	RejectBeforeApproval		
	ApproveWithInventory
	ApproveNoInventory
	SendForCorrection
	SendForProduction
	ProductionComplete
	PrepareForDispatch
	RejectAfterApproval
	Dispatch
	Completed
)

func GetSalesOrderStatusCode(code Action) int {
	switch codeString := code; codeString {
	case SaveDraft:
		return 0	
	case SendForApprovalWithInventory:
		return 11
	case SendForApprovalNoInventory:
		return 12
	case RejectBeforeApproval:		
		return 4
	case ApproveWithInventory:
		return 2
	case ApproveNoInventory:
		return 29
	case SendForCorrection:
		return 3
	case SendForProduction:
		return 5
	case ProductionComplete:
		return 25
	case PrepareForDispatch:
		return 6
	case RejectAfterApproval:
		return 90
	case Dispatch:
		return 7
	case Completed:
		return 8
	default:
		return -1
	}
}