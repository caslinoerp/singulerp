package models

import (
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/astaxie/beego/orm"
)

type StockItems struct {
	Orgid              string
	Userid             string
	Username           string
	ItemName           string
	Group              string
	SubGroup           string
	ItemGrade          string
	UOM                string
	Status             string
	last_updated       string
	ItemCode           string
	InvoiceNarration   string
	MinStockTrigger    string
	MinOrderQty        string
	MaxStorageQty      string
	GSTRate            string
	CessRate           string
	HSNSAC             string
	BinLocation        string
	LeadTimeDays       string
	GeoLocation        string
	GeoLocation1       []string
	GeoLocationEdit    string
	IsProduced         string
	NonConsumable      string
	IsPresentinDB      int
	ItemNameCheck      string
	ItemID             string
	ProductionStatus   string
	ShelfLife          string
	NewGenCode         string
	ItemUniqueSequence string
	OpeningStock       string
}

type StockInventory struct {
	Orgid        string
	Userid       string
	Username     string
	ItemName     string
	ItemCode     string
	OrderDate    string
	DeliveryDate string
	Suppliers    string
	QtyPurchase  string
	TotalPrice   string
	Location     string
	Currency     string
}

type StockViewInventory struct {
	Orgid    string
	Userid   string
	Username string
	ItemName string
	ItemCode string
	Min      string
	Max      string
	Group    string
	Grade    string
	Factory  string
}

type StockAdjustmentRecord struct {
	Orgid           string
	Userid          string
	Username        string
	Location        string
	ItemCode        string
	ItemOrgFkid     string
	DateInspection  string
	TotalQtyInStock string
	QtyObserved     string
	Remark          string
	Reason          string
}

type InventoryUsage struct {
	Orgid    string
	Userid   string
	Username string
	ItemCode string
	ItemName string
	Factory  string
	From     string
	To       string
}

type GRNUsage struct {
	Orgid    string
	Userid   string
	Username string
	ItemCode string
	ItemName string
	ItemID   string
	Factory  string
	FromDate string
	ToDate   string
}

type Packaging struct {
	Orgid       string
	PacketSize  string
	Price       string
	Status      string
	DateAdded   string
	DateRemoved string
}

func GetEditUOM(orgid string) []string {

	var uom []string
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT distinct unit_measure from item_org_assoc where _orgid=?", orgid).Values(&maps)

	if err == nil {
		var new_uom string
		var j int64 = 0
		for ; j < num; j++ {
			new_uom = maps[j]["unit_measure"].(string)
			uom = append(uom, new_uom)

		}
	}

	return uom
}

func GetEditGrades(orgid string) []string {

	var grade []string
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT distinct grade from item_org_assoc where _orgid=?", orgid).Values(&maps)

	if err == nil {
		var new_grade string
		var j int64 = 0
		for ; j < num; j++ {
			new_grade = maps[j]["grade"].(string)
			grade = append(grade, new_grade)

		}
	}

	return grade
}

func GetEditGroups(orgid string) []string {

	var group []string
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT distinct grp from item_org_assoc where _orgid=?", orgid).Values(&maps)

	if err == nil {
		var new_group string
		var j int64 = 0
		for ; j < num; j++ {
			new_group = maps[j]["grp"].(string)
			group = append(group, new_group)

		}
	}

	return group
}

func GetEditSubgroups(orgid string) []string {

	var subgroup []string
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT distinct sub_group from item_org_assoc where _orgid=?", orgid).Values(&maps)

	if err == nil {
		var new_subgroup string
		var j int64 = 0
		for ; j < num; j++ {
			new_subgroup = maps[j]["sub_group"].(string)
			subgroup = append(subgroup, new_subgroup)

		}
	}

	return subgroup
}

func GetItemDetailsByItemId(itemId string, orgID string) StockItems {
	o := orm.NewOrm()
	var maps []orm.Params
	var maps1 []orm.Params

	var itemStruct = StockItems{}
	num, err := o.Raw("SELECT ioa.non_consumable,ioa._id, ioa.item_code_gen, ioa.item_unique_code,ioa.name,ioa.grp,ioa.sub_group,ioa.grade,ioa.unit_measure,ioa.invoice_narration,ioa.min_stock_trigger,ioa.min_order_quantity,ioa.max_storage_quantity,ioa.bin_location,ioa.lead_time,ioa.status,ioa.geo_location,ioa._orgid,ioa.produced_internally,ioa.production_status, ioa.gst_rate, ioa.cess_rate, ioa.hsn_sac, ioa.shelf_life from item_org_assoc as ioa where ioa._orgid =?  and ioa.status = 1  and ioa._id = ? ", orgID, itemId).Values(&maps)
	if err != nil || num == 0 {
		itemStruct.IsPresentinDB = 0
	} else {
		itemStruct.ItemNameCheck = maps[0]["_id"].(string)
		itemStruct.ItemID = maps[0]["_id"].(string)
		itemStruct.ItemName = maps[0]["name"].(string)
		itemStruct.ItemCode = maps[0]["item_code_gen"].(string)
		itemStruct.Group = maps[0]["grp"].(string)
		itemStruct.SubGroup = maps[0]["sub_group"].(string)
		if maps[0]["grade"] != nil {
			itemStruct.ItemGrade = maps[0]["grade"].(string)
		} else {
			itemStruct.ItemGrade = ""
		}
		if maps[0]["bin_location"] != nil {
			itemStruct.BinLocation = maps[0]["bin_location"].(string)
		} else {
			itemStruct.BinLocation = ""
		}
		if maps[0]["production_status"] != nil {
			itemStruct.ProductionStatus = maps[0]["production_status"].(string)
		} else {
			itemStruct.ProductionStatus = ""
		}

		itemStruct.UOM = maps[0]["unit_measure"].(string)
		itemStruct.InvoiceNarration = maps[0]["invoice_narration"].(string)
		itemStruct.MinStockTrigger = maps[0]["min_stock_trigger"].(string)
		itemStruct.MinOrderQty = maps[0]["min_order_quantity"].(string)
		itemStruct.MaxStorageQty = maps[0]["max_storage_quantity"].(string)
		itemStruct.LeadTimeDays = maps[0]["lead_time"].(string)
		if maps[0]["gst_rate"] != nil {
			itemStruct.GSTRate = maps[0]["gst_rate"].(string)
		} else {
			itemStruct.GSTRate = ""
		}
		if maps[0]["cess_rate"] != nil {
			itemStruct.CessRate = maps[0]["cess_rate"].(string)
		} else {
			itemStruct.CessRate = ""
		}
		if maps[0]["hsn_sac"] != nil {
			itemStruct.HSNSAC = maps[0]["hsn_sac"].(string)
		} else {
			itemStruct.HSNSAC = ""
		}
		if maps[0]["shelf_life"] != nil {
			itemStruct.ShelfLife = maps[0]["shelf_life"].(string)
		}

		// var tags1 []string
		// var s string
		// tags1 = strings.Split(maps[0]["geo_location"].(string), ",")
		// for i := 0; i < len(tags1); i++ {
		// 	if tags1[i] == "" {

		// 	} else {
		// 		_, err0 := o.Raw("SELECT factory_location FROM organisation_factories WHERE _id=? and  _orgid=? and status = 1 ", tags1[i], orgID).Values(&maps1)
		// 		if err0 == nil {

		// 			s = s + maps1[0]["factory_location"].(string) + ","

		// 		}
		// 	}
		// }

		//itemStruct.GeoLocationEdit = s
		//itemStruct.IsProduced = maps[0]["raw_material_or_product"].(string)
		maps3 := make([]string, 0)
		var tags1 []string
		if maps[0]["geo_location"] != nil {
			tags1 = strings.Split(maps[0]["geo_location"].(string), ",")
			for i := 0; i < len(tags1); i++ {
				if tags1[i] == "" {

				} else {
					count, err0 := o.Raw("SELECT factory_location FROM organisation_factories WHERE _id=? and  _orgid=?  ", tags1[i], orgID).Values(&maps1)
					if err0 == nil && count > 0 {

						maps3 = append(maps3, maps1[0]["factory_location"].(string))

					}
				}
			}
			itemStruct.GeoLocation1 = maps3
		}
		if maps[0]["produced_internally"] == "1" {
			itemStruct.IsProduced = "true"
		}
		if maps[0]["non_consumable"] == "1" {
			itemStruct.NonConsumable = "true"
		}
		itemStruct.IsPresentinDB = 1
	}
	return itemStruct
}

func GetItemLocationDetailsByItemName(itemName string, orgID string) []string {
	o := orm.NewOrm()
	var maps []orm.Params
	var maps1 []orm.Params
	var itemStruct = StockItems{}
	maps3 := make([]string, 0)
	num, err := o.Raw("SELECT ioa.geo_location from item_org_assoc as ioa where ioa._orgid =?  and ioa.status = 1  and ioa._id = ? ", orgID, itemName).Values(&maps)
	if err != nil || num == 0 {
		itemStruct.IsPresentinDB = 0
	} else {

		var tags1 []string
		if maps[0]["geo_location"] != nil {
			tags1 = strings.Split(maps[0]["geo_location"].(string), ",")
			for i := 0; i < len(tags1); i++ {
				if tags1[i] == "" {

				} else {
					count, err0 := o.Raw("SELECT factory_location FROM organisation_factories WHERE _id=? and  _orgid=?  ", tags1[i], orgID).Values(&maps1)
					if err0 == nil && count > 0 {

						maps3 = append(maps3, maps1[0]["factory_location"].(string))

					}
				}
			}
			itemStruct.IsPresentinDB = 1
		} else {
			itemStruct.IsPresentinDB = 0
		}
	}
	return maps3
}

func GetItemDetailsByItemPKID(itemPKID string, orgID string) []orm.Params {
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT * from item_org_assoc where _orgid =? and status = 1 and _id = ? ", orgID, itemPKID).Values(&maps)
	if err != nil || num == 0 {
		maps[0] = nil
	}
	return maps
}

func CheckForExistingItemName(itemName string, orgID string) int {
	o := orm.NewOrm()
	var resultMap []orm.Params
	num, err := o.Raw("SELECT name FROM item_org_assoc WHERE name=? and  _orgid=? and status = 1", itemName, orgID).Values(&resultMap)
	//fmt.Println("Query Check Name==========", num)
	if err == nil && num > 0 {
		return 1
	}
	return 0
}

func CheckForExistingItemUniqueCode(itemUniqueCode string, orgID string) int {
	o := orm.NewOrm()
	var resultMap []orm.Params
	num, err := o.Raw("SELECT item_unique_code FROM item_org_assoc WHERE item_unique_code=? and  _orgid=? and status = 1", itemUniqueCode, orgID).Values(&resultMap)
	//fmt.Println("Query Check Code==========", num)
	if err == nil && num > 0 {
		return 1
	}
	return 0
}

func CheckForUserDefinedItemCode(itemGenCode string, orgID string) int {
	o := orm.NewOrm()
	var resultMap []orm.Params
	num, err := o.Raw("SELECT item_code_gen FROM item_org_assoc WHERE item_code_gen=? and  _orgid=? and status = 1", itemGenCode, orgID).Values(&resultMap)
	//fmt.Println("Query Check Code==========", num)
	if err == nil && num > 0 {
		return 1
	}
	return 0
}

func CheckForExistingItemName1(itemName string, orgID string, itemid string) int {
	o := orm.NewOrm()
	var resultMap []orm.Params
	num, err := o.Raw("SELECT name FROM item_org_assoc WHERE name=? and  _orgid=? and status = 1 and _id != ?", itemName, orgID, itemid).Values(&resultMap)
	//fmt.Println("Query Check Name==========", num)
	if err == nil && num > 0 {
		return 1
	}
	return 0
}

func CheckForExistingItemUniqueCode1(itemUniqueCode string, orgID string, itemid string) int {
	o := orm.NewOrm()
	var resultMap []orm.Params
	num, err := o.Raw("SELECT item_code_gen FROM item_org_assoc WHERE item_unique_code=? and  _orgid=? and status = 1 and _id != ?", itemUniqueCode, orgID, itemid).Values(&resultMap)
	//fmt.Println("Query Check Code==========", num)
	if err == nil && num > 0 {
		return 1
	}
	return 0
}
func CheckForExistingInvoiceNaration1(itemInvoiceNarration string, orgID string, itemid string) int {
	o := orm.NewOrm()
	var resultMap []orm.Params
	num, err := o.Raw("SELECT invoice_narration FROM item_org_assoc WHERE invoice_narration=? and  _orgid=? and status = 1 and _id != ?", itemInvoiceNarration, orgID, itemid).Values(&resultMap)
	//fmt.Println("Query Check Code==========", num)
	if err == nil && num > 0 {
		return 1
	}
	return 0
}

func CreateNewItemorProduct(formData StockItems) int {
	o := orm.NewOrm()
	ormTransactionalErr := o.Begin()
	//var maps []orm.Params
	//var maps2 []orm.Params
	var s string
	var fac_string *string
	fac_string = nil
	// if formData.InvoiceNarration != "" {
	// 	tempInNarr := formData.InvoiceNarration
	// 	inv_narration = &tempInNarr
	// }
	//fmt.Println("****formData.GeoLocation1: ", formData.GeoLocation1)
	//fmt.Println("****len factory string: ", len(formData.GeoLocation1))
	//var tags1 []string
	if len(formData.GeoLocation1) != 0 {
		//tags1 = strings.Split(formData.GeoLocation1[0], ",")
		if len(formData.GeoLocation1) == 1 {
			s = formData.GeoLocation1[0]
		} else {
			for i := 0; i < len(formData.GeoLocation1); i++ {
				s = s + formData.GeoLocation1[i] + ","
			}
		}
		fac_string = &s
	}
	//fmt.Println("****fac_string", *fac_string)
	//fmt.Println("Length", len(tags1))
	//return 0

	if formData.InvoiceNarration == "" {
		formData.InvoiceNarration = formData.ItemName
	}
	if formData.LeadTimeDays == "" {
		formData.LeadTimeDays = "0"
	}
	if formData.MinOrderQty == "" {
		formData.MinOrderQty = "0"
	}
	if formData.MaxStorageQty == "" {
		formData.MaxStorageQty = "0"
	}
	if formData.IsProduced == "on" {
		formData.IsProduced = "1"
	} else {
		formData.IsProduced = "0"
	}
	if formData.NonConsumable == "on" {
		formData.NonConsumable = "1"
	} else {
		formData.NonConsumable = "0"
	}
	if formData.ShelfLife == "" {
		formData.ShelfLife = "0"
	}

	var gst_rate *string
	gst_rate = nil
	if formData.GSTRate != "" {
		tempgst_rate := formData.GSTRate
		gst_rate = &tempgst_rate
	}
	var cess_rate *string
	cess_rate = nil
	if formData.CessRate != "" {
		tempcess_rate := formData.CessRate
		cess_rate = &tempcess_rate
	}
	var hsn_sac *string
	hsn_sac = nil
	if formData.HSNSAC != "" {
		temphsn_sac := formData.HSNSAC
		hsn_sac = &temphsn_sac
	}

	_, err := o.Raw("INSERT INTO item_org_assoc (name, grp, sub_group, grade, unit_measure, _orgid, item_unique_code, invoice_narration, bin_location, min_order_quantity, lead_time, min_stock_trigger, max_storage_quantity, geo_location, produced_internally,status,production_status,non_consumable, gst_rate, cess_rate, hsn_sac, shelf_life, item_code_gen) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ", formData.ItemName, formData.Group, formData.SubGroup, formData.ItemGrade, formData.UOM, formData.Orgid, formData.ItemCode, formData.InvoiceNarration, formData.BinLocation, formData.MinOrderQty, formData.LeadTimeDays, formData.MinStockTrigger, formData.MaxStorageQty, &fac_string, formData.IsProduced, 1, formData.ProductionStatus, formData.NonConsumable, &gst_rate, &cess_rate, &hsn_sac, formData.ShelfLife, formData.NewGenCode).Exec()
	if err != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}

	if ormTransactionalErr != nil {
		return 0
	}
	ormTransactionalErr = o.Commit()
	return 1
}

func AddStockItemsImport(formData StockItems) int {
	o := orm.NewOrm()
	ormTransactionalErr := o.Begin()
	var maps []orm.Params
	var maps2 []orm.Params
	var s string
	var factory_string *string
	factory_string = nil
	var tags1 []string
	tags1 = strings.Split(formData.GeoLocation, ",")
	for i := 0; i < len(tags1); i++ {
		if tags1[i] == "" {

		} else {
			num, err01 := o.Raw("SELECT _id FROM organisation_factories WHERE factory_location=? and _orgid=? and status=1", tags1[i], formData.Orgid).Values(&maps)
			if err01 == nil && num > 0 {
				if len(tags1) == 1 {
					s = maps[0]["_id"].(string)
				} else {
					s = s + maps[0]["_id"].(string) + ","
				}
				factory_string = &s
			} else {
				_, err1 := o.Raw("INSERT INTO organisation_factories (_orgid, factory_location, factory_type, added_on, status) VALUES (?,?,?,?,?)", formData.Orgid, tags1[i], "2", time.Now(), "1").Exec()
				if err1 != nil {
					ormTransactionalErr = o.Rollback()
					return 0
				}
				count, err2 := o.Raw("SELECT _id FROM organisation_factories WHERE factory_location=? and  _orgid=? and status = 1 ", tags1[i], formData.Orgid).Values(&maps2)
				if err1 == nil && err2 == nil && count > 0 {
					if len(tags1) == 1 {
						s = maps2[0]["_id"].(string)
					} else {
						s = s + maps2[0]["_id"].(string) + ","
					}

				}
				factory_string = &s
			}
		}
	}

	var gst_rate *string
	gst_rate = nil
	if formData.GSTRate != "" {
		tempgst := formData.GSTRate
		gst_rate = &tempgst
	}

	var cess_rate *string
	cess_rate = nil
	if formData.CessRate != "" {
		tempcess := formData.CessRate
		cess_rate = &tempcess
	}

	_, err := o.Raw("INSERT INTO item_org_assoc (name, grp, sub_group, grade, unit_measure, _orgid, item_code_gen, item_unique_code, invoice_narration, bin_location, min_order_quantity, lead_time, min_stock_trigger, max_storage_quantity, geo_location, produced_internally, production_status, non_consumable, gst_rate, cess_rate, hsn_sac, status) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ", formData.ItemName, formData.Group, formData.SubGroup, formData.ItemGrade, formData.UOM, formData.Orgid, formData.ItemCode, formData.ItemUniqueSequence, formData.InvoiceNarration, formData.BinLocation, formData.MinOrderQty, formData.LeadTimeDays, formData.MinStockTrigger, formData.MaxStorageQty, &factory_string, formData.IsProduced, formData.ProductionStatus, formData.NonConsumable, &gst_rate, &cess_rate, formData.HSNSAC, 1).Exec()
	if err != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}

	if ormTransactionalErr != nil {
		return 0
	}
	ormTransactionalErr = o.Commit()
	return 1
}

//UPDATE INVENTORY FOR IMPORTED ITEMS
func UpdateInventoryOnImport(formData StockItems) int {

	o := orm.NewOrm()
	ormTransactionalErr := o.Begin()
	var maps []orm.Params
	var stock []string
	stock = strings.Split(formData.OpeningStock, ",")
	var locations []string
	locations = strings.Split(formData.GeoLocation, ",")

	// Update stock for all geo locations if opening stock is not empty

	for i := 0; i < len(locations); i++ {
		if locations[i] != "" {
			for j := i; j < len(stock); j++ {
				insertflag := 0
				if stock[j] != "" && stock[j] != "0" {
					//Get the id for factory location
					num, err01 := o.Raw("SELECT _id FROM organisation_factories WHERE factory_location=? and _orgid=? and status=1", locations[j], formData.Orgid).Values(&maps)
					if err01 == nil && num > 0 {
						s := maps[0]["_id"].(string)

						addinv := StockInventory{
							Orgid:        formData.Orgid,
							ItemName:     formData.ItemName,
							ItemCode:     formData.ItemCode,
							OrderDate:    time.Now().Local().Format("2006-01-02 15:04:05"),
							DeliveryDate: time.Now().Local().Format("2006-01-02 15:04:05"),
							QtyPurchase:  stock[j],
							Location:     s,
						}

						insertinv := AddInventory(addinv)
						if insertinv == 0 {
							ormTransactionalErr = o.Rollback()
							return 0
						}

					}
					insertflag = 1
				}
				if insertflag == 1 {
					break
				}

			}
		}
	}

	if ormTransactionalErr != nil {
		return 0
	}
	ormTransactionalErr = o.Commit()
	return 1
}

func UpdateStockItems(formData StockItems) int {
	o := orm.NewOrm()
	//var maps []orm.Params

	var fac_string *string
	fac_string = nil

	if len(formData.GeoLocation1) != 0 {
		var s string
		if len(formData.GeoLocation1) == 1 {
			s = formData.GeoLocation1[0]
		} else {
			for i := 0; i < len(formData.GeoLocation1); i++ {
				s = s + formData.GeoLocation1[i] + ","
			}
		}
		// for i := 0; i < len(formData.GeoLocation1); i++ {
		// 	//num, err0 := o.Raw("SELECT _id FROM organisation_factories WHERE factory_location=? and  _orgid=? and status = 1 ", formData.GeoLocation1[i], formData.Orgid).Values(&maps)
		// 	//if err0 == nil && num > 0 {
		// 		//fmt.Println("ID", maps[0]["_id"])
		// 		if len(formData.GeoLocation1) == 1 {
		// 			s = maps[0]["_id"].(string)
		// 		} else {
		// 			s = s + maps[0]["_id"].(string) + ","
		// 		}
		// 	//}
		// }
		fac_string = &s
	}

	if formData.InvoiceNarration == "" {
		formData.InvoiceNarration = formData.ItemName
	}
	if formData.LeadTimeDays == "" {
		formData.LeadTimeDays = "0"
	}
	if formData.MinOrderQty == "" {
		formData.MinOrderQty = "0"
	}
	if formData.MaxStorageQty == "" {
		formData.MaxStorageQty = "0"
	}
	if formData.ProductionStatus == "" {
		formData.ProductionStatus = "0"
	}
	if formData.ShelfLife == "" {
		formData.ShelfLife = "0"
	}

	// var production_status *string
	// production_status = nil
	// if formData.IsProduced == "on" {
	// 	formData.IsProduced = "1"
	// 		temp_production_status := formData.ProductionStatus
	// 		production_status = &temp_production_status
	// } else {
	// 	formData.IsProduced = "0"
	// }

	if formData.NonConsumable == "on" {
		formData.NonConsumable = "1"
	} else {
		formData.NonConsumable = "0"
	}
	var gst_rate *string
	gst_rate = nil
	if formData.GSTRate != "" {
		tempgst_rate := formData.GSTRate
		gst_rate = &tempgst_rate
	}
	var cess_rate *string
	cess_rate = nil
	if formData.CessRate != "" {
		tempcess_rate := formData.CessRate
		cess_rate = &tempcess_rate
	}
	var hsn_sac *string
	hsn_sac = nil
	if formData.HSNSAC != "" {
		temphsn_sac := formData.HSNSAC
		hsn_sac = &temphsn_sac
	}
	_, err := o.Raw("UPDATE item_org_assoc SET name=?, grp=?, sub_group=?, grade=?, unit_measure=?, item_code_gen=?, invoice_narration=?, bin_location=?, min_order_quantity=?, lead_time=?, min_stock_trigger=?, max_storage_quantity=?, geo_location=?,status=?,production_status=?,non_consumable=?, gst_rate=?, cess_rate=?, hsn_sac=?,shelf_life=? WHERE _orgid=? and _id=?", formData.ItemName, formData.Group, formData.SubGroup, formData.ItemGrade, formData.UOM, formData.ItemCode, formData.InvoiceNarration, formData.BinLocation, formData.MinOrderQty, formData.LeadTimeDays, formData.MinStockTrigger, formData.MaxStorageQty, &fac_string, 1, formData.ProductionStatus, formData.NonConsumable, &gst_rate, &cess_rate, &hsn_sac, formData.ShelfLife, formData.Orgid, formData.ItemID).Exec()
	// _, err := o.Raw("UPDATE item_org_assoc SET name=?, grp=?, sub_group=?, grade=?, unit_measure=?, item_unique_code=?, invoice_narration=?, bin_location=?, min_order_quantity=?, lead_time=?, min_stock_trigger=?, max_storage_quantity=?, geo_location=?,status=?,production_status=?,non_consumable=?, gst_rate=?, cess_rate=?, hsn_sac=?,shelf_life=? WHERE _orgid=? and _id=?", formData.ItemName, formData.Group, formData.SubGroup, formData.ItemGrade, formData.UOM, formData.ItemCode, formData.InvoiceNarration, formData.BinLocation, formData.MinOrderQty, formData.LeadTimeDays, formData.MinStockTrigger, formData.MaxStorageQty, &fac_string, 1, &production_status, formData.NonConsumable, &gst_rate, &cess_rate, &hsn_sac,formData.ShelfLife, formData.Orgid, formData.ItemID).Exec()
	if err == nil {
		return 1
	}
	return 0
}

func DeleteStockItems(item_id string, orgid string) string {
	result := "false"
	o := orm.NewOrm()

	ormTransactionalErr := o.Begin()
	//var values []orm.Params

	_, err := o.Raw("UPDATE item_org_assoc SET status=?, last_updated=? WHERE _orgid=? and _id=? ORDER BY last_updated DESC LIMIT 1", "0", time.Now(), orgid, item_id).Exec()
	if err != nil {
		ormTransactionalErr = o.Rollback()
		result = "false"
	} else {
		result = "true"
	}

	_, err2 := o.Raw("UPDATE item_total_inventory SET status=?, last_updated=? WHERE _orgid=? and item_org_assoc_fkid=? ORDER BY last_updated DESC LIMIT 1", "111", time.Now(), orgid, item_id).Exec()
	if err2 != nil {
		ormTransactionalErr = o.Rollback()
		result = "false"
	} else {
		result = "true"
	}

	_, err3 := o.Raw("UPDATE item_stock SET latest=?, last_updated=? WHERE _orgid=? and item_org_assoc_fkid=? and latest=1 ORDER BY last_updated DESC LIMIT 1", "111", time.Now(), orgid, item_id).Exec()
	if err3 != nil {
		ormTransactionalErr = o.Rollback()
		result = "false"
	} else {
		result = "true"
	}

	ormTransactionalErr = o.Commit()
	if ormTransactionalErr != nil {
		return "false"
	}
	return result
}

func AddInventory(formData StockInventory) int {
	result := make(map[string]string)
	result["status"] = "0"
	o := orm.NewOrm()
	ormTransactionalErr := o.Begin()
	var maps []orm.Params

	var mapgrn []orm.Params
	var maplastUpdatedId []orm.Params
	_, err01 := o.Raw("SELECT _id FROM item_org_assoc WHERE  _id=? and _orgid=?", formData.ItemName, formData.Orgid).Values(&maps)
	item_id := maps[0]["_id"].(string)
	itemID, _ := strconv.ParseInt(item_id, 10, 64)

	//IF NO SUPPLIER IS SELECTED
	if formData.Suppliers != "" {
		_, err1 := o.Raw("INSERT INTO item_rates (_orgid, item_org_assoc_fkid, supplier_org_assoc_fkid, rate, currency,latest) VALUES (?,?,?,?,?,?) ", formData.Orgid, itemID, formData.Suppliers, formData.TotalPrice, formData.Currency, "1").Exec()
		if err1 != nil {
			ormTransactionalErr = o.Rollback()
			return 0
		}
	} else {
		if formData.TotalPrice != "" {
			_, err1 := o.Raw("INSERT INTO item_rates (_orgid, item_org_assoc_fkid, rate, currency,latest) VALUES (?,?,?,?,?) ", formData.Orgid, itemID, formData.TotalPrice, formData.Currency, "1").Exec()
			if err1 != nil {
				ormTransactionalErr = o.Rollback()
				return 0
			}
		}
	}

	_, errSel := o.Raw("SELECT max(grn_no) as maxgrn, max(batch_no_seq) as maxbatchNo from po_so_items_with_grn where _orgid=?", formData.Orgid).Values(&mapgrn)
	if mapgrn[0]["maxgrn"] == nil {
		fmt.Println(mapgrn[0]["maxgrn"])
		mapgrn[0]["maxgrn"] = "0"

	}

	grnStr := mapgrn[0]["maxgrn"].(string)
	grnval, errconv := strconv.ParseInt(grnStr, 10, 64)

	if mapgrn[0]["maxbatchNo"] == nil {
		fmt.Println(mapgrn[0]["maxbatchNo"])
		mapgrn[0]["maxbatchNo"] = "0"

	}

	batchStr := mapgrn[0]["maxbatchNo"].(string)
	batchNoVal, errconv := strconv.ParseInt(batchStr, 10, 64)

	//INCREMENT AND ASSIGN GRN
	grnval = grnval + 1
	grnCodeGenVal := "ADD" + strconv.Itoa(int(grnval))

	//INCREMENT AND ASSIGN BATCH NO
	batchNoVal = batchNoVal + 1
	batchNoGenVal := "ADD" + strconv.Itoa(int(batchNoVal))

	if errSel != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}

	totalpr, errPrice := strconv.ParseFloat(formData.TotalPrice, 64)
	totalquantity, errQty := strconv.ParseFloat(formData.QtyPurchase, 64)
	amount := totalpr * totalquantity

	if errconv == nil || errPrice == nil || errQty == nil {

	}

	//INSERT INTO P0_SO_ITEMS WITH GRN CONSIDERING IT TO BE A DUMMY GRN
	_, errGRN := o.Raw("INSERT INTO po_so_items_with_grn (_orgid, item_org_assoc_fkid, grn_no, grn_status, delivered_date,rate,quantity,total_cost,received_sent_quantity,status, grn_no_gen, batch_no, batch_no_seq, geo_location) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?) ", formData.Orgid, itemID, grnval, "1", formData.DeliveryDate, totalpr, totalquantity, amount, totalquantity, "1", grnCodeGenVal, batchNoGenVal, batchNoVal, formData.Location).Exec()
	if errGRN != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}

	//INSERT INTO ITEM STOCK ADJUSTMENT TABLE
	var stockAdjectedId string
	_, err05 := o.Raw("INSERT INTO item_stock_adjustment (_orgid, updated_by, observed_quantity, geo_location, item_org_assoc_fkid) VALUES (?,?,?,?,?) ", formData.Orgid, formData.Username, formData.QtyPurchase, formData.Location, itemID).Exec()
	if err05 != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	} else {

		_, err := o.Raw("SELECT _id FROM item_stock_adjustment order by last_updated DESC limit 1").Values(&maplastUpdatedId)
		if err == nil {
			stockAdjectedId = maplastUpdatedId[0]["_id"].(string)
		}
	}

	//INSERT INTO ITEM STOCK TABLE
	rate, _ := strconv.ParseFloat(formData.TotalPrice, 64)
	insertTableFkid, _ := strconv.ParseInt(stockAdjectedId, 10, 64)

	itemStockData := ItemStockTable{
		OrgId:           formData.Orgid,
		Itemid:          itemID,
		Currency:        formData.Currency,
		Location:        formData.Location,
		StartDate:       time.Now(),
		EndDate:         time.Now(),
		ExpiresBy:       time.Now(), // Temp setting as no provision available on ui
		InsertTableFkid: insertTableFkid,
		InsertStatus:    3,
		BatchNo:         batchNoGenVal,
		GRNNumber:       grnCodeGenVal,
	}

	if formData.TotalPrice != "" {
		itemStockData.Rate = rate
	}
	if formData.QtyPurchase != "" {
		itemStockData.QuantityModified = totalquantity
	}

	insertQueryStatus := InsertUpdateItemStock(itemStockData)
	if insertQueryStatus == 0 {
		ormTransactionalErr = o.Rollback()
		return 0
	} else {
		//UPDATE THE ITEM TOTAL INVENTORY
		itemInventoryData := ItemTotalInventory{
			OrgId:         formData.Orgid,
			Itemid:        itemID,
			Location:      formData.Location,
			AddRemoveFlag: 1,
		}
		if formData.QtyPurchase != "" {
			itemInventoryData.QuantityModified = totalquantity
		}

		insertInvStatus := InsertUpdateItemTotalInventory(itemInventoryData)
		if insertInvStatus == 0 {
			ormTransactionalErr = o.Rollback()
			return 0
		}
	}

	if err01 != nil || ormTransactionalErr != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}
	ormTransactionalErr = o.Commit()
	return 1
}

func GetInventoryQty(item_id string, factory_id string, orgId string) string {
	//var item_code string
	var existing_qty string

	o := orm.NewOrm()
	//var maps []orm.Params
	//_, err := o.Raw("SELECT _id, item_code_gen, item_unique_code FROM item_org_assoc WHERE _id=? and _orgid=?", item_narration, orgId).Values(&maps)
	//item_id := maps[0]["_id"]
	var maps1 []orm.Params
	_, err1 := o.Raw("SELECT SUM(quantity) AS Total FROM item_stock WHERE _orgid=? and item_org_assoc_fkid=? and item_stock.latest=1 and item_stock.geo_location=?", orgId, item_id, factory_id).Values(&maps1)

	if err1 == nil {
		//item_code = maps[0]["item_unique_code"].(string)
		//item_code = maps[0]["item_code_gen"].(string)
		if maps1[0]["Total"] != nil {
			existing_qty = maps1[0]["Total"].(string)
		} else {
			existing_qty = "0"
		}
	}

	return existing_qty
}

func SearchInventoryDetails(formData StockViewInventory) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	if formData.Min != "" && formData.Max != "" && formData.Factory != "" {
		//_, err := o.Raw("SELECT _id FROM organisation_factories WHERE factory_location=? and _orgid=?", formData.Factory, formData.Orgid).Values(&maps)
		//factory_id := maps[0]["_id"]
		num, err := o.Raw("SELECT iti.total_quantity, ioa._id, ioa.grp, ioa.invoice_narration, ioa.item_unique_code, ioa.grade, orgf.factory_location FROM item_org_assoc as ioa, item_total_inventory as iti, organisation_factories as orgf WHERE iti._orgid=? and (ioa.invoice_narration=? or ioa.item_unique_code=? or ioa.grp=? or ioa.grade=?) and iti.total_quantity>=? and iti.total_quantity<=? and iti.geo_location=? and iti.item_org_assoc_fkid=ioa._id and iti.status=1 and orgf._id=? and orgf._orgid=?", formData.Orgid, formData.ItemName, formData.ItemCode, formData.Group, formData.Grade, formData.Min, formData.Max, formData.Factory, formData.Factory, formData.Orgid).Values(&maps)
		if err == nil && num > 0 {
			//fmt.Println(maps[0]["name"])
			//var i int64 = 0
			//for ; i < num; i++ {
			//fmt.Println(maps[i])
			//}
			for k, v := range maps {

				key := "items_" + strconv.Itoa(k)
				result[key] = v
			}

		} else {
			//fmt.Println(err)
			result["status"] = nil
		}

	} else if formData.Min != "" && formData.Max == "" && formData.ItemName == "" && formData.ItemCode == "" && formData.Group == "" && formData.Grade == "" && formData.Factory == "" {
		num, err := o.Raw("SELECT iti.total_quantity, ioa._id, ioa.grp, ioa.invoice_narration, ioa.item_unique_code, ioa.grade, orgf.factory_location FROM item_org_assoc as ioa, item_total_inventory as iti, organisation_factories as orgf WHERE iti._orgid=? and iti.total_quantity>=? and iti.item_org_assoc_fkid=ioa._id and iti.status=1 and orgf._id=iti.geo_location and orgf._orgid=?", formData.Orgid, formData.Min, formData.Orgid).Values(&maps)
		if err == nil && num > 0 {
			//fmt.Println(maps[0]["name"])
			//var i int64 = 0
			//for ; i < num; i++ {
			//fmt.Println(maps[i])
			//}
			for k, v := range maps {

				key := "items_" + strconv.Itoa(k)
				result[key] = v
			}

		} else {
			//fmt.Println(err)
			result["status"] = nil
		}

	} else if formData.Min == "" && formData.Max != "" && formData.ItemName == "" && formData.ItemCode == "" && formData.Group == "" && formData.Grade == "" && formData.Factory == "" {
		num, err := o.Raw("SELECT iti.total_quantity, ioa._id, ioa.grp, ioa.invoice_narration, ioa.item_unique_code, ioa.grade, orgf.factory_location FROM item_org_assoc as ioa, item_total_inventory as iti, organisation_factories as orgf WHERE iti._orgid=? and iti.total_quantity<=?  and iti.item_org_assoc_fkid=ioa._id and iti.status=1 and orgf._id=iti.geo_location and orgf._orgid=?", formData.Orgid, formData.Max, formData.Orgid).Values(&maps)
		if err == nil && num > 0 {
			//fmt.Println(maps[0]["name"])
			//var i int64 = 0
			//for ; i < num; i++ {
			//fmt.Println(maps[i])
			//}
			for k, v := range maps {

				key := "items_" + strconv.Itoa(k)
				result[key] = v
			}

		} else {
			// fmt.Println(err)
			result["status"] = nil
		}

	} else if formData.Min == "" && formData.Max == "" && formData.ItemName == "" && formData.ItemCode == "" && formData.Group == "" && formData.Grade == "" && formData.Factory != "" {
		//_, err := o.Raw("SELECT _id FROM organisation_factories WHERE factory_location=? and _orgid=?", formData.Factory, formData.Orgid).Values(&maps)
		//factory_id := maps[0]["_id"]
		num, err := o.Raw("SELECT iti.total_quantity, ioa._id, ioa.grp, ioa.invoice_narration, ioa.item_unique_code, ioa.grade, orgf.factory_location FROM item_org_assoc as ioa, item_total_inventory as iti, organisation_factories as orgf WHERE iti._orgid=? and iti.geo_location=? and iti.item_org_assoc_fkid=ioa._id and iti.status=1 and orgf._id=? and orgf._orgid=?", formData.Orgid, formData.Factory, formData.Factory, formData.Orgid).Values(&maps)
		if err == nil && num > 0 {
			//fmt.Println(maps[0]["name"])
			//var i int64 = 0
			//for ; i < num; i++ {
			//fmt.Println(maps[i])
			//}
			for k, v := range maps {

				key := "items_" + strconv.Itoa(k)
				result[key] = v
			}

		} else {
			//fmt.Println(err)
			result["status"] = nil
		}

	} else if formData.Min == "" && formData.Max == "" && formData.Factory == "" {
		num, err := o.Raw("SELECT iti.total_quantity, ioa._id, ioa.grp, ioa.invoice_narration, ioa.item_unique_code, ioa.grade, orgf.factory_location FROM item_org_assoc as ioa, item_total_inventory as iti, organisation_factories as orgf WHERE iti._orgid=? and (ioa.invoice_narration=? or ioa.item_unique_code=? or ioa.grp=? or ioa.grade=?) and iti.item_org_assoc_fkid=ioa._id and iti.status=1 and orgf._id=iti.geo_location and orgf._orgid=?", formData.Orgid, formData.ItemName, formData.ItemCode, formData.Group, formData.Grade, formData.Orgid).Values(&maps)
		if err == nil && num > 0 {
			//fmt.Println(maps[0]["name"])
			//var i int64 = 0
			//for ; i < num; i++ {
			//fmt.Println(maps[i])
			//}
			for k, v := range maps {

				key := "items_" + strconv.Itoa(k)
				result[key] = v
			}

		} else {
			// fmt.Println(err)
			result["status"] = nil
		}

	} else if formData.Factory == "" {
		num, err := o.Raw("SELECT iti.total_quantity, ioa._id, ioa.grp, ioa.invoice_narration, ioa.item_unique_code, ioa.grade, orgf.factory_location FROM item_org_assoc as ioa, item_total_inventory as iti, organisation_factories as orgf WHERE iti._orgid=? and (ioa.invoice_narration=? or ioa.item_unique_code=? or ioa.grp=? or ioa.grade=?) and (iti.total_quantity<=? or iti.total_quantity >=?) and iti.item_org_assoc_fkid=ioa._id and iti.status=1 and orgf._id=iti.geo_location and orgf._orgid=?", formData.Orgid, formData.ItemName, formData.ItemCode, formData.Group, formData.Grade, formData.Max, formData.Min, formData.Orgid).Values(&maps)
		if err == nil && num > 0 {
			//fmt.Println(maps[0]["name"])
			//var i int64 = 0
			//for ; i < num; i++ {
			//fmt.Println(maps[i])
			//}
			for k, v := range maps {

				key := "items_" + strconv.Itoa(k)
				result[key] = v
			}

		} else {
			// fmt.Println(err)
			result["status"] = nil
		}

	} else if formData.ItemName == "" && formData.ItemCode == "" && formData.Group == "" && formData.Grade == "" && formData.Factory != "" {
		//_, err := o.Raw("SELECT _id FROM organisation_factories WHERE factory_location=? and _orgid=?", formData.Factory, formData.Orgid).Values(&maps)
		//factory_id := maps[0]["_id"]
		num, err := o.Raw("SELECT iti.total_quantity, ioa._id, ioa.grp, ioa.invoice_narration, ioa.item_unique_code, ioa.grade, orgf.factory_location FROM item_org_assoc as ioa, item_total_inventory as iti, organisation_factories as orgf WHERE iti._orgid=? and (iti.total_quantity<=? or iti.total_quantity >=?) and iti.geo_location=? and iti.item_org_assoc_fkid=ioa._id and iti.status=1 and orgf._id=? and orgf._orgid=?", formData.Orgid, formData.Max, formData.Min, formData.Factory, formData.Factory, formData.Orgid).Values(&maps)
		if err == nil && num > 0 {
			//fmt.Println(maps[0]["name"])
			//var i int64 = 0
			//for ; i < num; i++ {
			//fmt.Println(maps[i])
			//}
			for k, v := range maps {

				key := "items_" + strconv.Itoa(k)
				result[key] = v
			}

		} else {
			//fmt.Println(err)
			result["status"] = nil
		}

	} else if (formData.ItemName != "" || formData.ItemCode != "" || formData.Group != "" || formData.Grade != "") && (formData.Min != "" || formData.Max != "") && formData.Factory != "" {
		//_, err := o.Raw("SELECT _id FROM organisation_factories WHERE factory_location=? and _orgid=?", formData.Factory, formData.Orgid).Values(&maps)
		//factory_id := maps[0]["_id"]
		num, err := o.Raw("SELECT iti.total_quantity, ioa._id, ioa.grp, ioa.invoice_narration, ioa.item_unique_code, ioa.grade, orgf.factory_location FROM item_org_assoc as ioa, item_total_inventory as iti, organisation_factories as orgf WHERE iti._orgid=? and (ioa.invoice_narration=? or ioa.item_unique_code=? or ioa.grp=? or ioa.grade=?) and (iti.total_quantity<=? or iti.total_quantity >=?) and iti.geo_location=? and iti.item_org_assoc_fkid=ioa._id and iti.status=1 and orgf._id=? and orgf._orgid=?", formData.Orgid, formData.ItemName, formData.ItemCode, formData.Group, formData.Grade, formData.Max, formData.Min, formData.Factory, formData.Factory, formData.Orgid).Values(&maps)
		if err == nil && num > 0 {
			//fmt.Println(maps[0]["name"])
			var i int64 = 0
			for ; i < num; i++ {
				//fmt.Println(maps[i])
			}
			for k, v := range maps {

				key := "items_" + strconv.Itoa(k)
				result[key] = v
			}

		} else {
			//fmt.Println(err)
			result["status"] = nil
		}

	} else if formData.Min == "" && formData.Max == "" && formData.Factory != "" {
		//_, err := o.Raw("SELECT _id FROM organisation_factories WHERE factory_location=? and _orgid=?", formData.Factory, formData.Orgid).Values(&maps)
		//factory_id := maps[0]["_id"]
		num, err := o.Raw("SELECT iti.total_quantity, ioa._id, ioa.grp, ioa.invoice_narration, ioa.item_unique_code, ioa.grade, orgf.factory_location FROM item_org_assoc as ioa, item_total_inventory as iti, organisation_factories as orgf WHERE iti._orgid=? and (ioa.invoice_narration=? or ioa.item_unique_code=? or ioa.grp=? or ioa.grade=?) and iti.geo_location=? and iti.item_org_assoc_fkid=ioa._id and iti.status=1 and orgf._id=? and orgf._orgid=?", formData.Orgid, formData.ItemName, formData.ItemCode, formData.Group, formData.Grade, formData.Factory, formData.Factory, formData.Orgid).Values(&maps)
		if err == nil && num > 0 {
			//fmt.Println(maps[0]["name"])
			var i int64 = 0
			for ; i < num; i++ {
				//fmt.Println(maps[i])
			}
			for k, v := range maps {

				key := "items_" + strconv.Itoa(k)
				result[key] = v
			}

		} else {
			//fmt.Println(err)
			result["status"] = nil
		}

	}
	return result
}

func GetInventoryDetails(formData StockViewInventory) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT iti.total_quantity, iti.geo_location, ioa._id, ioa.grp, ioa.invoice_narration, ioa.item_unique_code, ioa.item_code_gen, ioa.grade, orgf.factory_location FROM item_total_inventory as iti, item_org_assoc as ioa, organisation_factories as orgf WHERE iti._orgid=? and ioa._id=iti.item_org_assoc_fkid and iti.status=1 and orgf._id=iti.geo_location and orgf._orgid=? ORDER BY iti.last_updated DESC", formData.Orgid, formData.Orgid).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		var i int64 = 0
		for ; i < num; i++ {
			//fmt.Println(maps[i])
		}
		for k, v := range maps {

			key := "items_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetLowLevelInventoryDetails(formData StockViewInventory) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT iti.total_quantity, iti.geo_location, ioa._id, ioa.grp, ioa.invoice_narration, ioa.item_unique_code, ioa.item_code_gen, ioa.grade, orgf.factory_location FROM item_total_inventory as iti, item_org_assoc as ioa, organisation_factories as orgf WHERE iti._orgid=? and ioa._id=iti.item_org_assoc_fkid and iti.status=1 and orgf._id=iti.geo_location and orgf._orgid=? and iti.low_level=1 ORDER BY iti.last_updated DESC", formData.Orgid, formData.Orgid).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		var i int64 = 0
		for ; i < num; i++ {
			//fmt.Println(maps[i])
		}
		for k, v := range maps {

			key := "items_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func NewSearchInventoryDetails(formData StockViewInventory) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()

	s := ""
	if formData.ItemName != "" {
		s = s + "iti.item_org_assoc_fkid = " + formData.ItemName + " AND "
	}
	if formData.Factory != "" {
		s = s + "iti.geo_location = " + formData.Factory + " AND "
	}
	if formData.Min != "" {
		s = s + "iti.total_quantity >= '" + formData.Min + "' AND "
	}
	if formData.Max != "" {
		s = s + "iti.total_quantity <= '" + formData.Max + "' AND "
	}
	if formData.Group != "" {
		s = s + "ioa.grp = '" + formData.Group + "' AND "
	}
	if formData.Grade != "" {
		s = s + "ioa.grade = '" + formData.Grade + "' AND "
	}

	var maps []orm.Params
	num, err := o.Raw("SELECT iti.total_quantity, iti.geo_location, ioa._id, ioa.grp, ioa.invoice_narration, ioa.item_unique_code, ioa.item_code_gen, ioa.grade, orgf.factory_location FROM item_total_inventory as iti, item_org_assoc as ioa, organisation_factories as orgf WHERE "+s+"iti._orgid=? and ioa._id=iti.item_org_assoc_fkid and iti.status=1 and orgf._id=iti.geo_location and orgf._orgid=? ORDER BY iti.last_updated DESC", formData.Orgid, formData.Orgid).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		var i int64 = 0
		for ; i < num; i++ {
			//fmt.Println(maps[i])
		}
		for k, v := range maps {

			key := "items_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetViewGRNItems(item_code string, orgid string) map[string]orm.Params {

	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT item_org_assoc.invoice_narration, item_org_assoc.item_unique_code FROM item_org_assoc WHERE _orgid=? and item_org_assoc.item_unique_code=?", orgid, item_code).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		var i int64 = 0
		for ; i < num; i++ {
			//fmt.Prinln(maps[i])
		}
		for k, v := range maps {

			key := "items_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result

}

//ACTIVE FUNCTION FOR STOCK ADJUSTMENT
func MakeStockAdjustment(formData StockAdjustmentRecord, stockData []map[string]string) int {

	o := orm.NewOrm()
	ormTransactionalErr := o.Begin()

	_, err2 := o.Raw("INSERT INTO item_stock_adjustment (_orgid, reason, date_of_inspection, quantity_on_record, observed_quantity, remark, item_org_assoc_fkid, adjusted) VALUES (?,?,?,?,?,?,?,?) ", formData.Orgid, formData.Reason, formData.DateInspection, formData.TotalQtyInStock, formData.QtyObserved, formData.Remark, formData.ItemOrgFkid, 1).Exec()
	if err2 != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}
	var maps8 []orm.Params
	_, err8 := o.Raw("SELECT _id FROM item_stock_adjustment WHERE reason=? and quantity_on_record=? and observed_quantity=? and _orgid=? and item_org_assoc_fkid=? and adjusted=1", formData.Reason, formData.TotalQtyInStock, formData.QtyObserved, formData.Orgid, formData.ItemOrgFkid).Values(&maps8)
	if err8 != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}

	// Sum, err := strconv.ParseFloat(formData.TotalQtyInStock, 64)
	// if err != nil {
	// 	ormTransactionalErr = o.Rollback()
	// 	return 0
	// }

	// QtyObs, err := strconv.ParseFloat(formData.QtyObserved, 64)
	// if err != nil {
	// 	ormTransactionalErr = o.Rollback()
	// 	return 0
	// }

	ItemOrgAssoc, err := strconv.ParseInt(formData.ItemOrgFkid, 10, 64)
	if err != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}

	//if Sum != QtyObs {
	for _, it := range stockData {
		if it["stock_adjust"] != "" {
			floatAjustQty, _ := strconv.ParseFloat(it["stock_adjust"], 64)
			if floatAjustQty < 0 {
				ormTransactionalErr = o.Rollback()
				return 0
			}

			itemFkid, _ := strconv.ParseInt(it["item_fkid"], 10, 64) // FK ID OF ITEM STOCK TABLEFKiD

			_insertTableFkid := maps8[0]["_id"].(string) // PRIMARY KEY OF THE ADJUSTMENT TABLE
			insertTableFkId, _ := strconv.ParseInt(_insertTableFkid, 10, 64)

			//INSERT IN ITEM STOCK TABLE
			itemStockData := ItemStockTable{
				Id:               itemFkid,
				Itemid:           ItemOrgAssoc,
				OrgId:            formData.Orgid,
				QuantityModified: floatAjustQty,
				InsertStatus:     2,
				InsertTableFkid:  insertTableFkId,
				StartDate:        time.Now(),
				EndDate:          time.Now(),
				ExpiresBy:        time.Now(), //Temporary Assigning Time.Now as no provision on UI
			}

			insertQueryStatus := InsertUpdateItemStock(itemStockData)
			if insertQueryStatus == 0 {
				ormTransactionalErr = o.Rollback()
				return 0
			}
		}
		//}

		//code64, _ := strconv.ParseInt(formData.ItemCode, 10, 64)
		//qtyObs64, _ := strconv.ParseFloat(formData.QtyObserved, 64)

		//UPDATE THE ITEM TOTAL INVENTORY
		// itemInventoryData := ItemTotalInventory{
		// 	OrgId:             formData.Orgid,
		// 	//Itemid:            code64,
		// 	Itemid:            ItemOrgAssoc,
		// 	//QuantityModified:  qtyObs64,
		// 	QuantityModified:  QtyObs,
		// 	Location:          formData.Location,
		// 	AddRemoveFlag:     31,
		// }

		// insertInvStatus := InsertUpdateItemTotalInventory(itemInventoryData)
		// if insertInvStatus == 0{
		// 	ormTransactionalErr = o.Rollback()
		// 	return 0
		// }

		//Update ITEM TOTAL INVENTORY
		insertInvStatus := InsertUpdateItemTotalInventoryDuringAdjustment(formData.Orgid, ItemOrgAssoc, formData.Location)
		if insertInvStatus == 0 {
			ormTransactionalErr = o.Rollback()
			return 0
		}
	}
	if err != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}

	if ormTransactionalErr != nil {
		return 0
	}
	ormTransactionalErr = o.Commit()
	return 1
}

func GetSuppliers(formData StockItems) map[string]orm.Params {
	// var stock []Stock
	// o := orm.NewOrm()
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	//num, err := o.Raw("SELECT * FROM item_master ").Values(&maps)
	num, err := o.Raw("SELECT * from supplier_org_assoc where _orgid =? and status = 1 ", formData.Orgid).Values(&maps)
	if err == nil && num > 0 {
		for k, v := range maps {
			key := "stock_" + strconv.Itoa(k)
			result[key] = v
		}
	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetAllItemsForDropdowns(orgId string) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT * from item_org_assoc where _orgid =? and status = 1 ", orgId).Values(&maps)
	if err == nil && num > 0 {
		for k, v := range maps {
			key := "stock_" + strconv.Itoa(k)
			result[key] = v
		}
	} else {
		result["status"] = nil
	}
	return result
}

//Do not use. Use GetAllItemsForDropdowns
func GetAllStockItems(formData StockItems) map[string]orm.Params {
	// var stock []Stock
	// o := orm.NewOrm()
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	//num, err := o.Raw("SELECT * FROM item_master ").Values(&maps)
	num, err := o.Raw("SELECT * from item_org_assoc where _orgid =? and status = 1 ", formData.Orgid).Values(&maps)
	if err == nil && num > 0 {
		var i int64 = 0
		for ; i < num; i++ {
			var factoryLoc = ""
			if maps[i]["geo_location"] != nil && maps[i]["geo_location"] != "" {
				var loc_tags []string
				loc_tags = strings.Split(maps[i]["geo_location"].(string), ",")
				for j := 0; j < len(loc_tags); j++ {
					if loc_tags[j] == "" {

					} else {
						var maps1 []orm.Params
						count, err1 := o.Raw("SELECT distinct factory_location FROM organisation_factories WHERE _id=? and _orgid=? and status=1", loc_tags[j], formData.Orgid).Values(&maps1)
						if err1 == nil && count > 0 {
							if len(loc_tags) == 1 {
								factoryLoc = maps1[0]["factory_location"].(string)
							} else if j < len(loc_tags)-2 {
								factoryLoc = factoryLoc + maps1[0]["factory_location"].(string) + ", "
							} else {
								factoryLoc = factoryLoc + maps1[0]["factory_location"].(string)
							}
						}
					}
				}
				maps[i]["factory_location"] = factoryLoc
			} else {
				maps[i]["factory_location"] = ""
			}
		}
		for k, v := range maps {
			key := "stock_" + strconv.Itoa(k)
			result[key] = v
		}
	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetAutocompleteItems() map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	//num, err := o.Raw("SELECT * FROM item_master ").Values(&maps)
	num, err := o.Raw("SELECT item_org_assoc.item_unique_code, item_org_assoc.invoice_narration FROM item_org_assoc ").Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		var i int64 = 0
		for ; i < num; i++ {
			//fmt.Println(maps[i])
		}
		for k, v := range maps {
			key := "stock_" + strconv.Itoa(k)
			result[key] = v
		}
	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetAdjustedInventory(formData StockAdjustmentRecord) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	//num, err := o.Raw("SELECT * FROM item_master ").Values(&maps)
	num, err := o.Raw("SELECT ioa.item_code_gen,ioa.invoice_narration, quantity_on_record , observed_quantity,reason,remark, date_of_inspection as ins_date from item_stock_adjustment as isa, item_org_assoc as ioa where  isa.item_org_assoc_fkid=ioa._id AND isa._orgid=? and adjusted = 1", formData.Orgid).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		var i int64 = 0
		for ; i < num; i++ {
			//fmt.Println(maps[i])
		}
		for k, v := range maps {
			if maps[k]["ins_date"] != nil && maps[k]["ins_date"] != "" {
				maps[k]["ins_date"] = convertDateString(maps[k]["ins_date"].(string))
			}

			key := "adjustment_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetItemsForAdjustment(formData StockAdjustmentRecord) map[string]orm.Params {

	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params

	num, err := o.Raw("SELECT ioa.invoice_narration, orgf.factory_location, ist._id, ist.quantity, ist.rate, ist.start_date, ist.last_updated, pso.unique_serial_no, psg.grn_no, psg.delivered_date, psg.received_sent_quantity FROM item_org_assoc as ioa, organisation_factories as orgf, item_stock as ist, purchase_service_order as pso, po_so_items_with_grn as psg WHERE ist._orgid=? and ist.geo_location=? and ioa._id=? and orgf._id=ist.geo_location and ist.item_org_assoc_fkid=? and psg._id=ist.po_so_items_with_grn_fkid and psg.item_org_assoc_fkid=? and pso._id=psg.purchase_service_order_fkid and ist.latest=1 ORDER BY ist.last_updated", formData.Orgid, formData.Location, formData.ItemOrgFkid, formData.ItemOrgFkid, formData.ItemOrgFkid).Values(&maps)
	if err == nil && num > 0 {

		for k, v := range maps {

			key := "item_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {

		result["status"] = nil
	}
	return result
}

func GetSum(formData StockAdjustmentRecord) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params

	num, err := o.Raw("SELECT SUM(quantity) AS Total FROM item_stock WHERE _orgid=? and item_org_assoc_fkid=? and item_stock.latest=1", formData.Orgid, formData.ItemOrgFkid).Values(&maps)
	if err == nil && num > 0 {

		for k, v := range maps {

			key := "sum_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {

		result["status"] = nil
	}
	return result
}

func GetTotalStockQty(formData StockAdjustmentRecord) string {
	result := "0"
	o := orm.NewOrm()
	var maps []orm.Params
	//num, err := o.Raw("SELECT SUM(quantity) AS Total FROM item_stock WHERE _orgid=? and item_org_assoc_fkid=? and geo_location=? and item_stock.latest=1 and blocked = 0", formData.Orgid, formData.ItemOrgFkid, formData.Location).Values(&maps)
	num, err := o.Raw("SELECT SUM(quantity) AS Total FROM item_stock WHERE _orgid=? and item_org_assoc_fkid=? and geo_location=? and item_stock.latest=1 ", formData.Orgid, formData.ItemOrgFkid, formData.Location).Values(&maps)
	if err == nil && num > 0 {
		if maps[0]["Total"] != nil {
			result = maps[0]["Total"].(string)
		}
	}
	return result
}

func GetSAdjItems(formData StockAdjustmentRecord) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT _id, invoice_narration, item_unique_code, unit_measure FROM item_org_assoc WHERE _orgid=? and status=1", formData.Orgid).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println("********num :", num)
		var i int64 = 0
		for ; i < num; i++ {
			//fmt.Println(maps[i])
		}
		for k, v := range maps {

			key := "item_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetFactory(formData StockItems) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT _id,factory_location,factory_type FROM organisation_factories WHERE _orgid=? and status=1 ", formData.Orgid).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println("********num :", num)
		var i int64 = 0
		for ; i < num; i++ {
			//fmt.Println(maps[i])
		}
		for k, v := range maps {

			key := "fac_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetFactoryStock(formData StockItems) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT distinct factory_location FROM organisation_factories WHERE _orgid=? and status=1 ", formData.Orgid).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println("********num :", num)
		var i int64 = 0
		for ; i < num; i++ {
			//fmt.Println(maps[i])
		}
		for k, v := range maps {

			key := "fac_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetStockInventory(formData InventoryUsage) map[string]orm.Params {
	// var stock []Stock
	// o := orm.NewOrm()
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	//num, err := o.Raw("SELECT * FROM item_master ").Values(&maps)
	num, err := o.Raw("SELECT s.quantity,s.rate,s.start_date,s.expires_by,s.last_updated,s.geo_location FROM item_stock as s where s._orgid =?  ", formData.Orgid).Values(&maps)
	if err == nil && num > 0 {
		for k, v := range maps {
			key := "stock_" + strconv.Itoa(k)
			result[key] = v
		}
	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetInventoryUsage(formData InventoryUsage) map[string]orm.Params {

	result := make(map[string]orm.Params)
	result["status"] = nil
	o := orm.NewOrm()
	var maps []orm.Params

	s := ""
	if formData.ItemName != "" {
		s = s + "ioa._id = " + formData.ItemName + " AND "
	}
	if formData.Factory != "" {
		s = s + "ist.geo_location = " + formData.Factory + " AND "
	}
	if formData.From != "" {
		s = s + "start_date >= '" + formData.From + "' AND "
	}
	if formData.To != "" {
		s = s + "end_date <= '" + formData.To + "' AND "
	}

	num, err := o.Raw("SELECT ist._id, ioa.invoice_narration,ist.last_updated, ist.batch_no, ist.quantity, ist.consumed_qty, ist.grn_no, ist.start_date as start_date, ist.rate, orgf.factory_location as location,case when blocked = 1 then (Select batch_no from production_orders as p where p._id = consumed_table_fkid)	when blocked = 3 then (Select sales_order_code_gen from sales_order as s where s._id = consumed_table_fkid) end as consumed_for, case when blocked = 1 then 'Production' when blocked = 3 then 'Sales Order' end as consumed_by, case when blocked = 1 then (Select _id from production_orders as p where p._id = consumed_table_fkid)	when blocked = 3 then (Select _id from sales_order as s where s._id = consumed_table_fkid) end as spPkId FROM item_org_assoc as ioa, item_stock as ist, organisation_factories as orgf	where "+s+" ioa._id = ist.item_org_assoc_fkid and orgf._id=ist.geo_location and ist.consumed_qty <>0 and ioa._orgid = ? and  ist._orgid = ? and orgf._orgid = ? ", formData.Orgid, formData.Orgid, formData.Orgid).Values(&maps)
	if err == nil && num > 0 {
		for k, v := range maps {
			// if maps[k]["start_date"] != nil && maps[k]["start_date"] != "" {
			// 	maps[k]["start_date"] = convertDateString(maps[k]["start_date"].(string))
			// }
			key := "stock_" + strconv.Itoa(k)
			result[key] = v
		}
	}

	// if formData.From == "" && formData.To == "" {
	// 	num, err := o.Raw("SELECT ioa.invoice_narration, ist.quantity, ist.grn_no, DATE(ist.start_date) as start_date, ist.rate, of.factory_location as location,case when blocked = 1 then (Select batch_no from production_orders as p where p._id = consumed_table_fkid)	when blocked = 3 then (Select sales_order_code_gen from sales_order as s where s._id = consumed_table_fkid)	end as consumed_for	, case when blocked = 1 then 'Production'	when blocked = 3 then 'Sales Order'end as consumed_by FROM item_org_assoc as ioa, item_stock as ist, organisation_factories as of	where ioa._id = ist.item_org_assoc_fkid and ist.geo_location = of._id and ioa._orgid = ? and  ist._orgid = ? and of._orgid = ? and (ioa._id=? OR ioa.item_code_gen=?)",formData.Orgid, formData.Orgid, formData.Orgid, formData.ItemName, formData.ItemCode).Values(&maps)
	// 	if err == nil && num > 0 {
	// 		for k, v := range maps {
	// 			key := "stock_" + strconv.Itoa(k)
	// 			result[key] = v
	// 		}
	// 	} else {

	// 		result["status"] = nil
	// 	}
	// } else if formData.ItemName == "" && formData.ItemCode == "" {
	// 	num, err := o.Raw("SELECT ioa.invoice_narration, ist.quantity, ist.grn_no, DATE(ist.start_date) as start_date, ist.rate, of.factory_location as location,case when blocked = 1 then (Select batch_no from production_orders as p where p._id = consumed_table_fkid)	when blocked = 3 then (Select sales_order_code_gen from sales_order as s where s._id = consumed_table_fkid)	end as consumed_for	, case when blocked = 1 then 'Production'	when blocked = 3 then 'Sales Order'end as consumed_by FROM item_org_assoc as ioa, item_stock as ist, organisation_factories as of	where ioa._id = ist.item_org_assoc_fkid and ist.geo_location = of._id and ioa._orgid = ? and  ist._orgid = ? and of._orgid = ? and  (start_date >= ? AND end_date <= ?)", formData.Orgid,formData.Orgid, formData.Orgid, formData.From, formData.To).Values(&maps)
	// 	if err == nil && num > 0 {
	// 		for k, v := range maps {
	// 			key := "stock_" + strconv.Itoa(k)
	// 			result[key] = v
	// 		}
	// 	} else {

	// 		result["status"] = nil
	// 	}

	// } else if (formData.ItemName != "" || formData.ItemCode != "") && (formData.From != "" || formData.To != "") {
	// 	num, err := o.Raw("SELECT ioa.invoice_narration, ist.quantity, ist.grn_no, DATE(ist.start_date) as start_date, ist.rate, of.factory_location as location,case when blocked = 1 then (Select batch_no from production_orders as p where p._id = consumed_table_fkid)	when blocked = 3 then (Select sales_order_code_gen from sales_order as s where s._id = consumed_table_fkid) end as consumed_for, case when blocked = 1 then 'Production'	when blocked = 3 then 'Sales Order'end as consumed_by	FROM item_org_assoc as ioa, item_stock as ist, organisation_factories as of	where ioa._id = ist.item_org_assoc_fkid and ist.geo_location = of._id and ioa._orgid = ? and  ist._orgid = ? and of._orgid = ? and (start_date >= ? AND end_date <= ?) and (ioa.item_code_gen = ? OR ioa._id =?)", formData.Orgid, formData.Orgid,formData.Orgid, formData.From, formData.To, formData.ItemCode, formData.ItemName).Values(&maps)
	// 	if err == nil && num > 0 {
	// 		for k, v := range maps {
	// 			key := "stock_" + strconv.Itoa(k)
	// 			result[key] = v
	// 		}
	// 	} else {

	// 		result["status"] = nil
	// 	}

	// }
	return result
}

func GetViewInventoryUsage(formData InventoryUsage) map[string]orm.Params {
	// var stock []Stock
	// o := orm.NewOrm()
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	//num, err := o.Raw("SELECT * FROM item_master ").Values(&maps)
	if formData.From == "" && formData.To == "" && (formData.ItemName != "" || formData.ItemCode != "") {
		num, err := o.Raw("SELECT ioa.item_unique_code, ioa.invoice_narration, isu.quantity_used, isi.rate, isi.start_date, isu.used_on, isi.geo_location FROM item_org_assoc as ioa, item_stock_usage as isu, item_stock as isi WHERE isu._orgid =? and isi._orgid =? and ioa._orgid=? and isu.item_org_assoc_fkid=ioa._id and isi.item_org_assoc_fkid=ioa._id and (ioa.invoice_narration=? OR ioa.item_unique_code=?)", formData.Orgid, formData.Orgid, formData.Orgid, formData.ItemName, formData.ItemCode).Values(&maps)
		if err == nil && num > 0 {
			for k, v := range maps {
				key := "stock_" + strconv.Itoa(k)
				result[key] = v
			}
		} else {
			//fmt.Println(err)
			result["status"] = nil
		}
	} else if formData.ItemName == "" && formData.ItemCode == "" && formData.From != "" && formData.To != "" {
		num, err := o.Raw("SELECT ioa.item_unique_code, ioa.invoice_narration, isu.quantity_used, isi.rate, isi.start_date, isu.used_on, isi.geo_location FROM item_org_assoc as ioa, item_stock_usage as isu, item_stock as isi WHERE isu._orgid =? and isi._orgid =? and ioa._orgid=? and ioa._id=isu.item_org_assoc_fkid and ioa._id=isi.item_org_assoc_fkid and (start_date >= ? AND end_date <= ?)", formData.Orgid, formData.Orgid, formData.Orgid, formData.From, formData.To).Values(&maps)
		if err == nil && num > 0 {
			for k, v := range maps {
				key := "stock_" + strconv.Itoa(k)
				result[key] = v
			}
		} else {
			//fmt.Println(err)
			result["status"] = nil
		}

	} else if (formData.ItemName != "" || formData.ItemCode != "") && (formData.From != "" && formData.To != "") {
		num, err := o.Raw("SELECT ioa.item_unique_code, ioa.invoice_narration, isu.quantity_used, isi.rate, isi.start_date, isu.used_on, isi.geo_location FROM item_org_assoc as ioa, item_stock_usage as isu, item_stock as isi WHERE isi._orgid=? and isu._orgid =? and ioa._orgid=? and ioa._d=isi.item_org_assoc_fkid and ioa._id=isu.item_org_assoc_fkid and (isi.start_date >= ? AND isi.end_date <= ?) and (ioa.item_unique_code = ? OR ioa.invoice_narration =?)", formData.Orgid, formData.Orgid, formData.Orgid, formData.From, formData.To, formData.ItemCode, formData.ItemName).Values(&maps)
		if err == nil && num > 0 {
			for k, v := range maps {
				key := "stock_" + strconv.Itoa(k)
				result[key] = v
			}
		} else {
			//fmt.Println(err)
			result["status"] = nil
		}

	} else {
		num, err := o.Raw("SELECT ioa.item_unique_code, ioa.invoice_narration, isu.quantity_used, isi.rate, isi.start_date, isi.used_on, isi.geo_location FROM item_org_assoc as ioa, item_stock_usage as isu, item_stock as isi WHERE isi._orgid=? and isu._orgid =? and ioa._orgid=? and ioa._d=isi.item_org_assoc_fkid and ioa._id=isu.item_org_assoc_fkid ORDER BY isu.lastupdated DESC", formData.Orgid, formData.Orgid, formData.Orgid).Values(&maps)
		if err == nil && num > 0 {
			for k, v := range maps {
				key := "stock_" + strconv.Itoa(k)
				result[key] = v
			}
		} else {
			//fmt.Println(err)
			result["status"] = nil
		}

	}
	return result
}

func GetGRNView(formData GRNUsage) map[string]orm.Params {
	// var stock []Stock
	// o := orm.NewOrm()
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	//num, err := o.Raw("SELECT * FROM item_master ").Values(&maps)
	s := ""
	if formData.ItemID != "" {
		s = s + "psg.item_org_assoc_fkid = " + formData.ItemID + " AND "
	}
	if formData.Factory != "" {
		s = s + "psg.geo_location = " + formData.Factory + " AND "
	}
	if formData.FromDate != "" {
		s = s + "date(psg.delivered_date) >= '" + formData.FromDate + "' AND "
	}
	if formData.ToDate != "" {
		s = s + "date(psg.delivered_date) <= '" + formData.ToDate + "' AND "
	}

	// if (formData.ItemID != "" && formData.Factory == "" && formData.FromDate == "" && formData == "" ) {
	// 	num, err := o.Raw("SELECT psg.grn_no, psg.grn_no_gen, psg.batch_no, pso.unique_serial_no, pso.purchase_order_code_gen, psg.received_sent_quantity, DATE(psg.delivered_date) as d_date, orgf.factory_location, psg.received_sent_by, ioa.invoice_narration, ioa.item_code_gen FROM po_so_items_with_grn as psg, item_org_assoc as ioa, purchase_service_order as pso, organisation_factories as orgf WHERE psg.item_org_assoc_fkid=? and pso._id=psg.purchase_service_order_fkid and orgf._id=pso.geo_location and psg._orgid = ? and ioa._orgid = ? and pso._orgid = ? and ioa._id=? ", formData.ItemID, formData.Orgid, formData.Orgid, formData.Orgid, formData.ItemID).Values(&maps)
	// } else if (formData.ItemID != "" && formData.Factory != "" && formData.FromDate == "" && formData.ToDate == "" ) {
	// 	num, err := o.Raw("SELECT psg.grn_no, psg.grn_no_gen, psg.batch_no, pso.unique_serial_no, pso.purchase_order_code_gen, psg.received_sent_quantity, DATE(psg.delivered_date) as d_date, orgf.factory_location, psg.received_sent_by, ioa.invoice_narration, ioa.item_code_gen FROM po_so_items_with_grn as psg, item_org_assoc as ioa, purchase_service_order as pso, organisation_factories as orgf WHERE psg.item_org_assoc_fkid=? and pso._id=psg.purchase_service_order_fkid and pso.geo_location=? and orgf._id=pso.geo_location and psg._orgid = ? and ioa._orgid = ? and pso._orgid = ? and ioa._id=? ", formData.ItemID, formData.Factory, formData.Orgid, formData.Orgid, formData.Orgid, formData.ItemID).Values(&maps)
	// } else if (formData.ItemID != "" && formData.Factory != "" && formData.FromDate != "" && formData.ToDate == "" ) {
	// 	num, err := o.Raw("SELECT psg.grn_no, psg.grn_no_gen, psg.batch_no, pso.unique_serial_no, pso.purchase_order_code_gen, psg.received_sent_quantity, DATE(psg.delivered_date) as d_date, orgf.factory_location, psg.received_sent_by, ioa.invoice_narration, ioa.item_code_gen FROM po_so_items_with_grn as psg, item_org_assoc as ioa, purchase_service_order as pso, organisation_factories as orgf WHERE psg.item_org_assoc_fkid=? and pso._id=psg.purchase_service_order_fkid and pso.geo_location=? and orgf._id=pso.geo_location and psg.delivered_date >=? and psg._orgid = ? and ioa._orgid = ? and pso._orgid = ? and ioa._id=? ", formData.ItemID, formData.Factory, formData.FromDate, formData.Orgid, formData.Orgid, formData.Orgid, formData.ItemID).Values(&maps)
	// } else if (formData.ItemID != "" && formData.Factory != "" && formData.FromDate != "" && formData.ToDate != "" ) { //When all fields entered
	// 	num, err := o.Raw("SELECT psg.grn_no, psg.grn_no_gen, psg.batch_no, pso.unique_serial_no, pso.purchase_order_code_gen, psg.received_sent_quantity, DATE(psg.delivered_date) as d_date, orgf.factory_location, psg.received_sent_by, ioa.invoice_narration, ioa.item_code_gen FROM po_so_items_with_grn as psg, item_org_assoc as ioa, purchase_service_order as pso, organisation_factories as orgf WHERE psg.item_org_assoc_fkid=? and pso._id=psg.purchase_service_order_fkid and pso.geo_location=? and orgf._id=pso.geo_location and psg.delivered_date >=? and psg.delivered_date <=? and psg._orgid = ? and ioa._orgid = ? and pso._orgid = ? and ioa._id=? ", formData.ItemID, formData.Factory, formData.FromDate, formData.ToDate, formData.Orgid, formData.Orgid, formData.Orgid, formData.ItemID).Values(&maps)
	// } else if (formData.ItemID != "" && formData.Factory != "" && formData.FromDate == "" && formData.ToDate != "" ) {
	// 	num, err := o.Raw("SELECT psg.grn_no, psg.grn_no_gen, psg.batch_no, pso.unique_serial_no, pso.purchase_order_code_gen, psg.received_sent_quantity, DATE(psg.delivered_date) as d_date, orgf.factory_location, psg.received_sent_by, ioa.invoice_narration, ioa.item_code_gen FROM po_so_items_with_grn as psg, item_org_assoc as ioa, purchase_service_order as pso, organisation_factories as orgf WHERE psg.item_org_assoc_fkid=? and pso._id=psg.purchase_service_order_fkid and pso.geo_location=? and orgf._id=pso.geo_location and psg.delivered_date <=? and psg._orgid = ? and ioa._orgid = ? and pso._orgid = ? and ioa._id=? ", formData.ItemID, formData.Factory, formData.ToDate, formData.Orgid, formData.Orgid, formData.Orgid, formData.ItemID).Values(&maps)
	// }
	num, err := o.Raw("SELECT psg.grn_no, psg.item_org_assoc_fkid, psg.geo_location, psg.grn_no_gen, psg.batch_no, psg.purchase_service_order_fkid, psg.received_sent_quantity, psg.delivered_date as d_date, orgf.factory_location, psg.received_sent_by, ioa.invoice_narration, ioa.item_code_gen FROM po_so_items_with_grn as psg, item_org_assoc as ioa, organisation_factories as orgf WHERE "+s+" orgf._id=psg.geo_location and psg._orgid = ? and ioa._orgid = ? and ioa._id=psg.item_org_assoc_fkid ", formData.Orgid, formData.Orgid).Values(&maps)
	if err == nil && num > 0 {
		for k, v := range maps {
			if maps[k]["purchase_service_order_fkid"] == nil || maps[k]["purchase_service_order_fkid"] == "" {
				maps[k]["unique_serial_no"] = ""
				maps[k]["purchase_order_code_gen"] = ""
			} else {
				var maps1 []orm.Params
				num1, err1 := o.Raw("SELECT pso.unique_serial_no, pso.purchase_order_code_gen FROM purchase_service_order as pso WHERE pso._id=? and pso.geo_location=? and pso._orgid = ? ", maps[k]["purchase_service_order_fkid"], maps[k]["geo_location"], formData.Orgid).Values(&maps1)
				if num1 <= 0 || err1 != nil {
					maps[k]["unique_serial_no"] = ""
					maps[k]["purchase_order_code_gen"] = ""
				} else {
					maps[k]["unique_serial_no"] = maps1[0]["unique_serial_no"].(string)
					maps[k]["purchase_order_code_gen"] = maps1[0]["purchase_order_code_gen"].(string)
				}
			}

			if maps[k]["d_date"] != nil && maps[k]["d_date"] != "" {
				maps[k]["d_date"] = convertDateString(maps[k]["d_date"].(string))
			}

			key := "stock_" + strconv.Itoa(k)
			result[key] = v
		}
	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func ConsumeInventoryByItemPKID(itemPKID string, quantityToConsume float64, orgid string, usedfor string, usedtablefkid string, usedtableCode string) int {

	var queryResultMap []orm.Params
	o := orm.NewOrm()
	ormTransactionalErr := o.Begin()
	num, err := o.Raw("select its._id, its._orgid, its.item_org_assoc_fkid, its.expires_by, its.quantity, its.start_date, its.end_date, its.rate, its.currency, its.geo_location, its.last_updated, its.latest, its.po_so_items_with_grn_fkid, its.items_stock_adjustment_fkid FROM item_stock as its, item_org_assoc as ioa WHERE its.item_org_assoc_fkid = ? and its.item_org_assoc_fkid = ioa._id and its._orgid = ? and its.quantity > 0 and its.latest =1 order by its.start_date asc  ", itemPKID, orgid).Values(&queryResultMap)
	if err == nil && num > 0 {
		for quantityToConsume > 0.0 {
			for count := int64(0); count <= num; count++ {
				rq := queryResultMap[count]["quantity"].(string)
				recordedQuantity, _ := strconv.ParseFloat(rq, 64)
				sid := queryResultMap[count]["_id"].(string)
				stockPKID, _ := strconv.ParseFloat(sid, 64)
				if quantityToConsume >= recordedQuantity {

					_, err := o.Raw("UPDATE item_stock set latest = 0 and end_date = ? where _id = ? and _orgid = ?", time.Now(), stockPKID, orgid).Exec()

					if err != nil {
						ormTransactionalErr = o.Rollback()
						return 0

					}
					_, err1 := o.Raw("INSERT into item_stock (_orgid,item_org_assoc_fkid,expires_by,quantity,start_date,end_date,rate,currency,geo_location,last_updated,latest,po-so_items_with_grn_fkid,items_stock_adjustment_fkid) values (?,?,?,?,?,?,?,?,?,?,?,?)", queryResultMap[count]["_orgid"], queryResultMap[count]["item_org_assoc_fkid"], queryResultMap[count]["expires_by"], 0.0, queryResultMap[count]["start_date"], queryResultMap[count]["end_date"], queryResultMap[count]["rate"], queryResultMap[count]["currency"], queryResultMap[count]["geo_location"], time.Now(), 1, queryResultMap[count]["po_so_items_with_grn_fkid"], queryResultMap[count]["items_stock_adjustment_fkid"]).Exec()
					if err1 != nil {
						ormTransactionalErr = o.Rollback()
						return 0
					}
					//supplierStockID, _ := result.LastInsertId()

					//insert recordedQuantity into item_stock_usage
					_, err2 := o.Raw("INSERT into item_stock_usage (_orgid, item_org_assoc_fkid, item_stock_fkid, quantity_used, used_for, used_table_fkid, used_table_unique_code, used_on, status) values (?,?,?,?,?,?,?,?,?)", queryResultMap[count]["_orgid"], queryResultMap[count]["item_org_assoc_fkid"], stockPKID, recordedQuantity, usedfor, usedtablefkid, usedtableCode, time.Now(), 1).Exec()
					if err2 != nil {
						ormTransactionalErr = o.Rollback()
						return 0
					}

					quantityToConsume = quantityToConsume - recordedQuantity
				} else {
					_, err := o.Raw("UPDATE item_stock set latest = 0 where _id = ? and _orgid = ?", stockPKID, time.Now(), orgid).Exec()
					if err != nil {
						ormTransactionalErr = o.Rollback()
						return 0
					}
					_, err2 := o.Raw("INSERT into item_stock (_orgid,item_org_assoc_fkid,expires_by,quantity,start_date,rate,currency,geo_location,last_updated,latest,po-so_items_with_grn_fkid,items_stock_adjustment_fkid) values (?,?,?,?,?,?,?,?,?,?,?,?)", queryResultMap[count]["_orgid"], queryResultMap[count]["item_org_assoc_fkid"], queryResultMap[count]["expires_by"], recordedQuantity-quantityToConsume, queryResultMap[count]["start_date"], queryResultMap[count]["rate"], queryResultMap[count]["currency"], queryResultMap[count]["geo_location"], time.Now(), 1, queryResultMap[count]["po_so_items_with_grn_fkid"], queryResultMap[count]["items_stock_adjustment_fkid"]).Exec()
					if err2 != nil {
						ormTransactionalErr = o.Rollback()
						return 0
					}

					//insert quantityToConsume into item_stock_usage
					_, err3 := o.Raw("INSERT into item_stock_usage (_orgid, item_org_assoc_fkid, item_stock_fkid, quantity_used, used_for, used_table_fkid, used_table_unique_code, used_on, status) values (?,?,?,?,?,?,?,?,?)", queryResultMap[count]["_orgid"], queryResultMap[count]["item_org_assoc_fkid"], stockPKID, quantityToConsume, usedfor, usedtablefkid, usedtableCode, time.Now(), 1).Exec()
					if err3 != nil {
						ormTransactionalErr = o.Rollback()
						return 0
					}

					quantityToConsume = 0.0
				}
			}
		}
	}
	ormTransactionalErr = o.Commit()
	if ormTransactionalErr != nil {
		return 0
	}
	return 1
}

func ConsumeInventoryByItemsMap(itemsMaps map[string]float64, orgid string, usedfor string, usedtablefkid string, usedtableCode string) int {
	var queryResultMap []orm.Params
	o := orm.NewOrm()
	ormTransactionalErr := o.Begin()

	//map[pkid][quantity]
	for itemPKID, quantityToConsume := range itemsMaps {
		num, err := o.Raw("SELECT its._id, its._orgid,its.item_org_assoc_fkid, its.expires_by, its.quantity, its.start_date, its.end_date, its.rate, its.currency, its.geo_location, its.last_updated, its.latest, its.po_so_items_with_grn_fkid, its.items_stock_adjustment_fkid FROM item_stock as its, item_org_assoc as ioa WHERE its.item_org_assoc_fkid=? and its._orgid=? and its.item_org_assoc_fkid=ioa._id and its.quantity > 0 and its.latest=1 order by its.start_date asc ", itemPKID, orgid).Values(&queryResultMap)
		var i int64 = 0
		for ; i < num; i++ {
			//fmt.Println(queryResultMap[i])
		}
		if err == nil && num > 0 {
			for quantityToConsume > 0.0 {
				for count := int64(0); count < num; count++ {

					if queryResultMap[count]["end_date"] == nil {

						queryResultMap[count]["end_date"] = nil
					}
					if queryResultMap[count]["po_so_items_with_grn_fkid"] == nil {

						queryResultMap[count]["po_so_items_with_grn_fkid"] = nil
					}
					if queryResultMap[count]["items_stock_adjustment_fkid"] == nil {

						queryResultMap[count]["items_stock_adjustment_fkid"] = nil
					}
					rq := queryResultMap[count]["quantity"].(string)
					recordedQuantity, _ := strconv.ParseFloat(rq, 64)
					sid := queryResultMap[count]["_id"].(string)
					stockPKID, _ := strconv.ParseFloat(sid, 64)
					if quantityToConsume >= recordedQuantity {
						_, err := o.Raw("UPDATE item_stock set latest = 0 and end_date = ? where _id = ? and _orgid = ?", time.Now(), stockPKID, orgid).Exec()
						if err != nil {
							ormTransactionalErr = o.Rollback()
							return 0
						}
						_, err1 := o.Raw("INSERT into item_stock (_orgid,item_org_assoc_fkid,expires_by,quantity,start_date,end_date,rate,currency,geo_location,last_updated,latest,po_so_items_with_grn_fkid,items_stock_adjustment_fkid) values (?,?,?,?,?,?,?,?,?,?,?,?,?)", queryResultMap[count]["_orgid"], queryResultMap[count]["item_org_assoc_fkid"], queryResultMap[count]["expires_by"], 0.0, queryResultMap[count]["start_date"], queryResultMap[count]["end_date"], queryResultMap[count]["rate"], queryResultMap[count]["currency"], queryResultMap[count]["geo_location"], time.Now(), 1, queryResultMap[count]["po_so_items_with_grn_fkid"], queryResultMap[count]["items_stock_adjustment_fkid"]).Exec()
						if err1 != nil {
							ormTransactionalErr = o.Rollback()
							return 0
						}

						//insert recordedQuantity into item_stock_usage
						_, err2 := o.Raw("INSERT into item_stock_usage (_orgid, item_org_assoc_fkid, item_stock_fkid, quantity_used, used_for, used_table_fkid, used_table_unique_code, used_on, status) values (?,?,?,?,?,?,?,?,?)", queryResultMap[count]["_orgid"], queryResultMap[count]["item_org_assoc_fkid"], stockPKID, recordedQuantity, usedfor, usedtablefkid, usedtableCode, time.Now(), 1).Exec()
						if err2 != nil {
							ormTransactionalErr = o.Rollback()
							return 0
						}

						quantityToConsume = quantityToConsume - recordedQuantity
					} else {
						_, err := o.Raw("UPDATE item_stock set latest = 0 where _id = ? and _orgid = ?", stockPKID, orgid).Exec()
						if err != nil {
							ormTransactionalErr = o.Rollback()
							return 0
						}
						_, err2 := o.Raw("INSERT into item_stock (_orgid,item_org_assoc_fkid,expires_by,quantity,start_date,rate,currency,geo_location,last_updated,latest,po_so_items_with_grn_fkid,items_stock_adjustment_fkid) values (?,?,?,?,?,?,?,?,?,?,?,?)", queryResultMap[count]["_orgid"], queryResultMap[count]["item_org_assoc_fkid"], queryResultMap[count]["expires_by"], recordedQuantity-quantityToConsume, queryResultMap[count]["start_date"], queryResultMap[count]["rate"], queryResultMap[count]["currency"], queryResultMap[count]["geo_location"], time.Now(), 1, queryResultMap[count]["po_so_items_with_grn_fkid"], queryResultMap[count]["items_stock_adjustment_fkid"]).Exec()
						if err2 != nil {
							ormTransactionalErr = o.Rollback()
							return 0
						}
						quantityToConsume = 0.0

						//insert quantityToConsume into item_stock_usage
						_, err3 := o.Raw("INSERT into item_stock_usage (_orgid, item_org_assoc_fkid, item_stock_fkid, quantity_used, used_for, used_table_fkid, used_table_unique_code, used_on, status) values (?,?,?,?,?,?,?,?,?)", queryResultMap[count]["_orgid"], queryResultMap[count]["item_org_assoc_fkid"], stockPKID, quantityToConsume, usedfor, usedtablefkid, usedtableCode, time.Now(), 1).Exec()
						if err3 != nil {
							ormTransactionalErr = o.Rollback()
							return 0
						}
					}
				}
			}
		}
	}
	ormTransactionalErr = o.Commit()
	if ormTransactionalErr != nil {
		return 0
	}
	return 1
}

func CheckInventoryLevelItemCode(itempkid string, quantity string, orgid string) int {

	o := orm.NewOrm()
	var maps []orm.Params
	var s string
	var result int
	num, err := o.Raw("SELECT SUM(quantity) AS Total FROM item_stock WHERE _orgid=? and item_org_assoc_fkid=? and latest = 1", orgid, itempkid).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println("num",num)
		if maps[0]["Total"] == nil {
			result = 2
		} else {
			s = maps[0]["Total"].(string)

			available, _ := strconv.ParseFloat(s, 64)
			sent, _ := strconv.ParseFloat(quantity, 64)
			// fmt.Println("sent",sent)
			// fmt.Println("quantity",available)
			if sent > available {

				result = 0
			} else {
				result = 1
			}
		}
	} else {

	}
	return result
}

func CheckInventoryLevelItemCodeProductionOrders(itempkid string, quantity string, orgid string, location string) int {

	o := orm.NewOrm()
	var maps []orm.Params
	var s string
	var result int
	num, err := o.Raw("SELECT total_quantity AS Total FROM item_total_inventory WHERE _orgid=? and item_org_assoc_fkid=? and status = 1 and geo_location=?", orgid, itempkid, location).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println("num",num)
		if maps[0]["Total"] == nil {
			result = 2
		} else {
			s = maps[0]["Total"].(string)

			available, _ := strconv.ParseFloat(s, 64)
			sent, _ := strconv.ParseFloat(quantity, 64)
			// fmt.Println("sent",sent)
			// fmt.Println("quantity",available)
			if sent > available {

				result = 0
			} else {
				result = 1
			}
		}
	} else {

	}
	return result
}

func ItemTotalInventoryUpdateByPkid(itemid string, orgid string, factoryId string) int {

	o := orm.NewOrm()
	//var maps0 []orm.Params
	var maps []orm.Params
	var maps1 []orm.Params
	var maps4 []orm.Params
	var t string
	var m string
	var low_level string
	//_, err0 := o.Raw("SELECT _id from item_org_assoc WHERE _orgid=? and (invoice_narration=? OR item_unique_code=?)", orgid, formData.Name, formData.Code).Values(&maps0)
	_, err := o.Raw("SELECT SUM(quantity) AS Total FROM item_stock WHERE _orgid=? and item_org_assoc_fkid=? and geo_location = ? and latest = 1", orgid, itemid, factoryId).Values(&maps)
	_, err1 := o.Raw("SELECT min_stock_trigger FROM item_org_assoc WHERE _orgid=? and _id=?", orgid, itemid).Values(&maps1)

	if err == nil && err1 == nil {

		t = maps[0]["Total"].(string)
		m = maps1[0]["min_stock_trigger"].(string)
		total_inventory, _ := strconv.ParseFloat(t, 64)
		min_trigger, _ := strconv.ParseFloat(m, 64)

		if total_inventory >= min_trigger {
			low_level = "0"
		} else {
			low_level = "1"
		}
		//fmt.Println("Inventory Level",low_level)
		_, err6 := o.Raw("SELECT _id FROM item_total_inventory WHERE _orgid=? and item_org_assoc_fkid=? and geo_location = ? and status=1", orgid, itemid, factoryId).Values(&maps4)

		if err6 == nil {

			_, err5 := o.Raw("UPDATE item_total_inventory set status = 0 where _id = ? and _orgid = ?", maps4[0]["_id"], orgid).Exec()
			_, err4 := o.Raw("INSERT into item_total_inventory (_orgid,item_org_assoc_fkid,total_quantity,last_updated,geo_location,status,low_level) values (?,?,?,?,?,?,?)", orgid, itemid, maps[0]["Total"], time.Now(), factoryId, "1", low_level).Exec()

			if err5 == nil && err4 == nil {
				return 1
			}
		}
	}
	return 0
}

func GetGroup(formData StockItems) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT distinct grp from item_org_assoc where _orgid=?", formData.Orgid).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		var i int64 = 0
		for ; i < num; i++ {
			//fmt.Println(maps[i])
		}
		for k, v := range maps {

			key := "items_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetSubGroup(formData StockItems) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT distinct sub_group from item_org_assoc where _orgid=?", formData.Orgid).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		var i int64 = 0
		for ; i < num; i++ {
			//fmt.Println(maps[i])
		}
		for k, v := range maps {

			key := "items_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetGrade(formData StockItems) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT distinct grade from item_org_assoc where _orgid=?", formData.Orgid).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		var i int64 = 0
		for ; i < num; i++ {
			//fmt.Println(maps[i])
		}
		for k, v := range maps {

			key := "items_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetItemsUOM(formData StockItems) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT distinct unit_measure from item_org_assoc where _orgid=?", formData.Orgid).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		var i int64 = 0
		for ; i < num; i++ {
			//fmt.Println(maps[i])
		}
		for k, v := range maps {

			key := "items_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func SearchByItemNameCode(ItemId string, itemName string, Orgid string) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT * from item_org_assoc where _orgid =? and ( item_unique_code like ? and name like  ? ) and status = 1", Orgid, "%"+ItemId+"%", "%"+itemName+"%").Values(&maps)
	if err == nil && num > 0 {
		for k, v := range maps {
			key := "stock_" + strconv.Itoa(k)
			result[key] = v
		}
	} else {
		result["status"] = nil
	}
	return result
}

//ACTIVE : Packaging Functions to be used.
func AddItemPackaging(_orgid string, ItemCode string, PacketSizes []map[string]string, PacketPrice []map[string]string) int {
	result := make(map[string]string)
	result["status"] = "0"
	o := orm.NewOrm()
	ormTransactionalErr := o.Begin()
	var err error

	for _, itt := range PacketSizes {
		if itt["packet_size"] != "" {
			var price *string
			price = nil
			if itt["price"] != "" {
				tempprice := itt["price"]
				price = &tempprice
			}

			_, err = o.Raw("INSERT INTO packaging ( item_org_assoc_fkid, packet_size, price, status, _orgid) VALUES (?,?,?,?,?)", ItemCode, itt["packet_size"], &price, 1, _orgid).Exec()
			if err != nil {
				ormTransactionalErr = o.Rollback()
				return 0
			}

		}
	}

	if err != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}

	if ormTransactionalErr != nil {
		return 0
	}
	ormTransactionalErr = o.Commit()
	return 1
}

func GetArchivedPackages(ItemCode string, _Orgid string) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params

	num, err := o.Raw("SELECT * from packaging where _orgid = ? and status = 0 and item_org_assoc_fkid = ? ", _Orgid, ItemCode).Values(&maps)

	if err == nil && num > 0 {
		for k, v := range maps {
			key := "package_" + strconv.Itoa(k)
			result[key] = v
		}
	} else {
		result["status"] = nil
	}
	return result
}

func GetItemsPackages(ItemCode string, _Orgid string) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params

	num, err := o.Raw("SELECT pac.pk_id, pac.item_org_assoc_fkid, pac.packet_size, pac.price, pac.status, pac.date_added, pac.date_removed, pac._orgid, ioa.unit_measure from packaging  pac , item_org_assoc ioa where pac.item_org_assoc_fkid = ioa._id and pac._orgid =? and pac.item_org_assoc_fkid =? and pac.status = 1", _Orgid, ItemCode).Values(&maps)
	if err == nil && num > 0 {
		for k, v := range maps {
			key := "package_" + strconv.Itoa(k)
			result[key] = v
		}
	}
	return result
}

func DeletePackage(pkg_id string, pkg_removedDate string, orgid string) string {
	result := "false"
	o := orm.NewOrm()
	_, err := o.Raw("UPDATE packaging SET status=?, date_removed=? WHERE _orgid=? and pk_id =? ", "0", pkg_removedDate, orgid, pkg_id).Exec()
	if err != nil {
	} else {
		result = "true"
	}
	return result
}

// END of Packaging Functions

//ACTIVE : Used to items based on factory selected
func GetItemsByFactory(factoryId string, orgId string) ([]string, []string, []string, []string) {
	var itemId []string
	var itemCode []string
	var itemUom []string
	var itemNarration []string
	o := orm.NewOrm()
	var maps []orm.Params
	// num, err := o.Raw("SELECT ioa._id, ioa.item_unique_code, ioa.unit_measure, ioa.invoice_narration FROM item_org_assoc as ioa, item_stock as ist WHERE _orgid=? and status=1 and FIND_IN_SET(?, geo_location) ", orgId, factoryId).Values(&maps)
	num, err := o.Raw("SELECT distinct(ioa._id), ioa.item_code_gen, ioa.unit_measure, ioa.invoice_narration FROM item_org_assoc as ioa, item_stock as ist WHERE ist._orgid=? and ioa._orgid=? and ist.geo_location=? and ist.latest=1 and ioa._id=ist.item_org_assoc_fkid and ioa.status=1", orgId, orgId, factoryId).Values(&maps)
	if num > 0 && err == nil {
		var new_itemId string
		var new_itemCode string
		var new_itemUom string
		var new_itemNarration string
		var j int64 = 0
		for ; j < num; j++ {
			new_itemId = maps[j]["_id"].(string)
			itemId = append(itemId, new_itemId)

			new_itemCode = maps[j]["item_code_gen"].(string)
			itemCode = append(itemCode, new_itemCode)

			if maps[j]["unit_measure"] != nil {
				new_itemUom = maps[j]["unit_measure"].(string)
			} else {
				new_itemUom = maps[j]["unit_measure"].(string)
			}
			itemUom = append(itemUom, new_itemUom)

			new_itemNarration = maps[j]["invoice_narration"].(string)
			itemNarration = append(itemNarration, new_itemNarration)
		}
	}
	return itemId, itemCode, itemUom, itemNarration
}

//ACTIVE : Used to fetch items for adjustment
func GetStockToAdjust(itemId string, factoryId string, orgId string) ([]string, []string, []string, []string, []string, []string, []string, []string, []string) {
	var stockId []string
	var quantity []string
	var rate []string
	var startDate []string
	var PurchaseOrProd []string
	var GrnNo []string
	var AddedQty []string
	var isEditable []string
	var thePlaceholder []string
	o := orm.NewOrm()
	var maps []orm.Params
	//GET ITEMS ADDED BY GRN, PRODUCTION AND ADD INVENTORY AND NOT CONSUMED AND NOT BLOCKED
	//num, err := o.Raw("SELECT ist._id as stockId, ist.quantity, ist.rate, ist.start_date, ist.insert_table_fkid,ist.insert_status,orgf.factory_location FROM organisation_factories as orgf, item_stock as ist WHERE ist._orgid= ? and ist.geo_location=? and orgf._id=ist.geo_location and ist.item_org_assoc_fkid=? and ist.latest= 1 and consumed_qty= 0 and blocked= 0", orgId, factoryId, itemId).Values(&maps)
	//Query below show all including blocked
	num, err := o.Raw("SELECT ist._id as stockId, ist.quantity, ist.rate, ist.start_date, ist.insert_table_fkid,ist.insert_status, ist.blocked, orgf.factory_location FROM organisation_factories as orgf, item_stock as ist WHERE ist._orgid= ? and ist.geo_location=? and orgf._id=ist.geo_location and ist.item_org_assoc_fkid=? and ist.latest= 1 and consumed_qty= 0 ", orgId, factoryId, itemId).Values(&maps)
	if num > 0 && err == nil {
		var new_stockId string
		var new_quantity string
		var new_rate string
		var new_startDate string
		var new_PurchaseOrProd string
		var new_GrnNo string
		var new_AddedQty string
		var new_isEditable string
		var new_thePlaceholder string
		var j int64 = 0
		for ; j < num; j++ {
			new_stockId = maps[j]["stockId"].(string)
			stockId = append(stockId, new_stockId)

			new_quantity = maps[j]["quantity"].(string)
			quantity = append(quantity, new_quantity)

			new_rate = maps[j]["rate"].(string)
			rate = append(rate, new_rate)

			if maps[j]["start_date"] != nil {
				new_startDate = maps[j]["start_date"].(string)
			} else {
				new_startDate = ""
			}
			startDate = append(startDate, new_startDate)

			if maps[j]["blocked"] == "0" {
				new_isEditable = ""
				new_thePlaceholder = ""
			} else {
				new_isEditable = "readonly"
				new_thePlaceholder = "Blocked"
			}
			isEditable = append(isEditable, new_isEditable)
			thePlaceholder = append(thePlaceholder, new_thePlaceholder)

			var maps1 []orm.Params
			new_PurchaseOrProd = ""
			new_GrnNo = ""
			new_AddedQty = ""
			inStatus := maps[j]["insert_status"].(string)
			insertStatus, _ := strconv.ParseInt(inStatus, 10, 64)
			if insertStatus == 4 {
				count, err1 := o.Raw("SELECT psg.grn_no_gen, pso.purchase_order_code_gen, psg.received_sent_quantity FROM po_so_items_with_grn as psg, purchase_service_order as pso WHERE psg._orgid=? and pso._orgid=? and psg._id=? and pso._id=psg.purchase_service_order_fkid", orgId, orgId, maps[j]["insert_table_fkid"]).Values(&maps1)
				if err1 == nil && count > 0 {
					if maps1[0]["grn_no_gen"] != nil {
						new_GrnNo = maps1[0]["grn_no_gen"].(string)
					}
					if maps1[0]["purchase_order_code_gen"] != nil {
						new_PurchaseOrProd = maps1[0]["purchase_order_code_gen"].(string)
					}
					if maps1[0]["received_sent_quantity"] != nil {
						new_AddedQty = maps1[0]["received_sent_quantity"].(string)
					}
				}
			} else if insertStatus == 1 {
				count, err1 := o.Raw("SELECT batch_no, qty FROM production_orders WHERE _orgid=? and _id=?", orgId, maps[j]["insert_table_fkid"]).Values(&maps1)
				if err1 == nil && count > 0 {
					if maps1[0]["batch_no"] != nil {
						new_PurchaseOrProd = maps1[0]["batch_no"].(string)
					}
					if maps1[0]["qty"] != nil {
						new_AddedQty = maps1[0]["qty"].(string)
					}
				}
			} else {
				if insertStatus == 2 || insertStatus == 3 {
					count, err1 := o.Raw("SELECT observed_quantity FROM item_stock_adjustment WHERE _orgid=? and _id=?", orgId, maps[j]["insert_table_fkid"]).Values(&maps1)
					if err1 == nil && count > 0 {
						if maps1[0]["observed_quantity"] != nil {
							new_AddedQty = maps1[0]["observed_quantity"].(string)
						}
					}
				}
			}
			PurchaseOrProd = append(PurchaseOrProd, new_PurchaseOrProd)
			GrnNo = append(GrnNo, new_GrnNo)
			AddedQty = append(AddedQty, new_AddedQty)
		}
	}
	return stockId, quantity, rate, startDate, PurchaseOrProd, GrnNo, AddedQty, isEditable, thePlaceholder
}

//--------------------------------Modular Functions for Reuse---------------------------------------//

//Active
func GetBOMItemsByProductId(product_id string, orgid string) (map[string]orm.Params, int64) {

	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT ioa._id,ioa.invoice_narration ,bom.quantity,bom.wastage,ioa.unit_measure FROM item_org_assoc as ioa,product_bom as bom where ioa._orgid=? and bom._orgid=? and bom.item_org_assoc_fkid=ioa._id and bom.item_product_org_assoc_fkid=? and bom.status=?", orgid, orgid, product_id, 1).Values(&maps)
	//fmt.Println("Count BOM=======", num)
	if err == nil && num > 0 {
		for k, v := range maps {
			key := "po_" + strconv.Itoa(k)
			result[key] = v
		}
	} else {
		//fmt.Println("Error in BOM retrival=======", err)
		result["status"] = nil
	}
	return result, num
}

func CheckExistingBOMByProductId(product_id string, orgid string) (int, int64) {

	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT ioa._id,ioa.invoice_narration ,bom.quantity,bom.wastage,ioa.unit_measure FROM item_org_assoc as ioa,product_bom as bom where ioa._orgid=? and bom._orgid=? and bom.item_org_assoc_fkid=ioa._id and bom.item_product_org_assoc_fkid=? and bom.status=?", orgid, orgid, product_id, 1).Values(&maps)

	if err != nil {
		return 1, 0
	} else if err == nil && num > 0 {
		return 0, num
	} else if err == nil && num == 0 {
		return 0, 0
	}
	return 1, 0
}

func GetItemsStockByPkId(id int64) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT * from item_stock where _id =?", id).Values(&maps)
	if err == nil && num > 0 {
		for k, v := range maps {
			key := "po_" + strconv.Itoa(k)
			result[key] = v
		}
	} else {
		result["status"] = nil
	}
	return result
}

func GetItemsTotalInventoryByOrgIdItemIdLocation(orgid string, itemid int64, location string) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT * FROM item_total_inventory WHERE _orgid=? and item_org_assoc_fkid=? and status=1 and geo_location=?", orgid, itemid, location).Values(&maps)
	if err == nil && num > 0 {
		for k, v := range maps {
			key := "po_" + strconv.Itoa(k)
			result[key] = v
		}
	} else {
		result["status"] = nil
	}
	return result
}

func GetItemInventory(item_id string, orgid string) (string, string) {
	o := orm.NewOrm()
	var maps3 []orm.Params
	num, err3 := o.Raw("SELECT iti.total_quantity, ioa.unit_measure FROM item_total_inventory as iti, item_org_assoc as ioa WHERE iti.item_org_assoc_fkid=? and iti._orgid=? and iti.status=1 and iti.item_org_assoc_fkid = ioa._id", item_id, orgid).Values(&maps3)

	var available_qty = ""
	var uom = ""
	if err3 == nil && num > 0 {
		available_qty = maps3[0]["total_quantity"].(string)
		uom = maps3[0]["unit_measure"].(string)
	}
	if available_qty == "" {
		available_qty = "0"
	}
	return available_qty, uom
}

func GetMaxItemCode(orgid string) int {
	var maxCode int
	maxCode = 0
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT max(item_unique_code) as maxcode FROM item_org_assoc where _orgid=?", orgid).Values(&maps)

	if num > 0 && err == nil {
		if maps[0]["maxcode"] != nil {
			code := maps[0]["maxcode"].(string)
			getValue, _ := strconv.ParseInt(code, 10, 64)
			maxCode = int(getValue)
		}
	}
	return maxCode
}

func GetItemsStockQuantityByItemIdLocation(itemid int64, location string, orgId string) float64 {
	var qty float64
	qty = 0
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT quantity from item_stock where item_org_assoc_fkid =? and geo_location=? and _orgid = ? and latest=1", itemid, location, orgId).Values(&maps)
	if err == nil && num > 0 {
		q := maps[0]["quantity"].(string)
		qty, _ = strconv.ParseFloat(q, 64)
	} else {
		qty = 0
	}
	return qty
}

func CheckForUniqueBatchNo(orgid string, batchNo string) int {
	o := orm.NewOrm()
	var stockmaps []orm.Params
	count, stockerr := o.Raw("SELECT _id FROM item_stock WHERE _orgid=? and batch_no=?", orgid, batchNo).Values(&stockmaps)
	if count > 0 && stockerr == nil {
		return 1 //batch no already exists
	}
	var maps []orm.Params
	num, err := o.Raw("SELECT _id FROM po_so_items_with_grn WHERE _orgid=? and batch_no=?", orgid, batchNo).Values(&maps)
	if num > 0 && err == nil {
		return 1 //batch no already exists
	}
	return 0 //batch no unique
}

func CheckForExistingGenCode(GenCode string, tabletype int, orgID string) int {
	o := orm.NewOrm()
	var resultMap []orm.Params
	if tabletype == 1 || tabletype == 2 { //item or product
		num, err := o.Raw("SELECT item_code_gen FROM item_org_assoc WHERE item_code_gen=? and  _orgid=? and status = 1", GenCode, orgID).Values(&resultMap)
		//fmt.Println("Query Check Code==========", num)
		if err == nil && num > 0 {
			return 1
		}
	} else if tabletype == 3 {
		num, err := o.Raw("SELECT supplier_code_gen FROM supplier_org_assoc WHERE supplier_code_gen=? and  _orgid=? and status = 1", GenCode, orgID).Values(&resultMap)
		//fmt.Println("Query Check Code==========", num)
		if err == nil && num > 0 {
			return 1
		}
	} else if tabletype == 4 {
		num, err := o.Raw("SELECT customer_code_gen FROM customer WHERE customer_code_gen=? and  _orgid=? and status = 1", GenCode, orgID).Values(&resultMap)
		//fmt.Println("Query Check Code==========", num)
		if err == nil && num > 0 {
			return 1
		}
	}
	return 0
}

func GetOrgFactoriesIncludingDisabled(_orgId string) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT * FROM organisation_factories WHERE _orgid=? ", _orgId).Values(&maps)
	if err == nil && num > 0 {
		for k, v := range maps {

			key := "org_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetItemCode(item_id string, orgId string) string {

	itemCode := ""

	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT _id, item_code_gen, item_unique_code FROM item_org_assoc WHERE _id=? and _orgid=?", item_id, orgId).Values(&maps)

	if num > 0 && err == nil {
		itemCode = maps[0]["item_code_gen"].(string)
	}

	return itemCode
}

func GetItemName(item_id string, orgId string) string {

	itemName := ""

	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT name FROM item_org_assoc WHERE _id=? and _orgid=?", item_id, orgId).Values(&maps)

	if num > 0 && err == nil {
		itemName = maps[0]["name"].(string)
	}

	return itemName
}
