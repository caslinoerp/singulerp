package models

import (
	"strconv"
	"time"

	"github.com/astaxie/beego/orm"
)

func GetDashboardSales(orgid string) map[string]string {

	result := make(map[string]string)

	result["newSales"] = strconv.Itoa(GetOpenOrderSales(orgid))
	result["completedSales"] = strconv.Itoa(GetCompletedSales(orgid))
	result["revenue"] = strconv.FormatFloat(GetCompletedSalesRevenue(orgid), 'f', -1, 64)

	return result
}

func GetDashboardPO(orgid string) map[string]string {

	result := make(map[string]string)

	result["newPurchases"] = strconv.Itoa(GetOpenPurchases(orgid))
	result["completedPurchases"] = strconv.Itoa(GetCompletedPurchases(orgid))
	result["expenses"] = strconv.FormatFloat(GetCompletedPurchasesExpenses(orgid), 'f', -1, 64)

	return result
}

func GetDashboardProd(orgid string) map[string]string {

	result := make(map[string]string)

	result["openProduction"] = strconv.Itoa(GetOpenProduction(orgid))
	result["completedProduction"] = strconv.Itoa(GetCompletedProduction(orgid))
	result["inProduction"] = strconv.Itoa(GetInProduction(orgid))

	return result
}

func GetDashboardUnderProd(orgid string) map[string]orm.Params {

	result := make(map[string]orm.Params)

	o := orm.NewOrm()
	var maps []orm.Params
	//num, err := o.Raw("SELECT * FROM item_master ").Values(&maps)
	num, err := o.Raw("SELECT count(_id) as under_production FROM production_orders WHERE _orgid=? and status=? and order_date > ?-15", orgid, "2", time.Now()).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		// var i int64 = 0
		// for ; i < num; i++ {
		// 	fmt.Println(maps[i])
		// }
		for k, v := range maps {
			key := "underproduction_" + strconv.Itoa(k)
			result[key] = v
		}
	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetDashboardPendingDispatch(orgid string) map[string]orm.Params {

	result := make(map[string]orm.Params)

	o := orm.NewOrm()
	var maps []orm.Params
	//num, err := o.Raw("SELECT * FROM item_master ").Values(&maps)
	num, err := o.Raw("SELECT count(sales_order._id) as pending_dispatch FROM sales_order, sales_order_status WHERE sales_order._orgid=? and sales_order_status._orgid=? and sales_order_status.sales_order_fkid=sales_order._id and sales_order_status.status=? and order_date > ?-15", orgid, orgid, "6", time.Now()).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		// var i int64 = 0
		// for ; i < num; i++ {
		// 	fmt.Println(maps[i])
		// }
		for k, v := range maps {
			key := "pendingdispatch_" + strconv.Itoa(k)
			result[key] = v
		}
	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetDashboardInTransit(orgid string) map[string]string {

	result := make(map[string]string)

	result["pendingDispatch"] = strconv.Itoa(GetPendingDispatch(orgid))
	result["inTransit"] = strconv.Itoa(GetInTransit(orgid))
	result["Dispatched"] = strconv.Itoa(GetCompletedSales(orgid))

	return result

}

func GetLowLevels(orgid string) map[string]orm.Params {

	result := make(map[string]orm.Params)

	o := orm.NewOrm()
	var maps []orm.Params

	num, err := o.Raw("SELECT ioa.invoice_narration, ioa.item_code_gen, iti.total_quantity,ioa.unit_measure, of.factory_location as geolocation	FROM item_total_inventory as iti,item_org_assoc as ioa , organisation_factories as of where	ioa._orgid=? and iti._orgid=? and of._orgid = ? and	iti.item_org_assoc_fkid=ioa._id and iti.status=1 and iti.low_level=1 and iti.geo_location = of._id ORDER BY iti.last_updated DESC LIMIT 10", orgid, orgid, orgid).Values(&maps)
	if err == nil && num > 0 {

		for k, v := range maps {
			key := "levels_" + strconv.Itoa(k)
			result[key] = v
		}
	} else {

		result["status"] = nil
	}

	return result
}

func GetDelayedPOs(orgid string) map[string]orm.Params {

	result := make(map[string]orm.Params)

	o := orm.NewOrm()
	var maps []orm.Params
	//num, err := o.Raw("SELECT * FROM item_master ").Values(&maps)
	num, err := o.Raw("SELECT soa.company_name, pso._id, pso.purchase_order_code_gen, pso.unique_serial_no, pso.delivery_date as delivery_date FROM purchase_service_order as pso, supplier_org_assoc as soa WHERE pso._orgid=? and soa._orgid=? and pso.supplier_fkid=soa._id and pso.delivery_date<? and status != 5 ORDER BY pso.delivery_date ASC LIMIT 10 ", orgid, orgid, GetTodaysDate()).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		// var i int64 = 0
		// for ; i < num; i++ {
		// 	fmt.Println(maps[i])
		// }
		for k, v := range maps {
			if maps[k]["delivery_date"] != "" && maps[k]["delivery_date"] != nil {
				maps[k]["delivery_date"] = convertDateString(maps[k]["delivery_date"].(string))
			}

			key := "pos_" + strconv.Itoa(k)
			result[key] = v
		}
	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetDelayedProduction(orgid string) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params

	num, err := o.Raw("SELECT po._id, po.batch_no, po.production_order_unique_code, po.qty, po.end_date , ioa.item_code_gen, ioa.invoice_narration FROM production_orders as po, item_org_assoc as ioa WHERE po._orgid=? and ioa._orgid=? and ioa._id=po.item_org_assoc_fkid and po.end_date<? and (po.status=2 or po.status=5) ORDER BY po.end_date ASC LIMIT 10", orgid, orgid, GetTodaysDate()).Values(&maps)
	if err == nil && num > 0 {

		for k, v := range maps {
			if maps[k]["end_date"] != "" && maps[k]["end_date"] != nil {
				maps[k]["end_date"] = convertDateString(maps[k]["end_date"].(string))
			}
			key := "prod_" + strconv.Itoa(k)
			result[key] = v
		}
	} else {
		result["status"] = nil
	}
	return result
}

func GetDelayedSales(orgid string) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params

	num, err := o.Raw("SELECT so._id, so.order_no, so.delivery_date, c.company_name FROM sales_order as so, customer as c WHERE so._orgid=? and c._orgid=? and c._id=so.customer_fkid and so.delivery_date<? ORDER BY so.delivery_date ASC LIMIT 10", orgid, orgid, GetTodaysDate()).Values(&maps)
	if err == nil && num > 0 {

		for k, v := range maps {
			if maps[k]["delivery_date"] != "" && maps[k]["delivery_date"] != nil {
				maps[k]["delivery_date"] = convertDateString(maps[k]["delivery_date"].(string))
			}
			key := "sales_" + strconv.Itoa(k)
			result[key] = v
		}
	} else {

		result["status"] = nil
	}
	return result
}

//==================================NEW FUNCTIONS===========================================//

func GetOpenOrderSales(orgid string) int {

	var openSales int
	openSales = 0
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT count(_id) as new_sales FROM sales_order WHERE _orgid=? and status <> 8", orgid).Values(&maps)

	if num > 0 && err == nil {
		if maps[0]["new_sales"] != nil {
			code := maps[0]["new_sales"].(string)
			getValue, _ := strconv.ParseInt(code, 10, 64)
			openSales = int(getValue)
		}
	}
	return openSales

}

func GetCompletedSales(orgid string) int {

	var completed int
	completed = 0
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT count(_id) as completed FROM sales_order WHERE _orgid=? and status = 8 and last_updated_date BETWEEN CURDATE() - INTERVAL 30 DAY AND CURDATE()", orgid).Values(&maps)

	if num > 0 && err == nil {
		if maps[0]["completed"] != nil {
			code := maps[0]["completed"].(string)
			getValue, _ := strconv.ParseInt(code, 10, 64)
			completed = int(getValue)
		}
	}
	return completed

}

func GetCompletedSalesRevenue(orgid string) float64 {

	var revenue float64
	revenue = 0.0
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT sum(total_sales_price) as total_price FROM sales_order WHERE _orgid=? and status = 8", orgid).Values(&maps)

	if num > 0 && err == nil {
		if maps[0]["total_price"] != nil {
			code := maps[0]["total_price"].(string)
			revenue, _ = strconv.ParseFloat(code, 64)
		}
	}
	return revenue
}

func GetOpenPurchases(orgid string) int {

	var openPurchases int
	openPurchases = 0
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT count(_id) as openPurchases FROM purchase_service_order WHERE _orgid=? and status <> 8", orgid).Values(&maps)

	if num > 0 && err == nil {
		if maps[0]["openPurchases"] != nil {
			code := maps[0]["openPurchases"].(string)
			getValue, _ := strconv.ParseInt(code, 10, 64)
			openPurchases = int(getValue)
		}
	}
	return openPurchases

}

func GetCompletedPurchases(orgid string) int {

	var completed int
	completed = 0
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT count(_id) as completed FROM purchase_service_order WHERE _orgid=? and status = 8 and delivery_date BETWEEN CURDATE() - INTERVAL 30 DAY AND CURDATE()", orgid).Values(&maps)

	if num > 0 && err == nil {
		if maps[0]["completed"] != nil {
			code := maps[0]["completed"].(string)
			getValue, _ := strconv.ParseInt(code, 10, 64)
			completed = int(getValue)
		}
	}
	return completed

}

func GetCompletedPurchasesExpenses(orgid string) float64 {

	var expense float64
	expense = 0.0
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT sum(total_invoice_value) as total_price FROM purchase_service_order WHERE _orgid=? and status = 8", orgid).Values(&maps)

	if num > 0 && err == nil {
		if maps[0]["total_price"] != nil {
			code := maps[0]["total_price"].(string)
			expense, _ = strconv.ParseFloat(code, 64)
		}
	}
	return expense
}

func GetOpenProduction(orgid string) int {

	var openProduction int
	openProduction = 0
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT count(_id) as openProduction FROM production_orders WHERE _orgid=? and status in (0,1,5)", orgid).Values(&maps)

	if num > 0 && err == nil {
		if maps[0]["openProduction"] != nil {
			code := maps[0]["openProduction"].(string)
			getValue, _ := strconv.ParseInt(code, 10, 64)
			openProduction = int(getValue)
		}
	}
	return openProduction

}

func GetInProduction(orgid string) int {

	var inProduction int
	inProduction = 0
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT count(_id) as inProduction FROM production_orders WHERE _orgid=? and status = 2", orgid).Values(&maps)

	if num > 0 && err == nil {
		if maps[0]["inProduction"] != nil {
			code := maps[0]["inProduction"].(string)
			getValue, _ := strconv.ParseInt(code, 10, 64)
			inProduction = int(getValue)
		}
	}
	return inProduction

}

func GetCompletedProduction(orgid string) int {

	var completed int
	completed = 0
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT count(_id) as completed FROM production_orders WHERE _orgid=? and status = 3 and end_date BETWEEN CURDATE() - INTERVAL 30 DAY AND CURDATE()", orgid).Values(&maps)

	if num > 0 && err == nil {
		if maps[0]["completed"] != nil {
			code := maps[0]["completed"].(string)
			getValue, _ := strconv.ParseInt(code, 10, 64)
			completed = int(getValue)
		}
	}
	return completed

}

func GetInTransit(orgid string) int {

	var inTransit int
	inTransit = 0
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT count(_id) as inTransit FROM sales_order WHERE _orgid=? and status = 7", orgid).Values(&maps)

	if num > 0 && err == nil {
		if maps[0]["inTransit"] != nil {
			code := maps[0]["inTransit"].(string)
			getValue, _ := strconv.ParseInt(code, 10, 64)
			inTransit = int(getValue)
		}
	}
	return inTransit

}

func GetPendingDispatch(orgid string) int {

	var pending int
	pending = 0
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT count(_id) as pending FROM sales_order WHERE _orgid=? and status = 6", orgid).Values(&maps)

	if num > 0 && err == nil {
		if maps[0]["pending"] != nil {
			code := maps[0]["pending"].(string)
			getValue, _ := strconv.ParseInt(code, 10, 64)
			pending = int(getValue)
		}
	}
	return pending

}

//==================================== END ================================================//
