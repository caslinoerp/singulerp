package models

import (
	"fmt"

	//"fmt"
	//"strings"

	"strconv"
	"strings"
	"time"

	"github.com/astaxie/beego/orm"
)

type Product struct {
	ProductName      string
	InvoiceNarration string
	ProductCode      string
	ProductType      string
	Group            string
	SubGroup         string
	Capacity         string
	UOM              string
	RatePerUOM       string
	MinStock         string
	Transport        string
	ProductProcess   string
	Status1          string
	Orgid            string
	Userid           string
	Username         string
	ProductID        string
	ProductCheck     string
	IsPresentinDB    int
	FactoryLoc       []string
	FactoryLoc1      string
	NewGenCode       string
}

type BomImport struct {
	Orgid         string
	ProductCode   string
	ItemCode      string
	Quantity      string
	Wastage       string
	NonConsumable string
	Status1       string
}

type ModifyBom struct {
	Orgid       string
	Userid      string
	Username    string
	ProductName string
	ProductCode string
	ItemName    string
	Quantity    string
	Wastage     string
}

type ProductQueue struct {
	Orgid    string
	Userid   string
	Username string
	ProductQ string
	Status   string
	Type     string
}

type ProductionOrder struct {
	BatchNo             string
	OrderDate           string
	Product             string
	Quantity            string
	SDate               string
	EDate               string
	Machine             string
	FactoryLoc          string
	St                  string
	Orgid               string
	Userid              string
	Username            string
	UniqueCode          string
	TemporaryBOMUsed    string
	ManuallyAssignBatch string
	ShowBom             string
}

type Items struct {
	BomProductCode string
	BomProductName string
	Orgid          string
	Userid         string
	Username       string
	ItemName1      string
	Qty1           string
	Wastage1       string
	ItemName2      string
	Qty2           string
	Wastage2       string
	ItemName3      string
	Qty3           string
	Wastage3       string
	ItemName4      string
	Qty4           string
	Wastage4       string
	ItemName5      string
	Qty5           string
	Wastage5       string
	ItemName6      string
	Qty6           string
	Wastage6       string
	ItemName7      string
	Qty7           string
	Wastage7       string
	ItemName       string
	Qty            string
	Wastage        string
	Nc1            string
	NC2            string
	NC3            string
	NC4            string
	NC5            string
	NC6            string
	NC7            string
	NC             string
	ItemCodeType   string
}

type Machine struct {
	Orgid         string
	Userid        string
	Username      string
	PrevId        string
	MachineName   string
	AddedOn       string
	Status        string
	MachineCode   string
	GeoLocation   string
	MachineID     string
	IsPresentinDB int
	NewGenCode    string
}

type JobcardM struct {
	Id                          int64
	JobcardNo                   string
	JobcardDate                 string
	Purchase_service_order_fkid string
	OrderDate                   string
	InvoiceNo                   string
	InvoiceDate                 string
	Customer_name               string
	Sr_no                       string
	Status                      string
	QCDate                      string
	SpecialTest                 string
	MachineId                   string
	FiringSystem                string
	MaxTemp                     string
	RemTemp                     string
	PreCheck                    string
	Operator                    string
	InspectedBy                 string
	InspectionDate              string
	Notes                       string
	Orgid                       string
	Userid                      string
	Username                    string
}

type ProductionOrderItem struct {
	ProductionOrderFkID int
	ItemOrgAssocFkID    int
	Quantity            float64
	Wastage             float64
	UOM                 string
	Location            string
	OrgID               string
	Status              int
	BomStatus           int
}

func CheckForExistingMachineName(machineName string, orgID string) int {
	o := orm.NewOrm()
	var resultMap []orm.Params
	num, err := o.Raw("SELECT machine_name FROM machine WHERE machine_name=? and  _orgid=? and status = 1", machineName, orgID).Values(&resultMap)
	//fmt.Println("Query Check Name==========", num)
	if err == nil && num > 0 {
		return 1
	}
	return 0
}

func CheckForExistingMachineCode(machineCode string, orgID string) int {
	o := orm.NewOrm()
	var resultMap []orm.Params
	num, err := o.Raw("SELECT machine_code FROM machine WHERE machine_code=? and  _orgid=? and status = 1", machineCode, orgID).Values(&resultMap)
	//fmt.Println("Query Check Code==========", num)
	if err == nil && num > 0 {
		return 1
	}
	return 0
}

func GetAllMachine(formData Machine) map[string]orm.Params {
	// var stock []Stock
	// o := orm.NewOrm()
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	//num, err := o.Raw("SELECT * FROM item_master ").Values(&maps)
	num, err := o.Raw("SELECT m._pkid, m.machine_name as machine_name, m.machine_code, m.machine_code_gen, m.machine_status, m.added_on as ad_on, o.factory_location as factory_location from machine m, organisation_factories o where m._orgid =? and m.status=1 and o._orgid=? and m.geo_location=o._id ", formData.Orgid, formData.Orgid).Values(&maps)
	if err == nil && num > 0 {
		for k, v := range maps {
			if maps[k]["ad_on"] != nil && maps[k]["ad_on"] != "" {
				maps[k]["ad_on"] = convertDateString(maps[k]["ad_on"].(string))
			}

			key := "stock_" + strconv.Itoa(k)
			result[key] = v
		}
	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func AddNewMachine(formData Machine) int {
	o := orm.NewOrm()
	_, err := o.Raw("INSERT INTO machine (machine_name, _orgid, machine_code, added_on, geo_location,status, machine_status, machine_code_gen) VALUES (?,?,?,?,?,?,?,?) ", formData.MachineName, formData.Orgid, formData.MachineCode, formData.AddedOn, formData.GeoLocation, "1", formData.Status, formData.NewGenCode).Exec()

	if err == nil {
		return 1
	}
	return 0
}

func UpdateMachine(formData Machine) int {
	o := orm.NewOrm()
	ormTransactionalErr := o.Begin()
	_, err0 := o.Raw("UPDATE machine SET status=? WHERE _pkid=?", "0", formData.PrevId).Exec()
	if err0 != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}
	_, err := o.Raw("INSERT INTO machine (machine_name, _orgid, machine_code, added_on, geo_location,status, machine_status) VALUES (?,?,?,?,?,?,?) ", formData.MachineName, formData.Orgid, formData.MachineCode, formData.AddedOn, formData.GeoLocation, "1", formData.Status).Exec()
	if err != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}
	ormTransactionalErr = o.Commit()
	if ormTransactionalErr != nil {
		return 0
	}
	return 1
}

func Jobcard(formData JobcardM) int {

	o := orm.NewOrm()
	_, err := o.Raw("INSERT INTO production_job_card (jobcard_no, jobcard_date, purchase_service_order_fkid, order_date, invoice_no, invoice_date, customer_name, serial_no_fkuniqueSrno, status, QC_date, special_test, machine_id, firing_system, max_oven_temp, removal_temp, preloading_check, operator, inspected_by, inspection_date,notes,_orgid) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ", formData.JobcardNo, formData.JobcardDate, formData.Purchase_service_order_fkid, formData.OrderDate, formData.InvoiceNo, formData.InvoiceDate, formData.Customer_name, formData.Sr_no, formData.Status, formData.QCDate, formData.SpecialTest, formData.MachineId, formData.FiringSystem, formData.MaxTemp, formData.RemTemp, formData.PreCheck, formData.Operator, formData.InspectedBy, formData.InspectionDate, formData.Notes, formData.Orgid).Exec()
	if err == nil {
		return 1
	}
	return 0

}

func GetPrevMachine(prevId string, orgID string) (string, string) {
	o := orm.NewOrm()
	var resultMap []orm.Params
	num, err := o.Raw("SELECT machine_name, machine_code FROM machine WHERE _pkid=? and  _orgid=? ", prevId, orgID).Values(&resultMap)
	//fmt.Println("Query Check Name==========", num)
	var machineName = ""
	var machineCode = ""
	if err == nil && num > 0 {
		machineName = resultMap[0]["machine_name"].(string)
		machineCode = resultMap[0]["machine_code"].(string)
	}
	return machineName, machineCode
}

func CheckForExistingProductName(productName string, orgID string) int {
	o := orm.NewOrm()
	var resultMap []orm.Params
	num, err := o.Raw("SELECT product_org_name FROM product_org_assoc WHERE product_org_name=? and  _orgid=? and status = 1", productName, orgID).Values(&resultMap)
	//fmt.Println("Query Check Name==========", num)
	if err == nil && num > 0 {
		return 1
	}
	return 0
}

func CheckForExistingProductUniqueCode(productUniqueCode string, orgID string) int {
	o := orm.NewOrm()
	var resultMap []orm.Params
	num, err := o.Raw("SELECT product_org_code FROM product_org_assoc WHERE product_org_code=? and  _orgid=? and status = 1", productUniqueCode, orgID).Values(&resultMap)
	//fmt.Println("Query Check Code==========", num)
	if err == nil && num > 0 {
		return 1
	}
	return 0
}

func CreateNewProduct(formData Product) int {

	o := orm.NewOrm()

	var inv_narration *string
	inv_narration = nil
	if formData.InvoiceNarration != "" {
		tempInNarr := formData.InvoiceNarration
		inv_narration = &tempInNarr
	}
	var rate_per_uom *string
	rate_per_uom = nil
	if formData.RatePerUOM != "" {
		tempRateUOM := formData.RatePerUOM
		rate_per_uom = &tempRateUOM
	}

	var factoryLoc string
	var maps []orm.Params

	// num, err := o.Raw("SELECT _id FROM organisation_factories WHERE _orgid=? and factory_location=?", formData.Orgid, formData.FactoryLoc).Values(&maps)
	// if err == nil && num > 0 {
	// 	factoryLoc = maps[0]["_id"].(string)
	// }
	for i := 0; i < len(formData.FactoryLoc); i++ {

		num, err0 := o.Raw("SELECT _id FROM organisation_factories WHERE factory_location=? and  _orgid=? and status = 1 ", formData.FactoryLoc[i], formData.Orgid).Values(&maps)
		if err0 == nil && num > 0 {
			//fmt.Println("ID", maps[0]["_id"])
			if len(formData.FactoryLoc) == 1 {
				factoryLoc = maps[0]["_id"].(string)
			} else {
				factoryLoc = factoryLoc + maps[0]["_id"].(string) + ","
			}
		} else {

		}

	}

	_, err5 := o.Raw("INSERT INTO product_org_assoc (product_org_code, product_org_name, _orgid, invoice_narration, rate_per_UOM, minimum_stock_keep, status, grp, subgroup, unit_of_measure, capacity_measure, is_finished_product, transport, process_name,product_type, geo_location) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ", formData.ProductCode, formData.ProductName, formData.Orgid, &inv_narration, &rate_per_uom, formData.MinStock, formData.Status1, formData.Group, formData.SubGroup, formData.UOM, formData.Capacity, 1, formData.Transport, formData.ProductProcess, formData.ProductType, factoryLoc).Exec()
	if err5 == nil {
		return 1
	}
	return 0
}

func NewProductImport(formData Product) int {

	o := orm.NewOrm()

	var inv_narration *string
	inv_narration = nil
	if formData.InvoiceNarration != "" {
		tempInNarr := formData.InvoiceNarration
		inv_narration = &tempInNarr
	}
	var rate_per_uom *string
	rate_per_uom = nil
	if formData.RatePerUOM != "" {
		tempRateUOM := formData.RatePerUOM
		rate_per_uom = &tempRateUOM
	}

	var factoryLoc string
	var maps []orm.Params
	var maps2 []orm.Params

	var tags1 []string
	tags1 = strings.Split(formData.FactoryLoc1, ",")
	for i := 0; i < len(tags1); i++ {
		if tags1[i] == "" {

		} else {
			num, err01 := o.Raw("SELECT _id FROM organisation_factories WHERE factory_location=? and _orgid=? and status=1", tags1[i], formData.Orgid).Values(&maps)
			if err01 == nil && num > 0 {
				if len(tags1) == 1 {
					factoryLoc = maps[0]["_id"].(string)
				} else {
					factoryLoc = factoryLoc + maps[0]["_id"].(string) + ","
				}
			} else {
				_, err1 := o.Raw("INSERT INTO organisation_factories (_orgid, factory_location, added_on, status) VALUES (?,?,?,?)", formData.Orgid, tags1[i], time.Now(), "1").Exec()

				count, err2 := o.Raw("SELECT _id FROM organisation_factories WHERE factory_location=? and  _orgid=? and status = 1 ", tags1[i], formData.Orgid).Values(&maps2)
				if err1 == nil && err2 == nil && count > 0 {

					if len(tags1) == 1 {
						factoryLoc = maps2[0]["_id"].(string)
					} else {
						factoryLoc = factoryLoc + maps2[0]["_id"].(string) + ","
					}

				}

			}
		}
	}

	_, err5 := o.Raw("INSERT INTO product_org_assoc (product_org_code, product_org_name, _orgid, invoice_narration, rate_per_UOM, minimum_stock_keep, status, grp, subgroup, unit_of_measure, capacity_measure, is_finished_product, transport, process_name,product_type, geo_location) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ", formData.ProductCode, formData.ProductName, formData.Orgid, &inv_narration, &rate_per_uom, formData.MinStock, formData.Status1, formData.Group, formData.SubGroup, formData.UOM, formData.Capacity, 1, formData.Transport, formData.ProductProcess, formData.ProductType, factoryLoc).Exec()
	if err5 == nil {
		return 1
	}
	return 0
}

func GetProductItems(product_id string, orgid string) ([]string, []string, []string, []string, string) {

	status := "false"
	var items []string
	var quantity []string
	var wastage []string
	var consume []string
	o := orm.NewOrm()
	var maps []orm.Params
	// num, err := o.Raw("SELECT product_bom.non_consumable,product_bom.product_narration, product_bom.quantity,product_bom.wastage FROM product_bom WHERE item_product_org_assoc_fkid=? and product_bom._orgid=?  ", product_id, orgid).Values(&maps)

	num, err := o.Raw(`SELECT CONCAT("[",itemOrg.item_code_gen,"] ",itemOrg.name) as product_narration,  product_bom.non_consumable, product_bom.quantity,product_bom.wastage FROM product_bom , item_org_assoc as itemOrg WHERE item_product_org_assoc_fkid=? and product_bom._orgid=? and itemOrg._orgid = ? and itemOrg._id = item_org_assoc_fkid `, product_id, orgid, orgid).Values(&maps)

	//fmt.Println("num=", num)
	var i int64 = 0
	for ; i < num; i++ {
		//fmt.Println(maps[i])
	}
	if err == nil {
		var new_items string
		var new_quantity string
		var new_wastage string
		var new_consume string
		var j int64 = 0
		for ; j < num; j++ {
			new_items = maps[j]["product_narration"].(string)
			items = append(items, new_items)

			if maps[j]["quantity"] != nil {
				new_quantity = maps[j]["quantity"].(string)
			} else {
				new_quantity = "0"
			}
			quantity = append(quantity, new_quantity)

			if maps[j]["wastage"] != nil {
				new_wastage = maps[j]["wastage"].(string)
			} else {
				new_wastage = "0"
			}
			wastage = append(wastage, new_wastage)

			if maps[j]["non_consumable"] == "1" {
				maps[j]["non_consumable"] = "non_consumable"
			} else {
				maps[j]["non_consumable"] = "consumable"
			}

			new_consume = maps[j]["non_consumable"].(string)
			consume = append(consume, new_consume)

		}
		status = "true"
	}
	//fmt.Println("***items=", items)
	//fmt.Println("***quantity=", quantity)
	return items, quantity, wastage, consume, status
}

func UniqueCheck(formData Product) int {
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT _id FROM product_org_assoc WHERE product_org_code=?", formData.ProductCode).Values(&maps)
	//fmt.Println("num:",num)
	if err != nil || num > 0 {
		//fmt.Println("Returns 1")
		return 0
	}
	return 1
}

func NewImportBomProduct(formData BomImport) int {
	o := orm.NewOrm()
	var maps []orm.Params

	var wastage *string
	wastage = nil
	if formData.Wastage != "" {
		tempwastage := formData.Wastage
		wastage = &tempwastage
	}

	var non_consumable string
	if formData.NonConsumable == "1" {
		non_consumable = "1"
	} else {
		non_consumable = "0"
	}

	//product_id
	num, err := o.Raw("SELECT _id FROM item_org_assoc WHERE item_unique_code=? and _orgid=? and status=1", formData.ProductCode, formData.Orgid).Values(&maps)
	if num > 0 && err == nil {
		var maps2 []orm.Params
		//item_id
		count, err01 := o.Raw("SELECT _id, non_consumable, invoice_narration FROM item_org_assoc WHERE item_unique_code=? and _orgid=? and status=1", formData.ItemCode, formData.Orgid).Values(&maps2)
		if count > 0 && err01 == nil {
			var bom_maps []orm.Params
			bom_num, err2 := o.Raw("SELECT _id FROM product_bom WHERE item_product_org_assoc_fkid=? and item_org_assoc_fkid=? and _orgid=? and status=1", maps[0]["_id"], maps2[0]["_id"], formData.Orgid).Values(&bom_maps)
			if bom_num > 0 && err2 == nil {
				return 4
			}
			_, err1 := o.Raw("INSERT INTO product_bom (item_product_org_assoc_fkid, item_org_assoc_fkid, quantity, wastage, status, _orgid, product_narration, non_consumable) VALUES (?,?,?,?,?,?,?,?) ", maps[0]["_id"], maps2[0]["_id"], formData.Quantity, &wastage, "1", formData.Orgid, maps2[0]["invoice_narration"], non_consumable).Exec()
			if err1 == nil {
				return 1
			}
		} else {
			return 3
		}
	} else {
		return 2
	}

	return 0
}

//the original BOM function
//this is still used in production
//this temporary function need to be removed and necessory changes should be made to production controller to have one common BOM function
func BillofMaterial(formData Items) int {
	o := orm.NewOrm()
	var maps []orm.Params
	_, err := o.Raw("SELECT _id,invoice_narration FROM item_org_assoc WHERE item_code_gen=? and name=? and _orgid=?", formData.BomProductCode, formData.BomProductName, formData.Orgid).Values(&maps)
	//_, err := o.Raw("SELECT _id,invoice_narration FROM item_org_assoc WHERE item_code_gen=? and name=? and _orgid=?", formData.BomProductCode, formData.BomProductName, formData.Orgid).Values(&maps)
	p_id := maps[0]["_id"]

	//fmt.Println("------p_id is ",p_id)
	//for i:=1; i<8; i++{
	//item_key := "ItemName" + strconv.Itoa(i)
	if formData.ItemName1 != "" {
		_, err01 := o.Raw("SELECT _id,invoice_narration,item_unique_code FROM item_org_assoc WHERE invoice_narration=? and _orgid=? ", formData.ItemName1, formData.Orgid).Values(&maps)
		//a := strconv.Itoa(Itemid1)
		//Itemid1 := maps[0]["_id"]
		//fmt.Println("********Itemid1 is ", Itemid1)
		//err_key := "err" + strconv.Itoa(i)
		//qty_key := "Qty" + strconv.Itoa(i)
		if formData.Wastage1 == "" {
			formData.Wastage1 = "0.0"
		}
		if formData.Nc1 == "on" {
			formData.Nc1 = "1"
		} else {
			formData.Nc1 = "0"
		}
		_, err1 := o.Raw("INSERT INTO product_bom (item_product_org_assoc_fkid, item_org_assoc_fkid,product_narration,quantity, status, wastage, _orgid,non_consumable) VALUES (?,?,?,?,?,?,?,?) ", p_id, maps[0]["_id"], maps[0]["invoice_narration"], formData.Qty1, "1", formData.Wastage1, formData.Orgid, formData.Nc1).Exec()
		if err01 != nil || err1 != nil {
			//fmt.Println("Error in Item1 queries")
		}
	}
	if formData.ItemName2 != "" {
		if formData.Wastage2 == "" {
			formData.Wastage2 = "0.0"
		}
		if formData.NC2 == "on" {
			formData.NC2 = "1"
		} else {
			formData.NC2 = "0"
		}
		_, err02 := o.Raw("SELECT _id,invoice_narration,item_unique_code FROM item_org_assoc WHERE invoice_narration=? and _orgid=?", formData.ItemName2, formData.Orgid).Values(&maps)
		_, err2 := o.Raw("INSERT INTO product_bom (item_product_org_assoc_fkid, item_org_assoc_fkid,product_narration,quantity, status,wastage, _orgid,non_consumable) VALUES (?,?,?,?,?,?,?,?) ", p_id, maps[0]["_id"], maps[0]["invoice_narration"], formData.Qty2, "1", formData.Wastage2, formData.Orgid, formData.NC2).Exec()
		if err02 != nil || err2 != nil {
			//fmt.Println("Error in Item2 queries")
		}
	}
	if formData.ItemName3 != "" {
		if formData.Wastage3 == "" {
			formData.Wastage3 = "0.0"
		}
		if formData.NC3 == "on" {
			formData.NC3 = "1"
		} else {
			formData.NC3 = "0"
		}
		_, err03 := o.Raw("SELECT _id,invoice_narration,item_unique_code FROM item_org_assoc WHERE invoice_narration=? and _orgid=?", formData.ItemName3, formData.Orgid).Values(&maps)
		_, err3 := o.Raw("INSERT INTO product_bom (item_product_org_assoc_fkid, item_org_assoc_fkid,product_narration,quantity, status,wastage, _orgid,non_consumable) VALUES (?,?,?,?,?,?,?,?) ", p_id, maps[0]["_id"], maps[0]["invoice_narration"], formData.Qty3, "1", formData.Wastage3, formData.Orgid, formData.NC3).Exec()
		if err03 != nil || err3 != nil {
			//fmt.Println("Error in Item3 queries")
		}
	}
	if formData.ItemName4 != "" {
		if formData.Wastage4 == "" {
			formData.Wastage4 = "0.0"
		}
		if formData.NC4 == "on" {
			formData.NC4 = "1"
		} else {
			formData.NC4 = "0"
		}
		_, err04 := o.Raw("SELECT _id,invoice_narration,item_unique_code FROM item_org_assoc WHERE invoice_narration=? and _orgid=?", formData.ItemName4, formData.Orgid).Values(&maps)
		_, err4 := o.Raw("INSERT INTO product_bom (item_product_org_assoc_fkid, item_org_assoc_fkid,product_narration,quantity, status,wastage, _orgid,non_consumable) VALUES (?,?,?,?,?,?,?,?) ", p_id, maps[0]["_id"], maps[0]["invoice_narration"], formData.Qty4, "1", formData.Wastage4, formData.Orgid, formData.NC4).Exec()
		if err04 != nil || err4 != nil {
			//fmt.Println("Error in Item1 queries")
		}
	}
	if formData.ItemName5 != "" {
		if formData.Wastage5 == "" {
			formData.Wastage5 = "0.0"
		}
		if formData.NC5 == "on" {
			formData.NC5 = "1"
		} else {
			formData.NC5 = "0"
		}
		_, err05 := o.Raw("SELECT _id,invoice_narration,item_unique_code FROM item_org_assoc WHERE invoice_narration=? and _orgid=?", formData.ItemName5, formData.Orgid).Values(&maps)
		_, err5 := o.Raw("INSERT INTO product_bom (item_product_org_assoc_fkid, item_org_assoc_fkid,product_narration,quantity, status,wastage, _orgid,non_consumable) VALUES (?,?,?,?,?,?,?,?) ", p_id, maps[0]["_id"], maps[0]["invoice_narration"], formData.Qty5, "1", formData.Wastage5, formData.Orgid, formData.NC5).Exec()
		if err05 != nil || err5 != nil {
			//fmt.Println("Error in Item1 queries")
		}
	}
	if formData.ItemName6 != "" {
		if formData.Wastage6 == "" {
			formData.Wastage6 = "0.0"
		}
		if formData.NC6 == "on" {
			formData.NC6 = "1"
		} else {
			formData.NC6 = "0"
		}
		_, err06 := o.Raw("SELECT _id,invoice_narration,item_unique_code FROM item_org_assoc WHERE invoice_narration=? and _orgid=?", formData.ItemName6, formData.Orgid).Values(&maps)
		_, err6 := o.Raw("INSERT INTO product_bom (item_product_org_assoc_fkid, item_org_assoc_fkid,product_narration, quantity, status,wastage, _orgid,non_consumable) VALUES (?,?,?,?,?,?,?,?) ", p_id, maps[0]["_id"], maps[0]["invoice_narration"], formData.Qty6, "1", formData.Wastage6, formData.Orgid, formData.NC6).Exec()
		if err06 != nil || err6 != nil {
			//fmt.Println("Error in Item1 queries")
		}
	}
	if formData.ItemName7 != "" {
		if formData.Wastage7 == "" {
			formData.Wastage7 = "0.0"
		}
		if formData.NC7 == "on" {
			formData.NC7 = "1"
		} else {
			formData.NC7 = "0"
		}
		_, err07 := o.Raw("SELECT _id,invoice_narration,item_unique_code FROM item_org_assoc WHERE invoice_narration=? and _orgid=?", formData.ItemName7, formData.Orgid).Values(&maps)
		_, err7 := o.Raw("INSERT INTO product_bom (item_product_org_assoc_fkid, item_org_assoc_fkid,product_narration,quantity, status,wastage, _orgid,non_consumable) VALUES (?,?,?,?,?,?,?,?) ", p_id, maps[0]["_id"], maps[0]["invoice_narration"], formData.Qty7, "1", formData.Wastage7, formData.Orgid, formData.NC7).Exec()
		if err07 != nil || err7 != nil {
			//fmt.Println("Error in Item1 queries")
		}
	}
	if formData.ItemName != "" {
		if formData.Wastage == "" {
			formData.Wastage = "0.0"
		}
		if formData.NC == "on" {
			formData.NC = "1"
		} else {
			formData.NC = "0"
		}
		_, err08 := o.Raw("SELECT _id,invoice_narration,item_unique_code FROM item_org_assoc WHERE invoice_narration=? and _orgid=?", formData.ItemName, formData.Orgid).Values(&maps)
		_, err8 := o.Raw("INSERT INTO product_bom (item_product_org_assoc_fkid, item_org_assoc_fkid,product_narration,quantity, status,wastage, _orgid,non_consumable) VALUES (?,?,?,?,?,?,?,?) ", p_id, maps[0]["_id"], maps[0]["invoice_narration"], formData.Qty, "1", formData.Wastage, formData.Orgid, formData.NC).Exec()
		if err08 != nil || err8 != nil {
			//fmt.Println("Error in Item1 queries")
		}
	}
	if err == nil {
		return 1
		//fmt.Println("Bill of material added")
	}
	return 0
}

//the new BOM function to handle user def code
//the BOM function call from production controller should be replaced by the call to this function(by making necessory changes)
func BillofMaterialNew(formData Items) int {
	o := orm.NewOrm()
	var maps []orm.Params
	var err1, err2, err3, err4, err5, err6, err7, err8 error
	var err, err01, err02, err03, err04, err05, err06, err07, err08 error
	//if formData.ItemCodeType == "0" {
	_, err = o.Raw("SELECT _id,invoice_narration FROM item_org_assoc WHERE item_code_gen=? and name=? and _orgid=?", formData.BomProductCode, formData.BomProductName, formData.Orgid).Values(&maps)

	//	} else {
	//	_, err = o.Raw("SELECT _id,invoice_narration FROM item_org_assoc WHERE item_unique_code=? and name=? and _orgid=?", formData.BomProductCode, formData.BomProductName, formData.Orgid).Values(&maps)
	//}

	p_id := maps[0]["_id"]

	//fmt.Println("------p_id is ",p_id)
	//for i:=1; i<8; i++{
	//item_key := "ItemName" + strconv.Itoa(i)
	if formData.ItemName1 != "" {

		if formData.ItemCodeType == "0" {
			_, err01 = o.Raw("SELECT _id,invoice_narration,item_code_gen FROM item_org_assoc WHERE invoice_narration=? and _orgid=? ", formData.ItemName1, formData.Orgid).Values(&maps)

		} else {
			_, err01 = o.Raw("SELECT _id,invoice_narration,item_unique_code FROM item_org_assoc WHERE invoice_narration=? and _orgid=? ", formData.ItemName1, formData.Orgid).Values(&maps)
		}

		//a := strconv.Itoa(Itemid1)
		//Itemid1 := maps[0]["_id"]
		//fmt.Println("********Itemid1 is ", Itemid1)
		//err_key := "err" + strconv.Itoa(i)
		//qty_key := "Qty" + strconv.Itoa(i)
		if formData.Wastage1 == "" {
			formData.Wastage1 = "0.0"
		}
		if formData.Nc1 == "on" {
			formData.Nc1 = "1"
		} else {
			formData.Nc1 = "0"
		}

		if formData.ItemCodeType == "0" {
			_, err1 = o.Raw("INSERT INTO product_bom (item_product_org_assoc_fkid, item_org_assoc_fkid,product_narration, quantity, status, wastage, _orgid,non_consumable) VALUES (?,?,?,?,?,?,?,?) ", p_id, maps[0]["_id"], maps[0]["invoice_narration"], formData.Qty1, "1", formData.Wastage1, formData.Orgid, formData.Nc1).Exec()

		} else {
			_, err1 = o.Raw("INSERT INTO product_bom (item_product_org_assoc_fkid, item_org_assoc_fkid,product_narration, quantity, status, wastage, _orgid,non_consumable) VALUES (?,?,?,?,?,?,?,?,?) ", p_id, maps[0]["_id"], maps[0]["invoice_narration"], formData.Qty1, "1", formData.Wastage1, formData.Orgid, formData.Nc1).Exec()
		}

		if err01 != nil || err1 != nil {
			//fmt.Println("Error in Item1 queries")
		}
	}
	if formData.ItemName2 != "" {
		if formData.Wastage2 == "" {
			formData.Wastage2 = "0.0"
		}
		if formData.NC2 == "on" {
			formData.NC2 = "1"
		} else {
			formData.NC2 = "0"
		}

		if formData.ItemCodeType == "0" {
			_, err02 = o.Raw("SELECT _id,invoice_narration,item_code_gen FROM item_org_assoc WHERE invoice_narration=? and _orgid=?", formData.ItemName2, formData.Orgid).Values(&maps)
			_, err2 = o.Raw("INSERT INTO product_bom (item_product_org_assoc_fkid, item_org_assoc_fkid,product_narration, quantity, status,wastage, _orgid,non_consumable) VALUES (?,?,?,?,?,?,?,?) ", p_id, maps[0]["_id"], maps[0]["invoice_narration"], formData.Qty2, "1", formData.Wastage2, formData.Orgid, formData.NC2).Exec()

		} else {
			_, err02 = o.Raw("SELECT _id,invoice_narration,item_unique_code FROM item_org_assoc WHERE invoice_narration=? and _orgid=?", formData.ItemName2, formData.Orgid).Values(&maps)
			_, err2 = o.Raw("INSERT INTO product_bom (item_product_org_assoc_fkid, item_org_assoc_fkid,product_narration, quantity, status,wastage, _orgid,non_consumable) VALUES (?,?,?,?,?,?,?,?) ", p_id, maps[0]["_id"], maps[0]["invoice_narration"], formData.Qty2, "1", formData.Wastage2, formData.Orgid, formData.NC2).Exec()
		}

		if err02 != nil || err2 != nil {
			//fmt.Println("Error in Item2 queries")
		}
	}
	if formData.ItemName3 != "" {
		if formData.Wastage3 == "" {
			formData.Wastage3 = "0.0"
		}
		if formData.NC3 == "on" {
			formData.NC3 = "1"
		} else {
			formData.NC3 = "0"
		}

		if formData.ItemCodeType == "0" {
			_, err03 = o.Raw("SELECT _id,invoice_narration,item_code_gen FROM item_org_assoc WHERE invoice_narration=? and _orgid=?", formData.ItemName3, formData.Orgid).Values(&maps)
			_, err3 = o.Raw("INSERT INTO product_bom (item_product_org_assoc_fkid, item_org_assoc_fkid,product_narration, quantity, status,wastage, _orgid,non_consumable) VALUES (?,?,?,?,?,?,?,?) ", p_id, maps[0]["_id"], maps[0]["invoice_narration"], formData.Qty3, "1", formData.Wastage3, formData.Orgid, formData.NC3).Exec()

		} else {
			_, err03 = o.Raw("SELECT _id,invoice_narration,item_unique_code FROM item_org_assoc WHERE invoice_narration=? and _orgid=?", formData.ItemName3, formData.Orgid).Values(&maps)
			_, err3 = o.Raw("INSERT INTO product_bom (item_product_org_assoc_fkid, item_org_assoc_fkid,product_narration, quantity, status,wastage, _orgid,non_consumable) VALUES (?,?,?,?,?,?,?,?) ", p_id, maps[0]["_id"], maps[0]["invoice_narration"], formData.Qty3, "1", formData.Wastage3, formData.Orgid, formData.NC3).Exec()
		}

		if err03 != nil || err3 != nil {
			//fmt.Println("Error in Item3 queries")
		}
	}
	if formData.ItemName4 != "" {
		if formData.Wastage4 == "" {
			formData.Wastage4 = "0.0"
		}
		if formData.NC4 == "on" {
			formData.NC4 = "1"
		} else {
			formData.NC4 = "0"
		}

		if formData.ItemCodeType == "0" {
			_, err04 = o.Raw("SELECT _id,invoice_narration,item_code_gen FROM item_org_assoc WHERE invoice_narration=? and _orgid=?", formData.ItemName4, formData.Orgid).Values(&maps)
			_, err4 = o.Raw("INSERT INTO product_bom (item_product_org_assoc_fkid, item_org_assoc_fkid,product_narration, quantity, status,wastage, _orgid,non_consumable) VALUES (?,?,?,?,?,?,?,?) ", p_id, maps[0]["_id"], maps[0]["invoice_narration"], formData.Qty4, "1", formData.Wastage4, formData.Orgid, formData.NC4).Exec()

		} else {
			_, err04 = o.Raw("SELECT _id,invoice_narration,item_unique_code FROM item_org_assoc WHERE invoice_narration=? and _orgid=?", formData.ItemName4, formData.Orgid).Values(&maps)
			_, err4 = o.Raw("INSERT INTO product_bom (item_product_org_assoc_fkid, item_org_assoc_fkid,product_narration, quantity, status,wastage, _orgid,non_consumable) VALUES (?,?,?,?,?,?,?,?) ", p_id, maps[0]["_id"], maps[0]["invoice_narration"], formData.Qty4, "1", formData.Wastage4, formData.Orgid, formData.NC4).Exec()
		}

		if err04 != nil || err4 != nil {
			//fmt.Println("Error in Item1 queries")
		}
	}
	if formData.ItemName5 != "" {
		if formData.Wastage5 == "" {
			formData.Wastage5 = "0.0"
		}
		if formData.NC5 == "on" {
			formData.NC5 = "1"
		} else {
			formData.NC5 = "0"
		}

		if formData.ItemCodeType == "0" {
			_, err05 = o.Raw("SELECT _id,invoice_narration,item_code_gen FROM item_org_assoc WHERE invoice_narration=? and _orgid=?", formData.ItemName5, formData.Orgid).Values(&maps)
			_, err5 = o.Raw("INSERT INTO product_bom (item_product_org_assoc_fkid, item_org_assoc_fkid,product_narration, quantity, status,wastage, _orgid,non_consumable) VALUES (?,?,?,?,?,?,?,?) ", p_id, maps[0]["_id"], maps[0]["invoice_narration"], formData.Qty5, "1", formData.Wastage5, formData.Orgid, formData.NC5).Exec()

		} else {
			_, err05 = o.Raw("SELECT _id,invoice_narration,item_unique_code FROM item_org_assoc WHERE invoice_narration=? and _orgid=?", formData.ItemName5, formData.Orgid).Values(&maps)
			_, err5 = o.Raw("INSERT INTO product_bom (item_product_org_assoc_fkid, item_org_assoc_fkid,product_narration,quantity, status,wastage, _orgid,non_consumable) VALUES (?,?,?,?,?,?,?,?) ", p_id, maps[0]["_id"], maps[0]["invoice_narration"], formData.Qty5, "1", formData.Wastage5, formData.Orgid, formData.NC5).Exec()
		}

		if err05 != nil || err5 != nil {
			//fmt.Println("Error in Item1 queries")
		}
	}
	if formData.ItemName6 != "" {
		if formData.Wastage6 == "" {
			formData.Wastage6 = "0.0"
		}
		if formData.NC6 == "on" {
			formData.NC6 = "1"
		} else {
			formData.NC6 = "0"
		}

		if formData.ItemCodeType == "0" {
			_, err06 = o.Raw("SELECT _id,invoice_narration,item_code_gen FROM item_org_assoc WHERE invoice_narration=? and _orgid=?", formData.ItemName6, formData.Orgid).Values(&maps)
			_, err6 = o.Raw("INSERT INTO product_bom (item_product_org_assoc_fkid, item_org_assoc_fkid,product_narration, quantity, status,wastage, _orgid,non_consumable) VALUES (?,?,?,?,?,?,?,?) ", p_id, maps[0]["_id"], maps[0]["invoice_narration"], formData.Qty6, "1", formData.Wastage6, formData.Orgid, formData.NC6).Exec()

		} else {
			_, err06 = o.Raw("SELECT _id,invoice_narration,item_unique_code FROM item_org_assoc WHERE invoice_narration=? and _orgid=?", formData.ItemName6, formData.Orgid).Values(&maps)
			_, err6 = o.Raw("INSERT INTO product_bom (item_product_org_assoc_fkid, item_org_assoc_fkid,product_narration, quantity, status,wastage, _orgid,non_consumable) VALUES (?,?,?,?,?,?,?,?) ", p_id, maps[0]["_id"], maps[0]["invoice_narration"], formData.Qty6, "1", formData.Wastage6, formData.Orgid, formData.NC6).Exec()
		}

		if err06 != nil || err6 != nil {
			//fmt.Println("Error in Item1 queries")
		}
	}
	if formData.ItemName7 != "" {
		if formData.Wastage7 == "" {
			formData.Wastage7 = "0.0"
		}
		if formData.NC7 == "on" {
			formData.NC7 = "1"
		} else {
			formData.NC7 = "0"
		}

		if formData.ItemCodeType == "0" {
			_, err07 = o.Raw("SELECT _id,invoice_narration,item_code_gen FROM item_org_assoc WHERE invoice_narration=? and _orgid=?", formData.ItemName7, formData.Orgid).Values(&maps)
			_, err7 = o.Raw("INSERT INTO product_bom (item_product_org_assoc_fkid, item_org_assoc_fkid,product_narration, quantity, status,wastage, _orgid,non_consumable) VALUES (?,?,?,?,?,?,?,?) ", p_id, maps[0]["_id"], maps[0]["invoice_narration"], formData.Qty7, "1", formData.Wastage7, formData.Orgid, formData.NC7).Exec()

		} else {
			_, err07 = o.Raw("SELECT _id,invoice_narration,item_unique_code FROM item_org_assoc WHERE invoice_narration=? and _orgid=?", formData.ItemName7, formData.Orgid).Values(&maps)
			_, err7 = o.Raw("INSERT INTO product_bom (item_product_org_assoc_fkid, item_org_assoc_fkid,product_narration,quantity, status,wastage, _orgid,non_consumable) VALUES (?,?,?,?,?,?,?,?) ", p_id, maps[0]["_id"], maps[0]["invoice_narration"], formData.Qty7, "1", formData.Wastage7, formData.Orgid, formData.NC7).Exec()
		}

		if err07 != nil || err7 != nil {
			//fmt.Println("Error in Item1 queries")
		}
	}
	if formData.ItemName != "" {
		if formData.Wastage == "" {
			formData.Wastage = "0.0"
		}
		if formData.NC == "on" {
			formData.NC = "1"
		} else {
			formData.NC = "0"
		}

		if formData.ItemCodeType == "0" {
			_, err08 = o.Raw("SELECT _id,invoice_narration,item_code_gen FROM item_org_assoc WHERE invoice_narration=? and _orgid=?", formData.ItemName, formData.Orgid).Values(&maps)
			_, err8 = o.Raw("INSERT INTO product_bom (item_product_org_assoc_fkid, item_org_assoc_fkid,product_narration,quantity, status,wastage, _orgid,non_consumable) VALUES (?,?,?,?,?,?,?,?) ", p_id, maps[0]["_id"], maps[0]["invoice_narration"], formData.Qty, "1", formData.Wastage, formData.Orgid, formData.NC).Exec()

		} else {
			_, err08 = o.Raw("SELECT _id,invoice_narration,item_unique_code FROM item_org_assoc WHERE invoice_narration=? and _orgid=?", formData.ItemName, formData.Orgid).Values(&maps)
			_, err8 = o.Raw("INSERT INTO product_bom (item_product_org_assoc_fkid, item_org_assoc_fkid,product_narration,quantity, status,wastage, _orgid,non_consumable) VALUES (?,?,?,?,?,?,?,?) ", p_id, maps[0]["_id"], maps[0]["invoice_narration"], formData.Qty, "1", formData.Wastage, formData.Orgid, formData.NC).Exec()
		}

		if err08 != nil || err8 != nil {
			//fmt.Println("Error in Item1 queries")
		}
	}
	if err == nil {
		return 1
		//fmt.Println("Bill of material added")
	}
	return 0
}

func BillofMaterialRepeater(formData Items, bomitems []map[string]string) int {
	o := orm.NewOrm()
	ormTransactionalErr := o.Begin()
	var maps []orm.Params
	var err, err01, err1 error

	_, err = o.Raw("SELECT _id,invoice_narration FROM item_org_assoc WHERE item_code_gen=? and name=? and _orgid=?", formData.BomProductCode, formData.BomProductName, formData.Orgid).Values(&maps)
	if err != nil {
		return 0
	}
	p_id := maps[0]["_id"]

	for _, it := range bomitems {
		if it["item_name"] != "" && it["qty"] != "" {

			if formData.ItemCodeType == "0" {
				_, err01 = o.Raw("SELECT _id,invoice_narration,item_code_gen FROM item_org_assoc WHERE invoice_narration=? and _orgid=? ", it["item_name"], formData.Orgid).Values(&maps)

			} else {
				_, err01 = o.Raw("SELECT _id,invoice_narration,item_unique_code FROM item_org_assoc WHERE invoice_narration=? and _orgid=? ", it["item_name"], formData.Orgid).Values(&maps)
			}

			//a := strconv.Itoa(Itemid1)
			//Itemid1 := maps[0]["_id"]
			//fmt.Println("********Itemid1 is ", Itemid1)
			//err_key := "err" + strconv.Itoa(i)
			//qty_key := "Qty" + strconv.Itoa(i)
			if it["wastage"] == "" {
				it["wastage"] = "0.0"
			}
			if it["nc"] == "on" {
				it["nc"] = "1"
			} else {
				it["nc"] = "0"
			}

			if formData.ItemCodeType == "0" {
				_, err1 = o.Raw("INSERT INTO product_bom (item_product_org_assoc_fkid, item_org_assoc_fkid,product_narration, quantity, status, wastage, _orgid,non_consumable) VALUES (?,?,?,?,?,?,?,?) ", p_id, maps[0]["_id"], maps[0]["invoice_narration"], it["qty"], "1", it["wastage"], formData.Orgid, it["nc"]).Exec()

			} else {
				_, err1 = o.Raw("INSERT INTO product_bom (item_product_org_assoc_fkid, item_org_assoc_fkid,product_narration, quantity, status, wastage, _orgid,non_consumable) VALUES (?,?,?,?,?,?,?,?) ", p_id, maps[0]["_id"], maps[0]["invoice_narration"], it["qty"], "1", it["wastage"], formData.Orgid, it["nc"]).Exec()
			}

			if err01 != nil || err1 != nil {
				//fmt.Println("Error in Item1 queries")
				ormTransactionalErr = o.Rollback()
				return 0
			}
		}
	}
	if ormTransactionalErr != nil {
		return 0
	}
	ormTransactionalErr = o.Commit()
	return 1
}

func InsertBOMByProductID(productId string, orgID string, bomitems []map[string]string) int {
	o := orm.NewOrm()
	ormTransactionalErr := o.Begin()

	p_id := productId

	for _, it := range bomitems {

		if it["wastage"] == "" {
			it["wastage"] = "0.0"
		}
		if it["nc"] == "on" {
			it["nc"] = "1"
		} else {
			it["nc"] = "0"
		}

		_, err1 := o.Raw("INSERT INTO product_bom (item_product_org_assoc_fkid, item_org_assoc_fkid,product_narration, quantity, status, wastage, _orgid,non_consumable) VALUES (?,?,?,?,?,?,?,?) ", p_id, it["item_name"], "", it["qty"], "1", it["wastage"], orgID, it["nc"]).Exec()

		if err1 != nil {
			//fmt.Println("Error in Item1 queries")
			ormTransactionalErr = o.Rollback()
			return 0
		}

	}
	if ormTransactionalErr != nil {
		return 0
	}
	ormTransactionalErr = o.Commit()
	return 1
}

func GetBOMProduct(formData Product) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT LAST(product_org_code, product_org_name) FROM product_org_assoc and _orgid=?", formData.Orgid).Values(&maps)
	if err == nil && num > 0 {
		var i int64 = 0
		for ; i < num; i++ {
			//fmt.Println(maps[i])
		}
		for k, v := range maps {
			key := "bom_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetJobcard(formData JobcardM) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	//num, err := o.Raw("SELECT * FROM item_master ").Values(&maps)
	num, err := o.Raw("SELECT *, jobcard_date as j_date, order_date as o_date, invoice_date as i_date, inspection_date as ins_date, QC_date as qc_date from production_job_card where _orgid=?", formData.Orgid).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		var i int64 = 0
		for ; i < num; i++ {
			//fmt.Println(maps[i])
		}
		for k, v := range maps {
			key := "jobcard_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetProduct(formData Product) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	//num, err := o.Raw("SELECT * FROM item_master ").Values(&maps)
	num, err := o.Raw("SELECT poa._id, poa.product_org_code, poa.product_org_name, poa._orgid, poa.invoice_narration, poa.rate_per_UOM, poa.minimum_stock_keep, poa.lead_time, poa.status, poa.grp, poa.subgroup, poa.product_type, poa.unit_of_measure, poa.capacity_measure, poa.is_finished_product, poa.transport, poa.process_name, of.factory_location FROM product_org_assoc as poa, organisation_factories as of where poa._orgid=? and of._orgid=? and of.status=1 and of._id=poa.geo_location", formData.Orgid, formData.Orgid).Values(&maps)
	//var maps1 []orm.Params
	//count, err1 := o.Raw("SELECT item_org_assoc.invoice_narration, product_bom.quantity FROM item_org_assoc, product_bom, product_org_assoc WHERE product_bom.product_org_assoc_fkid=product_org_assoc._id and item_org_assoc._id=product_bom.item_org_assoc_fkid and _orgid=?" , formData.Orgid).Values(&maps1)
	//num, err := o.Raw("SELECT product_org_code, product_org_name, product_org_assoc.invoice_narration, is_finished_product, product_org_assoc.grp, product_org_assoc.subgroup, capacity_measure, unit_of_measure, rate_per_UOM, transport, process_name, product_org_assoc.status, item_org_assoc.invoice_narration as items, product_bom.quantity FROM product_org_assoc, product_bom, item_org_assoc where product_org_assoc._orgid=? and product_bom.product_org_assoc_fkid=product_org_assoc._id and item_org_assoc._id=product_bom.item_org_assoc_fkid",formData.Orgid).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		var i int64 = 0
		for ; i < num; i++ {
			//fmt.Println(maps[i])
		}
		for k, v := range maps {
			key := "product_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

// func GetProductCode(formData Product) string {
// 	var result string
// 	o := orm.NewOrm()
// 	var maps []orm.Params
// 	num, err := o.Raw("SELECT product_org_code FROM product_org_assoc WHERE _orgid=? ORDER BY _id DESC LIMIT 1", formData.Orgid).Values(&maps)
// 	//fmt.Println("****code:",maps[0]["product_org_code"])
// 	if err == nil {
// 		if num > 0 {
// 			s := maps[0]["product_org_code"].(string)
// 			fmt.Println("**s:", s)
// 			fmt.Println("***length", len(s))
// 			prefix := s[0 : len(s)-1]
// 			fmt.Println("Prefix slice:", prefix)
// 			last := s[len(s)-1:]
// 			fmt.Println("Last slice:", last)
// 			sufix, _ := strconv.Atoi(last)
// 			sufix = sufix + 1
// 			new_sufix := strconv.Itoa(sufix)
// 			fmt.Println("Sufix:", new_sufix)
// 			append_code := prefix + new_sufix
// 			fmt.Println("Append_code:", append_code)
// 			count, _ := o.Raw("SELECT _id FROM product_org_assoc WHERE product_org_code=? ", append_code).Values(&maps)
// 			if count == 0 {
// 				result = append_code
// 			} else {
// 				var safe int = 1
// 				for safe != 0 {
// 					prefix := append_code[0 : len(append_code)-1]
// 					last := append_code[len(append_code)-1:]
// 					fmt.Println("Last slice:", last)
// 					sufix, _ := strconv.Atoi(last)
// 					sufix = sufix + 1
// 					new_sufix := strconv.Itoa(sufix)
// 					fmt.Println("Sufix:", new_sufix)
// 					append_code := prefix + new_sufix
// 					count, _ := o.Raw("SELECT _id FROM product_org_assoc WHERE product_org_code=? ", append_code).Values(&maps)
// 					if count == 0 {
// 						safe = 0
// 						result = append_code
// 					} else {
// 						safe = 0
// 					}
// 				}
// 			}
// 		} else {
// 			prefix := "SINGU"
// 			s := formData.Orgid
// 			mid := s[0:3]
// 			sufix := "P1"
// 			append_code := prefix + mid + sufix
// 			count, _ := o.Raw("SELECT _id FROM product_org_assoc WHERE product_org_code=? ", append_code).Values(&maps)
// 			if count == 0 {
// 				result = append_code
// 			} else {
// 				var safe int = 1
// 				for safe != 0 {
// 					prefix := append_code[0 : len(append_code)-1]
// 					last := append_code[len(append_code)-1:]
// 					fmt.Println("Last slice:", last)
// 					sufix, _ := strconv.Atoi(last)
// 					sufix = sufix + 1
// 					new_sufix := strconv.Itoa(sufix)
// 					fmt.Println("Sufix:", new_sufix)
// 					append_code := prefix + new_sufix
// 					count, _ := o.Raw("SELECT _id FROM product_org_assoc WHERE product_org_code=? ", append_code).Values(&maps)
// 					if count == 0 {
// 						safe = 0
// 						result = append_code
// 					} else {
// 						safe = 0
// 					}
// 				}
// 			}
// 		}
// 	} else {
// 		fmt.Println("Error in code query")
// 	}

// num, err := o.Raw("SELECT product_code FROM product_org_assoc where product_org_assoc._orgid=? ",formData.Orgid).Values(&maps)
// //num, err := o.Raw("SELECT * FROM product_org_assoc, product_bom, item_org_assoc where product_org_assoc._orgid=? and product_bom.product_org_assoc_fkid=product_org_assoc._id and item_org_assoc._id=product_bom.item_org_assoc_fkid",formData.Orgid).Values(&maps)
// if err == nil && num > 0 {
// 	//fmt.Println(maps[0]["name"])
// 	var i int64 = 0
// 	for ; i<num; i++ {
// 		fmt.Println(maps[i])
// 	}
// 	for k, v:= range maps{
// 		key := "product_" + strconv.Itoa(k)
// 		result[key] = v
// 	}

// } else {
// 	fmt.Println(err)
// 	result["status"] = nil
// }
// 	return result
// }

func GetBOItems(formData Items) map[string]orm.Params {

	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params

	num, err := o.Raw("SELECT * FROM item_org_assoc WHERE _orgid=?", formData.Orgid).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		var i int64 = 0
		for ; i < num; i++ {
			//fmt.Println(maps[i])
		}
		for k, v := range maps {

			key := "io_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetBOIActiveItems(formData Items) map[string]orm.Params {

	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params

	num, err := o.Raw("SELECT * FROM item_org_assoc WHERE _orgid=? and status=1", formData.Orgid).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		var i int64 = 0
		for ; i < num; i++ {
			//fmt.Println(maps[i])
		}
		for k, v := range maps {

			key := "io_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetAllProducts(formData ProductionOrder) map[string]orm.Params {

	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params

	num, err := o.Raw("SELECT ioa._id, ioa.invoice_narration, ioa.unit_measure, ioa.item_unique_code , ioa.item_code_gen FROM item_org_assoc as ioa WHERE ioa._orgid=? and ioa.produced_internally=1 and ioa.status=1", formData.Orgid).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		var i int64 = 0
		for ; i < num; i++ {
			//fmt.Println(maps[i])
		}
		for k, v := range maps {

			key := "io_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetFactoryName(_orgId string, FactoryId string) string {
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT factory_location FROM organisation_factories WHERE _orgid=? and _id=?", _orgId, FactoryId).Values(&maps)
	if err == nil && num > 0 {
		if maps[0]["factory_location"] != nil {
			return maps[0]["factory_location"].(string)
		}
	}
	return ""
}

func GetMachineName(_orgId string, MachineId string) string {
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT machine_name FROM machine WHERE _orgid=? and _pkid=?", _orgId, MachineId).Values(&maps)
	if err == nil && num > 0 {
		if maps[0]["machine_name"] != nil {
			return maps[0]["machine_name"].(string)
		}
	}
	return ""
}

func GetProductName(_orgId string, ProductId string) string {
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT invoice_narration FROM item_org_assoc WHERE _orgid=? and _id=?", _orgId, ProductId).Values(&maps)
	if err == nil && num > 0 {
		if maps[0]["invoice_narration"] != nil {
			return maps[0]["invoice_narration"].(string)
		}
	}
	return ""
}

//ACTIVE FUNCTION FOR PRODUCTION ORDERS
//THIS WILL RETURN THE INSERT STATUS AND THE PRODUCTION ORDER FKID
func CreateNewProductionOrder(formData ProductionOrder, Status string) (int, string) {

	o := orm.NewOrm()
	ormTransactionalErr := o.Begin()
	var maps3 []orm.Params
	_, err3 := o.Raw("SELECT unit_measure FROM item_org_assoc WHERE _orgid=? and _id=?", formData.Orgid, formData.Product).Values(&maps3)

	if err3 != nil {
		ormTransactionalErr = o.Rollback()
		return 0, "failed"
	}

	_, err1 := o.Raw("INSERT INTO production_orders (_orgid, batch_no,order_date, item_org_assoc_fkid,qty,uom,start_date,end_date,last_updated_date,added_by_user_account_fkid,status,machine_name,geo_location, production_order_unique_code, temporary_bom_used, manually_assign_batch, only_code) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ", formData.Orgid, formData.BatchNo, formData.OrderDate, formData.Product, formData.Quantity, maps3[0]["unit_measure"], formData.SDate, formData.EDate, time.Now(), formData.Userid, Status, formData.Machine, formData.FactoryLoc, formData.UniqueCode, formData.TemporaryBOMUsed, formData.ManuallyAssignBatch, formData.ShowBom).Exec()

	if err1 != nil {
		ormTransactionalErr = o.Rollback()
		return 0, "failed"
	}

	var maps2 []orm.Params
	_, err2 := o.Raw("SELECT _id FROM production_orders WHERE _orgid=? and batch_no=? ", formData.Orgid, formData.BatchNo).Values(&maps2)

	if err2 != nil {
		ormTransactionalErr = o.Rollback()
		return 0, "failed"
	}

	_, err := o.Raw("INSERT INTO production_order_status (_orgid, production_orders_fkid, status, latest, updated_date,updated_by_user_account_fkid) VALUES (?,?,?,?,?,?)", formData.Orgid, maps2[0]["_id"], Status, "1", time.Now(), formData.Userid).Exec()
	if err != nil {
		ormTransactionalErr = o.Rollback()
		return 0, "failed"
	}

	if ormTransactionalErr != nil {
		return 0, "failed"
	}

	ormTransactionalErr = o.Commit()
	return 1, maps2[0]["_id"].(string)
}

func CreateProQueue(formData ProductionOrder, Status string) int {

	o := orm.NewOrm()
	ormTransactionalErr := o.Begin()
	var maps3 []orm.Params
	_, err3 := o.Raw("SELECT unit_of_measure FROM product_org_assoc WHERE _orgid=? and _id=?", formData.Orgid, formData.Product).Values(&maps3)
	_, err1 := o.Raw("INSERT INTO production_orders (_orgid, batch_no,order_date, item_org_assoc_fkid,qty,uom,start_date,end_date,last_updated_date,added_by_user_account_fkid,status,machine_name,geo_location) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?) ", formData.Orgid, formData.BatchNo, formData.OrderDate, formData.Product, formData.Quantity, maps3[0]["unit_of_measure"], formData.SDate, formData.EDate, time.Now(), formData.Userid, Status, formData.Machine, formData.FactoryLoc).Exec()
	if err3 != nil || err1 != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}
	var maps2 []orm.Params
	_, err2 := o.Raw("SELECT _id FROM production_orders WHERE _orgid=? and batch_no=? ", formData.Orgid, formData.BatchNo).Values(&maps2)

	_, err := o.Raw("INSERT INTO production_order_status (_orgid, production_orders_fkid, status, latest, updated_date,updated_by_user_account_fkid) VALUES (?,?,?,?,?,?)", formData.Orgid, maps2[0]["_id"], Status, "1", time.Now(), formData.Userid).Exec()
	if err2 != nil || err != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}
	// if err != nil {
	// 	fmt.Println("Error in Item queries")
	// }
	// }
	_, err4 := o.Raw("INSERT INTO production_queue (_orgid,org_product_assoc_fkid,qty,uom, prod_order_fkid, status,last_updated_date,added_by_user_account_fkid,type) VALUES (?,?,?,?,?,?,?,?,?)", formData.Orgid, formData.Product, formData.Quantity, maps3[0]["unit_of_measure"], maps2[0]["_id"], "1", time.Now(), formData.Userid, "1").Exec()
	if err4 != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}
	var maps4 []orm.Params
	_, err5 := o.Raw("SELECT _id FROM production_queue WHERE _orgid=? and prod_order_fkid=? ", formData.Orgid, maps2[0]["_id"]).Values(&maps4)
	_, err6 := o.Raw("INSERT INTO production_queue_status (_orgid, production_queue_fkid, status, latest, updated_date,updated_by_user_account_fkid) VALUES (?,?,?,?,?,?)", formData.Orgid, maps4[0]["_id"], "1", "1", time.Now(), formData.Userid).Exec()
	if err5 != nil || err6 != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}
	if ormTransactionalErr != nil {
		return 0
	}
	ormTransactionalErr = o.Commit()
	return 1
}

func GetProductionOrdersdetailsById(productionId string, orgId string) map[string]orm.Params {

	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params

	num, err := o.Raw("SELECT production._id,production.batch_no,production.order_date,production.item_org_assoc_fkid,production.qty,production.uom,production.start_date,production.end_date,production.geo_location,production.temporary_bom_used,orgFactory.factory_location,machine.machine_name,itemOrgAssoc.name,itemOrgAssoc.item_code_gen from production_orders as production, organisation_factories as orgFactory,item_org_assoc as itemOrgAssoc, machine as machine where production._id = ? and production.item_org_assoc_fkid = itemOrgAssoc._id and production.machine_name = machine._pkid and production.geo_location = orgFactory._id and production.item_org_assoc_fkid = itemOrgAssoc._id and production._orgid = ? ", productionId, orgId).Values(&maps)
	if err == nil && num > 0 {
		for k, v := range maps {
			// if maps[k]["o_date"] != nil && maps[k]["o_date"] != "" {
			// 	maps[k]["o_date"] = convertDateString(maps[k]["o_date"].(string))
			// }
			// if maps[k]["start_date"] != nil && maps[k]["start_date"] != "" {
			// 	maps[k]["start_date"] = convertDateString(maps[k]["start_date"].(string))
			// }
			// if maps[k]["end_date"] != nil && maps[k]["end_date"] != "" {
			// 	maps[k]["end_date"] = convertDateString(maps[k]["end_date"].(string))
			// }
			key := "po_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		result["status"] = nil
	}
	return result
}

func GetProductionOrdersdetails(formData ProductionOrder) map[string]orm.Params {

	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params

	num, err := o.Raw("SELECT pos.*, org_f.factory_location FROM (SELECT po._id,po.batch_no,po.order_date as o_date, ioa.name, ioa.invoice_narration ,po.uom, po.item_org_assoc_fkid, po.qty,po.start_date,po.end_date,po.status,machine.machine_name, po.geo_location FROM production_orders as po, item_org_assoc as ioa, machine WHERE ioa._id=po.item_org_assoc_fkid  and po._orgid=? and ioa._orgid=? and machine._pkid=po.machine_name) AS pos, organisation_factories AS org_f WHERE pos.geo_location = org_f._id", formData.Orgid, formData.Orgid).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])

		// var i int64 = 0

		// for ; i < num; i++ {
		// 	fmt.Println(maps[i])
		// }
		for k, v := range maps {
			// if maps[k]["o_date"] != nil && maps[k]["o_date"] != "" {
			// 	maps[k]["o_date"] = convertDateString(maps[k]["o_date"].(string))
			// }
			// if maps[k]["start_date"] != nil && maps[k]["start_date"] != "" {
			// 	maps[k]["start_date"] = convertDateString(maps[k]["start_date"].(string))
			// }
			// if maps[k]["end_date"] != nil && maps[k]["end_date"] != "" {
			// 	maps[k]["end_date"] = convertDateString(maps[k]["end_date"].(string))
			// }
			key := "po_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetDelayedProductionOrdersdetails(formData ProductionOrder) map[string]orm.Params {

	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params

	num, err := o.Raw("SELECT pos.*, org_f.factory_location FROM (SELECT po._id,po.batch_no,po.order_date as o_date, ioa.name, ioa.invoice_narration ,po.uom, po.item_org_assoc_fkid, po.qty,po.start_date,po.end_date,po.status,machine.machine_name, po.geo_location FROM production_orders as po, item_org_assoc as ioa, machine WHERE ioa._id=po.item_org_assoc_fkid  and po._orgid=? and ioa._orgid=? and machine._pkid=po.machine_name) AS pos, organisation_factories AS org_f WHERE pos.geo_location = org_f._id and pos.end_date<? and (pos.status=2 or pos.status=5) ORDER BY pos.end_date", formData.Orgid, formData.Orgid, GetTodaysDate()).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])

		// var i int64 = 0

		// for ; i < num; i++ {
		// 	fmt.Println(maps[i])
		// }
		for k, v := range maps {
			if maps[k]["o_date"] != nil && maps[k]["o_date"] != "" {
				maps[k]["o_date"] = convertDateString(maps[k]["o_date"].(string))
			}
			if maps[k]["start_date"] != nil && maps[k]["start_date"] != "" {
				maps[k]["start_date"] = convertDateString(maps[k]["start_date"].(string))
			}
			if maps[k]["end_date"] != nil && maps[k]["end_date"] != "" {
				maps[k]["end_date"] = convertDateString(maps[k]["end_date"].(string))
			}
			key := "po_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetInitialPQDetails(formData ProductQueue) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	num, err1 := o.Raw("SELECT poa.invoice_narration, poa.product_org_code, pq.qty, pq.status, pq.type, pq.uom, pq._id FROM production_queue as pq, product_org_assoc as poa WHERE poa._id=pq.org_product_assoc_fkid and pq._orgid=? and poa._orgid=? ORDER BY pq.last_updated_date DESC", formData.Orgid, formData.Orgid).Values(&maps)
	if err1 == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		// var i int64 = 0
		// for ; i<num; i++ {
		// 	fmt.Println(maps[i])
		// }
		for k, v := range maps {

			if maps[k]["type"] == "0" {
				//fmt.Println("****Found SO")
				var maps2 []orm.Params
				_, err := o.Raw("SELECT order_no FROM sales_order, production_queue WHERE sales_order._id=production_queue.sales_order_fkid and production_queue._id=? and sales_order._orgid=? and production_queue._orgid=?", maps[k]["_id"], formData.Orgid, formData.Orgid).Values(&maps2)

				if err == nil {
					//fmt.Println("****map**",maps2[0]["order_no"])
					//fmt.Println("***No error in mapping")
					//fmt.Println("****map**", maps2[0]["batch_no"])
					maps[k]["Number"] = maps2[0]["order_no"]
				} else {
					//fmt.Println("Error in QUery")
				}

			} else {
				var maps3 []orm.Params
				//fmt.Println("****Found PO")
				_, err := o.Raw("SELECT batch_no FROM production_orders, production_queue WHERE production_orders._id=production_queue.prod_order_fkid and production_queue._id=? and production_orders._orgid=? and production_queue._orgid=?", maps[k]["_id"], formData.Orgid, formData.Orgid).Values(&maps3)
				if err == nil {
					//fmt.Println(maps3[0]["batch_no"])
					maps[k]["Number"] = maps3[0]["batch_no"]
				}

			}

			key := "po_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err1)
		result["status"] = nil
	}
	return result
}

func GetProductionQueuesdetails(formData ProductQueue) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	if formData.ProductQ != "" {
		if formData.Status == "" && formData.Type == "" {
			var maps1 []orm.Params
			count, err := o.Raw("SELECT _id FROM product_org_assoc WHERE invoice_narration=? and _orgid=?", formData.ProductQ, formData.Orgid).Values(&maps1)
			if err == nil && count > 0 {
				var maps []orm.Params
				num, err1 := o.Raw("SELECT poa.invoice_narration, poa.product_org_code, pq.qty, pq.status, pq.type, pq.uom, pq._id FROM production_queue as pq, product_org_assoc as poa WHERE poa._id=pq.org_product_assoc_fkid and pq._orgid=? and poa._orgid=? and poa.invoice_narration=? and poa._id=? ORDER BY pq.last_updated_date DESC", formData.Orgid, formData.Orgid, formData.ProductQ, maps1[0]["_id"]).Values(&maps)
				var i int64 = 0
				for ; i < num; i++ {
					//fmt.Println(maps[i])
				}
				if err1 == nil && num > 0 {
					for k, v := range maps {
						if maps[k]["type"] == "0" {
							//fmt.Println("****Found SO")
							var maps2 []orm.Params
							_, err := o.Raw("SELECT order_no FROM sales_order, production_queue WHERE sales_order._id=production_queue.sales_order_fkid and production_queue._id=? and sales_order._orgid=? and production_queue._orgid=?", maps[k]["_id"], formData.Orgid, formData.Orgid).Values(&maps2)

							if err == nil {
								//fmt.Println("****map**",maps2[0]["order_no"])
								//fmt.Println("***No error in mapping")
								//fmt.Println("****map**", maps2[0]["order_no"])
								maps[k]["Number"] = maps2[0]["order_no"]
							} else {
								//fmt.Println("Error in QUery")
							}

						} else {
							var maps3 []orm.Params
							//fmt.Println("****Found PO")
							_, err := o.Raw("SELECT batch_no FROM production_orders, production_queue WHERE production_orders._id=production_queue.prod_order_fkid and production_queue._id=? and production_orders._orgid=? and production_queue._orgid=?", maps[k]["_id"], formData.Orgid, formData.Orgid).Values(&maps3)
							if err == nil {
								//fmt.Println(maps3[0]["batch_no"])
								maps[k]["Number"] = maps3[0]["batch_no"]
							}

						}

						key := "po_" + strconv.Itoa(k)
						result[key] = v
					}

				} else {
					//fmt.Println(err1)
					result["status"] = nil
				}
			} else {
				//fmt.Println(err)
				result["status"] = nil
			}
		} else if formData.Status != "" && formData.Type == "" {
			var maps1 []orm.Params
			count, err := o.Raw("SELECT _id FROM product_org_assoc WHERE invoice_narration=? and _orgid=?", formData.ProductQ, formData.Orgid).Values(&maps1)
			if err == nil && count > 0 {
				var maps []orm.Params
				num, err1 := o.Raw("SELECT poa.invoice_narration, poa.product_org_code, pq.qty, pq.status, pq.type, pq.uom, pq._id FROM production_queue as pq, product_org_assoc as poa WHERE poa._id=pq.org_product_assoc_fkid and pq._orgid=? and poa._orgid=? and poa.invoice_narration=? and poa._id=? and pq.status=?", formData.Orgid, formData.Orgid, formData.ProductQ, maps1[0]["_id"], formData.Status).Values(&maps)
				var i int64 = 0
				for ; i < num; i++ {
					//fmt.Println(maps[i])
				}
				if err1 == nil && num > 0 {
					for k, v := range maps {
						if maps[k]["type"] == "0" {
							//fmt.Println("****Found SO")
							var maps2 []orm.Params
							_, err := o.Raw("SELECT order_no FROM sales_order, production_queue WHERE sales_order._id=production_queue.sales_order_fkid and production_queue._id=? and sales_order._orgid=? and production_queue._orgid=?", maps[k]["_id"], formData.Orgid, formData.Orgid).Values(&maps2)

							if err == nil {
								//fmt.Println("****map**",maps2[0]["order_no"])
								//fmt.Println("***No error in mapping")
								//fmt.Println("****map**", maps2[0]["order_no"])
								maps[k]["Number"] = maps2[0]["order_no"]
							} else {
								//fmt.Println("Error in QUery")
							}

						} else {
							var maps3 []orm.Params
							//fmt.Println("****Found PO")
							_, err := o.Raw("SELECT batch_no FROM production_orders, production_queue WHERE production_orders._id=production_queue.prod_order_fkid and production_queue._id=? and production_orders._orgid=? and production_queue._orgid=?", maps[k]["_id"], formData.Orgid, formData.Orgid).Values(&maps3)
							if err == nil {
								//fmt.Println(maps3[0]["batch_no"])
								maps[k]["Number"] = maps3[0]["batch_no"]
							}

						}

						key := "po_" + strconv.Itoa(k)
						result[key] = v
					}

				} else {
					//fmt.Println(err1)
					result["status"] = nil
				}
			} else {
				//fmt.Println(err)
				result["status"] = nil
			}
		} else if formData.Status == "" && formData.Type != "" {
			var maps1 []orm.Params
			count, err := o.Raw("SELECT _id FROM product_org_assoc WHERE invoice_narration=? and _orgid=?", formData.ProductQ, formData.Orgid).Values(&maps1)
			if err == nil && count > 0 {
				var maps []orm.Params
				num, err1 := o.Raw("SELECT poa.invoice_narration, poa.product_org_code, pq.qty, pq.status, pq.type, pq.uom, pq._id FROM production_queue as pq, product_org_assoc as poa WHERE poa._id=pq.org_product_assoc_fkid and pq._orgid=? and poa._orgid=? and poa.invoice_narration=? and poa._id=? and pq.type=?", formData.Orgid, formData.Orgid, formData.ProductQ, maps1[0]["_id"], formData.Type).Values(&maps)
				var i int64 = 0
				for ; i < num; i++ {
					//fmt.Println(maps[i])
				}
				if err1 == nil && num > 0 {
					for k, v := range maps {
						if maps[k]["type"] == "0" {
							//fmt.Println("****Found SO")
							var maps2 []orm.Params
							_, err := o.Raw("SELECT order_no FROM sales_order, production_queue WHERE sales_order._id=production_queue.sales_order_fkid and production_queue._id=? and sales_order._orgid=? and production_queue._orgid=?", maps[k]["_id"], formData.Orgid, formData.Orgid).Values(&maps2)

							if err == nil {
								//fmt.Println("****map**",maps2[0]["order_no"])
								//fmt.Println("***No error in mapping")
								//fmt.Println("****map**", maps2[0]["order_no"])
								maps[k]["Number"] = maps2[0]["order_no"]
							} else {
								//fmt.Println("Error in QUery")
							}

						} else {
							var maps3 []orm.Params
							//fmt.Println("****Found PO")
							_, err := o.Raw("SELECT batch_no FROM production_orders, production_queue WHERE production_orders._id=production_queue.prod_order_fkid and production_queue._id=? and production_orders._orgid=? and production_queue._orgid=?", maps[k]["_id"], formData.Orgid, formData.Orgid).Values(&maps3)
							if err == nil {
								//fmt.Println(maps3[0]["batch_no"])
								maps[k]["Number"] = maps3[0]["batch_no"]
							}

						}

						key := "po_" + strconv.Itoa(k)
						result[key] = v
					}

				} else {
					//fmt.Println(err1)
					result["status"] = nil
				}
			} else {
				//fmt.Println(err)
				result["status"] = nil
			}
		} else {
			var maps1 []orm.Params
			count, err := o.Raw("SELECT _id FROM product_org_assoc WHERE invoice_narration=? and _orgid=?", formData.ProductQ, formData.Orgid).Values(&maps1)
			if err == nil && count > 0 {
				var maps []orm.Params
				num, err1 := o.Raw("SELECT poa.invoice_narration, poa.product_org_code, pq.qty, pq.status, pq.type, pq.uom, pq._id FROM production_queue as pq, product_org_assoc as poa WHERE poa._id=pq.org_product_assoc_fkid and pq._orgid=? and poa._orgid=? and poa.invoice_narration=? and poa._id=? and pq.type=? and pq.status=?", formData.Orgid, formData.Orgid, formData.ProductQ, maps1[0]["_id"], formData.Type, formData.Status).Values(&maps)
				var i int64 = 0
				for ; i < num; i++ {
					//fmt.Println(maps[i])
				}
				if err1 == nil && num > 0 {
					for k, v := range maps {
						if maps[k]["type"] == "0" {
							//fmt.Println("****Found SO")
							var maps2 []orm.Params
							_, err := o.Raw("SELECT order_no FROM sales_order, production_queue WHERE sales_order._id=production_queue.sales_order_fkid and production_queue._id=? and sales_order._orgid=? and production_queue._orgid=?", maps[k]["_id"], formData.Orgid, formData.Orgid).Values(&maps2)

							if err == nil {
								//fmt.Println("****map**",maps2[0]["order_no"])
								//fmt.Println("***No error in mapping")
								//fmt.Println("****map**", maps2[0]["order_no"])
								maps[k]["Number"] = maps2[0]["order_no"]
							} else {
								//fmt.Println("Error in QUery")
							}

						} else {
							var maps3 []orm.Params
							//fmt.Println("****Found PO")
							_, err := o.Raw("SELECT batch_no FROM production_orders, production_queue WHERE production_orders._id=production_queue.prod_order_fkid and production_queue._id=? and production_orders._orgid=? and production_queue._orgid=?", maps[k]["_id"], formData.Orgid, formData.Orgid).Values(&maps3)
							if err == nil {
								//fmt.Println(maps3[0]["batch_no"])
								maps[k]["Number"] = maps3[0]["batch_no"]
							}

						}

						key := "po_" + strconv.Itoa(k)
						result[key] = v
					}

				} else {
					//fmt.Println(err1)
					result["status"] = nil
				}
			} else {
				//fmt.Println(err)
				result["status"] = nil
			}
		}

	} else {
		if formData.Status != "" && formData.Type == "" {
			var maps []orm.Params
			num, err1 := o.Raw("SELECT poa.invoice_narration, poa.product_org_code, pq.qty, pq.status, pq.type, pq.uom, pq._id FROM production_queue as pq, product_org_assoc as poa WHERE poa._id=pq.org_product_assoc_fkid and pq._orgid=? and poa._orgid=? and pq.status=?", formData.Orgid, formData.Orgid, formData.Status).Values(&maps)
			if err1 == nil && num > 0 {
				//fmt.Println(maps[0]["name"])
				var i int64 = 0
				for ; i < num; i++ {
					//fmt.Println(maps[i])
				}
				for k, v := range maps {
					if maps[k]["type"] == "0" {
						//fmt.Println("****Found SO")
						var maps2 []orm.Params
						_, err := o.Raw("SELECT order_no FROM sales_order, production_queue WHERE sales_order._id=production_queue.sales_order_fkid and production_queue._id=? and sales_order._orgid=? and production_queue._orgid=?", maps[k]["_id"], formData.Orgid, formData.Orgid).Values(&maps2)

						if err == nil {
							//fmt.Println("****map**",maps2[0]["order_no"])
							//fmt.Println("***No error in mapping")
							//fmt.Println("****map**", maps2[0]["order_no"])
							maps[k]["Number"] = maps2[0]["order_no"]
						} else {
							//fmt.Println("Error in QUery")
						}

					} else {
						var maps3 []orm.Params
						//fmt.Println("****Found PO")
						_, err := o.Raw("SELECT batch_no FROM production_orders, production_queue WHERE production_orders._id=production_queue.prod_order_fkid and production_queue._id=? and production_orders._orgid=? and production_queue._orgid=?", maps[k]["_id"], formData.Orgid, formData.Orgid).Values(&maps3)
						if err == nil {
							//fmt.Println(maps3[0]["batch_no"])
							maps[k]["Number"] = maps3[0]["batch_no"]
						}

					}

					key := "po_" + strconv.Itoa(k)
					result[key] = v
				}

			} else {
				//fmt.Println(err1)
				result["status"] = nil
			}
		} else if formData.Status == "" && formData.Type != "" {
			var maps []orm.Params
			num, err1 := o.Raw("SELECT poa.invoice_narration, poa.product_org_code, pq.qty, pq.status, pq.type, pq.uom, pq._id FROM production_queue as pq, product_org_assoc as poa WHERE poa._id=pq.org_product_assoc_fkid and pq._orgid=? and poa._orgid=? and pq.type=?", formData.Orgid, formData.Orgid, formData.Type).Values(&maps)
			if err1 == nil && num > 0 {
				//fmt.Println(maps[0]["name"])
				var i int64 = 0
				for ; i < num; i++ {
					//fmt.Println(maps[i])
				}
				for k, v := range maps {
					if maps[k]["type"] == "0" {
						//fmt.Println("****Found SO")
						var maps2 []orm.Params
						_, err := o.Raw("SELECT order_no FROM sales_order, production_queue WHERE sales_order._id=production_queue.sales_order_fkid and production_queue._id=? and sales_order._orgid=? and production_queue._orgid=?", maps[k]["_id"], formData.Orgid, formData.Orgid).Values(&maps2)

						if err == nil {
							//fmt.Println("****map**",maps2[0]["order_no"])
							//fmt.Println("***No error in mapping")
							//fmt.Println("****map**", maps2[0]["order_no"])
							maps[k]["Number"] = maps2[0]["order_no"]
						} else {
							//fmt.Println("Error in QUery")
						}

					} else {
						var maps3 []orm.Params
						//fmt.Println("****Found PO")
						_, err := o.Raw("SELECT batch_no FROM production_orders, production_queue WHERE production_orders._id=production_queue.prod_order_fkid and production_queue._id=? and production_orders._orgid=? and production_queue._orgid=?", maps[k]["_id"], formData.Orgid, formData.Orgid).Values(&maps3)
						if err == nil {
							//fmt.Println(maps3[0]["batch_no"])
							maps[k]["Number"] = maps3[0]["batch_no"]
						}

					}

					key := "po_" + strconv.Itoa(k)
					result[key] = v
				}

			} else {
				//fmt.Println(err1)
				result["status"] = nil
			}
		} else {
			var maps []orm.Params
			num, err1 := o.Raw("SELECT poa.invoice_narration, poa.product_org_code, pq.qty, pq.status, pq.type, pq.uom, pq._id FROM production_queue as pq, product_org_assoc as poa WHERE poa._id=pq.org_product_assoc_fkid and pq._orgid=? and poa._orgid=? and pq.status=? and pq.type=?", formData.Orgid, formData.Orgid, formData.Status, formData.Type).Values(&maps)
			if err1 == nil && num > 0 {
				//fmt.Println(maps[0]["name"])
				var i int64 = 0
				for ; i < num; i++ {
					//fmt.Println(maps[i])
				}
				for k, v := range maps {
					if maps[k]["type"] == "0" {
						//fmt.Println("****Found SO")
						var maps2 []orm.Params
						_, err := o.Raw("SELECT order_no FROM sales_order, production_queue WHERE sales_order._id=production_queue.sales_order_fkid and production_queue._id=? and sales_order._orgid=? and production_queue._orgid=?", maps[k]["_id"], formData.Orgid, formData.Orgid).Values(&maps2)

						if err == nil {
							//fmt.Println("****map**",maps2[0]["order_no"])
							//fmt.Println("***No error in mapping")
							//fmt.Println("****map**", maps2[0]["order_no"])
							maps[k]["Number"] = maps2[0]["order_no"]
						} else {
							//fmt.Println("Error in QUery")
						}

					} else {
						var maps3 []orm.Params
						//fmt.Println("****Found PO")
						_, err := o.Raw("SELECT batch_no FROM production_orders, production_queue WHERE production_orders._id=production_queue.prod_order_fkid and production_queue._id=? and production_orders._orgid=? and production_queue._orgid=?", maps[k]["_id"], formData.Orgid, formData.Orgid).Values(&maps3)
						if err == nil {
							//fmt.Println(maps3[0]["batch_no"])
							maps[k]["Number"] = maps3[0]["batch_no"]
						}

					}

					key := "po_" + strconv.Itoa(k)
					result[key] = v
				}

			} else {
				//fmt.Println(err1)
				result["status"] = nil
			}
		}
	}

	return result
}

func GetProductQueue(formData ProductQueue) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT * FROM product_org_assoc where product_org_assoc._orgid=? ", formData.Orgid).Values(&maps)
	if err == nil && num > 0 {
		var i int64 = 0
		for ; i < num; i++ {
			//fmt.Println(maps[i])
		}
		for k, v := range maps {
			key := "product_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

//ACTIVE : USE THIS FUNCTION
func ChangeProductionOrderStatus(production_order_id string, product_status string, OrgId string, UserId string) int {

	o := orm.NewOrm()
	ormTransactionalErr := o.Begin()

	var maps []orm.Params
	num, err := o.Raw("SELECT _id, batch_no, order_date, item_org_assoc_fkid, qty, uom, start_date, end_date, last_updated_date, added_by_user_account_fkid, notes, status, machine_name, geo_location, production_order_unique_code FROM production_orders WHERE _id=? and _orgid=? ", production_order_id, OrgId).Values(&maps)
	if err != nil || num < 1 {
		return 0
	}

	pFkId := maps[0]["_id"].(string)
	productionOrderFkId, _ := strconv.ParseInt(pFkId, 10, 64)

	// prodQuantity := maps[0]["qty"].(string)
	// productionQuantity, _ := strconv.ParseFloat(prodQuantity, 64)

	//WHEN PRODUCTION ORDER IS COMPLETED THE ITEMS SHOULD BE CONSUMED FROM ITEMS STOCK AND ITEMS INVENTORY
	// ALSO THE ITEM PRODUCED SHOULD BE ADDED TO ITEM STOCK AND INVENTORY
	if product_status == "3" {
		// GET ALL THE ITEM STOCK ITEMS ASSIGNED FOR THE PRODUCTION ORDER BASED ON THE ID

		itemDetailsForOrder := ItemDetailsForOrder{
			OrgId:             OrgId,
			ConsumedTableFkId: productionOrderFkId,
			BlockedBy:         1,
			Location:          maps[0]["geo_location"].(string),
		}

		insertQueryStatus := ConsumeAllItemsOnOrderCompleted(itemDetailsForOrder) // THIS UPDATES ITEM STOCK
		if insertQueryStatus == 0 {
			ormTransactionalErr = o.Rollback()
			return 0
		} else {
			//ONCE CONSUMPTION IS DONE UPDATE TOTAL INVENTORY
			//GET ALL PRODUCTION ORDER ITEMS BASED ON THE PRODUCTION ORDER ID

			var productionOrderItems []orm.Params
			count, error := o.Raw("SELECT item_org_assoc_fkid, geo_location from production_order_items where _orgid =? and  geo_location =? and production_order_fkid=? ", OrgId, maps[0]["geo_location"], productionOrderFkId).Values(&productionOrderItems)

			// queryStr := "SELECT item_org_assoc_fkid, geo_location from production_order_items where _orgid = " + OrgId + " and  geo_location = " + maps[0]["geo_location"].(string) + " and production_order_fkid= " + strconv.FormatInt(productionOrderFkId, 10) + " group by item_org_assoc_fkid"

			// fmt.Println("query==============", queryStr)

			// count, error := o.Raw("SELECT sum(quantity) as requiredQty, item_org_assoc_fkid, geo_location from production_order_items where _orgid =? and  geo_location =? and production_order_fkid=? group by item_org_assoc_fkid", OrgId, maps[0]["geo_location"], productionOrderFkId).Values(&productionOrderItems)
			// fmt.Println("count=", count)
			// fmt.Println("error=", error)
			if count > 0 && error == nil {
				for _, item := range productionOrderItems {
					//fmt.Println("ItemID================================", item["item_org_assoc_fkid"].(string))

					insertInvStatus := ItemTotalInventoryUpdateByPkid(item["item_org_assoc_fkid"].(string), OrgId, maps[0]["geo_location"].(string))

					if insertInvStatus == 0 {
						ormTransactionalErr = o.Rollback()
						return 0
					}

					// itemid := item["item_org_assoc_fkid"].(string)
					// itemID, _ := strconv.ParseInt(itemid, 10, 64)

					// qty := item["requiredQty"].(string)
					// itemrequiredQty, _ := strconv.ParseFloat(qty, 64)

					// totalQuantity := productionQuantity * itemrequiredQty

					// //UPDATE THE ITEM TOTAL INVENTORY
					// itemInventoryData := ItemTotalInventory{
					// 	OrgId:            OrgId,
					// 	Itemid:           itemID,
					// 	QuantityModified: totalQuantity,
					// 	Location:         maps[0]["geo_location"].(string),
					// 	AddRemoveFlag:    0,
					// }

					// insertInvStatus := InsertUpdateItemTotalInventory(itemInventoryData)
					// if insertInvStatus == 0 {
					// 	ormTransactionalErr = o.Rollback()
					// 	return 0
					// }
				}
			}
		}
		//ADD THE PRODUCT TO ITEM STOCK AND ITEM INVENTORY
		itemId := maps[0]["item_org_assoc_fkid"].(string)
		productID, _ := strconv.ParseInt(itemId, 10, 64)
		production_order_id = maps[0]["_id"].(string)
		productionOrderFkid, _ := strconv.ParseInt(production_order_id, 10, 64)
		qty := maps[0]["qty"].(string)
		quantity, _ := strconv.ParseFloat(qty, 64)
		location := maps[0]["geo_location"].(string)

		itemstockDetails := ItemStockTable{
			OrgId:            OrgId,
			Itemid:           productID,
			Location:         location,
			InsertTableFkid:  productionOrderFkid,
			QuantityModified: quantity,
			InsertStatus:     1,
			StartDate:        time.Now(),
			EndDate:          time.Now(),
			ExpiresBy:        time.Now(), // setting it temporarily as there is no provision on UI
			BatchNo:          maps[0]["batch_no"].(string),
		}

		insertStatus := InsertUpdateItemStock(itemstockDetails)
		//UPDATE THE ITEM TOTAL INVENTORY
		if insertStatus == 1 {
			itemInventoryData := ItemTotalInventory{
				OrgId:            OrgId,
				Itemid:           productID,
				QuantityModified: quantity,
				Location:         location,
				AddRemoveFlag:    1,
			}

			insertInvStatus := InsertUpdateItemTotalInventory(itemInventoryData)
			//insertInvStatus := ItemTotalInventoryUpdateByPkid(strconv.FormatInt(productID, 10), OrgId, location)
			if insertInvStatus == 0 {
				ormTransactionalErr = o.Rollback()
				return 0
			}
		} else {
			ormTransactionalErr = o.Rollback()
			return 0
		}
	}

	//WHEN REJECT, UNBLOCK ITEMS FOR THAT PRODUCTION
	if product_status == "4" {
		//GET ALL PRODUCTION ORDER ITEMS BASED ON THE PRODUCTION ORDER ID
		var productionOrderItems []orm.Params
		count, error := o.Raw("SELECT sum(quantity) as requiredQty, item_org_assoc_fkid, geo_location from production_order_items where _orgid =? and  geo_location =? and production_order_fkid=? group by item_org_assoc_fkid", OrgId, maps[0]["geo_location"], productionOrderFkId).Values(&productionOrderItems)

		if count > 0 && error == nil {
			for _, item := range productionOrderItems {

				itemid := item["item_org_assoc_fkid"].(string)
				itemID, _ := strconv.ParseInt(itemid, 10, 64)

				itemstockDetails := ItemStockTable{
					OrgId:             OrgId,
					Itemid:            itemID,
					Location:          maps[0]["geo_location"].(string),
					IsBlocked:         0,
					ConsumedTableFkid: productionOrderFkId,
					QuantityModified:  -1,
				}
				insertStatus := InsertUpdateItemStock(itemstockDetails)
				if insertStatus == 0 {
					ormTransactionalErr = o.Rollback()
					return 0
				}
			}
		}
	}

	_, err0 := o.Raw("Update production_orders set status=? WHERE _id=? and _orgid=? ", product_status, production_order_id, OrgId).Exec()
	if err0 != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}

	_, err4 := o.Raw("Update production_order_status set latest=0 where _orgid=? and production_orders_fkid=? and latest=1", OrgId, production_order_id).Exec()
	if err4 != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}

	_, err2 := o.Raw("INSERT INTO production_order_status (_orgid, production_orders_fkid, status,latest,updated_date,updated_by_user_account_fkid) VALUES (?,?,?,?,?,?) ", OrgId, production_order_id, product_status, "1", time.Now(), UserId).Exec()
	if err2 != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}

	if ormTransactionalErr != nil {
		return 0
	}
	ormTransactionalErr = o.Commit()
	return 1

}

func GetAllProductsForDropdowns(orgId string) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT * FROM item_org_assoc as ioa WHERE ioa._orgid=? and ioa.status=1 and produced_internally=1", orgId).Values(&maps)
	if err == nil && num > 0 {
		var i int64 = 0
		for ; i < num; i++ {
			//fmt.Println(maps[i])
		}
		for k, v := range maps {
			key := "product_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

//Do not use. Use GetAllProductsForDropdowns
func GetProducts(formData ModifyBom) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT ioa.invoice_narration, ioa.item_unique_code,ioa.item_code_gen, ioa.unit_measure FROM item_org_assoc as ioa WHERE ioa._orgid=? and ioa.status=? and produced_internally=1", formData.Orgid, 1).Values(&maps)
	if err == nil && num > 0 {
		var i int64 = 0
		for ; i < num; i++ {
			//fmt.Println(maps[i])
		}
		for k, v := range maps {
			key := "product_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetBOMItems(formData ProductionOrder) map[string]orm.Params {

	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	var maps1 []orm.Params
	num, err := o.Raw("SELECT ioa._id,ioa.invoice_narration,bom.quantity,bom.wastage,ioa.unit_measure,ioa.status FROM item_org_assoc as ioa,product_bom as bom where ioa._orgid=? and bom._orgid=? and bom.item_org_assoc_fkid=ioa._id and bom.item_product_org_assoc_fkid=? and bom.status=? ", formData.Orgid, formData.Orgid, formData.Product, 1).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		// var i int64 = 0
		// for ; i < num; i++ {
		// 	fmt.Println(maps[i])
		// }
		for k, v := range maps {
			num1, err1 := o.Raw("SELECT iti.total_quantity,iti.geo_location FROM item_org_assoc as ioa,item_total_inventory as iti where ioa._orgid=? and iti._orgid=? and iti.item_org_assoc_fkid=ioa._id and iti.item_org_assoc_fkid=? and iti.status=? and iti.geo_location=? ", formData.Orgid, formData.Orgid, maps[k]["_id"], 1, formData.FactoryLoc).Values(&maps1)
			// var j int64 = 0
			// for ; j < num1; j++ {
			// 	fmt.Println(maps1[j])
			// }
			if err1 == nil && num1 > 0 {
				//fmt.Println("**items", maps1[0]["total_quantity"])
				maps[k]["total_quantity"] = maps1[0]["total_quantity"]

			} else {
				maps[k]["total_quantity"] = "Item not in stock"
			}
			key := "po_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

//FUNCTION FOR SHOWING BOM IN PRODUCTION ORDERS PAGE
func GetBOMProductionOrders(productId string, factoryId string, orgId string) ([]string, []string, []string, []string, []string, []string, []string, string) {

	status := "false"
	var item_pkid []string
	var items []string
	var itemcodes []string
	var qty_used []string
	var qty_wasted []string
	var qty_in_stock []string
	var uom []string
	o := orm.NewOrm()
	var maps []orm.Params
	var maps1 []orm.Params
	num, err := o.Raw("SELECT ioa._id, ioa.item_unique_code, ioa.item_code_gen, ioa.invoice_narration, bom.quantity, bom.wastage, ioa.unit_measure, ioa.status FROM item_org_assoc as ioa, product_bom as bom where ioa._orgid=? and bom._orgid=? and bom.item_org_assoc_fkid=ioa._id and bom.item_product_org_assoc_fkid=? and bom.status=? ", orgId, orgId, productId, 1).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		// var i int64 = 0
		// for ; i < num; i++ {
		// 	fmt.Println(maps[i])
		// }
		for k, _ := range maps {
			// num1, err1 := o.Raw("SELECT iti.total_quantity, iti.geo_location FROM item_org_assoc as ioa, item_total_inventory as iti where ioa._orgid=? and iti._orgid=? and iti.item_org_assoc_fkid=ioa._id and iti.item_org_assoc_fkid=? and iti.status=? and iti.geo_location=? and blocked = 0 ", orgId, orgId, maps[k]["_id"], 1, factoryId).Values(&maps1)
			num1, err1 := o.Raw("SELECT sum(iti.quantity) as total_quantity FROM item_org_assoc as ioa, item_stock as iti where ioa._orgid=? and iti._orgid=? and iti.item_org_assoc_fkid=ioa._id and iti.item_org_assoc_fkid=? and iti.latest=? and iti.geo_location=? and blocked = 0 ", orgId, orgId, maps[k]["_id"], 1, factoryId).Values(&maps1)
			// var j int64 = 0
			// for ; j < num1; j++ {
			// 	fmt.Println(maps1[j])
			// }
			if err1 == nil && num1 > 0 && maps1[0]["total_quantity"] != nil {
				//fmt.Println("**id", maps[k]["_id"])
				//fmt.Println("**items", maps1[0]["total_quantity"])
				maps[k]["total_quantity"] = maps1[0]["total_quantity"].(string)

			} else {
				maps[k]["total_quantity"] = "0.00000"
			}
			// key := "po_" + strconv.Itoa(k)
			// result[key] = v
		}

		var new_item_pkid string
		var new_items string
		var new_itemcodes string
		var new_qty_used string
		var new_qty_wasted string
		var new_qty_in_stock string
		var new_uom string

		var j int64 = 0
		for ; j < num; j++ {
			new_item_pkid = maps[j]["_id"].(string)
			item_pkid = append(item_pkid, new_item_pkid)

			new_itemcodes = maps[j]["item_code_gen"].(string)
			itemcodes = append(itemcodes, new_itemcodes)

			new_items = maps[j]["invoice_narration"].(string)
			items = append(items, new_items)

			if maps[j]["quantity"] != nil {
				new_qty_used = maps[j]["quantity"].(string)
			} else {
				new_qty_used = "0"
			}
			qty_used = append(qty_used, new_qty_used)

			if maps[j]["wastage"] != nil {
				new_qty_wasted = maps[j]["wastage"].(string)
			} else {
				new_qty_wasted = "0"
			}
			qty_wasted = append(qty_wasted, new_qty_wasted)

			if maps[j]["total_quantity"] != nil {
				new_qty_in_stock = maps[j]["total_quantity"].(string)
			} else {
				new_qty_in_stock = "0.00000"
			}
			qty_in_stock = append(qty_in_stock, new_qty_in_stock)

			if maps[j]["unit_measure"] != nil {
				new_uom = maps[j]["unit_measure"].(string)
			} else {
				new_uom = ""
			}
			uom = append(uom, new_uom)

		}
		status = "true"
	}
	//fmt.Println("***total_after_tax=", total_after_tax)
	//fmt.Println("***quantity=", quantity)
	return item_pkid, items, itemcodes, qty_used, qty_wasted, qty_in_stock, uom, status
}

//FUNCTION FOR SHOWING BOM IN MODAL
func GetBOMFromProductionOrderItems(production_id string, factoryId string, orgId string) ([]string, []string, []string, []string, []string, []string, []string, string, string) {

	status := "false"
	var item_pkid []string
	var items []string
	var itemcodes []string
	var qty_used []string
	var qty_wasted []string
	var qty_in_stock []string
	var uom []string
	o := orm.NewOrm()

	only_code := "1"
	var onlyCodemaps []orm.Params
	codenum, err01 := o.Raw("SELECT only_code FROM production_orders WHERE _orgid=? and _id=? ", orgId, production_id).Values(&onlyCodemaps)
	if codenum > 0 && err01 == nil {
		if onlyCodemaps[0]["only_code"] != nil && onlyCodemaps[0]["only_code"] != "" {
			only_code = onlyCodemaps[0]["only_code"].(string)
		}
	}
	var maps []orm.Params
	var maps1 []orm.Params
	num, err := o.Raw("SELECT ioa._id, ioa.item_unique_code, ioa.item_code_gen, ioa.invoice_narration, bom.quantity, bom.wastage, ioa.unit_measure, ioa.status FROM item_org_assoc as ioa, production_order_items as bom where ioa._orgid=? and bom._orgid=? and bom.item_org_assoc_fkid=ioa._id and bom.production_order_fkid=? and bom.status=? ", orgId, orgId, production_id, 1).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		// var i int64 = 0
		// for ; i < num; i++ {
		// 	fmt.Println(maps[i])
		// }
		for k, _ := range maps {
			num1, err1 := o.Raw("SELECT iti.total_quantity, iti.geo_location FROM item_org_assoc as ioa, item_total_inventory as iti where ioa._orgid=? and iti._orgid=? and iti.item_org_assoc_fkid=ioa._id and iti.item_org_assoc_fkid=? and iti.status=? and iti.geo_location=? and blocked = 0 ", orgId, orgId, maps[k]["_id"], 1, factoryId).Values(&maps1)
			// var j int64 = 0
			// for ; j < num1; j++ {
			// 	fmt.Println(maps1[j])
			// }
			if err1 == nil && num1 > 0 {
				//fmt.Println("**items", maps1[0]["total_quantity"])
				maps[k]["total_quantity"] = maps1[0]["total_quantity"]

			} else {
				maps[k]["total_quantity"] = "Item not in stock"
			}
			// key := "po_" + strconv.Itoa(k)
			// result[key] = v
		}

		var new_item_pkid string
		var new_items string
		var new_itemcodes string
		var new_qty_used string
		var new_qty_wasted string
		var new_qty_in_stock string
		var new_uom string

		var j int64 = 0
		for ; j < num; j++ {
			new_item_pkid = maps[j]["_id"].(string)
			item_pkid = append(item_pkid, new_item_pkid)

			new_itemcodes = maps[j]["item_code_gen"].(string)
			itemcodes = append(itemcodes, new_itemcodes)

			new_items = maps[j]["invoice_narration"].(string)
			items = append(items, new_items)

			if maps[j]["quantity"] != nil {
				new_qty_used = maps[j]["quantity"].(string)
			} else {
				new_qty_used = "0"
			}
			qty_used = append(qty_used, new_qty_used)

			if maps[j]["wastage"] != nil {
				new_qty_wasted = maps[j]["wastage"].(string)
			} else {
				new_qty_wasted = "0"
			}
			qty_wasted = append(qty_wasted, new_qty_wasted)

			if maps[j]["total_quantity"] != nil {
				new_qty_in_stock = maps[j]["total_quantity"].(string)
			} else {
				new_qty_in_stock = "0"
			}
			qty_in_stock = append(qty_in_stock, new_qty_in_stock)

			if maps[j]["unit_measure"] != nil {
				new_uom = maps[j]["unit_measure"].(string)
			} else {
				new_uom = ""
			}
			uom = append(uom, new_uom)
		}
		status = "true"
	}
	//fmt.Println("***total_after_tax=", total_after_tax)
	//fmt.Println("***quantity=", quantity)
	return item_pkid, items, itemcodes, qty_used, qty_wasted, qty_in_stock, uom, status, only_code
}

func GetProductionOrderFormDetails(batchNo string, productId string, orgId string) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT ioa._id, ioa.item_unique_code, ioa.item_code_gen, ioa.invoice_narration, ioa.unit_measure, orf.factory_location, orf._id, po._id as poPkid, po.batch_no, po.order_date as o_date, po.qty, po.start_date, po.end_date, po.geo_location, m.machine_name FROM item_org_assoc as ioa, organisation_factories as orf, production_orders as po, machine as m WHERE ioa._orgid=? and orf._orgid=? and po._orgid=? and m._orgid=? and po.batch_no=? and po.item_org_assoc_fkid=? and ioa._id=? and orf._id=po.geo_location and m._pkid=po.machine_name ", orgId, orgId, orgId, orgId, batchNo, productId, productId).Values(&maps)
	if err == nil && num > 0 {
		for k, v := range maps {
			if maps[k]["o_date"] != nil && maps[k]["o_date"] != "" {
				maps[k]["o_date"] = convertDateString(maps[k]["o_date"].(string))
			}
			if maps[k]["start_date"] != nil && maps[k]["start_date"] != "" {
				s := strings.Split(maps[k]["start_date"].(string), " ")
				maps[k]["start_date"] = convertDateString(maps[k]["start_date"].(string)) + " " + s[1]
			}
			if maps[k]["end_date"] != nil && maps[k]["end_date"] != "" {
				s := strings.Split(maps[k]["end_date"].(string), " ")
				maps[k]["end_date"] = convertDateString(maps[k]["end_date"].(string)) + " " + s[1]
			}
			key := "items_" + strconv.Itoa(k)
			result[key] = v
		}
	} else {
		result["status"] = nil
	}
	return result
}

func GetProductionEditProductionOrderItems(productionOrderFkid string, orgId string) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT ioa._id, ioa.item_unique_code, ioa.item_code_gen, ioa.invoice_narration, bom.quantity, bom.wastage, ioa.unit_measure, ioa.status, bom.geo_location FROM item_org_assoc as ioa, production_order_items as bom where ioa._orgid=? and bom._orgid=? and bom.item_org_assoc_fkid=ioa._id and bom.production_order_fkid=? and bom.status=? ", orgId, orgId, productionOrderFkid, 1).Values(&maps)

	if err == nil && num > 0 {
		for k, v := range maps {
			var batchmaps []orm.Params
			count, err1 := o.Raw("SELECT _id as batchId, batch_no FROM item_stock where blocked = 0 and _orgid=? and item_org_assoc_fkid=? and geo_location= ? and latest = 1 ", orgId, maps[k]["_id"], maps[k]["geo_location"]).Values(&batchmaps)
			if count > 0 && err1 == nil {
				maps[k]["batches"] = batchmaps
			}

			key := "items_" + strconv.Itoa(k)
			result[key] = v
		}
	} else {
		result["status"] = nil
	}
	return result
}

//Adding this for a edit bom fix : 8th dec 2017
func GetProductionOrderItemsFromProductBOM(productionOrderFkid string, orgId string) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT ioa._id, ioa.item_unique_code, ioa.item_code_gen, ioa.invoice_narration,bom.quantity, bom.wastage, ioa.unit_measure, ioa.status, po.geo_location FROM item_org_assoc as ioa, product_bom as bom, production_orders as po where ioa._orgid=? and bom._orgid=?  and po._id = ? and bom.item_product_org_assoc_fkid = po.item_org_assoc_fkid and ioa._id = bom.item_org_assoc_fkid ", orgId, orgId, productionOrderFkid).Values(&maps)

	if err == nil && num > 0 {
		for k, v := range maps {
			var batchmaps []orm.Params

			//should be removed after testing
			count, err1 := o.Raw("SELECT _id as batchId, batch_no FROM item_stock where blocked = 0 and _orgid=? and item_org_assoc_fkid=? and geo_location= ? ", orgId, maps[k]["_id"], maps[k]["geo_location"]).Values(&batchmaps)
			if count > 0 && err1 == nil {
				maps[k]["batches"] = batchmaps
			}

			key := "items_" + strconv.Itoa(k)
			result[key] = v
		}
	} else {
		result["status"] = nil
	}
	return result
}

func GetBomItems(formData ModifyBom) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	if formData.ProductName != "" {
		num, err := o.Raw("SELECT  pb.non_consumable, pb.quantity, pb.product_narration as item_narration,  ioa.item_code_gen as product_code,ioa.unit_measure, pb.status, pb._id, pb.wastage FROM  product_bom as pb, item_org_assoc as ioa WHERE  ioa._orgid=? and pb._orgid=? and pb.item_product_org_assoc_fkid=? and pb.item_org_assoc_fkid = ioa._id ORDER BY pb.status DESC", formData.Orgid, formData.Orgid, formData.ProductName).Values(&maps)
		if err == nil && num > 0 {
			var i int64 = 0
			for ; i < num; i++ {
				// fmt.Println(maps[i])
			}
			for k, v := range maps {
				if maps[k]["non_consumable"] == "1" {
					maps[k]["non_consumable"] = "non_consumable"
				} else {
					maps[k]["non_consumable"] = "consumable"
				}
				key := "product_" + strconv.Itoa(k)
				result[key] = v
			}

		} else {
			//fmt.Println(err)
			result["status"] = nil
		}
	} else {
		//fmt.Println("Product Name or Code required")
		result["status"] = nil
	}

	return result
}

func GetBomItemsByGet(itemid string, orgid string) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params

	num, err := o.Raw("SELECT  pb.non_consumable,pb.quantity, pb.product_narration as item_narration,ioa.item_code_gen as product_code, ioa.unit_measure, pb.status, pb._id, pb.wastage FROM  product_bom as pb, item_org_assoc as ioa WHERE  ioa._orgid=? and pb._orgid=? and ioa._id=? and pb.item_product_org_assoc_fkid=ioa._id  ORDER BY pb.status DESC", orgid, orgid, itemid).Values(&maps)
	if err == nil && num > 0 {
		var i int64 = 0
		for ; i < num; i++ {
			// fmt.Println(maps[i])
		}
		for k, v := range maps {
			if maps[k]["non_consumable"] == "1" {
				maps[k]["non_consumable"] = "non_consumable"
			} else {
				maps[k]["non_consumable"] = "consumable"
			}
			key := "product_" + strconv.Itoa(k)
			result[key] = v
		}
	} else {

	}

	return result
}
func GetBomItemsByName(itemid string, orgid string) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params

	// num, err := o.Raw("SELECT  pb.non_consumable,pb.quantity, pb.product_narration as item_narration, ioa.item_code_gen as product_code, ioa.unit_measure, pb.status, pb._id, pb.wastage FROM  product_bom as pb, item_org_assoc as ioa WHERE  ioa._orgid=? and pb._orgid=? and ioa._id=? and pb.item_product_org_assoc_fkid=ioa._id  ORDER BY pb.status DESC", orgid, orgid, itemid).Values(&maps)

	num, err := o.Raw("SELECT itemOrg.name as item_narration,  product_bom.non_consumable, product_bom.quantity,product_bom.wastage,itemOrg.item_code_gen as product_code, itemOrg.unit_measure, product_bom.status, product_bom._id FROM product_bom , item_org_assoc as itemOrg WHERE item_product_org_assoc_fkid=? and product_bom._orgid=? and itemOrg._orgid = ? and itemOrg._id = item_org_assoc_fkid", orgid, orgid, itemid).Values(&maps)

	if err == nil && num > 0 {
		var i int64 = 0
		for ; i < num; i++ {
			// fmt.Println(maps[i])
		}
		for k, v := range maps {
			if maps[k]["non_consumable"] == "1" {
				maps[k]["non_consumable"] = "non_consumable"
			} else {
				maps[k]["non_consumable"] = "consumable"
			}
			key := "product_" + strconv.Itoa(k)
			result[key] = v
		}
	} else {

	}

	return result
}

// Use this function
func GetBomItemsByProductID(productId string, orgID string) (int64, map[string]orm.Params) {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params

	num, err := o.Raw("SELECT  pb.non_consumable, pb.quantity, ioa.name as item_narration,  ioa.item_code_gen as product_code,ioa.unit_measure, pb.status, pb._id, pb.wastage FROM  product_bom as pb, item_org_assoc as ioa WHERE  ioa._orgid=? and pb._orgid=? and pb.item_product_org_assoc_fkid=? and pb.item_org_assoc_fkid = ioa._id ORDER BY pb.status DESC", orgID, orgID, productId).Values(&maps)
	if err == nil && num > 0 {
		for k, v := range maps {
			if maps[k]["non_consumable"] == "1" {
				maps[k]["non_consumable"] = "Non consumable"
			} else {
				maps[k]["non_consumable"] = "Consumable"
			}
			key := "product_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		result["status"] = nil
	}

	return num, result
}

func DeleteBomItems(item_id string, orgid string) string {
	result := "false"
	o := orm.NewOrm()
	//var values []orm.Params
	_, err := o.Raw("UPDATE product_bom SET status=? WHERE _orgid=? and _id=? ", "0", orgid, item_id).Exec()
	if err != nil {
		//fmt.Println("Error in UPDATE")
	} else {
		result = "true"
	}
	return result
}

func RestoreBomItems(item_id string, orgid string) string {
	result := "false"
	o := orm.NewOrm()
	//var values []orm.Params
	_, err := o.Raw("UPDATE product_bom SET status=? WHERE _orgid=? and _id=? ", "1", orgid, item_id).Exec()
	if err != nil {
		//fmt.Println("Error in UPDATE")
	} else {
		result = "true"
	}
	return result
}

func ChangeBomItems(item_id string, new_qty string, new_wastage string, orgid string) string {
	result := "false"
	o := orm.NewOrm()
	//var values []orm.Params
	_, err := o.Raw("UPDATE product_bom SET quantity=?, wastage=? WHERE _orgid=? and _id=? ", new_qty, new_wastage, orgid, item_id).Exec()
	if err != nil {
		//fmt.Println("Error in UPDATE")
	} else {
		result = "true"
	}
	return result
}

func GetUOM(item_id string, orgid string) (string, string) {

	status := "false"

	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT unit_measure FROM item_org_assoc WHERE invoice_narration=? and _orgid=? ", item_id, orgid).Values(&maps)

	var uom string
	if err == nil && num > 0 {
		uom = maps[0]["unit_measure"].(string)
		status = "true"
	}
	//fmt.Println("***UOM=", uom)
	return uom, status
}

func GetQuantityForSelectedBatchNo(batchNo string, orgid string, location string) (string, string) {

	status := "false"

	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT sum(quantity) as quantity FROM item_stock WHERE batch_no= ? and _orgid=? and geo_location= ? and latest = 1 and  blocked =0 group by batch_no", batchNo, orgid, location).Values(&maps)

	var qty string
	if err == nil && num > 0 {
		qty = maps[0]["quantity"].(string)
		status = "true"
	}
	return qty, status
}

func InsertNewItem(product_name string, product_code string, new_item string, new_qty string, new_wastage string, formData ModifyBom) string {
	var product_id string
	var item_id string
	o := orm.NewOrm()
	var maps []orm.Params
	if product_name != "" && product_code == "" {
		num, err := o.Raw("SELECT _id FROM product_org_assoc as poa WHERE poa._orgid=? and poa.invoice_narration=? ", formData.Orgid, product_name).Values(&maps)
		if err == nil && num > 0 {
			product_id = maps[0]["_id"].(string)
		} else {
			return "0"
		}
	} else if product_name == "" && product_code != "" {
		num, err := o.Raw("SELECT _id FROM product_org_assoc as poa WHERE poa._orgid=? and poa.product_org_code=? ", formData.Orgid, product_code).Values(&maps)
		if err == nil && num > 0 {
			product_id = maps[0]["_id"].(string)
		} else {
			return "0"
		}
	} else if product_name != "" && product_code != "" {
		num, err := o.Raw("SELECT _id FROM product_org_assoc as poa WHERE poa._orgid=? and poa.invoice_narration=? and poa.product_org_code=? ", formData.Orgid, product_name, product_code).Values(&maps)
		if err == nil && num > 0 {
			product_id = maps[0]["_id"].(string)
		} else {
			return "0"
		}
	} else {
		//fmt.Println("ProductName or Code required")
		return "2"
	}

	var maps1 []orm.Params
	count, err1 := o.Raw("SELECT _id FROM item_org_assoc as ioa WHERE ioa._orgid=? and ioa.invoice_narration=? ", formData.Orgid, new_item).Values(&maps1)
	if err1 == nil && count > 0 {
		item_id = maps1[0]["_id"].(string)
	} else {
		return "0"
	}

	_, err2 := o.Raw("INSERT INTO product_bom (quantity, product_org_assoc_fkid, item_org_assoc_fkid, wastage, status, _orgid) VALUES (?,?,?,?,?,?) ", new_qty, product_id, item_id, new_wastage, 1, formData.Orgid).Exec()

	if err2 == nil {
		return "1"
	} else {
		return "0"
	}
}

func GetProductDetailsByName(productName string, orgID string) Product {
	o := orm.NewOrm()
	var maps []orm.Params
	var maps1 []orm.Params
	maps3 := make([]string, 0)
	var itemStruct = Product{}
	num, err := o.Raw("SELECT poa._id, poa.minimum_stock_keep, poa.transport, poa.process_name, poa.status, poa.geo_location, poa.product_org_name, poa.invoice_narration, poa.product_org_code, poa.product_type, poa.grp, poa.subgroup, poa.capacity_measure, poa.unit_of_measure, poa.rate_per_UOM FROM product_org_assoc as poa where poa._orgid =?  and poa.product_org_name = ?", orgID, productName).Values(&maps)
	if err != nil || num == 0 {
		itemStruct.IsPresentinDB = 0
	} else {
		itemStruct.ProductID = maps[0]["_id"].(string)
		itemStruct.ProductName = maps[0]["product_org_name"].(string)
		itemStruct.InvoiceNarration = maps[0]["invoice_narration"].(string)
		itemStruct.ProductCode = maps[0]["product_org_code"].(string)
		itemStruct.ProductType = maps[0]["product_type"].(string)
		itemStruct.Group = maps[0]["grp"].(string)
		itemStruct.SubGroup = maps[0]["subgroup"].(string)
		itemStruct.Capacity = maps[0]["capacity_measure"].(string)
		itemStruct.UOM = maps[0]["unit_of_measure"].(string)
		itemStruct.RatePerUOM = maps[0]["rate_per_UOM"].(string)
		itemStruct.MinStock = maps[0]["minimum_stock_keep"].(string)
		itemStruct.Transport = maps[0]["transport"].(string)
		itemStruct.ProductProcess = maps[0]["process_name"].(string)
		itemStruct.Status1 = maps[0]["status"].(string)
		//itemStruct.FactoryLoc = maps[0]["factory_location"].(string)
		var tags1 []string

		tags1 = strings.Split(maps[0]["geo_location"].(string), ",")
		for i := 0; i < len(tags1); i++ {
			if tags1[i] == "" {

			} else {
				_, err0 := o.Raw("SELECT factory_location FROM organisation_factories WHERE _id=? and  _orgid=? and status = 1 ", tags1[i], orgID).Values(&maps1)
				if err0 == nil {

					maps3 = append(maps3, maps1[0]["factory_location"].(string))

				}
			}
		}
		itemStruct.FactoryLoc = maps3
		itemStruct.IsPresentinDB = 1
	}
	return itemStruct
}

func GetProductLocationDetailsByName(productName string, orgID string) []string {
	o := orm.NewOrm()
	var maps []orm.Params
	var maps1 []orm.Params
	var itemStruct = Product{}
	maps3 := make([]string, 0)
	num, err := o.Raw("SELECT poa.geo_location from product_org_assoc as poa where poa._orgid =?  and poa.product_org_name = ? ", orgID, productName).Values(&maps)
	if err != nil || num == 0 {
		itemStruct.IsPresentinDB = 0
	} else {

		var tags1 []string

		tags1 = strings.Split(maps[0]["geo_location"].(string), ",")
		for i := 0; i < len(tags1); i++ {
			if tags1[i] == "" {

			} else {
				_, err0 := o.Raw("SELECT factory_location FROM organisation_factories WHERE _id=? and  _orgid=? and status = 1 ", tags1[i], orgID).Values(&maps1)
				if err0 == nil {

					maps3 = append(maps3, maps1[0]["factory_location"].(string))

				}
			}
		}

		itemStruct.IsPresentinDB = 1
	}
	return maps3
}

func CheckForExistingProductName1(productName string, orgID string, productid string) int {
	o := orm.NewOrm()
	var resultMap []orm.Params
	num, err := o.Raw("SELECT product_org_name FROM product_org_assoc WHERE product_org_name=? and  _orgid=? and status = 1 and _id != ?", productName, orgID, productid).Values(&resultMap)
	//fmt.Println("Query Check Name==========", num)
	if err == nil && num > 0 {
		return 1
	}
	return 0
}

func CheckForExistingProductUniqueCode1(productUniqueCode string, orgID string, productid string) int {
	o := orm.NewOrm()
	var resultMap []orm.Params
	num, err := o.Raw("SELECT product_org_code FROM product_org_assoc WHERE product_org_code=? and  _orgid=? and status = 1 and _id != ?", productUniqueCode, orgID, productid).Values(&resultMap)
	//fmt.Println("Query Check Code==========", num)
	if err == nil && num > 0 {
		return 1
	}
	return 0
}

func CheckForExistingProductInvoiceNarration1(productInvoice string, orgID string, productid string) int {
	o := orm.NewOrm()
	var resultMap []orm.Params
	num, err := o.Raw("SELECT invoice_narration FROM product_org_assoc WHERE invoice_narration=? and  _orgid=? and status = 1 and _id != ?", productInvoice, orgID, productid).Values(&resultMap)
	//fmt.Println("Query Check Code==========", num)
	if err == nil && num > 0 {
		return 1
	}
	return 0
}

func UpdateProduct(formData Product) int {

	o := orm.NewOrm()
	_, err5 := o.Raw("UPDATE product_org_assoc SET product_org_code=?, product_org_name=?,invoice_narration=?, rate_per_UOM=?, minimum_stock_keep=?, status=?, grp=?, subgroup=?, unit_of_measure=?, capacity_measure=?, is_finished_product=?, transport=?, process_name=?,product_type=? where _orgid=? and _id=? ", formData.ProductCode, formData.ProductName, formData.InvoiceNarration, formData.RatePerUOM, formData.MinStock, formData.Status1, formData.Group, formData.SubGroup, formData.UOM, formData.Capacity, 1, formData.Transport, formData.ProductProcess, formData.ProductType, formData.Orgid, formData.ProductID).Exec()
	if err5 == nil {
		return 1
	}
	return 0
}

func GetMachines(formData ProductionOrder) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT _pkid,machine_code,machine_name FROM machine WHERE _orgid=? and status=1 and geo_location=? ", formData.Orgid, formData.FactoryLoc).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println("********num :", num)
		var i int64 = 0
		for ; i < num; i++ {
			//fmt.Println(maps[i])
		}
		for k, v := range maps {

			key := "fac_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetGroupProducts(formData Product) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT distinct grp from product_org_assoc where _orgid=?", formData.Orgid).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		var i int64 = 0
		for ; i < num; i++ {
			//fmt.Println(maps[i])
		}
		for k, v := range maps {

			key := "items_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetSubGroupProducts(formData Product) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT distinct subgroup from product_org_assoc where _orgid=?", formData.Orgid).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		var i int64 = 0
		for ; i < num; i++ {
			//fmt.Println(maps[i])
		}
		for k, v := range maps {

			key := "items_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetProductsEditUOM(orgid string) []string {

	var uom []string
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT distinct unit_of_measure from product_org_assoc where _orgid=?", orgid).Values(&maps)

	if err == nil {
		var new_uom string
		var j int64 = 0
		for ; j < num; j++ {
			new_uom = maps[j]["unit_of_measure"].(string)
			uom = append(uom, new_uom)

		}
	}

	return uom
}

func GetProductsEditGroups(orgid string) []string {

	var group []string
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT distinct grp from product_org_assoc where _orgid=?", orgid).Values(&maps)

	if err == nil {
		var new_group string
		var j int64 = 0
		for ; j < num; j++ {
			new_group = maps[j]["grp"].(string)
			group = append(group, new_group)

		}
	}

	return group
}

func GetProductsEditSubgroups(orgid string) []string {

	var subgroup []string
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT distinct subgroup from product_org_assoc where _orgid=?", orgid).Values(&maps)

	if err == nil {
		var new_subgroup string
		var j int64 = 0
		for ; j < num; j++ {
			new_subgroup = maps[j]["subgroup"].(string)
			subgroup = append(subgroup, new_subgroup)

		}
	}

	return subgroup
}

func GetMachinesByFactoryId(factory_id string, orgid string) ([]string, []string, []string, string) {

	status := "false"

	o := orm.NewOrm()
	var maps []orm.Params
	num, err1 := o.Raw("SELECT _pkid, machine_code_gen, machine_name FROM machine WHERE geo_location=? and _orgid=? and status=1", factory_id, orgid).Values(&maps)

	var machine_ids []string
	var machine_codes []string
	var machine_names []string

	if err1 == nil && num > 0 {

		var new_machine_ids string
		var new_machine_codes string
		var new_machine_names string

		var j int64 = 0
		for ; j < num; j++ {
			new_machine_ids = maps[j]["_pkid"].(string)
			machine_ids = append(machine_ids, new_machine_ids)

			new_machine_codes = maps[j]["machine_code_gen"].(string)
			machine_codes = append(machine_codes, new_machine_codes)

			new_machine_names = maps[j]["machine_name"].(string)
			machine_names = append(machine_names, new_machine_names)
		}

		status = "true"
	}

	return machine_ids, machine_codes, machine_names, status
}

func SearchProductionOrdersdetails(ProductName, FactoryLocation, Machine, _Orgid string) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT pos.*, org_f.factory_location FROM (SELECT po._id,po.batch_no,po.order_date as o_date, ioa.name, ioa.invoice_narration ,po.uom,po.qty,po.start_date,po.end_date,po.status,machine.machine_name, po.geo_location FROM production_orders as po, item_org_assoc as ioa, machine WHERE ioa._id=po.item_org_assoc_fkid  and po._orgid=? and ioa._orgid=? and machine._pkid=po.machine_name) AS pos, organisation_factories AS org_f WHERE pos.geo_location = org_f._id and (name like ? and factory_location like ? and machine_name like ?)", _Orgid, _Orgid, "%"+ProductName+"%", "%"+FactoryLocation+"%", "%"+Machine+"%").Values(&maps)
	fmt.Println(num)
	if err == nil && num > 0 {
		for k, v := range maps {
			if maps[k]["o_date"] != nil && maps[k]["o_date"] != "" {
				maps[k]["o_date"] = convertDateString(maps[k]["o_date"].(string))
			}
			if maps[k]["start_date"] != nil && maps[k]["start_date"] != "" {
				maps[k]["start_date"] = convertDateString(maps[k]["start_date"].(string))
			}
			if maps[k]["end_date"] != nil && maps[k]["end_date"] != "" {
				maps[k]["end_date"] = convertDateString(maps[k]["end_date"].(string))
			}

			key := "po_" + strconv.Itoa(k)
			result[key] = v
		}
	} else {
		result["status"] = nil
	}
	return result
}

func SearchProductionOrdersByDateRange(FromOrderDate, ToOrderDate, _orgId string) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	var num int64
	var err error

	if (FromOrderDate == "") && (ToOrderDate == "") {
		num, err = o.Raw("SELECT pos.*, org_f.factory_location FROM (SELECT po._id,po.batch_no,po.order_date as o_date, ioa.name, ioa.invoice_narration ,po.uom,po.qty,po.start_date,po.end_date,po.status,machine.machine_name, po.geo_location FROM production_orders as po, item_org_assoc as ioa, machine WHERE ioa._id=po.item_org_assoc_fkid  and po._orgid=? and ioa._orgid=? and machine._pkid=po.machine_name) AS pos, organisation_factories AS org_f WHERE pos.geo_location = org_f._id ", _orgId, _orgId).Values(&maps)
	}
	if (ToOrderDate == "") && (FromOrderDate != "") {
		num, err = o.Raw("SELECT pos.*, org_f.factory_location FROM (SELECT po._id,po.batch_no,po.order_date as o_date, ioa.name, ioa.invoice_narration ,po.uom,po.qty,po.start_date,po.end_date,po.status,machine.machine_name, po.geo_location FROM production_orders as po, item_org_assoc as ioa, machine WHERE ioa._id=po.item_org_assoc_fkid  and po._orgid=? and ioa._orgid=? and machine._pkid=po.machine_name) AS pos, organisation_factories AS org_f WHERE pos.geo_location = org_f._id and (start_date >= ? )", _orgId, _orgId, FromOrderDate).Values(&maps)
	}
	if (FromOrderDate == "") && (ToOrderDate != "") {
		ToOrderDate = ToOrderDate + " 24:00:00"
		num, err = o.Raw("SELECT pos.*, org_f.factory_location FROM (SELECT po._id,po.batch_no,po.order_date as o_date, ioa.name, ioa.invoice_narration ,po.uom,po.qty,po.start_date,po.end_date,po.status,machine.machine_name, po.geo_location FROM production_orders as po, item_org_assoc as ioa, machine WHERE ioa._id=po.item_org_assoc_fkid  and po._orgid=? and ioa._orgid=? and machine._pkid=po.machine_name) AS pos, organisation_factories AS org_f WHERE pos.geo_location = org_f._id and (end_date <= ? )", _orgId, _orgId, ToOrderDate).Values(&maps)
	}
	if (FromOrderDate != "") && (ToOrderDate != "") {
		ToOrderDate = ToOrderDate + " 24:00:00"
		num, err = o.Raw("SELECT pos.*, org_f.factory_location FROM (SELECT po._id,po.batch_no,po.order_date as o_date, ioa.name, ioa.invoice_narration ,po.uom,po.qty,po.start_date,po.end_date,po.status,machine.machine_name, po.geo_location FROM production_orders as po, item_org_assoc as ioa, machine WHERE ioa._id=po.item_org_assoc_fkid  and po._orgid=? and ioa._orgid=? and machine._pkid=po.machine_name) AS pos, organisation_factories AS org_f WHERE pos.geo_location = org_f._id and (start_date >=? and end_date <=?)", _orgId, _orgId, FromOrderDate, ToOrderDate).Values(&maps)
	}

	if err == nil && num > 0 {
		for k, v := range maps {
			if maps[k]["o_date"] != nil && maps[k]["o_date"] != "" {
				maps[k]["o_date"] = convertDateString(maps[k]["o_date"].(string))
			}
			if maps[k]["start_date"] != nil && maps[k]["start_date"] != "" {
				maps[k]["start_date"] = convertDateString(maps[k]["start_date"].(string))
			}
			if maps[k]["end_date"] != nil && maps[k]["end_date"] != "" {
				maps[k]["end_date"] = convertDateString(maps[k]["end_date"].(string))
			}

			key := "po_" + strconv.Itoa(k)
			result[key] = v
		}
	} else {
		result["status"] = nil
	}
	return result
}

func GetMachineDataById(machine_id string, orgID string) Machine {
	o := orm.NewOrm()
	var maps []orm.Params
	var itemStruct = Machine{}
	num, err := o.Raw("SELECT _pkid, machine_code, machine_name, geo_location, added_on, machine_status FROM machine WHERE _pkid=? and _orgid=?", machine_id, orgID).Values(&maps)
	if err != nil || num == 0 {
		itemStruct.IsPresentinDB = 0
	} else {
		itemStruct.PrevId = maps[0]["_pkid"].(string)
		if maps[0]["machine_code"] == nil {
			itemStruct.MachineCode = ""
		} else {
			itemStruct.MachineCode = maps[0]["machine_code"].(string)
		}
		if maps[0]["machine_name"] == nil {
			itemStruct.MachineName = ""
		} else {
			itemStruct.MachineName = maps[0]["machine_name"].(string)
		}
		if maps[0]["geo_location"] == nil {
			itemStruct.GeoLocation = ""
		} else {
			itemStruct.GeoLocation = maps[0]["geo_location"].(string)
		}
		if maps[0]["added_on"] == nil {
			itemStruct.AddedOn = ""
		} else {
			itemStruct.AddedOn = maps[0]["added_on"].(string)
		}
		if maps[0]["machine_status"] == nil {
			itemStruct.Status = ""
		} else {
			itemStruct.Status = maps[0]["machine_status"].(string)
		}

		itemStruct.IsPresentinDB = 1
	}
	return itemStruct
}

func GetMaxMachineCode(orgid string) int {
	var maxCode int
	maxCode = 1
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT max(machine_code) as maxcode FROM machine where _orgid=?", orgid).Values(&maps)

	if num > 0 && err == nil {
		if maps[0]["maxcode"] != nil {
			code := maps[0]["maxcode"].(string)
			getValue, _ := strconv.ParseInt(code, 10, 64)
			maxCode = int(getValue)
		}
	}
	return maxCode
}

func GetMaxProductionOrderCode(orgid string) int {
	var maxCode int
	maxCode = 1
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT max(production_order_unique_code) as maxcode FROM production_orders where _orgid=?", orgid).Values(&maps)

	if num > 0 && err == nil {
		if maps[0]["maxcode"] != nil {
			code := maps[0]["maxcode"].(string)
			getValue, _ := strconv.ParseInt(code, 10, 64)
			maxCode = int(getValue)
		}
	}
	return maxCode
}

func GetProductIdByProductionOrderId(OrgId string, production_order_id string) (string, string, string) {
	o := orm.NewOrm()
	var location string = ""
	var maps []orm.Params
	num, err := o.Raw("SELECT item_org_assoc_fkid, qty, geo_location FROM production_orders where _orgid=? and _id=?", OrgId, production_order_id).Values(&maps)
	if err == nil && num > 0 {
		var reqQty string
		if maps[0]["qty"] != nil && maps[0]["qty"] != "" {
			reqQty = maps[0]["qty"].(string)
		} else {
			reqQty = "0"
		}
		if maps[0]["geo_location"] != nil {
			location = maps[0]["geo_location"].(string)
		}
		return maps[0]["item_org_assoc_fkid"].(string), reqQty, location
	}
	return "false", "false", "false"
}

func InsertProductionOrderItems(productionOrderItem ProductionOrderItem) int {
	o := orm.NewOrm()

	ormTransactionalErr := o.Begin()
	_, err3 := o.Raw("INSERT INTO production_order_items (_orgid, production_order_fkid,item_org_assoc_fkid,geo_location,quantity,wastage,uom,bom_status) VALUES (?,?,?,?,?,?,?,?) ", productionOrderItem.OrgID, productionOrderItem.ProductionOrderFkID, productionOrderItem.ItemOrgAssocFkID, productionOrderItem.Location, productionOrderItem.Quantity, productionOrderItem.Wastage, productionOrderItem.UOM, productionOrderItem.BomStatus).Exec()
	if err3 != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}
	ormTransactionalErr = o.Commit()
	if ormTransactionalErr != nil {
		return 0
	}
	return 1
}

func GetProductionOrderFkIDFromOrgidBatchNo(orgId string, batchNo string) int {
	var id int
	id = 0
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT _id FROM production_orders WHERE _orgid=? and batch_no=? ", orgId, batchNo).Values(&maps)

	if num > 0 && err == nil {
		if maps[0]["_id"] != nil {
			code := maps[0]["_id"].(string)
			getValue, _ := strconv.ParseInt(code, 10, 64)
			id = int(getValue)
		}
	}
	return id
}

func GetPOdetailsByPkId(poId string, orgId string) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT * FROM production_orders as po, item_org_assoc as ioa, machine as m, organisation_factories as orgf WHERE po._orgid=? and po._id=? and ioa._orgid=? and ioa._id=po.item_org_assoc_fkid and m._orgid=? and m._pkid=po.machine_name and orgf._orgid=? and orgf._id=po.geo_location", orgId, poId, orgId, orgId, orgId).Values(&maps)
	if err == nil && num > 0 {
		for k, v := range maps {
			var poItemsMaps []orm.Params
			count, err1 := o.Raw("SELECT * From production_order_items as poi, item_org_assoc as ioa, item_total_inventory as iti WHERE poi._orgid=? and poi.production_order_fkid=? and poi.status=1 and ioa._id=poi.item_org_assoc_fkid and ioa._orgid=? and iti._orgid=? and iti.item_org_assoc_fkid=poi.item_org_assoc_fkid and iti.status=1 and iti.geo_location=poi.geo_location", orgId, poId, orgId, orgId).Values(&poItemsMaps)
			if count > 0 && err1 == nil {
				maps[k]["poitems"] = poItemsMaps
			}

			// var originLocMaps []orm.Params
			// origincount, err2 := o.Raw("SELECT * From organisation_factories WHERE _orgid=? and _id=?", orgId, maps[0]["origin_location"]).Values(&originLocMaps)
			// if origincount > 0 && err2 == nil {
			// 	maps[k]["origin_factory"] = originLocMaps[0]["factory_location"]
			// }

			// var deliveryLocMaps []orm.Params
			// deliverycount, err3 := o.Raw("SELECT * From organisation_factories WHERE _orgid=? and _id=?", orgId, maps[0]["delivery_location"]).Values(&deliveryLocMaps)
			// if deliverycount > 0 && err3 == nil {
			// 	maps[k]["delivery_factory"] = deliveryLocMaps[0]["factory_location"]
			// }

			key := "po_" + strconv.Itoa(k)
			result[key] = v
		}
	} else {
		result["status"] = nil
	}
	return result
}

func GetProductionOrderItems(productionOrderFkid string, orgId string) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT ioa._id, ioa.item_unique_code, ioa.item_code_gen, ioa.invoice_narration, bom.quantity, bom.wastage, ioa.unit_measure, ioa.status, bom.geo_location FROM item_org_assoc as ioa, production_order_items as bom where ioa._orgid=? and bom._orgid=? and bom.item_org_assoc_fkid=ioa._id and bom.production_order_fkid=? and bom.status=? ", orgId, orgId, productionOrderFkid, 1).Values(&maps)
	if err == nil && num > 0 {
		for k, v := range maps {
			key := "items_" + strconv.Itoa(k)
			result[key] = v
		}
	} else {
		result["status"] = nil
	}
	return result
}

func GetStatusOfProductionOrder(productionOrderFkid string, orgId string) (int, int) {
	o := orm.NewOrm()

	var status int64
	var isBatchAssigned int64
	status = 0
	isBatchAssigned = 0

	var maps []orm.Params
	num, err := o.Raw("SELECT status, manually_assign_batch FROM production_orders where _orgid=? and _id=?", orgId, productionOrderFkid).Values(&maps)
	if err == nil && num > 0 {

		if maps[0]["status"] != nil {
			s := maps[0]["status"].(string)
			status, _ = strconv.ParseInt(s, 10, 64)
		} else {
			status = 0
		}

		if maps[0]["manually_assign_batch"] != nil {
			batchAssigned := maps[0]["manually_assign_batch"].(string)
			isBatchAssigned, _ = strconv.ParseInt(batchAssigned, 10, 64)
		} else {
			isBatchAssigned = 0
		}

		return int(status), int(isBatchAssigned)
	}
	return int(status), int(isBatchAssigned)

}

func UpdateProductionOrderQuantity(productionOrderID string, orgId string, quantity string) int {

	status := 0
	o := orm.NewOrm()

	_, err0 := o.Raw("UPDATE production_orders set qty=? WHERE _id=? and _orgid = ?", quantity, productionOrderID, orgId).Exec()
	if err0 != nil {
		status = 0
	} else {
		status = 1
	}

	return status

}
