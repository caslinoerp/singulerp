package models

import (
	"strconv"
	"time"

	"github.com/astaxie/beego/orm"
)

//This should have all the structs and functions  to be used by all modules

type ItemStockTable struct {
	Id                int64
	OrgId             string
	Itemid            int64
	QuantityModified  float64
	Location          string
	GRNNumber         string
	BatchNo           string
	IsBlocked         int
	InsertStatus      int
	InsertTableFkid   int64
	ConsumedTableFkid int64
	ExpiresBy         time.Time
	EndDate           time.Time
	StartDate         time.Time
	Rate              float64
	Currency          string
	ConsumedQuantity  float64
}

type ItemDetailsForOrder struct {
	OrgId             string
	ItemId            int64
	RequiredQuantity  float64
	Location          string
	BlockedBy         int
	ConsumedTableFkId int64
	BatchNo           string
}

type ItemTotalInventory struct {
	Id               int64
	OrgId            string
	Itemid           int64
	QuantityModified float64
	Location         string
	LowLevel         float64
	AddRemoveFlag    int
}

const layout = "2006-01-02 15:04:05"

//Functions
func InsertUpdateItemStock(itemStockData ItemStockTable) int {
	o := orm.NewOrm()

	ormTransactionalErr := o.Begin()
	var itemstockDetails []orm.Params
	var num int64
	var err error

	if itemStockData.Id != 0 {
		num, err = o.Raw("select * from item_stock WHERE _id=?", itemStockData.Id).Values(&itemstockDetails)

	} else {
		num, err = o.Raw("select * from item_stock WHERE _orgid=? and item_org_assoc_fkid=? and latest=1 and geo_location=?", itemStockData.OrgId, itemStockData.Itemid, itemStockData.Location).Values(&itemstockDetails)
	}
	if err != nil {
		return 0 // error in database
	}

	//ASSIGN VALUES FROM PASSED OBJECT
	orgId := itemStockData.OrgId
	itemId := itemStockData.Itemid
	expiresBy := itemStockData.ExpiresBy
	quantity := itemStockData.QuantityModified
	endDate := itemStockData.EndDate
	startDate := itemStockData.StartDate
	rate := itemStockData.Rate
	currency := itemStockData.Currency
	geoLocation := itemStockData.Location
	batchNo := itemStockData.BatchNo
	grnNo := itemStockData.GRNNumber
	blocked := itemStockData.IsBlocked
	insertStatus := itemStockData.InsertStatus
	insertTableFkId := itemStockData.InsertTableFkid
	consumedTableFkId := itemStockData.ConsumedTableFkid
	consumedQuantity := itemStockData.ConsumedQuantity

	//IF NOT TAKE FROM DATABASE
	if num > 0 && err == nil {

		_, err2 := o.Raw("UPDATE item_stock SET latest = 0 WHERE _id =?", itemStockData.Id).Exec()
		if err2 != nil {
			ormTransactionalErr = o.Rollback()
			return 0
		}

		if orgId == "" {
			if itemstockDetails[0]["_orgid"] != nil {
				orgId = itemstockDetails[0]["_orgid"].(string)
			}
		}

		if itemstockDetails[0]["expires_by"] != nil {
			p := itemstockDetails[0]["expires_by"].(string)
			t, err := time.Parse(layout, p)
			if err == nil {
				expiresBy = t
			}

		}

		if quantity == -1 {
			p := itemstockDetails[0]["quantity"].(string)
			quantity, _ = strconv.ParseFloat(p, 64)
		}

		if itemstockDetails[0]["end_date"] != nil {
			p := itemstockDetails[0]["end_date"].(string)
			t, err := time.Parse(layout, p)
			if err == nil {
				endDate = t
			}
		}

		if itemstockDetails[0]["start_date"] != nil {
			p := itemstockDetails[0]["start_date"].(string)
			t, err := time.Parse(layout, p)
			if err == nil {
				startDate = t
			}
		}

		if rate == 0 {
			if itemstockDetails[0]["rate"] != nil {

				p := itemstockDetails[0]["rate"].(string)
				rate, _ = strconv.ParseFloat(p, 64)
			}
		}

		if currency == "" {
			if itemstockDetails[0]["currency"] != nil {
				currency = itemstockDetails[0]["currency"].(string)
			}
		}

		if geoLocation == "" {
			if itemstockDetails[0]["geo_location"] != nil {
				geoLocation = itemstockDetails[0]["geo_location"].(string)
			}
		}

		if batchNo == "" {
			if itemstockDetails[0]["batch_no"] != nil {
				batchNo = itemstockDetails[0]["batch_no"].(string)
			}
		}

		if grnNo == "" {
			if itemstockDetails[0]["grn_no"] != nil {
				grnNo = itemstockDetails[0]["grn_no"].(string)
			}
		}

		if insertTableFkId == 0 {
			if itemstockDetails[0]["insert_table_fkid"] != nil {
				p := itemstockDetails[0]["insert_table_fkid"].(string)
				insertTableFkId, _ = strconv.ParseInt(p, 10, 64)
			}
		}

		if consumedTableFkId == 0 {
			if itemstockDetails[0]["consumed_table_fkid"] != nil {
				p := itemstockDetails[0]["consumed_table_fkid"].(string)
				consumedTableFkId, _ = strconv.ParseInt(p, 10, 64)
			}
		}

		if insertStatus == 0 {
			p := itemstockDetails[0]["insert_status"].(string)
			iStatus, _ := strconv.ParseInt(p, 10, 64)
			insertStatus = int(iStatus)
		}
	}

	_, err3 := o.Raw("INSERT INTO item_stock (_orgid, item_org_assoc_fkid, quantity, rate, currency, geo_location,start_date,end_date,expires_by, batch_no, grn_no, blocked,insert_status,insert_table_fkid,consumed_table_fkid, consumed_qty, latest) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ", orgId, itemId, quantity, rate, currency, geoLocation, startDate, endDate, expiresBy, batchNo, grnNo, blocked, insertStatus, insertTableFkId, consumedTableFkId, consumedQuantity, 1).Exec()
	if err3 != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}

	ormTransactionalErr = o.Commit()
	if ormTransactionalErr != nil {
		return 0
	}

	return 1
}

func BlockItemsBeforeConsumption(itemDetailsForOrder ItemDetailsForOrder) int {

	requiredQuantity := itemDetailsForOrder.RequiredQuantity
	//CONSIDERING ITEMS ARE USED BASED ON START DATE [INSERTED DATE] NO BATCHES ARE ASSIGNED

	//DO THIS FOR EACH ITEM, NEED TO WRITE A FOR LOOP HERE
	o := orm.NewOrm()

	ormTransactionalErr := o.Begin()
	var itemstockDetails []orm.Params

	var num int64
	var err error

	if itemDetailsForOrder.BatchNo == "" {
		num, err = o.Raw("select * from item_stock WHERE _orgid=? and item_org_assoc_fkid=? and latest=1 and geo_location=? and blocked=0 order by start_date ASC", itemDetailsForOrder.OrgId, itemDetailsForOrder.ItemId, itemDetailsForOrder.Location).Values(&itemstockDetails)
	} else {
		num, err = o.Raw("select * from item_stock WHERE _orgid=? and item_org_assoc_fkid=? and latest=1 and geo_location=? and blocked=0 and batch_no =? ", itemDetailsForOrder.OrgId, itemDetailsForOrder.ItemId, itemDetailsForOrder.Location, itemDetailsForOrder.BatchNo).Values(&itemstockDetails)
	}

	if err != nil {
		return 0 // error in database
	}

	//FOR EACH RECORD
	if num > 0 && err == nil {

		for _, item := range itemstockDetails {
			if requiredQuantity > 0 {
				_id := item["_id"].(string)
				ID, _ := strconv.ParseInt(_id, 10, 64)

				orgId := item["_orgid"].(string)

				geoLocation := item["geo_location"].(string)

				itemid := item["item_org_assoc_fkid"].(string)
				itemID, _ := strconv.ParseInt(itemid, 10, 64)

				aQty := item["quantity"].(string)
				availableQty, _ := strconv.ParseFloat(aQty, 64)

				if availableQty == requiredQuantity {

					itemStockRecord := ItemStockTable{
						Id:                ID,
						OrgId:             orgId,
						Itemid:            itemID,
						Location:          geoLocation,
						QuantityModified:  requiredQuantity,
						IsBlocked:         itemDetailsForOrder.BlockedBy,
						ConsumedTableFkid: itemDetailsForOrder.ConsumedTableFkId,
					}

					insertQueryStatus := InsertUpdateItemStock(itemStockRecord)

					if insertQueryStatus == 0 {
						ormTransactionalErr = o.Rollback()
						return 0
					} else {
						requiredQuantity = 0
					}

				} else {
					if availableQty > requiredQuantity {

						itemStockRecord := ItemStockTable{
							Id:                ID,
							OrgId:             orgId,
							Itemid:            itemID,
							Location:          geoLocation,
							QuantityModified:  requiredQuantity,
							IsBlocked:         itemDetailsForOrder.BlockedBy,
							ConsumedTableFkid: itemDetailsForOrder.ConsumedTableFkId,
						}

						insertQueryStatus := InsertUpdateItemStock(itemStockRecord)

						if insertQueryStatus == 0 {
							ormTransactionalErr = o.Rollback()
							return 0
						}

						remainingQty := availableQty - requiredQuantity // THE REMAINING QUANTITY

						item2StockRecord := ItemStockTable{
							Id:               ID,
							OrgId:            orgId,
							Itemid:           itemID,
							Location:         geoLocation,
							QuantityModified: remainingQty,
							IsBlocked:        0,
						}

						insertQuery2Status := InsertUpdateItemStock(item2StockRecord)

						if insertQuery2Status == 0 {
							ormTransactionalErr = o.Rollback()
							return 0
						}
						if insertQueryStatus == 1 && insertQuery2Status == 1 {
							requiredQuantity = 0
						}
					} else {

						itemStockRecord := ItemStockTable{
							Id:                ID,
							OrgId:             orgId,
							Itemid:            itemID,
							Location:          geoLocation,
							QuantityModified:  availableQty,
							IsBlocked:         itemDetailsForOrder.BlockedBy,
							ConsumedTableFkid: itemDetailsForOrder.ConsumedTableFkId,
						}

						insertQueryStatus := InsertUpdateItemStock(itemStockRecord)

						if insertQueryStatus == 0 {
							ormTransactionalErr = o.Rollback()
							return 0
						} else {
							requiredQuantity = requiredQuantity - availableQty
						}
					}
				}

			}
		}
	}

	ormTransactionalErr = o.Commit()
	if ormTransactionalErr != nil {
		return 0
	}
	return 1

}

func ConsumeAllItemsOnOrderCompleted(itemDetailsForOrder ItemDetailsForOrder) int {

	//I/P PARAMETERS : TYPE OF ACTIVITY(SALES/PRODUCTION/PACKAGING)
	//				   ITEM ID
	//				   LOCATION

	o := orm.NewOrm()

	ormTransactionalErr := o.Begin()
	var itemstockDetails []orm.Params

	num, err := o.Raw("select * from item_stock WHERE _orgid=? and latest=1 and geo_location=? and blocked=? and  consumed_table_fkid=? order by item_org_assoc_fkid", itemDetailsForOrder.OrgId, itemDetailsForOrder.Location, itemDetailsForOrder.BlockedBy, itemDetailsForOrder.ConsumedTableFkId).Values(&itemstockDetails)

	if err != nil {
		return 0 // error in database
	}

	//FOR EACH RECORD START CONSUMPTION
	if num > 0 && err == nil {

		for _, item := range itemstockDetails {
			_id := item["_id"].(string)
			ID, _ := strconv.ParseInt(_id, 10, 64)

			qty := item["quantity"].(string)
			quantity, _ := strconv.ParseFloat(qty, 64)

			itemid := item["item_org_assoc_fkid"].(string)
			itemID, _ := strconv.ParseInt(itemid, 10, 64)

			itemstockData := ItemStockTable{
				Id:                ID,
				OrgId:             item["_orgid"].(string),
				Itemid:            itemID,
				QuantityModified:  0,
				ConsumedQuantity:  quantity,
				EndDate:           time.Now(),
				IsBlocked:         itemDetailsForOrder.BlockedBy,
				ConsumedTableFkid: itemDetailsForOrder.ConsumedTableFkId,
			}

			insertQueryStatus := InsertUpdateItemStock(itemstockData)
			if insertQueryStatus == 0 {
				ormTransactionalErr = o.Rollback()
				return 0
			}

		}
	}

	ormTransactionalErr = o.Commit()
	if ormTransactionalErr != nil {
		return 0
	}
	return 1

}

func GetLowLevelForItem(orgId string, itemId int64, location string) float64 {
	o := orm.NewOrm()

	ormTransactionalErr := o.Begin()
	var mapsmin []orm.Params
	var mapst []orm.Params

	low_level := 0.0
	total_existing_inventory := 0.0

	_, err := o.Raw("SELECT SUM(quantity) AS Total FROM item_stock WHERE _orgid=? and item_org_assoc_fkid=? and geo_location=? and latest = 1", orgId, itemId, location).Values(&mapst)
	if err != nil {
		ormTransactionalErr = o.Rollback()
		return low_level
	}

	t := "0"
	if mapst[0]["Total"] != "" && mapst[0]["Total"] != nil {
		t = mapst[0]["Total"].(string)
	}
	total_existing_inventory, _ = strconv.ParseFloat(t, 64)

	count, err := o.Raw("SELECT min_stock_trigger FROM item_org_assoc WHERE _orgid=? and _id=?", orgId, itemId).Values(&mapsmin)
	if count > 1 && err == nil {
		m := mapsmin[0]["min_stock_trigger"].(string)
		min_trigger, _ := strconv.ParseFloat(m, 64)

		if total_existing_inventory < min_trigger {
			low_level = 1.0
		}
	}

	ormTransactionalErr = o.Commit()
	if ormTransactionalErr != nil {
		return low_level
	}
	return low_level
}

//UPDATE THE ITEM TOTAL INVENTORY
func InsertUpdateItemTotalInventory(itemInventoryData ItemTotalInventory) float64 {
	o := orm.NewOrm()
	var recordDetail []orm.Params
	ormTransactionalErr := o.Begin()

	count, err1 := o.Raw("SELECT _id FROM item_total_inventory WHERE _orgid=? and item_org_assoc_fkid=? and status=1 and geo_location=?", itemInventoryData.OrgId, itemInventoryData.Itemid, itemInventoryData.Location).Values(&recordDetail)
	if err1 != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}

	if count > 0 && err1 == nil {
		_, err2 := o.Raw("UPDATE item_total_inventory set status = 0 where _id = ? and _orgid = ?", recordDetail[0]["_id"], itemInventoryData.OrgId).Exec()
		if err2 != nil {
			ormTransactionalErr = o.Rollback()
			return 0
		}
	}

	//UPDATE THE QUANTITY BASED ON TYPE OF ACTIVITY
	//ADD IN CASE OF PRODUCED ITEM/PURCHASE --1
	//SUBTRACT IN CASE OF SALES/ PRODUCTION USAGE -- 0
	updatedQuantity := 0.0
	existingInventoryQuantity := GetExistingTotalQtyFromItemInventory(itemInventoryData)
	if itemInventoryData.AddRemoveFlag == 0 {
		updatedQuantity = existingInventoryQuantity - itemInventoryData.QuantityModified
	} else if itemInventoryData.AddRemoveFlag == 1 {
		updatedQuantity = existingInventoryQuantity + itemInventoryData.QuantityModified
	} //else { //during Make adjustment AddRemoveFlag == 3
	// 	updatedQuantity = itemInventoryData.QuantityModified
	// }

	//GET LOW LEVEL OF THE ITEM AND INSERT THE FLAG

	lowLevel := GetLowLevelForItem(itemInventoryData.OrgId, itemInventoryData.Itemid, itemInventoryData.Location)

	_, err3 := o.Raw("INSERT into item_total_inventory (_orgid,item_org_assoc_fkid,total_quantity,last_updated,geo_location,low_level) values (?,?,?,?,?,?)", itemInventoryData.OrgId, itemInventoryData.Itemid, updatedQuantity, time.Now(), itemInventoryData.Location, lowLevel).Exec()
	if err3 != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}

	ormTransactionalErr = o.Commit()
	if ormTransactionalErr != nil {
		return 0
	}
	return 1
}

//UPDATE THE ITEM TOTAL INVENTORY during Ajustment
func InsertUpdateItemTotalInventoryDuringAdjustment(OrgId string, ItemId int64, LocationId string) float64 {
	o := orm.NewOrm()
	var recordDetail []orm.Params
	ormTransactionalErr := o.Begin()

	count, err1 := o.Raw("SELECT _id FROM item_total_inventory WHERE _orgid=? and item_org_assoc_fkid=? and status=1 and geo_location=?", OrgId, ItemId, LocationId).Values(&recordDetail)
	if err1 != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}

	if count > 0 && err1 == nil {
		_, err2 := o.Raw("UPDATE item_total_inventory set status = 0 where _id = ? and _orgid = ?", recordDetail[0]["_id"], OrgId).Exec()
		if err2 != nil {
			ormTransactionalErr = o.Rollback()
			return 0
		}
	}

	existingStockQuantity := GetExistingTotalQtyFromItemStock(OrgId, ItemId, LocationId)

	//GET LOW LEVEL OF THE ITEM AND INSERT THE FLAG
	lowLevel := GetLowLevelForItem(OrgId, ItemId, LocationId)

	_, err3 := o.Raw("INSERT into item_total_inventory (_orgid,item_org_assoc_fkid,total_quantity,last_updated,geo_location,low_level) values (?,?,?,?,?,?)", OrgId, ItemId, existingStockQuantity, time.Now(), LocationId, lowLevel).Exec()
	if err3 != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}

	ormTransactionalErr = o.Commit()
	if ormTransactionalErr != nil {
		return 0
	}
	return 1
}

func GetExistingTotalQtyFromItemStock(OrgId string, ItemId int64, LocationId string) float64 {
	o := orm.NewOrm()

	ormTransactionalErr := o.Begin()
	total_existing_inventory := 0.0
	var mapst []orm.Params

	count, err := o.Raw("SELECT SUM(quantity) AS Total FROM item_stock WHERE _orgid=? and item_org_assoc_fkid=? and geo_location=? and latest = 1", OrgId, ItemId, LocationId).Values(&mapst)
	if count > 0 && err == nil {
		t := mapst[0]["Total"].(string)
		total_existing_inventory, _ = strconv.ParseFloat(t, 64)
	} else if err != nil {
		ormTransactionalErr = o.Rollback()
		return 0.0
	}

	ormTransactionalErr = o.Commit()
	if ormTransactionalErr != nil {
		return 0.0
	}
	return total_existing_inventory
}

func GetExistingTotalQtyFromItemInventory(itemInventoryData ItemTotalInventory) float64 {
	o := orm.NewOrm()

	ormTransactionalErr := o.Begin()
	total_existing_inventory := 0.0
	var mapst []orm.Params

	count, err := o.Raw("SELECT SUM(total_quantity) AS Total FROM item_total_inventory WHERE _orgid=? and item_org_assoc_fkid=? and geo_location=? and status = 1", itemInventoryData.OrgId, itemInventoryData.Itemid, itemInventoryData.Location).Values(&mapst)
	if count > 0 && err == nil {
		if mapst[0]["Total"] != nil {
			t := mapst[0]["Total"].(string)
			total_existing_inventory, _ = strconv.ParseFloat(t, 64)
		} else {
			total_existing_inventory = 0.0
		}
	} else if err != nil {
		ormTransactionalErr = o.Rollback()
		return 0.0
	}

	ormTransactionalErr = o.Commit()
	if ormTransactionalErr != nil {
		return 0.0
	}
	return total_existing_inventory
}

func GetExistingUnblockedTotalQtyFromItemStock(itemStockData ItemStockTable) float64 {
	o := orm.NewOrm()

	ormTransactionalErr := o.Begin()
	total_existing_inventory := 0.0
	var mapst []orm.Params

	count, err := o.Raw("SELECT SUM(quantity) AS Total FROM item_stock WHERE _orgid=? and item_org_assoc_fkid=? and geo_location=? and latest = 1 and blocked = 0 ", itemStockData.OrgId, itemStockData.Itemid, itemStockData.Location).Values(&mapst)
	if count > 0 && err == nil {
		if mapst[0]["Total"] != nil {
			t := mapst[0]["Total"].(string)
			total_existing_inventory, _ = strconv.ParseFloat(t, 64)
		} else {
			total_existing_inventory = 0.0
		}
	} else if err != nil {
		ormTransactionalErr = o.Rollback()
		return 0.0
	}

	ormTransactionalErr = o.Commit()
	if ormTransactionalErr != nil {
		return 0.0
	}
	return total_existing_inventory
}

func convertDateString(dateToConvert string) string {
	t, _ := time.Parse(layout, dateToConvert)
	return t.Format("02/01/2006")
}

func GetTodaysDate() string {
	return time.Now().Format("2006-01-02")
}
