package models

import (
	//"fmt"
	"strconv"
	//"strings"
	"github.com/astaxie/beego/orm"
)

type Support struct {
	Id               int64
	TicketNo         string
	TicketDate       string
	Sales_order_fkid string
	InvoiceNo        string
	Party_name       string
	Party_address    string
	ProductName      string
	Details          string
	Observations     string
	Instructions     string
	Action           string
	CustAccepted     string
	DateVisit        string
	Other_remarks    string
	Your_remarks     string
	InitiatedBy      string
	VerifiedBy       string
	CheckedBy        string
	AuthorizedBy     string
	Orgid            string
	Userid           string
	Username         string
	NewGenCode       string
}

func CreateNewSupportTicket(formData Support) int {

	o := orm.NewOrm()

	var date_visited *string
	date_visited = nil
	if formData.DateVisit != "" {
		temdate_visited := formData.DateVisit
		date_visited = &temdate_visited
	}

	_, err := o.Raw("INSERT INTO customer_support (ticket_no, ticket_date, _sales_order_fkid, invoice_no, party_name, party_address, product_name, details, observations, instructions, action_taken, customer_accepted, date_visited, other_remarks, your_remarks, initiated_by, verified_by, checked_by, authorized_by,_orgid, support_ticket_code_gen) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ", formData.TicketNo, formData.TicketDate, formData.Sales_order_fkid, formData.InvoiceNo, formData.Party_name, formData.Party_address, formData.ProductName, formData.Details, formData.Observations, formData.Instructions, formData.Action, formData.CustAccepted, &date_visited, formData.Other_remarks, formData.Your_remarks, formData.InitiatedBy, formData.VerifiedBy, formData.CheckedBy, formData.AuthorizedBy, formData.Orgid, formData.NewGenCode).Exec()
	if err == nil {
		return 1
	}
	return 0
}

func GetSupportTicket(formData Support) map[string]orm.Params {

	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	//num, err := o.Raw("SELECT * FROM item_master ").Values(&maps)
	num, err := o.Raw("SELECT * from customer_support where _orgid=?", formData.Orgid).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		var i int64 = 0
		for ; i < num; i++ {
			//fmt.Println(maps[i])
		}
		for k, v := range maps {
			
			if maps[k]["ticket_date"] != nil && maps[k]["ticket_date"] != "" {
				maps[k]["ticket_date"] = convertDateString(maps[k]["ticket_date"].(string))
			}
			key := "support_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetSupportCustomers(formData Support) map[string]orm.Params {

	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT * from customer where _orgid=? and status=1", formData.Orgid).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		var i int64 = 0
		for ; i < num; i++ {
			//fmt.Println(maps[i])
		}
		for k, v := range maps {
			key := "customer_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetCustomerProducts(formData Support) map[string]orm.Params {

	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT * from item_org_assoc where _orgid=? and status=1", formData.Orgid).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		var i int64 = 0
		for ; i < num; i++ {
			//fmt.Println(maps[i])
		}
		for k, v := range maps {
			key := "product_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func SearchSupportTicketdetails(InvoiceNumber, CustomerName, _orgid string) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	var num int64
	var err error
	if (InvoiceNumber != "") && (CustomerName != "") {
		num, err = o.Raw("SELECT * from customer_support where _orgid=? and (invoice_no=? and party_name like ?)", _orgid, InvoiceNumber, "%"+CustomerName+"%").Values(&maps)
	}
	if (InvoiceNumber == "") && (CustomerName != "") {
		num, err = o.Raw("SELECT * from customer_support where _orgid=? and party_name like ?", _orgid, "%"+CustomerName+"%").Values(&maps)
	}
	if (InvoiceNumber != "") && (CustomerName == "") {
		num, err = o.Raw("SELECT * from customer_support where _orgid=? and invoice_no = ?", _orgid, InvoiceNumber).Values(&maps)
	}
	if (InvoiceNumber == "") && (CustomerName == "") {
		num, err = o.Raw("SELECT * from customer_support where _orgid=?", _orgid).Values(&maps)
	}
	if err == nil && num > 0 {
		for k, v := range maps {
			if maps[k]["ticket_date"] != nil && maps[k]["ticket_date"] != "" {
				maps[k]["ticket_date"] = convertDateString(maps[k]["ticket_date"].(string))
			}

			key := "support_" + strconv.Itoa(k)
			result[key] = v
		}
	} else {
		result["status"] = nil
	}
	return result
}

func GetMaxSupportTicketCode(orgid string) int {
	var maxCode int
	maxCode = 1
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT max(ticket_no) as maxcode FROM customer_support where _orgid=?", orgid).Values(&maps)

	if num > 0 && err == nil {
		if maps[0]["maxcode"] != nil {
			code := maps[0]["maxcode"].(string)
			getValue, _ := strconv.ParseInt(code, 10, 64)
			maxCode = int(getValue)
		}
	}
	return maxCode
}
