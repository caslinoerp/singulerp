package models

/* ReturnGeneratedItemSequence returns the Item/Product unique code
Prefix_master table; pkid = 1 = item_code
Prefix_org_choice table; prefix_master_fkid = 1 and status = 1
func ReturnGeneratedItemSequence(orgID string) string {
	var numberSequence string
	var cyclicSequenceNumber int
	var cyclicSequenceAlpha string
	o := orm.NewOrm()
	var resultMap []orm.Params
	_, err := o.Raw("SELECT prefix_master_fkid, prefix_dropdown_master_fkid, position, value, status from prefix_org_choice where prefix_master_fkid = 1 and status = 1 and _orgid = ?", orgID).Values(&resultMap)

	var sequenceMap []orm.Params
	numSequenceCount, err2 := o.Raw("SELECT sequence from used_sequence_codes where prefix_master_fkid = 1 and status = 1 and _orgid = ? order by pkid desc limit 1", orgID).Values(&sequenceMap)

	if err2 == nil && numSequenceCount > 0 {
		numberSequence = strconv.Itoa(sequenceMap[0]["sequence"].(int) + 1)
	}
	return "0"
}
*/
