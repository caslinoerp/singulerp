package models

import (
	//"fmt"
	//"strings"
	"strconv"

	"github.com/astaxie/beego/orm"
)

type Employee struct {
	Id         int64
	Empid      string
	Name       string
	DOB        string
	Address    string
	Email_id   string
	Contact_no string
	//Cell_no          string
	Emergency_contact string
	Econtact_name     string
	Relationship      string
	Id_proof          string
	Blood_group       string
	Qualification     string
	Department        string
	Designation       string
	Job_title         string
	Joining_date      string
	//Salary           string
	Orgid      string
	Userid     string
	Username   string
	NewGenCode string
}

type Contract struct {
	EmpID             string
	EmpStatus         string
	JobTitle          string
	ContractType      string
	ContractDate      string
	ContractRenewDate string
	Salary            string
	Orgid             string
	Userid            string
	Username          string
}

type Leaves struct {
	EmpID     string
	Mleaves   string
	Cleaves   string
	Eleaves   string
	Rholidays string
	Orgid     string
	Userid    string
	Username  string
}

func CreateNewEmployee(formData Employee) int {
	o := orm.NewOrm()
	// fmt.Println(formData.Orgid)
	//_, err := o.Raw("INSERT INTO hr_employeedetails (name, dob, address, email_id, contact_no, cell_no, emergency_contact, econtact_name, relationship, id_proof, blood_group, qualification, department, designation, job_title, joining_date, salary,_orgid) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ", data["Name"], data["DOB"],data["OfficeAddress"],data["Email"],data["ContactNo"],data["CellNo"],data["EPhoneNo"],data["EContact"],data["Relationship"],data["ID"],data["BloodGroup"],data["Qualification"], data["Department"],data["Designation"], data["JobTitle"], data["DOJ"],data["Salary"],"1").Values(&maps)
	_, err := o.Raw("INSERT INTO hr_employeedetails (employee_id,name, dob, address, email_id, contact_no, emergency_contact, econtact_name, relationship, id_proof, blood_group, qualification, department, designation, job_title, joining_date,_orgid, employee_code_gen) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ", formData.Empid, formData.Name, formData.DOB, formData.Address, formData.Email_id, formData.Contact_no, formData.Emergency_contact, formData.Econtact_name, formData.Relationship, formData.Id_proof, formData.Blood_group, formData.Qualification, formData.Department, formData.Designation, formData.Job_title, formData.Joining_date, formData.Orgid, formData.NewGenCode).Exec()

	if err == nil {
		return 1
	}
	return 0
}

func ManageLeaves(formData Leaves) int {
	o := orm.NewOrm()
	_, err := o.Raw("INSERT INTO hr_empleaves (_employee_fkid, monthly_leaves, casual_leaves, earn_leaves, restricted_holidays, _orgid) VALUES (?,?,?,?,?,?) ", formData.EmpID, formData.Mleaves, formData.Cleaves, formData.Eleaves, formData.Rholidays, formData.Orgid).Exec()

	if err == nil {
		return 1
	}
	return 0
}

func ManageContracts(formData Contract) int {
	o := orm.NewOrm()
	_, err := o.Raw("INSERT INTO hr_empcontract (_employee_fkid, employee_status, job_title, contract_type, contract_date, contract_renew_date,_orgid,salary) VALUES (?,?,?,?,?,?,?,?) ", formData.EmpID, formData.EmpStatus, formData.JobTitle, formData.ContractType, formData.ContractDate, formData.ContractRenewDate, formData.Orgid, formData.Salary).Exec()

	if err == nil {
		return 1
	}
	return 0
}

func GetEmployeeDetails(formData Employee) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	//num, err := o.Raw("SELECT * FROM item_master ").Values(&maps)
	//err1:=o.Raw("select MAX(_id) from hr_employeedetails").Values(&maps)
	num, err := o.Raw("SELECT * from hr_employeedetails where _orgid=?", formData.Orgid).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		var i int64 = 0
		for ; i < num; i++ {
			//fmt.Println(maps[i])
		}
		for k, v := range maps {
			if maps[k]["joining_date"] != nil && maps[k]["joining_date"] != "" {
				maps[k]["joining_date"] = convertDateString(maps[k]["joining_date"].(string))
			}
			if maps[k]["dob"] != nil && maps[k]["dob"] != "" {
				maps[k]["dob"] = convertDateString(maps[k]["dob"].(string))
			}
			key := "employee_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetLastEmp(formData Employee) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	num, err1 := o.Raw("select MAX(_id) from hr_employeedetails where _orgid=?", formData.Orgid).Values(&maps)
	if err1 == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		var i int64 = 0
		for ; i < num; i++ {
			//fmt.Println(maps[i])
		}
		for k, v := range maps {
			key := "employeem_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err1)
		result["status"] = nil
	}
	return result
}

func GetEmployeeContract(formData Contract) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	//num, err := o.Raw("SELECT * FROM item_master ").Values(&maps)
	num, err := o.Raw("SELECT ec._employee_fkid,ec.employee_status,ec.job_title,ec.contract_type, ec.contract_date as c_date, ec.contract_renew_date as r_date,ec.salary,ed.name from hr_employeedetails as ed,hr_empcontract as ec where ec._employee_fkid=ed.employee_id and ec._orgid=? and ed._orgid=?", formData.Orgid, formData.Orgid).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		var i int64 = 0
		for ; i < num; i++ {
			//fmt.Println(maps[i])
		}
		for k, v := range maps {
			key := "employee_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetEmployeeLeaves(formData Leaves) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	//num, err := o.Raw("SELECT * FROM item_master ").Values(&maps)
	num, err := o.Raw("SELECT el._employee_fkid,el.monthly_leaves,el.casual_leaves,el.earn_leaves,el.restricted_holidays,ed.name from hr_employeedetails as ed,hr_empleaves as el where el._employee_fkid=ed.employee_id and el._orgid=? and ed._orgid=?", formData.Orgid, formData.Orgid).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		var i int64 = 0
		for ; i < num; i++ {
			//fmt.Println(maps[i])
		}
		for k, v := range maps {
			key := "employee_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func SearchEmployees(employeeName string, employeedepartment string, Orgid string) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT * from hr_employeedetails where _orgid =? and ( name like ? and department like  ? ) ", Orgid, "%"+employeeName+"%", "%"+employeedepartment+"%").Values(&maps)
	if err == nil && num > 0 {
		for k, v := range maps {
			if maps[k]["joining_date"] != nil && maps[k]["joining_date"] != "" {
				maps[k]["joining_date"] = convertDateString(maps[k]["joining_date"].(string))
			}
			if maps[k]["dob"] != nil && maps[k]["dob"] != "" {
				maps[k]["dob"] = convertDateString(maps[k]["dob"].(string))
			}
			key := "employee_" + strconv.Itoa(k)
			result[key] = v
		}
	} else {
		result["status"] = nil
	}
	return result
}

func GetMaxEmployeeCode(orgid string) int {
	var maxCode int
	maxCode = 1
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT max(employee_id) as maxcode FROM hr_employeedetails where _orgid=?", orgid).Values(&maps)

	if num > 0 && err == nil {
		if maps[0]["maxcode"] != nil {
			code := maps[0]["maxcode"].(string)
			getValue, _ := strconv.ParseInt(code, 10, 64)
			maxCode = int(getValue)
		}
	}
	return maxCode
}
