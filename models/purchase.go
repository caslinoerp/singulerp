package models

import (

	//"strings"

	"strconv"
	"time"

	"github.com/astaxie/beego/orm"
)

type Createpo struct {
	POSONo               string
	PODate               string
	Supplier             string
	PoDeliveryDate       string
	DiscountSubTotal     string
	Currency             string
	DLoc                 string
	Transporter          string
	Vehicle              string
	TransporterWarehouse string
	Notes                string
	ItemName             string
	HSN                  string
	UnitPrice            string
	UOM                  string
	Quantity             string
	PacketSize           string
	TotalPrice           string
	TaxRegime            string
	//UnitMeasure 	   string
	ItemName1   string
	UnitPrice1  string
	Quantity1   string
	PacketSize1 string
	//UnitMeasure1 	   string
	ItemName2   string
	UnitPrice2  string
	Quantity2   string
	PacketSize2 string
	//UnitMeasure2 	   string
	ItemName3   string
	UnitPrice3  string
	Quantity3   string
	PacketSize3 string
	//UnitMeasure3 	   string
	ItemName4   string
	UnitPrice4  string
	Quantity4   string
	PacketSize4 string
	//UnitMeasure4 	   string
	ItemName5   string
	UnitPrice5  string
	Quantity5   string
	PacketSize5 string
	//UnitMeasure5 	   string
	ItemName6   string
	UnitPrice6  string
	Quantity6   string
	PacketSize6 string
	//UnitMeasure6 	   string
	FreightType       string
	FreightCost       string
	PaymentTerms      string
	OtherChargesTitle string
	OtherChargesValue string
	ExciseDuty        string
	OctroiCharges     string
	Total             string
	FinalDiscount     string
	FinalTotal        string
	BillingAddress    string
	Orgid             string
	Username          string
	Userid            string
	IsPresentinDB     int

	PurchaseId  string
	Total0      string
	Total1      string
	Total2      string
	Total3      string
	Total4      string
	Total5      string
	Total6      string
	ItemName0   string
	UnitPrice0  string
	Quantity0   string
	PacketSize0 string
	TotalPrice0 string

	DiscountPercent     string
	DiscountValue       string
	TaxableAmount       string
	GSTPercent          string
	IGSTRate            string
	IGSTAmount          string
	CGSTRate            string
	CGSTAmount          string
	SGSTRate            string
	SGSTAmount          string
	CessRate            string
	CessAmount          string
	ItemTotalTaxCess    string
	ItemTotalAfterTax   string
	Freight             string
	Insurance           string
	PackagingForwarding string
	Others              string
	TotalOtherCharges   string

	TotalTaxableValue     string
	TotalTaxCess          string
	TotalAfterTaxes       string
	TotalFreightPackaging string
	TotalInvoiceValue     string
	NewGenCode            string
}

type GRNData struct {
	Orgid             string
	Username          string
	Userid            string
	PoNo              string
	Itemss            string
	Suppliers         string
	DeliveryDate      string
	SupplierInvoice   string
	SupplierInvoiceDt string
	SupplierDcNo      string
	SupplierDcDt      string
	AddNote           string
	//Status                    string
	AQuantity           string
	RQuantity           string
	DeliveryLocation    string
	TotalCost           string
	DeliveredDate       string
	BatchNo             string
	NewGenCode          string
	GRNNo               string
	LrNo                string
	LrDt                string
	SupplierInvoiceFkId int64
	Transporter         string
	Vehicle             string
}

type Createso struct {
	Orgid      string
	Username   string
	Userid     string
	Contractor string
	ChallanNo  string
	DateOrder  string
	//Items                     string
	RItem                 string
	RQuantity             string
	SDiscountSubTotal     string
	Purpose               string
	EstimatedDeliveryDate string
	DispatchFrom          string
	DispatchDate          string
	DispatchFleet         string
	DispatchCost          string
	SPaymentTerms         string
	SOtherChargesTitle    string
	SOtherChargesValue    string
	SExciseDuty           string
	SOctroiCharges        string
	STotal                string
	SFinalDiscount        string
	SFinalTotal           string
	SBillingAddress       string
	SCurrency             string
	ItemName              string
	Quantity              string
	ItemName1             string
	Quantity1             string
	ItemName2             string
	Quantity2             string
	ItemName3             string
	Quantity3             string
	ItemName4             string
	Quantity4             string
	ItemName5             string
	Quantity5             string
	ItemName6             string
	Quantity6             string
}

type SupplierInvoice struct {
	Orgid             string
	SupplierFkId      int64
	SupplierInvoice   string
	SupplierInvoiceDt time.Time
	SupplierDcNo      string
	SupplierDcDt      time.Time
	DateAdded         time.Time
}

func CheckPo(formData Createpo) int {
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT _id FROM purchase_service_order WHERE unique_serial_no=? and _orgid=? and status=?", formData.POSONo, formData.Orgid, "0").Values(&maps)
	if err == nil {
		if num > 0 {
			return 1
		} else {
			return 0
		}
	} else {
		return 2
	}
}

func CreatePO(formData Createpo, poitems []map[string]string, Status string) int {

	o := orm.NewOrm()
	ormTransactionalErr := o.Begin()
	// var freighttype *string
	// freighttype = nil
	// if formData.FreightType != "" {
	// 	tempfreighttype := formData.FreightType
	// 	freighttype = &tempfreighttype
	// }
	var freightpackagingothers string
	freightpackagingothers = "0.00"
	if formData.TotalFreightPackaging != "" {
		tempTotalFreightPackaging := formData.TotalFreightPackaging
		freightpackagingothers = tempTotalFreightPackaging
	}
	// var dissubtotal *string
	// dissubtotal = nil
	// if formData.DiscountSubTotal != "" {
	// 	tempdissubtotal := formData.DiscountSubTotal
	// 	dissubtotal = &tempdissubtotal
	// }
	var paymentterms *string
	paymentterms = nil
	if formData.PaymentTerms != "" {
		temppaymentterms := formData.PaymentTerms
		paymentterms = &temppaymentterms
	}

	var billlocation *string
	billlocation = nil
	if formData.BillingAddress != "" {
		tempbilllocation := formData.BillingAddress
		billlocation = &tempbilllocation
	}

	var transporter *string
	transporter = nil
	if formData.Transporter != "" {
		temptransporter := formData.Transporter
		transporter = &temptransporter
	}

	var vehicle *string
	vehicle = nil
	if formData.Vehicle != "" {
		tempvehicle := formData.Vehicle
		vehicle = &tempvehicle
	}

	var transporter_warehouse *string
	transporter_warehouse = nil
	if formData.TransporterWarehouse != "" {
		tempWarehouse := formData.TransporterWarehouse
		transporter_warehouse = &tempWarehouse
	}

	_, err1 := o.Raw("INSERT INTO purchase_service_order (tax_regime, unique_serial_no, type, date, delivery_date, geo_location, payment_terms, currency, supplier_fkid, billing_address, total_taxable_value, total_tax_cess, total_freight_packaging_others, total_after_taxes, total_invoice_value, _orgid, status, purchase_order_code_gen, transporter_fkid, vehicle_fkid,transporter_warehouse_fkid,notes) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ", formData.TaxRegime, formData.POSONo, "0", formData.PODate, formData.PoDeliveryDate, formData.DLoc, &paymentterms, formData.Currency, formData.Supplier, &billlocation, formData.TotalTaxableValue, formData.TotalTaxCess, freightpackagingothers, formData.TotalAfterTaxes, formData.TotalInvoiceValue, formData.Orgid, Status, formData.NewGenCode, &transporter, &vehicle, &transporter_warehouse, formData.Notes).Exec()
	if err1 != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}
	var maps2 []orm.Params
	_, err2 := o.Raw("SELECT _id FROM purchase_service_order WHERE _orgid=? and date=? and delivery_date=? and type=? and currency=? and supplier_fkid=? and status=?", formData.Orgid, formData.PODate, formData.PoDeliveryDate, "0", formData.Currency, formData.Supplier, Status).Values(&maps2)
	if err2 != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}
	po_id := maps2[0]["_id"]

	_, err3 := o.Raw("INSERT INTO po_so_status (_orgid, purchase_service_order_fkid, actedon_by_fkid, date_actedon, status) VALUES (?,?,?,?,?)", formData.Orgid, po_id, formData.Userid, time.Now(), Status).Exec()
	if err3 != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}

	for _, it := range poitems {

		if it["item_name"] != "" {
			var hsn *string
			hsn = nil
			if it["hsn"] != "" {
				tempHSN := it["hsn"]
				hsn = &tempHSN
			}

			var discountrate string
			discountrate = "0"
			if it["discount_percent"] != "" {
				tempDiscountPercent := it["discount_percent"]
				discountrate = tempDiscountPercent
			}

			var discountamount string
			discountamount = "0.00"
			if it["discount_value"] != "" {
				tempDiscountAmount := it["discount_value"]
				discountamount = tempDiscountAmount
			}

			var gstrate string
			gstrate = "0"
			if it["gst_percent"] != "" {
				tempGSTRate := it["gst_percent"]
				gstrate = tempGSTRate
			}

			var igstrate string
			igstrate = "0"
			if it["igst_rate"] != "" {
				tempIGSTRate := it["igst_rate"]
				igstrate = tempIGSTRate
			}

			var cgstrate string
			cgstrate = "0"
			if it["cgst_rate"] != "" {
				tempCGSTRate := it["cgst_rate"]
				cgstrate = tempCGSTRate
			}

			var sgstrate string
			sgstrate = "0"
			if it["sgst_rate"] != "" {
				tempSGSTRate := it["sgst_rate"]
				sgstrate = tempSGSTRate
			}

			var cessrate string
			cessrate = "0"
			if it["cess_rate"] != "" {
				tempCessRate := it["cess_rate"]
				cessrate = tempCessRate
			}

			var totalothercharges string
			totalothercharges = "0.00"
			if it["total_other_charges"] != "" {
				tempTotalOtherCharges := it["total_other_charges"]
				totalothercharges = tempTotalOtherCharges
			}

			_, err4 := o.Raw("INSERT INTO po_so_items (purchase_service_order_fkid, item_org_assoc_fkid, quantity, uom, hsn_sac, rate, discount_rate, discount_value, delivery_date, taxable_amount, gst_rate, igst_rate, igst_amount, cgst_rate, cgst_amount, sgst_rate, sgst_amount, cess_rate, cess_amount, total_tax_cess, total_after_tax, total_other_charges, total_cost, _orgid, status) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ", po_id, it["item_name"], it["qty"], it["uom"], &hsn, it["cost"], discountrate, discountamount, formData.PoDeliveryDate, it["taxable_amount"], gstrate, igstrate, it["igst_amount"], cgstrate, it["cgst_amount"], sgstrate, it["sgst_amount"], cessrate, it["cess_amount"], it["total_tax_cess"], it["total_after_tax"], totalothercharges, formData.TotalInvoiceValue, formData.Orgid, "1").Exec()
			if err4 != nil {
				ormTransactionalErr = o.Rollback()
				return 0
			}

			if Status == "2" {
				var maps05 []orm.Params
				count, err05 := o.Raw("SELECT _id FROM item_rates WHERE _orgid=? and item_org_assoc_fkid=? and supplier_org_assoc_fkid=?", formData.Orgid, it["item_name"], formData.Supplier).Values(&maps05)
				if err05 != nil {
					ormTransactionalErr = o.Rollback()
					return 0
				}
				if count > 0 {
					_, err005 := o.Raw("UPDATE item_rates SET latest = 0 WHERE _orgid=? and item_org_assoc_fkid=? and supplier_org_assoc_fkid=?", formData.Orgid, it["item_name"], formData.Supplier).Exec()
					if err005 != nil {
						ormTransactionalErr = o.Rollback()
						return 0
					}
				}
				_, err5 := o.Raw("INSERT INTO item_rates (item_org_assoc_fkid, supplier_org_assoc_fkid, rate, currency, start_date, latest, _orgid) VALUES (?,?,?,?,?,?,?) ", it["item_name"], formData.Supplier, it["cost"], formData.Currency, time.Now(), "1", formData.Orgid).Exec()
				if err5 != nil {
					ormTransactionalErr = o.Rollback()
					return 0
				}
			}
		}
	}

	if ormTransactionalErr != nil {
		return 0
	}
	ormTransactionalErr = o.Commit()
	return 1
}

func InsertPOItems(formData Createpo, Status string) int {

	o := orm.NewOrm()
	var maps []orm.Params
	_, err1 := o.Raw("SELECT _id FROM purchase_service_order WHERE _orgid=? and date=? and delivery_date=? and type=? and currency=? and supplier_fkid=? and status=?", formData.Orgid, formData.PODate, formData.PoDeliveryDate, "0", formData.Currency, formData.Supplier, Status).Values(&maps)

	var hsn *string
	hsn = nil
	if formData.HSN != "" {
		tempHSN := formData.HSN
		hsn = &tempHSN
	}

	var discountrate string
	discountrate = "0"
	if formData.DiscountPercent != "" {
		tempDiscountPercent := formData.DiscountPercent
		discountrate = tempDiscountPercent
	}

	var gstrate string
	gstrate = "0"
	if formData.GSTPercent != "" {
		tempGSTPercent := formData.GSTPercent
		gstrate = tempGSTPercent
	}

	var cessrate string
	cessrate = "0"
	if formData.CessRate != "" {
		tempCessRate := formData.CessRate
		cessrate = tempCessRate
	}

	var freight string
	freight = "0"
	if formData.Freight != "" {
		tempFreight := formData.Freight
		freight = tempFreight
	}

	var insurance string
	insurance = "0"
	if formData.Insurance != "" {
		tempInsurance := formData.Insurance
		insurance = tempInsurance
	}

	var packagingforwarding string
	packagingforwarding = "0"
	if formData.PackagingForwarding != "" {
		tempPackagingForwarding := formData.PackagingForwarding
		packagingforwarding = tempPackagingForwarding
	}

	var others string
	others = "0"
	if formData.Others != "" {
		tempOthers := formData.Others
		others = tempOthers
	}

	var totalothercharges string
	totalothercharges = "0.00"
	if formData.TotalOtherCharges != "" {
		tempTotalOtherCharges := formData.TotalOtherCharges
		totalothercharges = tempTotalOtherCharges
	}

	_, err3 := o.Raw("INSERT INTO po_so_items (purchase_service_order_fkid, item_org_assoc_fkid, quantity, uom, hsn_sac, rate, discount_rate, discount_value, delivery_date, taxable_amount, gst_rate, igst_rate, igst_amount, cgst_rate, cgst_amount, sgst_rate, sgst_amount, cess_rate, cess_amount, total_tax_cess, total_after_tax, freight, insurance, packaging_forwarding, others, total_other_charges, _orgid, status) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ", maps[0]["_id"], formData.ItemName, formData.Quantity, formData.UOM, &hsn, formData.UnitPrice, discountrate, formData.DiscountValue, formData.PoDeliveryDate, formData.TaxableAmount, gstrate, formData.IGSTRate, formData.IGSTAmount, formData.CGSTRate, formData.CGSTAmount, formData.SGSTRate, formData.SGSTAmount, cessrate, formData.CessAmount, formData.ItemTotalTaxCess, formData.ItemTotalAfterTax, freight, insurance, packagingforwarding, others, totalothercharges, formData.Orgid, "1").Exec()

	if err1 != nil || err3 != nil {
		return 0
	}
	return 1
}

func CreateGRN1(formData GRNData) int {

	o := orm.NewOrm()
	ormTransactionalErr := o.Begin()
	var note *string
	note = nil
	if formData.AddNote != "" {
		tempnote := formData.AddNote
		note = &tempnote
	}
	var new_grn string
	var mapsgrn []orm.Params
	var mapspo []orm.Params
	num, errgrn := o.Raw("SELECT grn_no FROM po_so_items_with_grn WHERE _orgid=? ORDER BY _id DESC LIMIT 1", formData.Orgid).Values(&mapsgrn)
	//fmt.Println("****code:",maps[0]["order_no"])
	if errgrn == nil {
		if num > 0 {
			s := mapsgrn[0]["grn_no"].(string)
			//fmt.Println("**s:",s)
			//fmt.Println("***length",len(s))
			//prefix := s[0:len(s)-1]
			//fmt.Println("Prefix slice:",prefix)
			//last := s[len(s)-1:]
			//fmt.Println("Last slice:",last)
			sufix, _ := strconv.Atoi(s)
			sufix = sufix + 1
			new_sufix := strconv.Itoa(sufix)
			//fmt.Println("Sufix:",new_sufix)
			//append_code := prefix + new_sufix
			//fmt.Println("Append_code:",append_code)
			count, _ := o.Raw("SELECT _id FROM po_so_items_with_grn WHERE grn_no=? and _orgid=?", new_sufix, formData.Orgid).Values(&mapsgrn)
			if count == 0 {
				new_grn = new_sufix
			} else {
				var safe int = 1
				for safe != 0 {
					//prefix := append_code[0:len(append_code)-1]
					//last := append_code[len(append_code)-1:]
					//fmt.Println("Last slice:",last)
					sufix, _ := strconv.Atoi(new_sufix)
					sufix = sufix + 1
					new_sufix := strconv.Itoa(sufix)
					//fmt.Println("Sufix:",new_sufix)
					//append_code := prefix + new_sufix
					count, _ := o.Raw("SELECT _id FROM po_so_items_with_grn WHERE grn_no=? and _orgid=?", new_sufix, formData.Orgid).Values(&mapsgrn)
					if count == 0 {
						safe = 0
						new_grn = new_sufix
					} else {
						safe = 0
					}
				}
			}
		} else {
			//prefix := "SINGU"
			//s := formData.Orgid
			//mid := s[0:3]
			sufix := "1"
			//append_code := prefix + mid + sufix
			count, _ := o.Raw("SELECT _id FROM sales_order WHERE order_no=? and _orgid=? ", sufix, formData.Orgid).Values(&mapsgrn)
			if count == 0 {
				new_grn = sufix
			} else {
				var safe int = 1
				for safe != 0 {
					//prefix := append_code[0:len(append_code)-1]
					//last := append_code[len(append_code)-1:]
					//fmt.Println("Last slice:",last)
					isufix, _ := strconv.Atoi(sufix)
					isufix = isufix + 1
					sufix := strconv.Itoa(isufix)
					//fmt.Println("Sufix:",new_sufix)
					//append_code := prefix + new_sufix
					count, _ := o.Raw("SELECT _id FROM po_so_items_with_grn WHERE grn_no=? and _orgid=?", sufix, formData.Orgid).Values(&mapsgrn)
					if count == 0 {
						safe = 0
						new_grn = sufix
					} else {
						safe = 0
					}
				}
			}
		}

	}

	var maps []orm.Params
	//fmt.Println(formData.PoNo)
	//fmt.Println(formData.Itemss)
	_, err := o.Raw("SELECT po_so_items._id,purchase_service_order_fkid,item_org_assoc_fkid, rate, quantity, total_cost, received_quantity FROM po_so_items,purchase_service_order,item_org_assoc WHERE po_so_items._orgid=? and purchase_order_code_gen=? and item_unique_code=? and po_so_items.item_org_assoc_fkid=item_org_assoc._id and purchase_service_order._id=po_so_items.purchase_service_order_fkid and purchase_service_order._orgid=? and item_org_assoc._orgid=?", formData.Orgid, formData.PoNo, formData.Itemss, formData.Orgid, formData.Orgid).Values(&maps)
	// fmt.Println("***count", count)
	// fmt.Println("***_id", maps[0]["_id"])
	// fmt.Println("***purchase_service_order_fkid", maps[0]["purchase_service_order_fkid"])
	// fmt.Println("***item_org_assoc_fkid", maps[0]["item_org_assoc_fkid"])

	new_aqty := "0.0"
	if formData.AQuantity != "nil" && formData.AQuantity != "" {
		new_aqty = formData.AQuantity
	}
	NewAQty, _ := strconv.ParseFloat(new_aqty, 64)
	NewAccepted := NewAQty
	if maps[0]["received_quantity"] != nil {
		//fmt.Println("*****Quantity is not nil")
		p := maps[0]["received_quantity"].(string)
		//fmt.Println("*****P:", p)
		Previous, _ := strconv.ParseFloat(p, 64)
		//fmt.Println("*****Previous:", Previous)
		NewAccepted = Previous + NewAQty
		//fmt.Println("*****NewAccepted:", NewAccepted)

	}

	new_rqty := "0.0"
	if formData.RQuantity != "nil" && formData.RQuantity != "" {
		new_rqty = formData.RQuantity
	}
	NewRQty, _ := strconv.ParseFloat(new_rqty, 64)
	NewRejected := NewRQty
	if maps[0]["rejected_quantity"] != nil {
		//fmt.Println("*****Quantity is not nil")
		p := maps[0]["rejected_quantity"].(string)
		//fmt.Println("*****P:", p)
		Previous, _ := strconv.ParseFloat(p, 64)
		//fmt.Println("*****Previous:", Previous)
		NewRejected = Previous + NewRQty
		//fmt.Println("*****NewAccepted:", NewAccepted)

	}

	_, errnew := o.Raw("Update po_so_items SET received_quantity=?, rejected_quantity=? where purchase_service_order_fkid=? and item_org_assoc_fkid=? and _orgid=? and status=1", NewAccepted, NewRejected, maps[0]["purchase_service_order_fkid"], maps[0]["item_org_assoc_fkid"], formData.Orgid).Exec()
	if errnew != nil {
		//fmt.Println("Error in query when no inventory")
		ormTransactionalErr = o.Rollback()
		return 0
	}

	_, err1 := o.Raw("INSERT INTO po_so_items_with_grn (_orgid, purchase_service_order_fkid, item_org_assoc_fkid, po_so_items_fkid, grn_no, grn_status, delivered_date,rate,quantity,total_cost,received_sent_quantity,rejected_quantity,rejected_note,grn_no_gen) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?) ", formData.Orgid, maps[0]["purchase_service_order_fkid"], maps[0]["item_org_assoc_fkid"], maps[0]["_id"], formData.GRNNo, "1", formData.DeliveredDate, maps[0]["rate"], maps[0]["quantity"], formData.TotalCost, formData.AQuantity, formData.RQuantity, &note, formData.NewGenCode).Exec()
	if err1 != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}

	_, err2 := o.Raw("SELECT _id,purchase_service_order_fkid FROM po_so_items_with_grn WHERE _orgid=? and grn_no=? ", formData.Orgid, new_grn).Values(&maps)

	//Mark the PO as completed once the received + rejected = total
	//First get  the details of quantity from po_so_items using maps[0]["purchase_service_order_fkid"]

	_, errPoItems := o.Raw("SELECT _id, quantity, received_quantity,rejected_quantity FROM po_so_items WHERE _orgid=? and purchase_service_order_fkid=? ", formData.Orgid, maps[0]["purchase_service_order_fkid"]).Values(&mapspo)

	if errPoItems != nil {

	}

	qty, erqty := strconv.ParseFloat(mapspo[0]["quantity"].(string), 64)
	received, erreceive := strconv.ParseFloat(mapspo[0]["received_quantity"].(string), 64)
	rejected, errejected := strconv.ParseFloat(mapspo[0]["rejected_quantity"].(string), 64)
	//poId, errejected := strconv.ParseFloat(maps[0]["purchase_service_order_fkid"].(string), 64)
	if erqty != nil || erreceive != nil || errejected != nil {

	}

	//check if quantity will  be equal to received + rejected + formData.qtypurcahse

	if qty == received+rejected {
		//update the PO as completed

		_, errPO := o.Raw("Update purchase_service_order set status=? WHERE _id=? and purchase_service_order._orgid=? ", "5", maps[0]["purchase_service_order_fkid"], formData.Orgid).Exec()
		if errPO != nil {
			//fmt.Println("Error in query when no inventory")
			ormTransactionalErr = o.Rollback()
			return 0
		}

		_, errPOStatus := o.Raw("INSERT INTO po_so_status (_orgid, purchase_service_order_fkid, actedon_by_fkid, date_actedon, status) VALUES (?,?,?,?,?)", formData.Orgid, maps[0]["purchase_service_order_fkid"], formData.Userid, time.Now(), "5").Exec()
		if errPOStatus != nil {
			ormTransactionalErr = o.Rollback()
			return 0
		}
	}

	var maps09 []orm.Params
	_, err09 := o.Raw("SELECT _id from supplier_org_assoc where company_name=? and _orgid=? ", formData.Suppliers, formData.Orgid).Values(&maps09)
	_, err3 := o.Raw("INSERT INTO po_so_supplier_invoice (_orgid, po_so_items_with_grn_list, grn_no, invoice_no, invoice_date, dc_no, dc_date,supplier_org_assoc_fkid,purchase_service_order_fkid) VALUES (?,?,?,?,?,?,?,?,?)", formData.Orgid, maps[0]["_id"], new_grn, formData.SupplierInvoice, formData.SupplierInvoiceDt, formData.SupplierDcNo, formData.SupplierDcDt, maps09[0]["_id"], maps[0]["purchase_service_order_fkid"]).Exec()
	if err3 != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}
	var maps1 []orm.Params
	_, err4 := o.Raw("SELECT po_so_items_with_grn._id,purchase_service_order_fkid,item_org_assoc_fkid,rate,received_sent_quantity,currency,geo_location FROM purchase_service_order,po_so_items_with_grn WHERE po_so_items_with_grn._orgid=? and grn_no=? and po_so_items_with_grn.purchase_service_order_fkid=purchase_service_order._id", formData.Orgid, new_grn).Values(&maps1)
	_, err5 := o.Raw("INSERT INTO item_stock (_orgid, item_org_assoc_fkid, quantity, start_date, rate, currency, geo_location,last_updated,latest,po_so_items_with_grn_fkid, batch_no) VALUES (?,?,?,?,?,?,?,?,?,?,?)", formData.Orgid, maps1[0]["item_org_assoc_fkid"], maps1[0]["received_sent_quantity"], time.Now(), maps1[0]["rate"], maps1[0]["currency"], maps1[0]["geo_location"], time.Now(), "1", maps[0]["_id"], formData.BatchNo).Exec()
	if err5 != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}
	_, err6 := o.Raw("INSERT INTO item_rates (_orgid, item_org_assoc_fkid, supplier_org_assoc_fkid, rate, currency, start_date,latest) VALUES (?,?,?,?,?,?,?)", formData.Orgid, maps1[0]["item_org_assoc_fkid"], maps09[0]["_id"], maps1[0]["rate"], maps1[0]["currency"], time.Now(), "1").Exec()
	if err6 != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}
	var maps4 []orm.Params
	num, err07 := o.Raw("SELECT _id FROM item_total_inventory WHERE item_org_assoc_fkid=? and _orgid=? and geo_location=?", maps1[0]["item_org_assoc_fkid"], formData.Orgid, maps1[0]["geo_location"]).Values(&maps4)
	if err07 == nil {
		if num <= 0 {
			var low_level string
			var m string
			var mapsmin []orm.Params
			_, errMin := o.Raw("SELECT min_stock_trigger FROM item_org_assoc WHERE _orgid=? and _id=?", formData.Orgid, maps1[0]["item_org_assoc_fkid"]).Values(&mapsmin)
			if errMin == nil {

			}
			m = mapsmin[0]["min_stock_trigger"].(string)
			total_inventory, _ := strconv.ParseFloat(formData.AQuantity, 64)
			min_trigger, _ := strconv.ParseFloat(m, 64)
			if total_inventory >= min_trigger {
				low_level = "0"
			} else {
				low_level = "1"
			}
			_, err9 := o.Raw("INSERT INTO item_total_inventory (_orgid, item_org_assoc_fkid,total_quantity, last_updated, geo_location, status,low_level) VALUES (?,?,?,?,?,?,?)", formData.Orgid, maps1[0]["item_org_assoc_fkid"], formData.AQuantity, time.Now(), maps1[0]["geo_location"], "1", low_level).Exec()
			if err9 != nil {
				//fmt.Println("Error in query when no inventory")
				ormTransactionalErr = o.Rollback()
				return 0
			}
		} else {
			var maps2 []orm.Params
			_, err8 := o.Raw("SELECT total_quantity from item_total_inventory where item_org_assoc_fkid=? and _orgid=? and status=? ", maps1[0]["item_org_assoc_fkid"], formData.Orgid, "1").Values(&maps2)
			i := maps2[0]["total_quantity"].(string)
			Sum, _ := strconv.ParseFloat(i, 64)
			//Sum,ok := i.(float64)
			//fmt.Println("****Previous Quantity Sum: ", Sum)
			//_, err7 := o.Raw("Update item_total_inventory SET status=? where item_org_assoc_fkid=? and _orgid=? ","0",maps1[0]["item_org_assoc_fkid"],formData.Orgid).Exec()

			qty := formData.AQuantity
			//QtyObs := f.(float64)
			QtyObs, _ := strconv.ParseFloat(qty, 64)
			var SubSum float64
			SubSum = Sum + QtyObs
			var low_level string
			var m string
			var mapsmin []orm.Params
			_, errMin := o.Raw("SELECT min_stock_trigger FROM item_org_assoc WHERE _orgid=? and _id=?", formData.Orgid, maps1[0]["item_org_assoc_fkid"]).Values(&mapsmin)
			if errMin == nil {

			}
			m = mapsmin[0]["min_stock_trigger"].(string)
			//total_inventory, _ := strconv.ParseFloat(SubSum,64)
			min_trigger, _ := strconv.ParseFloat(m, 64)
			if SubSum >= min_trigger {
				low_level = "0"
			} else {
				low_level = "1"
			}
			var mapsinv []orm.Params
			_, err16 := o.Raw("SELECT _id FROM item_total_inventory WHERE _orgid=? and item_org_assoc_fkid=? and status=1", formData.Orgid, maps1[0]["item_org_assoc_fkid"]).Values(&mapsinv)
			_, err15 := o.Raw("UPDATE item_total_inventory set status = 0 WHERE _id = ? and _orgid = ?", mapsinv[0]["_id"], formData.Orgid).Exec()
			if err15 != nil {
				ormTransactionalErr = o.Rollback()
				return 0
			}
			_, err9 := o.Raw("INSERT INTO item_total_inventory (_orgid, item_org_assoc_fkid,total_quantity, last_updated, geo_location, status,low_level) VALUES (?,?,?,?,?,?,?)", formData.Orgid, maps1[0]["item_org_assoc_fkid"], SubSum, time.Now(), maps1[0]["geo_location"], "1", low_level).Exec()
			if err9 != nil {
				ormTransactionalErr = o.Rollback()
				return 0
			}
			if err8 != nil || err16 != nil {
				//fmt.Println("Error in Queries when Inventory present")
				ormTransactionalErr = o.Rollback()
				return 0
			}
		}
	} else {
		//fmt.Println("Error in Query")
		ormTransactionalErr = o.Rollback()
		return 0
	}

	if err != nil || err2 != nil || err4 != nil || err09 != nil || ormTransactionalErr != nil {

		return 0
	}

	ormTransactionalErr = o.Commit()
	return 1
}

//------------------------- New Function for GRN
// THIS WILL BE CALLED BATCH WISE
func CreateGRN(formData GRNData) int {

	o := orm.NewOrm()
	ormTransactionalErr := o.Begin()
	var note *string
	note = nil
	if formData.AddNote != "" {
		tempnote := formData.AddNote
		note = &tempnote
	}

	//FETCH THE ID, PURCHASE_ORDER_FKID AND ITEM_ORG_ASSOC_FKID FROM PO_SO_ITEMS [THIS HAS PO V/S ITEMS DATA]
	//WE WILL GET LIST OF PO_SO_ITEMS
	var maps []orm.Params

	_, err := o.Raw("SELECT po_so_items._id,purchase_service_order_fkid,item_org_assoc_fkid, rate, quantity, total_cost, received_quantity,purchase_service_order.geo_location, purchase_service_order.currency FROM po_so_items,purchase_service_order,item_org_assoc WHERE po_so_items._orgid=? and purchase_order_code_gen=? and item_unique_code=? and po_so_items.item_org_assoc_fkid=item_org_assoc._id and purchase_service_order._id=po_so_items.purchase_service_order_fkid and purchase_service_order._orgid=? and item_org_assoc._orgid=?", formData.Orgid, formData.PoNo, formData.Itemss, formData.Orgid, formData.Orgid).Values(&maps)
	if err != nil {

	}

	new_aqty := "0.0"
	if formData.AQuantity != "nil" && formData.AQuantity != "" {
		new_aqty = formData.AQuantity
	}
	NewAQty, _ := strconv.ParseFloat(new_aqty, 64)
	NewAccepted := NewAQty
	if maps[0]["received_quantity"] != nil {
		p := maps[0]["received_quantity"].(string)
		Previous, _ := strconv.ParseFloat(p, 64)
		NewAccepted = Previous + NewAQty
	}

	new_rqty := "0.0"
	if formData.RQuantity != "nil" && formData.RQuantity != "" {
		new_rqty = formData.RQuantity
	}
	NewRQty, _ := strconv.ParseFloat(new_rqty, 64)
	NewRejected := NewRQty
	if maps[0]["rejected_quantity"] != nil {
		p := maps[0]["rejected_quantity"].(string)
		Previous, _ := strconv.ParseFloat(p, 64)
		NewRejected = Previous + NewRQty
	}

	location := maps[0]["geo_location"].(string)
	itemid := maps[0]["item_org_assoc_fkid"].(string)
	itemID, _ := strconv.ParseInt(itemid, 10, 64)

	//UPDATE THE QUANTITY OF ITEM IN PO_SO_ITEMS [FOR QTY RECEIVED AND REJECTED IN THAT BATCH]
	_, errnew := o.Raw("Update po_so_items SET received_quantity=?, rejected_quantity=? where purchase_service_order_fkid=? and item_org_assoc_fkid=? and _orgid=? and status=1", NewAccepted, NewRejected, maps[0]["purchase_service_order_fkid"], maps[0]["item_org_assoc_fkid"], formData.Orgid).Exec()
	if errnew != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}

	//CHECK IF BATCH NO IS GIVEN BY THE USER, ELSE GET A GENERATED BATCH NO
	//RIGHT NOW COMBINING THE PURCHASE ORDER NUMBER AND GRN CODE GEN AS THE BATCH NO
	generatedBatchNo := formData.BatchNo

	var batchNoSeq []orm.Params
	var incBatchNo int64
	incBatchNo = 0
	// TO DO Refactor to order by Id and get the last batch_no_seq
	ct, errBatch := o.Raw("SELECT max(batch_no_seq) as maxBatchNo FROM po_so_items_with_grn WHERE _orgid=?", formData.Orgid).Values(&batchNoSeq)

	if ct > 0 && errBatch == nil {
		if batchNoSeq[0]["maxBatchNo"] != nil {
			maxbatch := batchNoSeq[0]["maxBatchNo"].(string)
			incBatchNo, _ = strconv.ParseInt(maxbatch, 10, 64)
			incBatchNo = incBatchNo + 1 // INCREMENT THE SEQUENCE BY 1
			if formData.BatchNo == "" {
				generatedBatchNo = formData.NewGenCode + strconv.Itoa(int(incBatchNo)) // SYSTEM DEFINED BATCH NUM IF USER DINT PROVIDE
			}
		} else {
			incBatchNo = incBatchNo + 1
			generatedBatchNo = formData.NewGenCode + "1"
		}
	} else {
		incBatchNo = incBatchNo + 1
		generatedBatchNo = formData.NewGenCode + "1"
	}

	var lr_date *string
	lr_date = nil
	if formData.LrDt != "" {
		templr_date := formData.LrDt
		lr_date = &templr_date
	}

	var lr_no *string
	lr_no = nil
	if formData.LrNo != "" {
		templr_no := formData.LrNo
		lr_no = &templr_no
	}

	var transporter *string
	transporter = nil
	if formData.Transporter != "" {
		temptransporter := formData.Transporter
		transporter = &temptransporter
	}

	var vehicle *string
	vehicle = nil
	if formData.Vehicle != "" {
		tempvehicle := formData.Vehicle
		vehicle = &tempvehicle
	}

	//CREATE ENTRY IN PO_SO_ITEMS_WITH_GRN WITH THE BATCH NO
	_, err1 := o.Raw("INSERT INTO po_so_items_with_grn (_orgid, purchase_service_order_fkid, item_org_assoc_fkid, po_so_items_fkid, grn_no, grn_status, delivered_date,rate,quantity,total_cost,received_sent_quantity,rejected_quantity,rejected_note,grn_no_gen, batch_no, po_so_supplier_invoice_fkid, batch_no_seq, geo_location, lr_date, lr_number, transporter_fkid, vehicle_fkid) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ", formData.Orgid, maps[0]["purchase_service_order_fkid"], maps[0]["item_org_assoc_fkid"], maps[0]["_id"], formData.GRNNo, "1", formData.DeliveredDate, maps[0]["rate"], maps[0]["quantity"], formData.TotalCost, formData.AQuantity, formData.RQuantity, &note, formData.NewGenCode, generatedBatchNo, formData.SupplierInvoiceFkId, incBatchNo, formData.DeliveryLocation, &lr_date, &lr_no, &transporter, &vehicle).Exec()
	if err1 != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}

	//GET THE INSERTED ID FROM THE PO_SO_ITEMS_WITH_GRN
	var grnData []orm.Params
	_, err2 := o.Raw("SELECT _id,purchase_service_order_fkid,item_org_assoc_fkid,rate,received_sent_quantity FROM po_so_items_with_grn WHERE _orgid=? and grn_no_gen=? and item_org_assoc_fkid=? and batch_no =?", formData.Orgid, formData.NewGenCode, itemID, generatedBatchNo).Values(&grnData)
	if err2 != nil {

	}

	grnId := grnData[0]["_id"].(string)
	grnFkId, _ := strconv.ParseInt(grnId, 10, 64)

	rate := grnData[0]["rate"].(string)
	itemRate, _ := strconv.ParseFloat(rate, 64)

	//INSERT IN THE ITEM_STOCK TABLE

	itemstockData := ItemStockTable{
		OrgId:            formData.Orgid,
		Itemid:           itemID,
		QuantityModified: NewAQty,
		Location:         location,
		GRNNumber:        formData.NewGenCode,
		InsertTableFkid:  grnFkId,
		StartDate:        time.Now(),
		EndDate:          time.Now(),
		ExpiresBy:        time.Now(), // Temp assignment as no provision from UI
		BatchNo:          generatedBatchNo,
		Rate:             itemRate,
		InsertStatus:     4,
	}

	insertStatus := InsertUpdateItemStock(itemstockData)

	if insertStatus == 0 {
		ormTransactionalErr = o.Rollback()
		return 0
	}

	//UPDATE THE ITEM TOTAL INVENTORY
	// itemInventoryData := ItemTotalInventory{
	// 	OrgId:            formData.Orgid,
	// 	Itemid:           itemID,
	// 	QuantityModified: NewAQty,
	// 	Location:         location,
	// 	AddRemoveFlag:    1,
	// }

	//insertInvStatus := InsertUpdateItemTotalInventory(itemInventoryData)
	insertInvStatus := ItemTotalInventoryUpdateByPkid(strconv.FormatInt(itemID, 10), formData.Orgid, location)
	if insertInvStatus == 0 {
		ormTransactionalErr = o.Rollback()
		return 0
	}

	//FETCH THE DETAILS FROM SUPPLIER AND INSERT IN PO_SO_SUPPLIER_INVOICE
	var supplierDetail []orm.Params
	_, err09 := o.Raw("SELECT _id from supplier_org_assoc where company_name=? and _orgid=? ", formData.Suppliers, formData.Orgid).Values(&supplierDetail)

	if err09 != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}

	//INSERT INTO ITEM RATES
	_, err6 := o.Raw("INSERT INTO item_rates (_orgid, item_org_assoc_fkid, supplier_org_assoc_fkid, rate, currency, start_date,latest) VALUES (?,?,?,?,?,?,?)", formData.Orgid, grnData[0]["item_org_assoc_fkid"], supplierDetail[0]["_id"], grnData[0]["rate"], maps[0]["currency"], time.Now(), "1").Exec()
	if err6 != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}

	ormTransactionalErr = o.Commit()
	if ormTransactionalErr != nil {
		return 0
	}
	return 1
}

//-------------------------End of Function

func InsertPOSOSupplierInvoice(supplierInvoice SupplierInvoice) int64 {
	o := orm.NewOrm()
	ormTransactionalErr := o.Begin()
	var Id int64
	Id = 0

	_, err3 := o.Raw("INSERT INTO po_so_supplier_invoice (_orgid, supplier_org_assoc_fkid, invoice_no, invoice_date, dc_no, dc_date,date_added) VALUES (?,?,?,?,?,?,?)", supplierInvoice.Orgid, supplierInvoice.SupplierFkId, supplierInvoice.SupplierInvoice, supplierInvoice.SupplierInvoiceDt, supplierInvoice.SupplierDcNo, supplierInvoice.SupplierDcDt, supplierInvoice.DateAdded).Exec()
	if err3 != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}

	var maps []orm.Params

	num, err := o.Raw("SELECT _id FROM po_so_supplier_invoice where _orgid=? and supplier_org_assoc_fkid=? and invoice_no=? and invoice_date=? and dc_no=? and dc_date=? and date_added=? ", supplierInvoice.Orgid, supplierInvoice.SupplierFkId, supplierInvoice.SupplierInvoice, supplierInvoice.SupplierInvoiceDt, supplierInvoice.SupplierDcNo, supplierInvoice.SupplierDcDt, supplierInvoice.DateAdded).Values(&maps)

	if err == nil && num > 0 {
		q := maps[0]["_id"].(string)
		Id, _ = strconv.ParseInt(q, 10, 64)
	} else {
		Id = 0
	}

	ormTransactionalErr = o.Commit()
	if ormTransactionalErr != nil {
		return 0
	}
	return Id
}

func GetPOdetails(formData Createpo) map[string]orm.Params {

	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT pso.status, pso._id, pso.purchase_order_code_gen, pso.type, pso.tax_regime, pso.date as p_date, pso.currency, pso.supplier_fkid,pso.delivery_date as d_date, organisation_factories.factory_location, pso.payment_terms, pso.billing_address, pso.total_taxable_value, pso.total_tax_cess, pso.total_freight_packaging_others, pso.total_after_taxes, pso.total_invoice_value, pso.transporter_fkid, pso.vehicle_fkid,pso.transporter_warehouse_fkid, soa.company_name FROM purchase_service_order as pso, supplier_org_assoc as soa, organisation_factories WHERE soa._id=pso.supplier_fkid and (type=0 or type=1) and pso._orgid=? and soa._orgid=? and organisation_factories._orgid=? and pso.geo_location=organisation_factories._id", formData.Orgid, formData.Orgid, formData.Orgid).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		var i int64 = 0
		for ; i < num; i++ {
			//fmt.Println(maps[i])
		}
		for k, v := range maps {
			if maps[k]["p_date"] != nil && maps[k]["p_date"] != "" {
				//t := time.Now()
				// const layout = "2006-01-02"
				// snapshot := maps[k]["p_date"].(string)
				// t, _ := time.Parse(layout, snapshot)
				// maps[k]["p_date"] = t.Format("02/01/2006")
				maps[k]["p_date"] = convertDateString(maps[k]["p_date"].(string))
			}
			if maps[k]["d_date"] != nil && maps[k]["d_date"] != "" {
				maps[k]["d_date"] = convertDateString(maps[k]["d_date"].(string))
			}
			if maps[k]["total_taxable_value"] == nil {
				maps[k]["total_taxable_value"] = ""
			}
			if maps[k]["total_tax_cess"] == nil {
				maps[k]["total_tax_cess"] = ""
			}
			if maps[k]["total_freight_packaging_others"] == nil {
				maps[k]["total_freight_packaging_others"] = ""
			}
			if maps[k]["total_after_taxes"] == nil {
				maps[k]["total_after_taxes"] = ""
			}
			if maps[k]["total_invoice_value"] == nil {
				maps[k]["total_invoice_value"] = ""
			}
			// if maps[k]["excise_duty"] == nil {
			// 	maps[k]["excise_duty"] = ""
			// }
			// if maps[k]["discount_percentage"] == nil {
			// 	maps[k]["discount_percentage"] = ""
			//}
			if maps[k]["billing_address"] == nil {
				maps[k]["billing_address"] = ""
			}
			if maps[k]["payment_terms"] == nil {
				maps[k]["payment_terms"] = ""
			}
			if maps[k]["instructions"] == nil {
				maps[k]["instructions"] = ""
			}
			if maps[k]["other_total"] == nil {
				maps[k]["other_total"] = ""
			}
			if maps[k]["factory_location"] == nil {
				maps[k]["factory_location"] = ""
			}

			// var transMaps []orm.Params
			// transNum, transErr := o.Raw("SELECT * FROM po_so_items WHERE _orgid=? and purchase_service_order_fkid=? and status=1", formData.Orgid, maps[k]["_id"]).Values(&transMaps)
			// if transNum > 0 && transErr == nil {
			// 	if transMaps[0]["transporter_fkid"] == nil {
			// 		maps[k]["transporter_fkid"] = ""
			// 	} else {
			// 		maps[k]["transporter_fkid"] = transMaps[0]["transporter_fkid"].(string)
			// 	}
			// 	if transMaps[0]["vehicle_fkid"] == nil {
			// 		maps[k]["vehicle_fkid"] = ""
			// 	} else {
			// 		maps[k]["vehicle_fkid"] = transMaps[0]["vehicle_fkid"].(string)
			// 	}
			// } else {
			// 	maps[k]["vehicle_fkid"] = ""
			// 	maps[k]["vehicle_fkid"] = ""
			// }

			if maps[k]["transporter_fkid"] != nil && maps[k]["transporter_fkid"] != "" {
				var transportermaps []orm.Params
				transporternum, transportererr := o.Raw("SELECT * FROM transporter WHERE _id=? and _orgid=? ", maps[k]["transporter_fkid"], formData.Orgid).Values(&transportermaps)
				if transporternum > 0 && transportererr == nil {
					maps[k]["transporter_name"] = transportermaps[0]["transporter_name"]
				}
			}

			if maps[k]["transporter_warehouse_fkid"] != nil && maps[k]["transporter_warehouse_fkid"] != "" {
				var transporter_warehouse_maps []orm.Params
				transporterwarehousenum, transporterwarehouseerr := o.Raw("SELECT * FROM warehouse WHERE _id=? and _orgid=? ", maps[k]["transporter_warehouse_fkid"], formData.Orgid).Values(&transporter_warehouse_maps)
				if transporterwarehousenum > 0 && transporterwarehouseerr == nil {
					maps[k]["transporter_warehouse_address"] = transporter_warehouse_maps[0]["billing_address"].(string) + ", " + transporter_warehouse_maps[0]["city"].(string) + ", " + transporter_warehouse_maps[0]["state"].(string) + ", " + transporter_warehouse_maps[0]["country"].(string) + ", " + transporter_warehouse_maps[0]["pincode"].(string)
				}
			}

			if maps[k]["vehicle_fkid"] != nil && maps[k]["vehicle_fkid"] != "" {
				var vehiclemaps []orm.Params
				vehiclenum, vehicleerr := o.Raw("SELECT * FROM vehicle WHERE _id=? and _orgid=? ", maps[k]["vehicle_fkid"], formData.Orgid).Values(&vehiclemaps)
				if vehiclenum > 0 && vehicleerr == nil {
					maps[k]["registration_number"] = vehiclemaps[0]["registration_number"]
				}
			}
			key := "po_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetdelayedPOdetails(formData Createpo) map[string]orm.Params {

	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT pso.status, pso._id, pso.purchase_order_code_gen, pso.type, pso.tax_regime, pso.date as p_date, pso.currency, pso.supplier_fkid,pso.delivery_date as d_date, organisation_factories.factory_location, pso.payment_terms, pso.billing_address, pso.total_taxable_value, pso.total_tax_cess, pso.total_freight_packaging_others, pso.total_after_taxes, pso.total_invoice_value, pso.transporter_fkid, pso.vehicle_fkid, soa.company_name FROM purchase_service_order as pso, supplier_org_assoc as soa, organisation_factories WHERE soa._id=pso.supplier_fkid and (type=0 or type=1) and pso._orgid=? and soa._orgid=? and organisation_factories._orgid=? and pso.geo_location=organisation_factories._id and pso.delivery_date<?", formData.Orgid, formData.Orgid, formData.Orgid, GetTodaysDate()).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		var i int64 = 0
		for ; i < num; i++ {
			//fmt.Println(maps[i])
		}
		for k, v := range maps {
			if maps[k]["p_date"] != nil && maps[k]["p_date"] != "" {
				//t := time.Now()
				// const layout = "2006-01-02"
				// snapshot := maps[k]["p_date"].(string)
				// t, _ := time.Parse(layout, snapshot)
				// maps[k]["p_date"] = t.Format("02/01/2006")
				maps[k]["p_date"] = convertDateString(maps[k]["p_date"].(string))
			}
			if maps[k]["d_date"] != nil && maps[k]["d_date"] != "" {
				maps[k]["d_date"] = convertDateString(maps[k]["d_date"].(string))
			}
			if maps[k]["total_taxable_value"] == nil {
				maps[k]["total_taxable_value"] = ""
			}
			if maps[k]["total_tax_cess"] == nil {
				maps[k]["total_tax_cess"] = ""
			}
			if maps[k]["total_freight_packaging_others"] == nil {
				maps[k]["total_freight_packaging_others"] = ""
			}
			if maps[k]["total_after_taxes"] == nil {
				maps[k]["total_after_taxes"] = ""
			}
			if maps[k]["total_invoice_value"] == nil {
				maps[k]["total_invoice_value"] = ""
			}
			// if maps[k]["excise_duty"] == nil {
			// 	maps[k]["excise_duty"] = ""
			// }
			// if maps[k]["discount_percentage"] == nil {
			// 	maps[k]["discount_percentage"] = ""
			//}
			if maps[k]["billing_address"] == nil {
				maps[k]["billing_address"] = ""
			}
			if maps[k]["payment_terms"] == nil {
				maps[k]["payment_terms"] = ""
			}
			if maps[k]["instructions"] == nil {
				maps[k]["instructions"] = ""
			}
			if maps[k]["other_total"] == nil {
				maps[k]["other_total"] = ""
			}
			if maps[k]["factory_location"] == nil {
				maps[k]["factory_location"] = ""
			}

			// var transMaps []orm.Params
			// transNum, transErr := o.Raw("SELECT * FROM po_so_items WHERE _orgid=? and purchase_service_order_fkid=? and status=1", formData.Orgid, maps[k]["_id"]).Values(&transMaps)
			// if transNum > 0 && transErr == nil {
			// 	if transMaps[0]["transporter_fkid"] == nil {
			// 		maps[k]["transporter_fkid"] = ""
			// 	} else {
			// 		maps[k]["transporter_fkid"] = transMaps[0]["transporter_fkid"].(string)
			// 	}
			// 	if transMaps[0]["vehicle_fkid"] == nil {
			// 		maps[k]["vehicle_fkid"] = ""
			// 	} else {
			// 		maps[k]["vehicle_fkid"] = transMaps[0]["vehicle_fkid"].(string)
			// 	}
			// } else {
			// 	maps[k]["vehicle_fkid"] = ""
			// 	maps[k]["vehicle_fkid"] = ""
			// }

			if maps[k]["transporter_fkid"] != nil && maps[k]["transporter_fkid"] != "" {
				var transportermaps []orm.Params
				transporternum, transportererr := o.Raw("SELECT * FROM transporter WHERE _id=? and _orgid=? ", maps[k]["transporter_fkid"], formData.Orgid).Values(&transportermaps)
				if transporternum > 0 && transportererr == nil {
					maps[k]["transporter_name"] = transportermaps[0]["transporter_name"]
				}
			}

			if maps[k]["vehicle_fkid"] != nil && maps[k]["vehicle_fkid"] != "" {
				var vehiclemaps []orm.Params
				vehiclenum, vehicleerr := o.Raw("SELECT * FROM vehicle WHERE _id=? and _orgid=? ", maps[k]["vehicle_fkid"], formData.Orgid).Values(&vehiclemaps)
				if vehiclenum > 0 && vehicleerr == nil {
					maps[k]["registration_number"] = vehiclemaps[0]["registration_number"]
				}
			}
			key := "po_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetPOdetailsByPOpkId(poId string, orgid string) map[string]orm.Params {

	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params

	num, err := o.Raw("SELECT pso.status, pso._id,pso.unique_serial_no, pso.type,pso.date as p_date, pso.tax_regime, pso.currency,pso.supplier_fkid,pso.delivery_date as d_date,organisation_factories.factory_location,pso.instructions,pso.payment_terms,pso.billing_address,pso.discount_percentage,pso.excise_duty,pso.freight_type,pso.freight_cost,pso.octoroi,pso.item_total,pso.other_charges,pso.other_total,pso.billing_address,pso.final_discount_percentage,pso.final_total,soa.company_name,pso.purchase_order_code_gen, pso.transporter_fkid, pso.vehicle_fkid FROM purchase_service_order as pso, supplier_org_assoc as soa, organisation_factories WHERE soa._id=pso.supplier_fkid and (type=0 or type=1) and pso._orgid=? and soa._orgid=? and organisation_factories._orgid=? and pso.geo_location=organisation_factories._id and pso._id=? ", orgid, orgid, orgid, poId).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		var i int64 = 0
		for ; i < num; i++ {
			//fmt.Println(maps[i])
		}
		for k, v := range maps {
			if maps[k]["p_date"] != nil && maps[k]["p_date"] != "" {
				maps[k]["p_date"] = convertDateString(maps[k]["p_date"].(string))
			}
			if maps[k]["d_date"] != nil && maps[k]["d_date"] != "" {
				maps[k]["d_date"] = convertDateString(maps[k]["d_date"].(string))
			}
			if maps[k]["transporter_fkid"] != nil && maps[k]["transporter_fkid"] != "" {
				var transportermaps []orm.Params
				transporternum, transportererr := o.Raw("SELECT * FROM transporter WHERE _id=? and _orgid=? ", maps[k]["transporter_fkid"], orgid).Values(&transportermaps)
				if transporternum > 0 && transportererr == nil {
					maps[k]["transporter_name"] = transportermaps[0]["transporter_name"]
				}
			}

			if maps[k]["vehicle_fkid"] != nil && maps[k]["vehicle_fkid"] != "" {
				var vehiclemaps []orm.Params
				vehiclenum, vehicleerr := o.Raw("SELECT * FROM vehicle WHERE _id=? and _orgid=? ", maps[k]["vehicle_fkid"], orgid).Values(&vehiclemaps)
				if vehiclenum > 0 && vehicleerr == nil {
					maps[k]["registration_number"] = vehiclemaps[0]["registration_number"]
				}
			}
			key := "po_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetAllGrnDetails(formData GRNData) map[string]orm.Params {
	// var stock []Stock
	// o := orm.NewOrm()

	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	//num, err := o.Raw("SELECT * FROM item_master ").Values(&maps)

	num, err := o.Raw("SELECT g._id as psig_id, g.grn_no_gen, g.batch_no, p.purchase_order_code_gen, s.company_name, o.invoice_narration, g.received_sent_quantity, g.delivered_date as d_date, ofactory.factory_location , i.invoice_no , i.invoice_date as i_date, i.dc_no, i.dc_date as c_date, g.rejected_note, g.grn_status, g.lr_date as l_date, g.lr_number, g.transporter_fkid, g.vehicle_fkid FROM po_so_items_with_grn as g, po_so_supplier_invoice as i, purchase_service_order as p, supplier_org_assoc as s, item_org_assoc as o,	organisation_factories as ofactory where g.purchase_service_order_fkid = p._id and g.item_org_assoc_fkid=o._id and p.geo_location = ofactory._id and i._id = g.po_so_supplier_invoice_fkid and i.supplier_org_assoc_fkid=s._id and i._orgid=? and g._orgid=? and p._orgid=? and s._orgid=? and o._orgid=?", formData.Orgid, formData.Orgid, formData.Orgid, formData.Orgid, formData.Orgid).Values(&maps)

	if err == nil && num > 0 {
		for k, v := range maps {
			if maps[k]["d_date"] != nil && maps[k]["d_date"] != "" {
				maps[k]["d_date"] = convertDateString(maps[k]["d_date"].(string))
			}
			if maps[k]["i_date"] != nil && maps[k]["i_date"] != "" {
				maps[k]["i_date"] = convertDateString(maps[k]["i_date"].(string))
			}
			if maps[k]["c_date"] != nil && maps[k]["c_date"] != "" {
				maps[k]["c_date"] = convertDateString(maps[k]["c_date"].(string))
			}
			if maps[k]["l_date"] != nil && maps[k]["l_date"] != "" {
				maps[k]["l_date"] = convertDateString(maps[k]["l_date"].(string))
			}
			if maps[k]["transporter_fkid"] != nil && maps[k]["transporter_fkid"] != "" {
				var transportermaps []orm.Params
				transporternum, transportererr := o.Raw("SELECT * FROM transporter WHERE _id=? and _orgid=? ", maps[k]["transporter_fkid"], formData.Orgid).Values(&transportermaps)
				if transporternum > 0 && transportererr == nil {
					maps[k]["transporter_name"] = transportermaps[0]["transporter_name"]
				}
			}

			if maps[k]["vehicle_fkid"] != nil && maps[k]["vehicle_fkid"] != "" {
				var vehiclemaps []orm.Params
				vehiclenum, vehicleerr := o.Raw("SELECT * FROM vehicle WHERE _id=? and _orgid=? ", maps[k]["vehicle_fkid"], formData.Orgid).Values(&vehiclemaps)
				if vehiclenum > 0 && vehicleerr == nil {
					maps[k]["registration_number"] = vehiclemaps[0]["registration_number"]
				}
			}
			key := "grn_" + strconv.Itoa(k)
			result[key] = v
		}
	} else {
		result["status"] = nil
	}
	return result
}

func GetPurchaseOrderDetailsByPONo(formData GRNData) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	//num, err := o.Raw("SELECT * FROM item_master ").Values(&maps)

	num, err := o.Raw("SELECT distinct(purchase_order_code_gen),company_name FROM purchase_service_order,po_so_items,supplier_org_assoc WHERE purchase_service_order._orgid=? and po_so_items._orgid=? and supplier_org_assoc._orgid=? and supplier_org_assoc._id=purchase_service_order.supplier_fkid and purchase_service_order._id=po_so_items.purchase_service_order_fkid and po_so_items.quantity != po_so_items.received_quantity and purchase_service_order.status=2", formData.Orgid, formData.Orgid, formData.Orgid).Values(&maps)

	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		var i int64 = 0
		for ; i < num; i++ {
			//fmt.Println(maps[i])
		}

		for k, v := range maps {

			key := "grn_" + strconv.Itoa(k)

			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetItems(formData GRNData) map[string]orm.Params {

	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	//var maps1 []orm.Params
	// count, err1 := o.Raw("SELECT pgrn._id FROM purchase_service_order as pso, po_so_items_with_grn as pgrn WHERE pgrn.purchase_service_order_fkid=pso._id and pso.unique_serial_no=? and pso._orgid=? and pgrn.purchase_service_order_fkid=pso._id and pgrn._orgid=?", formData.PoNo, formData.Orgid, formData.Orgid).Values(&maps1)
	// if err1 == nil {
	// 	if count <= 0 {
	//When no GRN is raised
	var maps []orm.Params
	num, err := o.Raw("SELECT io.invoice_narration, io.item_unique_code, psi.quantity, psi.received_quantity, io.unit_measure FROM item_org_assoc as io, purchase_service_order as pso, po_so_items as psi WHERE io._orgid=? and pso._orgid=? and psi._orgid=? and psi.purchase_service_order_fkid=pso._id and psi.item_org_assoc_fkid=io._id and psi.status=? and pso.purchase_order_code_gen=?", formData.Orgid, formData.Orgid, formData.Orgid, "1", formData.PoNo).Values(&maps)
	if err == nil && num > 0 {
		for k, v := range maps {
			//fmt.Println("Value k ",k)
			var o string
			var r string
			var QtyRemaining float64
			o = maps[k]["quantity"].(string)
			r = maps[k]["received_quantity"].(string)
			QtyOrdered, _ := strconv.ParseFloat(o, 64)
			QtyDelivered, _ := strconv.ParseFloat(r, 64)
			QtyRemaining = QtyOrdered - QtyDelivered
			maps[k]["qremaining"] = QtyRemaining
			maps[k]["Number"] = k
			key := "gi_" + strconv.Itoa(k)
			result[key] = v
		}
	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	// } else {
	// 	//When atleast once grn is raised
	// }
	// } else {
	// 	fmt.Println(err1)
	// 	result["status"] = nil
	// }

	// count, err1 := o.Raw("SELECT po_so_items_with_grn._id FROM purchase_service_order, po_so_items_with_grn WHERE po_so_items_with_grn.purchase_service_order_fkid=purchase_service_order._id and purchase_service_order.unique_serial_no=? and purchase_service_order._orgid=? and po_so_items_with_grn.purchase_service_order_fkid=purchase_service_order._id and po_so_items_with_grn._orgid=?", formData.PoNo, formData.Orgid, formData.Orgid).Values(&maps)
	// if err1 == nil {
	// 	//When Grn raised once
	// 	if count > 0 {
	// 		num, err := o.Raw("SELECT distinct(item_org_assoc.name), item_org_assoc.item_unique_code, item_org_assoc.unit_measure, po_so_items.quantity, Sum(received_sent_quantity) as received_sent_quantity FROM purchase_service_order, item_org_assoc, po_so_items, po_so_items_with_grn WHERE purchase_service_order._id=po_so_items.purchase_service_order_fkid and item_org_assoc._id=po_so_items.item_org_assoc_fkid  and po_so_items._orgid=? and purchase_service_order.unique_serial_no=? and purchase_service_order._orgid=? and po_so_items_with_grn._orgid=? and po_so_items_with_grn.purchase_service_order_fkid=purchase_service_order._id and po_so_items_with_grn.item_org_assoc_fkid=item_org_assoc._id ", formData.Orgid, formData.PoNo, formData.Orgid, formData.Orgid).Values(&maps)
	// 		//num,err := o.Raw("SELECT distinct(io.invoice_narration), io.item_unique_code, io.unit_measure, pgrn.received_sent_quantity, sum(SumGrouped) as received_sent_quantity FROM (select sum(received_sent_quantity) as 'SumGrouped' from purchase_service_order, po_so_items_with_grn where purchase_service_order.unique_serial_no=? and po_so_items_with_grn.purchase_service_order_fkid=purchase_service_order._id and item_org_assoc._id=po_so_items_with_grn.item_org_assoc_fkid and po_so_items_with_grn._orgid=?) item_org_assoc as io, po_so_items_with_grn as pgrn, purchase_service_order as pu WHERE io._orgid=? and pgrn._orgid=? and io._id=pgrn.item_org_assoc_fkid and pu.unique_serial_no=? and pu._id=pgrn.purchase_service_order_fkid", formData.PoNo, formData.Orgid, formData.Orgid, formData.Orgid, formData.PoNo).Values(&maps)
	// 		if err == nil && num > 0 {
	// 			//fmt.Println(maps[0]["name"])
	// 			// var i int64 = 0
	// 			// for ; i<num; i++ {
	// 			// 	//fmt.Println(maps[i])
	// 			// }
	// 			for k, v:= range maps{

	// 				key := "gi_" + strconv.Itoa(k)
	// 				result[key] = v
	// 			}

	// 		} else {
	// 		    fmt.Println(err)
	// 			result["status"] = nil
	// 		}
	// 		//return result
	// 	} else {
	// 		num, err := o.Raw("SELECT distinct(item_org_assoc.name), item_org_assoc.item_unique_code, item_org_assoc.unit_measure, po_so_items.quantity FROM purchase_service_order, item_org_assoc, po_so_items WHERE purchase_service_order._id=po_so_items.purchase_service_order_fkid and item_org_assoc._id=po_so_items.item_org_assoc_fkid  and po_so_items._orgid=? and purchase_service_order.unique_serial_no=? and purchase_service_order._orgid=? ",formData.Orgid,formData.PoNo,formData.Orgid).Values(&maps)
	// 		if err == nil && num > 0 {
	// 			//fmt.Println(maps[0]["name"])
	// 			var i int64 = 0
	// 			for ; i<num; i++ {
	// 				//fmt.Println(maps[i])
	// 			}
	// 			for k, v:= range maps{

	// 				key := "gi_" + strconv.Itoa(k)
	// 				result[key] = v
	// 			}

	// 		} else {
	// 		    fmt.Println(err)
	// 			result["status"] = nil
	// 		}
	// 	//	return result
	// 	}
	// } else {
	//     fmt.Println(err1)
	// 	result["status"] = nil
	// }

	//      num, err := o.Raw("SELECT distinct(item_org_assoc.name), item_org_assoc.unit_measure, po_so_items.rate,po_so_items.total_cost,po_so_items.quantity as available,purchase_service_order.final_total,purchase_service_order.type,purchase_service_order.date,purchase_service_order.geo_location,purchase_service_order.supplier_fkid,supplier_org_assoc.company_name FROM purchase_service_order, item_org_assoc, po_so_items,supplier_org_assoc WHERE purchase_service_order._id=po_so_items.purchase_service_order_fkid and item_org_assoc._id=po_so_items.item_org_assoc_fkid  and po_so_items._orgid=? and purchase_service_order.unique_serial_no=? and purchase_service_order._orgid=? and supplier_org_assoc._id=purchase_service_order.supplier_fkid",formData.Orgid,formData.PoNo,formData.Orgid).Values(&maps)
	// //num, err := o.Raw("SELECT * FROM purchase_service_order, item_org_assoc, po_so_items WHERE purchase_service_order._id=po_so_items.purchase_service_order_fkid and item_org_assoc._id=po_so_items.item_org_assoc_fkid and po_so_items._orgid=? and purchase_service_order.unique_serial_no=?",formData.Orgid,formData.PoNo).Values(&maps)
	// //num, err:=o.Raw("SELECT item_org_assoc.invoice_narration, po_so_items.rate, po_so_items.total_cost, (po_so_items.quantity-po_so_items_with_grn.received_sent_quantity) as difference FROM purchase_service_order, item_org_assoc, po_so_items_with_grn, po_so_items WHERE po_so_items.purchase_service_order_fkid=purchase_service_order._id and item_org_assoc._id=po_so_items.item_org_assoc_fkid and po_so_items_with_grn.po_so_items_fkid=po_so_items._id and po_so_items._orgid=? and purchase_service_order.unique_serial_no=?",formData.Orgid,formData.PoNo).Values(&maps)

	// if err == nil && num > 0 {
	// 	//fmt.Println(maps[0]["name"])
	// 	var i int64 = 0
	// 	for ; i<num; i++ {
	// 		//fmt.Println(maps[i])
	// 	}
	// 	for k, v:= range maps{

	// 		key := "gi_" + strconv.Itoa(k)
	// 		result[key] = v
	// 	}

	// } else {
	//     fmt.Println(err)
	// 	result["status"] = nil
	// }

	return result
}
func GetGRNItems(formData GRNData) map[string]orm.Params {

	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params

	num, err := o.Raw("SELECT pso._id, pso.total_invoice_value, pso.type, pso.date, pso.geo_location, pso.supplier_fkid, pso.transporter_fkid, pso.vehicle_fkid, organisation_factories.factory_location, supplier_org_assoc.company_name FROM purchase_service_order as pso, supplier_org_assoc,organisation_factories WHERE pso._orgid=? and pso.purchase_order_code_gen=? and supplier_org_assoc._orgid=? and supplier_org_assoc._id=pso.supplier_fkid and pso.geo_location=organisation_factories._id and organisation_factories._orgid=?", formData.Orgid, formData.PoNo, formData.Orgid, formData.Orgid).Values(&maps)
	//num, err := o.Raw("SELECT * FROM purchase_service_order, item_org_assoc, po_so_items WHERE purchase_service_order._id=po_so_items.purchase_service_order_fkid and item_org_assoc._id=po_so_items.item_org_assoc_fkid and po_so_items._orgid=? and purchase_service_order.unique_serial_no=?",formData.Orgid,formData.PoNo).Values(&maps)
	//num, err:=o.Raw("SELECT item_org_assoc.invoice_narration, po_so_items.rate, po_so_items.total_cost, (po_so_items.quantity-po_so_items_with_grn.received_sent_quantity) as difference FROM purchase_service_order, item_org_assoc, po_so_items_with_grn, po_so_items WHERE po_so_items.purchase_service_order_fkid=purchase_service_order._id and item_org_assoc._id=po_so_items.item_org_assoc_fkid and po_so_items_with_grn.po_so_items_fkid=po_so_items._id and po_so_items._orgid=? and purchase_service_order.unique_serial_no=?",formData.Orgid,formData.PoNo).Values(&maps)

	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		var i int64 = 0
		for ; i < num; i++ {
			//fmt.Println(maps[i])
		}
		for k, v := range maps {

			key := "gi_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetQuantity(formData GRNData) map[string]orm.Params {

	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params

	num, err := o.Raw("SELECT po_so_items_with_grn._id, (po_so_items.quantity-po_so_items_with_grn.received_sent_quantity) as available FROM purchase_service_order, po_so_items_with_grn, po_so_items WHERE purchase_service_order._id=po_so_items.purchase_service_order_fkid and po_so_items_with_grn.po_so_items_fkid=po_so_items._id and po_so_items._orgid=? and purchase_service_order.unique_serial_no=? order by(po_so_items_with_grn._id) desc limit 1", formData.Orgid, formData.PoNo).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		var i int64 = 0
		for ; i < num; i++ {
			//fmt.Println(maps[i])
		}
		for k, v := range maps {

			key := "av_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetSItems(formData Createso) map[string]orm.Params {

	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params

	num, err := o.Raw("SELECT * FROM supplier_org_assoc, org_supplier_item_assoc, item_org_assoc WHERE org_supplier_item_assoc.supplier_org_assoc_fkid=supplier_org_assoc._id and org_supplier_item_assoc.item_org_assoc_fkid=item_org_assoc._id and org_supplier_item_assoc._orgid=? and company_name=?", formData.Orgid, formData.Contractor).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		var i int64 = 0
		for ; i < num; i++ {
			//fmt.Println(maps[i])
		}
		for k, v := range maps {

			key := "co_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func CreateSO(formData Createso) int {

	o := orm.NewOrm()
	var freighttype *string
	freighttype = nil
	if formData.DispatchFleet != "" {
		tempfreighttype := formData.DispatchFleet
		freighttype = &tempfreighttype
	}
	var freightcost *string
	freightcost = nil
	if formData.DispatchCost != "" {
		tempfreightcost := formData.DispatchCost
		freightcost = &tempfreightcost
	}
	var dissubtotal *string
	dissubtotal = nil
	if formData.SDiscountSubTotal != "" {
		tempdissubtotal := formData.SDiscountSubTotal
		dissubtotal = &tempdissubtotal
	}
	var paymentterms *string
	paymentterms = nil
	if formData.SPaymentTerms != "" {
		temppaymentterms := formData.SPaymentTerms
		paymentterms = &temppaymentterms
	}
	var chargestitle *string
	chargestitle = nil
	if formData.SOtherChargesTitle != "" {
		tempchargestitle := formData.SOtherChargesTitle
		chargestitle = &tempchargestitle
	}
	var chargestotal *string
	chargestotal = nil
	if formData.SOtherChargesValue != "" {
		tempchargestotal := formData.SOtherChargesValue
		chargestotal = &tempchargestotal
	}
	var excise *string
	excise = nil
	if formData.SExciseDuty != "" {
		tempexcise := formData.SExciseDuty
		excise = &tempexcise
	}
	var octoroichrg *string
	octoroichrg = nil
	if formData.SOctroiCharges != "" {
		tempoctoroichrg := formData.SOctroiCharges
		octoroichrg = &tempoctoroichrg
	}
	var finaldis *string
	finaldis = nil
	if formData.SFinalDiscount != "" {
		tempfinaldis := formData.SFinalDiscount
		finaldis = &tempfinaldis
	}
	var billlocation *string
	billlocation = nil
	if formData.SBillingAddress != "" {
		tempbilllocation := formData.SBillingAddress
		billlocation = &tempbilllocation
	}
	var dispatchlocation *string
	dispatchlocation = nil
	if formData.DispatchFrom != "" {
		tempdispatchlocation := formData.DispatchFrom
		dispatchlocation = &tempdispatchlocation
	}
	var itotal *string
	itotal = nil
	if formData.STotal != "" {
		tempitotal := formData.STotal
		itotal = &tempitotal
	}
	var maps []orm.Params
	//var maps2 []orm.Params
	_, err2 := o.Raw("SELECT _id FROM supplier_org_assoc WHERE _orgid=? and company_name=?", formData.Orgid, formData.Contractor).Values(&maps)
	_, err1 := o.Raw("INSERT INTO purchase_service_order (unique_serial_no, type, date, delivery_date, geo_location, discount_percentage, freight_type, freight_cost, payment_terms, other_charges, other_total, currency, supplier_fkid, excise_duty, octoroi, item_total, final_discount_percentage, final_total, billing_address,_orgid,instructions,status) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ", formData.ChallanNo, "1", formData.DateOrder, formData.EstimatedDeliveryDate, &dispatchlocation, &dissubtotal, &freighttype, &freightcost, &paymentterms, &chargestitle, &chargestotal, formData.SCurrency, maps[0]["_id"], &excise, &octoroichrg, &itotal, &finaldis, formData.SFinalTotal, &billlocation, formData.Orgid, formData.Purpose, "0").Exec()
	_, err3 := o.Raw("SELECT _id from purchase_service_order where _orgid=? and unique_serial_no=? and type=?", formData.Orgid, formData.ChallanNo, "1").Values(&maps)
	if formData.ItemName != "" && formData.Quantity != "" {
		_, err0 := o.Raw("INSERT INTO so_items_used (_orgid,purchase_service_order_fkid,item_org_assoc_fkid,quantity_used,status) VALUES (?,?,?,?,?) ", formData.Orgid, maps[0]["_id"], formData.ItemName, formData.Quantity, "1").Exec()
		var maps0 []orm.Params
		num, err := o.Raw("SELECT quantity FROM item_stock WHERE _orgid=? and item_org_assoc_fkid=? and quantity<>0 ORDER BY last_updated ", formData.Orgid, formData.ItemName).Values(&maps0)
		qty := formData.Quantity
		QtyUsed, err := strconv.ParseFloat(qty, 64)
		if err == nil {
			for QtyUsed != 0 {
				j := maps0[num-1]["quantity"].(string)
				t_qty, _ := strconv.ParseFloat(j, 64)
				if t_qty <= QtyUsed {
					_, err := o.Raw("UPDATE item_stock SET quantity=?, last_updated=?  WHERE _orgid=? and item_org_assoc_fkid=? and quantity<>0 ORDER BY last_updated DESC LIMIT 1", "0", time.Now(), formData.Orgid, formData.ItemName).Exec()
					if err == nil {
						QtyUsed = QtyUsed - t_qty
						num = num - 1
					} else {
						//fmt.Println("Error in UPDATE when t_qty<=SubSum")
						break
					}

				} else {
					var new_qty float64
					new_qty = t_qty - QtyUsed
					//fmt.Println("****new_qty: ", new_qty)
					s := strconv.FormatFloat(new_qty, 'E', -1, 64)
					_, err := o.Raw("UPDATE item_stock SET quantity=?, last_updated=?  WHERE _orgid=? and item_org_assoc_fkid=? and quantity<>0 ORDER BY  last_updated DESC LIMIT 1", s, time.Now(), formData.Orgid, formData.ItemName).Exec()
					if err == nil {
						QtyUsed = 0
						//num = 0
					} else {
						//fmt.Println("Error in UPDATE when t_qty>SubSum")
						break
					}

				}
			}

		}

		if err0 != nil {
			//fmt.Println("Error in Item queries")
		}
	}

	if formData.ItemName1 != "" && formData.Quantity1 != "" {
		_, err01 := o.Raw("INSERT INTO so_items_used (_orgid,purchase_service_order_fkid,item_org_assoc_fkid,quantity_used,status) VALUES (?,?,?,?,?) ", formData.Orgid, maps[0]["_id"], formData.ItemName1, formData.Quantity1, "1").Exec()

		var maps0 []orm.Params
		num, err := o.Raw("SELECT quantity FROM item_stock WHERE _orgid=? and item_org_assoc_fkid=? and quantity<>0 ORDER BY last_updated ", formData.Orgid, formData.ItemName1).Values(&maps0)
		qty := formData.Quantity1
		QtyUsed, err := strconv.ParseFloat(qty, 64)
		if err == nil {
			for QtyUsed != 0 {
				j := maps0[num-1]["quantity"].(string)
				t_qty, _ := strconv.ParseFloat(j, 64)
				if t_qty <= QtyUsed {
					_, err := o.Raw("UPDATE item_stock SET quantity=?, last_updated=?  WHERE _orgid=? and item_org_assoc_fkid=? and quantity<>0 ORDER BY last_updated DESC LIMIT 1", "0", time.Now(), formData.Orgid, formData.ItemName1).Exec()
					if err == nil {
						QtyUsed = QtyUsed - t_qty
						num = num - 1
					} else {
						//fmt.Println("Error in UPDATE when t_qty<=SubSum")
						break
					}

				} else {
					var new_qty float64
					new_qty = t_qty - QtyUsed
					//fmt.Println("****new_qty: ", new_qty)
					s := strconv.FormatFloat(new_qty, 'E', -1, 64)
					_, err := o.Raw("UPDATE item_stock SET quantity=?, last_updated=?  WHERE _orgid=? and item_org_assoc_fkid=? and quantity<>0 ORDER BY  last_updated DESC LIMIT 1", s, time.Now(), formData.Orgid, formData.ItemName1).Exec()
					if err == nil {
						QtyUsed = 0
						//num = 0
					} else {
						//fmt.Println("Error in UPDATE when t_qty>SubSum")
						break
					}

				}
			}

		}
		if err01 != nil {
			//fmt.Println("Error in Item queries")
		}
	}
	if formData.ItemName2 != "" && formData.Quantity2 != "" {
		_, err02 := o.Raw("INSERT INTO so_items_used (_orgid,purchase_service_order_fkid,item_org_assoc_fkid,quantity_used,status) VALUES (?,?,?,?,?) ", formData.Orgid, maps[0]["_id"], formData.ItemName2, formData.Quantity2, "1").Exec()
		var maps0 []orm.Params
		num, err := o.Raw("SELECT quantity FROM item_stock WHERE _orgid=? and item_org_assoc_fkid=? and quantity<>0 ORDER BY last_updated ", formData.Orgid, formData.ItemName2).Values(&maps0)
		qty := formData.Quantity2
		QtyUsed, err := strconv.ParseFloat(qty, 64)
		if err == nil {
			for QtyUsed != 0 {
				j := maps0[num-1]["quantity"].(string)
				t_qty, _ := strconv.ParseFloat(j, 64)
				if t_qty <= QtyUsed {
					_, err := o.Raw("UPDATE item_stock SET quantity=?, last_updated=?  WHERE _orgid=? and item_org_assoc_fkid=? and quantity<>0 ORDER BY last_updated DESC LIMIT 1", "0", time.Now(), formData.Orgid, formData.ItemName2).Exec()
					if err == nil {
						QtyUsed = QtyUsed - t_qty
						num = num - 1
					} else {
						//fmt.Println("Error in UPDATE when t_qty<=SubSum")
						break
					}

				} else {
					var new_qty float64
					new_qty = t_qty - QtyUsed
					//fmt.Println("****new_qty: ", new_qty)
					s := strconv.FormatFloat(new_qty, 'E', -1, 64)
					_, err := o.Raw("UPDATE item_stock SET quantity=?, last_updated=?  WHERE _orgid=? and item_org_assoc_fkid=? and quantity<>0 ORDER BY  last_updated DESC LIMIT 1", s, time.Now(), formData.Orgid, formData.ItemName2).Exec()
					if err == nil {
						QtyUsed = 0
						//num = 0
					} else {
						//fmt.Println("Error in UPDATE when t_qty>SubSum")
						break
					}

				}
			}

		}

		if err02 != nil {
			//fmt.Println("Error in Item queries")
		}
	}

	if formData.ItemName3 != "" && formData.Quantity3 != "" {
		_, err03 := o.Raw("INSERT INTO so_items_used (_orgid,purchase_service_order_fkid,item_org_assoc_fkid,quantity_used,status) VALUES (?,?,?,?,?) ", formData.Orgid, maps[0]["_id"], formData.ItemName3, formData.Quantity3, "1").Exec()
		var maps0 []orm.Params
		num, err := o.Raw("SELECT quantity FROM item_stock WHERE _orgid=? and item_org_assoc_fkid=? and quantity<>0 ORDER BY last_updated ", formData.Orgid, formData.ItemName3).Values(&maps0)
		qty := formData.Quantity3
		QtyUsed, err := strconv.ParseFloat(qty, 64)
		if err == nil {
			for QtyUsed != 0 {
				j := maps0[num-1]["quantity"].(string)
				t_qty, _ := strconv.ParseFloat(j, 64)
				if t_qty <= QtyUsed {
					_, err := o.Raw("UPDATE item_stock SET quantity=?, last_updated=?  WHERE _orgid=? and item_org_assoc_fkid=? and quantity<>0 ORDER BY last_updated DESC LIMIT 1", "0", time.Now(), formData.Orgid, formData.ItemName3).Exec()
					if err == nil {
						QtyUsed = QtyUsed - t_qty
						num = num - 1
					} else {
						//fmt.Println("Error in UPDATE when t_qty<=SubSum")
						break
					}

				} else {
					var new_qty float64
					new_qty = t_qty - QtyUsed
					//fmt.Println("****new_qty: ", new_qty)
					s := strconv.FormatFloat(new_qty, 'E', -1, 64)
					_, err := o.Raw("UPDATE item_stock SET quantity=?, last_updated=?  WHERE _orgid=? and item_org_assoc_fkid=? and quantity<>0 ORDER BY  last_updated DESC LIMIT 1", s, time.Now(), formData.Orgid, formData.ItemName3).Exec()
					if err == nil {
						QtyUsed = 0
						//num = 0
					} else {
						//fmt.Println("Error in UPDATE when t_qty>SubSum")
						break
					}

				}
			}

		}

		if err03 != nil {
			//fmt.Println("Error in Item queries")
		}
	}

	if formData.ItemName4 != "" && formData.Quantity4 != "" {
		_, err04 := o.Raw("INSERT INTO so_items_used (_orgid,purchase_service_order_fkid,item_org_assoc_fkid,quantity_used,status) VALUES (?,?,?,?,?) ", formData.Orgid, maps[0]["_id"], formData.ItemName4, formData.Quantity4, "1").Exec()
		var maps0 []orm.Params
		num, err := o.Raw("SELECT quantity FROM item_stock WHERE _orgid=? and item_org_assoc_fkid=? and quantity<>0 ORDER BY last_updated ", formData.Orgid, formData.ItemName4).Values(&maps0)
		qty := formData.Quantity4
		QtyUsed, err := strconv.ParseFloat(qty, 64)
		if err == nil {
			for QtyUsed != 0 {
				j := maps0[num-1]["quantity"].(string)
				t_qty, _ := strconv.ParseFloat(j, 64)
				if t_qty <= QtyUsed {
					_, err := o.Raw("UPDATE item_stock SET quantity=?, last_updated=?  WHERE _orgid=? and item_org_assoc_fkid=? and quantity<>0 ORDER BY last_updated DESC LIMIT 1", "0", time.Now(), formData.Orgid, formData.ItemName4).Exec()
					if err == nil {
						QtyUsed = QtyUsed - t_qty
						num = num - 1
					} else {
						//fmt.Println("Error in UPDATE when t_qty<=SubSum")
						break
					}

				} else {
					var new_qty float64
					new_qty = t_qty - QtyUsed
					//fmt.Println("****new_qty: ", new_qty)
					s := strconv.FormatFloat(new_qty, 'E', -1, 64)
					_, err := o.Raw("UPDATE item_stock SET quantity=?, last_updated=?  WHERE _orgid=? and item_org_assoc_fkid=? and quantity<>0 ORDER BY  last_updated DESC LIMIT 1", s, time.Now(), formData.Orgid, formData.ItemName4).Exec()
					if err == nil {
						QtyUsed = 0
						//num = 0
					} else {
						//fmt.Println("Error in UPDATE when t_qty>SubSum")
						break
					}

				}
			}

		}

		if err04 != nil {
			//fmt.Println("Error in Item queries")
		}
	}

	if formData.ItemName5 != "" && formData.Quantity5 != "" {
		_, err05 := o.Raw("INSERT INTO so_items_used (_orgid,purchase_service_order_fkid,item_org_assoc_fkid,quantity_used,status) VALUES (?,?,?,?,?) ", formData.Orgid, maps[0]["_id"], formData.ItemName5, formData.Quantity5, "1").Exec()
		var maps0 []orm.Params
		num, err := o.Raw("SELECT quantity FROM item_stock WHERE _orgid=? and item_org_assoc_fkid=? and quantity<>0 ORDER BY last_updated ", formData.Orgid, formData.ItemName5).Values(&maps0)
		qty := formData.Quantity5
		QtyUsed, err := strconv.ParseFloat(qty, 64)
		if err == nil {
			for QtyUsed != 0 {
				j := maps0[num-1]["quantity"].(string)
				t_qty, _ := strconv.ParseFloat(j, 64)
				if t_qty <= QtyUsed {
					_, err := o.Raw("UPDATE item_stock SET quantity=?, last_updated=?  WHERE _orgid=? and item_org_assoc_fkid=? and quantity<>0 ORDER BY last_updated DESC LIMIT 1", "0", time.Now(), formData.Orgid, formData.ItemName5).Exec()
					if err == nil {
						QtyUsed = QtyUsed - t_qty
						num = num - 1
					} else {
						//fmt.Println("Error in UPDATE when t_qty<=SubSum")
						break
					}

				} else {
					var new_qty float64
					new_qty = t_qty - QtyUsed
					//fmt.Println("****new_qty: ", new_qty)
					s := strconv.FormatFloat(new_qty, 'E', -1, 64)
					_, err := o.Raw("UPDATE item_stock SET quantity=?, last_updated=?  WHERE _orgid=? and item_org_assoc_fkid=? and quantity<>0 ORDER BY  last_updated DESC LIMIT 1", s, time.Now(), formData.Orgid, formData.ItemName5).Exec()
					if err == nil {
						QtyUsed = 0
						//num = 0
					} else {
						//fmt.Println("Error in UPDATE when t_qty>SubSum")
						break
					}

				}
			}

		}

		if err05 != nil {
			//fmt.Println("Error in Item queries")
		}
	}

	if formData.ItemName6 != "" && formData.Quantity6 != "" {
		_, err06 := o.Raw("INSERT INTO so_items_used (_orgid,purchase_service_order_fkid,item_org_assoc_fkid,quantity_used,status) VALUES (?,?,?,?,?) ", formData.Orgid, maps[0]["_id"], formData.ItemName6, formData.Quantity6, "1").Exec()
		var maps0 []orm.Params
		num, err := o.Raw("SELECT quantity FROM item_stock WHERE _orgid=? and item_org_assoc_fkid=? and quantity<>0 ORDER BY last_updated ", formData.Orgid, formData.ItemName6).Values(&maps0)
		qty := formData.Quantity6
		QtyUsed, err := strconv.ParseFloat(qty, 64)
		if err == nil {
			for QtyUsed != 0 {
				j := maps0[num-1]["quantity"].(string)
				t_qty, _ := strconv.ParseFloat(j, 64)
				if t_qty <= QtyUsed {
					_, err := o.Raw("UPDATE item_stock SET quantity=?, last_updated=?  WHERE _orgid=? and item_org_assoc_fkid=? and quantity<>0 ORDER BY last_updated DESC LIMIT 1", "0", time.Now(), formData.Orgid, formData.ItemName6).Exec()
					if err == nil {
						QtyUsed = QtyUsed - t_qty
						num = num - 1
					} else {
						//fmt.Println("Error in UPDATE when t_qty<=SubSum")
						break
					}

				} else {
					var new_qty float64
					new_qty = t_qty - QtyUsed
					//fmt.Println("****new_qty: ", new_qty)
					s := strconv.FormatFloat(new_qty, 'E', -1, 64)
					_, err := o.Raw("UPDATE item_stock SET quantity=?, last_updated=?  WHERE _orgid=? and item_org_assoc_fkid=? and quantity<>0 ORDER BY  last_updated DESC LIMIT 1", s, time.Now(), formData.Orgid, formData.ItemName6).Exec()
					if err == nil {
						QtyUsed = 0
						//num = 0
					} else {
						//fmt.Println("Error in UPDATE when t_qty>SubSum")
						break
					}

				}
			}

		}

		if err06 != nil {
			//fmt.Println("Error in Item queries")
		}
	}
	var maps7 []orm.Params
	var maps8 []orm.Params
	_, err7 := o.Raw("SELECT _id FROM supplier_org_assoc WHERE _orgid=? and company_name=?", formData.Orgid, formData.Contractor).Values(&maps7)
	_, err8 := o.Raw("SELECT _id from purchase_service_order where _orgid=? and unique_serial_no=? and type=?", formData.Orgid, formData.ChallanNo, "1").Values(&maps8)
	var maps6 []orm.Params
	_, err6 := o.Raw("SELECT item_rates.rate FROM item_rates WHERE item_org_assoc_fkid=? and supplier_org_assoc_fkid=? and item_rates._orgid=? ORDER BY start_date DESC LIMIT 1 ", formData.RItem, maps7[0]["_id"], formData.Orgid).Values(&maps6)

	//_, err4 := o.Raw("SELECT _id from item_org_assoc where _orgid=? and _id=? ",formData.Orgid,formData.RItem).Values(&maps2)
	_, err5 := o.Raw("INSERT INTO po_so_items (_orgid, purchase_service_order_fkid, item_org_assoc_fkid,quantity, status,rate,total_cost) VALUES (?,?,?,?,?,?,?)", formData.Orgid, maps8[0]["_id"], formData.RItem, formData.RQuantity, "1", maps6[0]["rate"], formData.SFinalTotal).Exec()
	if err1 == nil && err2 == nil && err3 == nil && err5 == nil && err6 == nil && err7 == nil && err8 == nil {

		return 1
	}
	return 0
}

func GetRItems(formData Createso) map[string]orm.Params {

	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params

	num, err := o.Raw("SELECT item_org_assoc.unit_measure, item_org_assoc._id, invoice_narration, item_code_gen, item_unique_code, SUM(item_stock.quantity) as qavailable FROM item_org_assoc,item_stock WHERE item_org_assoc._orgid=? and item_stock._orgid=? and item_stock.latest = 1 and item_stock.item_org_assoc_fkid=item_org_assoc._id group by item_org_assoc._id", formData.Orgid, formData.Orgid).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		var i int64 = 0
		for ; i < num; i++ {
			//fmt.Println(maps[i])
		}
		for k, v := range maps {

			key := "io_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetPItems(formData Createpo) map[string]orm.Params {

	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params

	num, err := o.Raw("SELECT item_org_assoc._id, item_org_assoc.invoice_narration, item_org_assoc.item_unique_code, item_org_assoc.unit_measure FROM item_org_assoc WHERE item_org_assoc._orgid=? and status=1 ", formData.Orgid).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		// var i int64 = 0
		// for ; i < num; i++ {
		// 	if maps[i]["invoice_naration"] == nil {
		// 		maps[i]["invoice_naration"] = maps[i]["invoice_narration"]
		// 	}
		// 	//fmt.Println(maps[i])
		// }
		for k, v := range maps {

			key := "org_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetSupplierNames(formData Createpo) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params

	num, err := o.Raw("SELECT _id, company_name, supplier_code_gen, supplier_unique_code FROM supplier_org_assoc WHERE _orgid=?", formData.Orgid).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		var i int64 = 0
		for ; i < num; i++ {
			//fmt.Println(maps[i])
		}
		for k, v := range maps {

			key := "io_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetSupplierNamess(formData GRNData) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params

	num, err := o.Raw("SELECT _id, company_name, supplier_unique_code FROM supplier_org_assoc WHERE _orgid=?", formData.Orgid).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		var i int64 = 0
		for ; i < num; i++ {
			//fmt.Println(maps[i])
		}
		for k, v := range maps {

			key := "io_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetPOItems(purchase_id string, orgid string) (string, []string, []string, []string, []string, []string, []string, string, []string, []string, []string, []string, []string, []string, []string, []string, []string, []string, []string, []string, []string, []string, []string, []string, []string, []string, []string, []string, []string, []string, []string) {

	status := "false"
	var items []string
	var quantity []string
	var received_qty []string
	var rejected_qty []string
	var pending_qty []string
	var rate []string
	//var total_cost []string
	var gst_rate []string
	var packet_size []string

	var discount_rate []string
	var delivery_date []string
	var uom []string
	var discount_value []string
	var hsn_sac []string
	var taxable_amount []string
	var igst_rate []string
	var igst_amount []string
	var cgst_rate []string
	var cgst_amount []string
	var sgst_rate []string
	var sgst_amount []string
	var cess_rate []string
	var cess_amount []string
	var total_tax_cess []string
	var total_after_tax []string
	var freight []string
	var insurance []string
	var packaging_forwarding []string
	var others []string
	var total_other_charges []string

	var tax_regime string

	o := orm.NewOrm()
	var tax_maps []orm.Params
	count, err1 := o.Raw("SELECT tax_regime FROM purchase_service_order as pso WHERE pso._id=? and pso._orgid=?", purchase_id, orgid).Values(&tax_maps)
	if err1 == nil && count > 0 {
		if tax_maps[0]["tax_regime"] != nil {
			tax_regime = tax_maps[0]["tax_regime"].(string)
		} else {
			tax_regime = "1"
		}
	} else {
		tax_regime = "1"
	}

	var maps []orm.Params
	num, err := o.Raw("SELECT ioa.invoice_narration, psi.quantity, psi.received_quantity, psi.rejected_quantity, psi.uom, psi.hsn_sac, psi.rate, psi.discount_rate, psi.discount_value, psi.delivery_date as delivery_date, psi.taxable_amount, psi.gst_rate, psi.igst_rate, psi.igst_amount, psi.cgst_rate, psi.cgst_amount, psi.sgst_rate, psi.sgst_amount, psi.cess_rate, psi.cess_amount, psi.total_tax_cess, psi.total_after_tax, psi.freight, psi.insurance, psi.packaging_forwarding, psi.others, psi.total_other_charges, psi.packet_size FROM item_org_assoc as ioa, po_so_items as psi WHERE psi.status=1 and psi.purchase_service_order_fkid=? and psi._orgid=? and ioa._orgid=? and ioa._id=psi.item_org_assoc_fkid", purchase_id, orgid, orgid).Values(&maps)
	//fmt.Println("num=", num)
	var i int64 = 0
	for ; i < num; i++ {
		//fmt.Println(maps[i])
	}
	if err == nil {
		var new_items string
		var new_quantity string
		var new_received_qty string
		var new_rejected_qty string
		var new_pending_qty string
		var new_rate string
		//var new_total_cost string
		var new_gst_rate string
		var new_packet_size string

		var new_discount_rate string
		var new_delivery_date string
		var new_uom string

		var new_discount_value string
		var new_hsn_sac string
		var new_taxable_amount string
		var new_igst_rate string
		var new_igst_amount string
		var new_cgst_rate string
		var new_cgst_amount string
		var new_sgst_rate string
		var new_sgst_amount string
		var new_cess_rate string
		var new_cess_amount string
		var new_total_tax_cess string
		var new_total_after_tax string
		var new_freight string
		var new_insurance string
		var new_packaging_forwarding string
		var new_others string
		var new_total_other_charges string

		var j int64 = 0
		for ; j < num; j++ {
			new_items = maps[j]["invoice_narration"].(string)
			items = append(items, new_items)

			new_quantity = maps[j]["quantity"].(string)
			quantity = append(quantity, new_quantity)

			if maps[j]["received_quantity"] != nil {
				new_received_qty = maps[j]["received_quantity"].(string)
			} else {
				new_received_qty = "0.0"
			}
			received_qty = append(received_qty, new_received_qty)

			if maps[j]["rejected_quantity"] != nil {
				new_rejected_qty = maps[j]["rejected_quantity"].(string)
			} else {
				new_rejected_qty = "0.0"
			}
			rejected_qty = append(rejected_qty, new_rejected_qty)

			ordered, _ := strconv.ParseFloat(new_quantity, 64)
			delivered, _ := strconv.ParseFloat(new_received_qty, 64)
			pending := ordered - delivered
			new_pending_qty = strconv.FormatFloat(pending, 'f', 5, 64)
			pending_qty = append(pending_qty, new_pending_qty)

			new_rate = maps[j]["rate"].(string)
			rate = append(rate, new_rate)

			// new_total_cost = maps[j]["total_cost"].(string)
			// total_cost = append(total_cost, new_total_cost)

			if maps[j]["gst_rate"] != nil {
				new_gst_rate = maps[j]["gst_rate"].(string)
			} else {
				new_gst_rate = ""
			}
			gst_rate = append(gst_rate, new_gst_rate)

			if maps[j]["packet_size"] != nil {
				new_packet_size = maps[j]["packet_size"].(string)
			} else {
				new_packet_size = ""
			}
			packet_size = append(packet_size, new_packet_size)

			if maps[j]["discount_rate"] != nil {
				new_discount_rate = maps[j]["discount_rate"].(string)
			} else {
				new_discount_rate = ""
			}
			discount_rate = append(discount_rate, new_discount_rate)

			new_delivery_date = maps[j]["delivery_date"].(string)
			delivery_date = append(delivery_date, new_delivery_date)

			if maps[j]["uom"] != nil {
				new_uom = maps[j]["uom"].(string)
			} else {
				new_uom = ""
			}
			uom = append(uom, new_uom)

			if maps[j]["discount_value"] != nil {
				new_discount_value = maps[j]["discount_value"].(string)
			} else {
				new_discount_value = ""
			}
			discount_value = append(discount_value, new_discount_value)

			if maps[j]["hsn_sac"] != nil {
				new_hsn_sac = maps[j]["hsn_sac"].(string)

			} else {
				new_hsn_sac = ""
			}
			hsn_sac = append(hsn_sac, new_hsn_sac)

			if maps[j]["taxable_amount"] != nil {
				new_taxable_amount = maps[j]["taxable_amount"].(string)
			} else {
				new_taxable_amount = ""
			}
			taxable_amount = append(taxable_amount, new_taxable_amount)

			if maps[j]["igst_rate"] != nil {
				new_igst_rate = maps[j]["igst_rate"].(string)
			} else {
				new_igst_rate = ""
			}
			igst_rate = append(igst_rate, new_igst_rate)

			if maps[j]["igst_amount"] != nil {
				new_igst_amount = maps[j]["igst_amount"].(string)

			} else {
				new_igst_amount = ""
			}
			igst_amount = append(igst_amount, new_igst_amount)

			if maps[j]["cgst_rate"] != nil {
				new_cgst_rate = maps[j]["cgst_rate"].(string)

			} else {
				new_cgst_rate = ""
			}
			cgst_rate = append(cgst_rate, new_cgst_rate)

			if maps[j]["cgst_amount"] != nil {
				new_cgst_amount = maps[j]["cgst_amount"].(string)

			} else {
				new_cgst_amount = ""
			}
			cgst_amount = append(cgst_amount, new_cgst_amount)

			if maps[j]["sgst_rate"] != nil {
				new_sgst_rate = maps[j]["sgst_rate"].(string)

			} else {
				new_sgst_rate = ""
			}
			sgst_rate = append(sgst_rate, new_sgst_rate)

			if maps[j]["sgst_amount"] != nil {
				new_sgst_amount = maps[j]["sgst_amount"].(string)

			} else {
				new_sgst_amount = ""
			}
			sgst_amount = append(sgst_amount, new_sgst_amount)

			if maps[j]["cess_rate"] != nil {
				new_cess_rate = maps[j]["cess_rate"].(string)

			} else {
				new_cess_rate = ""
			}
			cess_rate = append(cess_rate, new_cess_rate)

			if maps[j]["cess_amount"] != nil {
				new_cess_amount = maps[j]["cess_amount"].(string)

			} else {
				new_cess_amount = ""
			}
			cess_amount = append(cess_amount, new_cess_amount)

			if maps[j]["total_tax_cess"] != nil {
				new_total_tax_cess = maps[j]["total_tax_cess"].(string)

			} else {
				new_total_tax_cess = ""
			}
			total_tax_cess = append(total_tax_cess, new_total_tax_cess)

			if maps[j]["total_after_tax"] != nil {
				new_total_after_tax = maps[j]["total_after_tax"].(string)

			} else {
				new_total_after_tax = ""
			}
			total_after_tax = append(total_after_tax, new_total_after_tax)

			if maps[j]["freight"] != nil {
				new_freight = maps[j]["freight"].(string)

			} else {
				new_freight = ""
			}
			freight = append(freight, new_freight)

			if maps[j]["insurance"] != nil {
				new_insurance = maps[j]["insurance"].(string)

			} else {
				new_insurance = ""
			}
			insurance = append(insurance, new_insurance)

			if maps[j]["packaging_forwarding"] != nil {
				new_packaging_forwarding = maps[j]["packaging_forwarding"].(string)

			} else {
				new_packaging_forwarding = ""
			}
			packaging_forwarding = append(packaging_forwarding, new_packaging_forwarding)

			if maps[j]["others"] != nil {
				new_others = maps[j]["others"].(string)

			} else {
				new_others = ""
			}
			others = append(others, new_others)

			if maps[j]["total_other_charges"] != nil {
				new_total_other_charges = maps[j]["total_other_charges"].(string)

			} else {
				new_total_other_charges = ""
			}
			total_other_charges = append(total_other_charges, new_total_other_charges)

		}
		status = "true"
	}
	// fmt.Println("***items=", items)
	// fmt.Println("***quantity=", quantity)
	return tax_regime, items, quantity, received_qty, rejected_qty, pending_qty, packet_size, status, rate, discount_rate, delivery_date, uom, discount_value, hsn_sac, taxable_amount, gst_rate, igst_rate, igst_amount, cgst_rate, cgst_amount, sgst_rate, sgst_amount, cess_rate, cess_amount, total_tax_cess, total_after_tax, freight, insurance, packaging_forwarding, others, total_other_charges
}

func GetPOItems2(product_id string, orgid string) ([]string, []string, []string, []string, []string, string, []string, []string) {

	status := "false"
	var items []string
	var quantity []string
	var received_qty []string
	var rejected_qty []string
	var pending_qty []string
	var rate []string
	var total_cost []string

	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT item_org_assoc.invoice_narration, po_so_items.quantity, po_so_items.received_quantity, po_so_items.rejected_quantity, po_so_items.rate,po_so_items.total_cost,po_so_items.packet_size FROM item_org_assoc, po_so_items WHERE purchase_service_order_fkid=? and po_so_items._orgid=? and item_org_assoc._orgid=? and item_org_assoc._id=po_so_items.item_org_assoc_fkid", product_id, orgid, orgid).Values(&maps)
	//fmt.Println("num=", num)
	var i int64 = 0
	for ; i < num; i++ {
		//fmt.Println(maps[i])
	}
	if err == nil {
		var new_items string
		var new_quantity string
		var new_received_qty string
		var new_rejected_qty string
		var new_pending_qty string
		var new_rate string
		var new_total_cost string

		var j int64 = 0
		for ; j < num; j++ {
			new_items = maps[j]["invoice_narration"].(string)
			items = append(items, new_items)

			new_quantity = maps[j]["quantity"].(string)
			quantity = append(quantity, new_quantity)

			if maps[j]["received_quantity"] != nil {
				new_received_qty = maps[j]["received_quantity"].(string)
			} else {
				new_received_qty = "0.0"
			}
			received_qty = append(received_qty, new_received_qty)

			if maps[j]["rejected_quantity"] != nil {
				new_rejected_qty = maps[j]["rejected_quantity"].(string)
			} else {
				new_rejected_qty = "0.0"
			}
			rejected_qty = append(rejected_qty, new_rejected_qty)

			ordered, _ := strconv.ParseFloat(new_quantity, 64)
			delivered, _ := strconv.ParseFloat(new_received_qty, 64)
			pending := ordered - delivered
			new_pending_qty = strconv.FormatFloat(pending, 'f', 5, 64)
			pending_qty = append(pending_qty, new_pending_qty)

			new_rate = maps[j]["rate"].(string)
			rate = append(rate, new_rate)

			new_total_cost = maps[j]["total_cost"].(string)
			total_cost = append(total_cost, new_total_cost)

		}
		status = "true"
	}
	// fmt.Println("***items=", items)
	// fmt.Println("***quantity=", quantity)
	return items, quantity, received_qty, rejected_qty, pending_qty, status, rate, total_cost
}

func GetSOItemsUsed(product_id string, orgid string) ([]string, []string, string) {

	status := "false"
	var items []string
	var quantity []string

	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT item_org_assoc.invoice_narration, so_items_used.quantity_used FROM item_org_assoc, so_items_used WHERE purchase_service_order_fkid=? and so_items_used._orgid=? and item_org_assoc._orgid=? and item_org_assoc._id=so_items_used.item_org_assoc_fkid", product_id, orgid, orgid).Values(&maps)
	//fmt.Println("num=", num)
	var i int64 = 0
	for ; i < num; i++ {
		//fmt.Println(maps[i])
	}
	if err == nil {
		var new_items string
		var new_quantity string

		var j int64 = 0
		for ; j < num; j++ {
			new_items = maps[j]["invoice_narration"].(string)
			items = append(items, new_items)

			new_quantity = maps[j]["quantity_used"].(string)
			quantity = append(quantity, new_quantity)

		}
		status = "true"
	}
	// fmt.Println("***items=", items)
	// fmt.Println("***quantity=", quantity)
	return items, quantity, status
}

func GetContractorNames(formData Createso) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params

	num, err := o.Raw("SELECT _id, company_name, supplier_code_gen, supplier_unique_code FROM supplier_org_assoc WHERE _orgid=?", formData.Orgid).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		var i int64 = 0
		for ; i < num; i++ {
			//fmt.Println(maps[i])
		}
		for k, v := range maps {

			key := "io_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetPORates(item_id string, supplier_id string, orgid string) (string, string, string, string, string, string) {

	status := "false"

	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT ioa.unit_measure, ioa.hsn_sac, ioa.gst_rate FROM item_org_assoc as ioa WHERE ioa._orgid=? and ioa._id=? and ioa.status=1 ", orgid, item_id).Values(&maps)
	//fmt.Println("***Num=", num)
	var new_hsn string
	var new_uom string
	var new_rate string
	var new_gst string
	var new_supplier_narration string

	if err == nil && num > 0 {

		var maps01 []orm.Params
		nos, err01 := o.Raw("SELECT ir.rate FROM item_rates as ir WHERE ir.item_org_assoc_fkid=? and ir.supplier_org_assoc_fkid=? and ir._orgid=? and ir.latest=1", item_id, supplier_id, orgid).Values(&maps01)
		if err01 == nil && nos > 0 {
			//fmt.Println("***Nos=", nos)
			if maps01[0]["rate"] != nil {
				new_rate = maps01[0]["rate"].(string)
			} else {
				new_rate = ""
			}
		} else {
			new_rate = ""
		}

		if maps[0]["unit_measure"] != nil {
			new_uom = maps[0]["unit_measure"].(string)
		} else {
			new_uom = ""
		}

		if maps[0]["hsn_sac"] != nil {
			new_hsn = maps[0]["hsn_sac"].(string)
		} else {
			new_hsn = ""
		}

		if maps[0]["gst_rate"] != nil {
			new_gst = maps[0]["gst_rate"].(string)
		} else {
			new_gst = ""
		}

		var maps1 []orm.Params
		count, err1 := o.Raw("SELECT osia.invoice_naration FROM org_supplier_item_assoc as osia WHERE osia.supplier_org_assoc_fkid=? and osia.item_org_assoc_fkid=? and osia._orgid=? and osia.status=1  ", supplier_id, item_id, orgid).Values(&maps1)

		if err1 == nil && count > 0 {
			//fmt.Println("***count=", count)
			if maps1[0]["invoice_naration"] != nil {
				new_supplier_narration = maps1[0]["invoice_naration"].(string)
			} else {
				new_supplier_narration = ""
			}
		} else {
			new_supplier_narration = ""
		}

		status = "true"
	}
	//fmt.Println("***", new_hsn, "***", new_uom, new_rate, new_gst, new_supplier_narration)
	return new_hsn, new_uom, new_rate, new_gst, new_supplier_narration, status
}

func GetLocation(formData Createpo) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT _id,factory_location,factory_type FROM organisation_factories WHERE _orgid=? and status=1", formData.Orgid).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println("********num :", num)
		var i int64 = 0
		for ; i < num; i++ {
			//fmt.Println(maps[i])
		}
		for k, v := range maps {

			key := "fac_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func GetDispatchLocation(formData Createso) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT _id,factory_location,factory_type FROM organisation_factories WHERE _orgid=? and status=1", formData.Orgid).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println("********num :", num)
		var i int64 = 0
		for ; i < num; i++ {
			//fmt.Println(maps[i])
		}
		for k, v := range maps {

			key := "fac_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func ChangeStatus(formData Createpo, product_status string) int {

	o := orm.NewOrm()
	//var maps []orm.Params
	_, err := o.Raw("Update purchase_service_order set status=? WHERE _id=? and purchase_service_order._orgid=? ", product_status, formData.POSONo, formData.Orgid).Exec()
	//	_, err1 := o.Raw("SELECT _id FROM purchase_service_order WHERE unique_serial_no=? and _orgid=? and status=?", formData.POSONo, formData.Orgid, product_status).Values(&maps)
	_, err2 := o.Raw("INSERT INTO po_so_status (_orgid, purchase_service_order_fkid, actedon_by_fkid, date_actedon, status) VALUES (?,?,?,?,?) ", formData.Orgid, formData.POSONo, formData.Userid, time.Now(), product_status).Exec()
	if err == nil && err2 == nil {
		return 1
	}
	return 0
}

func CreateSONew(formData Createso, status string) (int, string) {

	o := orm.NewOrm()
	ormTransactionalErr := o.Begin()

	var freighttype *string
	freighttype = nil
	if formData.DispatchFleet != "" {
		tempfreighttype := formData.DispatchFleet
		freighttype = &tempfreighttype
	}
	var freightcost *string
	freightcost = nil
	if formData.DispatchCost != "" {
		tempfreightcost := formData.DispatchCost
		freightcost = &tempfreightcost
	}
	var dissubtotal *string
	dissubtotal = nil
	if formData.SDiscountSubTotal != "" {
		tempdissubtotal := formData.SDiscountSubTotal
		dissubtotal = &tempdissubtotal
	}
	var paymentterms *string
	paymentterms = nil
	if formData.SPaymentTerms != "" {
		temppaymentterms := formData.SPaymentTerms
		paymentterms = &temppaymentterms
	}
	var chargestitle *string
	chargestitle = nil
	if formData.SOtherChargesTitle != "" {
		tempchargestitle := formData.SOtherChargesTitle
		chargestitle = &tempchargestitle
	}
	var chargestotal *string
	chargestotal = nil
	if formData.SOtherChargesValue != "" {
		tempchargestotal := formData.SOtherChargesValue
		chargestotal = &tempchargestotal
	}
	var excise *string
	excise = nil
	if formData.SExciseDuty != "" {
		tempexcise := formData.SExciseDuty
		excise = &tempexcise
	}
	var octoroichrg *string
	octoroichrg = nil
	if formData.SOctroiCharges != "" {
		tempoctoroichrg := formData.SOctroiCharges
		octoroichrg = &tempoctoroichrg
	}
	var finaldis *string
	finaldis = nil
	if formData.SFinalDiscount != "" {
		tempfinaldis := formData.SFinalDiscount
		finaldis = &tempfinaldis
	}
	var billlocation *string
	billlocation = nil
	if formData.SBillingAddress != "" {
		tempbilllocation := formData.SBillingAddress
		billlocation = &tempbilllocation
	}
	var dispatchlocation *string
	dispatchlocation = nil
	if formData.DispatchFrom != "" {
		tempdispatchlocation := formData.DispatchFrom
		dispatchlocation = &tempdispatchlocation
	}
	var itotal *string
	itotal = nil
	if formData.STotal != "" {
		tempitotal := formData.STotal
		itotal = &tempitotal
	}
	var maps []orm.Params
	//var maps2 []orm.Params
	_, err2 := o.Raw("SELECT _id FROM supplier_org_assoc WHERE _orgid=? and company_name=?", formData.Orgid, formData.Contractor).Values(&maps)
	if err2 != nil {
		ormTransactionalErr = o.Rollback()
		return 0, "failed"
	}
	_, err1 := o.Raw("INSERT INTO purchase_service_order (unique_serial_no, type, date, delivery_date, geo_location, discount_percentage, freight_type, freight_cost, payment_terms, other_charges, other_total, currency, supplier_fkid, excise_duty, octoroi, item_total, final_discount_percentage, final_total, billing_address,_orgid,instructions,status) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ", formData.ChallanNo, "1", formData.DateOrder, formData.EstimatedDeliveryDate, &dispatchlocation, &dissubtotal, &freighttype, &freightcost, &paymentterms, &chargestitle, &chargestotal, formData.SCurrency, maps[0]["_id"], &excise, &octoroichrg, &itotal, &finaldis, formData.SFinalTotal, &billlocation, formData.Orgid, formData.Purpose, status).Exec()
	if err1 != nil {
		ormTransactionalErr = o.Rollback()
		return 0, "failed"
	}
	_, err3 := o.Raw("SELECT _id from purchase_service_order where _orgid=? and unique_serial_no=? and type=?", formData.Orgid, formData.ChallanNo, "1").Values(&maps)
	if err3 != nil {
		ormTransactionalErr = o.Rollback()
		return 0, "failed"
	}
	psoId := maps[0]["_id"].(string)
	if formData.ItemName != "" && formData.Quantity != "" {
		_, err0 := o.Raw("INSERT INTO so_items_used (_orgid,purchase_service_order_fkid,item_org_assoc_fkid,quantity_used,status) VALUES (?,?,?,?,?) ", formData.Orgid, maps[0]["_id"], formData.ItemName, formData.Quantity, "1").Exec()

		if err0 != nil {
			//fmt.Println("Error in Item queries")
			ormTransactionalErr = o.Rollback()
			return 0, "failed"
		}
	}

	if formData.ItemName1 != "" && formData.Quantity1 != "" {
		_, err01 := o.Raw("INSERT INTO so_items_used (_orgid,purchase_service_order_fkid,item_org_assoc_fkid,quantity_used,status) VALUES (?,?,?,?,?) ", formData.Orgid, maps[0]["_id"], formData.ItemName1, formData.Quantity1, "1").Exec()

		if err01 != nil {
			//fmt.Println("Error in Item queries")
			ormTransactionalErr = o.Rollback()
			return 0, "failed"
		}
	}
	if formData.ItemName2 != "" && formData.Quantity2 != "" {
		_, err02 := o.Raw("INSERT INTO so_items_used (_orgid,purchase_service_order_fkid,item_org_assoc_fkid,quantity_used,status) VALUES (?,?,?,?,?) ", formData.Orgid, maps[0]["_id"], formData.ItemName2, formData.Quantity2, "1").Exec()

		if err02 != nil {
			//fmt.Println("Error in Item queries")
			ormTransactionalErr = o.Rollback()
			return 0, "failed"
		}
	}

	if formData.ItemName3 != "" && formData.Quantity3 != "" {
		_, err03 := o.Raw("INSERT INTO so_items_used (_orgid,purchase_service_order_fkid,item_org_assoc_fkid,quantity_used,status) VALUES (?,?,?,?,?) ", formData.Orgid, maps[0]["_id"], formData.ItemName3, formData.Quantity3, "1").Exec()
		if err03 != nil {
			//fmt.Println("Error in Item queries")
			ormTransactionalErr = o.Rollback()
			return 0, "failed"
		}
	}

	if formData.ItemName4 != "" && formData.Quantity4 != "" {
		_, err04 := o.Raw("INSERT INTO so_items_used (_orgid,purchase_service_order_fkid,item_org_assoc_fkid,quantity_used,status) VALUES (?,?,?,?,?) ", formData.Orgid, maps[0]["_id"], formData.ItemName4, formData.Quantity4, "1").Exec()

		if err04 != nil {
			//fmt.Println("Error in Item queries")
			ormTransactionalErr = o.Rollback()
			return 0, "failed"
		}
	}

	if formData.ItemName5 != "" && formData.Quantity5 != "" {
		_, err05 := o.Raw("INSERT INTO so_items_used (_orgid,purchase_service_order_fkid,item_org_assoc_fkid,quantity_used,status) VALUES (?,?,?,?,?) ", formData.Orgid, maps[0]["_id"], formData.ItemName5, formData.Quantity5, "1").Exec()

		if err05 != nil {
			//fmt.Println("Error in Item queries")
			ormTransactionalErr = o.Rollback()
			return 0, "failed"
		}
	}

	if formData.ItemName6 != "" && formData.Quantity6 != "" {
		_, err06 := o.Raw("INSERT INTO so_items_used (_orgid,purchase_service_order_fkid,item_org_assoc_fkid,quantity_used,status) VALUES (?,?,?,?,?) ", formData.Orgid, maps[0]["_id"], formData.ItemName6, formData.Quantity6, "1").Exec()

		if err06 != nil {
			//fmt.Println("Error in Item queries")
			ormTransactionalErr = o.Rollback()
			return 0, "failed"
		}
	}
	var maps7 []orm.Params
	var maps8 []orm.Params
	_, err7 := o.Raw("SELECT _id FROM supplier_org_assoc WHERE _orgid=? and company_name=?", formData.Orgid, formData.Contractor).Values(&maps7)
	if err7 != nil {
		ormTransactionalErr = o.Rollback()
		return 0, "failed"
	}
	_, err8 := o.Raw("SELECT _id from purchase_service_order where _orgid=? and unique_serial_no=? and type=?", formData.Orgid, formData.ChallanNo, "1").Values(&maps8)
	if err8 != nil {
		ormTransactionalErr = o.Rollback()
		return 0, "failed"
	}
	var maps6 []orm.Params
	_, err6 := o.Raw("SELECT item_rates.rate FROM item_rates WHERE item_org_assoc_fkid=? and supplier_org_assoc_fkid=? and item_rates._orgid=? ORDER BY start_date DESC LIMIT 1 ", formData.RItem, maps7[0]["_id"], formData.Orgid).Values(&maps6)
	if err6 != nil {
		ormTransactionalErr = o.Rollback()
		return 0, "failed"
	}
	//fmt.Println(maps6[0]["rate"])
	//_, err4 := o.Raw("SELECT _id from item_org_assoc where _orgid=? and _id=? ",formData.Orgid,formData.RItem).Values(&maps2)
	_, err5 := o.Raw("INSERT INTO po_so_items (_orgid, purchase_service_order_fkid, item_org_assoc_fkid,quantity, status,rate,total_cost) VALUES (?,?,?,?,?,?,?)", formData.Orgid, maps8[0]["_id"], formData.RItem, formData.RQuantity, "1", maps6[0]["rate"], formData.SFinalTotal).Exec()
	if err5 != nil {
		ormTransactionalErr = o.Rollback()
		return 0, "failed"
	}

	ormTransactionalErr = o.Commit()
	if ormTransactionalErr != nil {
		return 0, "failed"
	}
	return 1, psoId
}

func DelayedPO(orgid string) int {

	o := orm.NewOrm()
	//var maps []orm.Params
	_, err := o.Raw("Update purchase_service_order set delayed_status=1, date=? WHERE _orgid=? and status=2 and delivery_date<=?", time.Now(), orgid, time.Now()).Exec()
	//_, err1 := o.Raw("SELECT _id FROM purchase_service_order WHERE _orgid=? and status=2 and delivery_date>?", orgid, time.Now()).Values(&maps)
	//_, err2 := o.Raw("INSERT INTO po_so_status (_orgid, purchase_service_order_fkid, actedon_by_fkid, date_actedon, status) VALUES (?,?,?,?,?) ", formData.Orgid, maps[0]["_id"], formData.Userid, time.Now(), product_status).Exec()
	if err == nil {
		return 1
	}
	return 0
}

func GetPODetailsByCode(purchase_code string, orgID string) Createpo {
	o := orm.NewOrm()
	var maps []orm.Params
	var itemStruct = Createpo{}
	num, err := o.Raw("SELECT pso.purchase_order_code_gen, pso.tax_regime, pso.total_taxable_value, pso.total_tax_cess, pso.total_after_taxes, pso.total_freight_packaging_others, pso.total_invoice_value, pso.subtotal,pso.status, pso._id,pso.unique_serial_no,pso.type,pso.date,pso.currency,pso.supplier_fkid,pso.delivery_date,organisation_factories.factory_location,pso.instructions,pso.payment_terms,pso.billing_address,pso.discount_percentage,pso.excise_duty,pso.freight_type,pso.freight_cost,pso.octoroi,pso.item_total,pso.other_charges,pso.other_total,pso.billing_address,pso.final_discount_percentage,pso.final_total, pso.transporter_fkid, pso.vehicle_fkid,pso.transporter_warehouse_fkid, soa.company_name FROM purchase_service_order as pso, supplier_org_assoc as soa, organisation_factories WHERE soa._id=pso.supplier_fkid and pso._orgid=? and soa._orgid=? and organisation_factories._orgid=? and pso.geo_location=organisation_factories._id and pso._id=?", orgID, orgID, orgID, purchase_code).Values(&maps)
	if err != nil || num == 0 {
		itemStruct.IsPresentinDB = 0
	} else {
		if maps[0]["tax_regime"] == nil {
			itemStruct.TaxRegime = "1"
		} else {
			itemStruct.TaxRegime = maps[0]["tax_regime"].(string)
		}
		if maps[0]["total_taxable_value"] == nil {
			itemStruct.TotalTaxableValue = ""
		} else {
			itemStruct.TotalTaxableValue = maps[0]["total_taxable_value"].(string)
		}
		if maps[0]["total_tax_cess"] == nil {
			itemStruct.TotalTaxCess = ""
		} else {
			itemStruct.TotalTaxCess = maps[0]["total_tax_cess"].(string)
		}
		if maps[0]["total_after_taxes"] == nil {
			itemStruct.TotalAfterTaxes = ""
		} else {
			itemStruct.TotalAfterTaxes = maps[0]["total_after_taxes"].(string)
		}
		if maps[0]["total_freight_packaging_others"] == nil {
			itemStruct.TotalFreightPackaging = ""
		} else {
			itemStruct.TotalFreightPackaging = maps[0]["total_freight_packaging_others"].(string)
		}
		if maps[0]["total_invoice_value"] == nil {
			itemStruct.TotalInvoiceValue = ""
		} else {
			itemStruct.TotalInvoiceValue = maps[0]["total_invoice_value"].(string)
		}

		itemStruct.PurchaseId = maps[0]["_id"].(string)
		//itemStruct.POSONo = maps[0]["unique_serial_no"].(string)
		itemStruct.POSONo = maps[0]["purchase_order_code_gen"].(string)
		itemStruct.PODate = maps[0]["date"].(string)
		itemStruct.Supplier = maps[0]["company_name"].(string)
		itemStruct.PoDeliveryDate = maps[0]["delivery_date"].(string)
		if maps[0]["discount_percentage"] == nil {
			itemStruct.DiscountSubTotal = ""
		} else {
			itemStruct.DiscountSubTotal = maps[0]["discount_percentage"].(string)
		}

		itemStruct.Currency = maps[0]["currency"].(string)
		itemStruct.DLoc = maps[0]["factory_location"].(string)
		if maps[0]["freight_type"] == nil {
			itemStruct.FreightType = ""
		} else {
			itemStruct.FreightType = maps[0]["freight_type"].(string)
		}
		if maps[0]["freight_cost"] == nil {
			itemStruct.FreightCost = ""
		} else {
			itemStruct.FreightCost = maps[0]["freight_cost"].(string)
		}
		if maps[0]["payment_terms"] == nil {
			itemStruct.PaymentTerms = ""
		} else {
			itemStruct.PaymentTerms = maps[0]["payment_terms"].(string)
		}
		if maps[0]["other_charges"] == nil {
			itemStruct.OtherChargesTitle = ""
		} else {
			itemStruct.OtherChargesTitle = maps[0]["other_charges"].(string)
		}
		if maps[0]["other_total"] == nil {
			itemStruct.OtherChargesValue = ""
		} else {
			itemStruct.OtherChargesValue = maps[0]["other_total"].(string)
		}
		if maps[0]["excise_duty"] == nil {
			itemStruct.ExciseDuty = ""
		} else {
			itemStruct.ExciseDuty = maps[0]["excise_duty"].(string)
		}
		if maps[0]["octoroi"] == nil {
			itemStruct.OctroiCharges = ""
		} else {
			itemStruct.OctroiCharges = maps[0]["octoroi"].(string)
		}
		if maps[0]["item_total"] == nil {
			itemStruct.Total = ""
		} else {
			itemStruct.Total = maps[0]["item_total"].(string)
		}
		if maps[0]["final_discount_percentage"] == nil {
			itemStruct.FinalDiscount = ""
		} else {
			itemStruct.FinalDiscount = maps[0]["final_discount_percentage"].(string)
		}
		if maps[0]["final_total"] == nil {
			itemStruct.FinalTotal = ""
		} else {
			itemStruct.FinalTotal = maps[0]["final_total"].(string)
		}

		if maps[0]["billing_address"] == nil {
			itemStruct.BillingAddress = ""
		} else {
			itemStruct.BillingAddress = maps[0]["billing_address"].(string)
		}
		if maps[0]["subtotal"] == nil {
			itemStruct.TotalPrice = ""
		} else {
			itemStruct.TotalPrice = maps[0]["subtotal"].(string)
		}
		if maps[0]["transporter_fkid"] == nil {
			itemStruct.Transporter = ""
		} else {
			itemStruct.Transporter = maps[0]["transporter_fkid"].(string)
		}
		if maps[0]["vehicle_fkid"] == nil {
			itemStruct.Vehicle = ""
		} else {
			itemStruct.Vehicle = maps[0]["vehicle_fkid"].(string)
		}
		if maps[0]["transporter_warehouse_fkid"] == nil {
			itemStruct.TransporterWarehouse = ""
		} else {
			itemStruct.TransporterWarehouse = maps[0]["transporter_warehouse_fkid"].(string)
		}

		// var transporterMaps []orm.Params
		// transporterNum, transporterErr := o.Raw("SELECT * FROM po_so_items WHERE _orgid=? and purchase_service_order_fkid=? and status=1", orgID, purchase_code).Values(&transporterMaps)
		// if transporterNum > 0 && transporterErr == nil {
		// 	if transporterMaps[0]["transporter_fkid"] == nil {
		// 		itemStruct.Transporter = ""
		// 	} else {
		// 		itemStruct.Transporter = transporterMaps[0]["transporter_fkid"].(string)
		// 	}
		// 	if transporterMaps[0]["vehicle_fkid"] == nil {
		// 		itemStruct.Vehicle = ""
		// 	} else {
		// 		itemStruct.Vehicle = transporterMaps[0]["vehicle_fkid"].(string)
		// 	}
		// } else {
		// 	itemStruct.Transporter = ""
		// 	itemStruct.Vehicle = ""
		// }

		itemStruct.IsPresentinDB = 1
	}
	return itemStruct
}

func GetEditPOItems(code string, orgid string) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params

	num, err := o.Raw("SELECT psi._id, psi.uom, psi.hsn_sac, psi.rate, psi.discount_rate, psi.discount_value, psi.taxable_amount, psi.gst_rate, psi.igst_rate, psi.igst_amount, psi.cgst_rate, psi.cgst_amount, psi.sgst_rate, psi.sgst_amount, psi.cess_rate, psi.cess_amount, psi.total_tax_cess, psi.total_after_tax, psi.freight, psi.insurance, psi.packaging_forwarding, psi.others, psi.total_other_charges, psi.quantity, psi.received_quantity, psi.packet_size, psi.total_cost, ioa._id as item_id, ioa.invoice_narration FROM item_org_assoc as ioa, po_so_items as psi, purchase_service_order WHERE ioa._orgid=? and psi._orgid=? and psi.status=1 and purchase_service_order._orgid=? and purchase_service_order._id=psi.purchase_service_order_fkid and psi.item_org_assoc_fkid=ioa._id and purchase_service_order._id=?", orgid, orgid, orgid, code).Values(&maps)
	if err == nil && num > 0 {
		//fmt.Println(maps[0]["name"])
		var i int64 = 0
		for ; i < num; i++ {
			//fmt.Println(maps[i])
		}
		for k, v := range maps {
			maps[k]["no"] = k
			if maps[k]["received_quantity"] == nil || maps[k]["received_quantity"] == "" {
				maps[k]["editable"] = ""
			} else {
				recievedQty, _ := strconv.ParseFloat(maps[k]["received_quantity"].(string), 64)
				if recievedQty == 0 {
					maps[k]["editable"] = ""
				} else {
					maps[k]["editable"] = "readonly"
				}
			}

			key := "io_" + strconv.Itoa(k)
			// if v == nil {
			// 	result[key] = "".(orm.Params)
			// }
			result[key] = v
		}

	} else {
		//fmt.Println(err)
		result["status"] = nil
	}
	return result
}

func UpdatePO(formData Createpo, poitems []map[string]string) int {
	o := orm.NewOrm()
	ormTransactionalErr := o.Begin()

	var freightpackagingothers string
	freightpackagingothers = "0.00"
	if formData.TotalFreightPackaging != "" {
		tempTotalFreightPackaging := formData.TotalFreightPackaging
		freightpackagingothers = tempTotalFreightPackaging
	}

	// var paymentterms *string
	// paymentterms = nil
	// if formData.PaymentTerms != "" {
	// 	temppaymentterms := formData.PaymentTerms
	// 	paymentterms = &temppaymentterms
	// }

	var billlocation *string
	billlocation = nil
	if formData.BillingAddress != "" {
		tempbilllocation := formData.BillingAddress
		billlocation = &tempbilllocation
	}

	var transporter *string
	transporter = nil
	if formData.Transporter != "" {
		temptransporter := formData.Transporter
		transporter = &temptransporter
	}

	var vehicle *string
	vehicle = nil
	if formData.Vehicle != "" {
		tempvehicle := formData.Vehicle
		vehicle = &tempvehicle
	}

	var warehouse *string
	warehouse = nil
	if formData.TransporterWarehouse != "" {
		tempwarehouse := formData.TransporterWarehouse
		warehouse = &tempwarehouse
	}

	_, err := o.Raw("UPDATE purchase_service_order SET billing_address=?, delivery_date=?, total_taxable_value=?, total_tax_cess=?, total_freight_packaging_others=?, total_after_taxes=?, total_invoice_value=?, transporter_fkid=?, vehicle_fkid=?,transporter_warehouse_fkid=?  WHERE _orgid=? and _id=?", &billlocation, formData.PoDeliveryDate, formData.TotalTaxableValue, formData.TotalTaxCess, freightpackagingothers, formData.TotalAfterTaxes, formData.TotalInvoiceValue, &transporter, &vehicle, &warehouse, formData.Orgid, formData.PurchaseId).Exec()
	if err != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}

	//Remove deleted items
	var allItemsMaps []orm.Params
	_, allItemsErr := o.Raw("SELECT _id, item_org_assoc_fkid FROM po_so_items as poi WHERE poi._orgid=? and poi.purchase_service_order_fkid=? and status=1", formData.Orgid, formData.PurchaseId).Values(&allItemsMaps)
	if allItemsErr != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}
	for k, _ := range allItemsMaps {
		itemPresentFlag := 0
		for _, it := range poitems {
			if allItemsMaps[k]["item_org_assoc_fkid"] == it["item_name"] {
				itemPresentFlag = 1
				break
			}
		}
		if itemPresentFlag == 0 {
			_, removeErr := o.Raw("UPDATE po_so_items SET status=0 WHERE _orgid=? and purchase_service_order_fkid=? and item_org_assoc_fkid=? and status=1", formData.Orgid, formData.PurchaseId, allItemsMaps[k]["item_org_assoc_fkid"]).Exec()
			if removeErr != nil {
				ormTransactionalErr = o.Rollback()
				return 0
			}
		}
	}

	for _, it := range poitems {

		if it["item_name"] != "" {

			// _, err1 := o.Raw("UPDATE po_so_items SET quantity=?, hsn_sac=?, rate=?, discount_rate=?, discount_value=?, delivery_date=?, taxable_amount=?, gst_rate=?, igst_rate=?, igst_amount=?, cgst_rate=?, cgst_amount=?, sgst_rate=?, sgst_amount=?, cess_rate=?, cess_amount=?, total_tax_cess=?, total_after_tax=?, freight=?, insurance=?, packaging_forwarding=?, others=?, total_other_charges=?, total_cost=?, transporter_fkid=?, vehicle_fkid=? WHERE _orgid=? and purchase_service_order_fkid=? and item_org_assoc_fkid=? and status=1", it["qty"], it["hsn"], it["cost"], discountrate, discountamount, formData.PoDeliveryDate, it["taxable_amount"], gstrate, igstrate, it["igst_amount"], cgstrate, it["cgst_amount"], sgstrate, it["sgst_amount"], cessrate, it["cess_amount"], it["total_tax_cess"], it["total_after_tax"], freight, insurance, packagingforwarding, others, totalothercharges, formData.TotalInvoiceValue, &transporter, &vehicle, formData.Orgid, formData.PurchaseId, it["item_name"]).Exec()
			// if err1 != nil {
			// 	ormTransactionalErr = o.Rollback()
			// 	return 0
			// }
			var psgmaps []orm.Params
			psgCount, psgErr := o.Raw("SELECT * FROM po_so_items_with_grn as psg WHERE psg._orgid=? and psg.purchase_service_order_fkid=? and psg.item_org_assoc_fkid=? and psg.status=1", formData.Orgid, formData.PurchaseId, it["item_name"]).Values(&psgmaps)
			if psgErr != nil {
				ormTransactionalErr = o.Rollback()
				return 0
			} else if psgCount > 0 {
				// Part of the order is delivered, Do not edit
			} else {
				_, err0 := o.Raw("UPDATE po_so_items SET status=0 WHERE _orgid=? and purchase_service_order_fkid=? and item_org_assoc_fkid=? and status=1", formData.Orgid, formData.PurchaseId, it["item_name"]).Exec()
				if err0 != nil {
					ormTransactionalErr = o.Rollback()
					return 0
				}

				var hsn *string
				hsn = nil
				if it["hsn"] != "" {
					tempHSN := it["hsn"]
					hsn = &tempHSN
				}

				var received_quantity *string
				received_quantity = nil
				if it["received_quantity"] != "" {
					tempreceived_quantity := it["received_quantity"]
					received_quantity = &tempreceived_quantity
				}

				var rejected_quantity *string
				rejected_quantity = nil
				if it["rejected_quantity"] != "" {
					temprejected_quantity := it["rejected_quantity"]
					rejected_quantity = &temprejected_quantity
				}

				var discountrate string
				discountrate = "0"
				if it["discount_percent"] != "" {
					tempDiscountPercent := it["discount_percent"]
					discountrate = tempDiscountPercent
				}

				var discountamount string
				discountamount = "0.00"
				if it["discount_value"] != "" {
					tempDiscountAmount := it["discount_value"]
					discountamount = tempDiscountAmount
				}

				var gstrate string
				gstrate = "0"
				if it["gst_percent"] != "" {
					tempGSTPercent := it["gst_percent"]
					gstrate = tempGSTPercent
				}

				var igstrate string
				igstrate = "0"
				if it["igst_rate"] != "" {
					tempIGSTRate := it["igst_rate"]
					igstrate = tempIGSTRate
				}

				var cgstrate string
				cgstrate = "0"
				if it["cgst_rate"] != "" {
					tempCGSTRate := it["cgst_rate"]
					cgstrate = tempCGSTRate
				}

				var sgstrate string
				sgstrate = "0"
				if it["sgst_rate"] != "" {
					tempSGSTRate := it["sgst_rate"]
					sgstrate = tempSGSTRate
				}

				var cessrate string
				cessrate = "0"
				if it["cess_rate"] != "" {
					tempCessRate := it["cess_rate"]
					cessrate = tempCessRate
				}

				// var freight string
				// freight = "0"
				// if it["freight"] != "" {
				// 	tempFreight := it["freight"]
				// 	freight = tempFreight
				// }

				// var insurance string
				// insurance = "0"
				// if it["insurance"] != "" {
				// 	tempInsurance := it["insurance"]
				// 	insurance = tempInsurance
				// }

				// var packagingforwarding string
				// packagingforwarding = "0"
				// if it["packaging_forwarding"] != "" {
				// 	tempPackagingForwarding := it["packaging_forwarding"]
				// 	packagingforwarding = tempPackagingForwarding
				// }

				// var others string
				// others = "0"
				// if it["others"] != "" {
				// 	tempOthers := it["others"]
				// 	others = tempOthers
				// }

				var totalothercharges string
				totalothercharges = "0.00"
				if it["total_other_charges"] != "" {
					tempTotalOtherCharges := it["total_other_charges"]
					totalothercharges = tempTotalOtherCharges
				}

				_, err4 := o.Raw("INSERT INTO po_so_items (purchase_service_order_fkid, item_org_assoc_fkid, quantity, received_quantity, rejected_quantity, uom, hsn_sac, rate, discount_rate, discount_value, delivery_date, taxable_amount, gst_rate, igst_rate, igst_amount, cgst_rate, cgst_amount, sgst_rate, sgst_amount, cess_rate, cess_amount, total_tax_cess, total_after_tax, total_other_charges, total_cost, _orgid, status) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ", formData.PurchaseId, it["item_name"], it["qty"], &received_quantity, &rejected_quantity, it["uom"], &hsn, it["cost"], discountrate, discountamount, formData.PoDeliveryDate, it["taxable_amount"], gstrate, igstrate, it["igst_amount"], cgstrate, it["cgst_amount"], sgstrate, it["sgst_amount"], cessrate, it["cess_amount"], it["total_tax_cess"], it["total_after_tax"], totalothercharges, formData.TotalInvoiceValue, formData.Orgid, "1").Exec()
				if err4 != nil {
					ormTransactionalErr = o.Rollback()
					return 0
				}

				// if Status == "2" {
				// 	var maps05 []orm.Params
				// 	count, err05 := o.Raw("SELECT _id FROM item_rates WHERE _orgid=? and item_org_assoc_fkid=? and supplier_org_assoc_fkid=?", formData.Orgid, it["item_name"], formData.Supplier).Values(&maps05)
				// 	if err05 != nil {
				// 		ormTransactionalErr = o.Rollback()
				// 		return 0
				// 	}
				// 	if count > 0 {
				// 		_, err005 := o.Raw("UPDATE item_rates SET latest = 0 WHERE _orgid=? and item_org_assoc_fkid=? and supplier_org_assoc_fkid=?", formData.Orgid, it["item_name"], formData.Supplier).Exec()
				// 		if err005 != nil {
				// 			ormTransactionalErr = o.Rollback()
				// 			return 0
				// 		}
				// 	}
				// 	_, err5 := o.Raw("INSERT INTO item_rates (item_org_assoc_fkid, supplier_org_assoc_fkid, rate, currency, start_date, latest, _orgid) VALUES (?,?,?,?,?,?,?) ", it["item_name"], formData.Supplier, it["cost"], formData.Currency, time.Now(), "1", formData.Orgid).Exec()
				// 	if err5 != nil {
				// 		ormTransactionalErr = o.Rollback()
				// 		return 0
				// 	}
				// }
			}
		}
	}

	if ormTransactionalErr != nil {
		return 0
	}
	ormTransactionalErr = o.Commit()
	return 1
}

func UpdatePOEditItems(formData Createpo) int {
	o := orm.NewOrm()

	var hsn *string
	hsn = nil
	if formData.HSN != "" {
		tempHSN := formData.HSN
		hsn = &tempHSN
	}

	var discountrate string
	discountrate = "0"
	if formData.DiscountPercent != "" {
		tempDiscountPercent := formData.DiscountPercent
		discountrate = tempDiscountPercent
	}

	var discountamount string
	discountamount = "0.00"
	if formData.DiscountValue != "" {
		tempDiscountAmount := formData.DiscountValue
		discountamount = tempDiscountAmount
	}

	var gstrate string
	gstrate = "0"
	if formData.GSTPercent != "" {
		tempGSTPercent := formData.GSTPercent
		gstrate = tempGSTPercent
	}

	var cessrate string
	cessrate = "0"
	if formData.CessRate != "" {
		tempCessRate := formData.CessRate
		cessrate = tempCessRate
	}

	// var freight string
	// freight = "0"
	// if formData.Freight != "" {
	// 	tempFreight := formData.Freight
	// 	freight = tempFreight
	// }

	// var insurance string
	// insurance = "0"
	// if formData.Insurance != "" {
	// 	tempInsurance := formData.Insurance
	// 	insurance = tempInsurance
	// }

	// var packagingforwarding string
	// packagingforwarding = "0"
	// if formData.PackagingForwarding != "" {
	// 	tempPackagingForwarding := formData.PackagingForwarding
	// 	packagingforwarding = tempPackagingForwarding
	// }

	// var others string
	// others = "0"
	// if formData.Others != "" {
	// 	tempOthers := formData.Others
	// 	others = tempOthers
	// }

	var totalothercharges string
	totalothercharges = "0.00"
	if formData.TotalOtherCharges != "" {
		tempTotalOtherCharges := formData.TotalOtherCharges
		totalothercharges = tempTotalOtherCharges
	}
	_, err := o.Raw("UPDATE po_so_items SET quantity=?, hsn_sac=?, rate=?, discount_rate=?, discount_value=?, delivery_date=?, taxable_amount=?, gst_rate=?, igst_rate=?, igst_amount=?, cgst_rate=?, cgst_amount=?, sgst_rate=?, sgst_amount=?, cess_rate=?, cess_amount=?, total_tax_cess=?, total_after_tax=?, total_other_charges=? WHERE _orgid=? and purchase_service_order_fkid=? and item_org_assoc_fkid=? and status=1", formData.Quantity, &hsn, formData.UnitPrice, discountrate, discountamount, formData.PoDeliveryDate, formData.TaxableAmount, gstrate, formData.IGSTRate, formData.IGSTAmount, formData.CGSTRate, formData.CGSTAmount, formData.SGSTRate, formData.SGSTAmount, cessrate, formData.CessAmount, formData.ItemTotalTaxCess, formData.ItemTotalAfterTax, totalothercharges, formData.Orgid, formData.PurchaseId, formData.ItemName).Exec()

	if err == nil {
		return 1
	}
	return 0
}

func UpdateDraftPO(formData Createpo, poitems []map[string]string) int {
	o := orm.NewOrm()
	ormTransactionalErr := o.Begin()

	var freightpackagingothers string
	freightpackagingothers = "0.00"
	if formData.TotalFreightPackaging != "" {
		tempTotalFreightPackaging := formData.TotalFreightPackaging
		freightpackagingothers = tempTotalFreightPackaging
	}

	// var paymentterms *string
	// paymentterms = nil
	// if formData.PaymentTerms != "" {
	// 	temppaymentterms := formData.PaymentTerms
	// 	paymentterms = &temppaymentterms
	// }

	var billlocation *string
	billlocation = nil
	if formData.BillingAddress != "" {
		tempbilllocation := formData.BillingAddress
		billlocation = &tempbilllocation
	}

	var transporter *string
	transporter = nil
	if formData.Transporter != "" {
		temptransporter := formData.Transporter
		transporter = &temptransporter
	}

	var vehicle *string
	vehicle = nil
	if formData.Vehicle != "" {
		tempvehicle := formData.Vehicle
		vehicle = &tempvehicle
	}

	_, err := o.Raw("UPDATE purchase_service_order SET billing_address=?, delivery_date=?, total_taxable_value=?, total_tax_cess=?, total_freight_packaging_others=?, total_after_taxes=?, total_invoice_value=?, transporter_fkid=?, vehicle_fkid=? WHERE _orgid=? and _id=?", &billlocation, formData.PoDeliveryDate, formData.TotalTaxableValue, formData.TotalTaxCess, freightpackagingothers, formData.TotalAfterTaxes, formData.TotalInvoiceValue, &transporter, &vehicle, formData.Orgid, formData.PurchaseId).Exec()
	if err != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}

	_, err01 := o.Raw("UPDATE po_so_items SET status=? WHERE status=1 and _orgid=? and purchase_service_order_fkid=? ", 0, formData.Orgid, formData.PurchaseId).Exec()
	if err01 != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}

	for _, it := range poitems {
		var discountrate string
		discountrate = "0"
		if it["discount_percent"] != "" {
			tempDiscountPercent := it["discount_percent"]
			discountrate = tempDiscountPercent
		}

		var discountamount string
		discountamount = "0.00"
		if it["discount_value"] != "" {
			tempDiscountAmount := it["discount_value"]
			discountamount = tempDiscountAmount
		}

		var gstrate string
		gstrate = "0"
		if it["gst_percent"] != "" {
			tempGSTPercent := it["gst_percent"]
			gstrate = tempGSTPercent
		}

		var igstrate string
		igstrate = "0"
		if it["igst_rate"] != "" {
			tempIGSTRate := it["igst_rate"]
			igstrate = tempIGSTRate
		}

		var cgstrate string
		cgstrate = "0"
		if it["cgst_rate"] != "" {
			tempCGSTRate := it["cgst_rate"]
			cgstrate = tempCGSTRate
		}

		var sgstrate string
		sgstrate = "0"
		if it["sgst_rate"] != "" {
			tempSGSTRate := it["sgst_rate"]
			sgstrate = tempSGSTRate
		}

		var cessrate string
		cessrate = "0"
		if it["cess_rate"] != "" {
			tempCessRate := it["cess_rate"]
			cessrate = tempCessRate
		}

		// var freight string
		// freight = "0"
		// if it["freight"] != "" {
		// 	tempFreight := it["freight"]
		// 	freight = tempFreight
		// }

		// var insurance string
		// insurance = "0"
		// if it["insurance"] != "" {
		// 	tempInsurance := it["insurance"]
		// 	insurance = tempInsurance
		// }

		// var packagingforwarding string
		// packagingforwarding = "0"
		// if it["packaging_forwarding"] != "" {
		// 	tempPackagingForwarding := it["packaging_forwarding"]
		// 	packagingforwarding = tempPackagingForwarding
		// }

		// var others string
		// others = "0"
		// if it["others"] != "" {
		// 	tempOthers := it["others"]
		// 	others = tempOthers
		// }

		var totalothercharges string
		totalothercharges = "0.00"
		if it["total_other_charges"] != "" {
			tempTotalOtherCharges := it["total_other_charges"]
			totalothercharges = tempTotalOtherCharges
		}

		if it["item_name"] != "" {
			var maps001 []orm.Params
			num, err001 := o.Raw("SELECT _id FROM po_so_items WHERE _orgid=? and purchase_service_order_fkid=? and item_org_assoc_fkid=? and status=0", formData.Orgid, formData.PurchaseId, it["item_name"]).Values(&maps001)
			if err001 == nil && num > 0 {
				_, err2 := o.Raw("UPDATE po_so_items SET quantity=?, hsn_sac=?, rate=?, discount_rate=?, discount_value=?, delivery_date=?, taxable_amount=?, gst_rate=?, igst_rate=?, igst_amount=?, cgst_rate=?, cgst_amount=?, sgst_rate=?, sgst_amount=?, cess_rate=?, cess_amount=?, total_tax_cess=?, total_after_tax=?, total_other_charges=?, total_cost=?, status=? WHERE _orgid=? and purchase_service_order_fkid=? and item_org_assoc_fkid=? and status=?", it["qty"], it["hsn"], it["cost"], discountrate, discountamount, formData.PoDeliveryDate, it["taxable_amount"], gstrate, igstrate, it["igst_amount"], cgstrate, it["cgst_amount"], sgstrate, it["sgst_amount"], cessrate, it["cess_amount"], it["total_tax_cess"], it["total_after_tax"], totalothercharges, formData.TotalInvoiceValue, 1, formData.Orgid, formData.PurchaseId, it["item_name"], 0).Exec()
				if err2 != nil {
					ormTransactionalErr = o.Rollback()
					return 0
				}
			} else {
				_, err3 := o.Raw("INSERT INTO po_so_items (purchase_service_order_fkid, item_org_assoc_fkid, quantity, uom, hsn_sac, rate, discount_rate, discount_value, delivery_date, taxable_amount, gst_rate, igst_rate, igst_amount, cgst_rate, cgst_amount, sgst_rate, sgst_amount, cess_rate, cess_amount, total_tax_cess, total_after_tax, total_other_charges, total_cost, _orgid, status) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", formData.PurchaseId, it["item_name"], it["qty"], it["uom"], it["hsn"], it["cost"], discountrate, discountamount, formData.PoDeliveryDate, it["taxable_amount"], gstrate, igstrate, it["igst_amount"], cgstrate, it["cgst_amount"], sgstrate, it["sgst_amount"], cessrate, it["cess_amount"], it["total_tax_cess"], it["total_after_tax"], totalothercharges, formData.TotalInvoiceValue, formData.Orgid, "1").Exec()
				if err3 != nil {
					ormTransactionalErr = o.Rollback()
					return 0
				}
			}
		}
	}

	if ormTransactionalErr != nil {
		return 0
	}
	ormTransactionalErr = o.Commit()
	return 1
}

func SearchPOByOrderDetails(CustomerName, FromOrderDate, ToOrderDate, _orgId string) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	var err error
	var num int64
	if (ToOrderDate == "") && (FromOrderDate != "") {
		num, err = o.Raw("SELECT * FROM (SELECT pso.status, pso._id, pso.unique_serial_no, pso.type, pso.date as p_date, pso.currency, pso.supplier_fkid, pso.delivery_date as d_date, organisation_factories.factory_location, pso.payment_terms, pso.billing_address, pso.total_taxable_value, pso.total_tax_cess, pso.total_freight_packaging_others, pso.total_after_taxes, pso.total_invoice_value,soa.company_name, soa._orgid FROM purchase_service_order as pso, supplier_org_assoc as soa, organisation_factories WHERE soa._id=pso.supplier_fkid and (type=0 or type=1) and pso._orgid=? and soa._orgid=? and organisation_factories._orgid=? and pso.geo_location=organisation_factories._id ) AS pod WHERE pod._orgid=? and (pod.company_name like ? and p_date >= ?)", _orgId, _orgId, _orgId, _orgId, "%"+CustomerName+"%", FromOrderDate).Values(&maps)
	}
	if (FromOrderDate == "") && (ToOrderDate != "") {
		num, err = o.Raw("SELECT * FROM (SELECT pso.status, pso._id, pso.unique_serial_no, pso.type, pso.date as p_date, pso.currency, pso.supplier_fkid, pso.delivery_date as d_date, organisation_factories.factory_location, pso.payment_terms, pso.billing_address, pso.total_taxable_value, pso.total_tax_cess, pso.total_freight_packaging_others, pso.total_after_taxes, pso.total_invoice_value,soa.company_name, soa._orgid FROM purchase_service_order as pso, supplier_org_assoc as soa, organisation_factories WHERE soa._id=pso.supplier_fkid and (type=0 or type=1) and pso._orgid=? and soa._orgid=? and organisation_factories._orgid=? and pso.geo_location=organisation_factories._id ) AS pod WHERE pod._orgid=? and (pod.company_name like ? and p_date <= ?)", _orgId, _orgId, _orgId, _orgId, "%"+CustomerName+"%", ToOrderDate).Values(&maps)
	}
	if (FromOrderDate != "") && (ToOrderDate != "") {
		num, err = o.Raw("SELECT * FROM (SELECT pso.status, pso._id, pso.unique_serial_no, pso.type, pso.date as p_date, pso.currency, pso.supplier_fkid, pso.delivery_date as d_date, organisation_factories.factory_location, pso.payment_terms, pso.billing_address, pso.total_taxable_value, pso.total_tax_cess, pso.total_freight_packaging_others, pso.total_after_taxes, pso.total_invoice_value,soa.company_name, soa._orgid FROM purchase_service_order as pso, supplier_org_assoc as soa, organisation_factories WHERE soa._id=pso.supplier_fkid and (type=0 or type=1) and pso._orgid=? and soa._orgid=? and organisation_factories._orgid=? and pso.geo_location=organisation_factories._id ) AS pod WHERE pod._orgid=? and (pod.company_name like ? and (p_date between ? and ?))", _orgId, _orgId, _orgId, _orgId, "%"+CustomerName+"%", FromOrderDate, ToOrderDate).Values(&maps)
	}
	if (FromOrderDate == "") && (ToOrderDate == "") {
		num, err = o.Raw("SELECT * FROM (SELECT pso.status, pso._id, pso.unique_serial_no, pso.type, pso.date as p_date, pso.currency, pso.supplier_fkid, pso.delivery_date as d_date, organisation_factories.factory_location, pso.payment_terms, pso.billing_address, pso.total_taxable_value, pso.total_tax_cess, pso.total_freight_packaging_others, pso.total_after_taxes, pso.total_invoice_value,soa.company_name, soa._orgid FROM purchase_service_order as pso, supplier_org_assoc as soa, organisation_factories WHERE soa._id=pso.supplier_fkid and (type=0 or type=1) and pso._orgid=? and soa._orgid=? and organisation_factories._orgid=? and pso.geo_location=organisation_factories._id ) AS pod WHERE pod._orgid=? and pod.company_name like ?", _orgId, _orgId, _orgId, _orgId, "%"+CustomerName+"%").Values(&maps)
	}
	if err == nil && num > 0 {
		//var i int64 = 0
		for k, v := range maps {
			if maps[k]["p_date"] != nil && maps[k]["p_date"] != "" {
				maps[k]["p_date"] = convertDateString(maps[k]["p_date"].(string))
			}
			if maps[k]["d_date"] != nil && maps[k]["d_date"] != "" {
				maps[k]["d_date"] = convertDateString(maps[k]["d_date"].(string))
			}
			if maps[k]["total_taxable_value"] == nil {
				maps[k]["total_taxable_value"] = ""
			}
			if maps[k]["total_tax_cess"] == nil {
				maps[k]["total_tax_cess"] = ""
			}
			if maps[k]["total_freight_packaging_others"] == nil {
				maps[k]["total_freight_packaging_others"] = ""
			}
			if maps[k]["total_after_taxes"] == nil {
				maps[k]["total_after_taxes"] = ""
			}
			if maps[k]["total_invoice_value"] == nil {
				maps[k]["total_invoice_value"] = ""
			}
			if maps[k]["billing_address"] == nil {
				maps[k]["billing_address"] = ""
			}
			if maps[k]["payment_terms"] == nil {
				maps[k]["payment_terms"] = ""
			}
			if maps[k]["instructions"] == nil {
				maps[k]["instructions"] = ""
			}
			if maps[k]["other_total"] == nil {
				maps[k]["other_total"] = ""
			}
			if maps[k]["factory_location"] == nil {
				maps[k]["factory_location"] = ""
			}

			var transMaps []orm.Params
			transNum, transErr := o.Raw("SELECT * FROM po_so_items WHERE _orgid=? and purchase_service_order_fkid=? and status=1", _orgId, maps[k]["_id"]).Values(&transMaps)
			if transNum > 0 && transErr == nil {
				if transMaps[0]["transporter_fkid"] == nil {
					maps[k]["transporter_fkid"] = ""
				} else {
					maps[k]["transporter_fkid"] = transMaps[0]["transporter_fkid"].(string)
				}
				if transMaps[0]["vehicle_fkid"] == nil {
					maps[k]["vehicle_fkid"] = ""
				} else {
					maps[k]["vehicle_fkid"] = transMaps[0]["vehicle_fkid"].(string)
				}
			} else {
				maps[k]["vehicle_fkid"] = ""
				maps[k]["vehicle_fkid"] = ""
			}

			if maps[k]["transporter_fkid"] != nil && maps[k]["transporter_fkid"] != "" {
				var transportermaps []orm.Params
				transporternum, transportererr := o.Raw("SELECT * FROM transporter WHERE _id=? and _orgid=? ", maps[k]["transporter_fkid"], _orgId).Values(&transportermaps)
				if transporternum > 0 && transportererr == nil {
					maps[k]["transporter_name"] = transportermaps[0]["transporter_name"]
				}
			}

			if maps[k]["vehicle_fkid"] != nil && maps[k]["vehicle_fkid"] != "" {
				var vehiclemaps []orm.Params
				vehiclenum, vehicleerr := o.Raw("SELECT * FROM transporter WHERE _id=? and _orgid=? ", maps[k]["vehicle_fkid"], _orgId).Values(&vehiclemaps)
				if vehiclenum > 0 && vehicleerr == nil {
					maps[k]["registration_number"] = vehiclemaps[0]["registration_number"]
				}
			}
			key := "po_" + strconv.Itoa(k)
			result[key] = v
		}

	} else {
		result["status"] = nil
	}

	return result
}

func GetSupplierBillingAddress(supplierid string, orgid string) string {
	var address_line string
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT CONCAT(soa.hq_address_line1, ', ', soa.city, ', ', soa.state, ', ', soa.country, ', ', soa.pincode) AS address_string FROM supplier_org_assoc as soa WHERE soa._id=? and soa._orgid=? and soa.status=1 ", supplierid, orgid).Values(&maps)
	if num > 0 && err == nil {
		if maps[0]["address_string"] != nil {
			address_line = maps[0]["address_string"].(string)
		}
	} else {
		address_line = ""
	}
	return address_line
}

func GetMaxPurchaseOrderCode(orgid string) int {
	var maxCode int
	maxCode = 1
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT max(unique_serial_no) as maxcode FROM purchase_service_order where _orgid=?", orgid).Values(&maps)

	if num > 0 && err == nil {
		if maps[0]["maxcode"] != nil {
			code := maps[0]["maxcode"].(string)
			getValue, _ := strconv.ParseInt(code, 10, 64)
			maxCode = int(getValue)
		}
	}
	return maxCode
}

func ChangePurchaseOrderStatus(user_id string, orgid string, order_id string, order_status string) int {
	o := orm.NewOrm()
	ormTransactionalErr := o.Begin()
	_, err := o.Raw("Update purchase_service_order set status=? WHERE _id=? and _orgid=? ", order_status, order_id, orgid).Exec()
	if err != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}
	_, err2 := o.Raw("INSERT INTO po_so_status (_orgid, purchase_service_order_fkid, actedon_by_fkid, date_actedon, status) VALUES (?,?,?,?,?) ", orgid, order_id, user_id, time.Now(), order_status).Exec()
	if err2 != nil {
		ormTransactionalErr = o.Rollback()
		return 0
	}

	if ormTransactionalErr != nil {
		return 0
	}
	ormTransactionalErr = o.Commit()
	return 1
}

func CheckIfPOComplete(orgid string, order_id string) int {
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT * FROM po_so_items WHERE _orgid=? and purchase_service_order_fkid=? and status=1", orgid, order_id).Values(&maps)
	if err != nil && num < 1 {
		return 0
	}

	for k, _ := range maps {
		var quantity float64 = 0.0
		var recieved_quantity float64 = 0.0
		if maps[k]["quantity"] != nil && maps[k]["quantity"] != "" {
			quantity, _ = strconv.ParseFloat(maps[k]["quantity"].(string), 64)
		}
		if maps[k]["received_quantity"] != nil && maps[k]["received_quantity"] != "" {
			recieved_quantity, _ = strconv.ParseFloat(maps[k]["received_quantity"].(string), 64)
		}
		if quantity != recieved_quantity {
			return 0
		}
	}
	return 1
}

func GetMaxGRNCode(orgid string) int {
	var maxCode int
	maxCode = 1
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT max(grn_no) as maxcode FROM po_so_items_with_grn where _orgid=?", orgid).Values(&maps)

	if num > 0 && err == nil {
		if maps[0]["maxcode"] != nil {
			code := maps[0]["maxcode"].(string)
			getValue, _ := strconv.ParseInt(code, 10, 64)
			maxCode = int(getValue)
		}
	}
	return maxCode
}

func GetPurchasedetailsByPkId(purchaseId string, orgId string) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT pso.*, pso.date as order_date, soa.*, orgf.* FROM purchase_service_order as pso, supplier_org_assoc as soa, organisation_factories as orgf WHERE pso._orgid=? and pso._id=? and soa._orgid=? and soa._id=pso.supplier_fkid and orgf._orgid=? and orgf._id=pso.geo_location", orgId, purchaseId, orgId, orgId).Values(&maps)
	if err == nil && num > 0 {
		for k, v := range maps {
			if maps[k]["order_date"] != nil && maps[k]["order_date"] != "" {
				maps[k]["o_date"] = convertDateString(maps[k]["order_date"].(string))
			}
			if maps[k]["delivery_date"] != nil && maps[k]["delivery_date"] != "" {
				maps[k]["d_date"] = convertDateString(maps[k]["delivery_date"].(string))
			}
			var poItemsMaps []orm.Params
			count, err1 := o.Raw("SELECT * From po_so_items as psi, item_org_assoc as ioa WHERE psi._orgid=? and psi.purchase_service_order_fkid=? and psi.status=1 and ioa._id=psi.item_org_assoc_fkid and ioa._orgid=?", orgId, purchaseId, orgId).Values(&poItemsMaps)
			if count > 0 && err1 == nil {
				maps[k]["poitems"] = poItemsMaps
			}

			// var originLocMaps []orm.Params
			// origincount, err2 := o.Raw("SELECT * From organisation_factories WHERE _orgid=? and _id=?", orgId, maps[0]["origin_location"]).Values(&originLocMaps)
			// if origincount > 0 && err2 == nil {
			// 	maps[k]["origin_factory"] = originLocMaps[0]["factory_location"]
			// }

			// var deliveryLocMaps []orm.Params
			// deliverycount, err3 := o.Raw("SELECT * From organisation_factories WHERE _orgid=? and _id=?", orgId, maps[0]["delivery_location"]).Values(&deliveryLocMaps)
			// if deliverycount > 0 && err3 == nil {
			// 	maps[k]["delivery_factory"] = deliveryLocMaps[0]["factory_location"]
			// }
			if maps[k]["transporter_fkid"] != nil && maps[k]["transporter_fkid"] != "" {
				var transportermaps []orm.Params
				transporternum, transportererr := o.Raw("SELECT * FROM transporter WHERE _id=? and _orgid=? ", maps[k]["transporter_fkid"], orgId).Values(&transportermaps)
				if transporternum > 0 && transportererr == nil {
					maps[k]["transporter_name"] = transportermaps[0]["transporter_name"]
				}
			}

			if maps[k]["vehicle_fkid"] != nil && maps[k]["vehicle_fkid"] != "" {
				var vehiclemaps []orm.Params
				vehiclenum, vehicleerr := o.Raw("SELECT * FROM vehicle WHERE _id=? and _orgid=? ", maps[k]["vehicle_fkid"], orgId).Values(&vehiclemaps)
				if vehiclenum > 0 && vehicleerr == nil {
					maps[k]["registration_number"] = vehiclemaps[0]["registration_number"]
				}
			}

			key := "po_" + strconv.Itoa(k)
			result[key] = v
		}
	} else {
		result["status"] = nil
	}
	return result
}

func GetTransportersforDropdown(orgId string) map[string]orm.Params {
	result := make(map[string]orm.Params)
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT * from transporter where _orgid =? and status = 1 ", orgId).Values(&maps)
	if err == nil && num > 0 {
		for k, v := range maps {
			key := "transporter_" + strconv.Itoa(k)
			result[key] = v
		}
	} else {
		result["status"] = nil
	}
	return result
}

func CheckIfBatchNoUnique(orgId string, batchNO string) int {
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT * from item_stock as ist, po_so_items_with_grn as psg WHERE ist._orgid =? and psg._orgid=? and (ist.batch_no=? or psg.batch_no=?)", orgId, orgId, batchNO, batchNO).Values(&maps)
	if err != nil || num > 0 {
		return 0
	}
	return 1
}
