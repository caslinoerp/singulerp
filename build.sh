#!/bin/sh 
set -x
sudo systemctl stop singulerp.service
go get golang.org/x/crypto/bcrypt
go get github.com/astaxie/beego
go get github.com/astaxie/beego/validation
go get github.com/astaxie/beego/context
go get github.com/astaxie/beego/orm
go get github.com/go-sql-driver/mysql
go get gopkg.in/mailgun/mailgun-go.v1
go get github.com/astaxie/beego/session/mysql
go get github.com/gocarina/gocsv
go get github.com/jung-kurt/gofpdf
go get github.com/microcosm-cc/bluemonday
go get github.com/russross/blackfriday
go get github.com/aws/aws-sdk-go/...
go build
sudo systemctl restart singulerp.service
