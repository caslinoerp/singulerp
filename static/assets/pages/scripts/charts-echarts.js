jQuery(document).ready(function() {
	var getUrl = window.location;
	//var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
	var baseUrl = getUrl .protocol + "//" + getUrl.host;
	//console.log(baseUrl)
    // ECHARTS
    require.config({
        paths: {
            echarts: baseUrl+'/static/assets/global/plugins/echarts/'
        }
    });

    // DEMOS
    require(
        [
            'echarts',
            'echarts/chart/bar',
            'echarts/chart/chord',
            'echarts/chart/eventRiver',
            'echarts/chart/force',
            'echarts/chart/funnel',
            'echarts/chart/gauge',
            'echarts/chart/heatmap',
            'echarts/chart/k',
            'echarts/chart/line',
            'echarts/chart/map',
            'echarts/chart/pie',
            'echarts/chart/radar',
            'echarts/chart/scatter',
            'echarts/chart/tree',
            'echarts/chart/treemap',
            'echarts/chart/venn',
            'echarts/chart/wordCloud'
        ],
        function(ec) {
           
            // -- PIE --
            var myChart5 = ec.init(document.getElementById('echarts_pie'));
            myChart5.setOption({
                tooltip: {
                    show: true,
                    formatter: "{a} <br/>{b} : {c} ({d}%)"
                },
                legend: {
                    orient: 'horizontal',
                    x: 'left',
                    data: ['Payroll', 'Inventory', 'Electricity', 'Fuel', 'Transport', 'Statuatory']
                },
                toolbox: {
                    show: false,
                    feature: {
                        mark: {
                            show: true
                        },
                        dataView: {
                            show: true,
                            readOnly: false
                        },
                        restore: {
                            show: true
                        },
                        saveAsImage: {
                            show: true
                        }
                    }
                },
                calculable: true,
                series: [{
                    name: 'Source',
                    type: 'pie',
                    center: ['50%', 200],
                    radius: 80,
                    itemStyle: {
                        normal: {
                            label: {
                                position: 'inner',
                                formatter: function(params) {
                                    return (params.percent - 0).toFixed(0) + '%'
                                }
                            },
                            labelLine: {
                                show: false
                            }
                        },
                        emphasis: {
                            label: {
                                show: true,
                                formatter: "{b}\n{d}%"
                            }
                        }

                    },
                    data: [{
                        value: 335,
                        name: 'Payroll'
                    }, {
                        value: 679,
                        name: 'Inventory'
                    }, {
                        value: 1548,
                        name: 'Electricity'
                    }]
                }, {
                    name: 'Source',
                    type: 'pie',
                    center: ['50%', 200],
                    radius: [110, 140],
                    data: [{
                        value: 335,
                        name: 'Payroll'
                    }, {
                        value: 310,
                        name: 'Fuel'
                    }, {
                        value: 234,
                        name: 'Transport'
                    }, {
                        value: 234,
                        name: 'Statuatory'
                    }]
                }]
            });
        }
    );
});