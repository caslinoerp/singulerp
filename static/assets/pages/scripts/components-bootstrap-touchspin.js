var ComponentsBootstrapTouchSpin = function() {

    var handleDemo = function() {

        $("#touchspin_1").TouchSpin({
            min: 0,
            max: 10000000000000,
            step: 0.1,
            decimals: 2,
            boostat: 5,
            maxboostedstep: 10,
            postfix: '%'
        });

        $("#touchspin_2, #mua_lead_time_days1").TouchSpin({
            verticalbuttons: true,
            min: 0,
            max: 10000000000000,
            step: 1,
            decimals: 2,
            boostat: 5,
            maxboostedstep: 10,
            forcestepdivisibility: 'none'
        });
		
		$(".pcp .mt-repeater-add").click(function(){
			$(".pcp_qty").TouchSpin({
				verticalbuttons: true
			});
		});

        $("#touchspin_3, #mua_min_stock_qty, #mua_min_order_qty, #mua_lead_time_days, #mua_total_qty_in_storage_area, #sai_qty_purchased, #sa_qty_purchased, #Shelf_Life_Days, .pcp_qty").TouchSpin({
            verticalbuttons: true,
            min: 0,
            max: 10000000000000,
            step: 1,
            decimals: 5,
            boostat: 5,
            maxboostedstep: 10,
            forcestepdivisibility: 'none'
        });

        $("#touchspin_4").TouchSpin({
            verticalbuttons: true,
            verticalupclass: 'glyphicon glyphicon-plus',
            verticaldownclass: 'glyphicon glyphicon-minus'
        });

        $("#touchspin_5").TouchSpin();

        $("#touchspin_6").TouchSpin({
            initval: 40
        });

        $("#touchspin_7").TouchSpin({
            initval: 40
        });

        $("#touchspin_8").TouchSpin({
            postfix: "a button",
            postfix_extraclass: "btn red"
        });

        $("#touchspin_9").TouchSpin({
            postfix: "a button",
            postfix_extraclass: "btn green"
        });

        $("#touchspin_10").TouchSpin({
            prefix: "pre",
            postfix: "post"
        });

        $("#touchspin_11").TouchSpin({
            buttondown_class: "btn blue",
            buttonup_class: "btn red"
        });

    }

    return {
        //main function to initiate the module
        init: function() {
            handleDemo();
        }
    };

}();

jQuery(document).ready(function() {
    ComponentsBootstrapTouchSpin.init();
});