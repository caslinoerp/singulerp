			
$(document).ready(function() {
    BindControls();
});

function BindControls() {
    var org_id = $("#org_id").val();
    var info = 'id=' + org_id;
    // var Countries = ['ARGENTINA', 
    // 'AUSTRALIA', 
    // 'BRAZIL', 
    // 'BELARUS', 
    // 'BHUTAN',
    // 'CHILE', 
    // 'CAMBODIA', 
    // 'CANADA', 
    // 'CHILE', 
    // 'DENMARK', 
    // 'DOMINICA'];

    $.ajax({
            type: "GET",
            url: "/stock/get-edit-details",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            cache: false,
            success: function(data){ 
                $('#unit_measure').autocomplete({
                    source: data.uom,
                    minLength: 0,
                    scroll: true
                }).focus(function() {
                        $(this).autocomplete("search", "");
                });

                $('#grades').autocomplete({
                        source: data.grade,
                        minLength: 0,
                        scroll: true
                }).focus(function() {
                        $(this).autocomplete("search", "");
                });

                $('#groups').autocomplete({
                        source: data.group,
                        minLength: 0,
                        scroll: true
                }).focus(function() {
                        $(this).autocomplete("search", "");
                });

                $('#subgroups').autocomplete({
                        source: data.subgroup,
                        minLength: 0,
                        scroll: true
                }).focus(function() {
                        $(this).autocomplete("search", "");
                });

            } 
    });
}