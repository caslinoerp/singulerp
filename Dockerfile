# Start from a Debian image with the latest version of Go installed
# and a workspace (GOPATH) configured at /go.
FROM golang:latest

# Copy the local package files to the container's workspace.
ADD . /go/src/bitbucket.org/caslinoerp/singulerp

# Build the singulerp command inside the container.
# (You may fetch or manage dependencies here,
# either manually or with a tool like "godep".)
RUN go get golang.org/x/crypto/bcrypt
RUN go get github.com/astaxie/beego
RUN go get github.com/astaxie/beego/validation
RUN go get github.com/astaxie/beego/context
RUN go get github.com/astaxie/beego/orm
RUN go get github.com/go-sql-driver/mysql
RUN go get github.com/gocarina/gocsv
RUN go get gopkg.in/mailgun/mailgun-go.v1
RUN go get github.com/astaxie/beego/session/mysql
RUN go build bitbucket.org/caslinoerp/singulerp

# Run the singulerp command by default when the container starts.
ENTRYPOINT /go/src/bitbucket.org/caslinoerp/singulerp/singulerp

# Document that the service listens on port 8090.
EXPOSE 8090