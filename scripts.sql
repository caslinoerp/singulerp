Date: 2/2/2017
ALTER TABLE `login` 
CHANGE COLUMN `password` `password` TEXT(10) NOT NULL ,
CHANGE COLUMN `role` `role` TEXT(20) NOT NULL ;


Date 6/2/2017
ALTER TABLE `po-so_supplier_invoice` 
CHANGE COLUMN `_orgid` `_orgid` BIGINT(10) UNSIGNED NULL ,
CHANGE COLUMN `po-so_items_with_grn_list` `po-so_items_with_grn_list` VARCHAR(45) NULL ,
CHANGE COLUMN `grn_no` `grn_no` INT(11) NULL ,
CHANGE COLUMN `invoice_no` `invoice_no` INT(11) NULL ,
CHANGE COLUMN `invoice_date` `invoice_date` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ,
CHANGE COLUMN `dc_no` `dc_no` INT(11) NULL ,
CHANGE COLUMN `dc_date` `dc_date` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ;


ALTER TABLE `customer_support` 
CHANGE COLUMN `date_visited` `date_visited` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ;


ALTER TABLE `purchase_service_order` 
CHANGE COLUMN `unique_serial_no` `unique_serial_no` BIGINT(10) UNSIGNED NULL ,
CHANGE COLUMN `_orgid` `_orgid` BIGINT(10) UNSIGNED NULL ,
CHANGE COLUMN `type` `type` VARCHAR(20) NULL ,
CHANGE COLUMN `date` `date` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ,
CHANGE COLUMN `currency` `currency` VARCHAR(20) NULL ,
CHANGE COLUMN `supplier_fkid` `supplier_fkid` BIGINT(10) UNSIGNED NULL ,
CHANGE COLUMN `delivery_date` `delivery_date` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ,
CHANGE COLUMN `geo_location` `geo_location` VARCHAR(20) NULL ,
CHANGE COLUMN `instructions` `instructions` VARCHAR(45) NULL ,
CHANGE COLUMN `payment_terms` `payment_terms` VARCHAR(45) NULL ,
CHANGE COLUMN `billing_address` `billing_address` VARCHAR(50) NULL ,
CHANGE COLUMN `discount_percentage` `discount_percentage` INT(11) NULL ,
CHANGE COLUMN `excise_duty` `excise_duty` INT(11) NULL ,
CHANGE COLUMN `vat` `vat` INT(11) NULL ,
CHANGE COLUMN `freight_cost` `freight_cost` INT(11) NULL ,
CHANGE COLUMN `octoroi` `octoroi` VARCHAR(20) NULL ,
CHANGE COLUMN `item_total` `item_total` INT(11) NULL ,
CHANGE COLUMN `other_total` `other_total` INT(11) NULL ,
CHANGE COLUMN `final_discount_percentage` `final_discount_percentage` INT(11) NULL ,
CHANGE COLUMN `final_total` `final_total` INT(11) NULL ;



Date 7/2/2017

ALTER TABLE `hr_empleaves` 
ADD COLUMN `earn_leaves` INT NULL AFTER `casual_leaves`;

ALTER TABLE `production_job_card` 
ADD COLUMN `jobcard_no` INT NULL AFTER `_id`;


ALTER TABLE `item_org_assoc` 
ADD COLUMN `raw_material_or_product` VARCHAR(45) NULL AFTER `last_updated`;


ALTER TABLE `item_master` 
DROP COLUMN `raw_material_or_product`;



8/2/2017


ALTER TABLE `item_org_assoc` 
CHANGE COLUMN `min_stock_trigger` `min_stock_trigger` TINYINT(5) NULL ,
CHANGE COLUMN `min_order_quantity` `min_order_quantity` INT(11) NULL ,
CHANGE COLUMN `bin_location` `bin_location` VARCHAR(20) NULL ,
CHANGE COLUMN `lead_time` `lead_time` INT(11) NULL ,
CHANGE COLUMN `geo_location` `geo_location` VARCHAR(20) NULL ;


2/08/2017


ALTER TABLE `product_master` 
ADD COLUMN `product_code` VARCHAR(20) NULL AFTER `is_finished_product`,
ADD COLUMN `product_name` VARCHAR(20) NULL AFTER `product_code`;


ALTER TABLE `product_master` 
CHANGE COLUMN `capacity_measure` `capacity_measure` INT NOT NULL ;

ALTER TABLE `product_master` 
CHANGE COLUMN `group` `grp` VARCHAR(20) NOT NULL ;

ALTER TABLE `machine_master` 
CHANGE COLUMN `group` `grp` VARCHAR(20) NULL DEFAULT NULL ;

ALTER TABLE `operation_master` 
CHANGE COLUMN `group` `grp` VARCHAR(20) NOT NULL ;


9/02/2017



ALTER TABLE `item_org_assoc` 
CHANGE COLUMN `bin_location` `bin_location` VARCHAR(100) NULL DEFAULT NULL ;

ALTER TABLE `item_rates` 
CHANGE COLUMN `currency` `currency` VARCHAR(20) NULL ,
CHANGE COLUMN `start_date` `start_date` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ,
CHANGE COLUMN `end_date` `end_date` TIMESTAMP(6) NULL DEFAULT '0000-00-00 00:00:00.000000' ,
CHANGE COLUMN `latest` `latest` TINYINT(5) NULL ;

ALTER TABLE `item_total_inventory` 
CHANGE COLUMN `total_quantity` `total_quantity` INT(11) NULL ,
CHANGE COLUMN `geo_location` `geo_location` VARCHAR(20) NULL ;

ALTER TABLE `item_stock` 
CHANGE COLUMN `expires_by` `expires_by` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ,
CHANGE COLUMN `start_date` `start_date` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ,
CHANGE COLUMN `end_date` `end_date` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ,
CHANGE COLUMN `last_updated` `last_updated` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ,
CHANGE COLUMN `latest` `latest` VARCHAR(20) NULL ,
CHANGE COLUMN `po_fkid` `po_fkid` INT(11) NULL ;


9/2/2017

ALTER TABLE `customer` 
CHANGE COLUMN `group` `grp` VARCHAR(20) NULL DEFAULT NULL ;

ALTER TABLE `customer` 
DROP COLUMN `email`,
DROP COLUMN `cell_no`,
DROP COLUMN `phone_no`;


ALTER TABLE `supplier_master` 
CHANGE COLUMN `gst` `gst` VARCHAR(20) NULL ,
ADD COLUMN `notes` VARCHAR(45) NULL AFTER `gst`;


ALTER TABLE `hr_employeedetails` 
ADD COLUMN `_orgid` BIGINT(10) NULL AFTER `salary`;

ALTER TABLE `hr_empleaves` 
ADD COLUMN `_orgid` BIGINT(10) NULL AFTER `restricted_holidays`;

ALTER TABLE `hr_empcontract` 
ADD COLUMN `_orgid` BIGINT(10) NULL AFTER `contract_renew_date`;

ALTER TABLE `hr_employeedetails` 
CHANGE COLUMN `contact_no` `contact_no` INT(30) NULL DEFAULT NULL ,
CHANGE COLUMN `cell_no` `cell_no` INT(30) NULL DEFAULT NULL ,
CHANGE COLUMN `emergency_contact` `emergency_contact` INT(30) NULL DEFAULT NULL ;


ALTER TABLE `production_job_card` 
ADD COLUMN `_orgid` BIGINT(10) NULL AFTER `notes`;

ALTER TABLE `qa_inspection_predelivery` 
ADD COLUMN `_orgid` BIGINT(10) NULL AFTER `remark`;

ALTER TABLE `qa_inspection_item` 
ADD COLUMN `_orgid` BIGINT(10) NULL AFTER `remark`;

CREATE TABLE `sales_order` (
  `_id` BIGINT(10) UNSIGNED NOT NULL,
  `order_no` INT NULL,
  `order_date` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `customer_name` VARCHAR(25) NULL,
  `job_type` VARCHAR(20) NULL,
  `delivery_date` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `excise_charges` INT NULL,
  `transport_charges` INT NULL,
  `form30` VARCHAR(25) NULL,
  `formc` VARCHAR(25) NULL,
  `dispatch_type` VARCHAR(25) NULL,
  `payment_terms` VARCHAR(25) NULL,
  `repeat_order` VARCHAR(25) NULL,
  `remarks` VARCHAR(45) NULL,
  PRIMARY KEY (`_id`),
  UNIQUE INDEX `_id_UNIQUE` (`_id` ASC));


  CREATE TABLE `sales_order_items` (
  `_id` BIGINT(10) UNSIGNED NOT NULL,
  `_sales_order_fkid` BIGINT(10) UNSIGNED NULL,
  `item_name` VARCHAR(25) NULL,
  `quantity` INT NULL,
  `uom` VARCHAR(25) NULL,
  `price` INT NULL,
  `discount` INT NULL,
  `delivery_date` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`_id`),
  UNIQUE INDEX `_id_UNIQUE` (`_id` ASC));


ALTER TABLE `sales_order` 
ADD COLUMN `_orgid` BIGINT(10) NULL AFTER `remarks`;

ALTER TABLE `customer_support` 
CHANGE COLUMN `serial_no_fkuniqueSrno` `_sales_order_fkid` BIGINT(10) UNSIGNED NOT NULL ;


CREATE TABLE `customer_product_assoc` (
  `_id` BIGINT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `_product_master_fkid` BIGINT(10) UNSIGNED NULL,
  `_customer_fkid` BIGINT(10) UNSIGNED NULL,
  `last_updated` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`_id`),
  UNIQUE INDEX `_id_UNIQUE` (`_id` ASC));


  ALTER TABLE `sales_order` 
CHANGE COLUMN `_id` `_id` BIGINT(10) UNSIGNED NOT NULL AUTO_INCREMENT ;

ALTER TABLE `sales_order_items` 
CHANGE COLUMN `_id` `_id` BIGINT(10) UNSIGNED NOT NULL AUTO_INCREMENT ;

9/02/2017

ALTER TABLE `item_total_inventory` 
ADD COLUMN `status` TINYINT(6) NULL AFTER `geo_location`;

13/02/2017


ALTER TABLE `item_stock_adjustment` 
CHANGE COLUMN `updated_by` `updated_by` VARCHAR(10) NULL ;


ALTER TABLE `customer_contact_person` 
CHANGE COLUMN `cell_no` `cell_no` INT(30) NULL DEFAULT NULL ;

ALTER TABLE `supplier_address` 
CHANGE COLUMN `city` `wcity` VARCHAR(20) NOT NULL ,
CHANGE COLUMN `state` `wstate` VARCHAR(20) NOT NULL ,
CHANGE COLUMN `country` `wcountry` VARCHAR(20) NOT NULL ,
CHANGE COLUMN `pincode` `wpincode` INT(11) NOT NULL ;

ALTER TABLE `item_stock_adjustment` 
CHANGE COLUMN `updated_by` `updated_by` VARCHAR(45) NOT NULL ;

ALTER TABLE `item_stock_adjustment` 
CHANGE COLUMN `last_updated` `last_updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ;


14/02/2017

ALTER TABLE `purchase_service_order` 
ADD COLUMN `freight_type` VARCHAR(45) NULL AFTER `vat`;

ALTER TABLE `purchase_service_order` 
ADD COLUMN `other_charges` TEXT(30) NULL AFTER `item_total`;

ALTER TABLE `purchase_service_order` 
CHANGE COLUMN `unique_serial_no` `unique_serial_no` VARCHAR(46) NULL DEFAULT NULL ;

ALTER TABLE `purchase_service_order` 
CHANGE COLUMN `octoroi` `octroi` VARCHAR(20) NULL DEFAULT NULL ;


14/02/2017

ALTER TABLE `grn_no_used` 
ADD COLUMN `po-so_items_with_grn_fkid` BIGINT(10) UNSIGNED NULL AFTER `created_on`;

ALTER TABLE `po-so_items_with_grn` 
ADD COLUMN `delivered_date` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP AFTER `status`;

ALTER TABLE `po-so_items_with_grn` 
CHANGE COLUMN `received_quantity` `received_sent_quantity` INT(11) NOT NULL ,
ADD COLUMN `received_sent_by` VARCHAR(45) NULL AFTER `received_sent_quantity`;

DROP TABLE `login`;

ALTER TABLE `purchase_service_order` 
ADD COLUMN `other_charges` TEXT(20) NULL AFTER `item_total`;

CREATE TABLE `industry_type` (
  `_id` BIGINT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `industry_type` TEXT(30) NULL,
  `industry_group` TEXT(30) NULL,
  `industry_class` TEXT(30) NULL,
  PRIMARY KEY (`_id`),
  UNIQUE INDEX `_id_UNIQUE` (`_id` ASC));

CREATE TABLE `organisation_factories` (
  `_id` BIGINT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `_orgid` BIGINT(10) UNSIGNED NULL,
  `factory_location` VARCHAR(45) NULL,
  `factory_type` TINYINT(6) NULL,
  `added_on` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` BIGINT(10) UNSIGNED NULL,
  PRIMARY KEY (`_id`),
  UNIQUE INDEX `_id_UNIQUE` (`_id` ASC));

  CREATE TABLE `organisation` (
  `_id` BIGINT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `orgid` BIGINT(10) UNSIGNED NULL,
  `org_name` VARCHAR(45) NULL,
  `org_mobile` INT(30) NULL,
  `org_legal_name` VARCHAR(45) NULL,
  `industry_type_fkid` BIGINT(10) UNSIGNED NULL,
  `org_email` VARCHAR(45) NULL,
  `created_on` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `verification_code` VARCHAR(45) NULL,
  `verification_status` TINYINT(6) NULL,
  `status` TINYINT(6) NULL,
  PRIMARY KEY (`_id`),
  UNIQUE INDEX `_id_UNIQUE` (`_id` ASC));

CREATE TABLE `user_account` (
  `_id` BIGINT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(45) NULL,
  `password` VARCHAR(45) NULL,
  `_orgid` BIGINT(10) UNSIGNED NULL,
  `email` VARCHAR(45) NULL,
  `status` TINYINT(6) NULL,
  `usertype` TINYINT(6) NULL,
  PRIMARY KEY (`_id`),
  UNIQUE INDEX `_id_UNIQUE` (`_id` ASC));

  CREATE TABLE `user_roles` (
  `_id` BIGINT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `_orgid` BIGINT(10) UNSIGNED NULL,
  `user_account_fkid` BIGINT(10) UNSIGNED NULL,
  `role_master_fkid` BIGINT(10) UNSIGNED NULL,
  `assigned_by_fkid` BIGINT(10) UNSIGNED NULL,
  `assigned_on` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` TINYINT(6) NULL,
  PRIMARY KEY (`_id`),
  UNIQUE INDEX `_id_UNIQUE` (`_id` ASC));

CREATE TABLE `roles_master` (
  `_id` BIGINT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `module` TEXT(30) NULL,
  `role_type` TINYINT(6) NULL,
  `page` TEXT(30) NULL,
  PRIMARY KEY (`_id`),
  UNIQUE INDEX `_id_UNIQUE` (`_id` ASC));


ALTER TABLE `organisation_factories` 
CHANGE COLUMN `status` `status` TINYINT(6) UNSIGNED NULL DEFAULT NULL ;


ALTER TABLE `poso_supplier_invoice` 
RENAME TO  `po_so_supplier_invoice` ;

ALTER TABLE `po-so_status` 
RENAME TO  `po_so_status` ;

ALTER TABLE `po-so_items_with_grn` 
RENAME TO  `po_so_items_with_grn` ;

ALTER TABLE `grn_no_used` 
CHANGE COLUMN `po-so_items_with_grn_fkid` `po_so_items_with_grn_fkid` BIGINT(10) UNSIGNED NULL DEFAULT NULL ;


15/02/2017

ALTER TABLE `hr_employeedetails` 
CHANGE COLUMN `contact_no` `contact_no` INT(40) NULL DEFAULT NULL ,
CHANGE COLUMN `cell_no` `cell_no` INT(40) NULL DEFAULT NULL ,
CHANGE COLUMN `emergency_contact` `emergency_contact` INT(40) NULL DEFAULT NULL ;

ALTER TABLE `organisation` CHANGE COLUMN `org_mobile` `org_mobile` VARCHAR(40) NULL DEFAULT NULL ;
ALTER TABLE `user_account` CHANGE COLUMN `password` `password` TEXT(100) NULL DEFAULT NULL ;
ALTER TABLE `organisation` ADD COLUMN `org_username` VARCHAR(150) NULL AFTER `org_name`;


ALTER TABLE `item_org_assoc` 
CHANGE COLUMN `item_unique_code` `item_unique_code` VARCHAR(30) NOT NULL ;


16/02/2017

ALTER TABLE `hr_employeedetails` 
DROP COLUMN `salary`,
DROP COLUMN `cell_no`,
ADD COLUMN `employee_id` VARCHAR(100) NULL AFTER `_orgid`;

ALTER TABLE `hr_empcontract` 
ADD COLUMN `salary` INT(30) NULL AFTER `contract_renew_date`;


DROP TABLE `supplier_org_assoc`;

CREATE TABLE `supplier_org_assoc` (
`_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
`supplier_unique_code` varchar(150) NOT NULL,
`_orgid` bigint(10) unsigned NOT NULL,
`supplier_master_fkid` bigint(10) unsigned NULL,
`category` varchar(20) NOT NULL,
`company_name` varchar(20) NOT NULL,
`hq_address_line1` varchar(45) NOT NULL,
`city` varchar(150) NOT NULL,
`state` varchar(150) NOT NULL,
`country` varchar(250) NOT NULL,
`pincode` varchar(250) NOT NULL,  
`lst` varchar(50) NOT NULL,
`cst` varchar(50) NOT NULL,
`pan` varchar(50) NOT NULL,
`ecc` varchar(50) NOT NULL,
`gst` varchar(50) DEFAULT NULL,
`notes` varchar(250) DEFAULT NULL,
`last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
`status` tinyint(5) NOT NULL,
PRIMARY KEY (`_id`),
UNIQUE KEY `_id_UNIQUE` (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;


ALTER TABLE `supplier_address` 
CHANGE COLUMN `supplier_master_fkid` `supplier_org_assoc_fkid` BIGINT(10) UNSIGNED NOT NULL ;

ALTER TABLE `supplier_contact_person` 
CHANGE COLUMN `supplier_master_fkid` `supplier_org_assoc_fkid` BIGINT(10) UNSIGNED NOT NULL ;

ALTER TABLE `supplier_address` 
CHANGE COLUMN `last_updated` `last_updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP AFTER `type_address`,
CHANGE COLUMN `address_line1` `address_line1` VARCHAR(250) NOT NULL ,
CHANGE COLUMN `wcity` `city` VARCHAR(60) NOT NULL ,
CHANGE COLUMN `wstate` `state` VARCHAR(60) NOT NULL ,
CHANGE COLUMN `wcountry` `country` VARCHAR(60) NOT NULL ,
CHANGE COLUMN `wpincode` `pincode` VARCHAR(20) NOT NULL ,
CHANGE COLUMN `type_address` `type_address` TINYINT(5) NOT NULL COMMENT '1 - HQ\n2 - Warehouse\n3 - Office' ;

ALTER TABLE `supplier_org_assoc` 
CHANGE COLUMN `hq_address_line1` `hq_address_line1` VARCHAR(250) NOT NULL ;


17/02/2017

ALTER TABLE `purchase_service_order` 
ADD COLUMN `freight_type` VARCHAR(45) NULL AFTER `vat`;


17/02/2017

ALTER TABLE `customer_contact_person` 
CHANGE COLUMN `name` `name` VARCHAR(250) NULL DEFAULT NULL ,
CHANGE COLUMN `designation` `designation` VARCHAR(250) NULL DEFAULT NULL ,
CHANGE COLUMN `email` `email` VARCHAR(254) NULL DEFAULT NULL ,
ADD COLUMN `contact_type` TINYINT(10) NULL AFTER `email`;

ALTER TABLE `user_account` 
ADD COLUMN `full_name` VARCHAR(250) NULL AFTER `password`;



ALTER TABLE `customer` 
DROP COLUMN `_product_master_fkid`,
DROP INDEX `product_master_fkid_idx` ;

18/02/2017

ALTER TABLE `purchase_service_order` 
CHANGE COLUMN `unique_serial_no` `unique_serial_no` VARCHAR(150) NULL DEFAULT NULL ;

ALTER TABLE `po_so_items_with_grn` 
CHANGE COLUMN `so_action` `so_action` VARCHAR(45) NULL ;

ALTER TABLE `po_so_items_with_grn` 
CHANGE COLUMN `grn_no` `grn_no` INT(11) NULL ,
CHANGE COLUMN `grn_status` `grn_status` TINYINT(5) NULL ,
CHANGE COLUMN `received_sent_quantity` `received_sent_quantity` INT(11) NULL ,
CHANGE COLUMN `rejected_quantity` `rejected_quantity` INT(11) NULL ,
CHANGE COLUMN `rejected_note` `rejected_note` VARCHAR(45) NULL ,
CHANGE COLUMN `rejected_by` `rejected_by` VARCHAR(45) NULL ;


21/02/2017

ALTER TABLE `item_stock` 
DROP COLUMN `po_fkid`,
DROP INDEX `po_fkid` ;

ALTER TABLE `item_stock` 
ADD COLUMN `po_so_items_with_grn_fkid` BIGINT(10) UNSIGNED NULL AFTER `latest`,
ADD COLUMN `items_stock_adjustment_fkid` BIGINT(10) UNSIGNED NULL AFTER `po_so_items_with_grn_fkid`;

ALTER TABLE `item_stock_adjustment` 
DROP COLUMN `item_stock_fkid`,
DROP INDEX `item_stock_fkid_idx` ;

ALTER TABLE `item_stock_adjustment` 
ADD COLUMN `remark` VARCHAR(150) NULL AFTER `last_updated`,
ADD COLUMN `date_of_inspection` TIMESTAMP NULL AFTER `remark`,
ADD COLUMN `quantity_on_record` INT(40) NULL AFTER `date_of_inspection`,
ADD COLUMN `observed_quantity` INT(40) NULL AFTER `quantity_on_record`;

ALTER TABLE `po_so_supplier_invoice` 
ADD COLUMN `supplier_org_assoc_fkid` BIGINT(40) UNSIGNED NULL AFTER `dc_date`,
ADD COLUMN `purchase_service_order_fkid` BIGINT(40) UNSIGNED NULL AFTER `supplier_org_assoc_fkid`;

ALTER TABLE `po_so_items_with_grn` 
ADD COLUMN `po_so_items_fkid` BIGINT(40) UNSIGNED NULL AFTER `item_org_assoc_fkid`;

CREATE TABLE `po_so_items` (
  `_id` BIGINT(40) UNSIGNED NOT NULL AUTO_INCREMENT,
  `_orgid` BIGINT(40) UNSIGNED NULL,
  `purchase_service_order_fkid` BIGINT(40) UNSIGNED NULL,
  `item_org_assoc_fkid` BIGINT(40) UNSIGNED NULL,
  `rate` INT(40) NULL,
  `quantity` INT(100) NULL,
  `total_cost` INT(100) NULL,
  `status` TINYINT(10) NULL,
  PRIMARY KEY (`_id`));

21/02/2017

ALTER TABLE `product_org_assoc` 
ADD COLUMN `group` VARCHAR(45) NULL AFTER `status`,
ADD COLUMN `subgroup` VARCHAR(45) NULL AFTER `group`,
ADD COLUMN `unit_of_measure` VARCHAR(45) NULL AFTER `subgroup`,
ADD COLUMN `capacity_measure` VARCHAR(45) NULL AFTER `unit_of_measure`,
ADD COLUMN `is_finished_product` TINYINT(10) NULL AFTER `capacity_measure`;

ALTER TABLE `product_process` 
CHANGE COLUMN `product_master_fkid` `product_master_fkid` BIGINT(10) UNSIGNED NULL ;

ALTER TABLE `product_org_assoc` 
CHANGE COLUMN `product_org_code` `product_org_code` VARCHAR(150) NOT NULL ,
CHANGE COLUMN `product_org_name` `product_org_name` VARCHAR(200) NOT NULL ,
CHANGE COLUMN `product_master_fkid` `product_master_fkid` BIGINT(10) UNSIGNED NULL ;

ALTER TABLE `product_org_assoc` 
CHANGE COLUMN `invoice_naration` `invoice_naration` VARCHAR(150) NOT NULL ,
CHANGE COLUMN `group` `grp` VARCHAR(45) NULL DEFAULT NULL ;


22/02/2017

ALTER TABLE `product_bom` 
CHANGE COLUMN `product_master_fkid` `product_master_fkid` BIGINT(10) UNSIGNED NULL ,
ADD COLUMN `product_org_assoc_fkid` BIGINT(10) NOT NULL AFTER `quantity`;

23/02/2017

ALTER TABLE `product_bom` 
CHANGE COLUMN `item_master_fkid` `item_master_fkid` BIGINT(10) UNSIGNED NULL ,
ADD COLUMN `item_org_assoc_fkid` BIGINT(10) NOT NULL AFTER `product_org_assoc_fkid`,
ADD COLUMN `status` TINYINT(5) NULL AFTER `item_org_assoc_fkid`;

ALTER TABLE `product_bom` 
CHANGE COLUMN `product_name` `product_name` VARCHAR(20) NULL ,
CHANGE COLUMN `quantity` `quantity` INT(11) NULL ,
CHANGE COLUMN `product_org_assoc_fkid` `product_org_assoc_fkid` BIGINT(10) NULL ,
CHANGE COLUMN `item_org_assoc_fkid` `item_org_assoc_fkid` BIGINT(10) NULL ;

ALTER TABLE `po_so_items_with_grn` 
CHANGE COLUMN `quantity` `quantity` INT(11) NULL ,
CHANGE COLUMN `total_cost` `total_cost` INT(11) NULL ,
CHANGE COLUMN `status` `status` TINYINT(5) NULL ;


ALTER TABLE `po_so_supplier_invoice` 
CHANGE COLUMN `po-so_items_with_grn_list` `po_so_items_with_grn_list` VARCHAR(45) NULL DEFAULT NULL ;


28/2/2017

ALTER TABLE `product_bom` 
DROP COLUMN `item_master_fkid`,
DROP COLUMN `product_name`,
DROP COLUMN `product_master_fkid`;

ALTER TABLE `item_stock` 
CHANGE COLUMN `quantity` `quantity` INT(11) NULL ,
CHANGE COLUMN `rate` `rate` INT(11) NULL ,
CHANGE COLUMN `currency` `currency` VARCHAR(20) NULL ;


28/02/2017

ALTER TABLE `item_stock` 
CHANGE COLUMN `quantity` `quantity` DECIMAL(10,5) NOT NULL ;

1/03/2017

ALTER TABLE `item_stock` 
CHANGE COLUMN `rate` `rate` INT(11) NULL ,
CHANGE COLUMN `currency` `currency` VARCHAR(20) NULL ,
CHANGE COLUMN `geo_location` `geo_location` VARCHAR(20) NULL ;

ALTER TABLE `item_stock_adjustment` 
CHANGE COLUMN `updated_by` `updated_by` VARCHAR(45) NULL ,
CHANGE COLUMN `reason` `reason` VARCHAR(45) NULL ,
CHANGE COLUMN `last_updated` `last_updated` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ;


06/03/2017
ALTER TABLE `customer_product_assoc` 
ADD COLUMN `status` TINYINT(6) NULL AFTER `last_updated`;


ALTER TABLE `customer_product_assoc` 
ADD COLUMN `_orgid` BIGINT(10) UNSIGNED NULL AFTER `status`;


08/03/2017

ALTER TABLE `item_org_assoc` 
ADD COLUMN `max_storage_quantity` INT(11) NULL AFTER `min_order_quantity`;

08/03/2017
ALTER TABLE `customer_contact_person` 
ADD COLUMN `_orgid` BIGINT(10) NULL AFTER `_customer_fkid`;

09/03/2017

ALTER TABLE `product_org_assoc` 
CHANGE COLUMN `invoice_naration` `invoice_narration` VARCHAR(150) NOT NULL ,
CHANGE COLUMN `mininum_stock_keep` `minimum_stock_keep` TINYINT(5) NOT NULL ;


------- Executed till here:Caslino-------------

11/03/2017
ALTER TABLE `product_org_assoc` 
CHANGE COLUMN `rate_per_UOM` `rate_per_UOM` DECIMAL(10,5) NOT NULL ,
CHANGE COLUMN `minimum_stock_keep` `minimum_stock_keep` DECIMAL(10,5) NOT NULL ,
CHANGE COLUMN `capacity_measure` `capacity_measure` DECIMAL(10,5) NULL DEFAULT NULL ;

ALTER TABLE `production_job_card` 
CHANGE COLUMN `special_test` `special_test` DECIMAL(10,5) NULL DEFAULT NULL ,
CHANGE COLUMN `max_oven_temp` `max_oven_temp` DECIMAL(10,5) NULL DEFAULT NULL ,
CHANGE COLUMN `removal_temp` `removal_temp` DECIMAL(10,5) NULL DEFAULT NULL ;

ALTER TABLE `item_org_assoc` 
CHANGE COLUMN `min_stock_trigger` `min_stock_trigger` DECIMAL(10,5) NULL DEFAULT NULL ,
CHANGE COLUMN `max_storage_quantity` `max_storage_quantity` DECIMAL(10,5) NULL DEFAULT NULL ,
CHANGE COLUMN `lead_time` `lead_time` DECIMAL(10,5) NULL DEFAULT NULL ;



14/03/2017
ALTER TABLE `sales_order` 
CHANGE COLUMN `order_no` `order_no` VARCHAR(150) NULL DEFAULT NULL ;

ALTER TABLE `organisation` 
ADD COLUMN `org_brand_name` VARCHAR(250) NULL AFTER `org_legal_name`;

ALTER TABLE `organisation` 
DROP COLUMN `industry_type_fkid`;

ALTER TABLE `industry_type` 
CHANGE COLUMN `_id` `_id` BIGINT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
ADD COLUMN `_orgid` BIGINT(10) UNSIGNED NULL AFTER `industry_class`;


ALTER TABLE `product_org_assoc` 
ADD COLUMN `transport` VARCHAR(100) NULL AFTER `is_finished_product`;

15/03/2017
ALTER TABLE `supplier_address` 
CHANGE COLUMN `city` `wcity` VARCHAR(60) NOT NULL ,
CHANGE COLUMN `state` `wstate` VARCHAR(60) NOT NULL ,
CHANGE COLUMN `country` `wcountry` VARCHAR(60) NOT NULL ,
CHANGE COLUMN `pincode` `wpincode` VARCHAR(250) NOT NULL ;

CREATE TABLE `customer_warehouse_address` (
  `_id` BIGINT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `customer_fkid` BIGINT(10) UNSIGNED NULL,
  `_orgid` BIGINT(10) UNSIGNED NULL,
  `address_line1` VARCHAR(250) NULL,
  `wcity` VARCHAR(60) NULL,
  `wstate` VARCHAR(60) NULL,
  `wcountry` VARCHAR(60) NULL,
  `wpincode` VARCHAR(250) NULL,
  `type_address` TINYINT(6) NULL,
  `lastupdated` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` TINYINT(6) NULL,
  PRIMARY KEY (`_id`));

ALTER TABLE `product_org_assoc` 
ADD COLUMN `process_name` VARCHAR(150) NULL AFTER `transport`;

16/03/2017

ALTER TABLE `user_account` 
ADD COLUMN `full_name` VARCHAR(250) NULL AFTER `_id`;


20/03/2017

ALTER TABLE `customer` 
ADD COLUMN `status` TINYINT(5) NULL AFTER `service_location`;

21/03/2017

ALTER TABLE `purchase_service_order` 
DROP INDEX `unique_serial_no_UNIQUE` ;

ALTER TABLE `supplier_org_assoc` 
CHANGE COLUMN `lst` `lst` VARCHAR(50) NULL ,
CHANGE COLUMN `cst` `cst` VARCHAR(50) NULL ,
CHANGE COLUMN `pan` `pan` VARCHAR(50) NULL ,
CHANGE COLUMN `ecc` `ecc` VARCHAR(50) NULL ;

ALTER TABLE `customer` 
CHANGE COLUMN `address` `address` VARCHAR(250) NULL DEFAULT NULL ,
CHANGE COLUMN `city` `city` VARCHAR(250) NULL DEFAULT NULL ,
CHANGE COLUMN `state` `state` VARCHAR(250) NULL DEFAULT NULL ,
CHANGE COLUMN `pincode` `pincode` VARCHAR(50) NULL DEFAULT NULL ,
CHANGE COLUMN `country` `country` VARCHAR(200) NULL DEFAULT NULL ,
CHANGE COLUMN `TIN` `TIN` VARCHAR(50) NULL DEFAULT NULL ,
CHANGE COLUMN `area_code` `area_code` VARCHAR(50) NULL DEFAULT NULL ,
CHANGE COLUMN `PAN` `PAN` VARCHAR(50) NULL DEFAULT NULL ,
CHANGE COLUMN `CST` `CST` VARCHAR(50) NULL DEFAULT NULL ,
CHANGE COLUMN `ECC` `ECC` VARCHAR(50) NULL DEFAULT NULL ;

ALTER TABLE `customer_contact_person` 
CHANGE COLUMN `phone` `phone` VARCHAR(50) NULL DEFAULT NULL ,
CHANGE COLUMN `cell_no` `cell_no` VARCHAR(50) NULL DEFAULT NULL ;

ALTER TABLE `hr_employeedetails` 
CHANGE COLUMN `contact_no` `contact_no` VARCHAR(50) NULL DEFAULT NULL ,
CHANGE COLUMN `emergency_contact` `emergency_contact` VARCHAR(50) NULL DEFAULT NULL ,
CHANGE COLUMN `id_proof` `id_proof` VARCHAR(50) NULL DEFAULT NULL ;

22/03/2017

ALTER TABLE `po_so_items_with_grn` 
CHANGE COLUMN `rate` `rate` DECIMAL(10,2) NULL DEFAULT NULL ,
CHANGE COLUMN `total_cost` `total_cost` DECIMAL(10,5) NULL DEFAULT NULL ;

ALTER TABLE `po_so_items` 
CHANGE COLUMN `rate` `rate` DECIMAL(10,2) NULL DEFAULT NULL ,
CHANGE COLUMN `total_cost` `total_cost` DECIMAL(10,5) NULL DEFAULT NULL ;

ALTER TABLE `item_rates` 
CHANGE COLUMN `rate` `rate` DECIMAL(10,2) NOT NULL ;

ALTER TABLE `item_stock` 
CHANGE COLUMN `rate` `rate` DECIMAL(10,2) NULL DEFAULT NULL ;

------- Executed till here:Caslino-------------

ALTER TABLE `product_org_assoc` 
ADD COLUMN `product_type` VARCHAR(250) NULL DEFAULT NULL AFTER `subgroup`;

ALTER TABLE `product_org_assoc` 
CHANGE COLUMN `lead_time` `lead_time` INT(11) NULL DEFAULT NULL ;


23/03/2017

ALTER TABLE `purchase_service_order` 
CHANGE COLUMN `discount_percentage` `discount_percentage` VARCHAR(50) NULL DEFAULT NULL ,
CHANGE COLUMN `excise_duty` `excise_duty` VARCHAR(50) NULL DEFAULT NULL ,
CHANGE COLUMN `vat` `vat` VARCHAR(50) NULL DEFAULT NULL ,
CHANGE COLUMN `freight_cost` `freight_cost` VARCHAR(50) NULL DEFAULT NULL ,
CHANGE COLUMN `item_total` `item_total` VARCHAR(50) NULL DEFAULT NULL ,
CHANGE COLUMN `other_total` `other_total` VARCHAR(50) NULL DEFAULT NULL ,
CHANGE COLUMN `final_discount_percentage` `final_discount_percentage` VARCHAR(50) NULL DEFAULT NULL ;

ALTER TABLE `sales_order` 
CHANGE COLUMN `excise_charges` `excise_charges` VARCHAR(50) NULL DEFAULT NULL ,
CHANGE COLUMN `transport_charges` `transport_charges` VARCHAR(50) NULL DEFAULT NULL ;

ALTER TABLE `sales_order_items` 
CHANGE COLUMN `item_name` `item_unique_code_fkid` BIGINT(10) UNSIGNED NULL DEFAULT NULL ,
ADD COLUMN `_orgid` BIGINT(10) UNSIGNED NULL AFTER `delivery_date`;

ALTER TABLE `sales_order` 
ADD COLUMN `total_quantity` VARCHAR(50) NULL AFTER `_orgid`,
ADD COLUMN `total_price` VARCHAR(50) NULL AFTER `total_quantity`,
ADD COLUMN `total_discount` VARCHAR(50) NULL AFTER `total_price`,
ADD COLUMN `total_sales_price` VARCHAR(50) NULL AFTER `total_discount`;

24/03/2017

ALTER TABLE `sales_order_items` 
ADD COLUMN `status` TINYINT(6) NULL AFTER `_orgid`;

25/03/2017

ALTER TABLE `item_stock` 
CHANGE COLUMN `end_date` `end_date` DATE NULL AFTER `quantity`,
CHANGE COLUMN `start_date` `start_date` DATE NULL ;

27/03/2017

CREATE TABLE `so_items_used` (
  `_id` BIGINT(10) UNSIGNED NOT NULL,
  `_orgid` BIGINT(10) UNSIGNED NULL,
  `purchase_service_order_fkid` BIGINT(10) UNSIGNED NULL,
  `item_org_assoc_fkid` BIGINT(10) UNSIGNED NULL,
  PRIMARY KEY (`_id`));

ALTER TABLE `so_items_used` 
ADD COLUMN `quantity_used` DECIMAL(10,5) NULL AFTER `item_org_assoc_fkid`;

ALTER TABLE `so_items_used` 
ADD COLUMN `status` TINYINT(6) NULL AFTER `quantity_used`;

29/03/2017

ALTER TABLE `so_items_used` 
CHANGE COLUMN `_id` `_id` BIGINT(10) UNSIGNED NOT NULL AUTO_INCREMENT ;

31/03/2017
ALTER TABLE `sales_order_items` 
CHANGE COLUMN `discount` `discount` VARCHAR(100) NULL DEFAULT NULL ;

03/04/2017

ALTER TABLE `sales_order` 
CHANGE COLUMN `customer_name` `customer_fkid` BIGINT(10) UNSIGNED NULL DEFAULT NULL ;

ALTER TABLE `sales_order_items` 
CHANGE COLUMN `item_unique_code_fkid` `product_org_assoc_fkid` BIGINT(10) UNSIGNED NULL DEFAULT NULL ;

04/04/2017

ALTER TABLE `purchase_service_order` 
ADD COLUMN `status` TINYINT(6) UNSIGNED NULL AFTER `final_total`;

CREATE TABLE `sales_order_status` (
  `_id` BIGINT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `_orgid` BIGINT(10) UNSIGNED NULL,
  `sales_order_fkid` BIGINT(10) UNSIGNED NULL,
  `actedon_by_fkid` BIGINT(10) UNSIGNED NULL,
  `date_actedon` TIMESTAMP NULL,
  `status` TINYINT(6) NULL,
  `notes` VARCHAR(250) NULL,
  PRIMARY KEY (`_id`));

ALTER TABLE `sales_order` 
ADD COLUMN `status` TINYINT(6) NULL AFTER `total_sales_price`;


07/04/2017

CREATE TABLE `production_queue` (
  `_id` BIGINT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `_orgid` BIGINT(10) UNSIGNED NULL,
  `org_product_assoc_fkid` BIGINT(10) UNSIGNED NULL,
  `qty` DECIMAL(10,5) NULL,
  `uom` VARCHAR(250) NULL,
  `sales_order_fkid` BIGINT(10) UNSIGNED NULL,
  `prod_order_fkid` BIGINT(10) UNSIGNED NULL,
  `status` TINYINT(6) NULL,
  `last_updated_date` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `notes` VARCHAR(250) NULL,
  PRIMARY KEY (`_id`));

CREATE TABLE `production_queue_status` (
  `_id` BIGINT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `_orgid` BIGINT(10) UNSIGNED NULL,
  `production_queue_fkid` BIGINT(10) UNSIGNED NULL,
  `status` TINYINT(6) NULL,
  `latest` TINYINT(6) NULL,
  `date` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`_id`));

ALTER TABLE `production_queue_status` 
CHANGE COLUMN `date` `updated_date` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ,
ADD COLUMN `updated_by_user_account_fkid` BIGINT(10) UNSIGNED NULL AFTER `updated_date`;

ALTER TABLE `production_queue` 
ADD COLUMN `added_by_user_account_fkid` BIGINT(10) UNSIGNED NULL AFTER `notes`;

CREATE TABLE `production_orders` (
  `_id` BIGINT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `_orgid` BIGINT(10) UNSIGNED NULL,
  `order_no` BIGINT(10) NULL,
  `org_product_assoc_fkid` BIGINT(10) UNSIGNED NULL,
  `qty` DECIMAL(10,5) NULL,
  `uom` VARCHAR(250) NULL,
  `last_updated_date` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `added_by_user_account_fkid` BIGINT(10) UNSIGNED NULL,
  `notes` VARCHAR(250) NULL,
  `status` TINYINT(6) NULL,
  PRIMARY KEY (`_id`));

CREATE TABLE `production_order_status` (
  `_id` BIGINT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `_orgid` BIGINT(10) UNSIGNED NULL,
  `production_orders_fkid` BIGINT(10) UNSIGNED NULL,
  `status` TINYINT(6) NULL,
  `latest` TINYINT(6) NULL,
  `updated_date` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by_user_account_fkid` BIGINT(10) UNSIGNED NULL,
  PRIMARY KEY (`_id`));

ALTER TABLE `production_orders` 
ADD COLUMN `start_date` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP AFTER `uom`,
ADD COLUMN `end_date` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP AFTER `start_date`;


10/04/2017

ALTER TABLE `production_orders` 
ADD COLUMN `order_date` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP AFTER `order_no`;

ALTER TABLE `po_so_items` 
ADD COLUMN `received_quantity` DECIMAL(10,5) NULL AFTER `quantity`,
ADD COLUMN `rejected_quantity` DECIMAL(10,5) NULL DEFAULT NULL AFTER `received_quantity`;

ALTER TABLE `po_so_items` 
CHANGE COLUMN `received_quantity` `received_quantity` DECIMAL(10,5) NULL DEFAULT 0 ,
CHANGE COLUMN `rejected_quantity` `rejected_quantity` DECIMAL(10,5) NULL DEFAULT 0 ;

11/04/2017

ALTER TABLE `production_queue` 
ADD COLUMN `type` TINYINT(6) NULL DEFAULT NULL AFTER `status`;

12/04/2017

ALTER TABLE `production_orders` 
CHANGE COLUMN `order_no` `order_no` VARCHAR(250) NULL DEFAULT NULL ;

17/04/2017

ALTER TABLE `product_bom` 
ADD COLUMN `wastage` DECIMAL(10,5) NULL DEFAULT 0.0 AFTER `item_org_assoc_fkid`;

ALTER TABLE `supplier_org_assoc` 
ADD COLUMN `provisional_gst` VARCHAR(50) NULL AFTER `status`,
ADD COLUMN `ARN` VARCHAR(50) NULL AFTER `provisional_gst`;

ALTER TABLE `customer` 
ADD COLUMN `gst` VARCHAR(50) NULL AFTER `status`,
ADD COLUMN `provisional_gst` VARCHAR(50) NULL AFTER `gst`,
ADD COLUMN `ARN` VARCHAR(50) NULL AFTER `provisional_gst`;

18/04/2017

ALTER TABLE `product_bom` 
CHANGE COLUMN `quantity` `quantity` DECIMAL(10,5) NOT NULL ,
ADD COLUMN `_orgid` BIGINT(10) UNSIGNED NOT NULL AFTER `status`;

-- Added by Caslino --

CREATE TABLE `reports_master` (
  `pkid` BIGINT(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(500) NOT NULL,
  `unique_code` VARCHAR(250) NOT NULL,
  `_orgid` BIGINT(5) UNSIGNED NOT NULL,
  `type` VARCHAR(250) NOT NULL,
  `created_on` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` BIGINT(10) NULL,
  `status` TINYINT(5) NOT NULL DEFAULT 1,
  PRIMARY KEY (`pkid`));

  CREATE TABLE `reports_fields` (
  `pkid` BIGINT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `_orgid` BIGINT(10) UNSIGNED NOT NULL,
  `reports_master_fkid` BIGINT(10) UNSIGNED NOT NULL,
  `field_name` VARCHAR(500) NOT NULL,
  `field_type` TINYINT(5) NOT NULL,
  `listing_order` TINYINT(30) NOT NULL,
  `status` TINYINT(5) NOT NULL,
  PRIMARY KEY (`pkid`));

CREATE TABLE `reports_created` (
  `pkid` BIGINT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `_orgid` BIGINT(10) UNSIGNED NOT NULL,
  `reports_master_fkid` BIGINT(10) UNSIGNED NOT NULL,
  `unique_code` VARCHAR(500) NOT NULL,
  `created_on` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` BIGINT(10) UNSIGNED NOT NULL,
  `status` TINYINT(5) NOT NULL DEFAULT 1,
  PRIMARY KEY (`pkid`));

CREATE TABLE `reports_datastore` (
  `pkid` BIGINT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `_orgid` BIGINT(10) UNSIGNED NOT NULL,
  `reports_created_fkid` BIGINT(10) UNSIGNED NOT NULL,
  `field_name` VARCHAR(500) NOT NULL,
  `field_value` VARCHAR(500) NOT NULL,
  `status` TINYINT(5) NOT NULL DEFAULT 1,
  PRIMARY KEY (`pkid`));


-- executed on local and test -- 

-- Added on 25 April 2017 --


ALTER TABLE `production_orders` 
ADD COLUMN `machine_name` VARCHAR(250) NULL AFTER `status`;

ALTER TABLE `sales_order` 
CHANGE COLUMN `total_sales_price` `total_sales_price` DECIMAL(10,2) NULL DEFAULT NULL ;

ALTER TABLE `purchase_service_order` 
CHANGE COLUMN `final_total` `final_total` DECIMAL(10,2) NULL DEFAULT NULL ;

26/04/2017

ALTER TABLE `sales_order` 
DROP COLUMN `repeat_order`,
CHANGE COLUMN `total_sales_price` `total_sales_price` DECIMAL(10,3) NULL DEFAULT NULL ,
ADD COLUMN `vat` DECIMAL(10,3) NULL AFTER `status`,
ADD COLUMN `cst` DECIMAL(10,3) NULL AFTER `vat`,
ADD COLUMN `total_after_discount` DECIMAL(10,3) NULL AFTER `cst`,
ADD COLUMN `total_after_excise` DECIMAL(10,3) NULL AFTER `total_after_discount`,
ADD COLUMN `total_price_after_taxes` DECIMAL(10,3) NULL AFTER `total_after_excise`;

ALTER TABLE `production_orders` 
CHANGE COLUMN `order_no` `batch_no` VARCHAR(250) NULL DEFAULT NULL ;

27/04/2017

ALTER TABLE `user_roles` 
RENAME TO  `user_roles_allocation` ;



ALTER TABLE `roles_master` 
CHANGE COLUMN `_id` `roles_master_pkid` BIGINT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
CHANGE COLUMN `page` `functionality` TINYTEXT NULL DEFAULT NULL ;

INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('1', 'settings', '1', 'add-user');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('2', 'settings', '2', 'view-users');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('3', 'settings', '1', 'edit-configuration');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('4', 'settings', '2', 'view-configuration');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('5', 'stock', '1', 'add-stock-items');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('6', 'stock', '2', 'view-stock-items');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('7', 'stock', '1', 'add-inventory');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('8', 'stock', '2', 'view-inventory');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('9', 'stock', '1', 'raise-grn');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('10', 'stock', '2', 'view-grn');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('11', 'suppliers', '1', 'add-suppliers');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('12', 'suppliers', '2', 'view-suppliers');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('13', 'suppliers', '1', 'add-supplier-associations');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('14', 'suppliers', '2', 'view-supplier-associations');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('15', 'purchases', '1', 'create-po');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('16', 'purchases', '2', 'view-po');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('17', 'customers', '1', 'add-customers');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('18', 'customers', '2', 'view-customers');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('19', 'customers', '1', 'add-customer-association');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('20', 'customers', '2', 'view-customer-associations');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('21', 'sales', '1', 'create-sales-orders');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('22', 'sales', '2', 'view-sales-orders');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('23', 'production', '1', 'create-production-orders');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('24', 'production', '2', 'view-production-orders');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('25', 'production', '1', 'create-products');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('26', 'production', '2', 'view-products');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('27', 'production', '1', 'create-process');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('28', 'production', '2', 'view-process');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('29', 'statistics', '1', 'create-statistics-master');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('30', 'statistics', '2', 'view-statistics-master');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('31', 'reports', '1', 'create-report-master');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('32', 'reports', '2', 'view-report-master');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('33', 'reports', '1', 'create-reports');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('34', 'reports', '2', 'view-reports');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('35', 'hr', '1', 'add-employee-details');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('36', 'hr', '2', 'view-employee-details');

02/05/2017

CREATE TABLE `prefix_master` (
  `item_code` VARCHAR(45) NULL,
  `product_code` VARCHAR(45) NULL,
  `supplier_code` VARCHAR(45) NULL,
  `customer_code` VARCHAR(45) NULL,
  `purchase_order_code` VARCHAR(45) NULL,
  `sales_order_code` VARCHAR(45) NULL,
  `employee_id_code` VARCHAR(45) NULL,
  `machine_code` VARCHAR(45) NULL,
  `production_order_code` VARCHAR(45) NULL,
  `customer_ticket_code` VARCHAR(45) NULL);

CREATE TABLE `prefix_company_values` (
  `pkid` BIGINT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `_orgid` BIGINT(10) UNSIGNED NOT NULL,
  `prefix_master_fkid` BIGINT(10) UNSIGNED NOT NULL,
  `value` TINYINT(10) NULL,
  `added_on` TIMESTAMP NULL,
  `added_by_fkid` BIGINT(10) NULL,
  `status` TINYINT(10) NULL,
  PRIMARY KEY (`pkid`),
  UNIQUE INDEX `pkid_UNIQUE` (`pkid` ASC));

ALTER TABLE `prefix_master` 
ADD COLUMN `_id` BIGINT(10) UNSIGNED NOT NULL AUTO_INCREMENT FIRST,
ADD PRIMARY KEY (`_id`);

03/05/2017

ALTER TABLE `item_total_inventory` 
ADD COLUMN `low_level` VARCHAR(20) NULL AFTER `status`;

05/05/2017

ALTER TABLE `prefix_company_values` 
CHANGE COLUMN `value` `value` VARCHAR(50) NULL DEFAULT NULL ;

ALTER TABLE `prefix_master` 
DROP COLUMN `customer_ticket_code`,
DROP COLUMN `production_order_code`,
DROP COLUMN `machine_code`,
DROP COLUMN `employee_id_code`,
DROP COLUMN `sales_order_code`,
DROP COLUMN `purchase_order_code`,
DROP COLUMN `customer_code`,
DROP COLUMN `supplier_code`,
DROP COLUMN `product_code`,
CHANGE COLUMN `item_code` `prefix_codes` VARCHAR(45) NULL DEFAULT NULL ;

INSERT INTO `prefix_master` (`_id`, `prefix_codes`) VALUES ('1', 'item_code');
INSERT INTO `prefix_master` (`_id`, `prefix_codes`) VALUES ('2', 'product_code');
INSERT INTO `prefix_master` (`_id`, `prefix_codes`) VALUES ('3', 'supplier_code');
INSERT INTO `prefix_master` (`_id`, `prefix_codes`) VALUES ('4', 'customer_code');
INSERT INTO `prefix_master` (`_id`, `prefix_codes`) VALUES ('5', 'purchase_order_code');
INSERT INTO `prefix_master` (`_id`, `prefix_codes`) VALUES ('6', 'sales_order_code');
INSERT INTO `prefix_master` (`_id`, `prefix_codes`) VALUES ('7', 'employee_id_code');
INSERT INTO `prefix_master` (`_id`, `prefix_codes`) VALUES ('8', 'machine_code');
INSERT INTO `prefix_master` (`_id`, `prefix_codes`) VALUES ('9', 'production_order_code');
INSERT INTO `prefix_master` (`_id`, `prefix_codes`) VALUES ('10', 'customer_ticket_code');

/*Added on 8 May 2017*/
ALTER TABLE `item_org_assoc` CHANGE COLUMN `invoice_narration` `invoice_narration` VARCHAR(500) NULL DEFAULT NULL ;
ALTER TABLE `item_org_assoc` 
CHANGE COLUMN `item_unique_code` `item_unique_code` VARCHAR(200) NOT NULL ,
CHANGE COLUMN `grp` `grp` VARCHAR(100) NULL DEFAULT NULL ,
CHANGE COLUMN `sub_group` `sub_group` VARCHAR(100) NULL DEFAULT NULL ,
CHANGE COLUMN `grade` `grade` VARCHAR(100) NULL DEFAULT NULL ,
CHANGE COLUMN `unit_measure` `unit_measure` VARCHAR(100) NULL DEFAULT NULL ,
CHANGE COLUMN `geo_location` `geo_location` VARCHAR(200) NULL DEFAULT NULL ;
/*Executed on test*/

14/05/2017

CREATE TABLE `machine` (
  `_pkid` BIGINT(6) UNSIGNED NOT NULL AUTO_INCREMENT,
  `machine_code` VARCHAR(250) NULL,
  `machine_name` VARCHAR(250) NULL,
  `geo_location` VARCHAR(100) NULL,
  `added_on` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` TINYINT(6) UNSIGNED NULL,
  PRIMARY KEY (`_pkid`));

ALTER TABLE `machine` 
ADD COLUMN `_orgid` BIGINT(6) UNSIGNED NULL AFTER `status`;

16/05/2017

INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('37', 'settings', '1', 'manage-roles');
INSERT INTO `roles_master` (`roles_master_pkid`, `module`, `role_type`, `functionality`) VALUES ('38', 'settings', '2', 'view-roles');

ALTER TABLE `org_supplier_item_assoc` 
ADD COLUMN `invoice_naration` VARCHAR(250) NULL AFTER `last_updated`;

17/05/2017

ALTER TABLE `customer_product_assoc` 
ADD COLUMN `invoice_naration` VARCHAR(250) NULL AFTER `_orgid`;

ALTER TABLE `po_so_items` 
ADD COLUMN `packet_size` DECIMAL(10,5) NULL AFTER `status`;

ALTER TABLE `purchase_service_order` 
ADD COLUMN `delayed_status` TINYINT(5) NULL DEFAULT 0 AFTER `status`;

22/05/2017

ALTER TABLE `production_orders` 
ADD COLUMN `geo_location` BIGINT(6) UNSIGNED NULL AFTER `machine_name`;

24/05/2017

ALTER TABLE `customer` 
CHANGE COLUMN `date_of_introduction` `date_of_introduction` TIMESTAMP NULL DEFAULT NULL ;

25/05/2017

ALTER TABLE `supplier_address` 
CHANGE COLUMN `type_address` `type_address` TINYINT(5) NULL COMMENT '1 - HQ\n2 - Warehouse\n3 - Office' ;

ALTER TABLE `supplier_org_assoc` 
CHANGE COLUMN `category` `category` VARCHAR(20) NULL ;

ALTER TABLE `customer` 
CHANGE COLUMN `company_name` `company_name` VARCHAR(500) NOT NULL ,
CHANGE COLUMN `grp` `grp` VARCHAR(50) NULL DEFAULT NULL ,
CHANGE COLUMN `address` `address` TEXT(50) NULL DEFAULT NULL ,
CHANGE COLUMN `country` `country` VARCHAR(250) NULL DEFAULT NULL ,
CHANGE COLUMN `service_location` `service_location` VARCHAR(50) NULL DEFAULT NULL ;

ALTER TABLE `product_org_assoc` 
CHANGE COLUMN `product_org_name` `product_org_name` VARCHAR(500) NOT NULL ,
CHANGE COLUMN `invoice_narration` `invoice_narration` VARCHAR(500) NOT NULL ,
CHANGE COLUMN `rate_per_UOM` `rate_per_UOM` DECIMAL(10,5) NULL ;

ALTER TABLE `product_org_assoc` 
CHANGE COLUMN `product_org_name` `product_org_name` VARCHAR(500) NOT NULL ,
CHANGE COLUMN `invoice_narration` `invoice_narration` VARCHAR(500) NOT NULL ,
CHANGE COLUMN `rate_per_UOM` `rate_per_UOM` DECIMAL(10,5) NULL ;

05/06/2017

ALTER TABLE `supplier_org_assoc` 
ADD COLUMN `date_of_introduction` DATETIME NULL AFTER `ARN`;

ALTER TABLE `supplier_org_assoc` 
CHANGE COLUMN `lst` `tin` VARCHAR(50) NULL DEFAULT NULL ;

ALTER TABLE `product_org_assoc` 
ADD COLUMN `geo_location` VARCHAR(200) NULL AFTER `process_name`;

/* Executed till here on live and test */

07/06/2017


ALTER TABLE `purchase_service_order` 
ADD COLUMN `subtotal` DECIMAL(10,2) NULL AFTER `delayed_status`;

ALTER TABLE `sales_order` 
ADD COLUMN `tarrif_no` TEXT(10) NULL AFTER `total_price_after_taxes`;

ALTER TABLE `sales_order` 
CHANGE COLUMN `tarrif_no` `tariff_no` TINYTEXT NULL DEFAULT NULL ;


/* Product to Item migration scripts */

ALTER TABLE `product_bom` 
CHANGE COLUMN `product_org_assoc_fkid` `item_product_org_assoc_fkid` BIGINT(10) NULL DEFAULT NULL ;

ALTER TABLE `item_org_assoc` 
CHANGE COLUMN `raw_material_or_product` `produced_internally` TINYINT(6) NULL DEFAULT 0 ,
ADD COLUMN `production_status` TINYINT(6) NULL DEFAULT 0 AFTER `produced_internally`;

ALTER TABLE `product_bom` 
ADD COLUMN `product_narration` VARCHAR(250) NULL AFTER `_orgid`,
ADD COLUMN `product_code` VARCHAR(250) NULL AFTER `product_narration`;

ALTER TABLE `customer_product_assoc` 
CHANGE COLUMN `_product_master_fkid` `item_org_assoc_fkid` BIGINT(10) UNSIGNED NULL DEFAULT NULL ,
ADD COLUMN `sales_price` DECIMAL(10,2) NULL AFTER `invoice_naration`;

-- 08/06/2017

CREATE TABLE `data_master` (
  `pkid` BIGINT(10) UNSIGNED NOT NULL,
  `_orgid` BIGINT(10) NULL,
  `type` TINYINT(6) NULL,
  `content` TEXT(10) NULL,
  `date_added` DATETIME NULL,
  `added_by_fkid` BIGINT(10) UNSIGNED NULL,
  `status` TINYINT(6) NULL,
  PRIMARY KEY (`pkid`),
  UNIQUE INDEX `pkid_UNIQUE` (`pkid` ASC));

ALTER TABLE `data_master` 
CHANGE COLUMN `pkid` `pkid` BIGINT(10) UNSIGNED NOT NULL AUTO_INCREMENT ;

-- 09/06/2017

ALTER TABLE `sales_order_items` 
CHANGE COLUMN `product_org_assoc_fkid` `item_org_assoc_fkid` BIGINT(10) UNSIGNED NULL DEFAULT NULL ;

ALTER TABLE `production_orders` 
CHANGE COLUMN `org_product_assoc_fkid` `item_org_assoc_fkid` BIGINT(10) UNSIGNED NULL DEFAULT NULL ;



/* 13 June 2017  */

ALTER TABLE `item_org_assoc` 
ADD COLUMN `non_consumable` TINYINT(6) NULL DEFAULT 0 COMMENT '0 - No (default) \n1 - Yes ' AFTER `production_status`;

ALTER TABLE `product_bom` 
ADD COLUMN `non_consumable` TINYINT(6) NULL DEFAULT 0 COMMENT '0 - No (default) \n1 - Yes ';

CREATE TABLE `prefix_dropdown_master` (
  `pkid` BIGINT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `dropdown` VARCHAR(100) NULL,
  `display_text` VARCHAR(250) NULL,
  `status` TINYINT(6) NULL DEFAULT 1,
  PRIMARY KEY (`pkid`));

INSERT INTO `prefix_dropdown_master` (`pkid`, `dropdown`, `display_text`) VALUES ('1', 'current_year', 'Current Year');
INSERT INTO `prefix_dropdown_master` (`pkid`, `dropdown`, `display_text`) VALUES ('2', 'current_financial_year_india', 'Currnent Financial Year (April-March)');
INSERT INTO `prefix_dropdown_master` (`pkid`, `dropdown`, `display_text`) VALUES ('3', 'current_date', 'Current Date');
INSERT INTO `prefix_dropdown_master` (`pkid`, `dropdown`, `display_text`) VALUES ('4', 'current_time', 'Current Time');
INSERT INTO `prefix_dropdown_master` (`pkid`, `dropdown`, `display_text`) VALUES ('5', 'alphabet_seq', 'Cyclic Alphabet Sequence (A-Z)');
INSERT INTO `prefix_dropdown_master` (`pkid`, `dropdown`, `display_text`) VALUES ('6', 'number_seq', 'Cyclic Number Sequence (0-9)');
INSERT INTO `prefix_dropdown_master` (`pkid`, `dropdown`) VALUES ('7', 'static_alphabet-numeric');


CREATE TABLE `prefix_org_choice` (
  `pkid` BIGINT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `_orgid` BIGINT(10) UNSIGNED NOT NULL,
  `prefix_master_fkid` BIGINT(10) UNSIGNED NOT NULL,
  `prefix_dropdown_master_fkid` BIGINT(10) UNSIGNED NOT NULL,
  `position` VARCHAR(45) NULL,
  `value` VARCHAR(45) NULL,
  `reset_sequence` VARCHAR(45) NULL,
  `added_on` TIMESTAMP NULL,
  `added_by_fkid` BIGINT(10) UNSIGNED NOT NULL,
  `status` TINYINT(6) NULL DEFAULT 1,
  PRIMARY KEY (`pkid`));

ALTER TABLE `prefix_org_choice` 
CHANGE COLUMN `added_on` `added_on` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ;

DROP TABLE `prefix_master`;
DROP TABLE `prefix_org_values`;

CREATE TABLE `prefix_master` (
  `pkid` BIGINT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(145) NULL,
  PRIMARY KEY (`pkid`));

INSERT INTO `prefix_master` (`pkid`, `name`) VALUES ('1', 'item_code');
INSERT INTO `prefix_master` (`pkid`, `name`) VALUES ('2', 'product_code');
INSERT INTO `prefix_master` (`pkid`, `name`) VALUES ('3', 'supplier_code');
INSERT INTO `prefix_master` (`pkid`, `name`) VALUES ('4', 'customer_code');
INSERT INTO `prefix_master` (`pkid`, `name`) VALUES ('5', 'purchase_order_code');
INSERT INTO `prefix_master` (`pkid`, `name`) VALUES ('6', 'sales_order_code');
INSERT INTO `prefix_master` (`pkid`, `name`) VALUES ('7', 'employee_id_code');
INSERT INTO `prefix_master` (`pkid`, `name`) VALUES ('8', 'machine_code');
INSERT INTO `prefix_master` (`pkid`, `name`) VALUES ('9', 'production_order_code');
INSERT INTO `prefix_master` (`pkid`, `name`) VALUES ('10', 'customer_ticket_code');

UPDATE `prefix_dropdown_master` SET  `status`='0' WHERE `pkid`='7';

ALTER TABLE `prefix_org_choice` 
CHANGE COLUMN `prefix_dropdown_master_fkid` `prefix_dropdown_master_fkid` VARCHAR(100) NULL DEFAULT NULL ;

-- 13/06/2017

ALTER TABLE `item_total_inventory` 
CHANGE COLUMN `total_quantity` `total_quantity` DECIMAL(10,3) NULL DEFAULT NULL ;

ALTER TABLE `product_bom` 
ADD COLUMN `non_consumable` TINYINT(6) NULL DEFAULT 0 AFTER `product_code`;

ALTER TABLE `item_org_assoc` 
ADD COLUMN `non_consumable` TINYINT(6) NULL DEFAULT 0 AFTER `production_status`;


-- 14 June 2017

ALTER TABLE `item_org_assoc` 
CHANGE COLUMN `lead_time` `lead_time` DECIMAL(10,2) NULL DEFAULT NULL ;


-- 19 June 2017

ALTER TABLE `industry_type` 
DROP COLUMN `industry_class`,
DROP COLUMN `industry_group`,
ADD COLUMN `industry_type_master_fkid` BIGINT(10) UNSIGNED NULL AFTER `_id`,
ADD COLUMN `status` TINYINT(10) NULL DEFAULT 1 AFTER `_orgid`;

CREATE TABLE `industry_type_master` (
  `pkid` BIGINT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(500) NOT NULL,
  `group` VARCHAR(100) NULL,
  `classification` VARCHAR(100) NULL,
  `status` TINYINT(10) NULL DEFAULT 1,
  PRIMARY KEY (`pkid`));

INSERT INTO `industry_type_master` (`name`, `status`) VALUES ('Rotational Molding', '1');
INSERT INTO `industry_type_master` (`name`, `status`) VALUES ('Corrugated Box', '1');
INSERT INTO `industry_type_master` (`name`, `status`) VALUES ('Printing Ink', '1');
INSERT INTO `industry_type_master` (`name`, `status`) VALUES ('Paints', '1');
INSERT INTO `industry_type_master` (`name`, `status`) VALUES ('Injection Molding', '1');
INSERT INTO `industry_type_master` (`name`, `status`) VALUES ('CNC Metal Works', '1');

-- 22 June 2017
CREATE TABLE `session` (
 `session_key` char(64) NOT NULL,
 `session_data` blob,
 `session_expiry` int(11) unsigned NOT NULL,
 PRIMARY KEY (`session_key`)
 ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- 28 June 2017
CREATE TABLE `used_sequence_codes` (
  `pkid` BIGINT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `_orgid` BIGINT(10) UNSIGNED NULL,
  `sequence` BIGINT(10) UNSIGNED NULL,
  `prefix_master_fkid` VARCHAR(45) NULL,
  `generated_on` VARCHAR(45) NULL,
  `status` TINYINT(10) NULL DEFAULT 0,
  PRIMARY KEY (`pkid`));
-- 28 June 2017

ALTER TABLE `sales_order` 
ADD COLUMN `octroi_charges` VARCHAR(50) NULL DEFAULT NULL AFTER `excise_charges`,
ADD COLUMN `total_after_octroi` DECIMAL(10,3) NULL DEFAULT NULL AFTER `total_after_excise`;

-- 29 June 2017

ALTER TABLE `customer_warehouse_address` 
ADD COLUMN `GSTIN` VARCHAR(250) NULL DEFAULT NULL AFTER `type_address`;

ALTER TABLE `supplier_address` 
ADD COLUMN `GSTIN` VARCHAR(250) NULL DEFAULT NULL AFTER `type_address`;

-- 30 June 2017

ALTER TABLE `item_org_assoc` 
ADD COLUMN `gst_rate` DECIMAL(10,3) NULL DEFAULT NULL AFTER `non_consumable`,
ADD COLUMN `cess_rate` DECIMAL(10,3) NULL DEFAULT NULL AFTER `gst_rate`,
ADD COLUMN `hsn_sac` VARCHAR(100) NULL DEFAULT NULL AFTER `cess_rate`;

-- 01 June 2017

ALTER TABLE `organisation` 
ADD COLUMN `GSTIN` VARCHAR(250) NULL DEFAULT NULL AFTER `verification_status`,
ADD COLUMN `pan` VARCHAR(50) NULL DEFAULT NULL AFTER `GSTIN`,
ADD COLUMN `bank_name` VARCHAR(250) NULL DEFAULT NULL AFTER `pan`,
ADD COLUMN `account_no` VARCHAR(100) NULL DEFAULT NULL AFTER `bank_name`,
ADD COLUMN `ifsc_code` VARCHAR(100) NULL DEFAULT NULL AFTER `account_no`;

-- 10 July 2017

ALTER TABLE `sales_order` 
ADD COLUMN `total_taxable_value` DECIMAL(10,3) NULL DEFAULT NULL,
ADD COLUMN `total_tax_cess` DECIMAL(10,3) NULL DEFAULT NULL AFTER `total_taxable_value`,
ADD COLUMN `total_freight_packaging_others` DECIMAL(10,3) NULL DEFAULT NULL AFTER `total_tax_cess`;

ALTER TABLE `sales_order_items` 
CHANGE COLUMN `quantity` `quantity` DECIMAL(10,3) NULL DEFAULT NULL ,
CHANGE COLUMN `uom` `uom` VARCHAR(100) NULL DEFAULT NULL ,
CHANGE COLUMN `price` `price` DECIMAL(10,3) NULL DEFAULT NULL ,
CHANGE COLUMN `discount` `discount_value` DECIMAL(10,3) NULL DEFAULT NULL ,
ADD COLUMN `hsn_sac` VARCHAR(100) NULL DEFAULT NULL AFTER `uom`,
ADD COLUMN `discount_rate` DECIMAL(10,3) NULL DEFAULT NULL AFTER `price`,
ADD COLUMN `taxable_amount` DECIMAL(10,3) NULL DEFAULT NULL AFTER `delivery_date`,
ADD COLUMN `gst_rate` DECIMAL(10,3) NULL DEFAULT NULL AFTER `taxable_amount`,
ADD COLUMN `igst_rate` DECIMAL(10,3) NULL DEFAULT NULL AFTER `gst_rate`,
ADD COLUMN `igst_amount` DECIMAL(10,3) NULL DEFAULT NULL AFTER `igst_rate`,
ADD COLUMN `cgst_rate` DECIMAL(10,3) NULL DEFAULT NULL AFTER `igst_amount`,
ADD COLUMN `cgst_amount` DECIMAL(10,3) NULL DEFAULT NULL AFTER `cgst_rate`,
ADD COLUMN `sgst_rate` DECIMAL(10,3) NULL DEFAULT NULL AFTER `cgst_amount`,
ADD COLUMN `sgst_amount` DECIMAL(10,3) NULL DEFAULT NULL AFTER `sgst_rate`,
ADD COLUMN `cess_rate` DECIMAL(10,3) NULL DEFAULT NULL AFTER `sgst_amount`,
ADD COLUMN `cess_amount` DECIMAL(10,3) NULL DEFAULT NULL AFTER `cess_rate`,
ADD COLUMN `total_tax_cess` DECIMAL(10,3) NULL DEFAULT NULL AFTER `cess_amount`,
ADD COLUMN `freight` DECIMAL(10,3) NULL DEFAULT NULL AFTER `total_tax_cess`,
ADD COLUMN `insurance` DECIMAL(10,3) NULL DEFAULT NULL AFTER `freight`,
ADD COLUMN `packaging_forwarding` DECIMAL(10,3) NULL DEFAULT NULL AFTER `insurance`,
ADD COLUMN `others` DECIMAL(10,3) NULL DEFAULT NULL AFTER `packaging_forwarding`,
ADD COLUMN `total_other_charges` DECIMAL(10,3) NULL DEFAULT NULL AFTER `others`;

ALTER TABLE `po_so_items` 
ADD COLUMN `uom` VARCHAR(100) NULL DEFAULT NULL AFTER `item_org_assoc_fkid`,
ADD COLUMN `hsn_sac` VARCHAR(100) NULL DEFAULT NULL AFTER `uom`,
ADD COLUMN `discount_rate` DECIMAL(10,3) NULL DEFAULT NULL AFTER `rate`,
ADD COLUMN `discount_value` DECIMAL(10,3) NULL DEFAULT NULL AFTER `discount_rate`,
ADD COLUMN `delivery_date` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP AFTER `discount_value`,
ADD COLUMN `taxable_amount` DECIMAL(10,3) NULL DEFAULT NULL AFTER `delivery_date`,
ADD COLUMN `gst_rate` DECIMAL(10,3) NULL DEFAULT NULL AFTER `taxable_amount`,
ADD COLUMN `igst_rate` DECIMAL(10,3) NULL DEFAULT NULL AFTER `gst_rate`,
ADD COLUMN `igst_amount` DECIMAL(10,3) NULL DEFAULT NULL AFTER `igst_rate`,
ADD COLUMN `cgst_rate` DECIMAL(10,3) NULL DEFAULT NULL AFTER `igst_amount`,
ADD COLUMN `cgst_amount` DECIMAL(10,3) NULL DEFAULT NULL AFTER `cgst_rate`,
ADD COLUMN `sgst_rate` DECIMAL(10,3) NULL DEFAULT NULL AFTER `cgst_amount`,
ADD COLUMN `sgst_amount` DECIMAL(10,3) NULL DEFAULT NULL AFTER `sgst_rate`,
ADD COLUMN `cess_rate` DECIMAL(10,3) NULL DEFAULT NULL AFTER `sgst_amount`,
ADD COLUMN `cess_amount` DECIMAL(10,3) NULL DEFAULT NULL AFTER `cess_rate`,
ADD COLUMN `total_tax_cess` DECIMAL(10,3) NULL DEFAULT NULL AFTER `cess_amount`,
ADD COLUMN `freight` DECIMAL(10,3) NULL DEFAULT NULL AFTER `total_tax_cess`,
ADD COLUMN `insurance` DECIMAL(10,3) NULL DEFAULT NULL AFTER `freight`,
ADD COLUMN `packaging_forwarding` DECIMAL(10,3) NULL DEFAULT NULL AFTER `insurance`,
ADD COLUMN `others` DECIMAL(10,3) NULL DEFAULT NULL AFTER `packaging_forwarding`,
ADD COLUMN `total_other_charges` DECIMAL(10,3) NULL DEFAULT NULL AFTER `others`;

ALTER TABLE `purchase_service_order` 
ADD COLUMN `total_taxable_value` DECIMAL(10,3) NULL DEFAULT NULL AFTER `subtotal`,
ADD COLUMN `total_tax_cess` DECIMAL(10,3) NULL DEFAULT NULL AFTER `total_taxable_value`,
ADD COLUMN `total_freight_packaging_others` DECIMAL(10,3) NULL DEFAULT NULL AFTER `total_tax_cess`,
ADD COLUMN `total_after_taxes` DECIMAL(10,3) NULL DEFAULT NULL AFTER `total_freight_packaging_others`,
ADD COLUMN `total_invoice_value` DECIMAL(10,3) NULL DEFAULT NULL AFTER `total_after_taxes`;

-- 12 July 2017

ALTER TABLE `po_so_items` 
ADD COLUMN `total_after_tax` DECIMAL(10,3) NULL DEFAULT NULL AFTER `total_tax_cess`;

ALTER TABLE `sales_order` 
ADD COLUMN `total_after_tax` DECIMAL(10,3) NULL DEFAULT NULL AFTER `total_tax_cess`;

-- 14 July 2017

ALTER TABLE `sales_order` 
DROP COLUMN `total_after_tax`;

ALTER TABLE `sales_order_items` 
ADD COLUMN `total_after_tax` DECIMAL(10,3) NULL DEFAULT NULL AFTER `total_tax_cess`;

-- 17 July 2017

ALTER TABLE `sales_order` 
ADD COLUMN `delivery_location` BIGINT(10) UNSIGNED NULL DEFAULT NULL AFTER `delivery_date`;

ALTER TABLE `sales_order` 
ADD COLUMN `billing_address` BIGINT(10) UNSIGNED NULL DEFAULT NULL AFTER `tariff_no`;

-- 19 July 2017

ALTER TABLE `customer` 
ADD COLUMN `GSTIN` VARCHAR(250) NULL DEFAULT NULL AFTER `ARN`;

ALTER TABLE `supplier_org_assoc` 
ADD COLUMN `GSTIN` VARCHAR(250) NULL DEFAULT NULL AFTER `date_of_introduction`;

-- 26 July 2017

ALTER TABLE `po_so_items_with_grn` 
CHANGE COLUMN `rate` `rate` DECIMAL(10,2) NULL DEFAULT NULL ,
CHANGE COLUMN `total_cost` `total_cost` DECIMAL(10,2) NULL DEFAULT NULL ;

-- 27 July 2017

ALTER TABLE `po_so_supplier_invoice` 
CHANGE COLUMN `grn_no` `grn_no` VARCHAR(100) NULL DEFAULT NULL ,
CHANGE COLUMN `invoice_no` `invoice_no` VARCHAR(100) NULL DEFAULT NULL ,
CHANGE COLUMN `dc_no` `dc_no` VARCHAR(100) NULL DEFAULT NULL ;

-- 29 August 2017

ALTER TABLE `customer` 
ADD COLUMN `customer_unique_code` VARCHAR(150) NOT NULL AFTER `_orgid`;

CREATE TABLE `item_stock_usage` (
  `_id` BIGINT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `_orgid` BIGINT(10) UNSIGNED NOT NULL,
  `item_org_assoc_fkid` BIGINT(10) UNSIGNED NULL,
  `item_stock_fkid` BIGINT(10) UNSIGNED NULL,
  `quantity_used` DECIMAL(10,5) NOT NULL,
  `used_for` TINYINT(6) NULL,
  `used_table_fkid` BIGINT(10) UNSIGNED NULL,
  `used_table_unique_code` VARCHAR(150) NOT NULL,
  `used_on` DATETIME NULL,
  `lastupdated` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` TINYINT(6) NULL,
  PRIMARY KEY (`_id`));

  -- 31 August 2017

  ALTER TABLE `supplier_org_assoc` 
CHANGE COLUMN `company_name` `company_name` VARCHAR(500) NOT NULL ;

-- 1 September 2017

ALTER TABLE `purchase_service_order` 
CHANGE COLUMN `billing_address` `billing_address` TEXT(50) NULL DEFAULT NULL ;


-- 12 September 2017

-- added to singulerptest
ALTER TABLE `sales_order` 
ADD COLUMN `origin_location` BIGINT(10) UNSIGNED NULL DEFAULT NULL AFTER `delivery_location`;

-- 18 September 2017

-- added to singulerptest
ALTER TABLE `supplier_contact_person`                                    
ADD COLUMN `contact_type` TINYINT(10) NULL DEFAULT NULL AFTER `last_updated`;

-- added to singulerptest
ALTER TABLE `supplier_contact_person`                                       
CHANGE COLUMN `supplier_address_fkid` `supplier_address_fkid` BIGINT(10) UNSIGNED NULL DEFAULT NULL ;

-- 27 September 2017

-- added to singulerptest
--- ran by Joyston
ALTER TABLE `item_org_assoc`                                   
ADD COLUMN `shelf_life` DECIMAL(10,2) NULL DEFAULT NULL AFTER `hsn_sac`;

-- added to singulerptest
ALTER TABLE `customer_support`                       
ADD COLUMN `_orgid` BIGINT(10) UNSIGNED NOT NULL AFTER `_id`;

-- added to singulerptest
ALTER TABLE `customer_support` 
CHANGE COLUMN `_sales_order_fkid` `_sales_order_fkid` VARCHAR(150) NOT NULL ,
CHANGE COLUMN `invoice_no` `invoice_no` VARCHAR(150) NULL DEFAULT NULL ;


-- 03 October 2017

-- added by joyston
ALTER TABLE `supplier_org_assoc` 
ADD COLUMN `credit_in_days` INT(11) NULL DEFAULT NULL AFTER `cst`;

ALTER TABLE `supplier_org_assoc` 
ADD COLUMN `credit_limit` INT(11) NULL DEFAULT NULL AFTER `credit_in_days`;
-- added to singulerptest & singulerplive


-- 06 October 2017

-- added by joyston
ALTER TABLE `sales_order` 
ADD COLUMN `tax_regime` TINYINT(6) NULL DEFAULT NULL AFTER `origin_location`;
-- added to singulerptest
 

-- 9 October 2017

CREATE TABLE `Packaging` (
  `PK_id` bigint(10) NOT NULL AUTO_INCREMENT,
  `item_org_assoc_fkid` bigint(10) DEFAULT NULL,
  `Packet_Size` decimal(10,0) DEFAULT NULL,
  `Price` decimal(10,0) DEFAULT NULL,
  `Status` varchar(45) DEFAULT NULL,
  `date_added` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_removed` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `_orgid` bigint(10) DEFAULT NULL,
  PRIMARY KEY (`PK_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1

-- Updated on singulerptest 
-- 13 October 2017

-- added by Pranav
DROP TABLE `Packaging`;
-- added to singulerptest
-- ran by joyston

-- added by Pranav
CREATE TABLE `packaging` (
  `pk_id` BIGINT(10) NOT NULL AUTO_INCREMENT,
  `item_org_assoc_fkid` BIGINT(10) NULL DEFAULT NULL,
  `packet_size` DECIMAL(10,2) NULL DEFAULT NULL,
  `price` DECIMAL(10,3) NULL DEFAULT NULL,
  `status` TINYINT(1) NULL DEFAULT NULL,
  `date_added` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `date_removed` TIMESTAMP NULL DEFAULT NULL,
  `_orgid` BIGINT(10) NULL DEFAULT NULL,
  PRIMARY KEY (`pk_id`));
  -- added to singulerptest
-- ran by joyston

-- added by joyston
ALTER TABLE `singulerptest`.`purchase_service_order` 
ADD COLUMN `tax_regime` TINYINT(6) NULL DEFAULT NULL AFTER `delivery_date`;
-- added to singulerp & singulerptest

-- added by joyston
ALTER TABLE `organisation_factories` 
CHANGE COLUMN `added_on` `added_on` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ,
ADD COLUMN `last_updated` TIMESTAMP NULL DEFAULT NULL AFTER `status`;
-- Updated singulerptest

-- 12 October
ALTER TABLE `po_so_items_with_grn` 
CHANGE COLUMN `rejected_note` `rejected_note` TEXT(10) NULL DEFAULT NULL ;
-- added to singulerptest
-- ran by joyston

-- 12 October 2017

-- added by joyston
ALTER TABLE `purchase_service_order` 
ADD COLUMN `tax_regime` TINYINT(6) NULL DEFAULT NULL AFTER `delivery_date`;

-- add by vanisha on 13th Oct 2017
ALTER TABLE `po_so_items_with_grn` 
CHANGE COLUMN `purchase_service_order_fkid` `purchase_service_order_fkid` BIGINT(10) UNSIGNED NULL ;
-- added to singulerptest
-- ran by joyston

ALTER TABLE `item_rates` 
CHANGE COLUMN `supplier_org_assoc_fkid` `supplier_org_assoc_fkid` BIGINT(10) UNSIGNED NULL ;
-- added to singulerptest
-- ran by joyston

-- Executed till here on both live and test on 16 October 2017. Add new scripts below.

-- added by vanisha on 16th oct 2017
CREATE TABLE `sales_order_items_inventory` (
  `_id` BIGINT(10) NOT NULL AUTO_INCREMENT,
  `sales_order_fkid` BIGINT(10) NOT NULL,
  `item_org_assoc_fkid` BIGINT(10) NOT NULL,
  `item_stock_fkid` BIGINT(10) NULL,
  `quantity` DECIMAL(10,5) NULL,
  `status` TINYINT(6) NULL,
  `_orgid` BIGINT(10) NOT NULL,
  PRIMARY KEY (`_id`));
  -- added to singulerptest
  -- ran by joyston

-- added by joyston on 23rd oct 2017
ALTER TABLE `machine` 
ADD COLUMN `machine_status` BIGINT(6) UNSIGNED NULL DEFAULT NULL AFTER `_orgid`;
-- added to singulerptest

-- added by joyston on 24th oct 2017
ALTER TABLE `machine` 
CHANGE COLUMN `machine_status` `machine_status` BIGINT(6) UNSIGNED NULL DEFAULT 1 ;
-- added to singulerptest

-- added by vanisha on 24th oct 2017
ALTER TABLE `item_stock` 
ADD COLUMN `batch_no` VARCHAR(30) NULL AFTER `items_stock_adjustment_fkid`;
-- added to singulerptest
-- ran by joyston

-- added by vanisha on 24th/25th oct 2017
INSERT INTO `prefix_master` (`pkid`, `name`) VALUES ('11', 'grn_code');

ALTER TABLE`prefix_org_choice` 
CHANGE COLUMN `position` `position` TINYINT(1) UNSIGNED NULL DEFAULT NULL ,
ADD COLUMN `direction` VARCHAR(1) NULL AFTER `status`;


ALTER TABLE `purchase_service_order` 
CHANGE COLUMN `unique_serial_no` `unique_serial_no` BIGINT(10) NULL DEFAULT NULL ,
ADD COLUMN `purchase_order_code_gen` VARCHAR(45) NULL AFTER `total_invoice_value`;
-- added to singulerptest
-- ran by joyston
ALTER TABLE `item_org_assoc` 
ADD COLUMN `item_code_gen` VARCHAR(45) NULL AFTER `shelf_life`;


-- added by joyston in 29th oct 2017
ALTER TABLE `item_stock` 
ADD COLUMN `production_order_fkid` BIGINT(10) UNSIGNED NULL DEFAULT NULL AFTER `po_so_items_with_grn_fkid`;
-- added to singulerptest


-- added by vanisha on 31st oct 2017

ALTER TABLE `sales_order` 
ADD COLUMN `last_updated_date` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP AFTER `total_freight_packaging_others`;

ALTER TABLE `supplier_org_assoc` 
ADD COLUMN `supplier_code_gen` VARCHAR(45) NULL AFTER `GSTIN`;

ALTER TABLE `customer` 
ADD COLUMN `customer_code_gen` VARCHAR(45) NULL AFTER `GSTIN`;

ALTER TABLE `sales_order` 
ADD COLUMN `sales_order_code_gen` VARCHAR(45) NULL AFTER `last_updated_date`;

ALTER TABLE `machine` 
ADD COLUMN `machine_code_gen` VARCHAR(45) NULL AFTER `machine_status`;

ALTER TABLE `hr_employeedetails` 
ADD COLUMN `employee_code_gen` VARCHAR(45) NULL AFTER `employee_id`;

ALTER TABLE `customer_support` 
ADD COLUMN `support_ticket_code_gen` VARCHAR(45) NULL AFTER `_orgid`;


-- added by vanisha on 6th Nov 2017
ALTER TABLE `item_org_assoc` 
DROP COLUMN `item_code_gen`;

ALTER TABLE singulerp.item_org_assoc
CHANGE COLUMN item_unique_code item_code_gen varchar(45)

ALTER TABLE `item_org_assoc` 
ADD COLUMN `item_unique_code` BIGINT(10) NULL AFTER `shelf_life`;

-- added by joyston on 6th Nov 2017
ALTER TABLE `supplier_org_assoc` 
DROP COLUMN `supplier_code_gen`;

ALTER TABLE `supplier_org_assoc` 
CHANGE COLUMN `supplier_unique_code` `supplier_code_gen` VARCHAR(150) NOT NULL ;

ALTER TABLE`supplier_org_assoc` 
ADD COLUMN `supplier_unique_code` BIGINT(10) NULL DEFAULT NULL AFTER `GSTIN`;
-- updated singulerptest

-- added by Pranav on 6th November 2017
ALTER TABLE `item_stock_adjustment`
ADD COLUMN `geo_location` VARCHAR(20) NULL DEFAULT NULL AFTER `observed_quantity`;
-- updated singulerptest

-- added by joyston on 8th November 2017
ALTER TABLE `machine` 
DROP COLUMN `machine_code_gen`;

ALTER TABLE `machine` 
CHANGE COLUMN `machine_code` `machine_code_gen` VARCHAR(250) NULL DEFAULT NULL ;

ALTER TABLE `machine` 
ADD COLUMN `machine_code` BIGINT(10) NULL DEFAULT NULL AFTER `machine_status`;
-- updated singulerptest

-- added by vanisha on 8th Nov 2017
ALTER TABLE `production_orders` 
ADD COLUMN `production_order_unique_code` VARCHAR(45) NULL AFTER `geo_location`;

--- added by vanisha on 9th nov 2017
UPDATE item_org_assoc
JOIN (SELECT @rank := 0) r
SET item_unique_code=@rank:=@rank+1 WHere _orgid = <Put the org id>;
-- Updated on singuelerptest

-- added by joyston on 9th Nov 2017
ALTER TABLE `sales_order` 
DROP COLUMN `sales_order_code_gen`;

ALTER TABLE `sales_order` 
CHANGE COLUMN `order_no` `sales_order_code_gen` VARCHAR(150) NULL DEFAULT NULL ;

ALTER TABLE `sales_order` 
ADD COLUMN `order_no` BIGINT(10) NULL DEFAULT NULL AFTER `last_updated_date`;
-- updated singulerptest


-- added by vanisha 10th Nov 2017
UPDATE sales_order
JOIN (SELECT @rank := 0) r
SET order_no =@rank:=@rank+1 WHere _orgid = <Put the org id>;

-- added by joyston on 10th Nov 2017
ALTER TABLE `purchase_service_order` 
DROP COLUMN `purchase_order_code_gen`;

ALTER TABLE `purchase_service_order` 
CHANGE COLUMN `unique_serial_no` `purchase_order_code_gen` VARCHAR(150) NULL DEFAULT NULL ;

ALTER TABLE `purchase_service_order` 
ADD COLUMN `unique_serial_no` BIGINT(10) NULL DEFAULT NULL AFTER `total_invoice_value`;

ALTER TABLE `customer` 
DROP COLUMN `customer_code_gen`;

ALTER TABLE `customer` 
CHANGE COLUMN `customer_unique_code` `customer_code_gen` VARCHAR(150) NOT NULL ;

ALTER TABLE `customer` 
ADD COLUMN `customer_unique_code` BIGINT(10) NULL DEFAULT NULL AFTER `GSTIN`;
-- updated singulerptest

-- added by vanisha on 13th 2017
ALTER TABLE `po_so_items_with_grn` 
CHANGE COLUMN `grn_no` `grn_no` VARCHAR(50) NULL DEFAULT NULL ;

ALTER TABLE `po_so_items_with_grn` 
CHANGE COLUMN `grn_no` `grn_no_gen` VARCHAR(50) NULL DEFAULT NULL ;

ALTER TABLE `po_so_items_with_grn` 
ADD COLUMN `grn_no` BIGINT(10) NULL AFTER `delivered_date`;

UPDATE po_so_items_with_grn
JOIN (SELECT @rank := 0) r
SET grn_no =@rank:=@rank+1 WHere _orgid = <Put the org id>;

-- updated singulerptest


ALTER TABLE `po_so_items_with_grn` 
ADD COLUMN `batch_no` VARCHAR(45) NULL AFTER `grn_no`;

ALTER TABLE `item_stock` 
ADD COLUMN `grn_no` VARCHAR(45) NULL AFTER `batch_no`;


--- added by vanisha on 14th nov 2017
ALTER TABLE `production_orders` 
CHANGE COLUMN `production_order_unique_code` `production_order_unique_code` BIGINT(10) NULL DEFAULT NULL ;

UPDATE supplier_org_assoc
JOIN (SELECT @rank := 0) r
SET supplier_unique_code =@rank:=@rank+1 WHere _orgid = 5605772627595960429;

UPDATE customer
JOIN (SELECT @rank := 0) r
SET customer_unique_code =@rank:=@rank+1 WHere _orgid = 5605772627595960429;

UPDATE purchase_service_order
JOIN (SELECT @rank := 0) r
SET unique_serial_no =@rank:=@rank+1 WHere _orgid = 5605772627595960429;

UPDATE sales_order
JOIN (SELECT @rank := 0) r
SET order_no =@rank:=@rank+1 WHere _orgid = 5605772627595960429;

ALTER TABLE `hr_employeedetails` 
DROP COLUMN `employee_code_gen`;

ALTER TABLE `hr_employeedetails` 
CHANGE COLUMN `employee_id` `employee_code_gen` VARCHAR(150) NOT NULL ;

ALTER TABLE `hr_employeedetails` 
ADD COLUMN `employee_id` BIGINT(10) NULL DEFAULT NULL AFTER `_orgid`;

UPDATE hr_employeedetails
JOIN (SELECT @rank := 0) r
SET employee_id =@rank:=@rank+1 WHere _orgid = 5605772627595960429;

UPDATE machine
JOIN (SELECT @rank := 0) r
SET machine_code =@rank:=@rank+1 WHere _orgid = 5605772627595960429;

UPDATE production_orders
JOIN (SELECT @rank := 0) r
SET production_order_unique_code =@rank:=@rank+1 WHere _orgid = 5605772627595960429;

ALTER TABLE `customer_support` 
DROP COLUMN `support_ticket_code_gen`;

ALTER TABLE `customer_support` 
CHANGE COLUMN `ticket_no` `support_ticket_code_gen` VARCHAR(150) NOT NULL ;

ALTER TABLE `customer_support` 
ADD COLUMN `ticket_no` BIGINT(10) NULL DEFAULT NULL AFTER `_orgid`;

UPDATE customer_support
JOIN (SELECT @rank := 0) r
SET ticket_no =@rank:=@rank+1 WHere _orgid = 5605772627595960429;

-- Updated singulerptest

CREATE TABLE `production_order_items` (
  `_id` BIGINT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `production_order_fkid` BIGINT(10) NULL DEFAULT NULL,
  `item_org_assoc_fkid` BIGINT(10) UNSIGNED NULL DEFAULT NULL,
  `quantity` DECIMAL(10,5) NULL DEFAULT NULL,
  `uom` VARCHAR(250) NULL DEFAULT NULL,
  `geo_location` BIGINT(6) NULL DEFAULT NULL,
  `_orgid` BIGINT(10) UNSIGNED NULL DEFAULT NULL,
  `status` TINYINT(6) NULL DEFAULT 1,
  `bom_status` TINYINT(6) NULL DEFAULT 0,
  PRIMARY KEY (`_id`));

-- added by Joyston on 17th nov 2017
ALTER TABLE `production_order_items` 
ADD COLUMN `wastage` DECIMAL(10,5) NULL AFTER `quantity`;
-- added to singulerptest

ALTER TABLE `item_stock` 
ADD COLUMN `blocked` TINYINT(6) NULL DEFAULT 0 AFTER `grn_no`,
ADD COLUMN `insert_status` TINYINT(6) NULL DEFAULT 0 AFTER `blocked`,
ADD COLUMN `insert_table_fkid` BIGINT(10) NULL AFTER `insert_status`,
ADD COLUMN `consumed_table_fkid` BIGINT(10) NULL AFTER `insert_table_fkid`;


ALTER TABLE `item_stock` 
DROP COLUMN `items_stock_adjustment_fkid`,
DROP COLUMN `production_order_fkid`,
DROP COLUMN `po_so_items_with_grn_fkid`;


ALTER TABLE `item_stock` 
CHANGE COLUMN `insert_table_fkid` `insert_table_fkid` BIGINT(10) UNSIGNED NULL DEFAULT NULL ;
ALTER TABLE `item_stock` 
CHANGE COLUMN `consumed_table_fkid` `consumed_table_fkid` BIGINT(10) UNSIGNED NULL DEFAULT NULL ;

ALTER TABLE `item_stock` 
CHANGE COLUMN `latest` `latest` VARCHAR(20) NULL DEFAULT '1' ;

ALTER TABLE `item_stock` 
CHANGE COLUMN `expires_by` `expires_by` DATE NULL DEFAULT NULL ;

ALTER TABLE `item_stock` 
ADD COLUMN `consumed_qty` DECIMAL(10,5) NULL AFTER `consumed_table_fkid`;

ALTER TABLE `item_stock` 
CHANGE COLUMN `expires_by` `expires_by` TIMESTAMP NULL DEFAULT NULL ,
CHANGE COLUMN `end_date` `end_date` TIMESTAMP NULL DEFAULT NULL ,
CHANGE COLUMN `start_date` `start_date` TIMESTAMP NULL DEFAULT NULL ;

ALTER TABLE `item_stock_adjustment` 
ADD COLUMN `item_org_assoc_fkid` BIGINT(10) UNSIGNED NULL AFTER `geo_location`;

ALTER TABLE `item_stock_adjustment` 
ADD COLUMN `adjusted` TINYINT(6)  NULL AFTER `item_org_assoc_fkid`;

ALTER TABLE `item_stock_adjustment` 
CHANGE COLUMN `adjusted` `adjusted` TINYINT(6) NULL DEFAULT 0 ;

ALTER TABLE `item_total_inventory` 
CHANGE COLUMN `status` `status` TINYINT(6) NULL DEFAULT 1 ,
CHANGE COLUMN `low_level` `low_level` DECIMAL(10,3) NULL DEFAULT NULL ;

-- 20th Nov 2017
ALTER TABLE `po_so_supplier_invoice` 
DROP COLUMN `purchase_service_order_fkid`,
DROP COLUMN `grn_no`,
DROP COLUMN `po_so_items_with_grn_list`,
ADD COLUMN `status` TINYINT(6) NULL DEFAULT 1 AFTER `supplier_org_assoc_fkid`,
ADD COLUMN `date_added` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP AFTER `status`;


ALTER TABLE `po_so_items_with_grn` 
ADD COLUMN `po_so_supplier_invoice_fkid` BIGINT(10) NULL DEFAULT NULL AFTER `batch_no`;

-- executed till here by vanisha on test db

-- added by vanisha on 21st nov
ALTER TABLE `po_so_items_with_grn` 
ADD COLUMN `batch_no_seq` BIGINT(10) NULL AFTER `po_so_supplier_invoice_fkid`;

-- ran by joyston till here

-- added by joyston on 22nd nov
ALTER TABLE `customer` 
DROP COLUMN `gst`;
-- added to singulerptest

-- added by vanisha on 28th Nov 2017
ALTER TABLE `production_orders` 
ADD COLUMN `temporary_bom_used` TINYINT(6) NULL DEFAULT 0,
ADD COLUMN `manually_assign_batch` TINYINT(6) NULL DEFAULT 0 AFTER `temporary_bom_used`;

-- added to test db

-- Executed till here on both live and test on 28 November 2017. Add new scripts below.
-- added by vanisha on 30th november 2017
ALTER TABLE `sales_order` 
ADD COLUMN `manually_assign_batch` TINYINT(6) NULL DEFAULT 0 AFTER `order_no`;

ALTER TABLE `item_stock_adjustment` 
ADD COLUMN `grn_code_gen_fkid` VARCHAR(50) NULL AFTER `adjusted`,
ADD COLUMN `batch_no_fkid` VARCHAR(45) NULL AFTER `grn_code_gen_fkid`;
-- added to test db

-- added by joyston on 1st December 2017
ALTER TABLE `sales_order_items` 
ADD COLUMN `geo_location` BIGINT(6) NULL DEFAULT NULL AFTER `uom`;


ALTER TABLE `po_so_items_with_grn` 
ADD COLUMN `geo_location` BIGINT(6) NULL AFTER `po_so_items_fkid`;
-- added to test db


-- added to live DB till here

-- added by joyston on 11th December 2017
ALTER TABLE `po_so_items_with_grn` 
ADD COLUMN `lr_date` TIMESTAMP NULL DEFAULT NULL AFTER `batch_no`,
ADD COLUMN `lr_number` BIGINT(10) NULL AFTER `lr_date`;

ALTER TABLE `production_orders` 
ADD COLUMN `only_code` TINYINT(6) NULL DEFAULT 1 AFTER `production_order_unique_code`;
-- added to singulerptest
-- ran by joyston till here

-- added by vanisha on 11th Dec 2017
INSERT INTO `prefix_dropdown_master` (`dropdown`, `display_text`, `status`) VALUES ('current_month', 'Current Month', '1');
INSERT INTO `prefix_dropdown_master` (`dropdown`, `display_text`, `status`) VALUES ('current_day', 'Current Day', '1');

-- added by vanisha on 13th Dec 2017
ALTER TABLE `po_so_items_with_grn` 
CHANGE COLUMN `lr_number` `lr_number` VARCHAR(250) NULL DEFAULT NULL ;

ALTER TABLE `product_bom` 
DROP COLUMN `product_code`;
-- added to singulerptest

-- added by joyston on 13th December 2017
CREATE TABLE `transporter` (
  `_id` BIGINT(10) UNSIGNED NOT NULL,
  `_orgid` BIGINT(10) UNSIGNED NOT NULL,
  `transporter_code_gen` VARCHAR(150) NOT NULL,
  `transporter_unique_code` BIGINT(10) NULL DEFAULT NULL,
  `transporter_name` VARCHAR(500) NOT NULL,
  `main_contact_person` VARCHAR(500) NULL DEFAULT NULL,
  `telephone` VARCHAR(150) NULL DEFAULT NULL,
  `mobile` VARCHAR(150) NULL DEFAULT NULL,
  `fax` VARCHAR(150) NULL DEFAULT NULL,
  `email` VARCHAR(150) NULL DEFAULT NULL,
  `hq` VARCHAR(500) NULL DEFAULT NULL,
  `city` VARCHAR(250) NULL DEFAULT NULL,
  `state` VARCHAR(250) NULL DEFAULT NULL,
  `country` VARCHAR(250) NULL DEFAULT NULL,
  `pincode` VARCHAR(250) NULL DEFAULT NULL,
  `GSTIN` VARCHAR(250) NULL DEFAULT NULL,
  `PAN` VARCHAR(250) NULL DEFAULT NULL,
  `introduction_date` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `notes` VARCHAR(500) NULL DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE INDEX `_id_UNIQUE` (`_id` ASC));

CREATE TABLE `warehouse` (
  `_id` BIGINT(10) UNSIGNED NOT NULL,
  `_orgid` BIGINT(10) UNSIGNED NOT NULL,
  `billing_address` VARCHAR(500) NOT NULL,
  `city` VARCHAR(250) NULL DEFAULT NULL,
  `state` VARCHAR(250) NULL DEFAULT NULL,
  `country` VARCHAR(250) NULL DEFAULT NULL,
  `pincode` VARCHAR(250) NULL DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE INDEX `_id_UNIQUE` (`_id` ASC));

CREATE TABLE `contact` (
  `_id` BIGINT(10) UNSIGNED NOT NULL,
  `_orgid` BIGINT(10) UNSIGNED NOT NULL,
  `contact_person_name` VARCHAR(500) NOT NULL,
  `designation` VARCHAR(250) NULL DEFAULT NULL,
  `mobile` VARCHAR(250) NULL DEFAULT NULL,
  `phone` VARCHAR(250) NULL DEFAULT NULL,
  `email` VARCHAR(250) NULL DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE INDEX `_id_UNIQUE` (`_id` ASC));

  CREATE TABLE `vehicle` (
  `_id` BIGINT(10) UNSIGNED NOT NULL,
  `_orgid` BIGINT(10) UNSIGNED NOT NULL,
  `driver_name` VARCHAR(500) NOT NULL,
  `mobile` VARCHAR(250) NULL DEFAULT NULL,
  `email` VARCHAR(250) NULL DEFAULT NULL,
  `model_type` VARCHAR(250) NULL DEFAULT NULL,
  `registration_number` VARCHAR(45) NULL DEFAULT NULL,
  `storage_volume` VARCHAR(250) NULL DEFAULT NULL,
  `weight_capacity` VARCHAR(250) NULL DEFAULT NULL,
  `axel_weight` VARCHAR(250) NULL,
  PRIMARY KEY (`_id`),
  UNIQUE INDEX `_id_UNIQUE` (`_id` ASC));
-- added to singulerptest

-- added by joyston on 14th December 2017
ALTER TABLE `warehouse` 
ADD COLUMN `transporter_fkid` BIGINT(10) UNSIGNED NULL AFTER `_orgid`;

ALTER TABLE `vehicle` 
ADD COLUMN `transporter_fkid` BIGINT(10) UNSIGNED NULL AFTER `_orgid`;

ALTER TABLE `contact` 
ADD COLUMN `transporter_fkid` BIGINT(10) UNSIGNED NULL AFTER `_orgid`;

ALTER TABLE `transporter` 
ADD COLUMN `status` TINYINT(6) NULL AFTER `notes`;
-- added to singulerptest

-- added by joyston on 15th December 2017
ALTER TABLE `sales_order` 
ADD COLUMN `transporter_fkid` BIGINT(10) UNSIGNED NULL DEFAULT NULL AFTER `manually_assign_batch`,
ADD COLUMN `vehicle_fkid` BIGINT(10) UNSIGNED NULL DEFAULT NULL AFTER `transporter_fkid`;

ALTER TABLE `purchase_service_order` 
ADD COLUMN `transporter_fkid` BIGINT(10) UNSIGNED NULL DEFAULT NULL AFTER `unique_serial_no`,
ADD COLUMN `vehicle_fkid` BIGINT(10) UNSIGNED NULL DEFAULT NULL AFTER `transporter_fkid`;

ALTER TABLE `contact` 
ADD COLUMN `warehouse_fkid` BIGINT(10) UNSIGNED NULL AFTER `email`;

ALTER TABLE `sales_order` 
DROP COLUMN `vehicle_fkid`,
DROP COLUMN `transporter_fkid`;

ALTER TABLE `purchase_service_order` 
DROP COLUMN `vehicle_fkid`,
DROP COLUMN `transporter_fkid`;

ALTER TABLE `po_so_items` 
ADD COLUMN `transporter_fkid` BIGINT(10) UNSIGNED NULL DEFAULT NULL AFTER `packet_size`,
ADD COLUMN `vehicle_fkid` BIGINT(10) UNSIGNED NULL DEFAULT NULL AFTER `transporter_fkid`;

ALTER TABLE `sales_order_items` 
ADD COLUMN `transporter_fkid` BIGINT(10) UNSIGNED NULL DEFAULT NULL AFTER `total_other_charges`,
ADD COLUMN `vehicle_fkid` BIGINT(10) UNSIGNED NULL DEFAULT NULL AFTER `transporter_fkid`;
-- added to singulerptest

ALTER TABLE `transporter` 
CHANGE COLUMN `_id` `_id` BIGINT(10) UNSIGNED NOT NULL AUTO_INCREMENT ;

ALTER TABLE `contact` 
CHANGE COLUMN `_id` `_id` BIGINT(10) UNSIGNED NOT NULL AUTO_INCREMENT ;

ALTER TABLE `vehicle` 
CHANGE COLUMN `_id` `_id` BIGINT(10) UNSIGNED NOT NULL AUTO_INCREMENT ;

ALTER TABLE `warehouse` 
CHANGE COLUMN `_id` `_id` BIGINT(10) UNSIGNED NOT NULL AUTO_INCREMENT ;

-- added to singulerptest


ALTER TABLE `contact` 
ADD COLUMN `status` TINYINT(6) NULL DEFAULT 1 AFTER `warehouse_fkid`,
ADD COLUMN `last_updated` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP AFTER `status`;

ALTER TABLE `warehouse` 
ADD COLUMN `status` TINYINT(6) NULL DEFAULT 1 AFTER `pincode`,
ADD COLUMN `last_updated` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP AFTER `status`;

ALTER TABLE `vehicle` 
ADD COLUMN `status` TINYINT(6) NULL DEFAULT 1 AFTER `axel_weight`,
ADD COLUMN `last_updated` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP AFTER `status`;

ALTER TABLE `transporter`
ADD COLUMN `last_updated` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP AFTER `status`;

-- ran by joyston till here

-- added by joyston on 04/01/2018
ALTER TABLE `vehicle` 
ADD COLUMN `driver_license_no` VARCHAR(45) NULL AFTER `driver_name`;
-- added to sigulerptest

-- added by joyston on 15/01/2018
CREATE TABLE `sales_order_delivery` (
  `_id` BIGINT(10) UNSIGNED NOT NULL,
  `_orgid` BIGINT(10) UNSIGNED NOT NULL,
  `sales_order_fkid` BIGINT(10) UNSIGNED NULL DEFAULT NULL,
  `sales_order_items_fkid` BIGINT(10) UNSIGNED NULL DEFAULT NULL,
  `quantity_delivered` DECIMAL(10,5) NULL DEFAULT NULL,
  `date_of_delivery` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `transporter_fkid` BIGINT(10) NULL DEFAULT NULL,
  `vehicle_fkid` BIGINT(10) NULL DEFAULT NULL,
  `notes` VARCHAR(450) NULL,
  `status` TINYINT(5) NULL,
  PRIMARY KEY (`_id`),
  UNIQUE INDEX `_id_UNIQUE` (`_id` ASC));

ALTER TABLE `sales_order_items` 
ADD COLUMN `delivered_quantity` DECIMAL(10,3) NULL DEFAULT NULL AFTER `quantity`;

ALTER TABLE `sales_order_delivery` 
ADD COLUMN `dispatch_no` BIGINT(10) NULL DEFAULT NULL AFTER `status`,
ADD COLUMN `batch_no` VARCHAR(45) NULL DEFAULT NULL AFTER `dispatch_no`,
ADD COLUMN `batch_no_seq` BIGINT(10) NULL DEFAULT NULL AFTER `batch_no`,
ADD COLUMN `dispatch_no_gen` VARCHAR(50) NULL DEFAULT NULL AFTER `batch_no_seq`,
ADD COLUMN `customer_invoice_fkid` BIGINT(10) NULL DEFAULT NULL AFTER `dispatch_no_gen`;

ALTER TABLE `sales_order_delivery` 
CHANGE COLUMN `dispatch_no_gen` `dispatch_no_gen` VARCHAR(50) NULL DEFAULT NULL AFTER `dispatch_no`,
CHANGE COLUMN `notes` `notes` VARCHAR(250) NULL DEFAULT NULL ,
ADD COLUMN `geo_location` BIGINT(6) NULL DEFAULT NULL AFTER `sales_order_items_fkid`,
ADD COLUMN `rate` DECIMAL(10,2) NULL DEFAULT NULL AFTER `geo_location`,
ADD COLUMN `quantity` DECIMAL(10,5) NULL DEFAULT NULL AFTER `rate`,
ADD COLUMN `total_cost` DECIMAL(10,2) NULL DEFAULT NULL AFTER `quantity`,
ADD COLUMN `lr_date` TIMESTAMP NULL DEFAULT NULL AFTER `customer_invoice_fkid`,
ADD COLUMN `lr_number` BIGINT(10) NULL DEFAULT NULL AFTER `lr_date`;

ALTER TABLE `sales_order_delivery` 
CHANGE COLUMN `transporter_fkid` `transporter_fkid` BIGINT(10) NULL DEFAULT NULL AFTER `lr_number`,
CHANGE COLUMN `vehicle_fkid` `vehicle_fkid` BIGINT(10) NULL DEFAULT NULL AFTER `transporter_fkid`,
ADD COLUMN `item_org_assoc_fkid` BIGINT(10) UNSIGNED NOT NULL AFTER `sales_order_fkid`;

ALTER TABLE `sales_order_delivery` 
CHANGE COLUMN `date_of_delivery` `delivered_date` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ;

ALTER TABLE `sales_order_delivery` 
CHANGE COLUMN `_id` `_id` BIGINT(10) UNSIGNED NOT NULL AUTO_INCREMENT ;

ALTER TABLE `purchase_service_order` 
ADD COLUMN `transporter_fkid` BIGINT(10) NULL DEFAULT NULL AFTER `unique_serial_no`,
ADD COLUMN `vehicle_fkid` BIGINT(10) NULL DEFAULT NULL AFTER `transporter_fkid`;

ALTER TABLE `po_so_items_with_grn` 
ADD COLUMN `transporter_fkid` BIGINT(10) NULL DEFAULT NULL AFTER `batch_no_seq`,
ADD COLUMN `vehicle_fkid` BIGINT(10) NULL DEFAULT NULL AFTER `transporter_fkid`;

ALTER TABLE `po_so_items` 
DROP COLUMN `vehicle_fkid`,
DROP COLUMN `transporter_fkid`;

ALTER TABLE `sales_order` 
ADD COLUMN `transporter_fkid` BIGINT(10) NULL DEFAULT NULL,
ADD COLUMN `vehicle_fkid` BIGINT(10) NULL DEFAULT NULL;

ALTER TABLE `sales_order_items` 
DROP COLUMN `vehicle_fkid`,
DROP COLUMN `transporter_fkid`;
-- added to sigulerptest till here

-- added by joyston on 22/01/2018
INSERT INTO `prefix_master` (`pkid`, `name`) VALUES ('12', 'dispatch_code');
-- added to sigulerptest till here

-- -------------executed on live till here ----------------

-- TODO --
ALTER TABLE `po_so_items_with_grn` 
ADD COLUMN `geo_location` BIGINT(6) NULL AFTER `po_so_items_fkid`;

ALTER TABLE `po_so_items_with_grn` 
CHANGE COLUMN `quantity` `quantity` DECIMAL(20,5) NULL DEFAULT NULL ;

ALTER TABLE `po_so_items_with_grn` 
CHANGE COLUMN `total_cost` `total_cost` DECIMAL(20,4) NULL DEFAULT NULL ;

ALTER TABLE `po_so_items_with_grn` 
CHANGE COLUMN `received_sent_quantity` `received_sent_quantity` DECIMAL(20,5) NULL DEFAULT NULL ;

ALTER TABLE `po_so_items_with_grn` 
CHANGE COLUMN `rejected_quantity` `rejected_quantity` DECIMAL(20,5) NULL DEFAULT NULL ;

ALTER TABLE `item_stock_adjustment` 
CHANGE COLUMN `quantity_on_record` `quantity_on_record` DECIMAL(20,5) NULL DEFAULT NULL ,
CHANGE COLUMN `observed_quantity` `observed_quantity` DECIMAL(20,5) NULL DEFAULT NULL ;

ALTER TABLE `item_stock` 
CHANGE COLUMN `quantity` `quantity` DECIMAL(20,5) NULL DEFAULT NULL ,
CHANGE COLUMN `consumed_qty` `consumed_qty` DECIMAL(20,5) NULL DEFAULT NULL ;

ALTER TABLE `purchase_service_order` 
CHANGE COLUMN `subtotal` `subtotal` DECIMAL(20,2) NULL DEFAULT NULL ,
CHANGE COLUMN `total_taxable_value` `total_taxable_value` DECIMAL(20,3) NULL DEFAULT NULL ,
CHANGE COLUMN `total_tax_cess` `total_tax_cess` DECIMAL(20,3) NULL DEFAULT NULL ,
CHANGE COLUMN `total_freight_packaging_others` `total_freight_packaging_others` DECIMAL(20,3) NULL DEFAULT NULL ,
CHANGE COLUMN `total_after_taxes` `total_after_taxes` DECIMAL(20,3) NULL DEFAULT NULL ,
CHANGE COLUMN `total_invoice_value` `total_invoice_value` DECIMAL(20,3) NULL DEFAULT NULL ;

ALTER TABLE `po_so_items` 
CHANGE COLUMN `rate` `rate` DECIMAL(20,2) NULL DEFAULT NULL ,
CHANGE COLUMN `discount_rate` `discount_rate` DECIMAL(20,3) NULL DEFAULT NULL ,
CHANGE COLUMN `discount_value` `discount_value` DECIMAL(20,3) NULL DEFAULT NULL ,
CHANGE COLUMN `taxable_amount` `taxable_amount` DECIMAL(20,3) NULL DEFAULT NULL ,
CHANGE COLUMN `gst_rate` `gst_rate` DECIMAL(20,3) NULL DEFAULT NULL ,
CHANGE COLUMN `igst_rate` `igst_rate` DECIMAL(20,3) NULL DEFAULT NULL ,
CHANGE COLUMN `igst_amount` `igst_amount` DECIMAL(20,3) NULL DEFAULT NULL ,
CHANGE COLUMN `cgst_rate` `cgst_rate` DECIMAL(20,3) NULL DEFAULT NULL ,
CHANGE COLUMN `cgst_amount` `cgst_amount` DECIMAL(20,3) NULL DEFAULT NULL ,
CHANGE COLUMN `sgst_rate` `sgst_rate` DECIMAL(20,3) NULL DEFAULT NULL ,
CHANGE COLUMN `sgst_amount` `sgst_amount` DECIMAL(20,3) NULL DEFAULT NULL ,
CHANGE COLUMN `cess_rate` `cess_rate` DECIMAL(20,3) NULL DEFAULT NULL ,
CHANGE COLUMN `cess_amount` `cess_amount` DECIMAL(20,3) NULL DEFAULT NULL ,
CHANGE COLUMN `total_tax_cess` `total_tax_cess` DECIMAL(20,3) NULL DEFAULT NULL ,
CHANGE COLUMN `total_after_tax` `total_after_tax` DECIMAL(20,3) NULL DEFAULT NULL ,
CHANGE COLUMN `freight` `freight` DECIMAL(20,3) NULL DEFAULT NULL ,
CHANGE COLUMN `insurance` `insurance` DECIMAL(20,3) NULL DEFAULT NULL ,
CHANGE COLUMN `packaging_forwarding` `packaging_forwarding` DECIMAL(20,3) NULL DEFAULT NULL ,
CHANGE COLUMN `others` `others` DECIMAL(20,3) NULL DEFAULT NULL ,
CHANGE COLUMN `total_other_charges` `total_other_charges` DECIMAL(20,3) NULL DEFAULT NULL ,
CHANGE COLUMN `quantity` `quantity` DECIMAL(20,5) NULL DEFAULT NULL ,
CHANGE COLUMN `received_quantity` `received_quantity` DECIMAL(20,5) NULL DEFAULT '0.00000' ,
CHANGE COLUMN `rejected_quantity` `rejected_quantity` DECIMAL(20,5) NULL DEFAULT '0.00000' ,
CHANGE COLUMN `total_cost` `total_cost` DECIMAL(20,5) NULL DEFAULT NULL ,
CHANGE COLUMN `packet_size` `packet_size` DECIMAL(20,5) NULL DEFAULT NULL ;

ALTER TABLE `po_so_items_with_grn` 
CHANGE COLUMN `rate` `rate` DECIMAL(20,2) NULL DEFAULT NULL ;

ALTER TABLE `sales_order` 
ADD COLUMN `manually_assign_batch` TINYINT(6) NULL DEFAULT 0 AFTER `order_no`;

ALTER TABLE `item_stock_adjustment` 
ADD COLUMN `grn_code_gen_fkid` VARCHAR(50) NULL AFTER `adjusted`,
ADD COLUMN `batch_no_fkid` VARCHAR(45) NULL AFTER `grn_code_gen_fkid`;

ALTER TABLE `sales_order_items` 
ADD COLUMN `geo_location` BIGINT(6) NULL DEFAULT NULL AFTER `uom`;

INSERT INTO `prefix_dropdown_master` (`dropdown`, `display_text`, `status`) VALUES ('current_month', 'Current Month', '1');
INSERT INTO `prefix_dropdown_master` (`dropdown`, `display_text`, `status`) VALUES ('current_day', 'Current Day', '1');

ALTER TABLE `sales_order` 
DROP COLUMN `vehicle_fkid`,
DROP COLUMN `transporter_fkid`;

ALTER TABLE `purchase_service_order` 
DROP COLUMN `vehicle_fkid`,
DROP COLUMN `transporter_fkid`;

ALTER TABLE `po_so_items` 
ADD COLUMN `transporter_fkid` BIGINT(10) UNSIGNED NULL DEFAULT NULL AFTER `packet_size`,
ADD COLUMN `vehicle_fkid` BIGINT(10) UNSIGNED NULL DEFAULT NULL AFTER `transporter_fkid`;

ALTER TABLE `sales_order_items` 
ADD COLUMN `transporter_fkid` BIGINT(10) UNSIGNED NULL DEFAULT NULL AFTER `total_other_charges`,
ADD COLUMN `vehicle_fkid` BIGINT(10) UNSIGNED NULL DEFAULT NULL AFTER `transporter_fkid`;

ALTER TABLE `transporter` 
CHANGE COLUMN `_id` `_id` BIGINT(10) UNSIGNED NOT NULL AUTO_INCREMENT ;

ALTER TABLE `contact` 
CHANGE COLUMN `_id` `_id` BIGINT(10) UNSIGNED NOT NULL AUTO_INCREMENT ;

ALTER TABLE `vehicle` 
CHANGE COLUMN `_id` `_id` BIGINT(10) UNSIGNED NOT NULL AUTO_INCREMENT ;

ALTER TABLE `warehouse` 
CHANGE COLUMN `_id` `_id` BIGINT(10) UNSIGNED NOT NULL AUTO_INCREMENT ;

ALTER TABLE `purchase_service_order` 
ADD COLUMN `transporter_fkid` BIGINT(10) NULL DEFAULT NULL AFTER `unique_serial_no`,
ADD COLUMN `vehicle_fkid` BIGINT(10) NULL DEFAULT NULL AFTER `transporter_fkid`;

ALTER TABLE `sales_order_items` 
DROP COLUMN `vehicle_fkid`,
DROP COLUMN `transporter_fkid`;

INSERT INTO `prefix_master` (`pkid`, `name`) VALUES ('12', 'dispatch_code');


--- executed on live thill here. 1 May 2018 ----

ALTER TABLE `transporter` 
CHANGE COLUMN `introduction_date` `introduction_date` TIMESTAMP NULL DEFAULT NULL ;

ALTER TABLE `transporter` 
CHANGE COLUMN `introduction_date` `introduction_date` DATETIME NULL DEFAULT NULL ;

ALTER TABLE `purchase_service_order` 
ADD COLUMN `transporter_warehouse_fkid` BIGINT(10) NULL DEFAULT NULL AFTER `vehicle_fkid`;

ALTER TABLE `purchase_service_order` 
ADD COLUMN `notes` VARCHAR(1000) NULL DEFAULT NULL AFTER `transporter_warehouse_fkid`;

ALTER TABLE `purchase_service_order` 
CHANGE COLUMN `instructions` `instructions` VARCHAR(1000) NULL DEFAULT NULL ,
CHANGE COLUMN `payment_terms` `payment_terms` VARCHAR(1000) NULL DEFAULT NULL ;

ALTER TABLE `purchase_service_order` CHANGE COLUMN `final_total` `final_total` DECIMAL(20,2) NULL DEFAULT NULL ;




